<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    //Page header
    function metNumeroOrdenPago($var = false)
    {
        if ($var) {
            $this->nroOrden = $var;
        } else {
            return $this->nroOrden;
        }
    }

    function metOrganismo($var = false)
    {
        if ($var) {
            $this->organismo = $var;
        } else {
            return $this->organismo;
        }
    }

    function metfecOrdenPago($var = false)
    {
        if ($var) {
            $this->fecOrdenPago = $var;
        } else {
            return $this->fecOrdenPago;
        }
    }

    function metHoja($var = false)
    {
        if ($var) {
            $this->hoja = $var;
        } else {
            return $this->hoja;
        }
    }

    function metArrayAprueba($var = false, $posicion = false)
    {
        if ($var) {
            $this->arrayAprueba[$posicion] = $var;
        } else {
            if (!isset($this->arrayAprueba[$posicion])) {
                return '';
            } else {
                return $this->arrayAprueba[$posicion];
            }
        }
    }

    function metArrayRevisa($var = false, $posicion = false)
    {
        if ($var) {
            $this->arrayRevisa[$posicion] = $var;
        } else {
            if (!isset($this->arrayRevisa[$posicion])) {
                return '';
            } else {
                return $this->arrayRevisa[$posicion];
            }
        }
    }

    function metArrayConforma($var = false, $posicion = false)
    {
        if ($var) {
            $this->arrayConforma[$posicion] = $var;
        } else {
            if (!isset($this->arrayConforma[$posicion])) {
                return '';
            } else {
                return $this->arrayConforma[$posicion];
            }
        }
    }

    function metArrayEmitido($var = false, $posicion = false)
    {
        if ($var) {
            $this->arrayEmitido[$posicion] = $var;
        } else {
            if (!isset($this->arrayEmitido[$posicion])) {
                return '';
            } else {
                return $this->arrayEmitido[$posicion];
            }
        }
    }

    function Header()
    {

    }

    //Page footer
    function Footer()
    {
        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetXY(10, 227);
        $this->Rect(10, 227, 48.75, 25, "D");
        $this->Rect(58.75, 227, 48.75, 25, "D");
        $this->Rect(107.5, 227, 48.75, 25, "D");
        $this->Rect(156.25, 227, 48.75, 25, "D");
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(48.75, 5, 'EMITIDO POR', 1, 0, 'L');
        $this->Cell(48.75, 5, 'REVISADO POR', 1, 0, 'L');
        $this->Cell(48.75, 5, 'CONFORMADO POR', 1, 0, 'L');
        $this->Cell(48.75, 5, 'APROBADO POR', 1, 0, 'L');
        ##
        $this->SetFont('Arial', 'B', 7);
        $this->SetXY(10, 233);
        $this->MultiCell(48.75, 3, utf8_decode(PAGUESE), 0, 'L');
        $this->SetXY(58.75, 233);
        $this->MultiCell(48.75, 3, utf8_decode(NOMBRE_REVISA), 0, 'L');
        $this->SetXY(107.5, 233);
        $this->MultiCell(48.75, 3, utf8_decode($this->metArrayConforma(false, 'Nombre')), 0, 'L');
        $this->SetXY(156.25, 233);
        $this->MultiCell(48.75, 3, utf8_decode($this->metArrayAprueba(false, 'Nombre')), 0, 'L');
        ##
        $this->SetXY(10, 240);
        $this->MultiCell(48.75, 3, utf8_decode(CARGO_USUARIO), 0, 'C');
        $this->SetXY(58.75, 240);
        $this->MultiCell(48.75, 3, utf8_decode(CARGO_REVISA), 0, 'C');
        $this->SetXY(107.5, 240);
        $this->MultiCell(48.75, 3, utf8_decode(CARGO_CONFORMA), 0, 'C');
        $this->SetXY(156.25, 240);
        $this->MultiCell(48.75, 3, utf8_decode(CARGO_APRUEBA), 0, 'C');
        ##
        $this->SetXY(10, 252);
        $this->Cell(64, 5, 'Entrega Contra Factura Original', 1, 0, 'L', 1);
        $this->Cell(33.5, 5, 'Recibido Por', 1, 0, 'L', 1);
        $this->Cell(33, 5, 'C.I. No.', 1, 0, 'L', 1);
        $this->Cell(32, 5, 'Firma', 1, 0, 'L', 1);
        $this->Cell(32.5, 5, 'Fecha', 1, 0, 'L', 1);
        $this->Ln(5);
        $this->Cell(64, 15, '________ SI ________ NO', 1, 0, 'C', 1);
        $this->Cell(33.5, 15, '', 1, 0, 'L', 1);
        $this->Cell(33, 15, '', 1, 0, 'L', 1);
        $this->Cell(32, 15, '', 1, 0, 'L', 1);
        $this->Cell(32.5, 15, '', 1, 0, 'L', 1);
    }

}
