<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    //Page header
    function metNumeroOrdenPago($var=false)
    {
        if($var){
            $this->nroOrden = $var;
        }else{
            return $this->nroOrden;
        }
    }

    function metOrganismo($var=false)
    {
        if($var){
            $this->organismo = $var;
        }else{
            return $this->organismo;
        }
    }

    function metfecOrdenPago($var=false)
    {
        if($var){
            $this->fecOrdenPago = $var;
        }else{
            return $this->fecOrdenPago;
        }
    }

    function metHoja($var=false)
    {
        if($var){
            $this->hoja = $var;
        }else{
            return $this->hoja;
        }
    }

    function metArrayAprueba($var=false,$posicion=false)
    {
        if($var){
            $this->arrayAprueba[$posicion] = $var;
        }else{
            if(!isset($this->arrayAprueba[$posicion])){
                return '';
            }else{
                return $this->arrayAprueba[$posicion];
            }
        }
    }

    function metArrayRevisa($var=false,$posicion=false)
    {
        if($var){
            $this->arrayRevisa[$posicion] = $var;
        }else{
            if(!isset($this->arrayRevisa[$posicion])){
                return '';
            }else{
                return $this->arrayRevisa[$posicion];
            }
        }
    }

    function metArrayConforma($var=false,$posicion=false)
    {
        if($var){
            $this->arrayConforma[$posicion] = $var;
        }else{
            if(!isset($this->arrayConforma[$posicion])){
                return '';
            }else{
                return $this->arrayConforma[$posicion];
            }
        }
    }

    function Header()
    {
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 5, 10, 10);
        $this->SetFont('Arial', '', 10);
        $this->SetXY(20, 5);
        $this->Cell(100, 5, $this->metOrganismo(), 0, 1, 'L');
        $this->SetXY(20, 10);
        $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN Y PRESUPUESTO'), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->SetXY(165, 5);
        $this->Cell(20, 5, utf8_decode('Nro. Orden: '), 0, 0, 'L');
        $this->Cell(30, 5, $this->metNumeroOrdenPago(), 0, 1, 'L');
        $this->SetFont('Arial', '', 10);
        $this->SetXY(165, 10);
        $this->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->Cell(30, 5, $this->metfecOrdenPago(), 0, 1, 'L');
        $this->SetXY(165, 15);
        $this->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->Cell(30, 5, $this->PageNo() . ' de {nb}', 0, 1, 'L');
        $this->SetFont('Arial', 'B', 14);
        $this->SetXY(10, 20);
        $this->Cell(195, 5, utf8_decode($this->metHoja()), 0, 1, 'C', 0);

    }

    //Page footer
    function Footer()
    {
        if ($this->metHoja() == "ORDEN DE PAGO") {
            /*$this->SetXY(10, 232);
            $nombreResponsable='REVISADO POR: MANUEL MAITA';

            $this->Cell(110,  1, utf8_decode($nombreResponsable.' ________________  '), 0, 0, 'L');
            $this->Cell(10, 1, '__/__/____', 0, 0, 'L');*/

            $this->SetTextColor(0, 0, 0);
            $this->SetDrawColor(0, 0, 0);
            $this->SetFillColor(255, 255, 255);
            $this->SetXY(10, 237);
            $this->Rect(10, 237, 65, 35, "D");
            $this->Rect(75, 237, 65, 35, "D");
            $this->Rect(140, 237, 65, 35, "D");
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(65, 5, 'PAGUESE', 1, 0, 'L');
            $this->Cell(65, 5, 'CONFORME', 1, 0, 'L');
            $this->Cell(65, 5, 'BENEFICIARIO', 1, 1, 'L');
            ##
            $this->SetFont('Arial', 'B', 8);
            $this->SetXY(10, 242); $this->MultiCell(65, 4, 	utf8_decode(PAGUESE), 0, 'L');
            $this->SetXY(75, 242); $this->MultiCell(65, 4, 	utf8_decode(COMFORMA), 0, 'L');
            $this->SetXY(140, 242); $this->MultiCell(65, 4, '', 0, 'L');
            $this->SetXY(10, 246); $this->MultiCell(65, 4, 	utf8_decode(CARGO_APRUEBA), 0, 'C');
            $this->SetXY(75, 246); $this->MultiCell(65, 4, 	utf8_decode(CARGO_CONFORMA), 0, 'C');
            $this->SetXY(140, 246); $this->MultiCell(65, 4, '', 0, 'C');
            ##
            $this->SetXY(10, 263);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(65, 4, 'FECHA: '.FECHA_PAGUESE, 0, 0, 'L');
            $this->Cell(65, 4, 'FECHA: '.FECHA_CONFORMA, 0, 0, 'L');
            ##
            $this->SetXY(10, 268);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(65, 4, 'FIRMA Y SELLO', 1, 0, 'C');
            $this->Cell(65, 4, 'FIRMA Y SELLO', 1, 0, 'C');
            $this->Cell(65, 4, 'FIRMA', 1, 1, 'C');
        }
    }
}
