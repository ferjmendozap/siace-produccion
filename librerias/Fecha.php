<?php

class Fecha
{

    /**
     * Formatear una fecha.
     *
     * @param  String  Fecha a formatear
     * @param  String  Formato de entrada
     * @param  String  Formato de salida
     * @return String
     */
    public static function formatFecha($fecha, $formatoInput='d-m-Y', $formatoOutput='Y-m-d')
    {
        if ($fecha && static::validateDate($fecha, $formatoInput)) return \Datetime::createFromFormat($formatoInput, $fecha)->format($formatoOutput);
        else return $fecha;
    }

    /**
     * Validar una fecha.
     *
     * @param  String  Fecha a validar
     * @param  String  Formato a validar
     * @return Boolean
     */
    public static function validateDate($fecha, $formato = 'Y-m-d H:i:s') 
    {
        $d = \Datetime::createFromFormat($formato, $fecha);
        return $d && $d->format($formato) == $fecha;
    }

    /**
     * Obtener la edad a partir de una fecha.
     *
     * @param  String  Fecha del calculo
     * @return String
     */
    public static function edad($fecha) 
    {
        list($a, $m, $d) = explode('-', $fecha);
        return \Datetime::createFromDate($a, $m, $d)->age;
    }

    /**
     * Obtener el dia de la semana.
     *
     * @param  String  Fecha del calculo
     * @return String
     */
    public static function dia_semana($fecha) 
    {
        return date('N', strtotime($fecha));
    }

    /**
     * Obtener el nombre del dia de la semana.
     *
     * @param  String  Fecha del calculo
     * @return String
     */
    public static function dia_semana_nombre($fecha) 
    {
        $dias = array('','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
        
        return $dias[date('N', strtotime($fecha))];
    }

    /**
     * Obtener el nombre del mes.
     *
     * @param  String  Nro. del Mes
     * @return String
     */
    public static function mes_nombre($mes) 
    {
        $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        
        return $meses[intval($mes)];
    }

    /**
     * Obtener dias entre 2 fechas.
     *
     * @param  String  fecha inicial
     * @param  String  fecha final
     * @return String
     * Ejemplo: dias_fecha('2016-12-01','2016-12-22')
     */
    public static function dias_fecha($fecha_ini,$fecha_fin)
    {
        $datetime1 = new DateTime($fecha_ini);
        $datetime2 = new DateTime($fecha_fin);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%a');

    }

    /**
     * Sumar dias a una fecha.
     *
     * @param  String  fecha inicial
     * @param  Integer dias a sumar
     * @param  String  formato de salida (Y-m-d)
     * @return String
     * Ejemplo: sumar_dias('2016-12-01',5,'Y-m-d')
     */
    public static function sumar_dias($fecha_inicial,$dias,$formato='Y-m-d')
    {
        $fecha = date_create($fecha_inicial);
        date_add($fecha, date_interval_create_from_date_string("$dias days"));
        return date_format($fecha, $formato);
    }

    /**
     * Sumar meses a una fecha.
     *
     * @param  String  fecha inicial
     * @param  Integer meses a sumar
     * @param  String  formato de salida (Y-m-d)
     * @return String
     * Ejemplo: sumar_meses('2016-12-01',5,'Y-m-d')
     */
    public static function sumar_meses($fecha_inicial,$meses,$formato='Y-m-d')
    {
        $fe = date($formato, strtotime("$fecha_inicial + $meses months"));
        return $fe;
    }
}