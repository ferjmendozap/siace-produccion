<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;


class pdfEmpleado extends FPDF
{
    var $widths;
    var $aligns;

    function Header($valor1,$titulo){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.'CES.jpg', 10, 10, 22);
        $this->SetXY(33, 10);
        $this->Cell(260, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');
        $this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15);
        $this->Cell(260, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20);
        $this->Cell(260, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(350, 5, utf8_decode($titulo),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 6);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        if($valor1==1)
        {
            $this->SetWidths(array(15, 55, 45, 20, 15, 35, 30, 30, 10, 35, 15, 35));
            $this->SetAligns(array('C', 'L', 'L', 'L', 'R', 'L', 'L', 'L', 'C', 'L', 'L', 'L'));

            $this->Row(array('Fecha de Ingreso',
                'Dependencia',
                'Cargo',
                utf8_decode('Categoría'),
                'Sueldo',
                utf8_decode('Nómina'),
                'Pago',
                'Tipo Trabajador',
                'Estado',
                'Motivo de Cese',
                'Fecha de Cese',
                utf8_decode('Razón de Cese')));

        }
        elseif($valor1==2)
        {
            $this->SetWidths(array(20, 30, 60, 55, 40, 30, 40, 50));
            $this->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $this->Row(array('Fecha', 'Tipo', 'Dependencia', 'Cargo', utf8_decode('Categoría'), 'Sueldo', utf8_decode('Nómina'), 'Responsable'));
        }
        $this->SetFont('Arial', '', 6);
        $this->Ln();
    }


    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(275,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger){
            $this->AddPage($this->CurOrientation);
            $this->Header(1);
        }
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}


#PERMITE GENERAR REPORTE ORDEN DEL BENEFICIO DE UTILES
class pdfOrdenUtiles extends FPDF
{


    var $widths;
    var $aligns;


    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

}


?>
