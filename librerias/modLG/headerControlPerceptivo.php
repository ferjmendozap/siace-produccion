<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header(
    //    $fdesde,$fhasta
    )
    {
        $this->SetTitle('Control Perceptivo');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 20, 12); $this->SetFont('Arial', '', 8);

        $this->SetFont('Arial', '', 8);
        $this->SetXY(25, 5); $this->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->SetXY(25, 10); $this->Cell(100, 5,utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');

        $this->SetXY(225, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
        $this->SetXY(225, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');

        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(40, 20); $this->Cell(195, 5, utf8_decode('Controles Perceptivos'), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 7);
        $this->SetXY(5, 22); $this->Cell(195, 5, utf8_decode(' Desde: '.DESDE.' Hasta:  '.HASTA), 0, 1, 'L');

        $this->Ln(5);
        $this->SetDrawColor(255, 255, 255);
        $this->SetFillColor(200, 200, 200);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 7);
        $this->SetWidths(array(25, 40, 71, 30,70, 30));
        $this->SetAligns(array('C', 'C', 'C', 'C','C', 'C'));
        $this->Row(array(utf8_decode('Control Perceptivo'),
            utf8_decode('Nº de Procedimiento'),
            'Nombre de Procedimiento',
            utf8_decode('Nº de Orden de Compra'),
            'Proveedor',
            utf8_decode('Fecha de Realización')));

        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {
        $this->Ln(3);
        $y = -30;
        $this->SetXY(10, $y);
        $this->Cell(30, 6,'______________________________________', 0, 1, 'L');
        $this->SetFont('Arial', '', 7);
        $this->SetXY(10, $y+5);
        $this->Cell(120, 3, ('Elaborado Por:'), 0, 0, 'L');
        $this->SetXY(10, $y+10);
        $this->Cell(120, 3, utf8_decode(ELABORADO_POR), 0, 0, 'L');//nombre de quien elabora
        $this->SetXY(10, $y+15);
        $this->Cell(120, 3, utf8_decode(ELABORADO_CARGO), 0, 0, 'L');//cargo de quien elabora

    }
}
