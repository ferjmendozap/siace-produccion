<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetTitle('Invitacion de Proveedores');
        $this->AliasNbPages();
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.png", 8, 5, 19, 15);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.'LOGOSNCF.png', 190, 5, 12, 12);
        $this->SetFont('Arial', '', 8);

        $this->SetXY(8, 5); $this->Cell(190, 5, utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'), 0, 1, 'C');
        $this->SetXY(8, 8); $this->Cell(190, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'C');
        $this->SetXY(8, 11); $this->Cell(190, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 1, 'C');
        $this->SetXY(8, 14); $this->Cell(190, 5, utf8_decode('DIVISIÓN DE COMPRAS Y SUMINISTROS' ), 0, 1, 'C');
        $this->Ln(10);
    }
    function primeraPagina()
    {
        $this->SetXY(170, 17); $this->Cell(15, 5, utf8_decode('Invitación N°: '), 0, 0, 'R');
        $this->Cell(60, 5, NRO_INVITACION, 0, 1, 'L');
        $this->SetXY(170, 22); $this->Cell(15, 5, utf8_decode('# Acta de Inicio: '), 0, 0, 'R');
        $this->Cell(60, 5, COD_ACTA, 0, 1, 'L');
        $this->SetXY(170, 26); $this->Cell(15, 5, 'Fecha: ', 0, 0, 'R');
        $this->Cell(60, 5, FECHA_INV, 0, 1, 'L');

        $this->Ln(10);
        $this->SetXY(10, 30);
        $this->SetFillColor(250, 250, 250);
        $this->SetFont('Arial', '', 8);
        $this->Cell(35, 5, 'Proveedor: ', 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(150, 6, utf8_decode(PROVEEDOR), 0, 1, 'L');
        $this->SetFont('Arial', '', 8);
        $this->Cell(35, 5, 'R.I.F: ', 0, 0, 'L', 1);
        $this->Cell(50, 6, RIF, 0, 1, 'L');
        $this->Cell(35, 5, 'Domicilio: ', 0, 0, 'L', 1);
        $this->MultiCell(150, 6, utf8_decode(
            DIRECCION
        ), 0, 'L');
        $this->Cell(35, 5, 'Telefono Contacto: ', 0, 0, 'L', 1);
        $this->Cell(35, 6, TELEFONO , 0, 0, 'L');
        $this->Cell(15, 5, 'Fax: ', 0, 0, 'L', 1);
        $this->Cell(50, 6, FAX, 0, 1, 'L');
        $this->Ln(2);
    }

    //Page footer
    function Footer()
    {

    }
}
