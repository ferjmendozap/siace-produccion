<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Marcos Gonzalez                  |dt.ait.programador2@cgesucre.gob.ve |         0414-1909537           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        14-12-2017       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{

    function Header()
    {

        $this->SetTitle('Invitacion de Proveedores');
        $this->AliasNbPages();
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 20, 12);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.'LOGOSNCF.png', 190, 5, 15, 12);
        $this->SetFont('Arial', '', 8);

        $this->SetXY(20, 5); $this->Cell(190, 5, utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'), 0, 1, 'C');
        $this->SetXY(20, 8); $this->Cell(190, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'C');
        $this->SetXY(20, 11); $this->Cell(190, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 1, 'C');
        $this->SetXY(20, 14); $this->Cell(190, 5, utf8_decode('DIVISIÓN DE COMPRAS Y SUMINISTROS' ), 0, 1, 'C');
        $this->Ln(5);






    }

   function primeraPagina()
    {
        $this->SetXY(170, 17); $this->Cell(15, 5, utf8_decode('Invitación N°: '), 0, 0, 'R');
         $this->Cell(60, 5, NRO_INVITACION, 0, 1, 'L');
        $this->SetXY(170, 22); $this->Cell(15, 5, utf8_decode('# Acta de Inicio: '), 0, 0, 'R');
         $this->Cell(60, 5, COD_ACTA, 0, 1, 'L');
        $this->SetXY(170, 26); $this->Cell(15, 5, 'Fecha: ', 0, 0, 'R');
         $this->Cell(60, 5, FECHA_INV, 0, 1, 'L');

        $this->Ln(10);
        $this->SetXY(5, 30);
        $this->SetFillColor(250, 250, 250);
        $this->SetFont('Arial', '', 8);
        $this->Cell(45, 5, 'Proveedor: ', 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8);
         $this->Cell(150, 6, utf8_decode(PROVEEDOR), 0, 1, 'L');
        $this->SetFont('Arial', '', 8);
        $this->Cell(35, 5, 'R.I.F: ', 0, 0, 'L', 1);
         $this->Cell(50, 6, RIF, 0, 1, 'L');
        $this->Cell(35, 5, 'Domicilio: ', 0, 0, 'L', 1);
         $this->MultiCell(150, 6, utf8_decode(
             DIRECCION
        ), 0, 'L');
        $this->Cell(35, 5, 'Telefono Contacto: ', 0, 0, 'L', 1);
           $this->Cell(35, 6, TELEFONO , 0, 0, 'L');
        $this->Cell(15, 5, 'Fax: ', 0, 0, 'L', 1);
         $this->Cell(50, 6, FAX, 0, 1, 'L');
        $this->Ln(2);


    }
    /*  function campos()
     {
         $this->SetDrawColor(0, 0, 0);
         $this->SetFillColor(255, 255, 255);
         $this->SetTextColor(0, 0, 0);
         $this->SetFont('Arial', 'B', 8);
         $this->SetWidths(array(10, 20, 120, 20, 20));
         $this->SetAligns(array('R', 'C', 'L', 'C', 'R'));
         $this->Row(array('#', IC, 'DESCRIPCION', 'UNI.', 'CANT.'));
         $this->Ln(1);
     }
 
     //Page footer
     function Footer()
     {
 
         $this->SetDrawColor(0, 0, 0);
         $this->SetFillColor(0, 0, 0);
         $this->SetTextColor(0, 0, 0);
         $this->Ln(2);
         $this->SetFont('Arial', 'B', 7);
         $this->SetY(225);
         $this->Rect(20, 223, 65, 0.1, "DF");
         $this->SetXY(20, 225); $this->Cell(20, 4, 'Solicitado Por:', 0, 0, 'R'); $this->Cell(80, 4, NOMBRE_PREPARADO, 0, 1, 'L');
         $this->SetXY(20, 229); $this->Cell(20, 4, 'Cargo:', 0, 0, 'R'); $this->Cell(80, 4, CARGO_PREPARADO, 0, 1, 'L');
         $this->SetXY(20, 233); $this->Cell(20, 4, 'Fecha:', 0, 0, 'R'); $this->Cell(80, 4, FECHA_PREPARADO, 0, 1, 'L');
         ##
         $this->SetY(225);
         $this->Rect(130, 223, 65, 0.1, "DF");
         $this->SetXY(130, 225); $this->Cell(20, 4, 'Revisado Por:', 0, 0, 'R'); $this->Cell(80, 4, NOMBRE_REVISADO, 0, 1, 'L');
         $this->SetXY(130, 229); $this->Cell(20, 4, 'Cargo:', 0, 0, 'R'); $this->Cell(80, 4, CARGO_REVISADO, 0, 1, 'L');
         $this->SetXY(130, 233); $this->Cell(20, 4, 'Fecha:', 0, 0, 'R'); $this->Cell(80, 4, FECHA_REVISADO, 0, 1, 'L');
         ##
         $this->SetY(250);
         $this->Rect(20, 248, 65, 0.1, "DF");
         $this->SetXY(20, 250); $this->Cell(20, 4, 'Conformado Por:', 0, 0, 'R'); $this->Cell(80, 4, NOMBRE_CONFORMADO, 0, 1, 'L');
         $this->SetXY(20, 254); $this->Cell(20, 4, 'Cargo:', 0, 0, 'R'); $this->Cell(80, 4, CARGO_CONFORMADO, 0, 1, 'L');
         $this->SetXY(20, 258); $this->Cell(20, 4, 'Fecha:', 0, 0, 'R'); $this->Cell(80, 4, FECHA_CONFORMADO, 0, 1, 'L');
         ##
         $this->SetY(250);
         $this->Rect(130, 248, 65, 0.1, "DF");
         $this->SetXY(130, 250); $this->Cell(20, 4, 'Aprobado Por:', 0, 0, 'R'); $this->Cell(80, 4, NOMBRE_APROBADO, 0, 1, 'L');
         $this->SetXY(130, 254); $this->Cell(20, 4, 'Cargo:', 0, 0, 'R'); $this->Cell(80, 4, CARGO_APROBADO, 0, 1, 'L');
         $this->SetXY(130, 258); $this->Cell(20, 4, 'Fecha:', 0, 0, 'R'); $this->Cell(80, 4, FECHA_APROBADO, 0, 1, 'L');
     }*/
}
