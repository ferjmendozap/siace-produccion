<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT . 'librerias' . DS . 'modLG' . DS . 'tcpdf' . DS . 'tcpdf.php';
//require_once LIBRERIA_FPDF;

class pdf extends TCPDF
{
    function Header()
    {
        $this->SetTitle('Catálogo de Proveedores');
        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 22);
        $this->SetFont('times','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,'REPÚBLICA BOLIVARIANA DE VENEZUELA',0,1,'L');
        $this->SetXY(35,16); $this->Cell(100,5,'CONTRALORIA DEL ESTADO SUCRE',0,1,'L');
        $this->SetXY(35,20); $this->Cell(100,5,'DIRECCIÓN DE ADMINISTRACIÓN',0,1,'L');
        ## 
        $this->SetXY(222,12); $this->Cell(25,5,'Fecha: ' . date('d-m-Y'),0,1,'L');
       // $this->SetXY(222,12); $this->Cell(25,5,'Fecha:  23-02-2018' ,0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,'Página: ' . $this->PageNo() ,0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.jpg", 250, 10, 15, 15);
        $this->SetXY(90,30); $this->Cell(100,5,'Catálogo de Proveedores',0,1,'C');
        $this->Cell(20, 5, 'CUADRO COMPARATIVO DE OFERTAS ', 0, 0, 'L');
        $this->Ln(10);
    }

    //Page footer
    function Footer()
    {

        $this->SetXY(10, 180);
        $this->SetFont('times', 'B', 10);

        #Elaborador por y Revisado por Nombres
        $this->Cell(165, 6,'Elaborado Por:', 0, 0, 'C');
        $this->Cell(165, 6,'Revisado Por:', 0, 0, 'C');

        $this->Ln();
        $this->Cell(165, 6,strtoupper(ELABORADO_POR), 0, 0, 'C');
        $this->Cell(165, 6,strtoupper(REVISADO_POR), 0, 0, 'C');
        $this->Ln();
        $this->Cell(165, 6,strtoupper(CARGO_ELABORADO_POR), 0, 0, 'C');
        $this->Cell(165, 6,strtoupper(CARGO_REVISADO_POR), 0, 0, 'C');
    }
}
