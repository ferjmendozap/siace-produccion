<?php
require_once APP_PATH.'control'.DS.'db.php';

class ConsultarDescripcion
{
    
    /**
     * Mostrar un lista de valores para un select.
     *
     * @param  String  $tabla
     * @param  String  $campo
     * @param  String  $pk
     * @return String
     */

    public static function Consultar($pk, $campo, $tabla)
    {
        $data = '';

        $db = new db();
        $query = $db->query("SELECT * FROM $tabla WHERE $campo= '$pk' "); 
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $field = $query->fetchAll();
        
        foreach ($field as $f) {
                    $data .= $f['ind_descripcion'];
                }
        return $data;
    }

    public static function Consultar2($pk, $campo, $tabla)
    {
        $data = '';

        $db = new db();
        $query = $db->query("SELECT * FROM $tabla WHERE $campo= '$pk' ");
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $field = $query->fetchAll();
        
        foreach ($field as $f) {
                    $data .= $f['ind_nombre1'].' '.$f['ind_nombre2'].' '.$f['ind_apellido1'].' '.$f['ind_apellido2'];
                }
        return $data;
    }
    
    /**
     * @param  String  $codigo
     * @param  String  $valor
     * @param  Integer  $opt
     * @return String
     */
    public static function options($codigo, $valor=NULL, $opt=0)
    {
        switch ($codigo)
        {
            case 'cb_estado_voucher':
                $v['AB'] = 'Abierto';
                $v['AP'] = 'Aprobado';
                $v['MA'] = 'Mayorizado';
                $v['AN'] = 'Anulado';
                break;
        }

        $data = '';
        switch ($opt)
        {
            case 0:
                $data = $v[$valor];
                break;

            case 1:
                foreach ($v as $key => $value) {
                    $selected = (($valor == $key)?'selected':'');
                    $data .= '<option value="'.$key.'" '.$selected.'>'.$value.'</option>' ;
                }
                break;
        }

        return $data;
    }

    
}