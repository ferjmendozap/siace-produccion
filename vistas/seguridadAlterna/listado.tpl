<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Seguridad Alterna - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}seguridadAlterna/crear';
        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}seguridadAlterna/jsonDataTabla",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "ind_usuario" },
                { "data": "ind_nombre1" },
                { "data": "ind_apellido1" },
                { "orderable": false,"data": "acciones",'width':100 }
            ]
        );
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idUsuario: $(this).attr('idUsuario')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>