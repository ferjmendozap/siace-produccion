<section class="style-default-light">&nbsp;
    <div id="myPrintArea">

        <div class="row" >

            <div class="col-lg-12" style="text-align: justify" >
                <div class="card card-underline">
                    <div class="card-head ">
                        <header>
                            <h2 class="text-primary">Cumpleañeros</h2>
                        </header>
                        <div class="tools">
                            <div class="btn-group">
                                <a id="imprime" class="btn btn-icon-toggle btn-refresh">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body style-default-bright">
                        <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-hover" WIDTH="90%" >
                            <thead>
                            <tr  align="center">
                                <th ><i class=""></i>N°</th>
                                <th ><i class=""></i>Imagen</th>{* md-insert-photo*}
                                <th ><i class=""></i>Nombre y Apellido</th>
                                <th ><i class=""></i>Dependencia</th>
                                <th ><i class="md-cake"></i>Día</th>
                            </tr>
                            </thead>
                            <tbody >
                            {foreach name=for_cumple item=indice from=$listadoCumpleMes}
                                <tr class="fila_agregar" >
                                    <td>{$smarty.foreach.for_cumple.iteration}</td>
                                    <td ><img class="img-circle width-0" src="{$_Parametros.ruta_Img}modRH/fotos/{$indice.ind_foto}" alt="" data-backdrop="static" ></td>
                                    <td>{$indice.ind_nombre1} {$indice.ind_apellido1}</td>
                                    <td>{$indice.ind_dependencia}</td>
                                    <td>{$indice.fec_nacimiento}</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        $("#imprime").click(function () {
            $("#myPrintArea").printThis();
        })
    });
</script>