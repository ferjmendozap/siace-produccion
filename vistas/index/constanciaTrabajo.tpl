<section class="style-default-light">&nbsp;
    <div >

        <div class="row" >

            <div class="col-lg-12" style="text-align: justify" >
                <div class="card card-underline">
                    <div class="card-body style-default-bright">
							<div class="row">
                                <div class="col-lg-12" >
                                    <table class="table no-margin table-hover">
                                        <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Foto</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>N° Documento</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="info">
                                            <th>&nbsp;</th>
                                            <td ><img class="img-circle width-1" src="{$_Parametros.ruta_Img}modRH/fotos/{$datosUsuario['ind_foto']}" alt="" data-backdrop="static" ></td>
                                            <td>{$datosUsuario['ind_nombre1']}</td>
                                            <td>{$datosUsuario['ind_apellido1']}</td>
                                            <td>{$datosUsuario['ind_cedula_documento']}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row" >&nbsp;
                            </div>
                            <div class="row" >
                                <div class="col-lg-4" >&nbsp;</div>
								<div class="col-lg-4" >
									<button type="button" class="btn btn-info btn-block" id="btn_ver_constancia" pk_num_empleado="{$datosUsuario['pk_num_empleado']}" data-target="#formModal2" data-toggle="modal" >
										<span class="pull-left"><i class="glyphicon glyphicon-download"></i></span>
										constancia de trabajo
									</button>
								</div>
                                <div class="col-lg-4" >&nbsp;</div>
							</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script type="text/javascript">
$('#btn_ver_constancia').click(function() {

        $('#modalAncho2').css("width", "85%");
    $('#formModalLabel2').html(" ");
        $('#formModalLabel2').html($(this).attr('titulo'));
        $('#ContenidoModal2').html(" ");
        var pk_num_empleado = $(this).attr('pk_num_empleado');
        var urlReporte = '{$_Parametros.url}modRH/reportes/constanciaTrabajoCONTROL/GenerarConstanciaTrabajoMET/?pk_num_empleado=' + pk_num_empleado;
        $('#ContenidoModal2').html('<iframe src="' + urlReporte + '" width="100%" height="950"></iframe>');

});
</script>