<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Usuario - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>usuario</th>
                                <th>nombre y apellido</th>
                                <th>template</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=usuario from=$listado}
                                <tr id="idUsuario{$usuario.pk_num_seguridad_usuario}">
                                    <td><label>{$usuario.ind_usuario}</label></td>
                                    <td><label>{$usuario.ind_nombre1} {$usuario.ind_nombre2} {$usuario.ind_apellido1} {$usuario.ind_apellido2}</label></td>
                                    <td><label>{$usuario.ind_template}</label></td>
                                    <td>
                                        <i class="{if $usuario.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('AP-04-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idUsuario="{$usuario.pk_num_seguridad_usuario}" title="Editar"
                                                    descipcion="El Usuario a Modificado un usuario del sistema" titulo="Modificar Usuario">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('AP-04-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUsuario="{$usuario.pk_num_seguridad_usuario}"  boton="si, Eliminar" title="Eliminar"
                                                    descipcion="El usuario a eliminado un usuario del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Usuario!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">
                                {if in_array('AP-04-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un usuario del sistema"  titulo="Crear Usuario" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Usuario
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}usuario/crear';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idUsuario:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idUsuario: $(this).attr('idUsuario')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idUsuario=$(this).attr('idUsuario');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}usuario/Eliminar';
                $.post($url, { idUsuario: idUsuario },function(dato){
                    if(dato['status']=='OK'){
                        $(document.getElementById('idUsuario'+dato['idUsuario'])).html('');
                        swal("Eliminado!", "el usuario fue eliminado satisfactoriamente.", "success");
                    }
                },'json');
            });
        });
    });
</script>