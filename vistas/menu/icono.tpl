<div class="card">
    <div class="card-head">
        <ul class="nav nav-tabs" data-toggle="tabs">
            <li class="active"><a href="#MaterialDesign">Material Design Iconic</a></li>
            <li><a href="#FontAwesome">Font Awesome</a></li>
            <li><a href="#Icomoon">Icomoon</a></li>
        </ul>
    </div><!--end .card-head -->
    <div class="card-body tab-content">
        <div class="tab-pane active" id="MaterialDesign">
            <section>
                <div class="section-body">
                    <!-- BEGIN INTRO -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-primary">Material Design Iconic Font</h1>
                        </div><!--end .col -->
                    </div><!--end .row -->

                    <!-- BEGIN ACTION -->
                    <div class="card">
                        <div class="card-head"><header>Action</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-3d-rotation"></i> md-3d-rotation
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-accessibility"></i> md-accessibility
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-account-balance"></i> md-account-balance
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-account-balance-wallet"></i> md-account-balance-wallet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-account-box"></i> md-account-box
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-account-child"></i> md-account-child
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-account-circle"></i> md-account-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-add-shopping-cart"></i> md-add-shopping-cart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-alarm"></i> md-alarm
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-alarm-add"></i> md-alarm-add
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-alarm-off"></i> md-alarm-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-alarm-on"></i> md-alarm-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-android"></i> md-android
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-announcement"></i> md-announcement
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-aspect-ratio"></i> md-aspect-ratio
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-assessment"></i> md-assessment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-assignment"></i> md-assignment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-assignment-ind"></i> md-assignment-ind
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-assignment-late"></i> md-assignment-late
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-assignment-return"></i> md-assignment-return
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-assignment-returned"></i> md-assignment-returned
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-assignment-turned-in"></i> md-assignment-turned-in
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-autorenew"></i> md-autorenew
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-backup"></i> md-backup
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-book"></i> md-book
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-bookmark"></i> md-bookmark
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-bookmark-outline"></i> md-bookmark-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-bug-report"></i> md-bug-report
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cached"></i> md-cached
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-class"></i> md-class
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-credit-card"></i> md-credit-card
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dashboard"></i> md-dashboard
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-delete"></i> md-delete
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-description"></i> md-description
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dns"></i> md-dns
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-done"></i> md-done
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-done-all"></i> md-done-all
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-event"></i> md-event
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-exit-to-app"></i> md-exit-to-app
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-explore"></i> md-explore
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-extension"></i> md-extension
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-face-unlock"></i> md-face-unlock
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-favorite"></i> md-favorite
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-favorite-outline"></i> md-favorite-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-find-in-page"></i> md-find-in-page
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-find-replace"></i> md-find-replace
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flip-to-back"></i> md-flip-to-back
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flip-to-front"></i> md-flip-to-front
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-get-app"></i> md-get-app
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-grade"></i> md-grade
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-group-work"></i> md-group-work
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-help"></i> md-help
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-highlight-remove"></i> md-highlight-remove
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-history"></i> md-history
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-home"></i> md-home
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-https"></i> md-https
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-info"></i> md-info
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-info-outline"></i> md-info-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-input"></i> md-input
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-invert-colors"></i> md-invert-colors
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-label"></i> md-label
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-label-outline"></i> md-label-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-language"></i> md-language
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-launch"></i> md-launch
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-list"></i> md-list
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-lock"></i> md-lock
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-lock-open"></i> md-lock-open
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-lock-outline"></i> md-lock-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-loyalty"></i> md-loyalty
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-markunread-mailbox"></i> md-markunread-mailbox
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-note-add"></i> md-note-add
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-open-in-browser"></i> md-open-in-browser
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-open-in-new"></i> md-open-in-new
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-open-with"></i> md-open-with
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-pageview"></i> md-pageview
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-payment"></i> md-payment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-perm-camera-mic"></i> md-perm-camera-mic
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-perm-contact-cal"></i> md-perm-contact-cal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-perm-data-setting"></i> md-perm-data-setting
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-perm-device-info"></i> md-perm-device-info
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-perm-identity"></i> md-perm-identity
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-perm-media"></i> md-perm-media
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-perm-phone-msg"></i> md-perm-phone-msg
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-perm-scan-wifi"></i> md-perm-scan-wifi
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-picture-in-picture"></i> md-picture-in-picture
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-polymer"></i> md-polymer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-print"></i> md-print
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-query-builder"></i> md-query-builder
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-question-answer"></i> md-question-answer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-receipt"></i> md-receipt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-redeem"></i> md-redeem
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-report-problem"></i> md-report-problem
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-restore"></i> md-restore
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-room"></i> md-room
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-schedule"></i> md-schedule
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-search"></i> md-search
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings"></i> md-settings
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-applications"></i> md-settings-applications
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-backup-restore"></i> md-settings-backup-restore
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-bluetooth"></i> md-settings-bluetooth
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-cell"></i> md-settings-cell
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-display"></i> md-settings-display
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-ethernet"></i> md-settings-ethernet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-input-antenna"></i> md-settings-input-antenna
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-input-component"></i> md-settings-input-component
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-input-composite"></i> md-settings-input-composite
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-input-hdmi"></i> md-settings-input-hdmi
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-input-svideo"></i> md-settings-input-svideo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-overscan"></i> md-settings-overscan
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-phone"></i> md-settings-phone
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-power"></i> md-settings-power
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-remote"></i> md-settings-remote
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-voice"></i> md-settings-voice
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-shop"></i> md-shop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-shopping-basket"></i> md-shopping-basket
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-shopping-cart"></i> md-shopping-cart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-shop-two"></i> md-shop-two
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-speaker-notes"></i> md-speaker-notes
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-spellcheck"></i> md-spellcheck
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-star-rate"></i> md-star-rate
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-stars"></i> md-stars
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-store"></i> md-store
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-subject"></i> md-subject
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-swap-horiz"></i> md-swap-horiz
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-swap-vert"></i> md-swap-vert
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-swap-vert-circle"></i> md-swap-vert-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-system-update-tv"></i> md-system-update-tv
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tab"></i> md-tab
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tab-unselected"></i> md-tab-unselected
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-theaters"></i> md-theaters
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-thumb-down"></i> md-thumb-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-thumbs-up-down"></i> md-thumbs-up-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-thumb-up"></i> md-thumb-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-toc"></i> md-toc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-today"></i> md-today
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-track-changes"></i> md-track-changes
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-translate"></i> md-translate
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-trending-down"></i> md-trending-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-trending-neutral"></i> md-trending-neutral
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-trending-up"></i> md-trending-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-turned-in"></i> md-turned-in
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-turned-in-not"></i> md-turned-in-not
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-verified-user"></i> md-verified-user
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-agenda"></i> md-view-agenda
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-array"></i> md-view-array
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-carousel"></i> md-view-carousel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-column"></i> md-view-column
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-day"></i> md-view-day
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-headline"></i> md-view-headline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-list"></i> md-view-list
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-module"></i> md-view-module
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-quilt"></i> md-view-quilt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-stream"></i> md-view-stream
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-view-week"></i> md-view-week
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-visibility"></i> md-visibility
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-visibility-off"></i> md-visibility-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wallet-giftcard"></i> md-wallet-giftcard
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wallet-membership"></i> md-wallet-membership
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wallet-travel"></i> md-wallet-travel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-work"></i> md-work
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END ACTION -->
                    <!-- BEGIN ALERT -->
                    <div class="card">
                        <div class="card-head"><header>Alert</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-error"></i> md-error
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-warning"></i> md-warning
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END ALERT -->
                    <!-- BEGIN AUDIO/VIDEO -->
                    <div class="card">
                        <div class="card-head"><header>Audio/Video</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-album"></i> md-album
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-av-timer"></i> md-av-timer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-closed-caption"></i> md-closed-caption
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-equalizer"></i> md-equalizer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-explicit"></i> md-explicit
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-fast-forward"></i> md-fast-forward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-fast-rewind"></i> md-fast-rewind
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-games"></i> md-games
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-hearing"></i> md-hearing
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-high-quality"></i> md-high-quality
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-loop"></i> md-loop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mic"></i> md-mic
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mic-none"></i> md-mic-none
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mic-off"></i> md-mic-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-movie"></i> md-movie
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-my-library-add"></i> md-my-library-add
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-my-library-books"></i> md-my-library-books
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-my-library-music"></i> md-my-library-music
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-new-releases"></i> md-new-releases
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-not-interested"></i> md-not-interested
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-pause"></i> md-pause
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-pause-circle-fill"></i> md-pause-circle-fill
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-pause-circle-outline"></i> md-pause-circle-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-play-arrow"></i> md-play-arrow
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-play-circle-fill"></i> md-play-circle-fill
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-play-circle-outline"></i> md-play-circle-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-playlist-add"></i> md-playlist-add
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-play-shopping-bag"></i> md-play-shopping-bag
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-queue"></i> md-queue
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-queue-music"></i> md-queue-music
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-radio"></i> md-radio
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-recent-actors"></i> md-recent-actors
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-repeat"></i> md-repeat
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-repeat-one"></i> md-repeat-one
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-replay"></i> md-replay
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-shuffle"></i> md-shuffle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-skip-next"></i> md-skip-next
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-skip-previous"></i> md-skip-previous
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-snooze"></i> md-snooze
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-stop"></i> md-stop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-subtitles"></i> md-subtitles
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-surround-sound"></i> md-surround-sound
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-videocam"></i> md-videocam
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-videocam-off"></i> md-videocam-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-video-collection"></i> md-video-collection
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-volume-down"></i> md-volume-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-volume-mute"></i> md-volume-mute
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-volume-off"></i> md-volume-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-volume-up"></i> md-volume-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-web"></i> md-web
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END AUDIO/VIDEO -->
                    <!-- BEGIN COMMUNICATION -->
                    <div class="card">
                        <div class="card-head"><header>Communication</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-business"></i> md-business
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-call"></i> md-call
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-call-end"></i> md-call-end
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-call-made"></i> md-call-made
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-call-merge"></i> md-call-merge
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-call-missed"></i> md-call-missed
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-call-received"></i> md-call-received
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-call-split"></i> md-call-split
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-chat"></i> md-chat
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-clear-all"></i> md-clear-all
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-comment"></i> md-comment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-contacts"></i> md-contacts
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dialer-sip"></i> md-dialer-sip
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dialpad"></i> md-dialpad
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dnd-on"></i> md-dnd-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-email"></i> md-email
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-forum"></i> md-forum
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-import-export"></i> md-import-export
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-invert-colors-off"></i> md-invert-colors-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-invert-colors-on"></i> md-invert-colors-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-live-help"></i> md-live-help
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-location-off"></i> md-location-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-location-on"></i> md-location-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-message"></i> md-message
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-messenger"></i> md-messenger
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-no-sim"></i> md-no-sim
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone"></i> md-phone
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-portable-wifi-off"></i> md-portable-wifi-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-quick-contacts-dialer"></i> md-quick-contacts-dialer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-quick-contacts-mail"></i> md-quick-contacts-mail
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-ring-volume"></i> md-ring-volume
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-stay-current-landscape"></i> md-stay-current-landscape
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-stay-current-portrait"></i> md-stay-current-portrait
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-stay-primary-landscape"></i> md-stay-primary-landscape
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-stay-primary-portrait"></i> md-stay-primary-portrait
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-swap-calls"></i> md-swap-calls
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-textsms"></i> md-textsms
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-voicemail"></i> md-voicemail
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-vpn-key"></i> md-vpn-key
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END COMMUNICATION -->
                    <!-- BEGIN CONTENT -->
                    <div class="card">
                        <div class="card-head"><header>Content</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-add"></i> md-add
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-add-box"></i> md-add-box
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-add-circle"></i> md-add-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-add-circle-outline"></i> md-add-circle-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-archive"></i> md-archive
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-backspace"></i> md-backspace
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-block"></i> md-block
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-clear"></i> md-clear
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-content-copy"></i> md-content-copy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-content-cut"></i> md-content-cut
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-content-paste"></i> md-content-paste
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-create"></i> md-create
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-drafts"></i> md-drafts
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-list"></i> md-filter-list
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flag"></i> md-flag
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-forward"></i> md-forward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-gesture"></i> md-gesture
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-inbox"></i> md-inbox
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-link"></i> md-link
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mail"></i> md-mail
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-markunread"></i> md-markunread
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-redo"></i> md-redo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-remove"></i> md-remove
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-remove-circle"></i> md-remove-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-remove-circle-outline"></i> md-remove-circle-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-reply"></i> md-reply
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-reply-all"></i> md-reply-all
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-report"></i> md-report
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-save"></i> md-save
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-select-all"></i> md-select-all
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-send"></i> md-send
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sort"></i> md-sort
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-text-format"></i> md-text-format
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-undo"></i> md-undo
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END CONTENT -->
                    <!-- BEGIN DEVICE -->
                    <div class="card">
                        <div class="card-head"><header>Device</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-access-alarm"></i> md-access-alarm
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-access-alarms"></i> md-access-alarms
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-access-time"></i> md-access-time
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-add-alarm"></i> md-add-alarm
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-airplanemode-off"></i> md-airplanemode-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-airplanemode-on"></i> md-airplanemode-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-20"></i> md-battery-20
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-30"></i> md-battery-30
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-50"></i> md-battery-50
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-60"></i> md-battery-60
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-80"></i> md-battery-80
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-90"></i> md-battery-90
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-alert"></i> md-battery-alert
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-charging-20"></i> md-battery-charging-20
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-charging-30"></i> md-battery-charging-30
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-charging-50"></i> md-battery-charging-50
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-charging-60"></i> md-battery-charging-60
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-charging-80"></i> md-battery-charging-80
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-charging-90"></i> md-battery-charging-90
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-charging-full"></i> md-battery-charging-full
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-full"></i> md-battery-full
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-std"></i> md-battery-std
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-battery-unknown"></i> md-battery-unknown
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-bluetooth"></i> md-bluetooth
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-bluetooth-connected"></i> md-bluetooth-connected
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-bluetooth-disabled"></i> md-bluetooth-disabled
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-bluetooth-searching"></i> md-bluetooth-searching
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-auto"></i> md-brightness-auto
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-high"></i> md-brightness-high
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-low"></i> md-brightness-low
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-medium"></i> md-brightness-medium
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-data-usage"></i> md-data-usage
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-developer-mode"></i> md-developer-mode
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-devices"></i> md-devices
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dvr"></i> md-dvr
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-gps-fixed"></i> md-gps-fixed
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-gps-not-fixed"></i> md-gps-not-fixed
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-gps-off"></i> md-gps-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-location-disabled"></i> md-location-disabled
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-location-searching"></i> md-location-searching
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-multitrack-audio"></i> md-multitrack-audio
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-network-cell"></i> md-network-cell
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-network-wifi"></i> md-network-wifi
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-nfc"></i> md-nfc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-now-wallpaper"></i> md-now-wallpaper
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-now-widgets"></i> md-now-widgets
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-screen-lock-landscape"></i> md-screen-lock-landscape
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-screen-lock-portrait"></i> md-screen-lock-portrait
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-screen-lock-rotation"></i> md-screen-lock-rotation
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-screen-rotation"></i> md-screen-rotation
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sd-storage"></i> md-sd-storage
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-settings-system-daydream"></i> md-settings-system-daydream
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-0-bar"></i> md-signal-cellular-0-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-1-bar"></i> md-signal-cellular-1-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-2-bar"></i> md-signal-cellular-2-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-3-bar"></i> md-signal-cellular-3-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-4-bar"></i> md-signal-cellular-4-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-connected-no-internet-0-bar"></i> md-signal-cellular-connected-no-internet-0-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-connected-no-internet-1-bar"></i> md-signal-cellular-connected-no-internet-1-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-connected-no-internet-2-bar"></i> md-signal-cellular-connected-no-internet-2-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-connected-no-internet-3-bar"></i> md-signal-cellular-connected-no-internet-3-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-connected-no-internet-4-bar"></i> md-signal-cellular-connected-no-internet-4-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-no-sim"></i> md-signal-cellular-no-sim
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-null"></i> md-signal-cellular-null
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-cellular-off"></i> md-signal-cellular-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-wifi-0-bar"></i> md-signal-wifi-0-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-wifi-1-bar"></i> md-signal-wifi-1-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-wifi-2-bar"></i> md-signal-wifi-2-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-wifi-3-bar"></i> md-signal-wifi-3-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-wifi-4-bar"></i> md-signal-wifi-4-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-signal-wifi-off"></i> md-signal-wifi-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-storage"></i> md-storage
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-usb"></i> md-usb
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wifi-lock"></i> md-wifi-lock
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wifi-tethering"></i> md-wifi-tethering
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END DEVICE -->
                    <!-- BEGIN EDITOR -->
                    <div class="card">
                        <div class="card-head"><header>Editor</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-attach-file"></i> md-attach-file
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-attach-money"></i> md-attach-money
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-all"></i> md-border-all
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-bottom"></i> md-border-bottom
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-clear"></i> md-border-clear
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-color"></i> md-border-color
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-horizontal"></i> md-border-horizontal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-inner"></i> md-border-inner
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-left"></i> md-border-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-outer"></i> md-border-outer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-right"></i> md-border-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-style"></i> md-border-style
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-top"></i> md-border-top
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-border-vertical"></i> md-border-vertical
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-align-center"></i> md-format-align-center
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-align-justify"></i> md-format-align-justify
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-align-left"></i> md-format-align-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-align-right"></i> md-format-align-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-bold"></i> md-format-bold
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-clear"></i> md-format-clear
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-color-fill"></i> md-format-color-fill
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-color-reset"></i> md-format-color-reset
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-color-text"></i> md-format-color-text
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-indent-decrease"></i> md-format-indent-decrease
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-indent-increase"></i> md-format-indent-increase
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-italic"></i> md-format-italic
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-line-spacing"></i> md-format-line-spacing
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-list-bulleted"></i> md-format-list-bulleted
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-list-numbered"></i> md-format-list-numbered
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-paint"></i> md-format-paint
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-quote"></i> md-format-quote
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-size"></i> md-format-size
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-strikethrough"></i> md-format-strikethrough
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-textdirection-l-to-r"></i> md-format-textdirection-l-to-r
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-textdirection-r-to-l"></i> md-format-textdirection-r-to-l
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-format-underline"></i> md-format-underline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-functions"></i> md-functions
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-insert-chart"></i> md-insert-chart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-insert-comment"></i> md-insert-comment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-insert-drive-file"></i> md-insert-drive-file
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-insert-emoticon"></i> md-insert-emoticon
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-insert-invitation"></i> md-insert-invitation
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-insert-link"></i> md-insert-link
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-insert-photo"></i> md-insert-photo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-merge-type"></i> md-merge-type
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mode-comment"></i> md-mode-comment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mode-edit"></i> md-mode-edit
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-publish"></i> md-publish
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-vertical-align-bottom"></i> md-vertical-align-bottom
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-vertical-align-center"></i> md-vertical-align-center
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-vertical-align-top"></i> md-vertical-align-top
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wrap-text"></i> md-wrap-text
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END EDITOR -->
                    <!-- BEGIN FILE -->
                    <div class="card">
                        <div class="card-head"><header>File</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-attachment"></i> md-attachment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cloud"></i> md-cloud
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cloud-circle"></i> md-cloud-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cloud-done"></i> md-cloud-done
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cloud-download"></i> md-cloud-download
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cloud-off"></i> md-cloud-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cloud-queue"></i> md-cloud-queue
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cloud-upload"></i> md-cloud-upload
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-file-download"></i> md-file-download
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-file-upload"></i> md-file-upload
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-folder"></i> md-folder
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-folder-open"></i> md-folder-open
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-folder-shared"></i> md-folder-shared
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END FILE -->
                    <!-- BEGIN HARDWARE -->
                    <div class="card">
                        <div class="card-head"><header>Hardware</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cast"></i> md-cast
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cast-connected"></i> md-cast-connected
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-computer"></i> md-computer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-desktop-mac"></i> md-desktop-mac
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-desktop-windows"></i> md-desktop-windows
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dock"></i> md-dock
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-gamepad"></i> md-gamepad
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-headset"></i> md-headset
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-headset-mic"></i> md-headset-mic
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard"></i> md-keyboard
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-alt"></i> md-keyboard-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-arrow-down"></i> md-keyboard-arrow-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-arrow-left"></i> md-keyboard-arrow-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-arrow-right"></i> md-keyboard-arrow-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-arrow-up"></i> md-keyboard-arrow-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-backspace"></i> md-keyboard-backspace
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-capslock"></i> md-keyboard-capslock
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-control"></i> md-keyboard-control
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-hide"></i> md-keyboard-hide
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-return"></i> md-keyboard-return
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-tab"></i> md-keyboard-tab
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-keyboard-voice"></i> md-keyboard-voice
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-laptop"></i> md-laptop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-laptop-chromebook"></i> md-laptop-chromebook
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-laptop-mac"></i> md-laptop-mac
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-laptop-windows"></i> md-laptop-windows
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-memory"></i> md-memory
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mouse"></i> md-mouse
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone-android"></i> md-phone-android
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone-iphone"></i> md-phone-iphone
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phonelink"></i> md-phonelink
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phonelink-off"></i> md-phonelink-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-security"></i> md-security
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sim-card"></i> md-sim-card
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-smartphone"></i> md-smartphone
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-speaker"></i> md-speaker
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tablet"></i> md-tablet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tablet-android"></i> md-tablet-android
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tablet-mac"></i> md-tablet-mac
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tv"></i> md-tv
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-watch"></i> md-watch
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END HARDWARE -->
                    <!-- BEGIN IMAGE -->
                    <div class="card">
                        <div class="card-head"><header>Image</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-add-to-photos"></i> md-add-to-photos
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-adjust"></i> md-adjust
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-assistant-photo"></i> md-assistant-photo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-audiotrack"></i> md-audiotrack
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-blur-circular"></i> md-blur-circular
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-blur-linear"></i> md-blur-linear
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-blur-off"></i> md-blur-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-blur-on"></i> md-blur-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-1"></i> md-brightness-1
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-2"></i> md-brightness-2
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-3"></i> md-brightness-3
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-4"></i> md-brightness-4
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-5"></i> md-brightness-5
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-6"></i> md-brightness-6
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brightness-7"></i> md-brightness-7
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-brush"></i> md-brush
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-camera"></i> md-camera
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-camera-alt"></i> md-camera-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-camera-front"></i> md-camera-front
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-camera-rear"></i> md-camera-rear
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-camera-roll"></i> md-camera-roll
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-center-focus-strong"></i> md-center-focus-strong
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-center-focus-weak"></i> md-center-focus-weak
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-collections"></i> md-collections
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-colorize"></i> md-colorize
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-color-lens"></i> md-color-lens
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-compare"></i> md-compare
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-control-point"></i> md-control-point
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-control-point-duplicate"></i> md-control-point-duplicate
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop"></i> md-crop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-3-2"></i> md-crop-3-2
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-5-4"></i> md-crop-5-4
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-7-5"></i> md-crop-7-5
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-16-9"></i> md-crop-16-9
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-din"></i> md-crop-din
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-free"></i> md-crop-free
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-landscape"></i> md-crop-landscape
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-original"></i> md-crop-original
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-portrait"></i> md-crop-portrait
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-crop-square"></i> md-crop-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dehaze"></i> md-dehaze
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-details"></i> md-details
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-edit"></i> md-edit
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-exposure"></i> md-exposure
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-exposure-minus-1"></i> md-exposure-minus-1
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-exposure-minus-2"></i> md-exposure-minus-2
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-exposure-zero"></i> md-exposure-zero
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-exposure-plus-1"></i> md-exposure-plus-1
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-exposure-plus-2"></i> md-exposure-plus-2
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter"></i> md-filter
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-1"></i> md-filter-1
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-2"></i> md-filter-2
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-3"></i> md-filter-3
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-4"></i> md-filter-4
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-5"></i> md-filter-5
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-6"></i> md-filter-6
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-7"></i> md-filter-7
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-8"></i> md-filter-8
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-9"></i> md-filter-9
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-9-plus"></i> md-filter-9-plus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-b-and-w"></i> md-filter-b-and-w
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-center-focus"></i> md-filter-center-focus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-drama"></i> md-filter-drama
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-frames"></i> md-filter-frames
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-hdr"></i> md-filter-hdr
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-none"></i> md-filter-none
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-tilt-shift"></i> md-filter-tilt-shift
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-filter-vintage"></i> md-filter-vintage
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flare"></i> md-flare
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flash-auto"></i> md-flash-auto
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flash-off"></i> md-flash-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flash-on"></i> md-flash-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flip"></i> md-flip
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-gradient"></i> md-gradient
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-grain"></i> md-grain
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-grid-off"></i> md-grid-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-grid-on"></i> md-grid-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-hdr-off"></i> md-hdr-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-hdr-on"></i> md-hdr-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-hdr-strong"></i> md-hdr-strong
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-hdr-weak"></i> md-hdr-weak
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-healing"></i> md-healing
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-image"></i> md-image
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-image-aspect-ratio"></i> md-image-aspect-ratio
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-iso"></i> md-iso
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-landscape"></i> md-landscape
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-leak-add"></i> md-leak-add
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-leak-remove"></i> md-leak-remove
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-lens"></i> md-lens
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-looks"></i> md-looks
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-looks-1"></i> md-looks-1
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-looks-2"></i> md-looks-2
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-looks-3"></i> md-looks-3
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-looks-4"></i> md-looks-4
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-looks-5"></i> md-looks-5
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-looks-6"></i> md-looks-6
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-loupe"></i> md-loupe
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-movie-creation"></i> md-movie-creation
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-nature"></i> md-nature
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-nature-people"></i> md-nature-people
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-navigate-before"></i> md-navigate-before
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-navigate-next"></i> md-navigate-next
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-palette"></i> md-palette
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-panorama"></i> md-panorama
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-panorama-fisheye"></i> md-panorama-fisheye
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-panorama-horizontal"></i> md-panorama-horizontal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-panorama-vertical"></i> md-panorama-vertical
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-panorama-wide-angle"></i> md-panorama-wide-angle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-photo"></i> md-photo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-photo-album"></i> md-photo-album
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-photo-camera"></i> md-photo-camera
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-photo-library"></i> md-photo-library
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-portrait"></i> md-portrait
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-remove-red-eye"></i> md-remove-red-eye
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-rotate-left"></i> md-rotate-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-rotate-right"></i> md-rotate-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-slideshow"></i> md-slideshow
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-straighten"></i> md-straighten
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-style"></i> md-style
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-switch-camera"></i> md-switch-camera
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-switch-video"></i> md-switch-video
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tag-faces"></i> md-tag-faces
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-texture"></i> md-texture
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-timelapse"></i> md-timelapse
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-timer"></i> md-timer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-timer-3"></i> md-timer-3
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-timer-10"></i> md-timer-10
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-timer-auto"></i> md-timer-auto
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-timer-off"></i> md-timer-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tonality"></i> md-tonality
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-transform"></i> md-transform
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tune"></i> md-tune
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wb-auto"></i> md-wb-auto
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wb-cloudy"></i> md-wb-cloudy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wb-incandescent"></i> md-wb-incandescent
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wb-irradescent"></i> md-wb-irradescent
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-wb-sunny"></i> md-wb-sunny
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END IMAGE -->
                    <!-- BEGIN MAPS -->
                    <div class="card">
                        <div class="card-head"><header>Maps</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-beenhere"></i> md-beenhere
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions"></i> md-directions
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions-bike"></i> md-directions-bike
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions-bus"></i> md-directions-bus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions-car"></i> md-directions-car
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions-ferry"></i> md-directions-ferry
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions-subway"></i> md-directions-subway
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions-train"></i> md-directions-train
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions-transit"></i> md-directions-transit
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-directions-walk"></i> md-directions-walk
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-flight"></i> md-flight
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-hotel"></i> md-hotel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-layers"></i> md-layers
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-layers-clear"></i> md-layers-clear
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-airport"></i> md-local-airport
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-atm"></i> md-local-atm
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-attraction"></i> md-local-attraction
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-bar"></i> md-local-bar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-cafe"></i> md-local-cafe
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-car-wash"></i> md-local-car-wash
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-convenience-store"></i> md-local-convenience-store
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-drink"></i> md-local-drink
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-florist"></i> md-local-florist
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-gas-station"></i> md-local-gas-station
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-grocery-store"></i> md-local-grocery-store
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-hospital"></i> md-local-hospital
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-hotel"></i> md-local-hotel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-laundry-service"></i> md-local-laundry-service
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-library"></i> md-local-library
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-mall"></i> md-local-mall
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-movies"></i> md-local-movies
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-offer"></i> md-local-offer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-parking"></i> md-local-parking
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-pharmacy"></i> md-local-pharmacy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-phone"></i> md-local-phone
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-pizza"></i> md-local-pizza
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-play"></i> md-local-play
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-post-office"></i> md-local-post-office
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-print-shop"></i> md-local-print-shop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-restaurant"></i> md-local-restaurant
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-see"></i> md-local-see
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-shipping"></i> md-local-shipping
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-local-taxi"></i> md-local-taxi
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-location-history"></i> md-location-history
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-map"></i> md-map
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-my-location"></i> md-my-location
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-navigation"></i> md-navigation
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-pin-drop"></i> md-pin-drop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-place"></i> md-place
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-rate-review"></i> md-rate-review
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-restaurant-menu"></i> md-restaurant-menu
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-satellite"></i> md-satellite
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-store-mall-directory"></i> md-store-mall-directory
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-terrain"></i> md-terrain
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-traffic"></i> md-traffic
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END MAPS -->
                    <!-- BEGIN NAVIGATION -->
                    <div class="card">
                        <div class="card-head"><header>Navigation</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-apps"></i> md-apps
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cancel"></i> md-cancel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-arrow-drop-down-circle"></i> md-arrow-drop-down-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-arrow-drop-down"></i> md-arrow-drop-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-arrow-drop-up"></i> md-arrow-drop-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-arrow-back"></i> md-arrow-back
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-arrow-forward"></i> md-arrow-forward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-check"></i> md-check
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-close"></i> md-close
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-chevron-left"></i> md-chevron-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-chevron-right"></i> md-chevron-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-expand-less"></i> md-expand-less
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-expand-more"></i> md-expand-more
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-fullscreen"></i> md-fullscreen
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-fullscreen-exit"></i> md-fullscreen-exit
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-menu"></i> md-menu
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-more-horiz"></i> md-more-horiz
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-more-vert"></i> md-more-vert
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-refresh"></i> md-refresh
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-unfold-less"></i> md-unfold-less
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-unfold-more"></i> md-unfold-more
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END NAVIGATION -->
                    <!-- BEGIN NOTIFICATION -->
                    <div class="card">
                        <div class="card-head"><header>Notification</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-adb"></i> md-adb
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-bluetooth-audio"></i> md-bluetooth-audio
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-disc-full"></i> md-disc-full
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-dnd-forwardslash"></i> md-dnd-forwardslash
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-do-not-disturb"></i> md-do-not-disturb
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-drive-eta"></i> md-drive-eta
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-event-available"></i> md-event-available
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-event-busy"></i> md-event-busy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-event-note"></i> md-event-note
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-folder-special"></i> md-folder-special
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mms"></i> md-mms
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-more"></i> md-more
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-network-locked"></i> md-network-locked
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone-bluetooth-speaker"></i> md-phone-bluetooth-speaker
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone-forwarded"></i> md-phone-forwarded
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone-in-talk"></i> md-phone-in-talk
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone-locked"></i> md-phone-locked
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone-missed"></i> md-phone-missed
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-phone-paused"></i> md-phone-paused
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-play-download"></i> md-play-download
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-play-install"></i> md-play-install
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sd-card"></i> md-sd-card
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sim-card-alert"></i> md-sim-card-alert
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sms"></i> md-sms
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sms-failed"></i> md-sms-failed
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sync"></i> md-sync
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sync-disabled"></i> md-sync-disabled
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-sync-problem"></i> md-sync-problem
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-system-update"></i> md-system-update
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-tap-and-play"></i> md-tap-and-play
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-time-to-leave"></i> md-time-to-leave
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-vibration"></i> md-vibration
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-voice-chat"></i> md-voice-chat
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-vpn-lock"></i> md-vpn-lock
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END NOTIFICATION -->
                    <!-- BEGIN SOCIAL -->
                    <div class="card">
                        <div class="card-head"><header>Social</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-cake"></i> md-cake
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-domain"></i> md-domain
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-location-city"></i> md-location-city
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-mood"></i> md-mood
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-notifications-none"></i> md-notifications-none
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-notifications"></i> md-notifications
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-notifications-off"></i> md-notifications-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-notifications-on"></i> md-notifications-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-notifications-paused"></i> md-notifications-paused
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-pages"></i> md-pages
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-party-mode"></i> md-party-mode
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-group"></i> md-group
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-group-add"></i> md-group-add
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-people"></i> md-people
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-people-outline"></i> md-people-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-person"></i> md-person
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-person-add"></i> md-person-add
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-person-outline"></i> md-person-outline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-plus-one"></i> md-plus-one
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-poll"></i> md-poll
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-public"></i> md-public
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-school"></i> md-school
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-share"></i> md-share
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-whatshot"></i> md-whatshot
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END SOCIAL -->
                    <!-- BEGIN TOGGLE -->
                    <div class="card">
                        <div class="card-head"><header>Toggle</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-check-box"></i> md-check-box
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-check-box-outline-blank"></i> md-check-box-outline-blank
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-radio-button-off"></i> md-radio-button-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-radio-button-on"></i> md-radio-button-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-star"></i> md-star
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-star-half"></i> md-star-half
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="md md-star-outline"></i> md-star-outline
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END TOGGLE -->
                </div><!--end .section-body -->
            </section>
        </div>
        <div class="tab-pane" id="FontAwesome">
            <section>
                <div class="section-body">

                    <!-- BEGIN INTRO -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-primary">Font Awesome</h1>
                        </div><!--end .col -->
                    </div><!--end .row -->
                    <!-- END INTRO -->
                    <!-- BEGIN 40 NEW ICONS IN 4.3 -->
                    <div class="card">
                        <div class="card-head"><header>40 New Icons in 4.3</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bed"></i> fa-bed
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-buysellads"></i> fa-buysellads
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cart-arrow-down"></i> fa-cart-arrow-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cart-plus"></i> fa-cart-plus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-connectdevelop"></i> fa-connectdevelop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dashcube"></i> fa-dashcube
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-diamond"></i> fa-diamond
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-facebook-official"></i> fa-facebook-official
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-forumbee"></i> fa-forumbee
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-heartbeat"></i> fa-heartbeat
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hotel"></i> fa-hotel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-leanpub"></i> fa-leanpub
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars"></i> fa-mars
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars-double"></i> fa-mars-double
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars-stroke"></i> fa-mars-stroke
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars-stroke-h"></i> fa-mars-stroke-h
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars-stroke-v"></i> fa-mars-stroke-v
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-medium"></i> fa-medium
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mercury"></i> fa-mercury
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-motorcycle"></i> fa-motorcycle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-neuter"></i> fa-neuter
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pinterest-p"></i> fa-pinterest-p
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sellsy"></i> fa-sellsy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-server"></i> fa-server
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ship"></i> fa-ship
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-shirtsinbulk"></i> fa-shirtsinbulk
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-simplybuilt"></i> fa-simplybuilt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-skyatlas"></i> fa-skyatlas
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-street-view"></i> fa-street-view
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-subway"></i> fa-subway
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-train"></i> fa-train
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-transgender"></i> fa-transgender
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-transgender-alt"></i> fa-transgender-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-user-plus"></i> fa-user-plus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-user-secret"></i> fa-user-secret
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-user-times"></i> fa-user-times
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-venus"></i> fa-venus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-venus-double"></i> fa-venus-double
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-venus-mars"></i> fa-venus-mars
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-viacoin"></i> fa-viacoin
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-whatsapp"></i> fa-whatsapp
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END 40 NEW ICONS IN 4.3 -->
                    <!-- BEGIN WEB APPLICATION ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Web Application Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-adjust"></i> fa-adjust
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-anchor"></i> fa-anchor
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-archive"></i> fa-archive
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-area-chart"></i> fa-area-chart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrows"></i> fa-arrows
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrows-h"></i> fa-arrows-h
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrows-v"></i> fa-arrows-v
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-asterisk"></i> fa-asterisk
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-at"></i> fa-at
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-automobile"></i> fa-automobile
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ban"></i> fa-ban
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bank"></i> fa-bank
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bar-chart"></i> fa-bar-chart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bar-chart-o"></i> fa-bar-chart-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-barcode"></i> fa-barcode
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bars"></i> fa-bars
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bed"></i> fa-bed
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-beer"></i> fa-beer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bell"></i> fa-bell
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bell-o"></i> fa-bell-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bell-slash"></i> fa-bell-slash
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bell-slash-o"></i> fa-bell-slash-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bicycle"></i> fa-bicycle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-binoculars"></i> fa-binoculars
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-birthday-cake"></i> fa-birthday-cake
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bolt"></i> fa-bolt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bomb"></i> fa-bomb
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-book"></i> fa-book
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bookmark"></i> fa-bookmark
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bookmark-o"></i> fa-bookmark-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-briefcase"></i> fa-briefcase
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bug"></i> fa-bug
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-building"></i> fa-building
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-building-o"></i> fa-building-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bullhorn"></i> fa-bullhorn
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bullseye"></i> fa-bullseye
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bus"></i> fa-bus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cab"></i> fa-cab
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-calculator"></i> fa-calculator
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-calendar"></i> fa-calendar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-calendar-o"></i> fa-calendar-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-camera"></i> fa-camera
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-camera-retro"></i> fa-camera-retro
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-car"></i> fa-car
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-square-o-down"></i> fa-caret-square-o-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-square-o-left"></i> fa-caret-square-o-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-square-o-right"></i> fa-caret-square-o-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-square-o-up"></i> fa-caret-square-o-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cart-arrow-down"></i> fa-cart-arrow-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cart-plus"></i> fa-cart-plus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc"></i> fa-cc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-certificate"></i> fa-certificate
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-check"></i> fa-check
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-check-circle"></i> fa-check-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-check-circle-o"></i> fa-check-circle-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-check-square"></i> fa-check-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-check-square-o"></i> fa-check-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-child"></i> fa-child
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-circle"></i> fa-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-circle-o"></i> fa-circle-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-circle-o-notch"></i> fa-circle-o-notch
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-circle-thin"></i> fa-circle-thin
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-clock-o"></i> fa-clock-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-close"></i> fa-close
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cloud"></i> fa-cloud
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cloud-download"></i> fa-cloud-download
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cloud-upload"></i> fa-cloud-upload
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-code"></i> fa-code
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-code-fork"></i> fa-code-fork
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-coffee"></i> fa-coffee
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cog"></i> fa-cog
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cogs"></i> fa-cogs
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-comment"></i> fa-comment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-comment-o"></i> fa-comment-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-comments"></i> fa-comments
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-comments-o"></i> fa-comments-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-compass"></i> fa-compass
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-copyright"></i> fa-copyright
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-credit-card"></i> fa-credit-card
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-crop"></i> fa-crop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-crosshairs"></i> fa-crosshairs
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cube"></i> fa-cube
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cubes"></i> fa-cubes
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cutlery"></i> fa-cutlery
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dashboard"></i> fa-dashboard
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-database"></i> fa-database
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-desktop"></i> fa-desktop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-diamond"></i> fa-diamond
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dot-circle-o"></i> fa-dot-circle-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-download"></i> fa-download
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-edit"></i> fa-edit
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ellipsis-h"></i> fa-ellipsis-h
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ellipsis-v"></i> fa-ellipsis-v
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-envelope"></i> fa-envelope
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-envelope-o"></i> fa-envelope-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-envelope-square"></i> fa-envelope-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-eraser"></i> fa-eraser
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-exchange"></i> fa-exchange
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-exclamation"></i> fa-exclamation
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-exclamation-circle"></i> fa-exclamation-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-exclamation-triangle"></i> fa-exclamation-triangle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-external-link"></i> fa-external-link
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-external-link-square"></i> fa-external-link-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-eye"></i> fa-eye
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-eye-slash"></i> fa-eye-slash
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-eyedropper"></i> fa-eyedropper
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-fax"></i> fa-fax
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-female"></i> fa-female
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-fighter-jet"></i> fa-fighter-jet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-archive-o"></i> fa-file-archive-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-audio-o"></i> fa-file-audio-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-code-o"></i> fa-file-code-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-excel-o"></i> fa-file-excel-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-image-o"></i> fa-file-image-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-movie-o"></i> fa-file-movie-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-pdf-o"></i> fa-file-pdf-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-photo-o"></i> fa-file-photo-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-picture-o"></i> fa-file-picture-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-powerpoint-o"></i> fa-file-powerpoint-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-sound-o"></i> fa-file-sound-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-video-o"></i> fa-file-video-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-word-o"></i> fa-file-word-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-zip-o"></i> fa-file-zip-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-film"></i> fa-film
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-filter"></i> fa-filter
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-fire"></i> fa-fire
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-fire-extinguisher"></i> fa-fire-extinguisher
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-flag"></i> fa-flag
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-flag-checkered"></i> fa-flag-checkered
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-flag-o"></i> fa-flag-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-flash"></i> fa-flash
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-flask"></i> fa-flask
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-folder"></i> fa-folder
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-folder-o"></i> fa-folder-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-folder-open"></i> fa-folder-open
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-folder-open-o"></i> fa-folder-open-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-frown-o"></i> fa-frown-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-futbol-o"></i> fa-futbol-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gamepad"></i> fa-gamepad
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gavel"></i> fa-gavel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gear"></i> fa-gear
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gears"></i> fa-gears
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-genderless"></i> fa-genderless
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gift"></i> fa-gift
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-glass"></i> fa-glass
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-globe"></i> fa-globe
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-graduation-cap"></i> fa-graduation-cap
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-group"></i> fa-group
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hdd-o"></i> fa-hdd-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-headphones"></i> fa-headphones
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-heart"></i> fa-heart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-heart-o"></i> fa-heart-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-heartbeat"></i> fa-heartbeat
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-history"></i> fa-history
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-home"></i> fa-home
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hotel"></i> fa-hotel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-image"></i> fa-image
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-inbox"></i> fa-inbox
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-info"></i> fa-info
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-info-circle"></i> fa-info-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-institution"></i> fa-institution
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-key"></i> fa-key
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-keyboard-o"></i> fa-keyboard-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-language"></i> fa-language
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-laptop"></i> fa-laptop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-leaf"></i> fa-leaf
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-legal"></i> fa-legal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-lemon-o"></i> fa-lemon-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-level-down"></i> fa-level-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-level-up"></i> fa-level-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-life-bouy"></i> fa-life-bouy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-life-buoy"></i> fa-life-buoy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-life-ring"></i> fa-life-ring
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-life-saver"></i> fa-life-saver
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-lightbulb-o"></i> fa-lightbulb-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-line-chart"></i> fa-line-chart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-location-arrow"></i> fa-location-arrow
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-lock"></i> fa-lock
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-magic"></i> fa-magic
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-magnet"></i> fa-magnet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mail-forward"></i> fa-mail-forward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mail-reply"></i> fa-mail-reply
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mail-reply-all"></i> fa-mail-reply-all
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-male"></i> fa-male
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-map-marker"></i> fa-map-marker
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-meh-o"></i> fa-meh-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-microphone"></i> fa-microphone
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-microphone-slash"></i> fa-microphone-slash
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-minus"></i> fa-minus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-minus-circle"></i> fa-minus-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-minus-square"></i> fa-minus-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-minus-square-o"></i> fa-minus-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mobile"></i> fa-mobile
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mobile-phone"></i> fa-mobile-phone
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-money"></i> fa-money
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-moon-o"></i> fa-moon-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mortar-board"></i> fa-mortar-board
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-motorcycle"></i> fa-motorcycle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-music"></i> fa-music
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-navicon"></i> fa-navicon
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-newspaper-o"></i> fa-newspaper-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paint-brush"></i> fa-paint-brush
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paper-plane"></i> fa-paper-plane
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paper-plane-o"></i> fa-paper-plane-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paw"></i> fa-paw
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pencil"></i> fa-pencil
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pencil-square"></i> fa-pencil-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pencil-square-o"></i> fa-pencil-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-phone"></i> fa-phone
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-phone-square"></i> fa-phone-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-photo"></i> fa-photo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-picture-o"></i> fa-picture-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pie-chart"></i> fa-pie-chart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plane"></i> fa-plane
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plug"></i> fa-plug
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plus"></i> fa-plus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plus-circle"></i> fa-plus-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plus-square"></i> fa-plus-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plus-square-o"></i> fa-plus-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-power-off"></i> fa-power-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-print"></i> fa-print
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-puzzle-piece"></i> fa-puzzle-piece
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-qrcode"></i> fa-qrcode
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-question"></i> fa-question
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-question-circle"></i> fa-question-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-quote-left"></i> fa-quote-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-quote-right"></i> fa-quote-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-random"></i> fa-random
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-recycle"></i> fa-recycle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-refresh"></i> fa-refresh
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-remove"></i> fa-remove
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-reorder"></i> fa-reorder
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-reply"></i> fa-reply
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-reply-all"></i> fa-reply-all
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-retweet"></i> fa-retweet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-road"></i> fa-road
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rocket"></i> fa-rocket
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rss"></i> fa-rss
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rss-square"></i> fa-rss-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-search"></i> fa-search
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-search-minus"></i> fa-search-minus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-search-plus"></i> fa-search-plus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-send"></i> fa-send
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-send-o"></i> fa-send-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-server"></i> fa-server
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-share"></i> fa-share
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-share-alt"></i> fa-share-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-share-alt-square"></i> fa-share-alt-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-share-square"></i> fa-share-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-share-square-o"></i> fa-share-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-shield"></i> fa-shield
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ship"></i> fa-ship
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-shopping-cart"></i> fa-shopping-cart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sign-in"></i> fa-sign-in
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sign-out"></i> fa-sign-out
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-signal"></i> fa-signal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sitemap"></i> fa-sitemap
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sliders"></i> fa-sliders
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-smile-o"></i> fa-smile-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-soccer-ball-o"></i> fa-soccer-ball-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort"></i> fa-sort
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-alpha-asc"></i> fa-sort-alpha-asc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-alpha-desc"></i> fa-sort-alpha-desc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-amount-asc"></i> fa-sort-amount-asc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-amount-desc"></i> fa-sort-amount-desc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-asc"></i> fa-sort-asc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-desc"></i> fa-sort-desc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-down"></i> fa-sort-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-numeric-asc"></i> fa-sort-numeric-asc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-numeric-desc"></i> fa-sort-numeric-desc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sort-up"></i> fa-sort-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-space-shuttle"></i> fa-space-shuttle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-spinner"></i> fa-spinner
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-spoon"></i> fa-spoon
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-square"></i> fa-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-square-o"></i> fa-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-star"></i> fa-star
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-star-half"></i> fa-star-half
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-star-half-empty"></i> fa-star-half-empty
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-star-half-full"></i> fa-star-half-full
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-star-half-o"></i> fa-star-half-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-star-o"></i> fa-star-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-street-view"></i> fa-street-view
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-suitcase"></i> fa-suitcase
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sun-o"></i> fa-sun-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-support"></i> fa-support
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tablet"></i> fa-tablet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tachometer"></i> fa-tachometer
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tag"></i> fa-tag
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tags"></i> fa-tags
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tasks"></i> fa-tasks
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-taxi"></i> fa-taxi
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-terminal"></i> fa-terminal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-thumb-tack"></i> fa-thumb-tack
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-thumbs-down"></i> fa-thumbs-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-thumbs-o-down"></i> fa-thumbs-o-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-thumbs-o-up"></i> fa-thumbs-o-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-thumbs-up"></i> fa-thumbs-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ticket"></i> fa-ticket
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-times"></i> fa-times
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-times-circle"></i> fa-times-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-times-circle-o"></i> fa-times-circle-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tint"></i> fa-tint
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-down"></i> fa-toggle-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-left"></i> fa-toggle-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-off"></i> fa-toggle-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-on"></i> fa-toggle-on
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-right"></i> fa-toggle-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-up"></i> fa-toggle-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-trash"></i> fa-trash
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-trash-o"></i> fa-trash-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tree"></i> fa-tree
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-trophy"></i> fa-trophy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-truck"></i> fa-truck
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tty"></i> fa-tty
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-umbrella"></i> fa-umbrella
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-university"></i> fa-university
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-unlock"></i> fa-unlock
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-unlock-alt"></i> fa-unlock-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-unsorted"></i> fa-unsorted
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-upload"></i> fa-upload
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-user"></i> fa-user
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-user-plus"></i> fa-user-plus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-user-secret"></i> fa-user-secret
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-user-times"></i> fa-user-times
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-users"></i> fa-users
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-video-camera"></i> fa-video-camera
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-volume-down"></i> fa-volume-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-volume-off"></i> fa-volume-off
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-volume-up"></i> fa-volume-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-warning"></i> fa-warning
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-wheelchair"></i> fa-wheelchair
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-wifi"></i> fa-wifi
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-wrench"></i> fa-wrench
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END WEB APPLICATION ICONS -->
                    <!-- BEGIN TRANSPORTATION ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Transportation Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ambulance"></i> fa-ambulance
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-automobile"></i> fa-automobile
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bicycle"></i> fa-bicycle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bus"></i> fa-bus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cab"></i> fa-cab
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-car"></i> fa-car
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-fighter-jet"></i> fa-fighter-jet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-motorcycle"></i> fa-motorcycle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plane"></i> fa-plane
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rocket"></i> fa-rocket
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ship"></i> fa-ship
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-space-shuttle"></i> fa-space-shuttle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-subway"></i> fa-subway
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-taxi"></i> fa-taxi
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-train"></i> fa-train
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-truck"></i> fa-truck
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-wheelchair"></i> fa-wheelchair
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END TRANSPORTATION ICONS -->
                    <!-- BEGIN GENDER ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Gender Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-circle-thin"></i> fa-circle-thin
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-genderless"></i> fa-genderless
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars"></i> fa-mars
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars-double"></i> fa-mars-double
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars-stroke"></i> fa-mars-stroke
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars-stroke-h"></i> fa-mars-stroke-h
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mars-stroke-v"></i> fa-mars-stroke-v
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-mercury"></i> fa-mercury
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-neuter"></i> fa-neuter
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-transgender"></i> fa-transgender
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-transgender-alt"></i> fa-transgender-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-venus"></i> fa-venus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-venus-double"></i> fa-venus-double
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-venus-mars"></i> fa-venus-mars
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END GENDER ICONS -->
                    <!-- BEGIN FILE TYPE ICONS -->
                    <div class="card">
                        <div class="card-head"><header>File Type Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file"></i> fa-file
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-archive-o"></i> fa-file-archive-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-audio-o"></i> fa-file-audio-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-code-o"></i> fa-file-code-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-excel-o"></i> fa-file-excel-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-image-o"></i> fa-file-image-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-movie-o"></i> fa-file-movie-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-o"></i> fa-file-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-pdf-o"></i> fa-file-pdf-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-photo-o"></i> fa-file-photo-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-picture-o"></i> fa-file-picture-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-powerpoint-o"></i> fa-file-powerpoint-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-sound-o"></i> fa-file-sound-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-text"></i> fa-file-text
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-text-o"></i> fa-file-text-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-video-o"></i> fa-file-video-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-word-o"></i> fa-file-word-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-zip-o"></i> fa-file-zip-o
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END FILE TYPE ICONS -->
                    <!-- BEGIN SPINNER ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Spinner Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-circle-o-notch"></i> fa-circle-o-notch
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cog"></i> fa-cog
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gear"></i> fa-gear
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-refresh"></i> fa-refresh
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-spinner"></i> fa-spinner
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END SPINNER ICONS -->
                    <!-- BEGIN FORM CONTROL ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Form Control Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-check-square"></i> fa-check-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-check-square-o"></i> fa-check-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-circle"></i> fa-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-circle-o"></i> fa-circle-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dot-circle-o"></i> fa-dot-circle-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-minus-square"></i> fa-minus-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-minus-square-o"></i> fa-minus-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plus-square"></i> fa-plus-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plus-square-o"></i> fa-plus-square-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-square"></i> fa-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-square-o"></i> fa-square-o
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END FORM CONTROL ICONS -->
                    <!-- BEGIN PAYMENT ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Payment Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-amex"></i> fa-cc-amex
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-discover"></i> fa-cc-discover
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-mastercard"></i> fa-cc-mastercard
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-paypal"></i> fa-cc-paypal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-stripe"></i> fa-cc-stripe
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-visa"></i> fa-cc-visa
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-credit-card"></i> fa-credit-card
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-google-wallet"></i> fa-google-wallet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paypal"></i> fa-paypal
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END PAYMENT ICONS -->
                    <!-- BEGIN CHART ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Chart Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-area-chart"></i> fa-area-chart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bar-chart"></i> fa-bar-chart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bar-chart-o"></i> fa-bar-chart-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-line-chart"></i> fa-line-chart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pie-chart"></i> fa-pie-chart
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END CHART ICONS -->
                    <!-- BEGIN CURRENCY ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Currency Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bitcoin"></i> fa-bitcoin
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-btc"></i> fa-btc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cny"></i> fa-cny
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dollar"></i> fa-dollar
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-eur"></i> fa-eur
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-euro"></i> fa-euro
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gbp"></i> fa-gbp
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ils"></i> fa-ils
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-inr"></i> fa-inr
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-jpy"></i> fa-jpy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-krw"></i> fa-krw
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-money"></i> fa-money
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rmb"></i> fa-rmb
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rouble"></i> fa-rouble
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rub"></i> fa-rub
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ruble"></i> fa-ruble
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rupee"></i> fa-rupee
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-shekel"></i> fa-shekel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sheqel"></i> fa-sheqel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-try"></i> fa-try
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-turkish-lira"></i> fa-turkish-lira
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-usd"></i> fa-usd
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-won"></i> fa-won
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-yen"></i> fa-yen
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END CURRENCY ICONS -->
                    <!-- BEGIN TEXT EDITOR ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Text Editor Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-align-center"></i> fa-align-center
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-align-justify"></i> fa-align-justify
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-align-left"></i> fa-align-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-align-right"></i> fa-align-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bold"></i> fa-bold
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chain"></i> fa-chain
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chain-broken"></i> fa-chain-broken
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-clipboard"></i> fa-clipboard
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-columns"></i> fa-columns
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-copy"></i> fa-copy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cut"></i> fa-cut
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dedent"></i> fa-dedent
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-eraser"></i> fa-eraser
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file"></i> fa-file
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-o"></i> fa-file-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-text"></i> fa-file-text
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-file-text-o"></i> fa-file-text-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-files-o"></i> fa-files-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-floppy-o"></i> fa-floppy-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-font"></i> fa-font
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-header"></i> fa-header
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-indent"></i> fa-indent
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-italic"></i> fa-italic
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-link"></i> fa-link
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-list"></i> fa-list
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-list-alt"></i> fa-list-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-list-ol"></i> fa-list-ol
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-list-ul"></i> fa-list-ul
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-outdent"></i> fa-outdent
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paperclip"></i> fa-paperclip
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paragraph"></i> fa-paragraph
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paste"></i> fa-paste
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-repeat"></i> fa-repeat
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rotate-left"></i> fa-rotate-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rotate-right"></i> fa-rotate-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-save"></i> fa-save
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-scissors"></i> fa-scissors
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-strikethrough"></i> fa-strikethrough
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-subscript"></i> fa-subscript
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-superscript"></i> fa-superscript
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-table"></i> fa-table
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-text-height"></i> fa-text-height
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-text-width"></i> fa-text-width
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-th"></i> fa-th
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-th-large"></i> fa-th-large
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-th-list"></i> fa-th-list
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-underline"></i> fa-underline
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-undo"></i> fa-undo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-unlink"></i> fa-unlink
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END TEXT EDITOR ICONS -->
                    <!-- BEGIN DIRECTIONAL ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Directional Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angle-double-down"></i> fa-angle-double-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angle-double-left"></i> fa-angle-double-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angle-double-right"></i> fa-angle-double-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angle-double-up"></i> fa-angle-double-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angle-down"></i> fa-angle-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angle-left"></i> fa-angle-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angle-right"></i> fa-angle-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angle-up"></i> fa-angle-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-circle-down"></i> fa-arrow-circle-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-circle-left"></i> fa-arrow-circle-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-circle-o-down"></i> fa-arrow-circle-o-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-circle-o-left"></i> fa-arrow-circle-o-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-circle-o-right"></i> fa-arrow-circle-o-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-circle-o-up"></i> fa-arrow-circle-o-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-circle-right"></i> fa-arrow-circle-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-circle-up"></i> fa-arrow-circle-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-down"></i> fa-arrow-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-left"></i> fa-arrow-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-right"></i> fa-arrow-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrow-up"></i> fa-arrow-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrows"></i> fa-arrows
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrows-alt"></i> fa-arrows-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrows-h"></i> fa-arrows-h
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrows-v"></i> fa-arrows-v
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-down"></i> fa-caret-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-left"></i> fa-caret-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-right"></i> fa-caret-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-square-o-down"></i> fa-caret-square-o-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-square-o-left"></i> fa-caret-square-o-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-square-o-right"></i> fa-caret-square-o-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-square-o-up"></i> fa-caret-square-o-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-caret-up"></i> fa-caret-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chevron-circle-down"></i> fa-chevron-circle-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chevron-circle-left"></i> fa-chevron-circle-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chevron-circle-right"></i> fa-chevron-circle-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chevron-circle-up"></i> fa-chevron-circle-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chevron-down"></i> fa-chevron-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chevron-left"></i> fa-chevron-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chevron-right"></i> fa-chevron-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-chevron-up"></i> fa-chevron-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hand-o-down"></i> fa-hand-o-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hand-o-left"></i> fa-hand-o-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hand-o-right"></i> fa-hand-o-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hand-o-up"></i> fa-hand-o-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-long-arrow-down"></i> fa-long-arrow-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-long-arrow-left"></i> fa-long-arrow-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-long-arrow-right"></i> fa-long-arrow-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-long-arrow-up"></i> fa-long-arrow-up
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-down"></i> fa-toggle-down
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-left"></i> fa-toggle-left
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-right"></i> fa-toggle-right
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-toggle-up"></i> fa-toggle-up
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END DIRECTIONAL ICONS -->
                    <!-- BEGIN VIDEO PLAYER ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Video Player Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-arrows-alt"></i> fa-arrows-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-backward"></i> fa-backward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-compress"></i> fa-compress
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-eject"></i> fa-eject
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-expand"></i> fa-expand
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-fast-backward"></i> fa-fast-backward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-fast-forward"></i> fa-fast-forward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-forward"></i> fa-forward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pause"></i> fa-pause
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-play"></i> fa-play
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-play-circle"></i> fa-play-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-play-circle-o"></i> fa-play-circle-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-step-backward"></i> fa-step-backward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-step-forward"></i> fa-step-forward
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-stop"></i> fa-stop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-youtube-play"></i> fa-youtube-play
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END VIDEO PLAYER ICONS -->
                    <!-- BEGIN BRAND ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Brand Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-adn"></i> fa-adn
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-android"></i> fa-android
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-angellist"></i> fa-angellist
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-apple"></i> fa-apple
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-behance"></i> fa-behance
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-behance-square"></i> fa-behance-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bitbucket"></i> fa-bitbucket
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bitbucket-square"></i> fa-bitbucket-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-bitcoin"></i> fa-bitcoin
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-btc"></i> fa-btc
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-buysellads"></i> fa-buysellads
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-amex"></i> fa-cc-amex
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-discover"></i> fa-cc-discover
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-mastercard"></i> fa-cc-mastercard
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-paypal"></i> fa-cc-paypal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-stripe"></i> fa-cc-stripe
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-cc-visa"></i> fa-cc-visa
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-codepen"></i> fa-codepen
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-connectdevelop"></i> fa-connectdevelop
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-css3"></i> fa-css3
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dashcube"></i> fa-dashcube
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-delicious"></i> fa-delicious
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-deviantart"></i> fa-deviantart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-digg"></i> fa-digg
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dribbble"></i> fa-dribbble
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-dropbox"></i> fa-dropbox
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-drupal"></i> fa-drupal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-empire"></i> fa-empire
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-facebook"></i> fa-facebook
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-facebook-f"></i> fa-facebook-f
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-facebook-official"></i> fa-facebook-official
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-facebook-square"></i> fa-facebook-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-flickr"></i> fa-flickr
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-forumbee"></i> fa-forumbee
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-foursquare"></i> fa-foursquare
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ge"></i> fa-ge
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-git"></i> fa-git
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-git-square"></i> fa-git-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-github"></i> fa-github
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-github-alt"></i> fa-github-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-github-square"></i> fa-github-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gittip"></i> fa-gittip
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-google"></i> fa-google
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-google-plus"></i> fa-google-plus
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-google-plus-square"></i> fa-google-plus-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-google-wallet"></i> fa-google-wallet
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-gratipay"></i> fa-gratipay
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hacker-news"></i> fa-hacker-news
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-html5"></i> fa-html5
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-instagram"></i> fa-instagram
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ioxhost"></i> fa-ioxhost
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-joomla"></i> fa-joomla
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-jsfiddle"></i> fa-jsfiddle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-lastfm"></i> fa-lastfm
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-lastfm-square"></i> fa-lastfm-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-leanpub"></i> fa-leanpub
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-linkedin"></i> fa-linkedin
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-linkedin-square"></i> fa-linkedin-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-linux"></i> fa-linux
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-maxcdn"></i> fa-maxcdn
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-meanpath"></i> fa-meanpath
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-medium"></i> fa-medium
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-openid"></i> fa-openid
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pagelines"></i> fa-pagelines
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-paypal"></i> fa-paypal
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pied-piper"></i> fa-pied-piper
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pied-piper-alt"></i> fa-pied-piper-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pinterest"></i> fa-pinterest
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pinterest-p"></i> fa-pinterest-p
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-pinterest-square"></i> fa-pinterest-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-qq"></i> fa-qq
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ra"></i> fa-ra
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-rebel"></i> fa-rebel
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-reddit"></i> fa-reddit
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-reddit-square"></i> fa-reddit-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-renren"></i> fa-renren
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-sellsy"></i> fa-sellsy
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-share-alt"></i> fa-share-alt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-share-alt-square"></i> fa-share-alt-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-shirtsinbulk"></i> fa-shirtsinbulk
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-simplybuilt"></i> fa-simplybuilt
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-skyatlas"></i> fa-skyatlas
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-skype"></i> fa-skype
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-slack"></i> fa-slack
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-slideshare"></i> fa-slideshare
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-soundcloud"></i> fa-soundcloud
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-spotify"></i> fa-spotify
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-stack-exchange"></i> fa-stack-exchange
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-stack-overflow"></i> fa-stack-overflow
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-steam"></i> fa-steam
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-steam-square"></i> fa-steam-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-stumbleupon"></i> fa-stumbleupon
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-stumbleupon-circle"></i> fa-stumbleupon-circle
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tencent-weibo"></i> fa-tencent-weibo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-trello"></i> fa-trello
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tumblr"></i> fa-tumblr
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-tumblr-square"></i> fa-tumblr-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-twitch"></i> fa-twitch
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-twitter"></i> fa-twitter
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-twitter-square"></i> fa-twitter-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-viacoin"></i> fa-viacoin
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-vimeo-square"></i> fa-vimeo-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-vine"></i> fa-vine
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-vk"></i> fa-vk
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-wechat"></i> fa-wechat
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-weibo"></i> fa-weibo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-weixin"></i> fa-weixin
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-whatsapp"></i> fa-whatsapp
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-windows"></i> fa-windows
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-wordpress"></i> fa-wordpress
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-xing"></i> fa-xing
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-xing-square"></i> fa-xing-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-yahoo"></i> fa-yahoo
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-yelp"></i> fa-yelp
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-youtube"></i> fa-youtube
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-youtube-play"></i> fa-youtube-play
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-youtube-square"></i> fa-youtube-square
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END BRAND ICONS -->
                    <!-- BEGIN MEDICAL ICONS -->
                    <div class="card">
                        <div class="card-head"><header>Medical Icons</header></div>
                        <div class="card-body">
                            <div class="row demo-icon-list">
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-ambulance"></i> fa-ambulance
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-h-square"></i> fa-h-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-heart"></i> fa-heart
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-heart-o"></i> fa-heart-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-heartbeat"></i> fa-heartbeat
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-hospital-o"></i> fa-hospital-o
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-medkit"></i> fa-medkit
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-plus-square"></i> fa-plus-square
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-stethoscope"></i> fa-stethoscope
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-user-md"></i> fa-user-md
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="demo-icon-hover">
                                        <i class="fa fa-wheelchair"></i> fa-wheelchair
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    <!-- END MEDICAL ICONS -->


                </div><!--end .section-body -->
            </section>
        </div>
        <div id="Icomoon" class="tab-pane">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h4 class="text-center padded">
                        <strong>IcoMoon</strong>
                    </h4>
                </div>
            </div>

            <ul class="list-inline icon-demo-list">
                <li>
                    <i class="icm icm-home"></i>
                    <span>icm-home</span>
                </li>
                <li>
                    <i class="icm icm-home2"></i>
                    <span>icm-home2</span>
                </li>
                <li>
                    <i class="icm icm-home3"></i>
                    <span>icm-home3</span>
                </li>
                <li>
                    <i class="icm icm-home4"></i>
                    <span>icm-home4</span>
                </li>
                <li>
                    <i class="icm icm-home5"></i>
                    <span>icm-home5</span>
                </li>
                <li>
                    <i class="icm icm-home6"></i>
                    <span>icm-home6</span>
                </li>
                <li>
                    <i class="icm icm-home7"></i>
                    <span>icm-home7</span>
                </li>
                <li>
                    <i class="icm icm-home8"></i>
                    <span>icm-home8</span>
                </li>
                <li>
                    <i class="icm icm-home9"></i>
                    <span>icm-home9</span>
                </li>
                <li>
                    <i class="icm icm-home10"></i>
                    <span>icm-home10</span>
                </li>
                <li>
                    <i class="icm icm-home11"></i>
                    <span>icm-home11</span>
                </li>
                <li>
                    <i class="icm icm-office"></i>
                    <span>icm-office</span>
                </li>
                <li>
                    <i class="icm icm-newspaper"></i>
                    <span>icm-newspaper</span>
                </li>
                <li>
                    <i class="icm icm-pencil"></i>
                    <span>icm-pencil</span>
                </li>
                <li>
                    <i class="icm icm-pencil2"></i>
                    <span>icm-pencil2</span>
                </li>
                <li>
                    <i class="icm icm-pencil3"></i>
                    <span>icm-pencil3</span>
                </li>
                <li>
                    <i class="icm icm-pencil4"></i>
                    <span>icm-pencil4</span>
                </li>
                <li>
                    <i class="icm icm-pencil5"></i>
                    <span>icm-pencil5</span>
                </li>
                <li>
                    <i class="icm icm-pencil6"></i>
                    <span>icm-pencil6</span>
                </li>
                <li>
                    <i class="icm icm-quill"></i>
                    <span>icm-quill</span>
                </li>
                <li>
                    <i class="icm icm-quill2"></i>
                    <span>icm-quill2</span>
                </li>
                <li>
                    <i class="icm icm-quill3"></i>
                    <span>icm-quill3</span>
                </li>
                <li>
                    <i class="icm icm-pen"></i>
                    <span>icm-pen</span>
                </li>
                <li>
                    <i class="icm icm-pen2"></i>
                    <span>icm-pen2</span>
                </li>
                <li>
                    <i class="icm icm-pen3"></i>
                    <span>icm-pen3</span>
                </li>
                <li>
                    <i class="icm icm-pen4"></i>
                    <span>icm-pen4</span>
                </li>
                <li>
                    <i class="icm icm-pen5"></i>
                    <span>icm-pen5</span>
                </li>
                <li>
                    <i class="icm icm-marker"></i>
                    <span>icm-marker</span>
                </li>
                <li>
                    <i class="icm icm-home12"></i>
                    <span>icm-home12</span>
                </li>
                <li>
                    <i class="icm icm-marker2"></i>
                    <span>icm-marker2</span>
                </li>
                <li>
                    <i class="icm icm-blog"></i>
                    <span>icm-blog</span>
                </li>
                <li>
                    <i class="icm icm-blog2"></i>
                    <span>icm-blog2</span>
                </li>
                <li>
                    <i class="icm icm-brush"></i>
                    <span>icm-brush</span>
                </li>
                <li>
                    <i class="icm icm-palette"></i>
                    <span>icm-palette</span>
                </li>
                <li>
                    <i class="icm icm-palette2"></i>
                    <span>icm-palette2</span>
                </li>
                <li>
                    <i class="icm icm-eyedropper"></i>
                    <span>icm-eyedropper</span>
                </li>
                <li>
                    <i class="icm icm-eyedropper2"></i>
                    <span>icm-eyedropper2</span>
                </li>
                <li>
                    <i class="icm icm-droplet"></i>
                    <span>icm-droplet</span>
                </li>
                <li>
                    <i class="icm icm-droplet2"></i>
                    <span>icm-droplet2</span>
                </li>
                <li>
                    <i class="icm icm-droplet3"></i>
                    <span>icm-droplet3</span>
                </li>
                <li>
                    <i class="icm icm-droplet4"></i>
                    <span>icm-droplet4</span>
                </li>
                <li>
                    <i class="icm icm-paint-format"></i>
                    <span>icm-paint-format</span>
                </li>
                <li>
                    <i class="icm icm-paint-format2"></i>
                    <span>icm-paint-format2</span>
                </li>
                <li>
                    <i class="icm icm-image"></i>
                    <span>icm-image</span>
                </li>
                <li>
                    <i class="icm icm-image2"></i>
                    <span>icm-image2</span>
                </li>
                <li>
                    <i class="icm icm-image3"></i>
                    <span>icm-image3</span>
                </li>
                <li>
                    <i class="icm icm-images"></i>
                    <span>icm-images</span>
                </li>
                <li>
                    <i class="icm icm-image4"></i>
                    <span>icm-image4</span>
                </li>
                <li>
                    <i class="icm icm-image5"></i>
                    <span>icm-image5</span>
                </li>
                <li>
                    <i class="icm icm-image6"></i>
                    <span>icm-image6</span>
                </li>
                <li>
                    <i class="icm icm-images2"></i>
                    <span>icm-images2</span>
                </li>
                <li>
                    <i class="icm icm-image7"></i>
                    <span>icm-image7</span>
                </li>
                <li>
                    <i class="icm icm-camera"></i>
                    <span>icm-camera</span>
                </li>
                <li>
                    <i class="icm icm-camera2"></i>
                    <span>icm-camera2</span>
                </li>
                <li>
                    <i class="icm icm-camera3"></i>
                    <span>icm-camera3</span>
                </li>
                <li>
                    <i class="icm icm-camera4"></i>
                    <span>icm-camera4</span>
                </li>
                <li>
                    <i class="icm icm-music"></i>
                    <span>icm-music</span>
                </li>
                <li>
                    <i class="icm icm-music2"></i>
                    <span>icm-music2</span>
                </li>
                <li>
                    <i class="icm icm-music3"></i>
                    <span>icm-music3</span>
                </li>
                <li>
                    <i class="icm icm-music4"></i>
                    <span>icm-music4</span>
                </li>
                <li>
                    <i class="icm icm-music5"></i>
                    <span>icm-music5</span>
                </li>
                <li>
                    <i class="icm icm-music6"></i>
                    <span>icm-music6</span>
                </li>
                <li>
                    <i class="icm icm-piano"></i>
                    <span>icm-piano</span>
                </li>
                <li>
                    <i class="icm icm-guitar"></i>
                    <span>icm-guitar</span>
                </li>
                <li>
                    <i class="icm icm-headphones"></i>
                    <span>icm-headphones</span>
                </li>
                <li>
                    <i class="icm icm-headphones2"></i>
                    <span>icm-headphones2</span>
                </li>
                <li>
                    <i class="icm icm-play"></i>
                    <span>icm-play</span>
                </li>
                <li>
                    <i class="icm icm-play2"></i>
                    <span>icm-play2</span>
                </li>
                <li>
                    <i class="icm icm-movie"></i>
                    <span>icm-movie</span>
                </li>
                <li>
                    <i class="icm icm-movie2"></i>
                    <span>icm-movie2</span>
                </li>
                <li>
                    <i class="icm icm-movie3"></i>
                    <span>icm-movie3</span>
                </li>
                <li>
                    <i class="icm icm-film"></i>
                    <span>icm-film</span>
                </li>
                <li>
                    <i class="icm icm-film2"></i>
                    <span>icm-film2</span>
                </li>
                <li>
                    <i class="icm icm-film3"></i>
                    <span>icm-film3</span>
                </li>
                <li>
                    <i class="icm icm-film4"></i>
                    <span>icm-film4</span>
                </li>
                <li>
                    <i class="icm icm-camera5"></i>
                    <span>icm-camera5</span>
                </li>
                <li>
                    <i class="icm icm-camera6"></i>
                    <span>icm-camera6</span>
                </li>
                <li>
                    <i class="icm icm-camera7"></i>
                    <span>icm-camera7</span>
                </li>
                <li>
                    <i class="icm icm-camera8"></i>
                    <span>icm-camera8</span>
                </li>
                <li>
                    <i class="icm icm-camera9"></i>
                    <span>icm-camera9</span>
                </li>
                <li>
                    <i class="icm icm-dice"></i>
                    <span>icm-dice</span>
                </li>
                <li>
                    <i class="icm icm-gamepad"></i>
                    <span>icm-gamepad</span>
                </li>
                <li>
                    <i class="icm icm-gamepad2"></i>
                    <span>icm-gamepad2</span>
                </li>
                <li>
                    <i class="icm icm-gamepad3"></i>
                    <span>icm-gamepad3</span>
                </li>
                <li>
                    <i class="icm icm-pacman"></i>
                    <span>icm-pacman</span>
                </li>
                <li>
                    <i class="icm icm-spades"></i>
                    <span>icm-spades</span>
                </li>
                <li>
                    <i class="icm icm-clubs"></i>
                    <span>icm-clubs</span>
                </li>
                <li>
                    <i class="icm icm-diamonds"></i>
                    <span>icm-diamonds</span>
                </li>
                <li>
                    <i class="icm icm-king"></i>
                    <span>icm-king</span>
                </li>
                <li>
                    <i class="icm icm-queen"></i>
                    <span>icm-queen</span>
                </li>
                <li>
                    <i class="icm icm-rock"></i>
                    <span>icm-rock</span>
                </li>
                <li>
                    <i class="icm icm-bishop"></i>
                    <span>icm-bishop</span>
                </li>
                <li>
                    <i class="icm icm-knight"></i>
                    <span>icm-knight</span>
                </li>
                <li>
                    <i class="icm icm-pawn"></i>
                    <span>icm-pawn</span>
                </li>
                <li>
                    <i class="icm icm-chess"></i>
                    <span>icm-chess</span>
                </li>
                <li>
                    <i class="icm icm-bullhorn"></i>
                    <span>icm-bullhorn</span>
                </li>
                <li>
                    <i class="icm icm-megaphone"></i>
                    <span>icm-megaphone</span>
                </li>
                <li>
                    <i class="icm icm-new"></i>
                    <span>icm-new</span>
                </li>
                <li>
                    <i class="icm icm-connection"></i>
                    <span>icm-connection</span>
                </li>
                <li>
                    <i class="icm icm-connection2"></i>
                    <span>icm-connection2</span>
                </li>
                <li>
                    <i class="icm icm-podcast"></i>
                    <span>icm-podcast</span>
                </li>
                <li>
                    <i class="icm icm-radio"></i>
                    <span>icm-radio</span>
                </li>
                <li>
                    <i class="icm icm-feed"></i>
                    <span>icm-feed</span>
                </li>
                <li>
                    <i class="icm icm-connection3"></i>
                    <span>icm-connection3</span>
                </li>
                <li>
                    <i class="icm icm-radio2"></i>
                    <span>icm-radio2</span>
                </li>
                <li>
                    <i class="icm icm-podcast2"></i>
                    <span>icm-podcast2</span>
                </li>
                <li>
                    <i class="icm icm-podcast3"></i>
                    <span>icm-podcast3</span>
                </li>
                <li>
                    <i class="icm icm-mic"></i>
                    <span>icm-mic</span>
                </li>
                <li>
                    <i class="icm icm-mic2"></i>
                    <span>icm-mic2</span>
                </li>
                <li>
                    <i class="icm icm-mic3"></i>
                    <span>icm-mic3</span>
                </li>
                <li>
                    <i class="icm icm-mic4"></i>
                    <span>icm-mic4</span>
                </li>
                <li>
                    <i class="icm icm-mic5"></i>
                    <span>icm-mic5</span>
                </li>
                <li>
                    <i class="icm icm-book"></i>
                    <span>icm-book</span>
                </li>
                <li>
                    <i class="icm icm-book2"></i>
                    <span>icm-book2</span>
                </li>
                <li>
                    <i class="icm icm-books"></i>
                    <span>icm-books</span>
                </li>
                <li>
                    <i class="icm icm-reading"></i>
                    <span>icm-reading</span>
                </li>
                <li>
                    <i class="icm icm-library"></i>
                    <span>icm-library</span>
                </li>
                <li>
                    <i class="icm icm-library2"></i>
                    <span>icm-library2</span>
                </li>
                <li>
                    <i class="icm icm-graduation"></i>
                    <span>icm-graduation</span>
                </li>
                <li>
                    <i class="icm icm-file"></i>
                    <span>icm-file</span>
                </li>
                <li>
                    <i class="icm icm-profile"></i>
                    <span>icm-profile</span>
                </li>
                <li>
                    <i class="icm icm-file2"></i>
                    <span>icm-file2</span>
                </li>
                <li>
                    <i class="icm icm-file3"></i>
                    <span>icm-file3</span>
                </li>
                <li>
                    <i class="icm icm-file4"></i>
                    <span>icm-file4</span>
                </li>
                <li>
                    <i class="icm icm-file5"></i>
                    <span>icm-file5</span>
                </li>
                <li>
                    <i class="icm icm-file6"></i>
                    <span>icm-file6</span>
                </li>
                <li>
                    <i class="icm icm-files"></i>
                    <span>icm-files</span>
                </li>
                <li>
                    <i class="icm icm-file-plus"></i>
                    <span>icm-file-plus</span>
                </li>
                <li>
                    <i class="icm icm-file-minus"></i>
                    <span>icm-file-minus</span>
                </li>
                <li>
                    <i class="icm icm-file-download"></i>
                    <span>icm-file-download</span>
                </li>
                <li>
                    <i class="icm icm-file-upload"></i>
                    <span>icm-file-upload</span>
                </li>
                <li>
                    <i class="icm icm-file-check"></i>
                    <span>icm-file-check</span>
                </li>
                <li>
                    <i class="icm icm-file-remove"></i>
                    <span>icm-file-remove</span>
                </li>
                <li>
                    <i class="icm icm-file7"></i>
                    <span>icm-file7</span>
                </li>
                <li>
                    <i class="icm icm-file8"></i>
                    <span>icm-file8</span>
                </li>
                <li>
                    <i class="icm icm-file-plus2"></i>
                    <span>icm-file-plus2</span>
                </li>
                <li>
                    <i class="icm icm-file-minus2"></i>
                    <span>icm-file-minus2</span>
                </li>
                <li>
                    <i class="icm icm-file-download2"></i>
                    <span>icm-file-download2</span>
                </li>
                <li>
                    <i class="icm icm-file-upload2"></i>
                    <span>icm-file-upload2</span>
                </li>
                <li>
                    <i class="icm icm-file-check2"></i>
                    <span>icm-file-check2</span>
                </li>
                <li>
                    <i class="icm icm-file-remove2"></i>
                    <span>icm-file-remove2</span>
                </li>
                <li>
                    <i class="icm icm-file9"></i>
                    <span>icm-file9</span>
                </li>
                <li>
                    <i class="icm icm-copy"></i>
                    <span>icm-copy</span>
                </li>
                <li>
                    <i class="icm icm-copy2"></i>
                    <span>icm-copy2</span>
                </li>
                <li>
                    <i class="icm icm-copy3"></i>
                    <span>icm-copy3</span>
                </li>
                <li>
                    <i class="icm icm-copy4"></i>
                    <span>icm-copy4</span>
                </li>
                <li>
                    <i class="icm icm-paste"></i>
                    <span>icm-paste</span>
                </li>
                <li>
                    <i class="icm icm-paste2"></i>
                    <span>icm-paste2</span>
                </li>
                <li>
                    <i class="icm icm-paste3"></i>
                    <span>icm-paste3</span>
                </li>
                <li>
                    <i class="icm icm-stack"></i>
                    <span>icm-stack</span>
                </li>
                <li>
                    <i class="icm icm-stack2"></i>
                    <span>icm-stack2</span>
                </li>
                <li>
                    <i class="icm icm-stack3"></i>
                    <span>icm-stack3</span>
                </li>
                <li>
                    <i class="icm icm-folder"></i>
                    <span>icm-folder</span>
                </li>
                <li>
                    <i class="icm icm-folder-download"></i>
                    <span>icm-folder-download</span>
                </li>
                <li>
                    <i class="icm icm-folder-upload"></i>
                    <span>icm-folder-upload</span>
                </li>
                <li>
                    <i class="icm icm-folder-plus"></i>
                    <span>icm-folder-plus</span>
                </li>
                <li>
                    <i class="icm icm-folder-plus2"></i>
                    <span>icm-folder-plus2</span>
                </li>
                <li>
                    <i class="icm icm-folder-minus"></i>
                    <span>icm-folder-minus</span>
                </li>
                <li>
                    <i class="icm icm-folder-minus2"></i>
                    <span>icm-folder-minus2</span>
                </li>
                <li>
                    <i class="icm icm-folder8"></i>
                    <span>icm-folder8</span>
                </li>
                <li>
                    <i class="icm icm-folder-remove"></i>
                    <span>icm-folder-remove</span>
                </li>
                <li>
                    <i class="icm icm-folder2"></i>
                    <span>icm-folder2</span>
                </li>
                <li>
                    <i class="icm icm-folder-open"></i>
                    <span>icm-folder-open</span>
                </li>
                <li>
                    <i class="icm icm-folder3"></i>
                    <span>icm-folder3</span>
                </li>
                <li>
                    <i class="icm icm-folder4"></i>
                    <span>icm-folder4</span>
                </li>
                <li>
                    <i class="icm icm-folder-plus3"></i>
                    <span>icm-folder-plus3</span>
                </li>
                <li>
                    <i class="icm icm-folder-minus3"></i>
                    <span>icm-folder-minus3</span>
                </li>
                <li>
                    <i class="icm icm-folder-plus4"></i>
                    <span>icm-folder-plus4</span>
                </li>
                <li>
                    <i class="icm icm-folder-remove2"></i>
                    <span>icm-folder-remove2</span>
                </li>
                <li>
                    <i class="icm icm-folder-download2"></i>
                    <span>icm-folder-download2</span>
                </li>
                <li>
                    <i class="icm icm-folder-upload2"></i>
                    <span>icm-folder-upload2</span>
                </li>
                <li>
                    <i class="icm icm-folder-download3"></i>
                    <span>icm-folder-download3</span>
                </li>
                <li>
                    <i class="icm icm-folder-upload3"></i>
                    <span>icm-folder-upload3</span>
                </li>
                <li>
                    <i class="icm icm-folder5"></i>
                    <span>icm-folder5</span>
                </li>
                <li>
                    <i class="icm icm-folder-open2"></i>
                    <span>icm-folder-open2</span>
                </li>
                <li>
                    <i class="icm icm-folder6"></i>
                    <span>icm-folder6</span>
                </li>
                <li>
                    <i class="icm icm-folder-open3"></i>
                    <span>icm-folder-open3</span>
                </li>
                <li>
                    <i class="icm icm-certificate"></i>
                    <span>icm-certificate</span>
                </li>
                <li>
                    <i class="icm icm-cc"></i>
                    <span>icm-cc</span>
                </li>
                <li>
                    <i class="icm icm-tag"></i>
                    <span>icm-tag</span>
                </li>
                <li>
                    <i class="icm icm-tag2"></i>
                    <span>icm-tag2</span>
                </li>
                <li>
                    <i class="icm icm-tag3"></i>
                    <span>icm-tag3</span>
                </li>
                <li>
                    <i class="icm icm-tag4"></i>
                    <span>icm-tag4</span>
                </li>
                <li>
                    <i class="icm icm-tag5"></i>
                    <span>icm-tag5</span>
                </li>
                <li>
                    <i class="icm icm-tag6"></i>
                    <span>icm-tag6</span>
                </li>
                <li>
                    <i class="icm icm-tag7"></i>
                    <span>icm-tag7</span>
                </li>
                <li>
                    <i class="icm icm-tags"></i>
                    <span>icm-tags</span>
                </li>
                <li>
                    <i class="icm icm-tags2"></i>
                    <span>icm-tags2</span>
                </li>
                <li>
                    <i class="icm icm-tag8"></i>
                    <span>icm-tag8</span>
                </li>
                <li>
                    <i class="icm icm-barcode"></i>
                    <span>icm-barcode</span>
                </li>
                <li>
                    <i class="icm icm-barcode2"></i>
                    <span>icm-barcode2</span>
                </li>
                <li>
                    <i class="icm icm-qrcode"></i>
                    <span>icm-qrcode</span>
                </li>
                <li>
                    <i class="icm icm-ticket"></i>
                    <span>icm-ticket</span>
                </li>
                <li>
                    <i class="icm icm-cart"></i>
                    <span>icm-cart</span>
                </li>
                <li>
                    <i class="icm icm-cart2"></i>
                    <span>icm-cart2</span>
                </li>
                <li>
                    <i class="icm icm-cart3"></i>
                    <span>icm-cart3</span>
                </li>
                <li>
                    <i class="icm icm-cart4"></i>
                    <span>icm-cart4</span>
                </li>
                <li>
                    <i class="icm icm-cart5"></i>
                    <span>icm-cart5</span>
                </li>
                <li>
                    <i class="icm icm-cart6"></i>
                    <span>icm-cart6</span>
                </li>
                <li>
                    <i class="icm icm-cart7"></i>
                    <span>icm-cart7</span>
                </li>
                <li>
                    <i class="icm icm-cart-plus"></i>
                    <span>icm-cart-plus</span>
                </li>
                <li>
                    <i class="icm icm-cart-minus"></i>
                    <span>icm-cart-minus</span>
                </li>
                <li>
                    <i class="icm icm-cart-add"></i>
                    <span>icm-cart-add</span>
                </li>
                <li>
                    <i class="icm icm-cart-remove"></i>
                    <span>icm-cart-remove</span>
                </li>
                <li>
                    <i class="icm icm-cart-checkout"></i>
                    <span>icm-cart-checkout</span>
                </li>
                <li>
                    <i class="icm icm-cart-remove2"></i>
                    <span>icm-cart-remove2</span>
                </li>
                <li>
                    <i class="icm icm-basket"></i>
                    <span>icm-basket</span>
                </li>
                <li>
                    <i class="icm icm-basket2"></i>
                    <span>icm-basket2</span>
                </li>
                <li>
                    <i class="icm icm-bag"></i>
                    <span>icm-bag</span>
                </li>
                <li>
                    <i class="icm icm-bag2"></i>
                    <span>icm-bag2</span>
                </li>
                <li>
                    <i class="icm icm-bag3"></i>
                    <span>icm-bag3</span>
                </li>
                <li>
                    <i class="icm icm-coin"></i>
                    <span>icm-coin</span>
                </li>
                <li>
                    <i class="icm icm-coins"></i>
                    <span>icm-coins</span>
                </li>
                <li>
                    <i class="icm icm-credit"></i>
                    <span>icm-credit</span>
                </li>
                <li>
                    <i class="icm icm-credit2"></i>
                    <span>icm-credit2</span>
                </li>
                <li>
                    <i class="icm icm-calculate"></i>
                    <span>icm-calculate</span>
                </li>
                <li>
                    <i class="icm icm-calculate2"></i>
                    <span>icm-calculate2</span>
                </li>
                <li>
                    <i class="icm icm-support"></i>
                    <span>icm-support</span>
                </li>
                <li>
                    <i class="icm icm-phone"></i>
                    <span>icm-phone</span>
                </li>
                <li>
                    <i class="icm icm-phone2"></i>
                    <span>icm-phone2</span>
                </li>
                <li>
                    <i class="icm icm-phone3"></i>
                    <span>icm-phone3</span>
                </li>
                <li>
                    <i class="icm icm-phone4"></i>
                    <span>icm-phone4</span>
                </li>
                <li>
                    <i class="icm icm-contact-add"></i>
                    <span>icm-contact-add</span>
                </li>
                <li>
                    <i class="icm icm-contact-remove"></i>
                    <span>icm-contact-remove</span>
                </li>
                <li>
                    <i class="icm icm-contact-add2"></i>
                    <span>icm-contact-add2</span>
                </li>
                <li>
                    <i class="icm icm-contact-remove2"></i>
                    <span>icm-contact-remove2</span>
                </li>
                <li>
                    <i class="icm icm-call-incoming"></i>
                    <span>icm-call-incoming</span>
                </li>
                <li>
                    <i class="icm icm-call-outgoing"></i>
                    <span>icm-call-outgoing</span>
                </li>
                <li>
                    <i class="icm icm-phone5"></i>
                    <span>icm-phone5</span>
                </li>
                <li>
                    <i class="icm icm-phone6"></i>
                    <span>icm-phone6</span>
                </li>
                <li>
                    <i class="icm icm-phone-hang-up"></i>
                    <span>icm-phone-hang-up</span>
                </li>
                <li>
                    <i class="icm icm-phone-hang-up2"></i>
                    <span>icm-phone-hang-up2</span>
                </li>
                <li>
                    <i class="icm icm-address-book"></i>
                    <span>icm-address-book</span>
                </li>
                <li>
                    <i class="icm icm-address-book2"></i>
                    <span>icm-address-book2</span>
                </li>
                <li>
                    <i class="icm icm-notebook"></i>
                    <span>icm-notebook</span>
                </li>
                <li>
                    <i class="icm icm-envelope"></i>
                    <span>icm-envelope</span>
                </li>
                <li>
                    <i class="icm icm-envelope2"></i>
                    <span>icm-envelope2</span>
                </li>
                <li>
                    <i class="icm icm-mail-send"></i>
                    <span>icm-mail-send</span>
                </li>
                <li>
                    <i class="icm icm-envelope-opened"></i>
                    <span>icm-envelope-opened</span>
                </li>
                <li>
                    <i class="icm icm-envelope3"></i>
                    <span>icm-envelope3</span>
                </li>
                <li>
                    <i class="icm icm-pushpin"></i>
                    <span>icm-pushpin</span>
                </li>
                <li>
                    <i class="icm icm-location"></i>
                    <span>icm-location</span>
                </li>
                <li>
                    <i class="icm icm-location2"></i>
                    <span>icm-location2</span>
                </li>
                <li>
                    <i class="icm icm-location3"></i>
                    <span>icm-location3</span>
                </li>
                <li>
                    <i class="icm icm-location4"></i>
                    <span>icm-location4</span>
                </li>
                <li>
                    <i class="icm icm-location5"></i>
                    <span>icm-location5</span>
                </li>
                <li>
                    <i class="icm icm-location6"></i>
                    <span>icm-location6</span>
                </li>
                <li>
                    <i class="icm icm-location7"></i>
                    <span>icm-location7</span>
                </li>
                <li>
                    <i class="icm icm-compass"></i>
                    <span>icm-compass</span>
                </li>
                <li>
                    <i class="icm icm-compass2"></i>
                    <span>icm-compass2</span>
                </li>
                <li>
                    <i class="icm icm-map"></i>
                    <span>icm-map</span>
                </li>
                <li>
                    <i class="icm icm-map2"></i>
                    <span>icm-map2</span>
                </li>
                <li>
                    <i class="icm icm-map3"></i>
                    <span>icm-map3</span>
                </li>
                <li>
                    <i class="icm icm-map4"></i>
                    <span>icm-map4</span>
                </li>
                <li>
                    <i class="icm icm-direction"></i>
                    <span>icm-direction</span>
                </li>
                <li>
                    <i class="icm icm-history"></i>
                    <span>icm-history</span>
                </li>
                <li>
                    <i class="icm icm-history2"></i>
                    <span>icm-history2</span>
                </li>
                <li>
                    <i class="icm icm-clock"></i>
                    <span>icm-clock</span>
                </li>
                <li>
                    <i class="icm icm-clock2"></i>
                    <span>icm-clock2</span>
                </li>
                <li>
                    <i class="icm icm-clock3"></i>
                    <span>icm-clock3</span>
                </li>
                <li>
                    <i class="icm icm-clock4"></i>
                    <span>icm-clock4</span>
                </li>
                <li>
                    <i class="icm icm-watch"></i>
                    <span>icm-watch</span>
                </li>
                <li>
                    <i class="icm icm-clock5"></i>
                    <span>icm-clock5</span>
                </li>
                <li>
                    <i class="icm icm-clock6"></i>
                    <span>icm-clock6</span>
                </li>
                <li>
                    <i class="icm icm-clock7"></i>
                    <span>icm-clock7</span>
                </li>
                <li>
                    <i class="icm icm-alarm"></i>
                    <span>icm-alarm</span>
                </li>
                <li>
                    <i class="icm icm-alarm2"></i>
                    <span>icm-alarm2</span>
                </li>
                <li>
                    <i class="icm icm-bell"></i>
                    <span>icm-bell</span>
                </li>
                <li>
                    <i class="icm icm-bell2"></i>
                    <span>icm-bell2</span>
                </li>
                <li>
                    <i class="icm icm-alarm-plus"></i>
                    <span>icm-alarm-plus</span>
                </li>
                <li>
                    <i class="icm icm-alarm-minus"></i>
                    <span>icm-alarm-minus</span>
                </li>
                <li>
                    <i class="icm icm-alarm-check"></i>
                    <span>icm-alarm-check</span>
                </li>
                <li>
                    <i class="icm icm-alarm-cancel"></i>
                    <span>icm-alarm-cancel</span>
                </li>
                <li>
                    <i class="icm icm-stopwatch"></i>
                    <span>icm-stopwatch</span>
                </li>
                <li>
                    <i class="icm icm-calendar"></i>
                    <span>icm-calendar</span>
                </li>
                <li>
                    <i class="icm icm-calendar2"></i>
                    <span>icm-calendar2</span>
                </li>
                <li>
                    <i class="icm icm-calendar3"></i>
                    <span>icm-calendar3</span>
                </li>
                <li>
                    <i class="icm icm-calendar4"></i>
                    <span>icm-calendar4</span>
                </li>
                <li>
                    <i class="icm icm-calendar5"></i>
                    <span>icm-calendar5</span>
                </li>
                <li>
                    <i class="icm icm-print"></i>
                    <span>icm-print</span>
                </li>
                <li>
                    <i class="icm icm-print2"></i>
                    <span>icm-print2</span>
                </li>
                <li>
                    <i class="icm icm-print3"></i>
                    <span>icm-print3</span>
                </li>
                <li>
                    <i class="icm icm-mouse"></i>
                    <span>icm-mouse</span>
                </li>
                <li>
                    <i class="icm icm-mouse2"></i>
                    <span>icm-mouse2</span>
                </li>
                <li>
                    <i class="icm icm-mouse3"></i>
                    <span>icm-mouse3</span>
                </li>
                <li>
                    <i class="icm icm-mouse4"></i>
                    <span>icm-mouse4</span>
                </li>
                <li>
                    <i class="icm icm-keyboard"></i>
                    <span>icm-keyboard</span>
                </li>
                <li>
                    <i class="icm icm-keyboard2"></i>
                    <span>icm-keyboard2</span>
                </li>
                <li>
                    <i class="icm icm-screen"></i>
                    <span>icm-screen</span>
                </li>
                <li>
                    <i class="icm icm-screen2"></i>
                    <span>icm-screen2</span>
                </li>
                <li>
                    <i class="icm icm-screen3"></i>
                    <span>icm-screen3</span>
                </li>
                <li>
                    <i class="icm icm-screen4"></i>
                    <span>icm-screen4</span>
                </li>
                <li>
                    <i class="icm icm-laptop"></i>
                    <span>icm-laptop</span>
                </li>
                <li>
                    <i class="icm icm-mobile"></i>
                    <span>icm-mobile</span>
                </li>
                <li>
                    <i class="icm icm-mobile2"></i>
                    <span>icm-mobile2</span>
                </li>
                <li>
                    <i class="icm icm-tablet"></i>
                    <span>icm-tablet</span>
                </li>
                <li>
                    <i class="icm icm-mobile3"></i>
                    <span>icm-mobile3</span>
                </li>
                <li>
                    <i class="icm icm-tv"></i>
                    <span>icm-tv</span>
                </li>
                <li>
                    <i class="icm icm-cabinet"></i>
                    <span>icm-cabinet</span>
                </li>
                <li>
                    <i class="icm icm-archive"></i>
                    <span>icm-archive</span>
                </li>
                <li>
                    <i class="icm icm-drawer"></i>
                    <span>icm-drawer</span>
                </li>
                <li>
                    <i class="icm icm-drawer2"></i>
                    <span>icm-drawer2</span>
                </li>
                <li>
                    <i class="icm icm-drawer3"></i>
                    <span>icm-drawer3</span>
                </li>
                <li>
                    <i class="icm icm-box"></i>
                    <span>icm-box</span>
                </li>
                <li>
                    <i class="icm icm-box-add"></i>
                    <span>icm-box-add</span>
                </li>
                <li>
                    <i class="icm icm-box-remove"></i>
                    <span>icm-box-remove</span>
                </li>
                <li>
                    <i class="icm icm-download"></i>
                    <span>icm-download</span>
                </li>
                <li>
                    <i class="icm icm-upload"></i>
                    <span>icm-upload</span>
                </li>
                <li>
                    <i class="icm icm-disk"></i>
                    <span>icm-disk</span>
                </li>
                <li>
                    <i class="icm icm-cd"></i>
                    <span>icm-cd</span>
                </li>
                <li>
                    <i class="icm icm-storage"></i>
                    <span>icm-storage</span>
                </li>
                <li>
                    <i class="icm icm-storage2"></i>
                    <span>icm-storage2</span>
                </li>
                <li>
                    <i class="icm icm-database"></i>
                    <span>icm-database</span>
                </li>
                <li>
                    <i class="icm icm-database2"></i>
                    <span>icm-database2</span>
                </li>
                <li>
                    <i class="icm icm-database3"></i>
                    <span>icm-database3</span>
                </li>
                <li>
                    <i class="icm icm-undo"></i>
                    <span>icm-undo</span>
                </li>
                <li>
                    <i class="icm icm-redo"></i>
                    <span>icm-redo</span>
                </li>
                <li>
                    <i class="icm icm-rotate"></i>
                    <span>icm-rotate</span>
                </li>
                <li>
                    <i class="icm icm-rotate2"></i>
                    <span>icm-rotate2</span>
                </li>
                <li>
                    <i class="icm icm-flip"></i>
                    <span>icm-flip</span>
                </li>
                <li>
                    <i class="icm icm-flip2"></i>
                    <span>icm-flip2</span>
                </li>
                <li>
                    <i class="icm icm-unite"></i>
                    <span>icm-unite</span>
                </li>
                <li>
                    <i class="icm icm-subtract"></i>
                    <span>icm-subtract</span>
                </li>
                <li>
                    <i class="icm icm-interset"></i>
                    <span>icm-interset</span>
                </li>
                <li>
                    <i class="icm icm-exclude"></i>
                    <span>icm-exclude</span>
                </li>
                <li>
                    <i class="icm icm-align-left"></i>
                    <span>icm-align-left</span>
                </li>
                <li>
                    <i class="icm icm-align-center-horizontal"></i>
                    <span>icm-align-center-horizontal</span>
                </li>
                <li>
                    <i class="icm icm-align-right"></i>
                    <span>icm-align-right</span>
                </li>
                <li>
                    <i class="icm icm-align-top"></i>
                    <span>icm-align-top</span>
                </li>
                <li>
                    <i class="icm icm-align-center-vertical"></i>
                    <span>icm-align-center-vertical</span>
                </li>
                <li>
                    <i class="icm icm-align-bottom"></i>
                    <span>icm-align-bottom</span>
                </li>
                <li>
                    <i class="icm icm-undo2"></i>
                    <span>icm-undo2</span>
                </li>
                <li>
                    <i class="icm icm-redo2"></i>
                    <span>icm-redo2</span>
                </li>
                <li>
                    <i class="icm icm-forward"></i>
                    <span>icm-forward</span>
                </li>
                <li>
                    <i class="icm icm-reply"></i>
                    <span>icm-reply</span>
                </li>
                <li>
                    <i class="icm icm-reply2"></i>
                    <span>icm-reply2</span>
                </li>
                <li>
                    <i class="icm icm-bubble"></i>
                    <span>icm-bubble</span>
                </li>
                <li>
                    <i class="icm icm-bubbles"></i>
                    <span>icm-bubbles</span>
                </li>
                <li>
                    <i class="icm icm-bubbles2"></i>
                    <span>icm-bubbles2</span>
                </li>
                <li>
                    <i class="icm icm-bubble2"></i>
                    <span>icm-bubble2</span>
                </li>
                <li>
                    <i class="icm icm-bubbles3"></i>
                    <span>icm-bubbles3</span>
                </li>
                <li>
                    <i class="icm icm-bubbles4"></i>
                    <span>icm-bubbles4</span>
                </li>
                <li>
                    <i class="icm icm-bubble-notification"></i>
                    <span>icm-bubble-notification</span>
                </li>
                <li>
                    <i class="icm icm-bubbles5"></i>
                    <span>icm-bubbles5</span>
                </li>
                <li>
                    <i class="icm icm-bubbles6"></i>
                    <span>icm-bubbles6</span>
                </li>
                <li>
                    <i class="icm icm-bubble3"></i>
                    <span>icm-bubble3</span>
                </li>
                <li>
                    <i class="icm icm-bubble-dots"></i>
                    <span>icm-bubble-dots</span>
                </li>
                <li>
                    <i class="icm icm-bubble4"></i>
                    <span>icm-bubble4</span>
                </li>
                <li>
                    <i class="icm icm-bubble5"></i>
                    <span>icm-bubble5</span>
                </li>
                <li>
                    <i class="icm icm-bubble-dots2"></i>
                    <span>icm-bubble-dots2</span>
                </li>
                <li>
                    <i class="icm icm-bubble6"></i>
                    <span>icm-bubble6</span>
                </li>
                <li>
                    <i class="icm icm-bubble7"></i>
                    <span>icm-bubble7</span>
                </li>
                <li>
                    <i class="icm icm-bubble8"></i>
                    <span>icm-bubble8</span>
                </li>
                <li>
                    <i class="icm icm-bubbles7"></i>
                    <span>icm-bubbles7</span>
                </li>
                <li>
                    <i class="icm icm-bubble9"></i>
                    <span>icm-bubble9</span>
                </li>
                <li>
                    <i class="icm icm-bubbles8"></i>
                    <span>icm-bubbles8</span>
                </li>
                <li>
                    <i class="icm icm-bubble10"></i>
                    <span>icm-bubble10</span>
                </li>
                <li>
                    <i class="icm icm-bubble-dots3"></i>
                    <span>icm-bubble-dots3</span>
                </li>
                <li>
                    <i class="icm icm-bubble11"></i>
                    <span>icm-bubble11</span>
                </li>
                <li>
                    <i class="icm icm-bubble12"></i>
                    <span>icm-bubble12</span>
                </li>
                <li>
                    <i class="icm icm-bubble-dots4"></i>
                    <span>icm-bubble-dots4</span>
                </li>
                <li>
                    <i class="icm icm-bubble13"></i>
                    <span>icm-bubble13</span>
                </li>
                <li>
                    <i class="icm icm-bubbles9"></i>
                    <span>icm-bubbles9</span>
                </li>
                <li>
                    <i class="icm icm-bubbles10"></i>
                    <span>icm-bubbles10</span>
                </li>
                <li>
                    <i class="icm icm-bubble-blocked"></i>
                    <span>icm-bubble-blocked</span>
                </li>
                <li>
                    <i class="icm icm-bubble-quote"></i>
                    <span>icm-bubble-quote</span>
                </li>
                <li>
                    <i class="icm icm-bubble-user"></i>
                    <span>icm-bubble-user</span>
                </li>
                <li>
                    <i class="icm icm-bubble-check"></i>
                    <span>icm-bubble-check</span>
                </li>
                <li>
                    <i class="icm icm-bubble-video-chat"></i>
                    <span>icm-bubble-video-chat</span>
                </li>
                <li>
                    <i class="icm icm-bubble-link"></i>
                    <span>icm-bubble-link</span>
                </li>
                <li>
                    <i class="icm icm-bubble-locked"></i>
                    <span>icm-bubble-locked</span>
                </li>
                <li>
                    <i class="icm icm-bubble-star"></i>
                    <span>icm-bubble-star</span>
                </li>
                <li>
                    <i class="icm icm-bubble-heart"></i>
                    <span>icm-bubble-heart</span>
                </li>
                <li>
                    <i class="icm icm-bubble-paperclip"></i>
                    <span>icm-bubble-paperclip</span>
                </li>
                <li>
                    <i class="icm icm-bubble-cancel"></i>
                    <span>icm-bubble-cancel</span>
                </li>
                <li>
                    <i class="icm icm-bubble-plus"></i>
                    <span>icm-bubble-plus</span>
                </li>
                <li>
                    <i class="icm icm-bubble-minus"></i>
                    <span>icm-bubble-minus</span>
                </li>
                <li>
                    <i class="icm icm-bubble-notification2"></i>
                    <span>icm-bubble-notification2</span>
                </li>
                <li>
                    <i class="icm icm-bubble-trash"></i>
                    <span>icm-bubble-trash</span>
                </li>
                <li>
                    <i class="icm icm-bubble-left"></i>
                    <span>icm-bubble-left</span>
                </li>
                <li>
                    <i class="icm icm-bubble-right"></i>
                    <span>icm-bubble-right</span>
                </li>
                <li>
                    <i class="icm icm-bubble-up"></i>
                    <span>icm-bubble-up</span>
                </li>
                <li>
                    <i class="icm icm-bubble-down"></i>
                    <span>icm-bubble-down</span>
                </li>
                <li>
                    <i class="icm icm-bubble-first"></i>
                    <span>icm-bubble-first</span>
                </li>
                <li>
                    <i class="icm icm-bubble-last"></i>
                    <span>icm-bubble-last</span>
                </li>
                <li>
                    <i class="icm icm-bubble-replu"></i>
                    <span>icm-bubble-replu</span>
                </li>
                <li>
                    <i class="icm icm-bubble-forward"></i>
                    <span>icm-bubble-forward</span>
                </li>
                <li>
                    <i class="icm icm-bubble-reply"></i>
                    <span>icm-bubble-reply</span>
                </li>
                <li>
                    <i class="icm icm-bubble-forward2"></i>
                    <span>icm-bubble-forward2</span>
                </li>
                <li>
                    <i class="icm icm-user"></i>
                    <span>icm-user</span>
                </li>
                <li>
                    <i class="icm icm-users"></i>
                    <span>icm-users</span>
                </li>
                <li>
                    <i class="icm icm-user-plus"></i>
                    <span>icm-user-plus</span>
                </li>
                <li>
                    <i class="icm icm-user-plus2"></i>
                    <span>icm-user-plus2</span>
                </li>
                <li>
                    <i class="icm icm-user-minus"></i>
                    <span>icm-user-minus</span>
                </li>
                <li>
                    <i class="icm icm-user-minus2"></i>
                    <span>icm-user-minus2</span>
                </li>
                <li>
                    <i class="icm icm-user-cancel"></i>
                    <span>icm-user-cancel</span>
                </li>
                <li>
                    <i class="icm icm-user-block"></i>
                    <span>icm-user-block</span>
                </li>
                <li>
                    <i class="icm icm-users2"></i>
                    <span>icm-users2</span>
                </li>
                <li>
                    <i class="icm icm-user2"></i>
                    <span>icm-user2</span>
                </li>
                <li>
                    <i class="icm icm-users3"></i>
                    <span>icm-users3</span>
                </li>
                <li>
                    <i class="icm icm-user-plus3"></i>
                    <span>icm-user-plus3</span>
                </li>
                <li>
                    <i class="icm icm-user-minus3"></i>
                    <span>icm-user-minus3</span>
                </li>
                <li>
                    <i class="icm icm-user-cancel2"></i>
                    <span>icm-user-cancel2</span>
                </li>
                <li>
                    <i class="icm icm-user-block2"></i>
                    <span>icm-user-block2</span>
                </li>
                <li>
                    <i class="icm icm-user3"></i>
                    <span>icm-user3</span>
                </li>
                <li>
                    <i class="icm icm-user4"></i>
                    <span>icm-user4</span>
                </li>
                <li>
                    <i class="icm icm-user5"></i>
                    <span>icm-user5</span>
                </li>
                <li>
                    <i class="icm icm-user6"></i>
                    <span>icm-user6</span>
                </li>
                <li>
                    <i class="icm icm-users4"></i>
                    <span>icm-users4</span>
                </li>
                <li>
                    <i class="icm icm-user7"></i>
                    <span>icm-user7</span>
                </li>
                <li>
                    <i class="icm icm-user8"></i>
                    <span>icm-user8</span>
                </li>
                <li>
                    <i class="icm icm-users5"></i>
                    <span>icm-users5</span>
                </li>
                <li>
                    <i class="icm icm-vcard"></i>
                    <span>icm-vcard</span>
                </li>
                <li>
                    <i class="icm icm-tshirt"></i>
                    <span>icm-tshirt</span>
                </li>
                <li>
                    <i class="icm icm-hanger"></i>
                    <span>icm-hanger</span>
                </li>
                <li>
                    <i class="icm icm-quotes-left"></i>
                    <span>icm-quotes-left</span>
                </li>
                <li>
                    <i class="icm icm-quotes-right"></i>
                    <span>icm-quotes-right</span>
                </li>
                <li>
                    <i class="icm icm-quotes-right2"></i>
                    <span>icm-quotes-right2</span>
                </li>
                <li>
                    <i class="icm icm-quotes-right3"></i>
                    <span>icm-quotes-right3</span>
                </li>
                <li>
                    <i class="icm icm-busy"></i>
                    <span>icm-busy</span>
                </li>
                <li>
                    <i class="icm icm-busy2"></i>
                    <span>icm-busy2</span>
                </li>
                <li>
                    <i class="icm icm-busy3"></i>
                    <span>icm-busy3</span>
                </li>
                <li>
                    <i class="icm icm-busy4"></i>
                    <span>icm-busy4</span>
                </li>
                <li>
                    <i class="icm icm-spinner"></i>
                    <span>icm-spinner</span>
                </li>
                <li>
                    <i class="icm icm-spinner2"></i>
                    <span>icm-spinner2</span>
                </li>
                <li>
                    <i class="icm icm-spinner3"></i>
                    <span>icm-spinner3</span>
                </li>
                <li>
                    <i class="icm icm-spinner4"></i>
                    <span>icm-spinner4</span>
                </li>
                <li>
                    <i class="icm icm-spinner5"></i>
                    <span>icm-spinner5</span>
                </li>
                <li>
                    <i class="icm icm-spinner6"></i>
                    <span>icm-spinner6</span>
                </li>
                <li>
                    <i class="icm icm-spinner7"></i>
                    <span>icm-spinner7</span>
                </li>
                <li>
                    <i class="icm icm-spinner8"></i>
                    <span>icm-spinner8</span>
                </li>
                <li>
                    <i class="icm icm-spinner9"></i>
                    <span>icm-spinner9</span>
                </li>
                <li>
                    <i class="icm icm-spinner10"></i>
                    <span>icm-spinner10</span>
                </li>
                <li>
                    <i class="icm icm-spinner11"></i>
                    <span>icm-spinner11</span>
                </li>
                <li>
                    <i class="icm icm-spinner12"></i>
                    <span>icm-spinner12</span>
                </li>
                <li>
                    <i class="icm icm-microscope"></i>
                    <span>icm-microscope</span>
                </li>
                <li>
                    <i class="icm icm-binoculars"></i>
                    <span>icm-binoculars</span>
                </li>
                <li>
                    <i class="icm icm-binoculars2"></i>
                    <span>icm-binoculars2</span>
                </li>
                <li>
                    <i class="icm icm-search"></i>
                    <span>icm-search</span>
                </li>
                <li>
                    <i class="icm icm-search2"></i>
                    <span>icm-search2</span>
                </li>
                <li>
                    <i class="icm icm-zoomin"></i>
                    <span>icm-zoomin</span>
                </li>
                <li>
                    <i class="icm icm-zoomout"></i>
                    <span>icm-zoomout</span>
                </li>
                <li>
                    <i class="icm icm-search3"></i>
                    <span>icm-search3</span>
                </li>
                <li>
                    <i class="icm icm-search4"></i>
                    <span>icm-search4</span>
                </li>
                <li>
                    <i class="icm icm-zoomin2"></i>
                    <span>icm-zoomin2</span>
                </li>
                <li>
                    <i class="icm icm-zoomout2"></i>
                    <span>icm-zoomout2</span>
                </li>
                <li>
                    <i class="icm icm-search5"></i>
                    <span>icm-search5</span>
                </li>
                <li>
                    <i class="icm icm-expand"></i>
                    <span>icm-expand</span>
                </li>
                <li>
                    <i class="icm icm-contract"></i>
                    <span>icm-contract</span>
                </li>
                <li>
                    <i class="icm icm-scale-up"></i>
                    <span>icm-scale-up</span>
                </li>
                <li>
                    <i class="icm icm-scale-down"></i>
                    <span>icm-scale-down</span>
                </li>
                <li>
                    <i class="icm icm-expand2"></i>
                    <span>icm-expand2</span>
                </li>
                <li>
                    <i class="icm icm-contract2"></i>
                    <span>icm-contract2</span>
                </li>
                <li>
                    <i class="icm icm-scale-up2"></i>
                    <span>icm-scale-up2</span>
                </li>
                <li>
                    <i class="icm icm-scale-down2"></i>
                    <span>icm-scale-down2</span>
                </li>
                <li>
                    <i class="icm icm-fullscreen"></i>
                    <span>icm-fullscreen</span>
                </li>
                <li>
                    <i class="icm icm-expand3"></i>
                    <span>icm-expand3</span>
                </li>
                <li>
                    <i class="icm icm-contract3"></i>
                    <span>icm-contract3</span>
                </li>
                <li>
                    <i class="icm icm-key"></i>
                    <span>icm-key</span>
                </li>
                <li>
                    <i class="icm icm-key2"></i>
                    <span>icm-key2</span>
                </li>
                <li>
                    <i class="icm icm-key3"></i>
                    <span>icm-key3</span>
                </li>
                <li>
                    <i class="icm icm-key4"></i>
                    <span>icm-key4</span>
                </li>
                <li>
                    <i class="icm icm-key5"></i>
                    <span>icm-key5</span>
                </li>
                <li>
                    <i class="icm icm-keyhole"></i>
                    <span>icm-keyhole</span>
                </li>
                <li>
                    <i class="icm icm-lock"></i>
                    <span>icm-lock</span>
                </li>
                <li>
                    <i class="icm icm-lock2"></i>
                    <span>icm-lock2</span>
                </li>
                <li>
                    <i class="icm icm-lock3"></i>
                    <span>icm-lock3</span>
                </li>
                <li>
                    <i class="icm icm-lock4"></i>
                    <span>icm-lock4</span>
                </li>
                <li>
                    <i class="icm icm-unlocked"></i>
                    <span>icm-unlocked</span>
                </li>
                <li>
                    <i class="icm icm-lock5"></i>
                    <span>icm-lock5</span>
                </li>
                <li>
                    <i class="icm icm-unlocked2"></i>
                    <span>icm-unlocked2</span>
                </li>
                <li>
                    <i class="icm icm-wrench"></i>
                    <span>icm-wrench</span>
                </li>
                <li>
                    <i class="icm icm-wrench2"></i>
                    <span>icm-wrench2</span>
                </li>
                <li>
                    <i class="icm icm-wrench3"></i>
                    <span>icm-wrench3</span>
                </li>
                <li>
                    <i class="icm icm-wrench4"></i>
                    <span>icm-wrench4</span>
                </li>
                <li>
                    <i class="icm icm-settings"></i>
                    <span>icm-settings</span>
                </li>
                <li>
                    <i class="icm icm-equalizer"></i>
                    <span>icm-equalizer</span>
                </li>
                <li>
                    <i class="icm icm-equalizer2"></i>
                    <span>icm-equalizer2</span>
                </li>
                <li>
                    <i class="icm icm-equalizer3"></i>
                    <span>icm-equalizer3</span>
                </li>
                <li>
                    <i class="icm icm-cog"></i>
                    <span>icm-cog</span>
                </li>
                <li>
                    <i class="icm icm-cogs"></i>
                    <span>icm-cogs</span>
                </li>
                <li>
                    <i class="icm icm-cog2"></i>
                    <span>icm-cog2</span>
                </li>
                <li>
                    <i class="icm icm-cog3"></i>
                    <span>icm-cog3</span>
                </li>
                <li>
                    <i class="icm icm-cog4"></i>
                    <span>icm-cog4</span>
                </li>
                <li>
                    <i class="icm icm-cog5"></i>
                    <span>icm-cog5</span>
                </li>
                <li>
                    <i class="icm icm-cog6"></i>
                    <span>icm-cog6</span>
                </li>
                <li>
                    <i class="icm icm-cog7"></i>
                    <span>icm-cog7</span>
                </li>
                <li>
                    <i class="icm icm-factory"></i>
                    <span>icm-factory</span>
                </li>
                <li>
                    <i class="icm icm-hammer"></i>
                    <span>icm-hammer</span>
                </li>
                <li>
                    <i class="icm icm-tools"></i>
                    <span>icm-tools</span>
                </li>
                <li>
                    <i class="icm icm-screwdriver"></i>
                    <span>icm-screwdriver</span>
                </li>
                <li>
                    <i class="icm icm-screwdriver2"></i>
                    <span>icm-screwdriver2</span>
                </li>
                <li>
                    <i class="icm icm-wand"></i>
                    <span>icm-wand</span>
                </li>
                <li>
                    <i class="icm icm-wand2"></i>
                    <span>icm-wand2</span>
                </li>
                <li>
                    <i class="icm icm-health"></i>
                    <span>icm-health</span>
                </li>
                <li>
                    <i class="icm icm-aid"></i>
                    <span>icm-aid</span>
                </li>
                <li>
                    <i class="icm icm-patch"></i>
                    <span>icm-patch</span>
                </li>
                <li>
                    <i class="icm icm-bug"></i>
                    <span>icm-bug</span>
                </li>
                <li>
                    <i class="icm icm-bug2"></i>
                    <span>icm-bug2</span>
                </li>
                <li>
                    <i class="icm icm-inject"></i>
                    <span>icm-inject</span>
                </li>
                <li>
                    <i class="icm icm-inject2"></i>
                    <span>icm-inject2</span>
                </li>
                <li>
                    <i class="icm icm-construction"></i>
                    <span>icm-construction</span>
                </li>
                <li>
                    <i class="icm icm-cone"></i>
                    <span>icm-cone</span>
                </li>
                <li>
                    <i class="icm icm-pie"></i>
                    <span>icm-pie</span>
                </li>
                <li>
                    <i class="icm icm-pie2"></i>
                    <span>icm-pie2</span>
                </li>
                <li>
                    <i class="icm icm-pie3"></i>
                    <span>icm-pie3</span>
                </li>
                <li>
                    <i class="icm icm-pie4"></i>
                    <span>icm-pie4</span>
                </li>
                <li>
                    <i class="icm icm-pie5"></i>
                    <span>icm-pie5</span>
                </li>
                <li>
                    <i class="icm icm-pie6"></i>
                    <span>icm-pie6</span>
                </li>
                <li>
                    <i class="icm icm-pie7"></i>
                    <span>icm-pie7</span>
                </li>
                <li>
                    <i class="icm icm-stats"></i>
                    <span>icm-stats</span>
                </li>
                <li>
                    <i class="icm icm-stats2"></i>
                    <span>icm-stats2</span>
                </li>
                <li>
                    <i class="icm icm-stats3"></i>
                    <span>icm-stats3</span>
                </li>
                <li>
                    <i class="icm icm-bars"></i>
                    <span>icm-bars</span>
                </li>
                <li>
                    <i class="icm icm-bars2"></i>
                    <span>icm-bars2</span>
                </li>
                <li>
                    <i class="icm icm-bars3"></i>
                    <span>icm-bars3</span>
                </li>
                <li>
                    <i class="icm icm-bars4"></i>
                    <span>icm-bars4</span>
                </li>
                <li>
                    <i class="icm icm-bars5"></i>
                    <span>icm-bars5</span>
                </li>
                <li>
                    <i class="icm icm-bars6"></i>
                    <span>icm-bars6</span>
                </li>
                <li>
                    <i class="icm icm-stats-up"></i>
                    <span>icm-stats-up</span>
                </li>
                <li>
                    <i class="icm icm-stats-down"></i>
                    <span>icm-stats-down</span>
                </li>
                <li>
                    <i class="icm icm-stairs-down"></i>
                    <span>icm-stairs-down</span>
                </li>
                <li>
                    <i class="icm icm-stairs-down2"></i>
                    <span>icm-stairs-down2</span>
                </li>
                <li>
                    <i class="icm icm-chart"></i>
                    <span>icm-chart</span>
                </li>
                <li>
                    <i class="icm icm-stairs"></i>
                    <span>icm-stairs</span>
                </li>
                <li>
                    <i class="icm icm-stairs2"></i>
                    <span>icm-stairs2</span>
                </li>
                <li>
                    <i class="icm icm-ladder"></i>
                    <span>icm-ladder</span>
                </li>
                <li>
                    <i class="icm icm-cake"></i>
                    <span>icm-cake</span>
                </li>
                <li>
                    <i class="icm icm-gift"></i>
                    <span>icm-gift</span>
                </li>
                <li>
                    <i class="icm icm-gift2"></i>
                    <span>icm-gift2</span>
                </li>
                <li>
                    <i class="icm icm-balloon"></i>
                    <span>icm-balloon</span>
                </li>
                <li>
                    <i class="icm icm-rating"></i>
                    <span>icm-rating</span>
                </li>
                <li>
                    <i class="icm icm-rating2"></i>
                    <span>icm-rating2</span>
                </li>
                <li>
                    <i class="icm icm-rating3"></i>
                    <span>icm-rating3</span>
                </li>
                <li>
                    <i class="icm icm-podium"></i>
                    <span>icm-podium</span>
                </li>
                <li>
                    <i class="icm icm-medal"></i>
                    <span>icm-medal</span>
                </li>
                <li>
                    <i class="icm icm-medal2"></i>
                    <span>icm-medal2</span>
                </li>
                <li>
                    <i class="icm icm-medal3"></i>
                    <span>icm-medal3</span>
                </li>
                <li>
                    <i class="icm icm-medal4"></i>
                    <span>icm-medal4</span>
                </li>
                <li>
                    <i class="icm icm-medal5"></i>
                    <span>icm-medal5</span>
                </li>
                <li>
                    <i class="icm icm-crown"></i>
                    <span>icm-crown</span>
                </li>
                <li>
                    <i class="icm icm-trophy"></i>
                    <span>icm-trophy</span>
                </li>
                <li>
                    <i class="icm icm-trophy2"></i>
                    <span>icm-trophy2</span>
                </li>
                <li>
                    <i class="icm icm-trophy-star"></i>
                    <span>icm-trophy-star</span>
                </li>
                <li>
                    <i class="icm icm-diamond"></i>
                    <span>icm-diamond</span>
                </li>
                <li>
                    <i class="icm icm-diamond2"></i>
                    <span>icm-diamond2</span>
                </li>
                <li>
                    <i class="icm icm-glass"></i>
                    <span>icm-glass</span>
                </li>
                <li>
                    <i class="icm icm-glass2"></i>
                    <span>icm-glass2</span>
                </li>
                <li>
                    <i class="icm icm-bottle"></i>
                    <span>icm-bottle</span>
                </li>
                <li>
                    <i class="icm icm-bottle2"></i>
                    <span>icm-bottle2</span>
                </li>
                <li>
                    <i class="icm icm-mug"></i>
                    <span>icm-mug</span>
                </li>
                <li>
                    <i class="icm icm-food"></i>
                    <span>icm-food</span>
                </li>
                <li>
                    <i class="icm icm-food2"></i>
                    <span>icm-food2</span>
                </li>
                <li>
                    <i class="icm icm-hamburger"></i>
                    <span>icm-hamburger</span>
                </li>
                <li>
                    <i class="icm icm-cup"></i>
                    <span>icm-cup</span>
                </li>
                <li>
                    <i class="icm icm-cup2"></i>
                    <span>icm-cup2</span>
                </li>
                <li>
                    <i class="icm icm-leaf"></i>
                    <span>icm-leaf</span>
                </li>
                <li>
                    <i class="icm icm-leaf2"></i>
                    <span>icm-leaf2</span>
                </li>
                <li>
                    <i class="icm icm-apple-fruit"></i>
                    <span>icm-apple-fruit</span>
                </li>
                <li>
                    <i class="icm icm-tree"></i>
                    <span>icm-tree</span>
                </li>
                <li>
                    <i class="icm icm-tree2"></i>
                    <span>icm-tree2</span>
                </li>
                <li>
                    <i class="icm icm-paw"></i>
                    <span>icm-paw</span>
                </li>
                <li>
                    <i class="icm icm-steps"></i>
                    <span>icm-steps</span>
                </li>
                <li>
                    <i class="icm icm-flower"></i>
                    <span>icm-flower</span>
                </li>
                <li>
                    <i class="icm icm-rocket"></i>
                    <span>icm-rocket</span>
                </li>
                <li>
                    <i class="icm icm-meter"></i>
                    <span>icm-meter</span>
                </li>
                <li>
                    <i class="icm icm-meter2"></i>
                    <span>icm-meter2</span>
                </li>
                <li>
                    <i class="icm icm-meter-slow"></i>
                    <span>icm-meter-slow</span>
                </li>
                <li>
                    <i class="icm icm-meter-medium"></i>
                    <span>icm-meter-medium</span>
                </li>
                <li>
                    <i class="icm icm-meter-fast"></i>
                    <span>icm-meter-fast</span>
                </li>
                <li>
                    <i class="icm icm-dashboard"></i>
                    <span>icm-dashboard</span>
                </li>
                <li>
                    <i class="icm icm-hammer2"></i>
                    <span>icm-hammer2</span>
                </li>
                <li>
                    <i class="icm icm-balance"></i>
                    <span>icm-balance</span>
                </li>
                <li>
                    <i class="icm icm-bomb"></i>
                    <span>icm-bomb</span>
                </li>
                <li>
                    <i class="icm icm-fire"></i>
                    <span>icm-fire</span>
                </li>
                <li>
                    <i class="icm icm-fire2"></i>
                    <span>icm-fire2</span>
                </li>
                <li>
                    <i class="icm icm-lab"></i>
                    <span>icm-lab</span>
                </li>
                <li>
                    <i class="icm icm-atom"></i>
                    <span>icm-atom</span>
                </li>
                <li>
                    <i class="icm icm-atom2"></i>
                    <span>icm-atom2</span>
                </li>
                <li>
                    <i class="icm icm-magnet"></i>
                    <span>icm-magnet</span>
                </li>
                <li>
                    <i class="icm icm-magnet2"></i>
                    <span>icm-magnet2</span>
                </li>
                <li>
                    <i class="icm icm-magnet3"></i>
                    <span>icm-magnet3</span>
                </li>
                <li>
                    <i class="icm icm-magnet4"></i>
                    <span>icm-magnet4</span>
                </li>
                <li>
                    <i class="icm icm-dumbbell"></i>
                    <span>icm-dumbbell</span>
                </li>
                <li>
                    <i class="icm icm-skull"></i>
                    <span>icm-skull</span>
                </li>
                <li>
                    <i class="icm icm-skull2"></i>
                    <span>icm-skull2</span>
                </li>
                <li>
                    <i class="icm icm-skull3"></i>
                    <span>icm-skull3</span>
                </li>
                <li>
                    <i class="icm icm-lamp"></i>
                    <span>icm-lamp</span>
                </li>
                <li>
                    <i class="icm icm-lamp2"></i>
                    <span>icm-lamp2</span>
                </li>
                <li>
                    <i class="icm icm-lamp3"></i>
                    <span>icm-lamp3</span>
                </li>
                <li>
                    <i class="icm icm-lamp4"></i>
                    <span>icm-lamp4</span>
                </li>
                <li>
                    <i class="icm icm-remove"></i>
                    <span>icm-remove</span>
                </li>
                <li>
                    <i class="icm icm-remove2"></i>
                    <span>icm-remove2</span>
                </li>
                <li>
                    <i class="icm icm-remove3"></i>
                    <span>icm-remove3</span>
                </li>
                <li>
                    <i class="icm icm-remove4"></i>
                    <span>icm-remove4</span>
                </li>
                <li>
                    <i class="icm icm-remove5"></i>
                    <span>icm-remove5</span>
                </li>
                <li>
                    <i class="icm icm-remove6"></i>
                    <span>icm-remove6</span>
                </li>
                <li>
                    <i class="icm icm-remove7"></i>
                    <span>icm-remove7</span>
                </li>
                <li>
                    <i class="icm icm-remove8"></i>
                    <span>icm-remove8</span>
                </li>
                <li>
                    <i class="icm icm-briefcase"></i>
                    <span>icm-briefcase</span>
                </li>
                <li>
                    <i class="icm icm-briefcase2"></i>
                    <span>icm-briefcase2</span>
                </li>
                <li>
                    <i class="icm icm-briefcase3"></i>
                    <span>icm-briefcase3</span>
                </li>
                <li>
                    <i class="icm icm-airplane"></i>
                    <span>icm-airplane</span>
                </li>
                <li>
                    <i class="icm icm-airplane2"></i>
                    <span>icm-airplane2</span>
                </li>
                <li>
                    <i class="icm icm-paperplane"></i>
                    <span>icm-paperplane</span>
                </li>
                <li>
                    <i class="icm icm-car"></i>
                    <span>icm-car</span>
                </li>
                <li>
                    <i class="icm icm-gas-pump"></i>
                    <span>icm-gas-pump</span>
                </li>
                <li>
                    <i class="icm icm-bus"></i>
                    <span>icm-bus</span>
                </li>
                <li>
                    <i class="icm icm-truck"></i>
                    <span>icm-truck</span>
                </li>
                <li>
                    <i class="icm icm-bike"></i>
                    <span>icm-bike</span>
                </li>
                <li>
                    <i class="icm icm-road"></i>
                    <span>icm-road</span>
                </li>
                <li>
                    <i class="icm icm-train"></i>
                    <span>icm-train</span>
                </li>
                <li>
                    <i class="icm icm-ship"></i>
                    <span>icm-ship</span>
                </li>
                <li>
                    <i class="icm icm-boat"></i>
                    <span>icm-boat</span>
                </li>
                <li>
                    <i class="icm icm-cube"></i>
                    <span>icm-cube</span>
                </li>
                <li>
                    <i class="icm icm-cube2"></i>
                    <span>icm-cube2</span>
                </li>
                <li>
                    <i class="icm icm-cube3"></i>
                    <span>icm-cube3</span>
                </li>
                <li>
                    <i class="icm icm-cube4"></i>
                    <span>icm-cube4</span>
                </li>
                <li>
                    <i class="icm icm-pyramid"></i>
                    <span>icm-pyramid</span>
                </li>
                <li>
                    <i class="icm icm-pyramid2"></i>
                    <span>icm-pyramid2</span>
                </li>
                <li>
                    <i class="icm icm-cylinder"></i>
                    <span>icm-cylinder</span>
                </li>
                <li>
                    <i class="icm icm-package"></i>
                    <span>icm-package</span>
                </li>
                <li>
                    <i class="icm icm-puzzle"></i>
                    <span>icm-puzzle</span>
                </li>
                <li>
                    <i class="icm icm-puzzle2"></i>
                    <span>icm-puzzle2</span>
                </li>
                <li>
                    <i class="icm icm-puzzle3"></i>
                    <span>icm-puzzle3</span>
                </li>
                <li>
                    <i class="icm icm-puzzle4"></i>
                    <span>icm-puzzle4</span>
                </li>
                <li>
                    <i class="icm icm-glasses"></i>
                    <span>icm-glasses</span>
                </li>
                <li>
                    <i class="icm icm-glasses2"></i>
                    <span>icm-glasses2</span>
                </li>
                <li>
                    <i class="icm icm-glasses3"></i>
                    <span>icm-glasses3</span>
                </li>
                <li>
                    <i class="icm icm-sunglasses"></i>
                    <span>icm-sunglasses</span>
                </li>
                <li>
                    <i class="icm icm-accessibility"></i>
                    <span>icm-accessibility</span>
                </li>
                <li>
                    <i class="icm icm-accessibility2"></i>
                    <span>icm-accessibility2</span>
                </li>
                <li>
                    <i class="icm icm-brain"></i>
                    <span>icm-brain</span>
                </li>
                <li>
                    <i class="icm icm-target"></i>
                    <span>icm-target</span>
                </li>
                <li>
                    <i class="icm icm-target2"></i>
                    <span>icm-target2</span>
                </li>
                <li>
                    <i class="icm icm-target3"></i>
                    <span>icm-target3</span>
                </li>
                <li>
                    <i class="icm icm-gun"></i>
                    <span>icm-gun</span>
                </li>
                <li>
                    <i class="icm icm-gun-ban"></i>
                    <span>icm-gun-ban</span>
                </li>
                <li>
                    <i class="icm icm-shield"></i>
                    <span>icm-shield</span>
                </li>
                <li>
                    <i class="icm icm-shield2"></i>
                    <span>icm-shield2</span>
                </li>
                <li>
                    <i class="icm icm-shield3"></i>
                    <span>icm-shield3</span>
                </li>
                <li>
                    <i class="icm icm-shield4"></i>
                    <span>icm-shield4</span>
                </li>
                <li>
                    <i class="icm icm-soccer"></i>
                    <span>icm-soccer</span>
                </li>
                <li>
                    <i class="icm icm-football"></i>
                    <span>icm-football</span>
                </li>
                <li>
                    <i class="icm icm-baseball"></i>
                    <span>icm-baseball</span>
                </li>
                <li>
                    <i class="icm icm-basketball"></i>
                    <span>icm-basketball</span>
                </li>
                <li>
                    <i class="icm icm-golf"></i>
                    <span>icm-golf</span>
                </li>
                <li>
                    <i class="icm icm-hockey"></i>
                    <span>icm-hockey</span>
                </li>
                <li>
                    <i class="icm icm-racing"></i>
                    <span>icm-racing</span>
                </li>
                <li>
                    <i class="icm icm-eightball"></i>
                    <span>icm-eightball</span>
                </li>
                <li>
                    <i class="icm icm-bowlingball"></i>
                    <span>icm-bowlingball</span>
                </li>
                <li>
                    <i class="icm icm-bowling"></i>
                    <span>icm-bowling</span>
                </li>
                <li>
                    <i class="icm icm-bowling2"></i>
                    <span>icm-bowling2</span>
                </li>
                <li>
                    <i class="icm icm-lightning"></i>
                    <span>icm-lightning</span>
                </li>
                <li>
                    <i class="icm icm-power"></i>
                    <span>icm-power</span>
                </li>
                <li>
                    <i class="icm icm-power2"></i>
                    <span>icm-power2</span>
                </li>
                <li>
                    <i class="icm icm-switch"></i>
                    <span>icm-switch</span>
                </li>
                <li>
                    <i class="icm icm-powercord"></i>
                    <span>icm-powercord</span>
                </li>
                <li>
                    <i class="icm icm-cord"></i>
                    <span>icm-cord</span>
                </li>
                <li>
                    <i class="icm icm-socket"></i>
                    <span>icm-socket</span>
                </li>
                <li>
                    <i class="icm icm-clipboard"></i>
                    <span>icm-clipboard</span>
                </li>
                <li>
                    <i class="icm icm-clipboard2"></i>
                    <span>icm-clipboard2</span>
                </li>
                <li>
                    <i class="icm icm-signup"></i>
                    <span>icm-signup</span>
                </li>
                <li>
                    <i class="icm icm-clipboard3"></i>
                    <span>icm-clipboard3</span>
                </li>
                <li>
                    <i class="icm icm-clipboard4"></i>
                    <span>icm-clipboard4</span>
                </li>
                <li>
                    <i class="icm icm-list"></i>
                    <span>icm-list</span>
                </li>
                <li>
                    <i class="icm icm-list2"></i>
                    <span>icm-list2</span>
                </li>
                <li>
                    <i class="icm icm-list3"></i>
                    <span>icm-list3</span>
                </li>
                <li>
                    <i class="icm icm-numbered-list"></i>
                    <span>icm-numbered-list</span>
                </li>
                <li>
                    <i class="icm icm-list4"></i>
                    <span>icm-list4</span>
                </li>
                <li>
                    <i class="icm icm-list5"></i>
                    <span>icm-list5</span>
                </li>
                <li>
                    <i class="icm icm-playlist"></i>
                    <span>icm-playlist</span>
                </li>
                <li>
                    <i class="icm icm-grid"></i>
                    <span>icm-grid</span>
                </li>
                <li>
                    <i class="icm icm-grid2"></i>
                    <span>icm-grid2</span>
                </li>
                <li>
                    <i class="icm icm-grid3"></i>
                    <span>icm-grid3</span>
                </li>
                <li>
                    <i class="icm icm-grid4"></i>
                    <span>icm-grid4</span>
                </li>
                <li>
                    <i class="icm icm-grid5"></i>
                    <span>icm-grid5</span>
                </li>
                <li>
                    <i class="icm icm-grid6"></i>
                    <span>icm-grid6</span>
                </li>
                <li>
                    <i class="icm icm-tree3"></i>
                    <span>icm-tree3</span>
                </li>
                <li>
                    <i class="icm icm-tree4"></i>
                    <span>icm-tree4</span>
                </li>
                <li>
                    <i class="icm icm-tree5"></i>
                    <span>icm-tree5</span>
                </li>
                <li>
                    <i class="icm icm-menu"></i>
                    <span>icm-menu</span>
                </li>
                <li>
                    <i class="icm icm-menu2"></i>
                    <span>icm-menu2</span>
                </li>
                <li>
                    <i class="icm icm-circle-small"></i>
                    <span>icm-circle-small</span>
                </li>
                <li>
                    <i class="icm icm-menu3"></i>
                    <span>icm-menu3</span>
                </li>
                <li>
                    <i class="icm icm-menu4"></i>
                    <span>icm-menu4</span>
                </li>
                <li>
                    <i class="icm icm-menu5"></i>
                    <span>icm-menu5</span>
                </li>
                <li>
                    <i class="icm icm-menu6"></i>
                    <span>icm-menu6</span>
                </li>
                <li>
                    <i class="icm icm-menu7"></i>
                    <span>icm-menu7</span>
                </li>
                <li>
                    <i class="icm icm-menu8"></i>
                    <span>icm-menu8</span>
                </li>
                <li>
                    <i class="icm icm-menu9"></i>
                    <span>icm-menu9</span>
                </li>
                <li>
                    <i class="icm icm-cloud"></i>
                    <span>icm-cloud</span>
                </li>
                <li>
                    <i class="icm icm-cloud2"></i>
                    <span>icm-cloud2</span>
                </li>
                <li>
                    <i class="icm icm-cloud3"></i>
                    <span>icm-cloud3</span>
                </li>
                <li>
                    <i class="icm icm-cloud-download"></i>
                    <span>icm-cloud-download</span>
                </li>
                <li>
                    <i class="icm icm-cloud-upload"></i>
                    <span>icm-cloud-upload</span>
                </li>
                <li>
                    <i class="icm icm-download2"></i>
                    <span>icm-download2</span>
                </li>
                <li>
                    <i class="icm icm-upload2"></i>
                    <span>icm-upload2</span>
                </li>
                <li>
                    <i class="icm icm-download3"></i>
                    <span>icm-download3</span>
                </li>
                <li>
                    <i class="icm icm-upload3"></i>
                    <span>icm-upload3</span>
                </li>
                <li>
                    <i class="icm icm-download4"></i>
                    <span>icm-download4</span>
                </li>
                <li>
                    <i class="icm icm-upload4"></i>
                    <span>icm-upload4</span>
                </li>
                <li>
                    <i class="icm icm-download5"></i>
                    <span>icm-download5</span>
                </li>
                <li>
                    <i class="icm icm-upload5"></i>
                    <span>icm-upload5</span>
                </li>
                <li>
                    <i class="icm icm-download6"></i>
                    <span>icm-download6</span>
                </li>
                <li>
                    <i class="icm icm-upload6"></i>
                    <span>icm-upload6</span>
                </li>
                <li>
                    <i class="icm icm-download7"></i>
                    <span>icm-download7</span>
                </li>
                <li>
                    <i class="icm icm-upload7"></i>
                    <span>icm-upload7</span>
                </li>
                <li>
                    <i class="icm icm-globe"></i>
                    <span>icm-globe</span>
                </li>
                <li>
                    <i class="icm icm-globe2"></i>
                    <span>icm-globe2</span>
                </li>
                <li>
                    <i class="icm icm-globe3"></i>
                    <span>icm-globe3</span>
                </li>
                <li>
                    <i class="icm icm-earth"></i>
                    <span>icm-earth</span>
                </li>
                <li>
                    <i class="icm icm-network"></i>
                    <span>icm-network</span>
                </li>
                <li>
                    <i class="icm icm-link"></i>
                    <span>icm-link</span>
                </li>
                <li>
                    <i class="icm icm-link2"></i>
                    <span>icm-link2</span>
                </li>
                <li>
                    <i class="icm icm-link3"></i>
                    <span>icm-link3</span>
                </li>
                <li>
                    <i class="icm icm-link22"></i>
                    <span>icm-link22</span>
                </li>
                <li>
                    <i class="icm icm-link4"></i>
                    <span>icm-link4</span>
                </li>
                <li>
                    <i class="icm icm-link5"></i>
                    <span>icm-link5</span>
                </li>
                <li>
                    <i class="icm icm-link6"></i>
                    <span>icm-link6</span>
                </li>
                <li>
                    <i class="icm icm-anchor"></i>
                    <span>icm-anchor</span>
                </li>
                <li>
                    <i class="icm icm-flag"></i>
                    <span>icm-flag</span>
                </li>
                <li>
                    <i class="icm icm-flag2"></i>
                    <span>icm-flag2</span>
                </li>
                <li>
                    <i class="icm icm-flag3"></i>
                    <span>icm-flag3</span>
                </li>
                <li>
                    <i class="icm icm-flag4"></i>
                    <span>icm-flag4</span>
                </li>
                <li>
                    <i class="icm icm-flag5"></i>
                    <span>icm-flag5</span>
                </li>
                <li>
                    <i class="icm icm-flag6"></i>
                    <span>icm-flag6</span>
                </li>
                <li>
                    <i class="icm icm-attachment"></i>
                    <span>icm-attachment</span>
                </li>
                <li>
                    <i class="icm icm-attachment2"></i>
                    <span>icm-attachment2</span>
                </li>
                <li>
                    <i class="icm icm-eye"></i>
                    <span>icm-eye</span>
                </li>
                <li>
                    <i class="icm icm-eye-blocked"></i>
                    <span>icm-eye-blocked</span>
                </li>
                <li>
                    <i class="icm icm-eye2"></i>
                    <span>icm-eye2</span>
                </li>
                <li>
                    <i class="icm icm-eye3"></i>
                    <span>icm-eye3</span>
                </li>
                <li>
                    <i class="icm icm-eye-blocked2"></i>
                    <span>icm-eye-blocked2</span>
                </li>
                <li>
                    <i class="icm icm-eye4"></i>
                    <span>icm-eye4</span>
                </li>
                <li>
                    <i class="icm icm-eye5"></i>
                    <span>icm-eye5</span>
                </li>
                <li>
                    <i class="icm icm-eye6"></i>
                    <span>icm-eye6</span>
                </li>
                <li>
                    <i class="icm icm-eye7"></i>
                    <span>icm-eye7</span>
                </li>
                <li>
                    <i class="icm icm-eye8"></i>
                    <span>icm-eye8</span>
                </li>
                <li>
                    <i class="icm icm-bookmark"></i>
                    <span>icm-bookmark</span>
                </li>
                <li>
                    <i class="icm icm-bookmark2"></i>
                    <span>icm-bookmark2</span>
                </li>
                <li>
                    <i class="icm icm-bookmarks"></i>
                    <span>icm-bookmarks</span>
                </li>
                <li>
                    <i class="icm icm-bookmark3"></i>
                    <span>icm-bookmark3</span>
                </li>
                <li>
                    <i class="icm icm-spotlight"></i>
                    <span>icm-spotlight</span>
                </li>
                <li>
                    <i class="icm icm-starburst"></i>
                    <span>icm-starburst</span>
                </li>
                <li>
                    <i class="icm icm-snowflake"></i>
                    <span>icm-snowflake</span>
                </li>
                <li>
                    <i class="icm icm-temperature"></i>
                    <span>icm-temperature</span>
                </li>
                <li>
                    <i class="icm icm-temperature2"></i>
                    <span>icm-temperature2</span>
                </li>
                <li>
                    <i class="icm icm-weather-lightning"></i>
                    <span>icm-weather-lightning</span>
                </li>
                <li>
                    <i class="icm icm-weather-rain"></i>
                    <span>icm-weather-rain</span>
                </li>
                <li>
                    <i class="icm icm-weather-snow"></i>
                    <span>icm-weather-snow</span>
                </li>
                <li>
                    <i class="icm icm-windy"></i>
                    <span>icm-windy</span>
                </li>
                <li>
                    <i class="icm icm-fan"></i>
                    <span>icm-fan</span>
                </li>
                <li>
                    <i class="icm icm-umbrella"></i>
                    <span>icm-umbrella</span>
                </li>
                <li>
                    <i class="icm icm-sun"></i>
                    <span>icm-sun</span>
                </li>
                <li>
                    <i class="icm icm-sun2"></i>
                    <span>icm-sun2</span>
                </li>
                <li>
                    <i class="icm icm-brightness-high"></i>
                    <span>icm-brightness-high</span>
                </li>
                <li>
                    <i class="icm icm-brightness-medium"></i>
                    <span>icm-brightness-medium</span>
                </li>
                <li>
                    <i class="icm icm-brightness-low"></i>
                    <span>icm-brightness-low</span>
                </li>
                <li>
                    <i class="icm icm-brightness-contrast"></i>
                    <span>icm-brightness-contrast</span>
                </li>
                <li>
                    <i class="icm icm-contrast"></i>
                    <span>icm-contrast</span>
                </li>
                <li>
                    <i class="icm icm-moon"></i>
                    <span>icm-moon</span>
                </li>
                <li>
                    <i class="icm icm-bed"></i>
                    <span>icm-bed</span>
                </li>
                <li>
                    <i class="icm icm-bed2"></i>
                    <span>icm-bed2</span>
                </li>
                <li>
                    <i class="icm icm-star"></i>
                    <span>icm-star</span>
                </li>
                <li>
                    <i class="icm icm-star2"></i>
                    <span>icm-star2</span>
                </li>
                <li>
                    <i class="icm icm-star3"></i>
                    <span>icm-star3</span>
                </li>
                <li>
                    <i class="icm icm-star4"></i>
                    <span>icm-star4</span>
                </li>
                <li>
                    <i class="icm icm-star5"></i>
                    <span>icm-star5</span>
                </li>
                <li>
                    <i class="icm icm-star6"></i>
                    <span>icm-star6</span>
                </li>
                <li>
                    <i class="icm icm-heart"></i>
                    <span>icm-heart</span>
                </li>
                <li>
                    <i class="icm icm-heart2"></i>
                    <span>icm-heart2</span>
                </li>
                <li>
                    <i class="icm icm-heart3"></i>
                    <span>icm-heart3</span>
                </li>
                <li>
                    <i class="icm icm-heart4"></i>
                    <span>icm-heart4</span>
                </li>
                <li>
                    <i class="icm icm-heart-broken"></i>
                    <span>icm-heart-broken</span>
                </li>
                <li>
                    <i class="icm icm-heart5"></i>
                    <span>icm-heart5</span>
                </li>
                <li>
                    <i class="icm icm-heart6"></i>
                    <span>icm-heart6</span>
                </li>
                <li>
                    <i class="icm icm-heart-broken2"></i>
                    <span>icm-heart-broken2</span>
                </li>
                <li>
                    <i class="icm icm-heart7"></i>
                    <span>icm-heart7</span>
                </li>
                <li>
                    <i class="icm icm-heart8"></i>
                    <span>icm-heart8</span>
                </li>
                <li>
                    <i class="icm icm-heart-broken3"></i>
                    <span>icm-heart-broken3</span>
                </li>
                <li>
                    <i class="icm icm-lips"></i>
                    <span>icm-lips</span>
                </li>
                <li>
                    <i class="icm icm-lips2"></i>
                    <span>icm-lips2</span>
                </li>
                <li>
                    <i class="icm icm-thumbs-up"></i>
                    <span>icm-thumbs-up</span>
                </li>
                <li>
                    <i class="icm icm-thumbs-up2"></i>
                    <span>icm-thumbs-up2</span>
                </li>
                <li>
                    <i class="icm icm-thumbs-down"></i>
                    <span>icm-thumbs-down</span>
                </li>
                <li>
                    <i class="icm icm-thumbs-down2"></i>
                    <span>icm-thumbs-down2</span>
                </li>
                <li>
                    <i class="icm icm-thumbs-up3"></i>
                    <span>icm-thumbs-up3</span>
                </li>
                <li>
                    <i class="icm icm-thumbs-up4"></i>
                    <span>icm-thumbs-up4</span>
                </li>
                <li>
                    <i class="icm icm-thumbs-up5"></i>
                    <span>icm-thumbs-up5</span>
                </li>
                <li>
                    <i class="icm icm-thumbs-up6"></i>
                    <span>icm-thumbs-up6</span>
                </li>
                <li>
                    <i class="icm icm-people"></i>
                    <span>icm-people</span>
                </li>
                <li>
                    <i class="icm icm-man"></i>
                    <span>icm-man</span>
                </li>
                <li>
                    <i class="icm icm-male"></i>
                    <span>icm-male</span>
                </li>
                <li>
                    <i class="icm icm-woman"></i>
                    <span>icm-woman</span>
                </li>
                <li>
                    <i class="icm icm-female"></i>
                    <span>icm-female</span>
                </li>
                <li>
                    <i class="icm icm-peace"></i>
                    <span>icm-peace</span>
                </li>
                <li>
                    <i class="icm icm-yin-yang"></i>
                    <span>icm-yin-yang</span>
                </li>
                <li>
                    <i class="icm icm-happy"></i>
                    <span>icm-happy</span>
                </li>
                <li>
                    <i class="icm icm-happy2"></i>
                    <span>icm-happy2</span>
                </li>
                <li>
                    <i class="icm icm-smiley"></i>
                    <span>icm-smiley</span>
                </li>
                <li>
                    <i class="icm icm-smiley2"></i>
                    <span>icm-smiley2</span>
                </li>
                <li>
                    <i class="icm icm-tongue"></i>
                    <span>icm-tongue</span>
                </li>
                <li>
                    <i class="icm icm-tongue2"></i>
                    <span>icm-tongue2</span>
                </li>
                <li>
                    <i class="icm icm-sad"></i>
                    <span>icm-sad</span>
                </li>
                <li>
                    <i class="icm icm-sad2"></i>
                    <span>icm-sad2</span>
                </li>
                <li>
                    <i class="icm icm-wink"></i>
                    <span>icm-wink</span>
                </li>
                <li>
                    <i class="icm icm-wink2"></i>
                    <span>icm-wink2</span>
                </li>
                <li>
                    <i class="icm icm-grin"></i>
                    <span>icm-grin</span>
                </li>
                <li>
                    <i class="icm icm-grin2"></i>
                    <span>icm-grin2</span>
                </li>
                <li>
                    <i class="icm icm-cool"></i>
                    <span>icm-cool</span>
                </li>
                <li>
                    <i class="icm icm-cool2"></i>
                    <span>icm-cool2</span>
                </li>
                <li>
                    <i class="icm icm-angry"></i>
                    <span>icm-angry</span>
                </li>
                <li>
                    <i class="icm icm-angry2"></i>
                    <span>icm-angry2</span>
                </li>
                <li>
                    <i class="icm icm-evil"></i>
                    <span>icm-evil</span>
                </li>
                <li>
                    <i class="icm icm-evil2"></i>
                    <span>icm-evil2</span>
                </li>
                <li>
                    <i class="icm icm-shocked"></i>
                    <span>icm-shocked</span>
                </li>
                <li>
                    <i class="icm icm-shocked2"></i>
                    <span>icm-shocked2</span>
                </li>
                <li>
                    <i class="icm icm-confused"></i>
                    <span>icm-confused</span>
                </li>
                <li>
                    <i class="icm icm-confused2"></i>
                    <span>icm-confused2</span>
                </li>
                <li>
                    <i class="icm icm-neutral"></i>
                    <span>icm-neutral</span>
                </li>
                <li>
                    <i class="icm icm-neutral2"></i>
                    <span>icm-neutral2</span>
                </li>
                <li>
                    <i class="icm icm-wondering"></i>
                    <span>icm-wondering</span>
                </li>
                <li>
                    <i class="icm icm-wondering2"></i>
                    <span>icm-wondering2</span>
                </li>
                <li>
                    <i class="icm icm-cursor"></i>
                    <span>icm-cursor</span>
                </li>
                <li>
                    <i class="icm icm-cursor2"></i>
                    <span>icm-cursor2</span>
                </li>
                <li>
                    <i class="icm icm-point-up"></i>
                    <span>icm-point-up</span>
                </li>
                <li>
                    <i class="icm icm-point-right"></i>
                    <span>icm-point-right</span>
                </li>
                <li>
                    <i class="icm icm-point-down"></i>
                    <span>icm-point-down</span>
                </li>
                <li>
                    <i class="icm icm-point-left"></i>
                    <span>icm-point-left</span>
                </li>
                <li>
                    <i class="icm icm-pointer"></i>
                    <span>icm-pointer</span>
                </li>
                <li>
                    <i class="icm icm-hand"></i>
                    <span>icm-hand</span>
                </li>
                <li>
                    <i class="icm icm-stack-empty"></i>
                    <span>icm-stack-empty</span>
                </li>
                <li>
                    <i class="icm icm-stack-plus"></i>
                    <span>icm-stack-plus</span>
                </li>
                <li>
                    <i class="icm icm-stack-minus"></i>
                    <span>icm-stack-minus</span>
                </li>
                <li>
                    <i class="icm icm-stack-star"></i>
                    <span>icm-stack-star</span>
                </li>
                <li>
                    <i class="icm icm-stack-picture"></i>
                    <span>icm-stack-picture</span>
                </li>
                <li>
                    <i class="icm icm-stack-down"></i>
                    <span>icm-stack-down</span>
                </li>
                <li>
                    <i class="icm icm-stack-up"></i>
                    <span>icm-stack-up</span>
                </li>
                <li>
                    <i class="icm icm-stack-cancel"></i>
                    <span>icm-stack-cancel</span>
                </li>
                <li>
                    <i class="icm icm-stack-checkmark"></i>
                    <span>icm-stack-checkmark</span>
                </li>
                <li>
                    <i class="icm icm-stack-list"></i>
                    <span>icm-stack-list</span>
                </li>
                <li>
                    <i class="icm icm-stack-clubs"></i>
                    <span>icm-stack-clubs</span>
                </li>
                <li>
                    <i class="icm icm-stack-spades"></i>
                    <span>icm-stack-spades</span>
                </li>
                <li>
                    <i class="icm icm-stack-hearts"></i>
                    <span>icm-stack-hearts</span>
                </li>
                <li>
                    <i class="icm icm-stack-diamonds"></i>
                    <span>icm-stack-diamonds</span>
                </li>
                <li>
                    <i class="icm icm-stack-user"></i>
                    <span>icm-stack-user</span>
                </li>
                <li>
                    <i class="icm icm-stack4"></i>
                    <span>icm-stack4</span>
                </li>
                <li>
                    <i class="icm icm-stack-music"></i>
                    <span>icm-stack-music</span>
                </li>
                <li>
                    <i class="icm icm-stack-play"></i>
                    <span>icm-stack-play</span>
                </li>
                <li>
                    <i class="icm icm-move"></i>
                    <span>icm-move</span>
                </li>
                <li>
                    <i class="icm icm-resize"></i>
                    <span>icm-resize</span>
                </li>
                <li>
                    <i class="icm icm-resize2"></i>
                    <span>icm-resize2</span>
                </li>
                <li>
                    <i class="icm icm-warning"></i>
                    <span>icm-warning</span>
                </li>
                <li>
                    <i class="icm icm-warning2"></i>
                    <span>icm-warning2</span>
                </li>
                <li>
                    <i class="icm icm-notification"></i>
                    <span>icm-notification</span>
                </li>
                <li>
                    <i class="icm icm-notification2"></i>
                    <span>icm-notification2</span>
                </li>
                <li>
                    <i class="icm icm-question"></i>
                    <span>icm-question</span>
                </li>
                <li>
                    <i class="icm icm-question2"></i>
                    <span>icm-question2</span>
                </li>
                <li>
                    <i class="icm icm-question3"></i>
                    <span>icm-question3</span>
                </li>
                <li>
                    <i class="icm icm-question4"></i>
                    <span>icm-question4</span>
                </li>
                <li>
                    <i class="icm icm-question5"></i>
                    <span>icm-question5</span>
                </li>
                <li>
                    <i class="icm icm-plus-circle"></i>
                    <span>icm-plus-circle</span>
                </li>
                <li>
                    <i class="icm icm-plus-circle2"></i>
                    <span>icm-plus-circle2</span>
                </li>
                <li>
                    <i class="icm icm-minus-circle"></i>
                    <span>icm-minus-circle</span>
                </li>
                <li>
                    <i class="icm icm-minus-circle2"></i>
                    <span>icm-minus-circle2</span>
                </li>
                <li>
                    <i class="icm icm-info"></i>
                    <span>icm-info</span>
                </li>
                <li>
                    <i class="icm icm-info2"></i>
                    <span>icm-info2</span>
                </li>
                <li>
                    <i class="icm icm-blocked"></i>
                    <span>icm-blocked</span>
                </li>
                <li>
                    <i class="icm icm-cancel-circle"></i>
                    <span>icm-cancel-circle</span>
                </li>
                <li>
                    <i class="icm icm-cancel-circle2"></i>
                    <span>icm-cancel-circle2</span>
                </li>
                <li>
                    <i class="icm icm-checkmark-circle"></i>
                    <span>icm-checkmark-circle</span>
                </li>
                <li>
                    <i class="icm icm-checkmark-circle2"></i>
                    <span>icm-checkmark-circle2</span>
                </li>
                <li>
                    <i class="icm icm-cancel"></i>
                    <span>icm-cancel</span>
                </li>
                <li>
                    <i class="icm icm-spam"></i>
                    <span>icm-spam</span>
                </li>
                <li>
                    <i class="icm icm-close"></i>
                    <span>icm-close</span>
                </li>
                <li>
                    <i class="icm icm-close2"></i>
                    <span>icm-close2</span>
                </li>
                <li>
                    <i class="icm icm-close3"></i>
                    <span>icm-close3</span>
                </li>
                <li>
                    <i class="icm icm-close4"></i>
                    <span>icm-close4</span>
                </li>
                <li>
                    <i class="icm icm-close5"></i>
                    <span>icm-close5</span>
                </li>
                <li>
                    <i class="icm icm-checkmark"></i>
                    <span>icm-checkmark</span>
                </li>
                <li>
                    <i class="icm icm-checkmark2"></i>
                    <span>icm-checkmark2</span>
                </li>
                <li>
                    <i class="icm icm-checkmark3"></i>
                    <span>icm-checkmark3</span>
                </li>
                <li>
                    <i class="icm icm-checkmark4"></i>
                    <span>icm-checkmark4</span>
                </li>
                <li>
                    <i class="icm icm-spell-check"></i>
                    <span>icm-spell-check</span>
                </li>
                <li>
                    <i class="icm icm-minus"></i>
                    <span>icm-minus</span>
                </li>
                <li>
                    <i class="icm icm-plus"></i>
                    <span>icm-plus</span>
                </li>
                <li>
                    <i class="icm icm-minus2"></i>
                    <span>icm-minus2</span>
                </li>
                <li>
                    <i class="icm icm-plus2"></i>
                    <span>icm-plus2</span>
                </li>
                <li>
                    <i class="icm icm-enter"></i>
                    <span>icm-enter</span>
                </li>
                <li>
                    <i class="icm icm-exit"></i>
                    <span>icm-exit</span>
                </li>
                <li>
                    <i class="icm icm-enter2"></i>
                    <span>icm-enter2</span>
                </li>
                <li>
                    <i class="icm icm-exit2"></i>
                    <span>icm-exit2</span>
                </li>
                <li>
                    <i class="icm icm-enter3"></i>
                    <span>icm-enter3</span>
                </li>
                <li>
                    <i class="icm icm-exit3"></i>
                    <span>icm-exit3</span>
                </li>
                <li>
                    <i class="icm icm-exit4"></i>
                    <span>icm-exit4</span>
                </li>
                <li>
                    <i class="icm icm-play3"></i>
                    <span>icm-play3</span>
                </li>
                <li>
                    <i class="icm icm-pause"></i>
                    <span>icm-pause</span>
                </li>
                <li>
                    <i class="icm icm-stop"></i>
                    <span>icm-stop</span>
                </li>
                <li>
                    <i class="icm icm-backward"></i>
                    <span>icm-backward</span>
                </li>
                <li>
                    <i class="icm icm-forward2"></i>
                    <span>icm-forward2</span>
                </li>
                <li>
                    <i class="icm icm-play4"></i>
                    <span>icm-play4</span>
                </li>
                <li>
                    <i class="icm icm-pause2"></i>
                    <span>icm-pause2</span>
                </li>
                <li>
                    <i class="icm icm-stop2"></i>
                    <span>icm-stop2</span>
                </li>
                <li>
                    <i class="icm icm-backward2"></i>
                    <span>icm-backward2</span>
                </li>
                <li>
                    <i class="icm icm-forward3"></i>
                    <span>icm-forward3</span>
                </li>
                <li>
                    <i class="icm icm-first"></i>
                    <span>icm-first</span>
                </li>
                <li>
                    <i class="icm icm-last"></i>
                    <span>icm-last</span>
                </li>
                <li>
                    <i class="icm icm-previous"></i>
                    <span>icm-previous</span>
                </li>
                <li>
                    <i class="icm icm-next"></i>
                    <span>icm-next</span>
                </li>
                <li>
                    <i class="icm icm-eject"></i>
                    <span>icm-eject</span>
                </li>
                <li>
                    <i class="icm icm-volume-high"></i>
                    <span>icm-volume-high</span>
                </li>
                <li>
                    <i class="icm icm-volume-medium"></i>
                    <span>icm-volume-medium</span>
                </li>
                <li>
                    <i class="icm icm-volume-low"></i>
                    <span>icm-volume-low</span>
                </li>
                <li>
                    <i class="icm icm-volume-mute"></i>
                    <span>icm-volume-mute</span>
                </li>
                <li>
                    <i class="icm icm-volume-mute2"></i>
                    <span>icm-volume-mute2</span>
                </li>
                <li>
                    <i class="icm icm-volume-increase"></i>
                    <span>icm-volume-increase</span>
                </li>
                <li>
                    <i class="icm icm-volume-decrease"></i>
                    <span>icm-volume-decrease</span>
                </li>
                <li>
                    <i class="icm icm-volume-high2"></i>
                    <span>icm-volume-high2</span>
                </li>
                <li>
                    <i class="icm icm-volume-medium2"></i>
                    <span>icm-volume-medium2</span>
                </li>
                <li>
                    <i class="icm icm-volume-low2"></i>
                    <span>icm-volume-low2</span>
                </li>
                <li>
                    <i class="icm icm-volume-mute3"></i>
                    <span>icm-volume-mute3</span>
                </li>
                <li>
                    <i class="icm icm-volume-mute4"></i>
                    <span>icm-volume-mute4</span>
                </li>
                <li>
                    <i class="icm icm-volume-increase2"></i>
                    <span>icm-volume-increase2</span>
                </li>
                <li>
                    <i class="icm icm-volume-decrease2"></i>
                    <span>icm-volume-decrease2</span>
                </li>
                <li>
                    <i class="icm icm-volume5"></i>
                    <span>icm-volume5</span>
                </li>
                <li>
                    <i class="icm icm-volume4"></i>
                    <span>icm-volume4</span>
                </li>
                <li>
                    <i class="icm icm-volume3"></i>
                    <span>icm-volume3</span>
                </li>
                <li>
                    <i class="icm icm-volume2"></i>
                    <span>icm-volume2</span>
                </li>
                <li>
                    <i class="icm icm-volume1"></i>
                    <span>icm-volume1</span>
                </li>
                <li>
                    <i class="icm icm-volume0"></i>
                    <span>icm-volume0</span>
                </li>
                <li>
                    <i class="icm icm-volume-mute5"></i>
                    <span>icm-volume-mute5</span>
                </li>
                <li>
                    <i class="icm icm-volume-mute6"></i>
                    <span>icm-volume-mute6</span>
                </li>
                <li>
                    <i class="icm icm-loop"></i>
                    <span>icm-loop</span>
                </li>
                <li>
                    <i class="icm icm-loop2"></i>
                    <span>icm-loop2</span>
                </li>
                <li>
                    <i class="icm icm-loop3"></i>
                    <span>icm-loop3</span>
                </li>
                <li>
                    <i class="icm icm-loop4"></i>
                    <span>icm-loop4</span>
                </li>
                <li>
                    <i class="icm icm-loop5"></i>
                    <span>icm-loop5</span>
                </li>
                <li>
                    <i class="icm icm-shuffle"></i>
                    <span>icm-shuffle</span>
                </li>
                <li>
                    <i class="icm icm-shuffle2"></i>
                    <span>icm-shuffle2</span>
                </li>
                <li>
                    <i class="icm icm-wave"></i>
                    <span>icm-wave</span>
                </li>
                <li>
                    <i class="icm icm-wave2"></i>
                    <span>icm-wave2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-first"></i>
                    <span>icm-arrow-first</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right"></i>
                    <span>icm-arrow-right</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up"></i>
                    <span>icm-arrow-up</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right2"></i>
                    <span>icm-arrow-right2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down"></i>
                    <span>icm-arrow-down</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left"></i>
                    <span>icm-arrow-left</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up2"></i>
                    <span>icm-arrow-up2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right3"></i>
                    <span>icm-arrow-right3</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down2"></i>
                    <span>icm-arrow-down2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left2"></i>
                    <span>icm-arrow-left2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-left"></i>
                    <span>icm-arrow-up-left</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up3"></i>
                    <span>icm-arrow-up3</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-right"></i>
                    <span>icm-arrow-up-right</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right4"></i>
                    <span>icm-arrow-right4</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-right"></i>
                    <span>icm-arrow-down-right</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down3"></i>
                    <span>icm-arrow-down3</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-left"></i>
                    <span>icm-arrow-down-left</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left3"></i>
                    <span>icm-arrow-left3</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-left2"></i>
                    <span>icm-arrow-up-left2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up4"></i>
                    <span>icm-arrow-up4</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-right2"></i>
                    <span>icm-arrow-up-right2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right5"></i>
                    <span>icm-arrow-right5</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-right2"></i>
                    <span>icm-arrow-down-right2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down4"></i>
                    <span>icm-arrow-down4</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-left2"></i>
                    <span>icm-arrow-down-left2</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left4"></i>
                    <span>icm-arrow-left4</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-left3"></i>
                    <span>icm-arrow-up-left3</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up5"></i>
                    <span>icm-arrow-up5</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-right3"></i>
                    <span>icm-arrow-up-right3</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right6"></i>
                    <span>icm-arrow-right6</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-right3"></i>
                    <span>icm-arrow-down-right3</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down5"></i>
                    <span>icm-arrow-down5</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-left3"></i>
                    <span>icm-arrow-down-left3</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left5"></i>
                    <span>icm-arrow-left5</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-left4"></i>
                    <span>icm-arrow-up-left4</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up6"></i>
                    <span>icm-arrow-up6</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-right4"></i>
                    <span>icm-arrow-up-right4</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right7"></i>
                    <span>icm-arrow-right7</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-right4"></i>
                    <span>icm-arrow-down-right4</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down6"></i>
                    <span>icm-arrow-down6</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-left4"></i>
                    <span>icm-arrow-down-left4</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left6"></i>
                    <span>icm-arrow-left6</span>
                </li>
                <li>
                    <i class="icm icm-arrow"></i>
                    <span>icm-arrow</span>
                </li>
                <li>
                    <i class="icm icm-arrow2"></i>
                    <span>icm-arrow2</span>
                </li>
                <li>
                    <i class="icm icm-arrow3"></i>
                    <span>icm-arrow3</span>
                </li>
                <li>
                    <i class="icm icm-arrow4"></i>
                    <span>icm-arrow4</span>
                </li>
                <li>
                    <i class="icm icm-arrow5"></i>
                    <span>icm-arrow5</span>
                </li>
                <li>
                    <i class="icm icm-arrow6"></i>
                    <span>icm-arrow6</span>
                </li>
                <li>
                    <i class="icm icm-arrow7"></i>
                    <span>icm-arrow7</span>
                </li>
                <li>
                    <i class="icm icm-arrow8"></i>
                    <span>icm-arrow8</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-left5"></i>
                    <span>icm-arrow-up-left5</span>
                </li>
                <li>
                    <i class="icm icm-arrowsquare"></i>
                    <span>icm-arrowsquare</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-right5"></i>
                    <span>icm-arrow-up-right5</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right8"></i>
                    <span>icm-arrow-right8</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-right5"></i>
                    <span>icm-arrow-down-right5</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down7"></i>
                    <span>icm-arrow-down7</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-left5"></i>
                    <span>icm-arrow-down-left5</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left7"></i>
                    <span>icm-arrow-left7</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up7"></i>
                    <span>icm-arrow-up7</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right9"></i>
                    <span>icm-arrow-right9</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down8"></i>
                    <span>icm-arrow-down8</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left8"></i>
                    <span>icm-arrow-left8</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up8"></i>
                    <span>icm-arrow-up8</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right10"></i>
                    <span>icm-arrow-right10</span>
                </li>
                <li>
                    <i class="icm icm-arrow-bottom"></i>
                    <span>icm-arrow-bottom</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left9"></i>
                    <span>icm-arrow-left9</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-left6"></i>
                    <span>icm-arrow-up-left6</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up9"></i>
                    <span>icm-arrow-up9</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-right6"></i>
                    <span>icm-arrow-up-right6</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right11"></i>
                    <span>icm-arrow-right11</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-right6"></i>
                    <span>icm-arrow-down-right6</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down9"></i>
                    <span>icm-arrow-down9</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-left6"></i>
                    <span>icm-arrow-down-left6</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left10"></i>
                    <span>icm-arrow-left10</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-left7"></i>
                    <span>icm-arrow-up-left7</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up10"></i>
                    <span>icm-arrow-up10</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up-right7"></i>
                    <span>icm-arrow-up-right7</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right12"></i>
                    <span>icm-arrow-right12</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-right7"></i>
                    <span>icm-arrow-down-right7</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down10"></i>
                    <span>icm-arrow-down10</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down-left7"></i>
                    <span>icm-arrow-down-left7</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left11"></i>
                    <span>icm-arrow-left11</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up11"></i>
                    <span>icm-arrow-up11</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right13"></i>
                    <span>icm-arrow-right13</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down11"></i>
                    <span>icm-arrow-down11</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left12"></i>
                    <span>icm-arrow-left12</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up12"></i>
                    <span>icm-arrow-up12</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right14"></i>
                    <span>icm-arrow-right14</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down12"></i>
                    <span>icm-arrow-down12</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left13"></i>
                    <span>icm-arrow-left13</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up13"></i>
                    <span>icm-arrow-up13</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right15"></i>
                    <span>icm-arrow-right15</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down13"></i>
                    <span>icm-arrow-down13</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left14"></i>
                    <span>icm-arrow-left14</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up14"></i>
                    <span>icm-arrow-up14</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right16"></i>
                    <span>icm-arrow-right16</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down14"></i>
                    <span>icm-arrow-down14</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left15"></i>
                    <span>icm-arrow-left15</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up15"></i>
                    <span>icm-arrow-up15</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right17"></i>
                    <span>icm-arrow-right17</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down15"></i>
                    <span>icm-arrow-down15</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left16"></i>
                    <span>icm-arrow-left16</span>
                </li>
                <li>
                    <i class="icm icm-arrow-up16"></i>
                    <span>icm-arrow-up16</span>
                </li>
                <li>
                    <i class="icm icm-arrow-right18"></i>
                    <span>icm-arrow-right18</span>
                </li>
                <li>
                    <i class="icm icm-arrow-down16"></i>
                    <span>icm-arrow-down16</span>
                </li>
                <li>
                    <i class="icm icm-arrow-left17"></i>
                    <span>icm-arrow-left17</span>
                </li>
                <li>
                    <i class="icm icm-menu10"></i>
                    <span>icm-menu10</span>
                </li>
                <li>
                    <i class="icm icm-menu11"></i>
                    <span>icm-menu11</span>
                </li>
                <li>
                    <i class="icm icm-menu-close"></i>
                    <span>icm-menu-close</span>
                </li>
                <li>
                    <i class="icm icm-menu-close2"></i>
                    <span>icm-menu-close2</span>
                </li>
                <li>
                    <i class="icm icm-enter4"></i>
                    <span>icm-enter4</span>
                </li>
                <li>
                    <i class="icm icm-enter5"></i>
                    <span>icm-enter5</span>
                </li>
                <li>
                    <i class="icm icm-esc"></i>
                    <span>icm-esc</span>
                </li>
                <li>
                    <i class="icm icm-backspace"></i>
                    <span>icm-backspace</span>
                </li>
                <li>
                    <i class="icm icm-backspace2"></i>
                    <span>icm-backspace2</span>
                </li>
                <li>
                    <i class="icm icm-backspace3"></i>
                    <span>icm-backspace3</span>
                </li>
                <li>
                    <i class="icm icm-tab"></i>
                    <span>icm-tab</span>
                </li>
                <li>
                    <i class="icm icm-transmission"></i>
                    <span>icm-transmission</span>
                </li>
                <li>
                    <i class="icm icm-transmission2"></i>
                    <span>icm-transmission2</span>
                </li>
                <li>
                    <i class="icm icm-sort"></i>
                    <span>icm-sort</span>
                </li>
                <li>
                    <i class="icm icm-sort2"></i>
                    <span>icm-sort2</span>
                </li>
                <li>
                    <i class="icm icm-key-keyboard"></i>
                    <span>icm-key-keyboard</span>
                </li>
                <li>
                    <i class="icm icm-key-A"></i>
                    <span>icm-key-A</span>
                </li>
                <li>
                    <i class="icm icm-key-up"></i>
                    <span>icm-key-up</span>
                </li>
                <li>
                    <i class="icm icm-key-right"></i>
                    <span>icm-key-right</span>
                </li>
                <li>
                    <i class="icm icm-key-down"></i>
                    <span>icm-key-down</span>
                </li>
                <li>
                    <i class="icm icm-key-left"></i>
                    <span>icm-key-left</span>
                </li>
                <li>
                    <i class="icm icm-command"></i>
                    <span>icm-command</span>
                </li>
                <li>
                    <i class="icm icm-checkbox-checked"></i>
                    <span>icm-checkbox-checked</span>
                </li>
                <li>
                    <i class="icm icm-checkbox-unchecked"></i>
                    <span>icm-checkbox-unchecked</span>
                </li>
                <li>
                    <i class="icm icm-square"></i>
                    <span>icm-square</span>
                </li>
                <li>
                    <i class="icm icm-checkbox-partial"></i>
                    <span>icm-checkbox-partial</span>
                </li>
                <li>
                    <i class="icm icm-checkbox"></i>
                    <span>icm-checkbox</span>
                </li>
                <li>
                    <i class="icm icm-checkbox-unchecked2"></i>
                    <span>icm-checkbox-unchecked2</span>
                </li>
                <li>
                    <i class="icm icm-checkbox-partial2"></i>
                    <span>icm-checkbox-partial2</span>
                </li>
                <li>
                    <i class="icm icm-checkbox-checked2"></i>
                    <span>icm-checkbox-checked2</span>
                </li>
                <li>
                    <i class="icm icm-checkbox-unchecked3"></i>
                    <span>icm-checkbox-unchecked3</span>
                </li>
                <li>
                    <i class="icm icm-checkbox-partial3"></i>
                    <span>icm-checkbox-partial3</span>
                </li>
                <li>
                    <i class="icm icm-radio-checked"></i>
                    <span>icm-radio-checked</span>
                </li>
                <li>
                    <i class="icm icm-radio-unchecked"></i>
                    <span>icm-radio-unchecked</span>
                </li>
                <li>
                    <i class="icm icm-circle"></i>
                    <span>icm-circle</span>
                </li>
                <li>
                    <i class="icm icm-circle2"></i>
                    <span>icm-circle2</span>
                </li>
                <li>
                    <i class="icm icm-crop"></i>
                    <span>icm-crop</span>
                </li>
                <li>
                    <i class="icm icm-crop2"></i>
                    <span>icm-crop2</span>
                </li>
                <li>
                    <i class="icm icm-vector"></i>
                    <span>icm-vector</span>
                </li>
                <li>
                    <i class="icm icm-rulers"></i>
                    <span>icm-rulers</span>
                </li>
                <li>
                    <i class="icm icm-scissors"></i>
                    <span>icm-scissors</span>
                </li>
                <li>
                    <i class="icm icm-scissors2"></i>
                    <span>icm-scissors2</span>
                </li>
                <li>
                    <i class="icm icm-scissors3"></i>
                    <span>icm-scissors3</span>
                </li>
                <li>
                    <i class="icm icm-filter"></i>
                    <span>icm-filter</span>
                </li>
                <li>
                    <i class="icm icm-filter2"></i>
                    <span>icm-filter2</span>
                </li>
                <li>
                    <i class="icm icm-filter3"></i>
                    <span>icm-filter3</span>
                </li>
                <li>
                    <i class="icm icm-filter4"></i>
                    <span>icm-filter4</span>
                </li>
                <li>
                    <i class="icm icm-font"></i>
                    <span>icm-font</span>
                </li>
                <li>
                    <i class="icm icm-font-size"></i>
                    <span>icm-font-size</span>
                </li>
                <li>
                    <i class="icm icm-type"></i>
                    <span>icm-type</span>
                </li>
                <li>
                    <i class="icm icm-text-height"></i>
                    <span>icm-text-height</span>
                </li>
                <li>
                    <i class="icm icm-text-width"></i>
                    <span>icm-text-width</span>
                </li>
                <li>
                    <i class="icm icm-height"></i>
                    <span>icm-height</span>
                </li>
                <li>
                    <i class="icm icm-width"></i>
                    <span>icm-width</span>
                </li>
                <li>
                    <i class="icm icm-bold"></i>
                    <span>icm-bold</span>
                </li>
                <li>
                    <i class="icm icm-underline"></i>
                    <span>icm-underline</span>
                </li>
                <li>
                    <i class="icm icm-italic"></i>
                    <span>icm-italic</span>
                </li>
                <li>
                    <i class="icm icm-strikethrough"></i>
                    <span>icm-strikethrough</span>
                </li>
                <li>
                    <i class="icm icm-strikethrough2"></i>
                    <span>icm-strikethrough2</span>
                </li>
                <li>
                    <i class="icm icm-font-size2"></i>
                    <span>icm-font-size2</span>
                </li>
                <li>
                    <i class="icm icm-bold2"></i>
                    <span>icm-bold2</span>
                </li>
                <li>
                    <i class="icm icm-underline2"></i>
                    <span>icm-underline2</span>
                </li>
                <li>
                    <i class="icm icm-italic2"></i>
                    <span>icm-italic2</span>
                </li>
                <li>
                    <i class="icm icm-strikethrough3"></i>
                    <span>icm-strikethrough3</span>
                </li>
                <li>
                    <i class="icm icm-omega"></i>
                    <span>icm-omega</span>
                </li>
                <li>
                    <i class="icm icm-sigma"></i>
                    <span>icm-sigma</span>
                </li>
                <li>
                    <i class="icm icm-nbsp"></i>
                    <span>icm-nbsp</span>
                </li>
                <li>
                    <i class="icm icm-page-break"></i>
                    <span>icm-page-break</span>
                </li>
                <li>
                    <i class="icm icm-page-break2"></i>
                    <span>icm-page-break2</span>
                </li>
                <li>
                    <i class="icm icm-superscript"></i>
                    <span>icm-superscript</span>
                </li>
                <li>
                    <i class="icm icm-subscript"></i>
                    <span>icm-subscript</span>
                </li>
                <li>
                    <i class="icm icm-superscript2"></i>
                    <span>icm-superscript2</span>
                </li>
                <li>
                    <i class="icm icm-subscript2"></i>
                    <span>icm-subscript2</span>
                </li>
                <li>
                    <i class="icm icm-text-color"></i>
                    <span>icm-text-color</span>
                </li>
                <li>
                    <i class="icm icm-highlight"></i>
                    <span>icm-highlight</span>
                </li>
                <li>
                    <i class="icm icm-pagebreak"></i>
                    <span>icm-pagebreak</span>
                </li>
                <li>
                    <i class="icm icm-clear-formatting"></i>
                    <span>icm-clear-formatting</span>
                </li>
                <li>
                    <i class="icm icm-table"></i>
                    <span>icm-table</span>
                </li>
                <li>
                    <i class="icm icm-table2"></i>
                    <span>icm-table2</span>
                </li>
                <li>
                    <i class="icm icm-insert-template"></i>
                    <span>icm-insert-template</span>
                </li>
                <li>
                    <i class="icm icm-pilcrow"></i>
                    <span>icm-pilcrow</span>
                </li>
                <li>
                    <i class="icm icm-lefttoright"></i>
                    <span>icm-lefttoright</span>
                </li>
                <li>
                    <i class="icm icm-righttoleft"></i>
                    <span>icm-righttoleft</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-left"></i>
                    <span>icm-paragraph-left</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-center"></i>
                    <span>icm-paragraph-center</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-right"></i>
                    <span>icm-paragraph-right</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-justify"></i>
                    <span>icm-paragraph-justify</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-left2"></i>
                    <span>icm-paragraph-left2</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-center2"></i>
                    <span>icm-paragraph-center2</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-right2"></i>
                    <span>icm-paragraph-right2</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-justify2"></i>
                    <span>icm-paragraph-justify2</span>
                </li>
                <li>
                    <i class="icm icm-indent-increase"></i>
                    <span>icm-indent-increase</span>
                </li>
                <li>
                    <i class="icm icm-indent-decrease"></i>
                    <span>icm-indent-decrease</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-left3"></i>
                    <span>icm-paragraph-left3</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-center3"></i>
                    <span>icm-paragraph-center3</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-right3"></i>
                    <span>icm-paragraph-right3</span>
                </li>
                <li>
                    <i class="icm icm-paragraph-justify3"></i>
                    <span>icm-paragraph-justify3</span>
                </li>
                <li>
                    <i class="icm icm-indent-increase2"></i>
                    <span>icm-indent-increase2</span>
                </li>
                <li>
                    <i class="icm icm-indent-decrease2"></i>
                    <span>icm-indent-decrease2</span>
                </li>
                <li>
                    <i class="icm icm-share"></i>
                    <span>icm-share</span>
                </li>
                <li>
                    <i class="icm icm-newtab"></i>
                    <span>icm-newtab</span>
                </li>
                <li>
                    <i class="icm icm-newtab2"></i>
                    <span>icm-newtab2</span>
                </li>
                <li>
                    <i class="icm icm-popout"></i>
                    <span>icm-popout</span>
                </li>
                <li>
                    <i class="icm icm-embed"></i>
                    <span>icm-embed</span>
                </li>
                <li>
                    <i class="icm icm-code"></i>
                    <span>icm-code</span>
                </li>
                <li>
                    <i class="icm icm-console"></i>
                    <span>icm-console</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment0"></i>
                    <span>icm-sevensegment0</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment1"></i>
                    <span>icm-sevensegment1</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment2"></i>
                    <span>icm-sevensegment2</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment3"></i>
                    <span>icm-sevensegment3</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment4"></i>
                    <span>icm-sevensegment4</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment5"></i>
                    <span>icm-sevensegment5</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment6"></i>
                    <span>icm-sevensegment6</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment7"></i>
                    <span>icm-sevensegment7</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment8"></i>
                    <span>icm-sevensegment8</span>
                </li>
                <li>
                    <i class="icm icm-sevensegment9"></i>
                    <span>icm-sevensegment9</span>
                </li>
                <li>
                    <i class="icm icm-share2"></i>
                    <span>icm-share2</span>
                </li>
                <li>
                    <i class="icm icm-share3"></i>
                    <span>icm-share3</span>
                </li>
                <li>
                    <i class="icm icm-mail"></i>
                    <span>icm-mail</span>
                </li>
                <li>
                    <i class="icm icm-mail2"></i>
                    <span>icm-mail2</span>
                </li>
                <li>
                    <i class="icm icm-mail3"></i>
                    <span>icm-mail3</span>
                </li>
                <li>
                    <i class="icm icm-mail4"></i>
                    <span>icm-mail4</span>
                </li>
                <li>
                    <i class="icm icm-google"></i>
                    <span>icm-google</span>
                </li>
                <li>
                    <i class="icm icm-googleplus"></i>
                    <span>icm-googleplus</span>
                </li>
                <li>
                    <i class="icm icm-googleplus2"></i>
                    <span>icm-googleplus2</span>
                </li>
                <li>
                    <i class="icm icm-googleplus3"></i>
                    <span>icm-googleplus3</span>
                </li>
                <li>
                    <i class="icm icm-googleplus4"></i>
                    <span>icm-googleplus4</span>
                </li>
                <li>
                    <i class="icm icm-google-drive"></i>
                    <span>icm-google-drive</span>
                </li>
                <li>
                    <i class="icm icm-facebook"></i>
                    <span>icm-facebook</span>
                </li>
                <li>
                    <i class="icm icm-facebook2"></i>
                    <span>icm-facebook2</span>
                </li>
                <li>
                    <i class="icm icm-facebook3"></i>
                    <span>icm-facebook3</span>
                </li>
                <li>
                    <i class="icm icm-facebook4"></i>
                    <span>icm-facebook4</span>
                </li>
                <li>
                    <i class="icm icm-instagram"></i>
                    <span>icm-instagram</span>
                </li>
                <li>
                    <i class="icm icm-twitter"></i>
                    <span>icm-twitter</span>
                </li>
                <li>
                    <i class="icm icm-twitter2"></i>
                    <span>icm-twitter2</span>
                </li>
                <li>
                    <i class="icm icm-twitter3"></i>
                    <span>icm-twitter3</span>
                </li>
                <li>
                    <i class="icm icm-feed2"></i>
                    <span>icm-feed2</span>
                </li>
                <li>
                    <i class="icm icm-feed3"></i>
                    <span>icm-feed3</span>
                </li>
                <li>
                    <i class="icm icm-feed4"></i>
                    <span>icm-feed4</span>
                </li>
                <li>
                    <i class="icm icm-youtube"></i>
                    <span>icm-youtube</span>
                </li>
                <li>
                    <i class="icm icm-youtube2"></i>
                    <span>icm-youtube2</span>
                </li>
                <li>
                    <i class="icm icm-vimeo"></i>
                    <span>icm-vimeo</span>
                </li>
                <li>
                    <i class="icm icm-vimeo2"></i>
                    <span>icm-vimeo2</span>
                </li>
                <li>
                    <i class="icm icm-vimeo3"></i>
                    <span>icm-vimeo3</span>
                </li>
                <li>
                    <i class="icm icm-lanyrd"></i>
                    <span>icm-lanyrd</span>
                </li>
                <li>
                    <i class="icm icm-flickr"></i>
                    <span>icm-flickr</span>
                </li>
                <li>
                    <i class="icm icm-flickr2"></i>
                    <span>icm-flickr2</span>
                </li>
                <li>
                    <i class="icm icm-flickr3"></i>
                    <span>icm-flickr3</span>
                </li>
                <li>
                    <i class="icm icm-flickr4"></i>
                    <span>icm-flickr4</span>
                </li>
                <li>
                    <i class="icm icm-picassa"></i>
                    <span>icm-picassa</span>
                </li>
                <li>
                    <i class="icm icm-picassa2"></i>
                    <span>icm-picassa2</span>
                </li>
                <li>
                    <i class="icm icm-dribbble"></i>
                    <span>icm-dribbble</span>
                </li>
                <li>
                    <i class="icm icm-dribbble2"></i>
                    <span>icm-dribbble2</span>
                </li>
                <li>
                    <i class="icm icm-dribbble3"></i>
                    <span>icm-dribbble3</span>
                </li>
                <li>
                    <i class="icm icm-forrst"></i>
                    <span>icm-forrst</span>
                </li>
                <li>
                    <i class="icm icm-forrst2"></i>
                    <span>icm-forrst2</span>
                </li>
                <li>
                    <i class="icm icm-deviantart"></i>
                    <span>icm-deviantart</span>
                </li>
                <li>
                    <i class="icm icm-deviantart2"></i>
                    <span>icm-deviantart2</span>
                </li>
                <li>
                    <i class="icm icm-steam"></i>
                    <span>icm-steam</span>
                </li>
                <li>
                    <i class="icm icm-steam2"></i>
                    <span>icm-steam2</span>
                </li>
                <li>
                    <i class="icm icm-github"></i>
                    <span>icm-github</span>
                </li>
                <li>
                    <i class="icm icm-github2"></i>
                    <span>icm-github2</span>
                </li>
                <li>
                    <i class="icm icm-github3"></i>
                    <span>icm-github3</span>
                </li>
                <li>
                    <i class="icm icm-github4"></i>
                    <span>icm-github4</span>
                </li>
                <li>
                    <i class="icm icm-github5"></i>
                    <span>icm-github5</span>
                </li>
                <li>
                    <i class="icm icm-wordpress"></i>
                    <span>icm-wordpress</span>
                </li>
                <li>
                    <i class="icm icm-wordpress2"></i>
                    <span>icm-wordpress2</span>
                </li>
                <li>
                    <i class="icm icm-joomla"></i>
                    <span>icm-joomla</span>
                </li>
                <li>
                    <i class="icm icm-blogger"></i>
                    <span>icm-blogger</span>
                </li>
                <li>
                    <i class="icm icm-blogger2"></i>
                    <span>icm-blogger2</span>
                </li>
                <li>
                    <i class="icm icm-tumblr"></i>
                    <span>icm-tumblr</span>
                </li>
                <li>
                    <i class="icm icm-tumblr2"></i>
                    <span>icm-tumblr2</span>
                </li>
                <li>
                    <i class="icm icm-yahoo"></i>
                    <span>icm-yahoo</span>
                </li>
                <li>
                    <i class="icm icm-tux"></i>
                    <span>icm-tux</span>
                </li>
                <li>
                    <i class="icm icm-apple"></i>
                    <span>icm-apple</span>
                </li>
                <li>
                    <i class="icm icm-finder"></i>
                    <span>icm-finder</span>
                </li>
                <li>
                    <i class="icm icm-android"></i>
                    <span>icm-android</span>
                </li>
                <li>
                    <i class="icm icm-windows"></i>
                    <span>icm-windows</span>
                </li>
                <li>
                    <i class="icm icm-windows8"></i>
                    <span>icm-windows8</span>
                </li>
                <li>
                    <i class="icm icm-soundcloud"></i>
                    <span>icm-soundcloud</span>
                </li>
                <li>
                    <i class="icm icm-soundcloud2"></i>
                    <span>icm-soundcloud2</span>
                </li>
                <li>
                    <i class="icm icm-skype"></i>
                    <span>icm-skype</span>
                </li>
                <li>
                    <i class="icm icm-reddit"></i>
                    <span>icm-reddit</span>
                </li>
                <li>
                    <i class="icm icm-linkedin"></i>
                    <span>icm-linkedin</span>
                </li>
                <li>
                    <i class="icm icm-lastfm"></i>
                    <span>icm-lastfm</span>
                </li>
                <li>
                    <i class="icm icm-lastfm2"></i>
                    <span>icm-lastfm2</span>
                </li>
                <li>
                    <i class="icm icm-delicious"></i>
                    <span>icm-delicious</span>
                </li>
                <li>
                    <i class="icm icm-stumbleupon"></i>
                    <span>icm-stumbleupon</span>
                </li>
                <li>
                    <i class="icm icm-stumbleupon2"></i>
                    <span>icm-stumbleupon2</span>
                </li>
                <li>
                    <i class="icm icm-stackoverflow"></i>
                    <span>icm-stackoverflow</span>
                </li>
                <li>
                    <i class="icm icm-pinterest"></i>
                    <span>icm-pinterest</span>
                </li>
                <li>
                    <i class="icm icm-pinterest2"></i>
                    <span>icm-pinterest2</span>
                </li>
                <li>
                    <i class="icm icm-xing"></i>
                    <span>icm-xing</span>
                </li>
                <li>
                    <i class="icm icm-xing2"></i>
                    <span>icm-xing2</span>
                </li>
                <li>
                    <i class="icm icm-flattr"></i>
                    <span>icm-flattr</span>
                </li>
                <li>
                    <i class="icm icm-foursquare"></i>
                    <span>icm-foursquare</span>
                </li>
                <li>
                    <i class="icm icm-foursquare2"></i>
                    <span>icm-foursquare2</span>
                </li>
                <li>
                    <i class="icm icm-paypal"></i>
                    <span>icm-paypal</span>
                </li>
                <li>
                    <i class="icm icm-paypal2"></i>
                    <span>icm-paypal2</span>
                </li>
                <li>
                    <i class="icm icm-paypal3"></i>
                    <span>icm-paypal3</span>
                </li>
                <li>
                    <i class="icm icm-yelp"></i>
                    <span>icm-yelp</span>
                </li>
                <li>
                    <i class="icm icm-libreoffice"></i>
                    <span>icm-libreoffice</span>
                </li>
                <li>
                    <i class="icm icm-file-pdf"></i>
                    <span>icm-file-pdf</span>
                </li>
                <li>
                    <i class="icm icm-file-openoffice"></i>
                    <span>icm-file-openoffice</span>
                </li>
                <li>
                    <i class="icm icm-file-word"></i>
                    <span>icm-file-word</span>
                </li>
                <li>
                    <i class="icm icm-file-excel"></i>
                    <span>icm-file-excel</span>
                </li>
                <li>
                    <i class="icm icm-file-zip"></i>
                    <span>icm-file-zip</span>
                </li>
                <li>
                    <i class="icm icm-file-powerpoint"></i>
                    <span>icm-file-powerpoint</span>
                </li>
                <li>
                    <i class="icm icm-file-xml"></i>
                    <span>icm-file-xml</span>
                </li>
                <li>
                    <i class="icm icm-file-css"></i>
                    <span>icm-file-css</span>
                </li>
                <li>
                    <i class="icm icm-html5"></i>
                    <span>icm-html5</span>
                </li>
                <li>
                    <i class="icm icm-html52"></i>
                    <span>icm-html52</span>
                </li>
                <li>
                    <i class="icm icm-css3"></i>
                    <span>icm-css3</span>
                </li>
                <li>
                    <i class="icm icm-chrome"></i>
                    <span>icm-chrome</span>
                </li>
                <li>
                    <i class="icm icm-firefox"></i>
                    <span>icm-firefox</span>
                </li>
                <li>
                    <i class="icm icm-IE"></i>
                    <span>icm-IE</span>
                </li>
                <li>
                    <i class="icm icm-opera"></i>
                    <span>icm-opera</span>
                </li>
                <li>
                    <i class="icm icm-safari"></i>
                    <span>icm-safari</span>
                </li>
                <li>
                    <i class="icm icm-IcoMoon"></i>
                    <span>icm-IcoMoon</span>
                </li>
            </ul>
        </div>
    </div><!--end .card-body -->
</div><!--end .card -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#modalAncho2').css( "width", "85%" );
        $('.demo-icon-hover').css( "cursor", "pointer" );
        $('#MaterialDesign section').on( 'click', '.demo-icon-hover', function () {
            var $Icono = $(this).find('i').attr('class');
            $(document.getElementById("ind_icono")).val($Icono);
            $(document.getElementById("IconoNew")).attr( "class",  $Icono);
            $('#cerrarModal2').click();
        });
        $('#FontAwesome section').on( 'click', '.demo-icon-hover', function () {
            var $Icono = $(this).find('i').attr('class');
            $(document.getElementById("ind_icono")).val($Icono);
            $(document.getElementById("IconoNew")).attr( "class",  $Icono);
            $('#cerrarModal2').click();
        });
        $('#Icomoon ul').on( 'click', 'li', function () {
            var $Icono = $(this).find('i').attr('class');
            $(document.getElementById("ind_icono")).val($Icono);
            $(document.getElementById("IconoNew")).attr( "class",  $Icono);
            $('#cerrarModal2').click();
        });
    });
</script>

