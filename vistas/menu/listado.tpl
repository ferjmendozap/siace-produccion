<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Menu - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod Interno</th>
                                <th>Cod Padre</th>
                                <th>Aplicacion</th>
                                <th>Nombre</th>
                                <th>Nivel</th>
                                <th>Rol</th>
                                <th>Icono</th>
                                <th>Estatus</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="9">
                                {if in_array('AP-02-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un Menu"  titulo="Crear Menu" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Menu
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}menu/crear';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}menu/jsonDataTabla",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_interno" },
                    { "data": "cod_padre" },
                    { "data": "cod_aplicacion" },
                    { "data": "ind_nombre" },
                    { "data": "num_nivel" },
                    { "data": "ind_rol" },
                    { "orderable": false,"data": "ind_icono",'width':30 },
                    { "orderable": false,"data": "num_estatus",'width':30 },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idMenu:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMenu: $(this).attr('idMenu')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {

            var idMenu=$(this).attr('idMenu');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}menu/Eliminar';
                $.post($url, { idMenu: idMenu },function(dato){
                    if(dato['status']=='OK'){
                        app.metEliminarRegistroJson('dataTablaJson','"El Menu fue eliminado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                },'json');
            });
        });
    });
</script>