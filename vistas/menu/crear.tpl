<form action="{$_Parametros.url}menu/crear" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idMenu}" name="idMenu"/>


        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="fk_a015_num_seguridad_aplicacionError">
                        <select id="fk_a015_num_seguridad_aplicacion" name="form[int][fk_a015_num_seguridad_aplicacion]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$aplicacion}
                                {if isset($formDB.fk_a015_num_seguridad_aplicacion)}
                                    {if $app.pk_num_seguridad_aplicacion==$formDB.fk_a015_num_seguridad_aplicacion}
                                        <option selected value="{$app.pk_num_seguridad_aplicacion}">{$app.ind_nombre_modulo}</option>
                                        {else}
                                        <option value="{$app.pk_num_seguridad_aplicacion}">{$app.ind_nombre_modulo}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_seguridad_aplicacion}">{$app.ind_nombre_modulo}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_a015_num_seguridad_aplicacion">Seleccione la aplicacion</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group floating-label" id="cod_internoError">
                        <input type="text" class="form-control" value="{if isset($formDB.cod_interno)}{$formDB.cod_interno}{/if}" name="form[txt][cod_interno]" id="cod_interno">
                        <label for="cod_interno">Cod Interno</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.cod_padre)}{$formDB.cod_padre}{/if}" name="form[txt][cod_padre]" id="cod_padre">
                        <label for="cod_padre">Cod Padre</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_nombreError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre)}{$formDB.ind_nombre}{/if}" name="form[alphaNum][ind_nombre]" id="ind_nombre">
                        <label for="ind_nombre">Nombre</label>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group floating-label"  id="ind_descripcionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[alphaNum][ind_descripcion]" id="ind_descripcion">
                        <label for="ind_descripcion">Descripcion</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_ruta)}{$formDB.ind_ruta}{/if}" name="form[txt][ind_ruta]" id="ind_ruta">
                        <label for="ind_ruta">URL Externa Sin la BASE. ejemplo: menu/crear</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group floating-label"  id="ind_rolError">
                        <input type="text" class="form-control"  value="{if isset($formDB.ind_rol)}{$formDB.ind_rol}{/if}" name="form[txt][ind_rol]" id="ind_rol">
                        <label for="ind_rol">Rol: cod Interno + accion a realizar</label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="num_nivelError">
                        <select id="num_nivel" name="form[int][num_nivel]" class="form-control">
                            {if isset($formDB.num_nivel) and $formDB.num_nivel == 0}
                                    <option selected value="0">Nivel 0</option>
                                {else}
                                    <option value="0">Nivel 0</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==1}
                                <option selected value="1">Nivel 1</option>
                                {else}
                                <option value="1">Nivel 1</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==2}
                                <option value="2" selected>Nivel 2</option>
                                {else}
                                <option value="2">Nivel 2</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==3}
                                <option value="3" selected>Nivel 3</option>
                                {else}
                                <option value="3">Nivel 3</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==4}
                                <option value="4" selected>Nivel 4</option>
                                {else}
                                <option value="4">Nivel 4</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==5}
                                <option value="5" selected>Nivel 5</option>
                                {else}
                                <option value="5">Nivel 5</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==9 }
                                <option value="9" selected>No Visible</option>
                                {else}
                                <option value="9">No Visible</option>
                            {/if}
                        </select>
                        <label for="num_nivel">Seleccione el Nivel</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_iconoError">
                        <input type="text" class="form-control"   value="{if isset($formDB.ind_icono)}{$formDB.ind_icono}{/if}" name="form[txt][ind_icono]" id="ind_icono">
                        <label for="ind_icono">Icono del Menu</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label">
                        <i id="IconoNew" class="{if isset($formDB.ind_icono)}{$formDB.ind_icono}{/if}"></i>
                        <button type="button" class="btn ink-reaction btn-raised btn-xs btn-info" id="iconoMenu" data-toggle="modal" data-target="#formModal2"
                                titulo="Seleccione el Icono" data-keyboard="false" data-backdrop="static"><i class="md md-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('#iconoMenu').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post("{$_Parametros.url}menu/icono",{ idMenu:0 },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#modalAncho').css( "width", "75%" );
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    for (var item in dato) {
                        if(dato[item]=='error'){
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                        }else{
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                            $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                            $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                        }
                    }
                }else if(dato['status']=='modificacion'){
                    app.metActualizarRegistroTablaJson('dataTablaJson','El Menu fue actualizado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='creacion'){
                    app.metNuevoRegistroTablaJson('dataTablaJson','El Menu fue creado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>