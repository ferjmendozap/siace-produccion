<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataNomina
{
    public function metDataProcesos()
    {
        $nm_b003_tipo_proceso = array(
            array('cod_proceso' => 'PRQ', 'ind_nombre_proceso' => 'PRIMERA QUINCENA', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'FIN', 'ind_nombre_proceso' => 'FIN DE MES', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'GPS', 'ind_nombre_proceso' => 'GARANTIA PRESTACIONES SOCIALES', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'GPA', 'ind_nombre_proceso' => 'GARANTIA PRESTACIONES SOCIALES DIAS ADICIONALES', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'RTA', 'ind_nombre_proceso' => 'RETROACTIVO', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'RPS', 'ind_nombre_proceso' => 'RETROACTIVO GARANTIA PRESTACIONES SOCIALES', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'RPA', 'ind_nombre_proceso' => 'RETROACTIVO GARANTIA PRESTACIONES SOCIALES DIAS ADICIONALES', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'BVC', 'ind_nombre_proceso' => 'BONO VACACIONAL', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'BFA', 'ind_nombre_proceso' => 'BONIFICACIÓN DE FIN DE AÑO', 'num_flag_adelanto' => '0', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1')
        );
        return $nm_b003_tipo_proceso;
    }

    public function metDataNomina()
    {
        $nm_b001_tipo_nomina = array(
            array('cod_tipo_nomina' => '01', 'ind_nombre_nomina' => 'EMPLEADOS', 'num_flag_pago_mensual' => '0', 'ind_titulo_boleta' => 'Nómina de Empleados', 'num_estatus' => '1', 'fec_ultima_modificacion' => '2016-12-05 10:46:33', 'fk_a018_num_seguridad_usuario' => '5'),
            array('cod_tipo_nomina' => '02', 'ind_nombre_nomina' => 'OBREROS', 'num_flag_pago_mensual' => '1', 'ind_titulo_boleta' => 'NOMINA OBREROS', 'num_estatus' => '1', 'fec_ultima_modificacion' => '2016-10-13 10:08:19', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_tipo_nomina' => '03', 'ind_nombre_nomina' => 'JUBILADOS', 'num_flag_pago_mensual' => '1', 'ind_titulo_boleta' => 'Jubilados', 'num_estatus' => '1', 'fec_ultima_modificacion' => '2016-11-07 13:45:40', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_tipo_nomina' => '04', 'ind_nombre_nomina' => 'PENSIONADOS', 'num_flag_pago_mensual' => '0', 'ind_titulo_boleta' => 'Nómina de Pensionados', 'num_estatus' => '1', 'fec_ultima_modificacion' => '2016-11-04 13:52:28', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_tipo_nomina' => '10', 'ind_nombre_nomina' => 'DIRECTORES Y JEFES', 'num_flag_pago_mensual' => '0', 'ind_titulo_boleta' => 'Nómina Directores y Jefes', 'num_estatus' => '1', 'fec_ultima_modificacion' => '2016-12-06 09:25:49', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_tipo_nomina' => 'AF', 'ind_nombre_nomina' => 'NOMINA ALTO FUNCIONARIO', 'num_flag_pago_mensual' => '0', 'ind_titulo_boleta' => 'NOMINA ALTO FUNCIONARIO', 'num_estatus' => '1', 'fec_ultima_modificacion' => '2016-12-06 09:25:49', 'fk_a018_num_seguridad_usuario' => '1')
        );
        return $nm_b001_tipo_nomina;
    }

    public function metDataConceptos()
    {
        $nm_b002_concepto = array(
            array('pk_num_concepto' => '1', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0001', 'ind_descripcion' => 'SUELDO BASICO', 'ind_impresion' => 'SUELDO BASICO', 'num_orden_planilla' => '1', 'ind_formula' => '//aqui tu Codigo
$this->atMonto = $this->metSueldoBasico();
$this->atCantidad = 1;', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-06 11:28:01', 'ind_abrebiatura' => 'SB', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '5','cod_detalle' => 'I'),
            array('pk_num_concepto' => '2', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0002', 'ind_descripcion' => 'DIFERENCIA DE SUELDO BASICO', 'ind_impresion' => 'DIFERENCIA DE SUELDO BASICO', 'num_orden_planilla' => '2', 'ind_formula' => '//aqui tu Codigo
$this->atMonto = $this->metDiferenciaSueldoBasico();
$this->atCantidad = 1;', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 09:07:34', 'ind_abrebiatura' => 'DSB', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('pk_num_concepto' => '3', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0003', 'ind_descripcion' => 'PRIMA POR HIJOS', 'ind_impresion' => 'PRIMA POR HIJOS', 'num_orden_planilla' => '2', 'ind_formula' => '//aqui tu Codigo
$nroHijos = $this->metNroHijos();
$this->atMonto = $nroHijos * 500;
$this->atCantidad = $nroHijos;', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 10:51:45', 'ind_abrebiatura' => 'PH', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '1', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '5','cod_detalle' => 'I'),
            array('pk_num_concepto' => '4', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0004', 'ind_descripcion' => 'PRIMA POR PROFESIONALIZACION', 'ind_impresion' => 'PRIMA POR PROFESIONALIZACION', 'num_orden_planilla' => '2', 'ind_formula' => '//aqui tu Codigo
$this->atMonto = 0;
$this->atCantidad = 0;

$gradoInstruccion = $this->metGradoInstruccion();
if( $gradoInstruccion == "MAG" ){
   	$this->atMonto =1500;
	$this->atCantidad = 0;
}elseif( $gradoInstruccion == "POS" ){
   	$this->atMonto =1500;
	$this->atCantidad = 0;
}elseif( $gradoInstruccion == "UNI" ){
   	$this->atMonto =1200;
	$this->atCantidad = 0;
}elseif( $gradoInstruccion == "TSU" ){
   $this->atMonto =800;
	$this->atCantidad = 0;
}', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-06 12:44:41', 'ind_abrebiatura' => 'PP', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '1', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('pk_num_concepto' => '5', 'fk_a006_num_tipo_concepto' => '7', 'cod_concepto' => '0005', 'ind_descripcion' => 'RETENCION SEGURO SOCIAL OBLIGATORIO SSO', 'ind_impresion' => 'RET SSO', 'num_orden_planilla' => '6', 'ind_formula' => '$this->atMonto = 0;
$this->atCantidad = 0;
//aqui tu Codigo
$sb = $this->metSueldoBasico(true);
$dsb = $this->metDiferenciaSueldoBasico(true);
$sc = $sb + $dsb;
$sueldoNormal = $sc + $this->metObtenerConcepto(\'PH\') + $this->metObtenerConcepto(\'PP\');

if($sueldoNormal > ($this->atConstSueldoMinimo*5)){
	$this->atMonto = ( ( ( ( ($this->atConstSueldoMinimo * 5 ) * 12)  / 52) * (4/100)) *  $this->metLunesLaborados());
	$this->atCantidad = 1;
}else{
	$this->atMonto = ( ( ( ( ($sueldoNormal ) * 12)  / 52) * (4/100)) *  $this->metLunesLaborados());
	$this->atCantidad = 1;
}', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-06 12:43:23', 'ind_abrebiatura' => 'RET SSO', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('pk_num_concepto' => '6', 'fk_a006_num_tipo_concepto' => '6', 'cod_concepto' => '0006', 'ind_descripcion' => 'APORTE SEGURO SOCIAL OBLIGATORIO SSO', 'ind_impresion' => 'APORTE SEGURO SOCIAL OBLIGATORIO SSO', 'num_orden_planilla' => '6', 'ind_formula' => '$this->atMonto = 0;
$this->atCantidad = 0;
//aqui tu Codigo
$sb = $this->metSueldoBasico(true);
$dsb = $this->metDiferenciaSueldoBasico(true);
$sc = $sb + $dsb;
$sueldoNormal = $sc + $this->metObtenerConcepto(\'PH\') + $this->metObtenerConcepto(\'PP\');

if($sueldoNormal > ($this->atConstSueldoMinimo*5)){
	$this->atMonto = ( ( ( ( ($this->atConstSueldoMinimo * 5 ) * 12)  / 52) * (9/100)) *  $this->metLunesLaborados());
	$this->atCantidad = 1;
}else{
	$this->atMonto = ( ( ( ( ($sueldoNormal ) * 12)  / 52) * (9/100)) *  $this->metLunesLaborados());
	$this->atCantidad = 1;
}', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 09:53:52', 'ind_abrebiatura' => 'APSSO', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('pk_num_concepto' => '7', 'fk_a006_num_tipo_concepto' => '7', 'cod_concepto' => '0007', 'ind_descripcion' => 'RETENCION PERDIDA INVOLUNTARIA DE E PIE', 'ind_impresion' => 'RETEN PIE', 'num_orden_planilla' => '6', 'ind_formula' => '$this->atMonto = 0;
$this->atCantidad = 0;
//aqui tu Codigo
$sb = $this->metSueldoBasico(true);
$dsb = $this->metDiferenciaSueldoBasico(true);
$sc = $sb + $dsb;
$sueldoNormal = $sc + $this->metObtenerConcepto(\'PH\') + $this->metObtenerConcepto(\'PP\');

if($sueldoNormal > ($this->atConstSueldoMinimo*5)){
	$this->atMonto = ( ( ( ( ($this->atConstSueldoMinimo * 5 ) * 12)  / 52) * (0.5/100)) *  $this->metLunesLaborados());
	$this->atCantidad = 1;
}else{
	$this->atMonto = ( ( ( ( ($sueldoNormal ) * 12)  / 52) * (0.5/100)) *  $this->metLunesLaborados());
	$this->atCantidad = 1;
}', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 10:28:12', 'ind_abrebiatura' => 'RETEN PIE', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('pk_num_concepto' => '8', 'fk_a006_num_tipo_concepto' => '6', 'cod_concepto' => '0008', 'ind_descripcion' => 'APORTE PERDIDA INVOLUNTARIA DE EMPLEO PIE', 'ind_impresion' => 'APORTE PERDIDA INVOLUNTARIA DE EMPLEO PIE', 'num_orden_planilla' => '6', 'ind_formula' => '$this->atMonto = 0;
$this->atCantidad = 0;
//aqui tu Codigo
$sb = $this->metSueldoBasico(true);
$dsb = $this->metDiferenciaSueldoBasico(true);
$sc = $sb + $dsb;
$sueldoNormal = $sc + $this->metObtenerConcepto(\'PH\') + $this->metObtenerConcepto(\'PP\');

if($sueldoNormal > ($this->atConstSueldoMinimo*5)){
	$this->atMonto = ( ( ( ( ($this->atConstSueldoMinimo * 5 ) * 12)  / 52) * (2/100)) *  $this->metLunesLaborados());
	$this->atCantidad = 1;
}else{
	$this->atMonto = ( ( ( ( ($sueldoNormal ) * 12)  / 52) * (2/100)) *  $this->metLunesLaborados());
	$this->atCantidad = 1;
}', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 10:30:56', 'ind_abrebiatura' => 'AP  PIE', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('pk_num_concepto' => '9', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0009', 'ind_descripcion' => 'PRIMA', 'ind_impresion' => 'PRIMA', 'num_orden_planilla' => '2', 'ind_formula' => '//aqui tu Codigo', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 14:04:44', 'ind_abrebiatura' => 'PG', 'num_flag_automatico' => '0', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '5','cod_detalle' => 'I'),
            array('pk_num_concepto' => '11', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0010', 'ind_descripcion' => 'PRIMA POR ANTIGUEDAD', 'ind_impresion' => 'PRIMA POR ANTIGUEDAD', 'num_orden_planilla' => '6', 'ind_formula' => '//aqui tu Codigo
	if($this->atArgsCodTipoProceso == "RTA"){
		$dsb = $this->metSueldoBasico(true)/30;
	}else{
	   	$dsb = $this->metSueldoBasico(true)/30;
	}

	if($this->metObtenerAñosAdministracionPublica()  >= 20){
		$this->atMonto = (($dsb*30)* (1/100) ) *  20   ;
	}else{
		$this->atMonto = (($dsb*30)* (1/100) ) *  $this->metObtenerAñosAdministracionPublica() ;
	}', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-06 09:05:26', 'ind_abrebiatura' => 'PA', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '1', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('pk_num_concepto' => '17', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0011', 'ind_descripcion' => 'GARANTIA DE PRESTACIONES SOCIALES', 'ind_impresion' => 'GARANTIA DE PRESTACIONES SOCIALES', 'num_orden_planilla' => '1', 'ind_formula' => '//aqui tu Codigo', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 15:35:25', 'ind_abrebiatura' => 'GPS', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '5','cod_detalle' => 'I'),
            array('pk_num_concepto' => '19', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0012', 'ind_descripcion' => 'GARANTIA DE PRESTACIONES SOCIALES ADICIONALES', 'ind_impresion' => 'GARANTIA DE PRESTACIONES SOCIALES ADICIONALES', 'num_orden_planilla' => '1', 'ind_formula' => '//aqui tu Codigo', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 15:55:18', 'ind_abrebiatura' => 'GPA', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '5','cod_detalle' => 'I'),
            array('pk_num_concepto' => '20', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0013', 'ind_descripcion' => 'RETROACTIVO PRESTACIONES SOCIALES', 'ind_impresion' => 'RETROACTIVO PRESTACIONES SOCIALES', 'num_orden_planilla' => '1', 'ind_formula' => '//aqui tu Codigo', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 15:40:05', 'ind_abrebiatura' => 'RPS', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '5','cod_detalle' => 'I'),
            array('pk_num_concepto' => '21', 'fk_a006_num_tipo_concepto' => '4', 'cod_concepto' => '0014', 'ind_descripcion' => 'RETROACTIVO PRESTACIONES SOCIALES DIAS ADICIONALES', 'ind_impresion' => 'RETROACTIVO PRESTACIONES SOCIALES DIAS ADICIONALES', 'num_orden_planilla' => '1', 'ind_formula' => '//aqui tu Codigo', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-05 15:40:34', 'ind_abrebiatura' => 'RPA', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '5','cod_detalle' => 'I'),
            array('pk_num_concepto' => '22', 'fk_a006_num_tipo_concepto' => '7', 'cod_concepto' => '0015', 'ind_descripcion' => 'RETENCION CAJA DE AHORRO', 'ind_impresion' => 'RET CAJA DE AHORRO', 'num_orden_planilla' => '6', 'ind_formula' => '//aqui tu Codigo
$this->atMonto = ($this->metSueldoBasico()+$this->metDiferenciaSueldoBasico())*0.20;
$this->atCantidad = 1;', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-06 12:47:17', 'ind_abrebiatura' => 'RET CA', 'num_flag_automatico' => '0', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('pk_num_concepto' => '23', 'fk_a006_num_tipo_concepto' => '6', 'cod_concepto' => '0016', 'ind_descripcion' => 'APORTE CAJA DE AHORRO', 'ind_impresion' => 'APORTE CAJA DE AHORRO', 'num_orden_planilla' => '6', 'ind_formula' => '//aqui tu Codigo
$this->atMonto = ($this->metSueldoBasico()+$this->metDiferenciaSueldoBasico())*0.40;
$this->atCantidad = 1;', 'num_estatus' => '1', 'fec_ultima_moficacion' => '2016-12-06 08:15:14', 'ind_abrebiatura' => 'AP CA', 'num_flag_automatico' => '1', 'num_flag_bono' => '0', 'num_flag_incidencia' => '0', 'num_flag_jubilacion' => NULL, 'fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            );
        return $nm_b002_concepto;
    }

}
