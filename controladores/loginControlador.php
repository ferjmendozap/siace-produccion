<?php
require_once ROOT. 'sistema' . DS . 'control'. DS .'dbLdap.php';
class loginControlador extends Controlador
{
    private $atloginModelo;
    private $atParametrosModelo;
    private $atLogsModelo;
    private $atDbLdap;

    public function __construct()
	{
		parent::__construct();
        $this->atloginModelo=$this->metCargarModelo('login');
        $this->atLogsModelo=$this->metCargarModelo('logs');
        $this->atParametrosModelo=$this->metCargarModelo('parametros');
        $this->atDbLdap=new conexLdap();
	}

	public function metIndex()
	{
        Session::metNoAccesoSiLogueo();
        $this->atVista->metRenderizar('login','login');
	}

    /**
     * Actualizado por  : Alexis Ontiveros, 11 Ago 2017.
     * Descripción      : Permite al usuario validarse mediante el siace ó a través del LDAP para entrar al sistema.
     */
    public function metLoginCheck(){
        Session::metNoAccesoSiLogueo();
        $form=$this->metObtenerTexto('login');
        $validarUsuario='';
        if(!lOGINLDAP){//Si entra, se loguéa a través del siace. lOGINLDAP: variable establecida en el config.
            if(!$validarUsuario=$this->atloginModelo->metValidarUsuario($form['usuario'],$form['password'])){
                $Msj='Usuario o Contraseña erróneos';
            }
        }else{//De lo contrario se loguéa en el LDAP.
            if($conex=$this->atDbLdap->metLdapConexion()) {
                //Se valida el usuario
                if ($login = $this->atDbLdap->matLdapVerificaUsuario($form['usuario'], $form['password'], $conex)) {
                    //Se extrae el número de cédula del usuario en el ldap.
                    $campos = array("pager");
                    if ($cedulaUser = $this->atDbLdap->metLdapBuscaDataUser($form['usuario'], $conex, $campos)[0]) {
                        //Se extrae los datos del usuario en el siace.
                        if (!$validarUsuario = $this->atloginModelo->metBuscaDataUsuario($cedulaUser)) {
                            $Msj = '¡Atención!, no se encontró sus datos como usuario del sistema';
                        }
                    } else {
                        $Msj = 'Disculpe, hubo un error de conexión con el LDAP';
                    }
                    $this->atDbLdap->metLdapCerrar($conex);
                } else {
                    $Msj = 'Usuario o Contraseña erróneos para LDAP';
                }
            }else{
                $Msj = 'No hay conexión con el LDAP';
            }
        }
        if($validarUsuario){
            Session::metCrear('codigoUnico', Hash::metObtenerHash($validarUsuario['ind_usuario'].$validarUsuario['pk_num_seguridad_usuario'].Session::metObtenerIp()));
            Session::metCrear('usuario', $validarUsuario['ind_usuario']);
            Session::metCrear('idUsuario', $validarUsuario['pk_num_seguridad_usuario']);
            Session::metCrear('idEmpleado', $validarUsuario['pk_num_empleado']);
            Session::metCrear('idPersona', $validarUsuario['pk_num_persona']);
            if($validarUsuario['ind_foto']!=null){
                Session::metCrear('imagenUsuario', BASE_URL . 'publico/imagenes/modRH/fotos/'.$validarUsuario['ind_foto']);
            }else{
                if($validarUsuario['cod_detalle']!='F'){
                    Session::metCrear('imagenUsuario', BASE_URL . 'publico/imagenes/TemaBasico/avatar/avatar-hombre.png');
                }else{
                    Session::metCrear('imagenUsuario', BASE_URL . 'publico/imagenes/TemaBasico/avatar/avatar-mujer.png');
                }
            }
            Session::metCrear('idEmpleado', $validarUsuario['pk_num_empleado']);
            if(!isset($validarUsuario['ind_nombre2']) || $validarUsuario['ind_nombre2']==''){
                $validarUsuario['ind_nombre2']=' ';
            }
            if(!isset($validarUsuario['ind_apellido1']) || $validarUsuario['ind_apellido1']==''){
                $validarUsuario['ind_apellido1']=' ';
            }
            if(!isset($validarUsuario['ind_apellido2']) || $validarUsuario['ind_apellido2']=='') {
                $validarUsuario['ind_apellido2']=' ';
            }
            Session::metCrear('nombreUsuario', $validarUsuario['ind_nombre1'].' '.$validarUsuario['ind_nombre2'][0].'. '.$validarUsuario['ind_apellido1'].' '.$validarUsuario['ind_apellido2'][0].'.');
            $parametros=$this->atParametrosModelo->metListarParametros(true);
            for($i=0;$i<count($parametros);$i++){
                Session::metCrear($parametros[$i]['ind_parametro_clave'], $parametros[$i]['ind_valor_parametro']);
            }
            Session::metCrear('tiempo', time());
            Session::metCrear('ip',Session::metObtenerIp());
            $this->atloginModelo->metActualizazStatus($validarUsuario['ind_usuario'],Session::metObtener('ip'));
            $this->atLogsModelo->metRegistrarLogs(Session::metObtener('idUsuario'),9,'Session', Session::metObtener('ip'));
            $arrayUsuario=array(
                'status'=>'valido'
            );
        }else{
            $arrayUsuario=array(
                'status'=>$Msj
            );
        }
        echo json_encode($arrayUsuario);
    }

    public function metLoginCerrarSesion()
    {
        $this->atLogsModelo->metRegistrarLogs(Session::metObtener('idUsuario'),10,'Session', Session::metObtener('ip'));
        $this->atloginModelo->metActualizazStatus(Session::metObtener('usuario'),'');
        Session::metCerrarSesion();
        $this->metRedireccionar('login');
    }
}
