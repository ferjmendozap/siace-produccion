<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        29-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class miscelaneoControlador extends Controlador
{
    private $atMiscelaneoModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atMiscelaneoModelo = $this->metCargarModelo('miscelaneo');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $filtro = $this->metObtenerInt('filtro');
        if ($filtro === 1) {
            $formInt = $this->metObtenerInt('form', 'int');
            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    if ($tituloInt != 'num_estatus') {
                        $validacion[$tituloInt] = 'error';
                    } else {
                        $validacion[$tituloInt] = '0';
                    }
                }
            }
            $listado = $this->atMiscelaneoModelo->metListarMiscelaneos($validacion['fk_a015_num_seguridad_aplicacion'], $validacion['num_estatus']);
            echo json_encode($listado);
            exit;
        } else {
            $listado = $this->atMiscelaneoModelo->metListarMiscelaneos();
        }

        $this->atVista->assign('aplicaciones', $this->atMiscelaneoModelo->metListarAplicacion());
        $this->atVista->assign('listado', $listado);
        $this->atVista->metRenderizar('listado');
    }

    public function metCrear()
    {

        $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $idMiscelaneo = $this->metObtenerInt('idMiscelaneo');
        $valido = $this->metObtenerInt('valido');
        if ($valido === 1) {
            $excepcion = array('num_estatus');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'txt');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);
            if ($alphaNum != null && $ind == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($alphaNum, $ind);
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = 0;
            }

            if (!isset($validacion['num_estatusDet'])) {
                $validacion['num_estatusDet'] = false;
            }

            if ($idMiscelaneo == 0) {
                $validacion['status'] = 'nuevo';
                $id = $this->atMiscelaneoModelo->metCrearMiscelaneo($validacion['ind_nombre_maestro'], $validacion['ind_descripcion'],
                    $validacion['cod_maestro'], $validacion['fk_a015_num_seguridad_aplicacion'],
                    $validacion['num_estatus'], $validacion['ind_nombre_detalle'], $validacion['num_estatusDet'], $validacion['cod_detalle']);
            } else {
                $validacion['status'] = 'modificar';
                $id = $this->atMiscelaneoModelo->metActualizarMiscelaneo($idMiscelaneo, $validacion['ind_nombre_maestro'], $validacion['ind_descripcion'],
                    $validacion['cod_maestro'], $validacion['fk_a015_num_seguridad_aplicacion'], $validacion['num_estatus'],
                    $validacion['ind_nombre_detalle'], $validacion['num_estatusDet'], $validacion['pk_num_miscelaneo_detalle'], $validacion['cod_detalle']);
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])){
                        if (strpos($id[2], (string)$validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idMiscelaneo'] = $id;
            echo json_encode($validacion);
            exit;
        }
        if ($idMiscelaneo != 0) {
            $miscelaneo = $this->atMiscelaneoModelo->metMostrarMiscelaneo($idMiscelaneo);
            $miscelaneoDetalle = $this->atMiscelaneoModelo->metMostrarSelect($miscelaneo['cod_maestro']);
            $this->atVista->assign('numero', 1);
            $this->atVista->assign('n', 0);
            $this->atVista->assign('formDB', $miscelaneo);
            $this->atVista->assign('formDBSelect', $miscelaneoDetalle);
        }
        $this->atVista->assign('aplicacion', $this->atMiscelaneoModelo->metListarAplicacion());
        $this->atVista->assign('idMiscelaneo', $idMiscelaneo);
        $this->atVista->metRenderizar('crear', 'modales');
    }

}
