<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class parametrosControlador extends Controlador
{
    private $atParametrosModelo;
    private $atMiscelaneosModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atParametrosModelo = $this->metCargarModelo('parametros');
        $this->atMiscelaneosModelo = $this->metCargarModelo('miscelaneo');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->atParametrosModelo->metListarParametros());
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $idParametro = $this->metObtenerInt('idParametro');
        $valido = $this->metObtenerInt('valido');
        if ($valido == 1) {
            $this->metValidarToken();
            $excepcion = array('num_estatus','ind_explicacion');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'txt', $excepcion);
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);
            if ($alphaNum != null && $ind == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($alphaNum, $ind);
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = 0;
            }

            if($idParametro==0){
                $id=$this->atParametrosModelo->metRegistrarParametro($validacion['num_estatus'], $validacion['fk_a006_num_miscelaneo_detalle'],$validacion['fk_a015_num_seguridad_aplicacion'],$validacion['ind_descripcion'], $validacion['ind_explicacion'],$validacion['ind_parametro_clave'], $validacion['ind_valor_parametro']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atParametrosModelo->metModificarParametro($validacion['num_estatus'], $validacion['fk_a006_num_miscelaneo_detalle'],$validacion['fk_a015_num_seguridad_aplicacion'],$validacion['ind_descripcion'], $validacion['ind_explicacion'],$validacion['ind_parametro_clave'], $validacion['ind_valor_parametro'], $idParametro);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idParametro'] = $id;

            echo json_encode($validacion);
            exit;
        }

        if ($idParametro != 0) {
            $this->atVista->assign('formDB', $this->atParametrosModelo->metMostrarParametro($idParametro));
        }
        $this->atVista->assign('idParametro', $idParametro );
        $this->atVista->assign('aplicaciones', $this->atParametrosModelo->metListarAplicacion());
        $this->atVista->assign('miscelaneo', $this->atMiscelaneosModelo->metMostrarSelect('TPPARAMET'));
        $this->atVista->metRenderizar('crear', 'modales');
    }

}