<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                |          TELEFONO              |
 * | 1 |            Deivis  Millan                |d.millan@contraloriamonagas.gob.ve   |         04269372700            |
 * |   |                                         |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class errorControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();

    }

    public function metIndex()
    {
        $check=$this->metObtenerInt('verificacion');
        if($check) {
            Session::metAcceso();
            Session::metCerrarSesion();
            echo '1';
        }

    }

    public function metError($codigoError)
    {
        if($codigoError=='404'){#No Encontrado
            $img='error.png';
            $titulo='Sitio No Encontrado';
        }elseif($codigoError=='401'){#Fin de sesion por inactividad
            $img='error_2.png';
            $titulo='Sesión Caducada';
        }elseif($codigoError=='402'){#Error de Vista
            $img='error_2.png';
            $titulo='Error de Vista';
        }elseif($codigoError=='403'){#Error de Modelo
            $img='error_2.png';
            $titulo='Error de Modelo';
        }

        $this->atVista->assign('img',$img);
        $this->atVista->assign('titulo',$titulo);
        $this->atVista->metRenderizar('error','errores');


    }








}
