<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |  lbastardo@contraloriadebolivar.gob.ve |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class indicepresupuestarioControlador extends Controlador
{
    private $atIndicePresupuestario;
	
    public function __construct()
    {
        parent::__construct();
        $this->atIndicePresupuestario=$this->metCargarModelo('indicepresupuestario');
		$this->atPresupuesto=$this->metCargarModelo('presupuesto');
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atPartida=$this->metCargarModelo('partida','maestros');
		
		$this->atSector=$this->metCargarModelo('sector','maestros');
		$this->atPrograma=$this->metCargarModelo('programa','maestros');
		$this->atSubPrograma=$this->metCargarModelo('subprograma','maestros');
		$this->atProyecto=$this->metCargarModelo('proyecto','maestros');
		$this->atActividad=$this->metCargarModelo('actividad','maestros');
		$this->atUnidadEjecutora=$this->metCargarModelo('unidadejecutora','maestros');
		$this->atAespecifica=$this->metCargarModelo('aespecifica','maestros');
		$this->atAcentralizada=$this->metCargarModelo('acentralizada','maestros');
		 
    }


    public function metIndex($status=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
			
        );
		$js= array('materialSiace/core/demo/DemoTableDynamic','ModPR/prFunciones');
		
		
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atIndicePresupuestario->metListarIndicePresupuestario($status));
		$this->atVista->assign('status', $status);
        $this->atVista->metRenderizar('listado');
    }


    public function metJsonProyecto()
    {

        $sector = $this->metObtenerInt('sector');
		$filtro  = $this->atProyecto->metMostrarProyectoSelect($sector);
        echo json_encode($filtro);
        exit;

    }
	
	
	public function metJsonSubPrograma()
    {

        $programa = $this->metObtenerInt('programa');
		$filtro  = $this->atSubPrograma->metMostrarSubprogramaSelect($programa);
		if ($filtro)
           echo json_encode($filtro);
        exit;

    }


    public function metJsonActividad()
    {

        $proyecto = $this->metObtenerInt('proyecto');
		$filtro  = $this->atActividad->metMostrarActividadSelect($proyecto);
        echo json_encode($filtro);
        exit;

    }
	
    
	public function metJsonCategoria()
    {

        $tipo_presupuesto = $this->metObtenerTexto('tipo_presupuesto');
		$filtro  = $this->atIndicePresupuestario->metMostrarCategoriaSelect($tipo_presupuesto);
        echo json_encode($filtro);
        exit;

    }


    public function metCrearModificar()
    {
	
	   $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
        );
	

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
	
        $valido=$this->metObtenerInt('valido');
        $idIndicePresupuestario=$this->metObtenerInt('idIndicePresupuestario');
		$status = $this->metObtenerAlphaNumerico('status');
		$suma_total=0;	 
		
		
        if($valido==1){
		
		
	  //  if($status == 'PR') {
           $this->metValidarToken();
         // $Excceccion=array('fk_prb008_num_sector', 'fk_prb009_num_programa', 'fk_prb010_num_subprograma', 'fk_prb005_num_proyecto', 'fk_prb011_num_actividad');
		  $Excceccion=array('');
		   
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			$formTxt=$this->metObtenerTexto('form','txt',$Excceccion);
				
		 
	   foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{				  
				 /*  if (in_array($tituloAlphaNum, $Excceccion ) )
					  $validacion[$tituloAlphaNum]='';
				    else	*/	
                      $validacion[$tituloAlphaNum]='error';

                }
            }
		 
		 
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
			
			
			 if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
			
			if ($ind)
			{
			   foreach ($ind as $tituloInt => $valorInt) {
                  if(!empty($ind[$tituloInt]) && $ind[$tituloInt]>0 ){
                      $validacion[$tituloInt]=$valorInt;
                  }else{
                        $validacion[$tituloInt]='0';
                  }
                }
			 } 
			
			if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }
			
		if($status == 'PR') {	
			
             if($idIndicePresupuestario==0){
					
				 $id=$this->atIndicePresupuestario->metCrearIndicePresupuestario($validacion['ind_tipo_presupuesto'], $validacion['ind_tipo_categoria'], $validacion['fk_a001_num_organismo'],$validacion['fk_prb008_num_sector'], $validacion['fk_prb009_num_programa'], $validacion['fk_prb010_num_subprograma'], $validacion['fk_prb005_num_proyecto'], $validacion['fk_prb011_num_actividad'],  $validacion['fk_prb012_unidad_ejecutora'], $validacion['num_estatus']);
				 	
						$validacion['status']='nuevo';
				
              }else{
				
				$id=$this->atIndicePresupuestario->metModificarIndicePresupuestario($validacion['ind_tipo_presupuesto'], $validacion['ind_tipo_categoria'], $validacion['fk_a001_num_organismo'],$validacion['fk_prb008_num_sector'], $validacion['fk_prb009_num_programa'], $validacion['fk_prb010_num_subprograma'], $validacion['fk_prb005_num_proyecto'], $validacion['fk_prb011_num_actividad'],$validacion['fk_prb012_unidad_ejecutora'], $validacion['num_estatus'], $idIndicePresupuestario);     
					
						  $validacion['status']='modificar';
						
			   }
			
			}	
		    
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idIndicePresupuestario']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idIndicePresupuestario!=0){
		   
            $this->atVista->assign('formDB',$this->atIndicePresupuestario->metMostrarIndicePresupuestario($idIndicePresupuestario));
            $this->atVista->assign('idIndicePresupuestario',$idIndicePresupuestario);
				
        }
		
		
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());
		
		$this->atVista->assign('sector',$this->atSector->metListarSector());
		$this->atVista->assign('programa',$this->atPrograma->metListarPrograma());
		$this->atVista->assign('subprograma',$this->atSubPrograma->metListarSubprograma());
		$this->atVista->assign('proyecto',$this->atProyecto->metListarProyecto());
		$this->atVista->assign('actividad',$this->atActividad->metListarActividad());
		$this->atVista->assign('unidadejecutora',$this->atUnidadEjecutora->metListarUnidadEjecutora());
		$this->atVista->assign('aespecifica',$this->atAespecifica->metListarAespecifica());
		$this->atVista->assign('acentralizada',$this->atAcentralizada->metListarAcentralizada());
		
		$this->atVista->assign('status',$status);
        $this->atVista->metRenderizar('CrearModificar','modales');
		
    }

   
    public function metEliminar()
    {
        $idIndicePresupuestario = $this->metObtenerInt('idIndicePresupuestario');
        if($idIndicePresupuestario!=0){
            $id=$this->atIndicePresupuestario->metEliminarIndicePresupuestario($idIndicePresupuestario);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Presupuesto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idIndicePresupuestario'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	
	
	protected function FormatoFecha($fecha)
    {
        $resultado=explode("-",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }	
	
	
	protected function FormatoMonto($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
	
}