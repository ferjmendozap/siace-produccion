<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |  lbastardo@contraloriadebolivar.gob.ve |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class presupuestoControlador extends Controlador
{
    private $atPresupuesto;
	
    public function __construct()
    {
        parent::__construct();
        $this->atPresupuesto=$this->metCargarModelo('presupuesto');
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atPartida=$this->metCargarModelo('partida','maestros');
		
		$this->atSector=$this->metCargarModelo('sector','maestros');
		$this->atPrograma=$this->metCargarModelo('programa','maestros');
		$this->atSubPrograma=$this->metCargarModelo('subprograma','maestros');
		$this->atProyecto=$this->metCargarModelo('proyecto','maestros');
		$this->atActividad=$this->metCargarModelo('actividad','maestros');
		$this->atUnidadEjecutora=$this->metCargarModelo('unidadejecutora','maestros');
		$this->atAespecifica=$this->metCargarModelo('aespecifica','maestros');
		$this->atAcentralizada=$this->metCargarModelo('acentralizada','maestros');
		 
    }


    public function metIndex($status=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
			
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atPresupuesto->metListarPresupuesto($status));
		$this->atVista->assign('status', $status);
        $this->atVista->metRenderizar('listado');
    }



    public function metCrearModificar()
    {
	
	   $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
        );
	

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
	
        $valido=$this->metObtenerInt('valido');
        $idPresupuesto=$this->metObtenerInt('idPresupuesto');
		$status = $this->metObtenerAlphaNumerico('status');
		$suma_total=0;	 
		
		
        if($valido==1){
		
		
	    if($status == 'PR') {
           $this->metValidarToken();
           $Excceccion=array('num_monto_generado','monto','ind_numero_gaceta','ind_numero_decreto','fec_decreto','fec_gaceta');
		
			 
            
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			$formTxt=$this->metObtenerTexto('form','txt',$Excceccion);
			
		
		 
		  foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='';

                }
            }
		 
		 
		   foreach ($ind as $tituloInt => $valorInt) {
                if(!empty($ind[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='0,00';
                }
            }
		 
		 
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
			
			
			 if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
			
			
			
            if(!isset($validacion['fk_prb002_num_partida_presupuestaria'])){
                $validacion['fk_prb002_num_partida_presupuestaria']=false;
            }
			
			if(!isset($validacion['monto'])){
                $validacion['monto']=false;
            }
			
			
			$validacion['num_monto_generado']=$this->FormatoMonto($validacion['num_monto_generado']); 
			$validacion['num_monto_presupuestado']=$this->FormatoMonto($validacion['num_monto_presupuestado']); 
			
			$validacion['fec_gaceta']=$this->FormatoFecha($validacion['fec_gaceta']); 
		    $validacion['fec_decreto']=$this->FormatoFecha($validacion['fec_decreto']); 	
		    $validacion['fec_presupuesto']=$this->FormatoFecha($validacion['fec_presupuesto']); 	
			$validacion['fec_inicio']=$this->FormatoFecha($validacion['fec_inicio']); 
			$validacion['dec_fin']=$this->FormatoFecha($validacion['dec_fin']); 	
			}
			 elseif($status=='AP' || $status =='GE'){
                $id = $this->atPresupuesto->metAccionesPresupuesto($idPresupuesto,$status);
                if($status=='AP'){
                    $validacion['status'] = 'aprobado';
                }else if($status=='GE'){
                    $validacion['status'] = 'generado';
                }
            }

             
		    
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idPresupuesto']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idPresupuesto!=0){
		   
            $this->atVista->assign('formDB',$this->atPresupuesto->metMostrarPresupuesto($idPresupuesto));
            $this->atVista->assign('idPresupuesto',$idPresupuesto);
			
			
			//AGREGADO PARA MOSTRAR LAS PARTIDAS--------------------
			 $partidaDetalle = $this->atPresupuesto->metMostrarPresupuestoDetalle($idPresupuesto);
			 
			 $partidaTitulo = $this->atPresupuesto->metMostrarPresupuestoTitulo();
			 
			 $suma_compromiso=0; 
			 $suma2_compromiso=0; 
			 $suma3_compromiso=0; 
			 $suma_total_compromiso=0;
			 
			 $suma_causado=0; 
			 $suma2_causado=0; 
			 $suma3_causado=0; 
			 $suma_total_causado=0;
			 
			 $suma_pagado=0; 
			 $suma2_pagado=0; 
			 $suma3_pagado=0; 
			 $suma_total_pagado=0;
			 
			 
			 //////////////////////////////////////////PRESUPUESTO POR PROYECTO///////////////////////////////////////////////
          
	if($partidaDetalle)
     if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
     {	
		     
			 $aespecifica = $this->atPartida->metAespecifica();
		     
			 $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma3=0;
			 $suma_total=0;
			 
			 $f=0;
			 $p=0;
			 $c=0;
			 $cuenta=0;
		
		
	    for($a=0;$a<count($aespecifica);$a++){
	    
	        $suma=0;
			$suma2=0;
			$suma3=0;
			$existeAespecifica=0;
			
			
			 $existeAespecifica = count($this->atPresupuesto->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $idPresupuesto));
				 
			if ($existeAespecifica>0)
			{
		        $partidas[$cuenta]=array(
                        'id'=>$aespecifica[$a]['pk_num_aespecifica'],
                        'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
                        'partida'=>$aespecifica[$a]['ind_descripcion'],
						'monto'=>0,
						'tipo'=>'E',
						'aespecifica'=>''
                    );
					
					//echo $aespecifica[$a]['pk_num_aespecifica']; echo '-';
			 }
			
				  
		 for($i=0;$i<count($partidaDetalle);$i++){	
		
		
		     if($f==0){
		        $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			    $f=1;
			  }  
		
		     if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		        $suma=0;
				$suma_compromiso=0;
				$suma_causado=0;
				$suma_pagado=0;
			    $f=0;
		     }
		   
		   
		     if($p==0){
		        $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			    $p=1;
			 }  
		
		     if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		       $suma2=0;
			   $suma2_compromiso=0;
			   $suma2_causado=0;
			   $suma2_pagado=0;
			   $p=0;
		     }
		   
		   ///////////////////
		     if($c==0){
		       $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			   $c=1;
			 }  
		
		     if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		       $suma3=0;
			   $suma3_compromiso=0;
			   $suma3_causado=0;
			   $suma3_pagado=0;
			   $c=0;
		     }
		   
		   /////////////////
		   
		  if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr006_num_aespecifica'])
		  { 
			 $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']; 
			 $suma3=$suma3+$partidaDetalle[$i]['num_monto_ajustado']; 
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado'];
			 
			//--------------------------COMPROMISO----------------------------------// 
			 
			 $suma_compromiso=$suma_compromiso+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2_compromiso=$suma2_compromiso+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma3_compromiso=$suma3_compromiso+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma_total_compromiso=$suma_total_compromiso+$partidaDetalle[$i]['num_monto_compromiso'];
			 
			 
			 //--------------------------CAUSADO----------------------------------// 
			 
			 $suma_causado=$suma_causado+$partidaDetalle[$i]['num_monto_causado']; 
			 $suma2_causado=$suma2_causado+$partidaDetalle[$i]['num_monto_causado']; 
			 $suma3_causado=$suma3_causado+$partidaDetalle[$i]['num_monto_causado']; 
			 $suma_total_causado=$suma_total_causado+$partidaDetalle[$i]['num_monto_causado'];
			 
			 
			 //--------------------------PAGADO----------------------------------// 
			 
			 $suma_pagado=$suma_pagado+$partidaDetalle[$i]['num_monto_pagado']; 
			 $suma2_pagado=$suma2_pagado+$partidaDetalle[$i]['num_monto_pagado']; 
			 $suma3_pagado=$suma3_pagado+$partidaDetalle[$i]['num_monto_pagado']; 
			 $suma_total_pagado=$suma_total_pagado+$partidaDetalle[$i]['num_monto_pagado'];
			
			 	   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			 
			    $cuenta=$cuenta+1;
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
			    
				$existe=0;
				$posnum = 0;
				$pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
				 				  
				
		if ($existe==1)		
		{
		
		  //---------------------------------------------	
			   if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'monto_compromiso'=>$suma2_compromiso,
						'monto_causado'=>$suma2_causado,
						'monto_pagado'=>$suma2_pagado,
						'monto_disponible'=>$suma2-$suma2_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
				   
				  }
								
			 //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'monto_compromiso'=>$suma_compromiso,
						'monto_causado'=>$suma_causado,
						'monto_pagado'=>$suma_pagado,
						'monto_disponible'=>$suma-$suma_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
					
                    );
				   
				  }
				  
				  //-----------------------------------------------	  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'monto_compromiso'=>$suma3_compromiso,
						'monto_causado'=>$suma3_causado,
						'monto_pagado'=>$suma3_pagado,
						'monto_disponible'=>$suma3-$suma3_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
					
                    );
				   
				  }
		}
		else//EXISTE
		{		
				
			//---------------------------------------------	
			   if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'monto_compromiso'=>$suma2_compromiso,
						'monto_causado'=>$suma2_causado,
						'monto_pagado'=>$suma2_pagado,
						'monto_disponible'=>$suma2-$suma2_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
					
                    );
				   
				  }
				
				
			 //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'monto_compromiso'=>$suma_compromiso,
						'monto_causado'=>$suma_causado,
						'monto_pagado'=>$suma_pagado,
						'monto_disponoble'=>$suma-$suma_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
					
                    );
				   
				  }
				  
		   //-----------------------------------------------	  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'monto_compromiso'=>$suma3_compromiso,
						'monto_causado'=>$suma3_causado,
						'monto_pagado'=>$suma3_pagado,
						'monto_disponible'=>$suma3-$suma3_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
					
                    );
				   
				  }
				 
			   
			 }//FIN IF EXISTE
			
			} 
			 
			        $cuenta=$cuenta+1;
                    $partidas[$cuenta]=array(
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto_compromiso'=>$partidaDetalle[$i]['num_monto_compromiso'],
						'monto_causado'=>$partidaDetalle[$i]['num_monto_causado'],
						'monto_pagado'=>$partidaDetalle[$i]['num_monto_pagado'],
						'monto_disponible'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
			  	
                  }//FIN IF AESPECIFICA
			 
			 
			  }//FIN DETALLE
			 $cuenta=$cuenta+1;
		    }//FIN FOR ESPECIFICA
		 
		  
		  }//FIN PRESUPUESTO POR PROYECTO
		  else
		  {
		 
			 $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma3=0;
			 $suma_total=0;
			 
			 $f=0;
			 $p=0;
			 $c=0;
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		
		     if($f==0){
		        $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			    $f=1;
			  }  
		
		      if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		        $suma=0;
				$suma_compromiso=0;
				$suma_causado=0;
			    $suma_pagado=0;
			    $f=0;
		      }
		   
		   
		      if($p==0){
		        $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			    $p=1;
			  }  
		
		      if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		        $suma2=0;
				$suma2_compromiso=0;
				$suma2_causado=0;
			    $suma2_pagado=0;
			    $p=0;
		     }
		   
		   
		   ///////////////////
		      if($c==0){
		        $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			    $c=1;
			  }  
		
		      if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		        $suma3=0;
				$suma3_compromiso=0;
				$suma3_causado=0;
			    $suma3_pagado=0;
			    $c=0;
		      }
		   
		   /////////////////
		   
		    
			 $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']; 
			 $suma3=$suma3+$partidaDetalle[$i]['num_monto_ajustado']; 
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado'];
			 
			 //--------------------------COMPROMISO----------------------------------// 
			 
			 $suma_compromiso=$suma_compromiso+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2_compromiso=$suma2_compromiso+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma3_compromiso=$suma3_compromiso+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma_total_compromiso=$suma_total_compromiso+$partidaDetalle[$i]['num_monto_compromiso'];
			 
			 
			  //--------------------------CAUSADO----------------------------------// 
			 
			 $suma_causado=$suma_causado+$partidaDetalle[$i]['num_monto_causado']; 
			 $suma2_causado=$suma2_causado+$partidaDetalle[$i]['num_monto_causado']; 
			 $suma3_causado=$suma3_causado+$partidaDetalle[$i]['num_monto_causado']; 
			 $suma_total_causado=$suma_total_causado+$partidaDetalle[$i]['num_monto_causado'];
			 
			 
			 //--------------------------PAGADO----------------------------------// 
			 
			 $suma_pagado=$suma_pagado+$partidaDetalle[$i]['num_monto_pagado']; 
			 $suma2_pagado=$suma2_pagado+$partidaDetalle[$i]['num_monto_pagado']; 
			 $suma3_pagado=$suma3_pagado+$partidaDetalle[$i]['num_monto_pagado']; 
			 $suma_total_pagado=$suma_total_pagado+$partidaDetalle[$i]['num_monto_pagado'];
			
			
			 	   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'monto_compromiso'=>$suma2_compromiso,
						'monto_causado'=>$suma2_causado,
						'monto_pagado'=>$suma2_pagado,
						'monto_disponible'=>$suma2-$suma2_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				
				
			 //-----------------------------------------------
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'monto_compromiso'=>$suma_compromiso,
						'monto_causado'=>$suma_causado,
						'monto_pagado'=>$suma_pagado,
						'monto_disponible'=>$suma-$suma_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				  
				  //-----------------------------------------------	  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'monto_compromiso'=>$suma3_compromiso,
						'monto_causado'=>$suma3_causado,
						'monto_pagado'=>$suma3_pagado,
						'monto_disponible'=>$suma3-$suma3_compromiso,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				 
			    
			 }
			        
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto_compromiso'=>$partidaDetalle[$i]['num_monto_compromiso'],
						'monto_causado'=>$partidaDetalle[$i]['num_monto_causado'],
						'monto_pagado'=>$partidaDetalle[$i]['num_monto_pagado'],
						'monto_disponible'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo']
                    );
					
                
            }
			
			
			}//FIN ELSE TIPO PRESUPUESTO
			 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
				
        }
		$this->atVista->assign('suma_total',$suma_total);
		$this->atVista->assign('suma_total_compromiso',$suma_total_compromiso);
		$this->atVista->assign('suma_total_causado',$suma_total_causado);
		$this->atVista->assign('suma_total_pagado',$suma_total_pagado);
		$this->atVista->assign('suma_total_disponible',$suma_total-$suma_total_compromiso);
		
        $this->atVista->assign('cuenta',$this->atPresupuesto->metCuentaListar());
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());
		
		$this->atVista->assign('sector',$this->atSector->metListarSector());
		$this->atVista->assign('programa',$this->atPrograma->metListarPrograma());
		$this->atVista->assign('subprograma',$this->atSubPrograma->metListarSubprograma());
		$this->atVista->assign('proyecto',$this->atProyecto->metListarProyecto());
		$this->atVista->assign('actividad',$this->atActividad->metListarActividad());
		$this->atVista->assign('unidadejecutora',$this->atUnidadEjecutora->metListarUnidadEjecutora());
		$this->atVista->assign('aespecifica',$this->atAespecifica->metListarAespecifica());
		$this->atVista->assign('acentralizada',$this->atAcentralizada->metListarAcentralizada());
		
		$this->atVista->assign('status',$status);
        $this->atVista->metRenderizar('CrearModificar','modales');
		
    }

   

    public function metEliminar()
    {
        $idPresupuesto = $this->metObtenerInt('idPresupuesto');
        if($idPresupuesto!=0){
            $id=$this->atPresupuesto->metEliminarPresupuesto($idPresupuesto);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Presupuesto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idPresupuesto'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	
	
	protected function FormatoFecha($fecha)
    {
        $resultado=explode("-",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }	
	
	
	protected function FormatoMonto($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
	
}