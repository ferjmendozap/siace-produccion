<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |  lbastardo@contraloriadebolivar.gob.ve |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/


class tipoCuentaControlador extends Controlador
{
    private $atTipoCuenta;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoCuenta=$this->metCargarModelo('tipoCuenta','maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atTipoCuenta->metListarTipoCuenta());
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idCuenta=$this->metObtenerInt('idCuenta');
		
		
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
           
            
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idCuenta==0){
                $id=$this->atTipoCuenta->metCrearTipoCuenta($validacion['cod_tipo_cuenta'],$validacion['ind_descripcion'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoCuenta->metModificarTipoCuenta($validacion['cod_tipo_cuenta'],$validacion['ind_descripcion'],$validacion['num_estatus'],$idCuenta);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idCuenta']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idCuenta!=0){
            $this->atVista->assign('formDB',$this->atTipoCuenta->metMostrarTipoCuenta($idCuenta));
			
			//var_dump($this->atTipoCuenta->metMostrarTipoCuenta($idCuenta));
			//exit;
            $this->atVista->assign('idCuenta',$idCuenta);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');
    }


    public function metEliminar()
    {
        $idCuenta = $this->metObtenerInt('idCuenta');
        if($idCuenta!=0){
            $id=$this->atTipoCuenta->metEliminarTipoCuenta($idCuenta);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la cuenta se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idCuenta'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}