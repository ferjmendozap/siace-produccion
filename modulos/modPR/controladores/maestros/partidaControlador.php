<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                 lbastardo@contraloriadebolivar.gob.ve   |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class partidaControlador extends Controlador
{
    private $atPartida;
	
    public function __construct()
    {
        parent::__construct();
        $this->atPartida=$this->metCargarModelo('partida','maestros');
    }

	
	/**
	*Funcion indice de partidas presupuestarias
	*
	*Parametro: $status=false
	*Return: N/A
	*
	**/
	public function metIndex($lista=false)
    {
	
       $complementosCss = array(
                'DataTables/jquery.dataTables',
                'DataTables/extensions/dataTables.colVis941e',
                'DataTables/extensions/dataTables.tableTools4029',
            ); 	
      
	   $js = array('materialSiace/core/demo/DemoTableDynamic','ModPR/prFunciones');
	   
	   $this->atVista->metCargarCssComplemento($complementosCss);
       $this->atVista->metCargarJs($js);		
      // $this->atVista->assign('listado',$this->atPartida->metListarPartida());
       $this->atVista->metRenderizar('listado');
       
    }


    /**
	*Funcion que muestar listado de partidas en una modal
	*
	*Parametro: $partida
	*Return: N/A
	*
	**/
    public function metPartida($partida)
    {
        $this->atVista->assign('listado',$this->atPartida->metListarPartidas_egresos());
		$this->atVista->assign('aespecifica',$this->atPartida->metAespecifica());
       
        $this->atVista->metRenderizar('listadoModal','modales');
    }

   
    /**
	*Funcion que muestar listado de responsables por dependencia en una modal
	*
	*Parametro: $persona=false
	*Return: N/A
	*
	**/
     public function metResponsable($persona=false)
	{
	     $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
		
		  if(!$persona){
			$js[] = 'materialSiace/core/demo/DemoTableDynamic';
			$this->atVista->metCargarCssComplemento($complementosCss);
			$this->atVista->metCargarJs($js);
			}
	  	$this->atVista->assign('listado',$this->atPartida->metListarResponsable());
        if($persona){
		$this->atVista->metRenderizar('listadoEmpleado','modales');
	     }else{
		$this->atVista->metRenderizar('listado');
        }
    }


    public function metCentroCosto($parametro = false, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atPartida->metListaCC());
        $this->atVista->assign('parametro', $parametro);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoCC', 'modales');
    }

    public function metCuentas($idCampo)
    {       
	
	    $this->atVista->assign('idCampo', $idCampo);
		$this->atVista->assign('lista', $this->atPartida->metCuentaContableListar());
       
		
	    if ($idCampo=='PUB20')
		   $this->atVista->assign('lista', $this->atPartida->metCuentaContablePub20Listar());
		   
	    if ($idCampo=='ONCOP')
		   $this->atVista->assign('lista', $this->atPartida->metCuentaContableOncoListar());   
		   
	    if ($idCampo=='GASTO')
		   $this->atVista->assign('lista', $this->atPartida->metCuentaContablePub20Listar());
		   
		
          $this->atVista->metRenderizar('listadoCuentas', 'modales');
    }
     
	 
	 public function metDependencias($list=false)
    {
	
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
		
		// if(!$list){
       // $js =  array('materialSiace/core/demo/DemoTableDynamic');
        $this->atVista->metCargarCssComplemento($complementosCss);
        //$this->atVista->metCargarJs($js);
		//}
		
		$this->atVista->assign('listado',$this->atPartida->metListarTipoDependenciaInt());
       // if($list){
		$this->atVista->metRenderizar('listadoDepInterna','modales');
		//	}
		//else{  
		//$this->atVista->metRenderizar('listado');
       // }

    } 
	 
	 public function metPartidaPrograma($partida)
    {
        $this->atVista->assign('listado',$this->atPartida->metListarPartidas_egresos());
		$this->atVista->assign('aespecifica',$this->atPartida->metAespecifica());
       
        $this->atVista->metRenderizar('listadoModalPrograma','modales');
    }

    
     public function metPartidaGenerar($partida)
    {
        $this->atVista->assign('listado',$this->atPartida->metListarPartidas_egresos());
        
        $this->atVista->metRenderizar('listadoModalGenerar','modales');
    }



    public function metCrearModificar()
    {
	
	     $js =  array('materialSiace/core/demo/DemoTableDynamic','ModPR/prFunciones');
        $this->atVista->metCargarJs($js);
	
        $valido=$this->metObtenerInt('valido');
        $idPartida=$this->metObtenerInt('idPartida');
		//$valido=1;
		//$idPartida=1;
		
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus','fk_cbb004_num_plan_cuenta_pub20','fk_cbb004_num_plan_cuenta_onco','fk_cbb004_num_plan_cuenta_gastopub20','ind_numeral','cod_partida');
			
			 
            
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
           
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            } 
			
			 if(!isset($validacion['fk_cbb004_num_plan_cuenta_pub20'])){
                $validacion['fk_cbb004_num_plan_cuenta_pub20']=0;
            } 
			
			 if(!isset($validacion['fk_cbb004_num_plan_cuenta_onco'])){
                $validacion['fk_cbb004_num_plan_cuenta_onco']=0;
            } 
			
			if(!isset($validacion['fk_cbb004_num_plan_cuenta_gastopub20'])){
                $validacion['fk_cbb004_num_plan_cuenta_gastopub20']=0;
            } 

            if($idPartida==0){
			
			    $validacion['cod_partida']=$validacion['fk_prb001_num_tipo_cuenta'].$validacion['ind_partida'].'.'.$validacion['ind_generica'].'.'.$validacion['ind_especifica'].'.'.$validacion['ind_subespecifica'];
				if ($validacion['ind_numeral'] != null)
				    $validacion['cod_partida']=$validacion['cod_partida'].'.'.$validacion['ind_numeral'];
				 
				   
                $id=$this->atPartida->metCrearPartida($validacion['fk_prb001_num_tipo_cuenta'], $validacion['ind_partida'], $validacion['ind_generica'],$validacion['ind_especifica'],$validacion['ind_subespecifica'],$validacion['ind_numeral'], $validacion['cod_partida'],$validacion['ind_denominacion'],$validacion['ind_tipo'],$validacion['num_nivel'],$validacion['fk_cbb004_num_plan_cuenta_pub20'],$validacion['fk_cbb004_num_plan_cuenta_onco'],$validacion['fk_cbb004_num_plan_cuenta_gastopub20'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
			
			 $validacion['cod_partida']=$validacion['fk_prb001_num_tipo_cuenta'].$validacion['ind_partida'].'.'.$validacion['ind_generica'].'.'.$validacion['ind_especifica'].'.'.$validacion['ind_subespecifica'];
				if ($validacion['ind_numeral'] != null)
				    $validacion['cod_partida']=$validacion['cod_partida'].'.'.$validacion['ind_numeral'];

			
                $id=$this->atPartida->metModificarPartida($validacion['fk_prb001_num_tipo_cuenta'], $validacion['ind_partida'], $validacion['ind_generica'],$validacion['ind_especifica'],$validacion['ind_subespecifica'],$validacion['ind_numeral'], $validacion['cod_partida'],$validacion['ind_denominacion'],$validacion['ind_tipo'],$validacion['num_nivel'], $validacion['fk_cbb004_num_plan_cuenta_pub20'], $validacion['fk_cbb004_num_plan_cuenta_onco'], $validacion['fk_cbb004_num_plan_cuenta_gastopub20'], $validacion['num_estatus'], $idPartida);
				
				
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idPartida']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idPartida!=0){
		   
            $this->atVista->assign('formDB',$this->atPartida->metMostrarPartida($idPartida));
            $this->atVista->assign('idPartida',$idPartida);
        }
        $this->atVista->assign('cuenta',$this->atPartida->metCuentaListar());
		$this->atVista->assign('cuentacontable',$this->atPartida->metCuentaContableListar());
		$this->atVista->assign('cuentacontableOnco',$this->atPartida->metCuentaContableOncoListar());
		$this->atVista->assign('cuentacontablePub20',$this->atPartida->metCuentaContablePub20Listar());
        $this->atVista->metRenderizar('CrearModificar','modales');
    }



    public function metEliminar()
    {
        $idPartida = $this->metObtenerInt('idPartida');
        if($idPartida!=0){
            $id=$this->atPartida->metEliminarPartida($idPartida);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la partida se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idPartida'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	
	 public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM pr_b002_partida_presupuestaria ";

        if ($busqueda['value']) {
            $sql .= "WHERE
                    ( 
                      cod_partida LIKE '%$busqueda[value]%' OR 
                      ind_denominacion LIKE '%$busqueda[value]%' 
                    )
                    ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_partida','ind_denominacion','ind_tipo','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_partida_presupuestaria';
        #los valores del listado con flag 'md md-check'
        //$flags = array('num_flag_adelanto');
        #construyo el listado de botones
       if (in_array('PR-01-01-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                 <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                         data-keyboard="false" data-backdrop="static" idPartida="'.$clavePrimaria.'"
                         descipcion="El Usuario a Modificado una partida del sistema" titulo="Modificar Partida">
                         <i class="fa fa-edit" style="color: #ffffff;"></i>
                 </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('PR-01-01-01-03-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPartida="'.$clavePrimaria.'"  boton="si, Eliminar"
                            descipcion="El usuario a eliminado una partida del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Partida!!">
                            <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,false);
    }


 public function metJsonDataTablaListadoModal()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM pr_b002_partida_presupuestaria ";

        if ($busqueda['value']) {
            $sql .= "WHERE
                    ( 
                      cod_partida LIKE '%$busqueda[value]%' OR 
                      ind_denominacion LIKE '%$busqueda[value]%' 
                    )
                    ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_partida','ind_denominacion','ind_tipo','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_partida_presupuestaria';
        #los valores del listado con flag 'md md-check'
        //$flags = array('num_flag_adelanto');
        #construyo el listado de botones
      /* if (in_array('PR-01-01-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                 <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                         data-keyboard="false" data-backdrop="static" idPartida="'.$clavePrimaria.'"
                         descipcion="El Usuario a Modificado una partida del sistema" titulo="Modificar Partida">
                         <i class="fa fa-edit" style="color: #ffffff;"></i>
                 </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('PR-01-01-01-03-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPartida="'.$clavePrimaria.'"  boton="si, Eliminar"
                            descipcion="El usuario a eliminado una partida del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Partida!!">
                            <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }*/

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,false);
    }



    
}