<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |      liduvica@hotmail.com          |         0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class programaControlador extends Controlador
{
    private $atPrograma;

    public function __construct()
    {
        parent::__construct();
        $this->atPrograma=$this->metCargarModelo('programa','maestros');
		$this->atSector=$this->metCargarModelo('sector','maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		
        $this->atVista->assign('listado',$this->atPrograma->metListarPrograma());
        $this->atVista->metRenderizar('listado');
    }
	
	

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idPrograma=$this->metObtenerInt('idPrograma');
	
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus', 'ind_cod_programa');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
			
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			
			 
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }


            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
           
            
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idPrograma==0){
			
               $id=$this->atPrograma->metCrearPrograma($validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_prb008_num_sector'],$validacion['num_cod_programa']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atPrograma->metModificarPrograma($validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_prb008_num_sector'],$validacion['num_cod_programa'], $idPrograma);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idPrograma']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idPrograma!=0){
            $this->atVista->assign('formDB',$this->atPrograma->metMostrarPrograma($idPrograma));
            $this->atVista->assign('idPrograma',$idPrograma);
        }

        $this->atVista->assign('sector',$this->atSector->metListarSector());
        $this->atVista->metRenderizar('CrearModificar','modales');
    }



    public function metEliminar()
    {
        $idPrograma = $this->metObtenerInt('idPrograma');
		
        if($idPrograma!=0){
            $id=$this->atPrograma->metEliminarPrograma($idPrograma);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Programa se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idPrograma'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}