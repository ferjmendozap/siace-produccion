<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |      liduvica@hotmail.com          |         0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class aespecificaControlador extends Controlador
{
    private $atAespecifica;

    public function __construct()
    {
        parent::__construct();
        $this->atAespecifica=$this->metCargarModelo('aespecifica','maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		
        $this->atVista->assign('listado',$this->atAespecifica->metListarAespecifica());
        $this->atVista->metRenderizar('listado');
    }
	
	

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idAespecifica=$this->metObtenerInt('idAespecifica');
	
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus', 'ind_cod_aespecifica');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
			
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			
			 
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }


            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
           
            
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idAespecifica==0){
			
			// $id=$this->atAespecifica->metCrearAespecifica('prueba','1');
               $id=$this->atAespecifica->metCrearAespecifica($validacion['ind_descripcion'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atAespecifica->metModificarAespecifica($validacion['ind_descripcion'],$validacion['num_estatus'],$idAespecifica);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idAespecifica']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idAespecifica!=0){
            $this->atVista->assign('formDB',$this->atAespecifica->metMostrarAespecifica($idAespecifica));
			
            $this->atVista->assign('idAespecifica',$idAespecifica);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');
    }


    public function metEliminar()
    {
        $idAespecifica = $this->metObtenerInt('idAespecifica');
		
        if($idAespecifica!=0){
            $id=$this->atAespecifica->metEliminarAespecifica($idAespecifica);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Acción Especifica se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idAespecifica'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}