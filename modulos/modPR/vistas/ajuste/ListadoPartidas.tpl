
		
		
	<!-----------DETALLE PRESUPUESTO---------------------------------->		 

			   
			<div>
				  
				 <div class="table-responsive">
                                      <table class="table no-margin" id="tablapartida">
                                                        <thead>
                                                        <tr>
                                                            <th>Codigo</th>
                                                            <th width="50%">Partida</th>
															<th>Monto Ajustado</th>
															<th>Monto Compromiso</th>
															<th>Monto Disponible</th>
															<th>Monto Ajuste</th>
                                                           <!-- <th>Acciones</th>-->
                                                        </tr>
                                                        </thead>
                                                        <tbody  id="codpartida">
                                                        <!--{if isset($partidas)}-->
														  {if isset($partidas)}
                                                            {foreach item=i from=$partidas}
															   
                                                                <tr class="idPartida{$i.pk}">
                                                                    <input type="hidden" value="{$i.pk}" name="form[int][fk_prc002_num_presupuesto_det][]" class="partidaInput" partida="{$i.cod}" id="{$i.pk}"/> 
																															
															 {if $i.tipo=='E'}			
																	<td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.cod}</td>
                                                                    <td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.partida}</td>
																	<input type="hidden" class="form-control" value="" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}" >
																	<input type="hidden" class="form-control" value="" name="form[int][monto_inicial][]" id="{$i.cod}" aespecifica="{$i.aespecifica}" >
																
															 {elseif $i.tipo=='T'}
																	<td style="font-family:Arial, Geneva, sans-serif; font-weight:bold">{$i.cod}</td>
                                                                    <td style="font-family:Arial, Geneva, sans-serif; font-weight:bold">{$i.partida}</td>
																	
																    <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control" readonly value="{$i.monto_inicial|number_format:2:",":"."}" name="form[int][monto_inicial][]" id="partidas" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	<!--------------------COMPROMISO---------------------------->
																	
																	
																	<td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control" readonly value="{$i.monto_comprometido|number_format:2:",":"."}" name="form[int][monto_comprometido][]" id="partidas" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	<!--------------------AJUSTADO---------------------------->
																	
																	
																	<td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control" readonly value="{$i.monto_disponible|number_format:2:",":"."}" name="form[int][monto_disponible][]" id="partidas" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	
																	<!------------------------------------------------->
																	
																	<th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control" readonly value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}"  style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
                                                                           </div>
                                                                      </div>
																	</th>
																{else}			
                                                                    <td>{$i.cod}</td>
                                                                    <td>{$i.partida}</td>
																	
																    <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input readonly type="text" class="form-control" value="{$i.monto_inicial|number_format:2:",":"."}" name="form[int][monto_inicial][]" id="-{$i.cod}" aespecifica="{$i.aespecifica}" style="text-align:right;">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	<!------------------COMPROMETIDO----------------------->
																	
																	<td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input readonly type="text" class="form-control" value="{$i.monto_comprometido|number_format:2:",":"."}" name="form[int][monto_comprometido][]" id="-c{$i.cod}" aespecifica="{$i.aespecifica}" style="text-align:right;">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	<!---------------------DISPONIBLE-------------------->
																	
																    <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input readonly type="text" class="form-control" value="{$i.monto_disponible|number_format:2:",":"."}" name="form[int][monto_disponible][]" id="-p{$i.cod}" aespecifica="{$i.aespecifica}" style="text-align:right;">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	<!----------------------------------------------------------->
																	
																	 <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control" value="{if $i.monto>0}{$i.monto|number_format:2:",":"."}{else}{''}{/if}" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}" style="text-align:right;" >
                                                                           </div>
                                                                      </div>
																	</td> 
																	
																	
																	 <td><!-- ACCION ESPECIFICA-->
																	 
													{if isset($formDB.ind_tipo_presupuesto)}{$tipo=$formDB.ind_tipo_presupuesto} {else} {$tipo=$tipoPresupuesto}{/if}
					    									  
																	{if $tipo=='P'}
																<!--<label width="6" type="text" disabled class="form-control" id="{$i.cod}" >{"00"|cat:$i.aespecifica}</label>-->
																		<input type="hidden" value="{$i.aespecifica}" name="form[alphaNum][Aespecifica][]"  id="Aespecifica"/>	
																    {/if}

                       					  
																	</td> <!--FIN ACCION ESPECIFICA-->
																	
																
																 {/if} 	
																																	
                                                                 
                                                                </tr>
															  {/foreach}	
                                                           
														 {/if}	
                                                       <!--  {/if} -->
                                                        </tbody>
														
														 <!-- Total  -->
														<tr>
														   <td align="right"  style="font-weight:bold" colspan="2">
														      TOTAL=
														   </td>
														    <td id="total_aprobado" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total|number_format:2:",":"."}" disabled name="monto_aprobado" id="monto_aprobado" style="text-align:right;"  />                            
															</td> 
															<!----------------------------COMPROMETIDO--------------------------------->
															 <td id="total_comprometido" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total_comp|number_format:2:",":"."}" disabled name="monto_compromiso" id="monto_compromiso" style="text-align:right;"  />                            
															</td> 
															
															<!--------------------------DISPONIBLE-------------------------------->
															 <td id="total_disponible" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total_disponible|number_format:2:",":"."}" disabled name="monto_disponible" id="monto_disponible" style="text-align:right;"  />                            
															</td> 
															
															<!-------------------------------------------------------------------------->
															
															<th id="total" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total_ajuste|number_format:2:",":"."}" disabled name="monto_total" id="monto_total" style="text-align:right;" /> 
															</th> 
														</tr>
	                                                    
                                                    </table>
                                                </div>
															
                                                   													 
			      <!--    <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="agregar">Agregar</button>-->
			      </div>
				  
				 <div class="modal-footer">
                     <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>Cancelar</          button>
                  <!--   <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
                  </div>-->  
                </div>
		   
    </div>
		
   
   


<script type="text/javascript">
   
	
      
		$('#codpartida').change(function () {
		var suma=0;
		var suma1='';
		var total=0;
		//var monto_total='';
		var codigo=0; 
		var codigo1=0; 
		var partida1='';
		var monto=0;
		var monto1=0;
		String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g,'') }
		
		  $("#codpartida th").each(function (){
		   
				 $(this).find('input').each (function() {
				 
					 aespecifica = $(this).attr('aespecifica');
					
				     idpartida1 = $(this).attr('id');
					
					 suma=actualizarMonto(idpartida1,aespecifica);
					 //suma=actualizarMonto(idpartida1);
					 $(this).val(suma).formatCurrency();
						 		 
					 
	             }) //imput
			 })	 
			 
			 
			  $("#codpartida th").each(function (){
		   
				 $(this).find('input').each (function() {
				 idpartida1 = $(this).attr('id');

					  codigo1= idpartida1.substring(4, 12);
					  if (codigo1=="00.00.00"){
					     monto= setNumero($(this).val());
						total=total+Number(monto);
				  }
						 	 				 
	             }) //imput
			 })	
			  
	  		 
			 $("#total").each(function (){
		   
				  $(this).find('input').each (function() {
				  $(this).val(total).formatCurrency();
				  $(document.getElementById('num_total_ajuste')).val(total).formatCurrency();
					
				
	             }) //imput
			 })	
         });
		
		

</script>


