<div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Check</th>
                            <th>Partida</th>
                            <th>Denominacion</th>
                            <th>Tipo</th>
                            <th>Estatus</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=partida from=$listado}
                            <tr id="idPartida{$partida.pk_num_partida_presupuestaria}">
                                <td>
                                    <div class="checkbox checkbox-styled">
									{if $partida.ind_tipo=='T'}
                                        <label>
                                            <input type="checkbox" class="valores" disabled id="check"
                                                   idPartida="{$partida.pk_num_partida_presupuestaria}"
                                                   partida="{$partida.ind_denominacion}" cod="{$partida.cod_partida}">
                                        </label>
									 {else}	
									    <label>
                                            <input type="checkbox" class="valores" 
                                                   idPartida="{$partida.pk_num_partida_presupuestaria}"
                                                   partida="{$partida.ind_denominacion}" cod="{$partida.cod_partida}">
                                        </label>
									 {/if}	
                                    </div>
                                </td>
                                <td><label>{$partida.cod_partida}</label></td>
                                <td><label>{$partida.ind_denominacion}</label></td>
                                <td><label>{$partida.ind_tipo}</label></td>
                                <td>
                                    <i class="{if $partida.ind_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
        </div>
    </div>
</div>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" class="btn btn-default ink-reaction btn-raised" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary ink-reaction btn-raised" id="agregarPartidaSeleccionada">Agregar
    </button>
</div>


<script type="text/javascript">
    $(document).ready(function () {
	  
	  var p=0;
	  if (p<=0)
	  {
	   var input2 = $('.valores');
	   p=9;
	  }
		    
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

	   
	   
	    
        $('#agregarPartidaSeleccionada').click(function () {
		
		
		
            var input = $('.valores');
			var idpartida1='';
			var valor=0;
			var insertar='';
			var pos=0;
			var id=0;
			
            for (i = 0; i < input.length; i++) {
			    var codigo ='';
				var codigo2 ='';
				
							
                if (input[i].checked == true) {
				
				
		 	//--------------------------------	
				
				for (j = 0; j < input2.length; j++) { 
				 
				     codigo= input[i].getAttribute('cod').substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 codigo2= input[i].getAttribute('cod').substring(0, 3);
					 codigo2= codigo2.concat(".00.00.00");
					 
					// alert (codigo2);
					
				
					
		  if ( input2[j].getAttribute('cod')==codigo || input2[j].getAttribute('cod')==codigo2 )  {
		   
			 //alert (input2[j].getAttribute('cod'));
			 //alert ($(document.getElementsByClassName('idPartida' + input[j].getAttribute('idPartida'))).length);
			 
		     	
			 
		if (!$(document.getElementsByClassName('idPartida' + input2[j].getAttribute('idPartida'))).length){
			 
			   //-------------------------------------------------------Prueba
		  insertar='append'
		  
		   nueva=parseInt(input2[j].getAttribute('idPartida'));
		   
		  // alert(nueva);
		  
		    $("#codpartida tr").each(function () 
                 { 
				    idpartida1 = $(this).attr('class');
					
					//alert(idpartida1);
					//
					if (idpartida1){
					
					//  idpartida1 = idpartida1.substring(9);
					  valor = parseInt(idpartida1.substring(9));
					  if ( valor>nueva){ 
					     insertar=idpartida1;
				         //alert (valor);
						 id=valor;
						 //alert(input[i].getAttribute('idPartida'));
						 return false;
					  }
					  else {
					     insertar='append'
						
						 }
					 } 
			
                 }) 
				 
				 
				/* alert (valor);
				 alert (nueva);
				 alert(insertar);*/
			//--------------------------------------------------------Fin Prueba	 
			 
                   //$(document.getElementsByClassName('idPartida' + input[j].getAttribute('idPartida'))).remove();
				   
				 if (insertar=='append'){     
                    $(document.getElementById('codpartida')).append(
                            '<tr class="idPartida' + input2[j].getAttribute('idPartida') + '">' +
                            '<input type="hidden" value="' + input2[j].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]" id="'+ input2[j].getAttribute('idPartida') +'" class="partidaInput" partida="' + input2[j].getAttribute('partida') + '" />' +
                            '<th>' + input2[j].getAttribute('cod') + '</th>' +
                            '<th>' + input2[j].getAttribute('partida') + '</th>' +
							
							'<td> <input  disabled type="text" class="form-control" value="0,00" name="form[int][monto]['+ input[i].getAttribute('idPartida') +']" id="partidas"   onchange="actualizarMonto('+input[i].getAttribute('cod')+')"  style="text-align:right;"></td>' +
							
							'<th> <input type="text" disabled  id="'+input2[j].getAttribute('cod')+'" class="form-control" value="0,00" name="form[int][monto]['+ input2[j].getAttribute('idPartida') +']" style="text-align:right;" ></th>' +
                            
                            '</tr>'
                    );
					
					}else{
					
					 $('#'+id+'').closest("tr").before('<tr class="idPartida' + input2[j].getAttribute('idPartida') + '">' +
                            '<input type="hidden" value="' + input2[j].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]" id="'+ input2[j].getAttribute('idPartida') +'" class="partidaInput" partida="' + input2[j].getAttribute('partida') + '" />' +
                            '<th>' + input2[j].getAttribute('cod') + '</th>' +
                            '<th>' + input2[j].getAttribute('partida') + '</th>' +
							
							'<td> <input  disabled type="text" class="form-control" value="0,00" name="form[int][monto]['+ input[i].getAttribute('idPartida') +']" id="partidas"   onchange="actualizarMonto('+input[i].getAttribute('cod')+')"  style="text-align:right;"></td>' +
							
							'<th> <input type="text" disabled  id="'+input2[j].getAttribute('cod')+'" class="form-control" value="0,00" name="form[int][monto]['+ input2[j].getAttribute('idPartida') +']" style="text-align:right;"></th>' +
                           
                            '</tr>'); 
					
					}
				   }// fin if !
                 }//fin if false
				
				}//if checked
				//------------------------------------------------
				
				 if (!$(document.getElementsByClassName('idPartida' + input[i].getAttribute('idPartida'))).length){
				 
				 //---------------------------------------------
				 
				 pos=parseInt(input[i].getAttribute('idPartida'));
				 
				 $("#codpartida tr").each(function () 
                 { 
				    idpartida1 = $(this).attr('class');
					
					//alert(idpartida1);
					//
					if (idpartida1){
					
					//  idpartida1 = idpartida1.substring(9);
					  valor = parseInt(idpartida1.substring(9));
					  if ( valor>pos){ 
					     insertar=idpartida1;
				         //alert (valor);
						 id=valor;
						 //alert(input[i].getAttribute('idPartida'));
						 return false;
					  }
					  else {
					     insertar='append'
						
						 }
						 
					 } 
			
                 }) 
				 
				/* alert (valor);
				 alert (pos);
				 alert(insertar);*/
				//--------------------------------------------------------------- 
				 
				 
				 
                  //  $(document.getElementsByClassName('idPartida' + input[i].getAttribute('idPartida'))).remove()
				 
			  
			 if (insertar=='append'){
                    $(document.getElementById('codpartida')).append(
                            '<tr class="idPartida' + input[i].getAttribute('idPartida') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]" id="'+ input[i].getAttribute('idPartida') +'" class="partidaInput" partida="' + input[i].getAttribute('partida') + '" />' +
                            '<td>' + input[i].getAttribute('cod') + '</td>' +
                            '<td>' + input[i].getAttribute('partida') + '</td>' +
							'<td> <input  disabled type="text" class="form-control" value="0,00" name="form[int][monto]['+ input[i].getAttribute('idPartida') +']" id="partidas"   onchange="actualizarMonto('+input[i].getAttribute('cod')+')"  style="text-align:right;"></td>' +
                            '<td> <input type="text" class="form-control" value="0,00" name="form[int][monto]['+ input[i].getAttribute('idPartida') +']" id="'+input[i].getAttribute('cod')+'"  onchange="actualizarMonto('+input[i].getAttribute('cod')+')"  style="text-align:right;"></td>' +
                            '</tr>'
                    );
				 }
				 
				 else{ 
                          $('#'+id+'').closest("tr").before('<tr class="idPartida' + input[i].getAttribute('idPartida') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]"  id="'+ input[i].getAttribute('idPartida') +'"  class="partidaInput" partida="' + input[i].getAttribute('partida') + '" />' +
                            '<td>' + input[i].getAttribute('cod') + '</td>' +
                            '<td>' + input[i].getAttribute('partida') + '</td>' +
							'<td> <input disabled type="text" class="form-control" value="0,00" name="form[int][monto]['+ input[i].getAttribute('idPartida') +']" id="partidas"  onchange="actualizarMonto('+input[i].getAttribute('cod')+')"  style="text-align:right;"></td>' +
							'<td> <input type="text" class="form-control" value="0,00" name="form[int][monto]['+ input[i].getAttribute('idPartida') +']" id="'+input[i].getAttribute('cod')+'"  onchange="actualizarMonto('+input[i].getAttribute('cod')+')"  style="text-align:right;"></td>' +
                           
                            '</tr>'); 
				 
				 }
				 
                }//if false
			  }// for
            }
			
			
			
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>