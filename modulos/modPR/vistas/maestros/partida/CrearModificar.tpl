<form action="{$_Parametros.url}modPR/maestros/partidaCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idPartida) }
            <input type="hidden" value="{$idPartida}" name="idPartida"/>
        {/if}
		
		 <div class="row">
                <div class="col-sm-10">
                    <div class="form-group floating-label" id="fk_prb001_num_tipo_cuentaError">
                        <select id="fk_prb001_num_tipo_cuenta" name="form[int][fk_prb001_num_tipo_cuenta]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$cuenta}
                                {if isset($formDB.fk_prb001_num_tipo_cuenta)}
                                    {if $app.cod_detalle==$formDB.fk_prb001_num_tipo_cuenta}
                                        <option selected value="{$app.cod_detalle}">{$app.ind_nombre_detalle}</option>
                                        {else}
                                        <option value="{$app.cod_detalle}">{$app.ind_nombre_detalle}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.cod_detalle}">{$app.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb001_num_tipo_cuenta">Seleccione Tipo de Cuenta</label>
                    </div>
                </div>
			  </div>	
			  
		 <div class="row">
		
		       <div class="col-sm-2">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_partida)}{$formDB.ind_partida}{/if}" name="form[alphaNum][ind_partida]" id="ind_partida">
                        <label for="ind_partida">Partida</label>
                    </div>
                 </div>
				 
				   <div class="col-sm-2">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_generica)}{$formDB.ind_generica}{/if}" name="form[alphaNum][ind_generica]" id="ind_generica">
                        <label for="ind_generica">Generica</label>
                    </div>
                 </div>
				 
				  <div class="col-sm-2">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_especifica)}{$formDB.ind_especifica}{/if}" name="form[alphaNum][ind_especifica]" id="ind_especifica">
                        <label for="ind_especifica">Especifica</label>
                    </div>
                 </div>
				 
				  <div class="col-sm-2">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_subespecifica)}{$formDB.ind_subespecifica}{/if}" name="form[alphaNum][ind_subespecifica]" id="ind_subespecifica">
                        <label for="ind_subespecifica">Sub-Especifica</label>
                    </div>
                 </div>
				
				
				 <div class="col-sm-2">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numeral)}{$formDB.ind_numeral}{/if}" name="form[alphaNum][ind_numeral]" id="ind_numeral">
                        <label for="ind_numeral">Numeral</label>
                    </div>
                 </div>
				
		 </div>
		
		
         <div class="row">
		
           	 <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_denominacionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_denominacion)}{$formDB.ind_denominacion}{/if}" 
						onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())" name="form[alphaNum][ind_denominacion]" id="ind_denominacion">
                        <label for="ind_denominacion"><i class="icm icm-cog3"></i> Denominacion</label>
                    </div>
                </div>
						
			
			    <div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_tipoError">
                        <select id="ind_tipo" name="form[alphaNum][ind_tipo]" class="form-control">
                       							
                            {if isset($formDB.ind_tipo) and $formDB.ind_tipo =='T'}
                                <option selected value="T">TITULO</option>
                                {else}
                                <option value="T">TITULO</option>
                            {/if}
                            {if isset($formDB.ind_tipo) and $formDB.ind_tipo =='D'}
                                <option value="D" selected>DETALLE</option>
                                {else}
                                <option value="D">DETALLE</option>
                            {/if}
                            
                        </select>
                        <label for="num_nivel">Seleccione el Tipo</label>
                    </div>
                </div>
		  
				 <div class="col-sm-4">
                    <div class="form-group floating-label" id="num_nivelError">
                        <select id="num_nivel" name="form[alphaNum][num_nivel]" class="form-control">
                           
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==1}
                                <option selected value="1">Nivel 1</option>
                                {else}
                                <option value="1">Nivel 1</option>
                            {/if}
							
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==2}
                                <option value="2" selected>Nivel 2</option>
                                {else}
                                <option value="2">Nivel 2</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==3}
                                <option value="3" selected>Nivel 3</option>
                                {else}
                                <option value="3">Nivel 3</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==4}
                                <option value="4" selected>Nivel 4</option>
                                {else}
                                <option value="4">Nivel 4</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==5}
                                <option value="5" selected>Nivel 5</option>
                                {else}
                                <option value="5">Nivel 5</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==6 }
                                <option value="6" selected>Nivel 6</option>
                                {else}
                                <option value="6">Nivel 6</option>
                            {/if}
                        </select>
                        <label for="num_nivel">Seleccione el Nivel</label>
                    </div>
                </div>
				
				
            </div>
			
			
			
        <div class="row">
		
		      <div class="col-sm-3">
			              <input id="fk_cbb004_num_plan_cuenta_pub20" type="hidden"  value="{if isset($formDB.fk_cbb004_num_plan_cuenta_pub20)}{$formDB.fk_cbb004_num_plan_cuenta_pub20}{/if}" name="form[alphaNum][fk_cbb004_num_plan_cuenta_pub20]">
			       
                          <label for="cod_cuenta"><i class="icm icm-calculate2"></i>
                                  Cuenta PUB20</label>
                                  <input type="text" class="form-control"
                                  value="{if isset($formDB.cod_cuenta)}{$formDB.cod_cuenta}{/if}"
                                  id="cod_cuenta" disabled readonly>
			 </div>
			 <div class="col-sm-5">					  
								  <label for="ind_descripcion"><i class="md md-assignment"></i>
                                  Descripcion</label>
                                  <input type="text" class="form-control"
                                  value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}"
                                  id="ind_descripcion" disabled readonly>
			</div>					  
								  
				<div class="col-sm-1">	 
				    {if isset($ver)} {else} 		  
				     	<button type="button"
                            class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                            id="agregarCuentaPub20"
                            data-toggle="modal" data-target="#formModal2"
                            data-keyboard="false" data-backdrop="static"
                            titulo="Agregar Cuenta Pub20"
                            url="{$_Parametros.url}modPR/maestros/partidaCONTROL/CuentasMET/PUB20">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                       </button>
				    {/if} 
				</div>	
									  
		</div>	  
				 
		<div class="row">
		
		      <div class="col-sm-3">
			              <input id="fk_cbb004_num_plan_cuenta_onco" type="hidden"  value="{if isset($formDB.fk_cbb004_num_plan_cuenta_onco)}{$formDB.fk_cbb004_num_plan_cuenta_onco}{/if}" name="form[alphaNum][fk_cbb004_num_plan_cuenta_onco]">
			       
                          <label for="cod_cuenta_onco"><i class="icm icm-calculate2"></i>
                                  Cuenta ONCOP</label>
                                  <input type="text" class="form-control"
                                  value="{if isset($formDB.cod_cuenta_onco)}{$formDB.cod_cuenta_onco}{/if}"
                                  id="cod_cuenta_onco" disabled readonly>
			 </div>
			 <div class="col-sm-5">					  
								  <label for="ind_descripcion_onco"><i class="md md-assignment"></i>
                                  Descripcion</label>
                                  <input type="text" class="form-control"
                                  value="{if isset($formDB.ind_descripcion_onco)}{$formDB.ind_descripcion_onco}{/if}"
                                  id="ind_descripcion_onco" disabled readonly>
			</div>					  
								  
				
				
				<div class="col-sm-1">	 
				    {if isset($ver)} {else} 		  
				     	<button type="button"
                            class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                            id="agregarCuentaOnco"
                            data-toggle="modal" data-target="#formModal2"
                            data-keyboard="false" data-backdrop="static"
                            titulo="Agregar Cuenta Oncop"
                            url="{$_Parametros.url}modPR/maestros/partidaCONTROL/CuentasMET/ONCOP">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                       </button>
				    {/if} 
				</div>							  
		</div>	  
				  
		   		  
	   <div class="row">
		
		      <div class="col-sm-3">
			              <input id="fk_cbb004_num_plan_cuenta_gastopub20" type="hidden"  value="{if isset($formDB.fk_cbb004_num_plan_cuenta_gastopub20)}{$formDB.fk_cbb004_num_plan_cuenta_gastopub20}{/if}" name="form[alphaNum][fk_cbb004_num_plan_cuenta_gastopub20]">
			       
                          <label for="cod_cuenta_onco"><i class="icm icm-calculate2"></i>
                                  Cuenta GASTO PUB20</label>
                                  <input type="text" class="form-control"
                                  value="{if isset($formDB.cod_cuenta_gasto)}{$formDB.cod_cuenta_gasto}{/if}"
                                  id="cod_cuenta_gasto" disabled readonly>
			 </div>
			 <div class="col-sm-5">					  
								  <label for="ind_descripcion_gasto"><i class="md md-assignment"></i>
                                  Descripcion</label>
                                  <input type="text" class="form-control"
                                  value="{if isset($formDB.ind_descripcion_gasto)}{$formDB.ind_descripcion_gasto}{/if}"
                                  id="ind_descripcion_gasto" disabled readonly>
			</div>					  
								  
				<div class="col-sm-1">	 
				    {if isset($ver)} {else} 		  
				     	<button type="button"
                            class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                            id="agregarCuentaGasto"
                            data-toggle="modal" data-target="#formModal2"
                            data-keyboard="false" data-backdrop="static"
                            titulo="Agregar Cuenta Gasto"
                            url="{$_Parametros.url}modPR/maestros/partidaCONTROL/CuentasMET/GASTO">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                       </button>
				    {/if} 
				</div>	
				
				
				<div class="checkbox checkbox-styled col-sm-1">
						<label>
							<input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked  {elseif  isset($formDB.num_estatus) and $formDB.num_estatus==0}} {else} checked {/if} value="1" name="form[int][num_estatus]">
							<span>Estatus</span>
						</label>
			    </div>							  
		</div>	  
			
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
		
		
        $("#formAjax").submit(function(){
            return false;
        });
		
		
		$('#agregarCuentaPub20').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		$('#agregarCuentaOnco').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		
		$('#agregarCuentaGasto').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		
        $('#modalAncho').css("width","85%");
        $('#accion').click(function(){
		   swal({
                title: "�Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                  
                    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idPartida'+dato['idPartida'])).html('<td>'+dato['cod_partida']+'</td>' +
                            '<td>'+dato['ind_denominacion']+'</td>' +
                            '<td>'+dato['ind_tipo']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +
                            '<td>' +
                            '{if in_array('PR-01-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPartida="'+dato['idPartida']+'"' +
                            'descipcion="El Usuario a Modificado una Partida" titulo="Modificar Partida">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-01-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPartida="'+dato['idPartida']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado una Partida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Partida!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "La Partida fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='nuevo'){
                   
                    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idPartida'+dato['idPartida']+'">' +
                            '<td>'+dato['cod_partida']+'</td>' +
                            '<td>'+dato['ind_denominacion']+'</td>' +
                            '<td>'+dato['ind_tipo']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td>' +
                            '{if in_array('PR-01-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPartida="'+dato['idPartida']+'"' +
                            'descipcion="El Usuario a Modificado una Partida" titulo="Modificar Partida">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-01-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPartida="'+dato['idPartida']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado una Partida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Partida!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "La Partida fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>