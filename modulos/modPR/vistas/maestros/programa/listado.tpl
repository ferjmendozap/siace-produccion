<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Programa - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Codigo</th>
								<th>Id</th>
                                <th width="70%">Descripcion</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=programa from=$listado}
                                <tr id="idPrograma{$programa.pk_num_programa}">
                                    <td><label>{$programa.ind_cod_programa}</label></td>
									<td><label>{$programa.num_cod_programa}</label></td>
                                    <td><label>{$programa.ind_descripcion}</label></td>
                                    <td>
                                        <i class="{if $programa.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="left">
                                        {if in_array('PR-01-01-02-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idPrograma="{$programa.pk_num_programa}"
                                                    descipcion="El Usuario a Modificado un programa" titulo="Modificar un programa">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('PR-01-01-02-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPrograma="{$programa.pk_num_programa}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado una acci&oacute;n programa" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el programa!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="5">
							<div>
                                {if in_array('PR-01-01-02-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un nuevo programa"  titulo="Crear un programa" id="nuevo" >
                                        Nuevo programa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                {/if}
							</div>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
	

        var $url='{$_Parametros.url}modPR/maestros/programaCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPrograma:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
		 
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPrograma: $(this).attr('idPrograma')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idPrograma=$(this).attr('idPrograma');
			
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/maestros/programaCONTROL/eliminarMET';
                $.post($url, { idPrograma: idPrograma },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idPrograma'+dato['idPrograma'])).html('');
                        swal("Eliminado!", "El Programa fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>