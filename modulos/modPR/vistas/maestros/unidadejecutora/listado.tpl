<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Unidad Ejecutora - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-head">
                <header></header>
                <div class="tools">
                 <!--   <div class="btn-group">
                        <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
                    </div>-->
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                           <!-- <th>Organismo</th>-->
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=miscelaneo from=$listado}
                            <tr id="idUnidadEjecutora{$miscelaneo.pk_num_unidad_ejecutora}">
                                <!--<td><label>{$miscelaneo.ind_descripcion_empresa}</label></td>-->
                                <td><label>{$miscelaneo.cod_unidad_ejecutora}</label></td>
                                <td><label>{$miscelaneo.ind_nombre_unidad_ejecutora}</label></td>
                                <td><label>{$miscelaneo.ind_descripcion}</label></td>
                                <td>
                                    <i class="{if $miscelaneo.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>
                                <td align="left">
                                      {if in_array('PR-01-05-01-02-M',$_Parametros.perfil)}
                                        <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idUnidadEjecutora="{$miscelaneo.pk_num_unidad_ejecutora}" title="Editar"
                                                descipcion="El Usuario a Modificado una Unidad Ejecutora" titulo="Modificar Unidad Ejecutora">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                    &nbsp;&nbsp;
                                    {if in_array('PR-01-05-01-02-M',$_Parametros.perfil)}
                                        <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUnidadEjecutora="{$miscelaneo.pk_num_unidad_ejecutora}"  boton="si, Eliminar" title="Eliminar"
                                                descipcion="El usuario a eliminado una Unidad Ejecutora" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Unidad Ejecutora!!">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="3">
                                {if in_array('PR-01-05-01-02-M',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una Unidad Ejecutora"  titulo="Crear Unidad Ejecutora" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nueva Unidad Ejecutora
                                    </button>
                                {/if}
								
								
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- filtro -->
<div class="offcanvas">
    <div id="offcanvas-filtro" class="offcanvas-pane width-7">
        <div class="offcanvas-head">
            <header>Filtro</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form id="filtro" action="{$_Parametros.url}miscelaneo" class="form" role="form" method="post">
                <input type="hidden" name="filtro" value="1">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control">
                                <option value="">&nbsp;</option>
                                {foreach item=app from=$aplicaciones}
                                    <option value="{$app.pk_num_seguridad_aplicacion}">{$app.ind_descripcion_empresa}</option>
                                {/foreach}
                            </select>
                            <label for="fk_a001_num_organismo"><i class="md md-apps"></i>Seleccione la aplicacion</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <select id="num_estatus" name="form[int][num_estatus]" class="form-control">
                                <option value="">&nbsp;</option>
                                <option value="1" selected>Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                            <label for="num_estatus"><i class="md md-visibility"></i>Seleccione el estatus</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#filtro").submit(function(){
            return false;
        });
        $('#botonFiltro').click(function(){
            $.post($("#filtro").attr("action"), $( "#filtro" ).serialize(),function(dato){
                $("#datatable1 tbody").html('');
                for(var i=0;i < dato.length;i++){
                    $("#datatable1 tbody").append('<tr id="idUnidadEjecutora'+dato[i]["pk_num_unidad_ejecutora"]+'">'+
                           // '<td><label>'+dato[i]['ind_descripcion_empresa']+'</label></td>'+
                            '<td><label>'+dato[i]['cod_unidad_ejecutora']+'</label></td>'+
                            '<td><label>'+dato[i]['ind_nombre_unidad_ejecutora']+'</label></td>'+
                            '<td><label>'+dato[i]['ind_descripcion']+'</label></td>'+
                            '<td>'+
                            ''+
                            '</td>'+
                            '<td align="center">'+
                            '{if in_array('AP-05-02-M',$_Parametros.perfil)}'+
                                '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"'+
                                'data-keyboard="false" data-backdrop="static" idUnidadEjecutora="'+dato[i]["pk_num_unidad_ejecutora"]+'"'+
                                'descipcion="El Usuario a Modificado una unidad ejecutora" titulo="Modificar Unidad Ejecutora">'+
                                '<i class="fa fa-edit" style="color: #ffffff;"></i>'+
                                '</button>'+
                            '{/if}'+
                            '&nbsp;&nbsp;'+
                            '{if in_array('AP-05-03-E',$_Parametros.perfil)}'+
                                '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUnidadEjecutora="'+dato[i]["pk_num_unidad_ejecutora"]+'"  boton="si, Eliminar"'+
                                'descipcion="El usuario a eliminado una Unidad Ejecutora" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Unidad Ejecutora!!">'+
                                '<i class="md md-delete" style="color: #ffffff;"></i>'+
                                '</button>'+
                            '{/if}'+
                            '</td>'+
                    '</tr>');
                }
            },'json');
        });

        var $url='{$_Parametros.url}modPR/maestros/unidadejecutoraCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idUnidadEjecutora:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idUnidadEjecutora: $(this).attr('idUnidadEjecutora')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
       
	    $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idUnidadEjecutora=$(this).attr('idUnidadEjecutora');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/maestros/unidadejecutoraCONTROL/eliminarMET';
                $.post($url, { idUnidadEjecutora: idUnidadEjecutora },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idUnidadEjecutora'+dato['idUnidadEjecutora'])).html('');
                        swal("Eliminado!", "La unidad Ejecutora fue eliminada satisfactoriamente.", "success");
					 }else{
                        swal("Error!", dato['mensaje'] , "error");	
                    }
                },'json');
            });
        });
    });
</script>