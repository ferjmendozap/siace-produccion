<form action="{$_Parametros.url}modPR/maestros/unidadejecutoraCONTROL/crearModificarMET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idUnidadEjecutora}" name="idUnidadEjecutora"/>

        <div class="col-sm-20">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_a001_num_organismoError">
                        <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control">
                           {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}  
                        </select>
                        <label for="fk_a001_num_organismo"><i class="md md-apps"></i> Seleccione El Organimo</label>
                    </div>
                 </div>
				
					<div class="col-sm-3">
						<div class="form-group floating-label" id="cod_unidad_ejecutoraError">
							<input style="text-transform:uppercase;" maxlength="3" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())" type="text" class="form-control" value="{if isset($formDB.cod_unidad_ejecutora)}{$formDB.cod_unidad_ejecutora}{/if}" {if isset($formDB.cod_unidad_ejecutora)} readonly {/if} name="form[txt][cod_unidad_ejecutora]" id="cod_unidad_ejecutora">
							<label for="cod_unidad_ejecutora"><i class="fa fa-barcode"></i> Cod Unidad Ejecutora</label>
						</div>
					</div>
	          </div>
			
			
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_nombre_unidad_ejecutoraError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre_unidad_ejecutora)}{$formDB.ind_nombre_unidad_ejecutora}{/if}" name="form[txt][ind_nombre_unidad_ejecutora]" id="ind_nombre_unidad_ejecutora">
                        <label for="ind_nombre_unidad_ejecutora"><i class="md md-border-color"></i> Nombre Unidad Ejecutora</label>
                    </div>
                </div>

            
                <div class="col-sm-8">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[txt][ind_descripcion]" id="ind_descripcion">
                        <label for="ind_descripcion"><i class="md md-insert-comment"></i> Descripcion la Unidad Ejecutora</label>
                    </div>
                </div>
            </div>


           <div class="row">
			    <div class="col-sm-1">
                   <label for="fk_a023_num_centro_de_costo"  class="control-label"  style="margin-top: 10px;">Centro de Costo:</label>
                </div>
                                               
               <div class="col-sm-1">
                   <div class="form-group" id="fk_a023_num_centro_de_costoError">
                         <input type="text" class="form-control" value="{if isset($formDB.fk_a023_num_centro_de_costo)}{$formDB.fk_a023_num_centro_de_costo}{/if}" name="form[int][fk_a023_num_centro_de_costo]" id="fk_a023_num_centro_de_costo" readonly>
																		
                   </div>
              </div>
			  
			  <div class="col-sm-5">
                 <div class="form-group" id="ind_descripcion_centro_costoError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion_centro_costo)}{$formDB.ind_descripcion_centro_costo}{/if}" name="form[int][ind_descripcion_centro_costo]" id="ind_descripcion_centro_costo" readonly>
			     </div>
			  </div>
						 <div class="col-sm-3">                                             
							<div class="form-group floating-label">
								<button
								   type="button"
								   class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
								   id="agregarCentrocosto"
								   data-toggle="modal"
								   data-target="#formModal2"
								   titulo="Listado Centro de Costo"
								   {if isset($ver) and $ver==1}disabled{/if}
								   url="{$_Parametros.url}modPR/maestros/partidaCONTROL/centroCostoMET">
								   <i class="md md-search"></i>
							   </button>
						  </div>    	   
						</div>    
				  
				   <div class="col-sm-2">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked  {elseif  isset($formDB.num_estatus) and $formDB.num_estatus==0}} {else} checked {/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
				                                          
              </div>
				
            </div>
        </div>
		
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="table-responsive no-margin">
                            <table class="table no-margin" id="contenidoTabla">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th style="width: 70px;">Codigo</th>
                                        <th>Dependencia</th>
                                        <th style="width: 20px;">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {if isset($formDBSelect)}
                                    {foreach item=opciones from=$formDBSelect}
                                        <tr>
                                            <input type="hidden" value="{$opciones.pk_num_unidad_ejecutora_dependencia}" name="form[int][pk_num_unidad_ejecutora_dependencia][{$n}]">
                                            <td class="text-right" style="vertical-align: middle;">
                                                {$numero++}
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group floating-label" id="cod_unidad_ejecutora_dependencia{$n}Error">
                                                        <input type="text" class="form-control" value="{$opciones.cod_unidad_ejecutora_dependencia}" name="form[txt][cod_unidad_ejecutora_dependencia][{$n}]" id="cod_unidad_ejecutora_dependencia{$n}">
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group floating-label" id="ind_nombre_unidad_ejecutora_dependencia{$n}Error">
                                                        <input type="text" class="form-control"  value="{$opciones.ind_nombre_unidad_ejecutora_dependencia}" name="form[txt][ind_nombre_unidad_ejecutora_dependencia][{$n}]" id="ind_nombre_unidad_ejecutora_dependencia{$n}">      
                                                    </div>
                                                </div>
                                            </td>
                                            <td  class="text-center" style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="checkbox checkbox-styled">
                                                        <label>
                                                            <input type="checkbox" {if $opciones.num_estatus == 1}checked{/if} value="1" name="form[int][num_estatusDet][{$n++}]">
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                                </tbody>
                                <tfoot>
                                    <tr>
								
									                      <td colspan="5" align="right">
                                                                {if isset($veranular)} {else}
                                                                    <button type="button" 
																		   class="logsUsuario btn ink-reaction btn-raised btn-info accionModal"
                                                                            id="agregarDependencia"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Dependencias"
                                                                            url="{$_Parametros.url}modPR/maestros/partidaCONTROL/DependenciasMET/MODAL">
															+ Insertar Dependencia
                                                                    </button>
					
                                                                {/if}
                                                            </td>
								                  
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
      
		
		
		<!-----------------------------RESPONSABLE------------------------------------------->
		
		
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="table-responsive no-margin">
                            <table class="table no-margin" id="contenidoTablaResponsable">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th style="width: 100px;">Codigo</th>
                                        <th>Responsable</th>
                                        <th style="width: 20px;">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {if isset($formDBSelectRes)}
                                    {foreach item=opciones from=$formDBSelectRes}
                                        <tr>
                                            <input type="hidden" value="{$opciones.pk_num_responsable}" name="form[int][pk_num_responsable][{$r}]">
                                            <td class="text-right" style="vertical-align: middle;">
                                                {$numerores++}
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group floating-label" id="cod_responsable{$r}Error">
                                                        <input type="text" class="form-control" value="{$opciones.cod_responsable}" name="form[txt][cod_responsable][{$r}]" id="cod_responsable{$r}">
                                              <!--          <label for="cod_responsable{$r}"><i class="md md-insert-comment"></i> Codigo</label>-->
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group floating-label" id="ind_nombre_responsable{$r}Error">
                                                        <input type="text" class="form-control" value="{$opciones.ind_nombre_responsable}" name="form[txt][ind_nombre_responsable][{$r}]" id="ind_nombre_responsable{$r}">
                                              <!--          <label for="ind_nombre_responsable{$r}"><i class="md md-insert-comment"></i> Nombre </label>-->
                                                    </div>
                                                </div>
                                            </td>
                                            <td  class="text-center" style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="checkbox checkbox-styled">
                                                        <label>
                                                           <input type="checkbox" {if $opciones.num_estatus == 1}checked{/if} value="1" name="form[int][num_estatusDetRes][{$r++}]">
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                                </tbody>
                                <tfoot>
                                    <tr>
								
									                      <td colspan="5" align="right">
                                                                {if isset($veranular)} {else}
                                                                    <button type="button" 
																		   class="logsUsuario btn ink-reaction btn-raised btn-info accionModal"
                                                                            id="agregarResponsable"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Responsable"
                                                                            url="{$_Parametros.url}modPR/maestros/partidaCONTROL/ResponsableMET/MODAL">
															+ Insertar Responsable
                                                                    </button>
					
                                                                {/if}
                                                            </td>
								                  
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
       
		
		
		<!-------------------------------------------------------------------------------------->
		
		
		
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function() {
        var pr = new  PrFunciones();
        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
         
		    if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
		
		$("#formAjax").submit(function(){
            return false;
        });
		
        $('#contenidoTabla').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });
		
		 $('#contenidoTablaResponsable').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });
		
		$('#agregarDependencia').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		$('#agregarResponsable').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		$('#agregarCentrocosto').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0, tr: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
       
	   //$('#cod_unidad_ejecutora').inputmask("***");
		
        $('#modalAncho').css( "width", "75%" );
        $('#accion').click(function(){
		   swal({
                title: "�Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
		
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ["num_estatus"];
                var arrayMostrarOrden = ['cod_unidad_ejecutora','ind_nombre_unidad_ejecutora','ind_descripcion','num_estatus'];
                if(dato['status']=='error'){
                    pr.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    pr.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
               
			    }else if(dato['status']=='modificar'){
                    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
					
					$(document.getElementById('idUnidadEjecutora'+dato['idUnidadEjecutora'])).html(
                            '<td>'+dato['cod_unidad_ejecutora']+'</td>' +
                            '<td>'+dato['ind_nombre_unidad_ejecutora']+'</td>' +
							'<td>'+dato['ind_descripcion']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +
                            '<td align="left">' +
                            '{if in_array('PR-01-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idUnidadEjecutora="'+dato['idUnidadEjecutora']+'"' +
                            'descipcion="El Usuario a Modificado una Unidad Ejecutora" titulo="Modificar Unidad Ejecutora">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '</td>');
                    swal("Registro Modificado!", "La Unidad Ejecutora fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
                }else if(dato['status']=='nuevo'){
				
				 if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  idUnidadEjecutora="idUnidadEjecutora'+dato['idUnidadEjecutora']+'">' +
                            '<td>'+dato['cod_unidad_ejecutora']+'</td>' +
                            '<td>'+dato['ind_nombre_unidad_ejecutora']+'</td>' +
							'<td>'+dato['ind_descripcion']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td align="left">' +
                            '{if in_array('PR-01-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idUnidadEjecutora="'+dato['idUnidadEjecutora']+'"' +
                            'descipcion="El Usuario a Modificado una Unidad Ejecutora" titulo="Modificar Unidad Ejecutora">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;' +
							
							
							'{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUnidadEjecutora="'+dato['idUnidadEjecutora']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado una Unidad Ejecutora" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Unidad Ejecutora!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    
					 swal("Registro Creado!", "La Unidad Ejecutora fue creada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
                }
            },'json');
        });
    });
</script>