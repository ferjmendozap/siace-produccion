<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Acci&oacute;n Centralizada - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th width="70%">Descripcion</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=acentralizada from=$listado}
                                <tr id="idAcentralizada{$acentralizada.pk_num_acentralizada}">
                                    <td><label>{$acentralizada.ind_cod_acentralizada}</label></td>
                                    <td><label>{$acentralizada.ind_descripcion}</label></td>
                                    <td>
                                        <i class="{if $acentralizada.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('PR-01-01-02-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAcentralizada="{$acentralizada.pk_num_acentralizada}"
                                                    descipcion="El Usuario a Modificado una acci&oacute;n centralizada" titulo="Modificar acci&oacute;n centralizada">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('PR-01-01-02-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAcentralizada="{$acentralizada.pk_num_acentralizada}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado una acci&oacute;n acentralizada" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la acci&oacute;n centralizada!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">
							<div>
                                {if in_array('PR-01-01-02-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una nueva acci&oacute;n centralizada"  titulo="Crear acci&oacute;n centralizada" id="nuevo" >
                                        Nueva accion centralizada &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                {/if}
							</div>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
	

        var $url='{$_Parametros.url}modPR/maestros/acentralizadaCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAcentralizada:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
		 
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAcentralizada: $(this).attr('idAcentralizada')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idAcentralizada=$(this).attr('idAcentralizada');
			
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/maestros/acentralizadaCONTROL/eliminarMET';
                $.post($url, { idAcentralizada: idAcentralizada },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idAcentralizada'+dato['idAcentralizada'])).html('');
                        swal("Eliminado!", "La Accion Centralizada fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>