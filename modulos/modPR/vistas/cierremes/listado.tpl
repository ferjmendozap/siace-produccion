<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Cierre Mensual - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Presupuesto</th>
                                <th>Periodo</th>
								<th>Disponibilidad Presupuestaria</th>
								<th>Disponibilidad Financiera</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=cierrremes from=$listado}
                                <tr id="idCierreMes{$cierrremes.pk_num_ejecucion_presupuestaria}">
                                    <td><label>{$cierrremes.ind_cod_presupuesto}</label></td>
                                    <td><label>{$cierrremes.fec_anio|cat:"-"|cat:$cierrremes.fec_mes}</label></td>
									<td><label>{$cierrremes.num_disponibilidad_presupuestaria_sum|number_format:2:",":"."}</label></td>
									<td><label>{$cierrremes.num_disponibilidad_financiera_sum|number_format:2:",":"."}</label></td>
                                 
								 
								  <td>
									
								   
                                        {if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}
                                          <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idCierreMes="{$cierrremes.pk_num_ejecucion_presupuestaria}"
                                                descipcion="El Usuario esta consultando un Cierre Mensual" titulo="<i class='icm icm-calculate2'></i> Ver Cierre Mensual">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                          </button>
                                       {/if}
									   
									   
									<!--   {if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modPR/cierremesCONTROL/cierremesReportePdfMET/{$cierrremes.pk_num_ejecucion_presupuestaria}" target="_blank">
                                                <button title="Reporte Cierre Mensual"
                                                        class="generar logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        titulo="Ver Cierre Mensual Pdf" id="generar">
                                                    <i class="md md-assignment"></i>
                                                </button>
                                            </a>
                                        {/if}	-->
                                    </td>   
                                   
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="5">
							<div>
                                {if in_array('PR-01-02-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un Cierre Mensual del sistema"  titulo="Crear Cierre Mensual" id="nuevo" >
                                        Cierre Mensual &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                {/if}
							 </div>
                            </th>
							
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modPR/cierremesCONTROL/crearModificarMET';
       
	   $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCierreMes:0 ,status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });			


        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCierreMes: $(this).attr('idCierreMes'), status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
			$url='{$_Parametros.url}modPR/cierremesCONTROL/crearModificarMET';
			
			//window.open($url, '', 'width=330,height=252,scrollbars=NO,statusbar=NO,left=500,top=250');
            $.post($url,{ idCierreMes: $(this).attr('idCierreMes'), status:'VER'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idCierreMes=$(this).attr('idCierreMes');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/cierremesCONTROL/eliminarMET';
                $.post($url, { idCierreMes: idCierreMes },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idCierreMes'+dato['idCierreMes'])).html('');
                        swal("Eliminado!", "El Cierre Mensual fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>