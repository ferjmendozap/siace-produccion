<form action="{$_Parametros.url}modPR/indicepresupuestarioCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
	     <input type="hidden" value="1" name="valido" />
	   
	   {if $status =='VER'}
            <input type="hidden" value="1" name="ver" id="ver"/>
       {/if}
	   
	    <input type="hidden" value="{$status}" name="status" />
        <input type="hidden" value="{$idIndicePresupuestario}" name="idIndicePresupuestario"/>
		
		
		 <div>
   
  <!-- Tab panes -->
          
		<div class="row">
		
           	 <div class="col-sm-2">
                    <div class="form-group floating-label" id="cod_presupuestoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_cod_indice_presupuestario)}{$formDB.ind_cod_indice_presupuestario}{/if}" 
						onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())"
						name="form[alphaNum][ind_cod_indice_presupuestario]" id="ind_cod_indice_presupuestario">
                        <label for="cod_presupuesto">Cod. Indice Presupuestario</label>
                    </div>
                 </div>
				 
				 
			    <div class="col-lg-4">
			       <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo"  {if isset($veranular)}disabled{/if}  name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo</label>
                  </div>
		      </div> 
		</div>
			  
	    <div class="row">
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_tipo_presupuestoError">
                        <select id="ind_tipo_presupuesto" name="form[alphaNum][ind_tipo_presupuesto]" class="form-control">
                       		
							 {if isset($formDB.ind_tipo_presupuesto) and $formDB.ind_tipo_presupuesto =='G'}
                                <option value="G" selected>PROGRAMA</option>
                                {else}
                                <option value="G">PROGRAMA</option>
                            {/if}					
                            {if isset($formDB.ind_tipo_presupuesto) and $formDB.ind_tipo_presupuesto =='O'}
                                <option selected value="O">PROYECTO</option>
                                {else}
                                <option value="O">PROYECTO</option>
                            {/if}
                            
                        </select>
                        <label for="ind_tipo_presupuesto">Tipo de Presupuesto</label>	 
						
                    </div>
                </div>
				
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_tipo_categoriaError">
                        <select id="ind_tipo_categoria" name="form[alphaNum][ind_tipo_categoria]" class="form-control">
                       		
							 {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='G'}
                                <option value="G" selected>PROGRAMA</option>
                                {else}
                                <option value="G">PROGRAMA</option>
                            {/if}	
							
											
                       <!--     {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='O'}
                                <option selected value="O">PROYECTO</option>
                                {else}
                                <option value="O">PROYECTO</option>
                            {/if}
							
							 {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='C'}
                                <option selected value="C">ACCION CENTRALIZADA</option>
                                {else}
                                <option value="C">ACCION CENTRALIZADA</option>
                            {/if}-->
                            
                        </select>
                      <label for="ind_tipo_categoria">Tipo Categoria Presupuesto</label>	 
						
                    </div>
                </div>
        </div>		
		
		
		  <div class="row">
		      <div class="col-sm-10">
                    <div class="form-group floating-label" id="fk_prb008_num_sectorError">
                        <select id="fk_prb008_num_sector" name="form[alphaNum][fk_prb008_num_sector]" class="form-control dirty tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$sector}
                                {if isset($formDB.fk_prb008_num_sector)}
                                    {if $app.pk_num_sector==$formDB.fk_prb008_num_sector}
                                        <option selected value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb008_num_sector">Seleccione Sector</label>
                    </div>
              </div>
		  </div>
		
		  <div class="row">	  
			  <div class="col-sm-10">
                    <div class="form-group floating-label" id="fk_prb009_num_programaError">
                        <select id="fk_prb009_num_programa" name="form[alphaNum][fk_prb009_num_programa]" class="form-control dirty tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$programa}
                                {if isset($formDB.fk_prb009_num_programa)}
                                    {if $app.pk_num_programa==$formDB.fk_prb009_num_programa}
                                        <option selected value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.num_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.num_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.num_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb009_num_programa">Seleccione Programa</label>
                    </div>
               </div> 
		  </div>		
			
		  <div class="row">	
				<div class="col-sm-10">
                    <div class="form-group floating-label" id="fk_prb010_num_subprogramaError">
                        <select id="fk_prb010_num_subprograma" name="form[alphaNum][fk_prb010_num_subprograma]" class="form-control dirty tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$subprograma}
                                {if isset($formDB.fk_prb010_num_subprograma)}
                                    {if $app.pk_num_subprograma==$formDB.fk_prb010_num_subprograma}
                                        <option selected value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.num_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.num_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.num_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb010_num_subprograma">Seleccione Sub-Programa</label>
                    </div>
                </div> 
			 </div>
				
		
		    <div class="row">
		       <div class="col-sm-10">
                    <div class="form-group floating-label" id="fk_prb005_num_proyectoError">
                        <select id="fk_prb005_num_proyecto" name="form[alphaNum][fk_prb005_num_proyecto]" class="form-control dirty tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$proyecto}
                                {if isset($formDB.fk_prb005_num_proyecto)}
                                    {if $app.pk_num_proyecto==$formDB.fk_prb005_num_proyecto}
                                        <option selected value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.num_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.num_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.num_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                     <!--   <label  value="{if isset($accion)}{$accion}{else}Proyecto{/if}" name="proyecto_accion" id="proyecto_accion">{if isset($accion)}{$accion}{else}Proyecto{/if}</label>-->
					  <label><input type="text" style="border:0;" readonly value="Proyecto" name="proyecto_accion" id="proyecto_accion"></label>
                    </div>
                </div>
            </div> 
		   
		    <div class="row">
				<div class="col-sm-10">
                    <div class="form-group floating-label" id="fk_prb011_num_actividadError">
                        <select id="fk_prb011_num_actividad" name="form[alphaNum][fk_prb011_num_actividad]" class="form-control dirty tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$actividad}
                                {if isset($formDB.fk_prb011_num_actividad)}
                                    {if $app.pk_num_actividad==$formDB.fk_prb011_num_actividad}
                                        <option selected value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.num_cod_actividad|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.num_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.num_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label><input type="text" style="border:0;" readonly value="Actividad" name="actividad_aespecifica" id="actividad_aespecifica"></label>
                    </div>
                </div> 
			 </div>	
			   
			<div class="row">
				<div class="col-sm-10">
                    <div class="form-group floating-label" id="fk_prb012_unidad_ejecutoraError">
                        <select id="fk_prb012_unidad_ejecutora" name="form[alphaNum][fk_prb012_unidad_ejecutora]" class="form-control dirty tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$unidadejecutora}
                                {if isset($formDB.fk_prb012_unidad_ejecutora)}
                                    {if $app.pk_num_unidad_ejecutora==$formDB.fk_prb012_unidad_ejecutora}
                                        <option selected value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                        {else}
                                        <option value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb012_unidad_ejecutora">Seleccione Unidad Ejecutora</label>
                    </div>
                 </div>
		     </div>
			 
            <div class="row">
				<div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked  {elseif  isset($formDB.num_estatus) and $formDB.num_estatus==0}} {else} checked {/if} value="1" name="form[int][num_estatus]" id="num_estatus">
                            <span>Estatus</span>
                        </label>
                    </div>
               </div>
           </div>
		

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
				 <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                 
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
			   
	   <div class="modal-footer">
          <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</          button>
		  
	{if $status!='VER'}	  
         <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            
		    {if $status=='AP'}
                Aprobar
            {elseif $status=='GE'}
                Generar
            {elseif isset($IndicePresupuestario)}
                Modificar
            {else}
                <span class="glyphicon glyphicon-floppy-disk"></span>Guardar
            {/if}
        </button>
	{/if}	
		
       </div>
				   
  </div>
 				
</div>
   
</form>


<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		
		
		
		 var tipo_presupuesto = '{if isset($formDB.ind_tipo_presupuesto)}{$formDB.ind_tipo_presupuesto}{/if}';
		
			  if (tipo_presupuesto=='G')
			  {
				   $(document.getElementById('actividad_aespecifica')).val("Actividad")
				   $(document.getElementById('proyecto_accion')).val("Proyecto")
			  }
			  else if (tipo_presupuesto=='O')
			  {
				   $(document.getElementById('actividad_aespecifica')).val("Accion Especifica")
				   $(document.getElementById('proyecto_accion')).val("Proyecto")
			  }
			 
			  
			
		
		var tipo_categoria = '{if isset($formDB.ind_tipo_categoria)}{$formDB.ind_tipo_categoria}{/if}';
		
		
			  if (tipo_categoria=='G')
			  {
				   $(document.getElementById('actividad_aespecifica')).val("Actividad")
				   $(document.getElementById('proyecto_accion')).val("Proyecto")
			  }
			  else if (tipo_categoria=='O')
			  {
				   $(document.getElementById('actividad_aespecifica')).val("Accion Especifica")
				   $(document.getElementById('proyecto_accion')).val("Proyecto")
		      }
			  else if (tipo_categoria=='C')
			  {
				   $(document.getElementById('actividad_aespecifica')).val("Accion Especifica")
				   $(document.getElementById('proyecto_accion')).val("Accion Centralizada")
		      }
			  
		var pr = new  PrFunciones();
		   
		 
		var nuevo=$(document.getElementById("idIndicePresupuestario")).val();
		  if (nuevo==0){		
		  	
		    var sector = '{if isset($formDB.fk_prb008_num_sector)}{$formDB.fk_prb008_num_sector}{/if}';
		    var proyecto = '{if isset($formDB.fk_prb005_num_proyecto)}{$formDB.fk_prb005_num_proyecto}{/if}';
		    pr.metJsonProyecto('{$_Parametros.url}modPR/indicepresupuestarioCONTROL/JsonProyectoMET', sector, proyecto);
			
			var programa = '{if isset($formDB.fk_prb009_num_programa)}{$formDB.fk_prb009_num_programa}{/if}';
		    var subprograma = '{if isset($formDB.fk_prb010_num_subprograma)}{$formDB.fk_prb010_num_subprograma}{/if}';
		    pr.metJsonSubPrograma('{$_Parametros.url}modPR/indicepresupuestarioCONTROL/JsonSubProgramaMET', programa, subprograma);
			
		    var proyecto = '{if isset($formDB.fk_prb005_num_proyecto)}{$formDB.fk_prb005_num_proyecto}{/if}';
		    var actividad = '{if isset($formDB.fk_prb011_num_actividad)}{$formDB.fk_prb011_num_actividad}{/if}';
		    pr.metJsonActividad('{$_Parametros.url}modPR/indicepresupuestarioCONTROL/JsonActividadMET', proyecto, actividad);
		 }	
		 
			var tipo_presupuesto = '{if isset($formDB.ind_tipo_presupuesto)}{$formDB.ind_tipo_presupuesto}{/if}';
		    var tipo_categoria = '{if isset($formDB.ind_tipo_categoria)}{$formDB.ind_tipo_categoria}{/if}';
		    pr.metJsonCategoria('{$_Parametros.url}modPR/indicepresupuestarioCONTROL/JsonCategoriaMET', tipo_presupuesto);
		
		
	        var idtipopresupuesto = '{if isset($formDB.ind_tipo_presupuesto)}{$formDB.ind_tipo_presupuesto}{/if}';

		
		/***************************DESHABILITAR CONTROLES*****************************/
		   var disabled=$(document.getElementById("ver")).val();	   
		   if (disabled==1){
		       $("#formAjax input,textarea").prop("readonly", true);
			   $("#formAjax select").prop("disabled", true);
			   $("#formAjax input" ).removeClass('fechas2');
			   $("#formAjax input" ).removeClass('fecha_anio');
		   }
	   /********************************************************************************/	
	   
	   $('#ind_tipo_presupuesto').change(function () { 
	    var tipo_presupuesto=$(this).val();
		
	     if (tipo_presupuesto=='G')
			  {
			   $(document.getElementById('actividad_aespecifica')).val("Actividad")
			  }
			 else 
			  {
			     $(document.getElementById('actividad_aespecifica')).val("Accion Especifica")
			  }
	   });
	   
	   
	     $('#ind_tipo_categoria').change(function () { 
		  var tipo_categoria=$(this).val();
		  
	          if (tipo_categoria=='C')
			  {
			      $(document.getElementById('proyecto_accion')).val("Accion Centralizada")
			  }
			  else 
			  {
			     $(document.getElementById('proyecto_accion')).val("Proyecto")
			  }
	   });
	
	
        $('#modalAncho').css("width","85%");
		
        $('#accion').click(function(){
		
		    swal({
                title: "�Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
		  
		    $( ".tipo" ).prop( "disabled", false ); 
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
                }else if(dato['status']=='modificar'){
			       
                    if(dato['num_estatus']==1){ var icono='md md-check' }else{ var icono='md md-not-interested' } 
					
					var cod_sector=document.getElementById('fk_prb008_num_sector').options[document.getElementById('fk_prb008_num_sector').selectedIndex].text;
					var cod_programa=document.getElementById('fk_prb009_num_programa').options[document.getElementById('fk_prb009_num_programa').selectedIndex].text;
					var cod_subprograma=document.getElementById('fk_prb010_num_subprograma').options[document.getElementById('fk_prb010_num_subprograma').selectedIndex].text;
					var cod_proyecto=document.getElementById('fk_prb005_num_proyecto').options[document.getElementById('fk_prb005_num_proyecto').selectedIndex].text;
					var cod_actividad=document.getElementById('fk_prb011_num_actividad').options[document.getElementById('fk_prb011_num_actividad').selectedIndex].text;
					var cod_unidad_ejecutora=document.getElementById('fk_prb012_unidad_ejecutora').options[document.getElementById('fk_prb012_unidad_ejecutora').selectedIndex].text;
					
					 $(document.getElementById('idIndicePresupuestario'+dato['idIndicePresupuestario'])).html(
                            '<td>'+dato['ind_cod_indice_presupuestario']+'</td>' +
                            '<td>'+cod_sector.substring(0,3)+'</td>' +
							'<td>'+cod_programa.substring(0,3)+'</td>' +
							'<td>'+cod_subprograma.substring(0,3)+'</td>' +
							'<td>'+cod_proyecto.substring(0,3)+'</td>' +
							'<td>'+cod_actividad.substring(0,3)+'</td>' +
							'<td>'+cod_unidad_ejecutora.substring(0,3)+'</td>' +
							'<td><i class="'+icono+'"></i></td>' +
                            '<td  align="left">' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idIndicePresupuestario="'+dato['idIndicePresupuestario']+'"' +
                            'descipcion="El Usuario a Modificado un Presupuesto" titulo="Modificar Presupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idIndicePresupuestario="'+dato['idIndicePresupuestario']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Presupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Presupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}&nbsp;'+
							
							'{if in_array('PR-01-03-01-01-V',$_Parametros.perfil)}'+
                            '<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal" idIndicePresupuestario="'+dato['idIndicePresupuestario']+'"'+
                            'descipcion="El Usuario esta viendo un Indice Presupuestario" titulo="Ver Indice Presupuestario" >'+
                            '<i class="md md-remove-red-eye" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
							
                            '</td>');
                    swal("Registro Modificado!", "El Presupuesto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
                }else if(dato['status']=='nuevo'){
                   
				    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
					
					var cod_sector=document.getElementById('fk_prb008_num_sector').options[document.getElementById('fk_prb008_num_sector').selectedIndex].text;
					var cod_programa=document.getElementById('fk_prb009_num_programa').options[document.getElementById('fk_prb009_num_programa').selectedIndex].text;
					var cod_subprograma=document.getElementById('fk_prb010_num_subprograma').options[document.getElementById('fk_prb010_num_subprograma').selectedIndex].text;
					var cod_proyecto=document.getElementById('fk_prb005_num_proyecto').options[document.getElementById('fk_prb005_num_proyecto').selectedIndex].text;
					var cod_actividad=document.getElementById('fk_prb011_num_actividad').options[document.getElementById('fk_prb011_num_actividad').selectedIndex].text;
					var cod_unidad_ejecutora=document.getElementById('fk_prb012_unidad_ejecutora').options[document.getElementById('fk_prb012_unidad_ejecutora').selectedIndex].text;

					
                    $(document.getElementById('datatable1')).append('<tr  idIndicePresupuestario="idIndicePresupuestario'+dato['idIndicePresupuestario']+'">' +
                            '<td>'+dato['ind_cod_indice_presupuestario']+'</td>' +
                            '<td>'+cod_sector.substring(0,3)+'</td>' +
							'<td>'+cod_programa.substring(0,3)+'</td>' +
							'<td>'+cod_subprograma.substring(0,3)+'</td>' +
							'<td>'+cod_proyecto.substring(0,3)+'</td>' +
							'<td>'+cod_actividad.substring(0,3)+'</td>' +
							'<td>'+cod_unidad_ejecutora.substring(0,3)+'</td>' +
							'<td><i class="'+icono+'"></i></td>' +
                            '<td  align="left">' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idIndicePresupuestario="'+dato['idIndicePresupuestario']+'"' +
                            'descipcion="El Usuario a Modificado un Atepresupuesto" titulo="Modificar Presupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idIndicePresupuestario="'+dato['idIndicePresupuestario']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Presupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Presupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}&nbsp;'+
							
							'{if in_array('PR-01-03-01-01-V',$_Parametros.perfil)}'+
                            '<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal" idIndicePresupuestario="'+dato['idIndicePresupuestario']+'"'+
                            'descipcion="El Usuario esta viendo un Indice Presupuestario" titulo="Ver Indice Presupuestario" >'+
                            '<i class="md md-remove-red-eye" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
							
                            '</td>');
                    swal("Registro Modificado!", "El Presupuesto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });

	

</script>

