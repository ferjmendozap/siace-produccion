<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Indice Presupuestario - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod. Indice Presupuestario</th>
                                <th>Sector</th>
								<th>Programa</th>
								<th>Sub_programa</th>
								<th>Proyecto</th>
								<th>Actividad</th>
								<th>Unidad Ejecutora</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=indicepresupuestario from=$listado}
                                <tr id="idIndicePresupuestario{$indicepresupuestario.pk_num_indice_presupuestario}">
								    <td><label>{$indicepresupuestario.ind_cod_indice_presupuestario}</label></td>
                                    <td><label>{$indicepresupuestario.ind_cod_sector}</label></td>
                                    <td><label>{$indicepresupuestario.ind_cod_programa}</label></td>
									<td><label>{$indicepresupuestario.ind_cod_subprograma}</label></td>
									<td><label>{$indicepresupuestario.ind_cod_proyecto}</label></td>
									<td><label>{$indicepresupuestario.ind_cod_actividad}</label></td>
									<td><label>{$indicepresupuestario.cod_unidad_ejecutora}</label></td>
                                    <td>
									<!--<label>{$presupuesto.ind_estado}</label>-->
                                      <i class="{if $indicepresupuestario.estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="left">
									
                                      
                                            {if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}
                                             <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idIndicePresupuestario="{$indicepresupuestario.pk_num_indice_presupuestario}"
                                                    descipcion="El Usuario a Modificado un indice presupuestario del sistema" titulo="Modificar un indice presupuestario">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                             </button>
                                            {/if}
											
										    {if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idIndicePresupuestario=  
											"{$indicepresupuestario.pk_num_indice_presupuestario}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un indice presupuestario del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea 
													eliminar el indice presupuestario!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                            {/if}
										
										  {if in_array('PR-01-03-01-01-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idIndicePresupuestario="{$indicepresupuestario.pk_num_indice_presupuestario}" title="Ver Indice Presupuestario"
                                                descipcion="El Usuario esta viendo un Indice Presupuestario" titulo="<i class='icm icm-calculate2'></i> Ver Indice Presupuestario">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                          {/if}	
										
                                    &nbsp;&nbsp;
                                      
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="9">
							<div>
							  
                                {if in_array('PR-01-02-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una antepresupuesto del sistema" title="Nuevo Indice Presupuestario" titulo="Crear Indice Presupuestario" id="nuevo" >
                                        Nuevo Indice Presupuestario<i class="md md-create"></i>
                                    </button>
                                {/if}
							 
							  </div>	
							
                            </th>
							
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modPR/indicepresupuestarioCONTROL/crearModificarMET';
       
	   $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idIndicePresupuestario:0 ,status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
				

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idIndicePresupuestario: $(this).attr('idIndicePresupuestario'), status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
			
		
		$('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idIndicePresupuestario: $(this).attr('idIndicePresupuestario'), status:'VER'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idIndicePresupuestario: $(this).attr('idIndicePresupuestario'), status:'AP'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.generar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idIndicePresupuestario: $(this).attr('idIndicePresupuestario'), status:'GE'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
      
	
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idIndicePresupuestario=$(this).attr('idIndicePresupuestario');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/indicepresupuestarioCONTROL/eliminarMET';
                $.post($url, { idIndicePresupuestario: idIndicePresupuestario },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idIndicePresupuestario'+dato['idIndicePresupuestario'])).html('');
                        swal("Eliminado!", "El indice prepuestario fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>