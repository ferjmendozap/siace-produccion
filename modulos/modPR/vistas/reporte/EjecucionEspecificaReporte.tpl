<!--********************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Presupuesto
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           Liduvica Bastardo            |          liduvica@hotmail.com       |         04249080200            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-12-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
************************************************************************************************************************
 -->
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">EJECUCION ESPECIFICA </h2></header>
    </div>
	
    <div class="col-md-12">
        <h5>Reporte Especifica</h5>
    </div><!--end .col -->
</div><!--end .card -->

<div class="card">
    <div class="card-head">
        <header>Búsqueda</header>
    </div>


        <form   class="form" action="{$_Parametros.url}modPR/reporteCONTROL/ejecucionespecificaReportePdfMET"   method="post" target="iReporte">
		
		  <div class="col-lg-4">
			  <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo"  {if isset($veranular)}disabled{/if}  name="form[int][fk_a001_num_organismo]" class="form-control dirty" >
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo</label>
                 </div>
		    </div> 
		
		 <div class="col-sm-1">
             <div class="form-group floating-label">
                        <input type="text" class="form-control dirty" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}" name="form[txt][fec_anio]" id="fec_anio">
                        <label for="fec_anio">A&ntilde;o</label>
             </div>
        </div>
		
        <div class="col-sm-2">
            <div class="form-group">
                <select id="idpresupuesto" class="form-control" name="form[int][idpresupuesto]">
                    <option value=""> </option>
                    {foreach item=tipo from=$_PresupuestoPost}

                            <option value="{$tipo.pk_num_presupuesto}"> {$tipo.ind_cod_presupuesto}</option>

                    {/foreach}
                </select>
                <label for="select1">Presupuesto</label>
            </div>
		 </div>	
			
			 
		 <div class="col-sm-2">
              <div class="form-group floating-label" >
                        <select id="estado" name="form[txt][estado]" class="form-control dirty">
                           
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==1}
                                <option selected value="CO">Comprometido</option>
                                {else}
                                <option value="CO">Comprometido</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==2}
                                <option value="CA" selected>Causado</option>
                                {else}
                                <option value="CA">Causado</option>
                            {/if}
                            {if isset($formDB.num_nivel) and $formDB.num_nivel ==3}
                                <option value="PA" selected>Pagado</option>
                                {else}
                                <option value="PA">Pagado</option>
                            {/if}
                        </select>
                        <label for="num_nivel">Estado</label>
               </div>
          </div>

            <div class="col-sm-4">
                <div class="col-sm-2">
                    <label for="pago"
                           class="control-label" style="margin-top: 10px;"> Periodo: </label>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control text-center date"
                           id="desde"
                           name="form[txt][desde]"
                           style="text-align: center"
                           value=""
                           placeholder="Desde"
                           readonly>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control text-center date"
                           id="hasta"
                           style="text-align: center"
                           value=""
                           name="form[txt][hasta]"
                           placeholder="Hasta"
                           readonly>
                </div>
            </div>
			 

     
	  
	<div class="form-group   col-lg-12 " align="center">
		<button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:7px">Buscar</button>
		</button>
	</div>

</div><!--end .card -->

</form>

<section class="style-default-bright">

<div align="center" style="width:100%; background:#66CCCC">&nbsp;&nbsp;<font color="#FFFFFF"><b>Ejecuci&oacute;n Especifica</b></font></div>
    <div id="contenidoReporteGeneral">
          <center><iframe name="iReporte" id="iReporte" style="border:solid 1px #CDCDCD; width:1000px; height:400px;"></iframe></center>
    </div>
</div>	

    <script type="text/javascript">

        $(document).ready(function() {

            /// Complementos
            $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

		    var pr = new  PrFunciones();
	        var anio = '{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}';
            pr.metJsonPresupuestoReporte('{$_Parametros.url}modPR/reporteCONTROL/JsonPresupuestoReporteMET',anio);
			
            $('#modalAncho').css("width", "85%");
            $('#fechas').datepicker({ format: 'dd/mm/yyyy' });
			$('#fec_anio').datepicker({ format: 'yyyy', viewMode: "years", minViewMode: "years" }); 

        });
    </script>


