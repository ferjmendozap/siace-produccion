<form action="{$_Parametros.url}modPR/presupuestoCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
	     <input type="hidden" value="1" name="valido" />
	   
	  <!-- {if $status =='AP' || $status =='GE'}-->
            <input type="hidden" value="1" name="ver" id="ver"/>
      <!--  {/if}-->
	   
	    <input type="hidden" value="{$status}" name="status" />
        <input type="hidden" value="{$idPresupuesto}" name="idPresupuesto"/>
		
		
		 <div>
        
         <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab">Datos Generales</a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab">Detalle de Presupuesto</a></li> 
             </ul>

  <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="Datos">Datos Generales
			   
		<div class="row">
		
           	 <div class="col-sm-2">
                    <div class="form-group floating-label" id="cod_presupuestoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_cod_presupuesto)}{$formDB.ind_cod_presupuesto}{/if}" name="form[alphaNum][ind_cod_presupuesto]" id="ind_cod_presupuesto">
                        <label for="cod_presupuesto">Cod. Presupuesto</label>
                    </div>
                 </div>
				 
				 
			    <div class="col-lg-4">
			       <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo"  {if isset($veranular)}disabled{/if}  name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo</label>
                  </div>
		      </div> 
			 
		</div>
			  
		 <div class="row">
		
		       <div class="col-sm-2">
                    <div class="form-group floating-label" id="fec_anioError">
                        <input type="text" class="form-control" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{/if}" name="form[txt][fec_anio]" id="fec_anio">
                        <label for="fec_anio">A&ntilde;o</label>
                    </div>
                 </div>
				 
				  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_presupuestoError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_presupuesto)}{$formDB.fec_presupuesto|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_presupuesto]" id="fec_presupuesto">
					          <label for="fec_presupuesto"> <i class="fa fa-calendar"></i> Fecha Presupuesto</label>
                            
                    </div>
                </div>
       
                <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_inicioError">
                      
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_inicio)}{$formDB.fec_inicio|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_inicio]" id="fec_inicio">
					          <label for="fec_inicio"><i class="fa fa-calendar"></i> Fecha Inicio</label>
                           
                    </div>
                </div>
       	    
			     <div class='col-sm-2'>
                   <div class="form-group floating-label" id="dec_finError">
                      
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_fin)}{$formDB.fec_fin|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_fin]" id="fec_fin">
					          <label for="fec_fin"><i class="fa fa-calendar"></i> Fecha Fin</label>
                           
                    </div>
                </div>
				
				
		 </div>
						
		<div class="row">
		
		   <div class="col-sm-2">
                <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_gaceta)}{$formDB.ind_numero_gaceta}{/if}" name="form[alphaNum][ind_numero_gaceta]" id="ind_numero_gaceta">
                        <label for="ind_numero_gaceta">Numero Gaceta</label>
                 </div>
            </div>	
				
				
			  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_gacetaError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_gaceta)}{$formDB.fec_gaceta|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_gaceta]">
					          <label for="fec_gaceta"><i class="fa fa-calendar"></i> Fecha Gaceta</label>
                           
                    </div>
                </div>		

         </div>
			
			
	    <div class="row">
		
		   <div class="col-sm-2">
                <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_decreto)}{$formDB.ind_numero_decreto}{/if}" name="form[alphaNum][ind_numero_decreto]" id="ind_numero_decreto">
                        <label for="ind_numero_decreto">Numero Decreto</label>
                 </div>
            </div>	
			
			
			  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fecha_decretoError">
                     
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fecha_decreto)}{$formDB.fecha_decreto|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fecha_decreto]" >
					          <label for="fecha_decreto"><i class="fa fa-calendar"></i> Fecha Decreto</label>
                           
                    </div>
                </div>	
				
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_tipo_presupuestoError">
                        <select disabled="disabled" id="ind_tipo_presupuesto" name="form[alphaNum][ind_tipo_presupuesto]" class="form-control">
                       		
							 {if isset($formDB.ind_tipo_presupuesto) and $formDB.ind_tipo_presupuesto =='G'}
                                <option value="G" selected>PROGRAMA</option>
                                {else}
                                <option value="G">PROGRAMA</option>
                            {/if}					
                            {if isset($formDB.ind_tipo_presupuesto) and $formDB.ind_tipo_presupuesto =='O'}
                                <option selected value="O">PROYECTO</option>
                                {else}
                                <option value="O">PROYECTO</option>
                            {/if}
                           
                            
                        </select>
                        <label for="ind_tipo_presupuesto">Tipo de Presupuesto</label>	 
						
                    </div>
                </div>
				
				 <div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_tipo_categoriaError">
                        <select id="ind_tipo_categoria" name="form[alphaNum][ind_tipo_categoria]" class="form-control">
                       		
							 {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='G'}
                                <option value="G" selected>PROGRAMA</option>
                                {else}
                                <option value="G">PROGRAMA</option>
								
                            {/if}					
                          {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='O'}
                                <option selected value="O">PROYECTO</option>
                                {else}
                                <option value="O">PROYECTO</option>
                            {/if}
							 {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='C'}
                                <option selected value="C">ACCION CENTRALIZADA</option>
                                {else}
                                <option value="C">ACCION CENTRALIZADA</option>
                            {/if}
                            
                        </select>
                      <label for="ind_tipo_categoria">Tipo Categoria Presupuesto</label>	 
						
                    </div>
                </div>
				
        </div>		
		
		
		<div class="row">
		      <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb008_num_sectorError">
                        <select id="fk_prb008_num_sector" name="form[alphaNum][fk_prb008_num_sector]" class="form-control tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$sector}
                                {if isset($formDB.fk_prb008_num_sector)}
                                    {if $app.pk_num_sector==$formDB.fk_prb008_num_sector}
                                        <option selected value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb008_num_sector">Seleccione Sector</label>
                    </div>
              </div>
			  
			  
			<!--  <div class="col-sm-3">
                <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_cod_sector)}{$formDB.ind_cod_sector}{/if}" name=
						"form[alphaNum][ind_cod_sector]" id="ind_cod_sector">
                        <label for="ind_cod_sector">Seleccione Sector</label>
                 </div>
             </div>	-->
			  
			  
			  <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb009_num_programaError">
                        <select id="fk_prb009_num_programa" name="form[alphaNum][fk_prb009_num_programa]" class="form-control tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$programa}
                                {if isset($formDB.fk_prb009_num_programa)}
                                    {if $app.pk_num_programa==$formDB.fk_prb009_num_programa}
                                        <option selected value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb009_num_programa">Seleccione Programa</label>
                    </div>
                </div> 
				
				
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb010_num_subprogramaError">
                        <select id="fk_prb010_num_subprograma" name="form[alphaNum][fk_prb010_num_subprograma]" class="form-control tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$subprograma}
                                {if isset($formDB.fk_prb010_num_subprograma)}
                                    {if $app.pk_num_subprograma==$formDB.fk_prb010_num_subprograma}
                                        <option selected value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb010_num_subprograma">Seleccione Sub-Programa</label>
                    </div>
                </div> 
				
			
				
           </div>
		
		
		   <div class="row">
		   
		   <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb005_num_proyectoError">
                        <select id="fk_prb005_num_proyecto" name="form[alphaNum][fk_prb005_num_proyecto]" class="form-control tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$proyecto}
                                {if isset($formDB.fk_prb005_num_proyecto)}
                                    {if $app.pk_num_proyecto==$formDB.fk_prb005_num_proyecto}
                                        <option selected value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb005_num_proyecto"> <label><input type="text" style="border:0;" readonly value="Proyecto" name="proyecto_accion" id="proyecto_accion" /></label></label>
                    </div>
                </div>
		   
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb011_num_actividadError">
                        <select id="fk_prb011_num_actividad" name="form[alphaNum][fk_prb011_num_actividad]" class="form-control tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$actividad}
                                {if isset($formDB.fk_prb011_num_actividad)}
                                    {if $app.pk_num_actividad==$formDB.fk_prb011_num_actividad}
                                        <option selected value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label><input type="text" style="border:0;" readonly  value="Actividad" name="actividad_aespecifica" id="actividad_aespecifica"></label>
                    </div>
                </div> 
				
			
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb012_unidad_ejecutoraError">
                        <select id="fk_prb012_unidad_ejecutora" name="form[alphaNum][fk_prb012_unidad_ejecutora]" class="form-control tipo">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$unidadejecutora}
                                {if isset($formDB.fk_prb012_unidad_ejecutora)}
                                    {if $app.pk_num_unidad_ejecutora==$formDB.fk_prb012_unidad_ejecutora}
                                        <option selected value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                        {else}
                                        <option value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb012_unidad_ejecutora">Seleccione Unidad Ejecutora</label>
                    </div>
                 </div>
				  
		     </div>
		
		
			

        <div class="row">
				  <div class="col-sm-5">
                    <div class="form-group floating-label"  id="num_monto_aprobadoError">
                        <input type="text" class="form-control monto" value="{if isset($formDB.num_monto_aprobado)}{$formDB.num_monto_aprobado|number_format:2:",":"."}{/if}" name=  
						"form[int][num_monto_aprobado]" id="num_monto_aprobado">
                        <label for="num_monto_aprobado">Monto Aprobado</label>
                    </div>
                 </div>
		  
		     
			    <div class="col-sm-5">
                    <div class="form-group floating-label" >
                        <input type="text" class="form-control monto" value="{if isset($formDB.num_monto_ajustado)}{$formDB.num_monto_ajustado|number_format:2:",":"."}{/if}" name=  
						"form[int][num_monto_ajustado]" id="num_monto_ajustado">
                        <label for="num_monto_ajustado">Monto Ajustado</label>
                    </div>
                 </div>
		



                <div class="col-sm-2">
                    <div class="form-group floating-label" >
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_estado)}{$formDB.ind_estado}{/if}" name="form[alphaNum][ind_estado]" id="ind_estado">
                        <label for="ind_estado">Estatus</label>
                    </div>
                 </div>
        </div>
		

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
				 <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                 
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
			   
	   <div class="modal-footer">
          <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</          button>
		  
	{if $status!='VER'}	  
         <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            
		    {if $status=='AP'}
                Aprobar
            {elseif $status=='GE'}
                Generar
            {elseif isset($idPresupuesto)}
                Modificar
            {else}
                <span class="glyphicon glyphicon-floppy-disk"></span>Guardar
            {/if}
        </button>
	{/if}	
		
       </div>
				   
 </div>
             
		 
	<!-----------DETALLE PRESUPUESTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">Detalle de Presupuesto
			   
			      <div>
				  
				 <div class="table-responsive">
                                      <table class="table no-margin" id="tablapartida">
                                                        <thead>
                                                        <tr>
                                                            <th>Codigo</th>
                                                            <th width="33%">Partida</th>
															<th width="140">Monto Ajustado</th>
															<th width="140">Monto Compromiso </th>
															<th width="140">Monto Causado </th>
															<th width="140">Monto Pagado </th>
															<th width="140">Monto Disponoble</th>
															<!--<th width="5">Aespecifica</th>-->
                                                           <!-- <th>Acciones</th>-->
                                                        </tr>
                                                        </thead>
                                                        <tbody  id="codpartida">
                                                       
														  {if isset($partidas)}
                                                            {foreach item=i from=$partidas}
															   
                                                                <tr class="idPartida{$i.id}">
                                                                    <input type="hidden" value="{$i.id}" name="form[int][fk_prb002_num_partida_presupuestaria][]" class="partidaInput" partida="{$i.cod}" id="{$i.id}"/> 							
															 {if $i.tipo=='E'}			
																	<td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.cod}</td>
                                                                    <td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.partida}</td>
																   
																	
															 {else if $i.tipo=='T'}			
																	<th>{$i.cod}</th>
                                                                    <th  id >{$i.partida}</th>
																    <th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</th>
																	
																	<!-------------------------------COMPROMISO--------------------------------->
																	<th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto_compromiso|number_format:2:",":"."}" name="form[int][monto_compromiso][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</th>
																	
																	<!-------------------------------CAUSADO--------------------------------->
																	<th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto_causado|number_format:2:",":"."}" name="form[int][monto_causado][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</th>
																	
																	
																	<!-------------------------------PAGADO--------------------------------->
																	<th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto_pagado|number_format:2:",":"."}" name="form[int][monto_pagado][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</th>
																	
																	<!-------------------------------DISPONIBLE--------------------------------->
																	<th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto_disponible|number_format:2:",":"."}" name="form[int][monto_disponible][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</th>
																	
																{else}			
                                                                    <td>{$i.cod}</td>
                                                                    <td>{$i.partida}</td>
																    <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</td>
																
																	<!-------------------------------COMPROMISO--------------------------------->
																	<td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto_compromiso|number_format:2:",":"."}" name="form[int][monto_compromiso][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	<!-------------------------------CAUSADO--------------------------------->
																	<td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto_causado|number_format:2:",":"."}" name="form[int][monto_causado][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</td>
																	<!-------------------------------PAGADO--------------------------------->
																	<td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto_pagado|number_format:2:",":"."}" name="form[int][monto_pagado][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</td>
																	<!-------------------------------DISPONIBLE--------------------------------->
																	<td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto_disponible|number_format:2:",":"."}" name="form[int][monto_disponible][{$i.id}]" id="{$i.cod}" >
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	
																 {/if} 
                                                                  
                                                                </tr>
															  {/foreach}	
                                                           
														 {/if}	
                                                     
                                                        </tbody>
														
														 <!-- Total  -->
														<tr>
														   <td align="right"  style="font-weight:bold" colspan="2">
														      TOTAL=
														   </td>
														   
														    <th id="total" align="right"  style="text-align:right; font-family: Arial, Helvetica, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total|number_format:2:",":"."}" disabled name="monto_total" id="monto_total" />                            
															</th> 
															
															<!-----------------------------------------COMPROMISO----------------------------------------------->
															
															<th id="total" align="right"  style="text-align:right; font-family:Arial, Helvetica, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total_compromiso|number_format:2:",":"."}" disabled name="monto_total_compromiso" id="monto_total_compromiso" />                            
															</th> 
															
															<!-----------------------------------------CAUSADO----------------------------------------------->
															
															<th id="total" align="right"  style="text-align:right; font-family:Arial, Helvetica, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total_causado|number_format:2:",":"."}" disabled name="monto_total_causado" id="monto_total_causado" />                            
															</th> 
															
															<!-----------------------------------------PAGADO----------------------------------------------->
															
															<th id="total" align="right"  style="text-align:right; font-family:Arial, Helvetica, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total_pagado|number_format:2:",":"."}" disabled name="monto_total_pagado" id="monto_total_pagado" />                            
															</th> 
															
															<!-----------------------------------------DISPONIBLE----------------------------------------------->
															
															<th id="total" align="right"  style="text-align:right; font-family:Arial, Helvetica, sans-serif; font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total_disponible|number_format:2:",":"."}" disabled name="monto_total_disponible" id="monto_total_disponible" />                            
															</th> 
															
															
														</tr>
														
                                                        <tfoot>
                                                       
                                                        </tfoot>
                                                    </table>
                                                </div>
															
 			     
			      </div>
				  <div class="modal-footer">
                     <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</          button>
                  </div>
				  
			   </div>
			   
           </div>
		   
       </div>
		
    </div>
   
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		
		 var tipo_presupuesto = $(document.getElementById('ind_tipo_presupuesto')).val();
		
		  
			  if (tipo_presupuesto=='G')
			  {
				   $(document.getElementById('actividad_aespecifica')).val("Actividad")
				   $(document.getElementById('proyecto_accion')).val("Proyecto")
			  }
			  else 
			  {
				   $(document.getElementById('actividad_aespecifica')).val("Accion Especifica")
				   $(document.getElementById('proyecto_accion')).val("Proyecto")
			  }
		
		
			 $("#formAjax input,textarea,select").prop("disabled", true);
			 $("#actividad_aespecifica").prop( "disabled", false );
			 $("#proyecto_accion").prop( "disabled", false );
			 
		
			 $('#agregarPartida').click(function () {
				$('#formModalLabel2').html($(this).attr('titulo'));
				$.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
					$('#ContenidoModal2').html($dato);
				});
			 });
			
		
			 $('#codpartida').on('click', '.borrar', function () {
				var borrar = $(this);
				$(document.getElementsByClassName(borrar.attr('borrar'))).remove();
			 });
		
		
		$('#codpartida').change(function () {
		
		
		var suma=0;
		var total=0;
		var codigo=0; 
		var codigo1=0; 
		var partida1='';
		var monto=0;
		
		
		  $("#codpartida th").each(function (){
				 $(this).find('input').each (function() {
				     idpartida1 = $(this).attr('id');
					 suma=actualizarMonto(idpartida1);
					 $(this).val(suma);
	             }) //imput
			 })	 
			 
			 
			  $("#codpartida th").each(function (){
		   
				 $(this).find('input').each (function() {
				 idpartida1 = $(this).attr('id');
					  codigo1= idpartida1.substring(4, 12);
					  if (codigo1=="00.00.00"){
					     monto= $(this).val();
					     total=total+parseInt(monto);
				  }
						 	 			 
	             }) //imput
			 })	
			  
			  
	  		 
			 $("#total").each(function (){
		   
				  $(this).find('input').each (function() {
				 
				   $(this).val(total);
						 
	             }) //imput
			 })	
         });
		
   	  
		
        $('#modalAncho').css("width","100%");
		
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
					  }else if(dato['status']=='aprobado'){
                   
                    //if(dato['ind_estado']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idPresupuesto'+dato['idPresupuesto'])).html('<td>'+dato['cod_presupuesto']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_inicio']+'</td>' +
							'<td>'+dato['dec_fin']+'</td>' +
                            '<td>'+dato['num_monto_presupuestado']+'</td>' +
							'<td>'+dato['ind_estado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPresupuesto="'+dato['idPresupuesto']+'"' +
                            'descipcion="El Usuario a Modificado un Presupuesto" titulo="Modificar Presupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPresupuesto="'+dato['idPresupuesto']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Presupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Presupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Presupuesto fue aprobado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					
					//----------------------------------------------------
					
			   }else if(dato['status']=='generado'){
                   
					$(document.getElementById('idPresupuesto'+dato['idPresupuesto'])).html('');		
                    swal("Registro Generado!", "El Presupuesto fue generado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					//------------------------------------------------------
					
                }else if(dato['status']=='modificar'){
                   
                    //if(dato['ind_estado']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idPresupuesto'+dato['idPresupuesto'])).html('<td>'+dato['cod_presupuesto']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_inicio']+'</td>' +
							'<td>'+dato['dec_fin']+'</td>' +
                            '<td>'+dato['num_monto_presupuestado']+'</td>' +
							'<td>'+dato['ind_estado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPresupuesto="'+dato['idPresupuesto']+'"' +
                            'descipcion="El Usuario a Modificado un Presupuesto" titulo="Modificar Presupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPresupuesto="'+dato['idPresupuesto']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Presupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Presupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Presupuesto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
                }else if(dato['status']=='nuevo'){
                   
                    $(document.getElementById('datatable1')).append('<tr  id="idPresupuesto'+dato['idPresupuesto']+'">' +
                            '<td>'+dato['cod_presupuesto']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_inicio']+'</td>' +
							'<td>'+dato['dec_fin']+'</td>' +
                            '<td>'+dato['num_monto_presupuestado']+'</td>' +
							'<td>'+dato['ind_estado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPresupuesto="'+dato['idPresupuesto']+'"' +
                            'descipcion="El Usuario a Modificado un Atepresupuesto" titulo="Modificar Presupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPresupuesto="'+dato['idPresupuesto']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Presupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Presupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Presupuesto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });

	
function actualizarMonto(CodPartida) {
var suma=0;
		var suma=0; 
		var codigo=""; 
		var codigo1=""; 
		

   $("#codpartida td").each(function (){
		   
				 $(this).find('input').each (function() {
				     idpartida1 = $(this).attr('id');				  
					 codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 if (CodPartida==codigo){
					     monto= $(this).val();
					     suma=suma+parseInt(monto);
						 }
					 
					 
					 codigo1= idpartida1.substring(0, 3);
					 codigo1= codigo1.concat(".00.00.00");
					 
					 if (CodPartida==codigo1){
					     monto= $(this).val();
					     suma=suma+parseInt(monto);
						 }
	             }) //imput
			 })	
			 
			 return suma; 
}
	
</script>

