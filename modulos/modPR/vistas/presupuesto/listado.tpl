<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Presupuesto - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod. Presupuesto</th>
                                <th>Ejer. Presupuestario</th>
								<th>Fecha Inicio</th>
								<th>Fecha Fin</th>
								<th>Monto</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=presupuesto from=$listado}
                                <tr id="idPresupuesto{$presupuesto.pk_num_presupuesto}">
                                    <td><label>{$presupuesto.ind_cod_presupuesto}</label></td>
                                    <td><label>{$presupuesto.fec_anio}</label></td>
									<td><label>{$presupuesto.fec_inicio|date_format:" %d-%m-%Y"}</label></td>
									<td><label>{$presupuesto.fec_fin|date_format:" %d-%m-%Y"}</label></td>
									<td><label>{$presupuesto.num_monto_aprobado|number_format:2:",":"."}</label></td>
                                    <td>
									<!--<label>{$presupuesto.ind_estado}</label>-->
                                     <label>{if $presupuesto.ind_estado=='PR'}Preparaci&oacute;n{else if $presupuesto.ind_estado=='AP'}Aprobado{else if     
									 $presupuesto.ind_estado=='GE'}Generado{/if}</label>
                                    </td>
                                    <td align="center">
									 {if !$status}
                                       {if $presupuesto.ind_estado=='PR'}
                                            {if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}
                                             <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idPresupuesto="{$presupuesto.pk_num_presupuesto}"
                                                    descipcion="El Usuario a Modificado un presupuesto del sistema" titulo="Modificar presupuesto">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                             </button>
                                            {/if}
											
										    {if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPresupuesto=  
											"{$presupuesto.pk_num_presupuesto}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un presupuesto del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea 
													eliminar el presupuesto!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                            {/if}
											
                                      {/if}
										
										
										{if $presupuesto.ind_estado=='GE'}
                                            {if in_array('NM-01-01-03-04-C',$_Parametros.perfil)}
                                                <button class="cerrar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idControlProceso="{$i.pk_num_proceso_periodo}" title="Cerrar Proceso"
                                                        descipcion="El Usuario a Modificado el Perfil de Detalle" titulo="Modificar Perfil Detalle">
                                                    <i class="md md-timer-off" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {/if}
										
										{if $presupuesto.ind_estado=='AP'}
										  {if in_array('PR-01-03-01-01-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idPresupuesto="{$presupuesto.pk_num_presupuesto}" title="Ver Presupuesto"
                                                descipcion="El Usuario esta viendo un Presupuesto" titulo="<i class='icm icm-calculate2'></i> Ver Presupuesto">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                          {/if}	
										 {/if} 
																				
                                      {/if}
								   
                                  
									  
                                      &nbsp;&nbsp;
                                      
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7">
							
                            </th>
							
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modPR/presupuestoCONTROL/crearModificarMET';
       
	   $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPresupuesto:0 ,status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
				

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPresupuesto: $(this).attr('idPresupuesto'), status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
			
		
		$('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPresupuesto: $(this).attr('idPresupuesto'), status:'VER'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPresupuesto: $(this).attr('idPresupuesto'), status:'AP'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.generar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPresupuesto: $(this).attr('idPresupuesto'), status:'GE'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
      
	
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idPresupuesto=$(this).attr('idPresupuesto');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/presupuestoCONTROL/eliminarMET';
                $.post($url, { idPresupuesto: idPresupuesto },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idPresupuesto'+dato['idPresupuesto'])).html('');
                        swal("Eliminado!", "El presupuesto fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>