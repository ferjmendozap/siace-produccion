<form action="{$_Parametros.url}modPR/antepresupuestoCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
	     <input type="hidden" value="1" name="valido" />
	   
	   {if $status =='AP' || $status =='GE' || $status =='RE' || $status =='RV'}
            <input type="hidden" value="1" name="ver" id="ver" />
       {/if}
	   
	   {if $status =='VER'}
            <input type="hidden" value="2" name="ver" id="ver" />
       {/if}
	   
	    <input type="hidden" value="{$status}" name="status" id="status" />
        <input type="hidden" value="{$idAntepresupuesto}" name="idAntepresupuesto" id="idAntepresupuesto"/>
		
		 <div>
        
         <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab">Datos Generales</a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab">Detalle de Presupuesto</a></li>
               
             </ul>

  <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="Datos">Datos Generales
			   
			    <div class="row">
		
           	 <div class="col-sm-2">
                    <div class="form-group floating-label" id="cod_antepresupuestoError">
                        <input type="text" readonly class="form-control" value="{if isset($formDB.cod_antepresupuesto)}{$formDB.cod_antepresupuesto}{/if}" name="form[alphaNum][cod_antepresupuesto]" id="cod_antepresupuesto">
                        <label for="cod_antepresupuesto">Cod. Antepresupuesto</label>
                    </div>
                 </div>
				 
				 
				 <div class="col-sm-2">
                    <div class="form-group floating-label" id="fec_anioError">
                        <input type="text" class="form-control" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}" name="form[txt][fec_anio]" id="fec_anio">
                        <label for="fec_anio">A&ntilde;o</label>
                    </div>
                 </div>
				 
				 
			   <div class="col-lg-4">
			    <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo"  {if isset($veranular)}disabled{/if}  name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo</label>
                </div>
		     </div> 
				 
		</div>
			  
		 <div class="row">
		
		      
				 
				  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_antepresupuestoError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_antepresupuesto)}{$formDB.fec_antepresupuesto|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_antepresupuesto]" id="fec_antepresupuesto">
					          <label for="fec_antepresupuesto"> <i class="fa fa-calendar"></i> Fecha Antepresupuesto</label>
                            
                   </div>
                 </div>
       
                <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_inicioError">
                      
                              <input type="text" class="form-control fechas_inicio" value="{if isset($formDB.fec_inicio)}{$formDB.fec_inicio|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_inicio]" id="fec_inicio">
					          <label for="fec_inicio"><i class="fa fa-calendar"></i> Fecha Inicio</label>
                           
                  </div>
                </div>
       	    
			     <div class='col-sm-2'>
                   <div class="form-group floating-label" id="dec_finError">
                      
                              <input type="text" class="form-control fechas_fin" value="{if isset($formDB.dec_fin)}{$formDB.dec_fin|date_format:"%d-%m-%Y"}{/if}" name="form[txt][dec_fin]" id="dec_fin">
					          <label for="dec_fin"><i class="fa fa-calendar"></i> Fecha Fin</label>
                           
                   </div>
                </div>
		 </div>
						
		<div class="row">
		
		   <div class="col-sm-2">
                <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_gaceta)}{$formDB.ind_numero_gaceta}{/if}" name="form[alphaNum][ind_numero_gaceta]" id="ind_numero_gaceta">
                        <label for="ind_numero_gaceta">Numero Gaceta</label>
             </div>
          </div>	
				
				
			  <div class='col-sm-2'>
                   <div class="form-group floating-label">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_gaceta)}{$formDB.fec_gaceta|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_gaceta]">
					          <label for="fec_gaceta"><i class="fa fa-calendar"></i> Fecha Gaceta</label>
                           
                </div>
          </div>
		  
		  
		    <div class="col-sm-2">
                <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_decreto)}{$formDB.ind_numero_decreto}{/if}" name="form[alphaNum][ind_numero_decreto]" id="ind_numero_decreto">
                        <label for="ind_numero_decreto">Numero Decreto</label>
              </div>
           </div>	
			
			
			 <div class='col-sm-2'>
                <div class="form-group floating-label">
                     
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_decreto)}{$formDB.fec_decreto|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_decreto]" >
					          <label for="fec_decreto"><i class="fa fa-calendar"></i>Fecha Decreto</label>
                           
               </div>
             </div>				

         </div>
			
	    <div class="row">
			
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_tipo_presupuestoError">
                        <select id="ind_tipo_presupuesto" name="form[alphaNum][ind_tipo_presupuesto]" class="form-control">
                       		
							 {if isset($formDB.ind_tipo_presupuesto) and $formDB.ind_tipo_presupuesto =='G'}
                                <option value="G" selected>PROGRAMA</option>
                                {else}
                                <option value="G">PROGRAMA</option>
                            {/if}					
                            {if isset($formDB.ind_tipo_presupuesto) and $formDB.ind_tipo_presupuesto =='O'}
                                <option selected value="O">PROYECTO</option>
                                {else}
                                <option value="O">PROYECTO</option>
                            {/if}
                           
                            
                        </select>
                        <label for="ind_tipo_presupuesto">Tipo de Presupuesto</label>	 
						
                    </div>
                </div>
				
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_tipo_categoriaError">
                        <select id="ind_tipo_categoria" name="form[alphaNum][ind_tipo_categoria]" class="form-control">
                       		
							 {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='G'}
                                <option value="G" selected>PROGRAMA</option>
                                {else}
                                <option value="G">PROGRAMA</option>
								
                            {/if}					
                         <!--   {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='O'}
                                <option selected value="O">PROYECTO</option>
                                {else}
                                <option value="O">PROYECTO</option>
                            {/if}
							 {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='C'}
                                <option selected value="C">ACCION CENTRALIZADA</option>
                                {else}
                                <option value="C">ACCION CENTRALIZADA</option>
                            {/if}-->
                            
                        </select>
                      <label for="ind_tipo_categoria">Tipo Categoria Presupuesto</label>	 
						
                    </div>
                </div>
				
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="fk_prb016_indice_presupuestarioError">
                        <select id="fk_prb016_indice_presupuestario" name="form[alphaNum][fk_prb016_indice_presupuestario]"  class="form-control dirty">
						   <option value="">&nbsp;</option>
                            {foreach item=app from=$indicepresupuestario}
                                {if isset($formDB.fk_prb016_indice_presupuestario)}
                                    {if $app.pk_num_indice_presupuestario==$formDB.fk_prb016_indice_presupuestario}
                                        <option selected value="{$app.pk_num_indice_presupuestario}">{$app.ind_cod_indice_presupuestario}</option>
                                    {else}
                                        <option value="{$app.pk_num_indice_presupuestario}">{$app.ind_cod_indice_presupuestario}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_indice_presupuestario}">{$app.ind_cod_indice_presupuestario}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb008_num_sector">Seleccione Indice</label>
                    </div>
              </div>				
        </div>	
			
		 <div class="row">
		      <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb008_num_sectorError">
                        <select id="fk_prb008_num_sector" name="form[alphaNum][fk_prb008_num_sector]"  disabled="disabled"  class="form-control tipo dirty">
						   <option value="">&nbsp;</option>
                            {foreach item=app from=$sector}
                                {if isset($formDB.fk_prb008_num_sector)}
                                    {if $app.pk_num_sector==$formDB.fk_prb008_num_sector}
                                        <option selected value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb008_num_sector">Seleccione Sector</label>
                    </div>
              </div>
			  
			  <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb009_num_programaError">
                        <select id="fk_prb009_num_programa" name="form[alphaNum][fk_prb009_num_programa]" disabled="disabled"  class="form-control tipo dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$programa}
                                {if isset($formDB.fk_prb009_num_programa)}
                                    {if $app.pk_num_programa==$formDB.fk_prb009_num_programa}
                                        <option selected value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb009_num_programa">Seleccione Programa</label>
                    </div>
                </div> 
				
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb010_num_subprogramaError">
                        <select id="fk_prb010_num_subprograma" name="form[alphaNum][fk_prb010_num_subprograma]" disabled="disabled"  class="form-control tipo dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$subprograma}
                                {if isset($formDB.fk_prb010_num_subprograma)}
                                    {if $app.pk_num_subprograma==$formDB.fk_prb010_num_subprograma}
                                        <option selected value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb010_num_subprograma">Seleccione Sub-Programa</label>
                    </div>
                </div> 
				
           </div>
		
		   <div class="row">
		   
		   <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb005_num_proyectoError">
                        <select id="fk_prb005_num_proyecto" name="form[alphaNum][fk_prb005_num_proyecto]" disabled="disabled"  class="form-control tipo dirty">
						   <option value="">&nbsp;</option>
                            {foreach item=app from=$proyecto}
                                {if isset($formDB.fk_prb005_num_proyecto)}
                                    {if $app.pk_num_proyecto==$formDB.fk_prb005_num_proyecto}
                                        <option selected value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label><input type="text" style="border:0;" readonly value="Proyecto" name="proyecto_accion" id="proyecto_accion" /></label>
                    </div>
                </div>
		   
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb011_num_actividadError">
                        <select id="fk_prb011_num_actividad" name="form[alphaNum][fk_prb011_num_actividad]" disabled="disabled"   class="form-control tipo dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$actividad}
                                {if isset($formDB.fk_prb011_num_actividad)}
                                    {if $app.pk_num_actividad==$formDB.fk_prb011_num_actividad}
                                        <option selected value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label><input type="text" style="border:0;" readonly value="Actividad" name="actividad_aespecifica" id="actividad_aespecifica"></label>
                    </div>
                </div> 
				
			   
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb012_unidad_ejecutoraError">
                        <select id="fk_prb012_unidad_ejecutora" name="form[alphaNum][fk_prb012_unidad_ejecutora]" disabled="disabled"  class="form-control tipo dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$unidadejecutora}
                                {if isset($formDB.fk_prb012_unidad_ejecutora)}
                                    {if $app.pk_num_unidad_ejecutora==$formDB.fk_prb012_unidad_ejecutora}
                                        <option selected value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                        {else}
                                        <option value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb012_unidad_ejecutora">Seleccione Unidad Ejecutora</label>
                    </div>
                 </div>
				  
		     </div>
		
           <div class="row">
				  <div class="col-sm-5">
                    <div class="form-group"  id="num_monto_presupuestadoError">
                        <input type="text" readonly="readonly" class="form-control  monto" value="{if isset($formDB.num_monto_presupuestado)}{$formDB.num_monto_presupuestado|number_format:2:",":"."}{/if}" name= "form[int][num_monto_presupuestado]" id="num_monto_presupuestado">
                        <label for="num_monto_presupuestado">Monto Presupuestado</label>
                    </div>
                 </div>
		  
		     
			    <div class="col-sm-5">
                    <div class="form-group floating-label" >
                        <input type="text" readonly="readonly" class="form-control monto dirty" value="{if isset($formDB.num_monto_generado)}{$formDB.num_monto_generado|number_format:2:",":"."}{/if}" name=  
						"form[int][num_monto_generado]" id="num_monto_generado">
                        <label for="num_monto_generado">Monto Generado</label>
                    </div>
          </div>
		

                <div class="col-sm-2">
                    <div class="form-group floating-label" >
                        <input type="text" readonly class="form-control" value="{if isset($formDB.ind_estado)}{$formDB.ind_estado}{/if}" name="form[alphaNum][ind_estado]" id="ind_estado">
                        <label for="ind_estado">Estatus</label>
                    </div>
          </div>
        </div>
		

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
			   
	   	   
 </div>
             
		 
	<!-----------DETALLE PRESUPUESTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">Detalle de Presupuesto
			   
			   <div>
				 <div class="table-responsive">
                                      <table class="table no-margin" id="tablapartida">
                                                        <thead>
                                                        <tr>
                                                            <th>Codigo</th>
                                                            <th>Partida</th>
															<th>Monto</th>
															<!--<th width="5">Aespecifica</th>-->
														    {if $status!='AP' && $status!='VER'  && $status!='GE'}
                                                              <th>Acciones</th>
														    {/if}  
                                                        </tr>
                                                        </thead>
                                                        <tbody  id="codpartida">
                                                        <!--{if isset($partidas)}-->
														  {if isset($partidas)}
                                                            {foreach item=i from=$partidas}
															
																{if $formDB.ind_tipo_presupuesto =='P'}
							                                           <tr class="idPartida{$i.aespecifica|cat:'_'|cat:$i.cod|replace:".":"_"}">
                                                                    <input type="hidden" value="{$i.id}" name="form[int][fk_prb002_num_partida_presupuestaria][]" class="partidaInput" partida="{$i.cod}" id="{$i.aespecifica|cat:'_'|cat:$i.cod|replace:".":"_"}"/> 	
																 {else}	  
																      <tr class="idPartida{'0_'|cat:$i.cod|replace:".":"_"}">
                                                                    <input type="hidden" value="{$i.id}" name="form[int][fk_prb002_num_partida_presupuestaria][]" class="partidaInput" partida="{$i.cod}" id="{$i.aespecifica|cat:'_'|cat:$i.cod|replace:".":"_"}"/> 	
						                                         {/if}	
															
                                                                
															 {if $i.tipo=='E'}			
																	<td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.cod}</td>
                                                                    <td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.partida}</td>
																	<input type="hidden" class="form-control" value="{$i.aespecifica}" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}" >
																   
																	
															 {else if $i.tipo=='T'}	
																	<th>{$i.cod}</th>
                                                                    <th>{$i.partida}</th>
																    <th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}"  readonly>
																			  
                                                                           </div>
                                                                      </div>
																	</th>
																{else}			
                                                                    <td>{$i.cod}</td>
                                                                    <td>{$i.partida}</td>
																    <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}" >
                                                                           </div>
                                                                      </div>
																	  
																	  
																	</td>
																	
																	
					  <!-- ACCION ESPECIFICA-->
					  <td>			
					  
					   				  
						{if $formDB.ind_tipo_presupuesto =='P'}
							<label width="6" type="text" disabled class="form-control" id="{$i.cod}" >{"00"|cat:$i.aespecifica}</label>	
					        <input type="hidden" value="{$i.aespecifica}" name="form[alphaNum][Aespecifica][]"  id="Aespecifica"/>	
						{/if}	
							
							
				  																  
				    </td> <!--FIN ACCION ESPECIFICA-->
					
					<td>{if isset($ver) || $status=='AP' || $status=='VER'} {else} <button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" 
	borrar="idPartida{$i.aespecifica|cat:'_'|cat:$i.cod|replace:".":"_"}"><i class="md md-delete" style="color: #ffffff;"></i></button>{/if}</td>				
						
						<!-- <td>{if isset($ver) || $status=='AP' || $status=='VER'} {else} <button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida{$i.cod|replace:".":"_"}"><i class="md md-delete" style="color: #ffffff;"></i></button>{/if}</td>-->
						
													  
																	
																{/if} 	
                                                                   
                                                                </tr>
															  {/foreach}	
                                                           
														 {/if}	
                                                       <!--  {/if} -->
                                                        </tbody>
														
														 <!-- Total  -->
														<tr>
														   <td align="right"  style="font-weight:bold" colspan="2">
														      TOTAL=
														   </td>
														    <th id="total" align="right"  style="text-align:right;  font-weight:bold">
														     <input style="text-align:right;" type="text" class="form-control" value="{$suma_total|number_format:2:",":"."}" disabled name="monto_total" id="monto_total" />                            
															</th> 
														</tr>
														
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="center">
                                                                {if isset($ver) || $status=='AP'  || $status=='VER'} {else}
																  
																		<button type="button"
																				class="btn btn-primary ink-reaction btn-raised"
																				id="agregarPartida"
																				data-toggle="modal" data-target="#formModal2"
																				data-keyboard="false" data-backdrop="static"
																				titulo="Agregar Partidas"
																				url="{$_Parametros.url}modPR/maestros/partidaCONTROL/PartidaMET/MODAL"
																				><span class="glyphicon glyphicon-plus"></span> Agregar Partida
																		</button>
																	
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
															
                                                   													 
				  
			      <!--    <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="agregar">Agregar</button>-->
			      </div>
					 
                <!--     <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>-->
                  </div>
	 
                </div>		   
				  
				  
			   </div>
			   
			   
			   
					 <div class="modal-footer">
								  <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" 	
								  data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
								  
							  {if ($status!='VER')}
								 <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
									
									{if $status=='AP'}
										 <span class="glyphicon glyphicon-ok"></span> Aprobar
									 {elseif $status=='RV'}
										 <span class="glyphicon glyphicon-ok-circle"></span> Revisar   	
									{elseif $status=='GE'}
										 <span class="glyphicon glyphicon-refresh"></span> Generar
									{elseif $status=='AN'}
										<span class="glyphicon glyphicon-remove"></span> Anular 
									{elseif $status=='RE'}
										 <span class="glyphicon glyphicon-arrow-left"></span> Reversar 	           	
									{elseif isset($idAntepresupuesto)}
										<span class="glyphicon glyphicon-floppy-save"></span> Modificar
									{else}
										<span class="glyphicon glyphicon-floppy-disk"></span> Guardar
									{/if}
								</button>
							 {/if}	
					   </div>	
			   
           </div>
		   
       </div>
		
    </div>
   
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		
		$('form').keypress(function(e){   
          if(e == 13){
          return false;
         }
        });


        $('input').keypress(function(e){
         if(e.which == 13){
          return false;
         }
        });
		
	     var pr = new  PrFunciones();
		
		// var nuevo=$(document.getElementById("idAntepresupuesto")).val();
		//  if (nuevo==0){	
		      var estado= $(document.getElementById('status')).val();
			
			   
		  if (estado=='PR')
		  {
		       var tipo_presupuesto = '{if isset($formDB.ind_tipo_presupuesto)}{$formDB.ind_tipo_presupuesto}{/if}';
		       var tipo_categoria = '{if isset($formDB.ind_tipo_categoria)}{$formDB.ind_tipo_categoria}{/if}';
		       var indice_presupuestario = '{if isset($formDB.fk_prb016_indice_presupuestario)}{$formDB.fk_prb016_indice_presupuestario}{/if}';
			   
			   //var presupuesto = '{if isset($formDB.ind_tipo_presupuesto)}{$formDB.ind_tipo_presupuesto}{/if}';
		       pr.metJsonCategoria('{$_Parametros.url}modPR/indicepresupuestarioCONTROL/JsonCategoriaMET',tipo_presupuesto);
			   
			   pr.metJsonIndicePresupuestario('{$_Parametros.url}modPR/antepresupuestoCONTROL/JsonIndicePresupuestarioMET', tipo_presupuesto, tipo_categoria, 	
			   indice_presupuestario);
		 
			   var indice = '{if isset($formDB.fk_prb016_indice_presupuestario)}{$formDB.fk_prb016_indice_presupuestario}{/if}';
		       pr.metJsonIndiceValores('{$_Parametros.url}modPR/antepresupuestoCONTROL/JsonIndiceValoresMET', indice);
				
	     } 
		       
			   
			  
		/***************************DESHABILITAR CONTROLES*****************************/
		   var disabled=$(document.getElementById("ver")).val();
		   if (disabled==1){
		       $("#formAjax input,textarea").prop("readonly", true);
			   $("#formAjax select").prop("disabled", true);
			   $("#formAjax input" ).removeClass('fechas2');
			   $("#formAjax input" ).removeClass('fecha_anio');
			   
			   //$("#formAjax" ).removeClass('hasDatepicker')
  
		   }	   
		   else if (disabled==2){
		       $("#formAjax input,textarea,select").prop("disabled", true);
		   }
	   /********************************************************************************/	 
		
		
		$('#agregarPartida').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		
		
		 $('#ind_tipo_presupuesto').change(function () { 
	      var tipo_presupuesto=$(this).val();
		
	        if (tipo_presupuesto=='G')
			  {
			   $(document.getElementById('actividad_aespecifica')).val("Actividad")
			   $(document.getElementById('proyecto_accion')).val("Proyecto")
			  }
			 else 
			  {
			     $(document.getElementById('actividad_aespecifica')).val("Accion Especifica")
			  }
	     });
	   
	   
	     $('#ind_tipo_categoria').change(function () { 
		  var tipo_categoria=$(this).val();
		  
	          if (tipo_categoria=='C')
			  {
			      $(document.getElementById('proyecto_accion')).val("Accion Centralizada")
			  }
			  else 
			  {
			     $(document.getElementById('proyecto_accion')).val("Proyecto")
			  }
	   });
		
		
		
		$('#codpartida').on('click', '.borrar', function () {
            var borrar = $(this);
			var contador=0;
			var contador2=0;
			var contador3=0;
			var valor2="";
			var programa = 0;
			var valor=borrar.attr('borrar');
		     
			partida_monto=valor.substring(11, 23).replace(/[.]/gi, "\_");
			partida_monto=partida_monto.replace(/[_]/gi, "\.");
			
			
			if (valor.length<23)
			{
			  programa=1;
              valor=valor.substring(9, 21);			  
			  valor='idPartida0_'+valor;
			}
			
			
			valor2=valor.substring(9, 14);
			valor2= valor2.concat("_00_00_00");
			
			valor= valor.substring(9, 17);
			valor= valor.concat("_00_00");
			
			
				$("#codpartida tr").each(function (){
					
							idpartida1 = $(this).attr('class');
							
							if (idpartida1.length<23)
			                {
								  idpartida1=idpartida1.substring(9, 21);
								  idpartida1='idPartida0_'+idpartida1;
								  
		                 	}
							
							codigo= idpartida1.substring(9, 17);
							codigo= codigo.concat("_00_00");
							
							codigo2= idpartida1.substring(9, 14);
							codigo2= codigo2.concat("_00_00_00");

						if (valor==codigo)	
						{
							contador=contador+1;
						}	
						
						if (valor2==codigo2)	
						{
							contador2=contador2+1;
						}	
					//});	
					
				});
				if (contador==2)
				{
				  if (programa==1)
				      valor='idPartida'+valor.substring(2, 14);
				  else	  
				      valor='idPartida'+valor;
					  
				  $(document.getElementsByClassName(valor)).remove();
				}
				
				if (contador2==3)
				{    
				
				 if (programa==1)
				      valor2='idPartida'+valor2.substring(2, 14);
				  else	
				      valor2='idPartida'+valor2;
				  
				  $(document.getElementsByClassName(valor2)).remove();
				}
				
				$(document.getElementsByClassName(borrar.attr('borrar'))).remove();
				
				 contador3=0;
		         $("#codpartida th").each(function (){
				 $(this).find('input').each (function() {
					  			  
					  contador3=contador3+1;    
						 
	               }) //imput
			     })
				
				        
				    $('#codpartida').change();
				
				 // }
				 // $('#codpartida').change(function ());
        });
		
		
		$('#dec_fin').change(function () {
         var $from=$("#fec_inicio").datepicker('getDate');
         var $to =$("#dec_fin").datepicker('getDate');
            if($from>$to)
		    {
            // alert("La fecha de fin no puede ser menor que la fecha de inicio");
			  $("#dec_fin").val('');
			  swal("Error!", "La fecha de fin no puede ser menor que la fecha de inicio", "error");
		 	}
         });

		
		$('#fec_inicio').change(function () {
         var $from=$("#fec_inicio").datepicker('getDate');
         var $to =$("#dec_fin").datepicker('getDate');
            if($from>$to)
		    {
             //alert("La fecha de fin no puede ser menor que la fecha de inicio");
			  $("#fec_inicio").val('');
			  swal("Error!", "La fecha de fin no puede ser menor que la fecha de inicio","error");
		 	}
         });
		
		
		
		$('#codpartida').change(function () {
		
		
		var suma=0;
		var total=0;
		var codigo=0; 
		var codigo1=0; 
		var partida1='';
		var monto=0;
		var cont=0;
		var aesp=0;
		
		
		  $("#codpartida th").each(function (){
		   
				 $(this).find('input').each (function() {
				 
				    // var aesp_nueva = $(document.getElementById('Aespecifica')).val(); 
				     idpartida1 = $(this).attr('id');
					 aespecifica = $(this).attr('aespecifica');
					 suma=actualizarMonto(idpartida1,cont,aespecifica);
					 
					 cont++;
					 
					 /* if (aespecifica==undefined)
					     aespecifica=0;
					  else */
					    $(this).val(suma).formatCurrency();
					 
					 
	             }) //imput
			 })	 
			 
			 
			  $("#codpartida th").each(function (){
		   
				 $(this).find('input').each (function() {
				 
				 idpartida1 = $(this).attr('id');
				 
				 aespecifica = $(this).attr('aespecifica');
				 
			     codigo1= idpartida1.substring(4, 12);
				 	 					 
				  if (codigo1=="00.00.00"){
						  monto= setNumero($(this).val());
						  total= total+Number(monto);
				  }
					cont++;	 	 
				 
	             }) //imput
			 })	
			 
			  	  		 
			 $("#total").each(function (){
				  $(this).find('input').each (function() {
				  
				   $(this).val(total).formatCurrency();
				   $(document.getElementById('num_monto_presupuestado')).val(total).formatCurrency();
						 
	             }) //imput
			 })	
         });
		
		
		 $('#fec_inicio').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language:'es' }); 
		 $('#dec_fin').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language:'es' }); 
		 $('.fechas2').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language:'es' });
		 $('#fec_anio').datepicker({ format: 'yyyy', viewMode: "years", minViewMode: "years", startDate: '-1y', endDate: '+1y' ,language:'es' }); 
		 
		 
		 //$(".monto").inputmask( "999.999.999,99",{ numericInput: true});
            
		
        $('#modalAncho').css("width","85%");
		
        $('#accion').click(function(){
		
		/*    swal({
                title: "�Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });*/
			
		   $( ".tipo" ).prop( "disabled", false ); 
		
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
					 }else if(dato['status']=='aprobado'){
                   
						//if(dato['ind_estado']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
						$(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('');
						swal("Registro Aprobado!", "El Antepresupuesto fue aprobado satisfactoriamente.", "success");
						$(document.getElementById('cerrarModal')).click();
						$(document.getElementById('ContenidoModal')).html('');
					
					
					 }else if(dato['status']=='revisado'){
                   
					     $(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('');
                         swal("Registro Revisado!", "El Antepresupuesto fue revisado satisfactoriamente.", "success");
                         $(document.getElementById('cerrarModal')).click();
                         $(document.getElementById('ContenidoModal')).html('');
					
					//----------------------------------------------------
				 }else if(dato['status']=='anulado'){
                   
					$(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('');
                    swal("Registro Anulado!", "El Antepresupuesto fue aprobado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
			     }else if(dato['status']=='reversado'){
                   
					$(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('');
                    swal("Registro Reversado!", "El Antepresupuesto fue reversado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
			     }else if(dato['status']=='generado'){
                   
					$(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('');		
                    swal("Registro Generado!", "El Antepresupuesto fue generado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
                 }else if(dato['status']=='modificar'){
                   
                   if(dato['ind_estado']=='PR' ){ var estado='Preparaci&oacuten' }else if (dato['ind_estado']=='RV'){ var estado='Revisado' } else if (dato['ind_estado']=='AP'){ var estado='Aprobado' } else { var estado='Preparaci&oacuten' }
				   
                    $(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('<td>'+dato['cod_antepresupuesto']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_inicio']+'</td>' +
							'<td>'+dato['dec_fin']+'</td>' +
                            '<td>'+dato['num_monto_presupuestado']+'</td>' +
							'<td>'+estado+'</td>' +
                            '<td>' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idAntepresupuesto="'+dato['idAntepresupuesto']+'"' +
                            'descipcion="El Usuario a Modificado un Atepresupuesto" titulo="Modificar Antepresupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAntepresupuesto="'+dato['idAntepresupuesto']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a creado un Antepresupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Antepresupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Antepresupuesto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
                }else if(dato['status']=='nuevo'){
                   
                    if(dato['ind_estado']=='PR' ){ var estado='Preparaci&oacuten' }else if (dato['ind_estado']=='RV'){ var estado='Revisado' } else if (dato['ind_estado']=='AP'){ var estado='Aprobado' } else { var estado='Preparaci&oacuten' }
                    $(document.getElementById('datatable1')).append('<tr  id="idAntepresupuesto'+dato['idAntepresupuesto']+'">' +
                            '<td>'+dato['cod_antepresupuesto']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_inicio']+'</td>' +
							'<td>'+dato['dec_fin']+'</td>' +
                            '<td>'+dato['num_monto_presupuestado']+'</td>' +
							'<td>'+estado+'</td>' +
                            '<td>' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idAntepresupuesto="'+dato['idAntepresupuesto']+'"' +
                            'descipcion="El Usuario a Modificado un Atepresupuesto" titulo="Modificar Antepresupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAntepresupuesto="'+dato['idAntepresupuesto']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a creado un Antepresupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Antepresupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Creado!", "El Antepresupuesto fue creado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });



function actualizarMonto(CodPartida,cont,aespecifica1) {

		var suma=0; 
		var codigo=""; 
		var codigo1="";
		var codigo2=""; 
		var valor='';
		var contador=0;
		

   $("#codpartida td").each(function (){
   
 
   
       var presupuesto = $(document.getElementById('ind_tipo_presupuesto')).val();   
	   
		  
				 $(this).find('input').each (function() {
				 
				     idpartida1 = $(this).attr('id');
					 aespecifica = $(this).attr('aespecifica');
					 
					 
					  valor= $(this).val();  
					  $(this).val(valor).formatCurrency();
				
					 if (cont<=0)
					 {  
              		   valor= $(this).val();  
					   $(this).val(valor).formatCurrency();
					   cont++; 
					 }  
					
					
				  if(presupuesto=="P") 
				  {
				     //var aesp_nueva = $(document.getElementById('Aespecifica')).val();
				
					   
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 				 
					  if (CodPartida==codigo && aespecifica1==aespecifica){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
						 }
					 
					 
					 codigo1= idpartida1.substring(0, 3);
					 codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
						 }
						 
						 
					 codigo2= idpartida1.substring(0, 9);
					 codigo2= codigo2.concat(".00");
					 
					  if (CodPartida==codigo2 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					 }
					  
					  
					
				  }
				  else
				  {
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
				      if (CodPartida==codigo){
					    monto= setNumero($(this).val());
					    suma=suma+Number(monto);
					 }
					 
					 
					  codigo1= idpartida1.substring(0, 3);
					  codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
						 
						 
					   codigo2= idpartida1.substring(0, 9);
					   codigo2= codigo2.concat(".00");
					 
					   if (CodPartida==codigo2 ){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
				  }
				  
				  
										 	 
	             }) //imput
			 })	
			 
	    return suma; 
}




//	funcion para convertir un numero frmateado en su valor real
function setNumero(num_formateado) {
	var num = num_formateado.toString();
	num = num.replace(/[.]/gi, "");
	num = num.replace(/[,]/gi, ".");
	
	var numero = new Number(num);
	return numero;
}



</script>

