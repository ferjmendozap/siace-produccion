<form action="{$_Parametros.url}modPR/antepresupuestoCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
	     <input type="hidden" value="1" name="valido" />
	   
	   {if $status =='AP' || $status =='GE'}
            <input type="hidden" value="1" name="ver" id="ver"/>
       {/if}
	   
	    <input type="hidden" value="{$status}" name="status" />
        <input type="hidden" value="{$idAntepresupuesto}" name="idAntepresupuesto"/>
		
		
		 <div>
         <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab">Datos Generales</a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab">Detalle de Presupuesto</a></li>
             </ul>

  <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="Datos">Datos Generales
			   
			    <div class="row">
		
           	 <div class="col-sm-2">
                    <div class="form-group floating-label" id="cod_antepresupuestoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.cod_antepresupuesto)}{$formDB.cod_antepresupuesto}{/if}" name="form[alphaNum][cod_antepresupuesto]" id="cod_antepresupuesto">
                        <label for="cod_antepresupuesto">Cod. Antepresupuesto</label>
                    </div>
                 </div>
		</div>
			  
		 <div class="row">
		
		       <div class="col-sm-2">
                    <div class="form-group floating-label" id="fec_anioError">
                        <input type="text" class="form-control" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{/if}" name="form[txt][fec_anio]" id="fec_anio">
                        <label for="fec_anio">Anio</label>
                    </div>
                 </div>
				 
				  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_antepresupuestoError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_antepresupuesto)}{$formDB.fec_antepresupuesto|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_antepresupuesto]" id="fec_antepresupuesto">
					          <label for="fec_antepresupuesto"> <i class="fa fa-calendar"></i> Fecha Antepresupuesto</label>
                            
                    </div>
                </div>
       
                <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_inicioError">
                      
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_inicio)}{$formDB.fec_inicio|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_inicio]" id="fec_inicio">
					          <label for="fec_inicio"><i class="fa fa-calendar"></i> Fecha Inicio</label>
                           
                    </div>
                </div>
       	    
			     <div class='col-sm-2'>
                   <div class="form-group floating-label" id="dec_finError">
                      
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.dec_fin)}{$formDB.dec_fin|date_format:"%d-%m-%Y"}{/if}" name="form[txt][dec_fin]" id="dec_fin">
					          <label for="dec_fin"><i class="fa fa-calendar"></i> Fecha Fin</label>
                           
                    </div>
                </div>
				
				
		 </div>
						
		<div class="row">
		
		   <div class="col-sm-2">
                <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_gaceta)}{$formDB.ind_numero_gaceta}{/if}" name="form[alphaNum][ind_numero_gaceta]" id="ind_numero_gaceta">
                        <label for="ind_numero_gaceta">Numero Gaceta</label>
                 </div>
            </div>	
				
				
			  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_gacetaError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_gaceta)}{$formDB.fec_gaceta|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_gaceta]">
					          <label for="fec_gaceta"><i class="fa fa-calendar"></i> Fecha Gaceta</label>
                           
                    </div>
                </div>		

        
		
		   <div class="col-sm-2">
                <div class="form-group floating-label">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_decreto)}{$formDB.ind_numero_decreto}{/if}" name="form[alphaNum][ind_numero_decreto]" id="ind_numero_decreto">
                        <label for="ind_numero_decreto">Numero Decreto</label>
                 </div>
            </div>	
			
			
			  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_decretoError">
                     
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_decreto)}{$formDB.fec_decreto|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_decreto]" >
					          <label for="fec_decreto"><i class="fa fa-calendar"></i> Fecha Decreto</label>
                           
                    </div>
                </div>		 
        </div>		
			
        
		
		
		
		  <div class="row">
			
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_tipo_presupuestoError">
                        <select id="ind_tipo_presupuesto" name="form[alphaNum][ind_tipo_presupuesto]" class="form-control">
                       		
							 {if isset($formDB.ind_tipo_presupuesto) and $formDB.ind_tipo_presupuesto =='G'}
                                <option value="G" selected>PROGRAMA</option>
                                {else}
                                <option value="G">PROGRAMA</option>
                            {/if}					
                            {if isset($formDB.ind_tipo_presupuesto) and $formDB.ind_tipo_presupuesto =='O'}
                                <option selected value="O">PROYECTO</option>
                                {else}
                                <option value="O">PROYECTO</option>
                            {/if}
                           
                            
                        </select>
                        <label for="ind_tipo_presupuesto">Tipo de Presupuesto</label>	 
						
                    </div>
                </div>
				
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_tipo_categoriaError">
                        <select id="ind_tipo_categoria" name="form[alphaNum][ind_tipo_categoria]" class="form-control">
                       		
							 {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='G'}
                                <option value="G" selected>PROGRAMA</option>
                                {else}
                                <option value="G">PROGRAMA</option>
								
                            {/if}					
                         <!--   {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='O'}
                                <option selected value="O">PROYECTO</option>
                                {else}
                                <option value="O">PROYECTO</option>
                            {/if}
							 {if isset($formDB.ind_tipo_categoria) and $formDB.ind_tipo_categoria =='C'}
                                <option selected value="C">ACCION CENTRALIZADA</option>
                                {else}
                                <option value="C">ACCION CENTRALIZADA</option>
                            {/if}-->
                            
                        </select>
                      <label for="ind_tipo_categoria">Tipo Categoria Presupuesto</label>	 
						
                    </div>
                </div>
				
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="fk_prb016_indice_presupuestarioError">
                        <select id="fk_prb016_indice_presupuestario" name="form[alphaNum][fk_prb016_indice_presupuestario]"  class="form-control dirty">
						   <option value="">&nbsp;</option>
                            {foreach item=app from=$indicepresupuestario}
                                {if isset($formDB.fk_prb016_indice_presupuestario)}
                                    {if $app.pk_num_indice_presupuestario==$formDB.fk_prb016_indice_presupuestario}
                                        <option selected value="{$app.pk_num_indice_presupuestario}">{$app.ind_cod_indice_presupuestario}</option>
                                    {else}
                                        <option value="{$app.pk_num_indice_presupuestario}">{$app.ind_cod_indice_presupuestario}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_indice_presupuestario}">{$app.ind_cod_indice_presupuestario}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb008_num_sector">Seleccione Indice</label>
                    </div>
              </div>				
        </div>	




         
		  <div class="row">
		      <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb008_num_sectorError">
                        <select id="fk_prb008_num_sector" name="form[alphaNum][fk_prb008_num_sector]"  disabled="disabled"  class="form-control tipo dirty">
						   <option value="">&nbsp;</option>
                            {foreach item=app from=$sector}
                                {if isset($formDB.fk_prb008_num_sector)}
                                    {if $app.pk_num_sector==$formDB.fk_prb008_num_sector}
                                        <option selected value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_sector}">{$app.ind_cod_sector|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb008_num_sector">Seleccione Sector</label>
                    </div>
              </div>
			  
			  <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb009_num_programaError">
                        <select id="fk_prb009_num_programa" name="form[alphaNum][fk_prb009_num_programa]" disabled="disabled"  class="form-control tipo dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$programa}
                                {if isset($formDB.fk_prb009_num_programa)}
                                    {if $app.pk_num_programa==$formDB.fk_prb009_num_programa}
                                        <option selected value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_programa}">{$app.ind_cod_programa|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb009_num_programa">Seleccione Programa</label>
                    </div>
                </div> 
				
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb010_num_subprogramaError">
                        <select id="fk_prb010_num_subprograma" name="form[alphaNum][fk_prb010_num_subprograma]" disabled="disabled"  class="form-control tipo dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$subprograma}
                                {if isset($formDB.fk_prb010_num_subprograma)}
                                    {if $app.pk_num_subprograma==$formDB.fk_prb010_num_subprograma}
                                        <option selected value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_subprograma}">{$app.ind_cod_subprograma|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb010_num_subprograma">Seleccione Sub-Programa</label>
                    </div>
                </div> 
				
           </div>
		
		   <div class="row">
		   
		   <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb005_num_proyectoError">
                        <select id="fk_prb005_num_proyecto" name="form[alphaNum][fk_prb005_num_proyecto]" disabled="disabled"  class="form-control tipo dirty">
						   <option value="">&nbsp;</option>
                            {foreach item=app from=$proyecto}
                                {if isset($formDB.fk_prb005_num_proyecto)}
                                    {if $app.pk_num_proyecto==$formDB.fk_prb005_num_proyecto}
                                        <option selected value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_proyecto}">{$app.ind_cod_proyecto|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label><input type="text" style="border:0;" readonly value="Proyecto" name="proyecto_accion" id="proyecto_accion" /></label>
                    </div>
                </div>
		   
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb011_num_actividadError">
                        <select id="fk_prb011_num_actividad" name="form[alphaNum][fk_prb011_num_actividad]" disabled="disabled"   class="form-control tipo dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$actividad}
                                {if isset($formDB.fk_prb011_num_actividad)}
                                    {if $app.pk_num_actividad==$formDB.fk_prb011_num_actividad}
                                        <option selected value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_actividad}">{$app.ind_cod_actividad|cat:'-'|cat:$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label><input type="text" style="border:0;" readonly value="Actividad" name="actividad_aespecifica" id="actividad_aespecifica"></label>
                    </div>
                </div> 
				
			   
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb012_unidad_ejecutoraError">
                        <select id="fk_prb012_unidad_ejecutora" name="form[alphaNum][fk_prb012_unidad_ejecutora]" disabled="disabled"  class="form-control tipo dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$unidadejecutora}
                                {if isset($formDB.fk_prb012_unidad_ejecutora)}
                                    {if $app.pk_num_unidad_ejecutora==$formDB.fk_prb012_unidad_ejecutora}
                                        <option selected value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                        {else}
                                        <option value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_unidad_ejecutora}">{$app.cod_unidad_ejecutora|cat:'-'|cat:$app.ind_nombre_unidad_ejecutora}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb012_unidad_ejecutora">Seleccione Unidad Ejecutora</label>
                    </div>
                 </div>
				  
		     </div>









        <div class="row">
				  <div class="col-sm-5">
                    <div class="form-group floating-label"  id="num_monto_presupuestadoError">
                        <input type="text" class="form-control  monto" value="{if isset($formDB.num_monto_presupuestado)}{$formDB.num_monto_presupuestado|number_format:2:",":"."}{/if}" name=  
						"form[int][num_monto_presupuestado]" id="num_monto_presupuestado">
                        <label for="num_monto_presupuestado">Monto Presupuestado</label>
                    </div>
                 </div>
		  
		     
			    <div class="col-sm-5">
                    <div class="form-group floating-label" >
                        <input type="text" class="form-control monto" value="{if isset($formDB.num_monto_generado)}{$formDB.num_monto_presupuestado|number_format:2:",":"."}{/if}" name=  
						"form[int][num_monto_generado]" id="num_monto_generado">
                        <label for="num_monto_generado">Monto Generado</label>
                    </div>
                 </div>
		

                <div class="col-sm-2">
                    <div class="form-group floating-label" >
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_estado)}{$formDB.ind_estado}{/if}" name="form[alphaNum][ind_estado]" id="ind_estado">
                        <label for="ind_estado">Estatus</label>
                    </div>
                 </div>
        </div>
		

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
			   
	 
				   
 </div>
             
		 
	<!-----------DETALLE PRESUPUESTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">Detalle de Presupuesto
			   
			      <div>
				  
				 <div class="table-responsive">
                                      <table class="table no-margin" id="tablapartida">
                                                        <thead>
                                                        <tr>
														    <th>A.especifica </th>
                                                            <th>Codigo</th>
                                                            <th>Partida</th>
															<th>Monto Aprobado</th>
															<th>Monto Generar</th>
                                                            <!--<th>Acciones</th>-->
                                                        </tr>
                                                        </thead>
                                                        <tbody  id="codpartida">
                                                        <!--{if isset($partidas)}-->
														  {if isset($partidas)}
                                                            {foreach item=i from=$partidas}
															   
                                                                <tr class="idPartida{$i.id}">
                                                                    <input type="hidden" value="{$i.id}" name="form[int][fk_prb002_num_partida_presupuestaria][]" class="partidaInput" partida="{$i.cod}" id="{$i.id}"/> 							
															 
															 {if $i.tipo=='E'}			
																	<td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.cod}</td>
                                                                    <td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.partida}</td>
																   
																	
															 {else if $i.tipo=='T'}			
																	<td style="font-family:Arial, Geneva, sans-serif; font-weight:bold">{$i.cod}</td>
                                                                    <td style="font-family:Arial, Geneva, sans-serif; font-weight:bold">{$i.partida}</td>
																    <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control"  value="{$i.monto|number_format:2:",":"."}" name="form[int][monto_inicial][{$i.id}]" id="partidas" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	<th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control"  value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][{$i.id}]" id="{$i.cod}" style="text-align:right; font-family:Arial, Geneva, sans-serif; font-weight:bold">
                                                                           </div>
                                                                      </div>
																	</th>
																{else}	
																    <!--<td>
																	 {foreach item=app from=$aespecifica}
																	       {if $app.pk_num_aespecifica==$i.aespecifica}
																	           {$app.ind_cod_aespecifica}
																		    {/if}	   
																	   {/foreach}
																	    {$i.aespecifica}  
																	</td>-->	
																		
                                                                    <td>{$i.cod}</td>
                                                                    <td>{$i.partida}</td>
																    <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control" value="{$i.monto|number_format:2:",":"."}" name="form[int][monto_inicial][{$i.id}]" id="partidas" style="text-align:right;">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	
																	 <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input type="text" class="form-control generar" value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][{$i.id}]" id="{$i.cod}" style="text-align:right;" >
                                                                           </div>
                                                                      </div>
																	</td> 
																{/if} 	
																
															       
																	
                                                                   <!-- <td>{if isset($ver)} {else} <button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button>{/if}</td>-->
                                                                </tr>
															  {/foreach}	
                                                           
														 {/if}	
                                                       <!--  {/if} -->
                                                        </tbody>
														
														 <!-- Total  -->
														<tr>
														   <td align="right"  style="font-weight:bold" colspan="2">
														      TOTAL=
														   </td>
														    <td id="total_aprobado" style="text-align:right; font-family:Verdana, Geneva, sans-serif; font-weight:bold">
														     <input type="text" class="form-control" value="{$suma_total|number_format:2:",":"."}" disabled name="monto_aprobado" id="monto_aprobado" style="text-align:right;"  />                            
															</td> 
															<th id="total" style="text-align:right; font-family:Verdana, Geneva, sans-serif; font-weight:bold">
														     <input type="text" class="form-control" value="{$suma_total|number_format:2:",":"."}" disabled name="monto_total" id="monto_total" style="text-align:right;" />
															  
															</th> 
														</tr>
														
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="center">
                                                                {if isset($ver)} {else}
                                                                    <button type="button"
                                                                            class="btn btn-primary ink-reaction btn-raised"
                                                                            id="agregarPartida"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Partidas"
                                                                            url="{$_Parametros.url}modPR/maestros/partidaCONTROL/PartidaGenerarMET/MODAL"
                                                                            >Agregar Partida
                                                                    </button>
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
															
                                                   													 
				  
			      <!--    <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="agregar">Agregar</button>-->
			      </div>
				  
			   </div>
			   
			   
		      <div class="modal-footer">
			  
			  <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" 	
			    data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
			  
			   <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
					{if $status=='AP'}
						Aprobar
					{elseif $status=='GE'}
						<span class="glyphicon glyphicon-refresh"></span> Generar
					{elseif isset($idAntepresupuesto)}
						Modificar
					{else}
						Guardar
					{/if}
              </button>
			  
             </div>
			   
           </div>
		   
       </div>
		
    </div>
   
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		
		/***************************DESHABILITAR CONTROLES*****************************/
		   var disabled=$(document.getElementById("ver")).val();
		   if (disabled==1){
		       $("#formAjax textarea").prop("readonly", true);
			   $("#formAjax .form-control").prop("readonly", true)
			   $("#formAjax .generar").prop("readonly", false)
			   $("#formAjax select").prop("disabled", true);
			   $("#formAjax input" ).removeClass('fechas2');
			   $("#formAjax input" ).removeClass('fecha_anio');
			   
			   //$("#formAjax" ).removeClass('hasDatepicker')
  
		   }	   
		   else if (disabled==2){
		       $("#formAjax input,textarea,select").prop("disabled", true);
		   }
	   /********************************************************************************/	 
		
		
		
		$('#agregarPartida').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		
		
		 $('#codpartida').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		
	   $('#codpartida').change(function () {
		
		
		var suma=0;
		var suma1='';
		var total=0;
		//var monto_total='';
		var codigo=0; 
		var codigo1=0; 
		var partida1='';
		var monto=0;
		var monto1=0;
		String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g,'') }
		
		  $("#codpartida th").each(function (){
				 $(this).find('input').each (function() {
				     idpartida1 = $(this).attr('id');	
					// aespecifica = $(this).attr('aespecifica');
					 
					 suma=actualizarMonto(idpartida1);
					 $(this).val(suma).formatCurrency();
						 		 
					 
	             }) //imput
			 })	 
			 
			 
			  $("#codpartida th").each(function (){
		   
				 $(this).find('input').each (function() {
				 idpartida1 = $(this).attr('id');
					  
					  codigo1= idpartida1.substring(4, 12);
					  if (codigo1=="00.00.00"){
					     monto= setNumero($(this).val());
						 total=total+Number(monto);
				  }
						 	 
				 
	             }) //imput
			 })	
			  
			  
	  		 
			 $("#total").each(function (){
		   
				  $(this).find('input').each (function() {
				  $(this).val(total).formatCurrency();
				  $(document.getElementById('num_monto_generado')).val(total).formatCurrency();
					
	             }) //imput
			 })	
         });
		

		
        $('#modalAncho').css("width","85%");
		
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
					
			   }else if(dato['status']=='generado'){
                   
					$(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('');		
                    swal("Registro Generado!", "El Antepresupuesto fue generado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					//------------------------------------------------------
					
                }else if(dato['status']=='modificar'){
                    $(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('<td>'+dato['cod_antepresupuesto']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_inicio']+'</td>' +
							'<td>'+dato['dec_fin']+'</td>' +
                            '<td>'+dato['num_monto_presupuestado']+'</td>' +
							'<td>'+dato['ind_estado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idAntepresupuesto="'+dato['idAntepresupuesto']+'"' +
                            'descipcion="El Usuario a Modificado un Antepresupuesto" titulo="Modificar Antepresupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAntepresupuesto="'+dato['idAntepresupuesto']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Antepresupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Antepresupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Antepresupuesto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
                }else if(dato['status']=='nuevo'){
                    $(document.getElementById('datatable1')).append('<tr  id="idAntepresupuesto'+dato['idAntepresupuesto']+'">' +
                            '<td>'+dato['cod_antepresupuesto']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_inicio']+'</td>' +
							'<td>'+dato['dec_fin']+'</td>' +
                            '<td>'+dato['num_monto_presupuestado']+'</td>' +
							'<td>'+dato['ind_estado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idAntepresupuesto="'+dato['idAntepresupuesto']+'"' +
                            'descipcion="El Usuario a Modificado un Atepresupuesto" titulo="Modificar Antepresupuesto">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAntepresupuesto="'+dato['idAntepresupuesto']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Antepresupuesto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Antepresupuesto!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Antepresupuesto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });

	
function actualizarMonto(CodPartida) {
var suma=0;
		var suma=0; 
		var codigo=""; 
		var codigo1=""; 
		

   $("#codpartida td").each(function (){
		   
				 $(this).find('input').each (function() {
				     idpartida1 = $(this).attr('id');
					 
					  valor= $(this).val();  
					  $(this).val(valor).formatCurrency();
					  
					 codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 if (CodPartida==codigo){
					     monto= setNumero($(this).val());
						  suma=suma+Number(monto);
						 }
					 
					 
					 codigo1= idpartida1.substring(0, 3);
					 codigo1= codigo1.concat(".00.00.00");
					 
					 if (CodPartida==codigo1){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
						 }
	             }) //imput
			 })	
			 
			 return suma; 
}

/*function actualizarMonto(CodPartida,cont,aespecifica1) {

		var suma=0; 
		var codigo=""; 
		var codigo1="";
		var codigo2=""; 
		var valor='';
		var contador=0;
		

   $("#codpartida td").each(function (){
   
 
   
       var presupuesto = $(document.getElementById('ind_tipo_presupuesto')).val();   
	   
		  
				 $(this).find('input').each (function() {
				 
				     idpartida1 = $(this).attr('id');
					 aespecifica = $(this).attr('aespecifica');
					 
					 
					  valor= $(this).val();  
					  $(this).val(valor).formatCurrency();
				
					 if (cont<=0)
					 {  
              		   valor= $(this).val();  
					   $(this).val(valor).formatCurrency();
					   cont++; 
					 }  
					
					
				  if(presupuesto=="P") 
				  {
				     //var aesp_nueva = $(document.getElementById('Aespecifica')).val();
				
					   
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 				 
					  if (CodPartida==codigo && aespecifica1==aespecifica){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
						 }
					 
					 
					 codigo1= idpartida1.substring(0, 3);
					 codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
						 }
						 
						 
					 codigo2= idpartida1.substring(0, 9);
					 codigo2= codigo2.concat(".00");
					 
					  if (CodPartida==codigo2 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					 }
					  
					  
					
				  }
				  else
				  {
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
				      if (CodPartida==codigo){
					    monto= setNumero($(this).val());
					    suma=suma+Number(monto);
					 }
					 
					 
					  codigo1= idpartida1.substring(0, 3);
					  codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
						 
						 
					   codigo2= idpartida1.substring(0, 9);
					   codigo2= codigo2.concat(".00");
					 
					   if (CodPartida==codigo2 ){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
				  }
				  
				  
										 	 
	             }) //imput
			 })	
			 
	    return suma; 
}
*/


//-----------------------------------------




//	funcion para convertir un numero frmateado en su valor real
function setNumero(num_formateado) {
	var num = num_formateado.toString();
	num = num.replace(/[.]/gi, "");
	num = num.replace(/[,]/gi, ".");
	
	var numero = new Number(num);
	return numero;
}



</script>

