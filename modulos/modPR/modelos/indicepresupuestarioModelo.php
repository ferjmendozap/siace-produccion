<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * Antepresupuesto: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |lbastardo@contraloriadebolivar.gob.ve   |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class indicepresupuestarioModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarPresupuesto($idPresupuesto)
    {
        $antepresupuesto = $this->_db->query("
         SELECT * FROM pr_b004_presupuesto pbp
		  INNER JOIN a018_seguridad_usuario asu ON asu.pk_num_seguridad_usuario=pbp.fk_a018_num_seguridad_usuario
		 WHERE pbp.pk_num_presupuesto='$idPresupuesto'
        ");
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
	
        return $antepresupuesto->fetch();
    }


    public function metMostrarIndicePresupuestario($idIndicePresupuestario)
    {
        $antepresupuesto = $this->_db->query("
         SELECT * FROM pr_b016_indice_presupuestario pbip
		    WHERE pbip.pk_num_indice_presupuestario='$idIndicePresupuestario'
        ");
		
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
	
        return $antepresupuesto->fetch();
    }


    public function metMostrarCategoriaSelect($tipo_presupuesto)
    {
       if ($tipo_presupuesto=='G')
	     $cod_detalle="AND amd.cod_detalle = 'G'";
	   else
	     $cod_detalle="AND amd.cod_detalle != 'G'";	
		 
		
		 $cuenta =  $this->_db->query(
            "SELECT amd.cod_detalle, amd.ind_nombre_detalle FROM a005_miscelaneo_maestro  amm
             INNER JOIN a006_miscelaneo_detalle amd ON (amm.pk_num_miscelaneo_maestro = amd.fk_a005_num_miscelaneo_maestro)
             WHERE amm.ind_nombre_maestro='Tipo de Categoria' $cod_detalle" 
        );
		
		
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }

    //AGREGADO PARA LISTAR  PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoDetalle($idPresupuesto)
    {
	    
        $partida = $this->_db->query(
            "SELECT
              *, pca.num_monto_ajustado-pca.num_monto_compromiso AS num_monto_disponible
            FROM
               pr_b004_presupuesto pba
               INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
            WHERE pba.pk_num_presupuesto='$idPresupuesto'
            ORDER by pbp.cod_partida ASC
          " );
		  
		  //var_dump($partida);
		  // exit;
       
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------
	
	//AGREGADO PARA LISTAR  PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoDetallepc($idPresupuesto, $idpartida, $aespecifica=false)
    {
	    $year_actual=date("Y");
		
		if ($aespecifica)
			$partida = $this->_db->query(
				"SELECT
				  SUM(cdoc.num_monto) AS precompromiso
				FROM
				   pr_b004_presupuesto pba
				   INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
				   INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
				   LEFT JOIN cp_d013_obligacion_cuentas cdoc ON pbp.pk_num_partida_presupuestaria = cdoc.fk_prb002_num_partida_presupuestaria
				   INNER JOIN cp_d001_obligacion cdo ON cdoc.fk_cpd001_num_obligacion = cdo.pk_num_obligacion
				WHERE pba.pk_num_presupuesto='$idPresupuesto' AND pbp.pk_num_partida_presupuestaria=$idpartida AND pba.fec_anio='$year_actual' AND cdo.fec_anio='$year_actual'
				AND pca.fk_pr006_num_aespecifica='$aespecifica'
				ORDER by pbp.cod_partida ASC"
			);
		else 
		   $partida = $this->_db->query(
            "SELECT
              SUM(cdoc.num_monto) AS precompromiso
            FROM
               pr_b004_presupuesto pba
               INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
			   LEFT JOIN cp_d013_obligacion_cuentas cdoc ON pbp.pk_num_partida_presupuestaria = cdoc.fk_prb002_num_partida_presupuestaria
               INNER JOIN cp_d001_obligacion cdo ON cdoc.fk_cpd001_num_obligacion = cdo.pk_num_obligacion
            WHERE pba.pk_num_presupuesto='$idPresupuesto' AND pbp.pk_num_partida_presupuestaria=$idpartida AND pba.fec_anio='$year_actual' AND cdo.fec_anio='$year_actual'
            ORDER by pbp.cod_partida ASC"
        );
		//var_dump($partida);
		//exit;
		
        $partida->setFetchMode(PDO::FETCH_ASSOC);
		$registro=$partida->fetch();
        return $registro['precompromiso'];
		
    }
	//-------------------------------------------------------------------------------------------------------

     //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoTitulo()
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	
	
	
	//-------------------------------------------------------------------------------------------------------

      //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoTituloEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T' and pk_num_partida_presupuestaria='$id'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------


    
	public function metExisteAespecificaEs($id,$idPresupuesto)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_c002_presupuesto_det 
            WHERE  fk_pr006_num_aespecifica='$id' AND fk_prb004_num_presupuesto='$idPresupuesto'
            
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	//-------------------------------------------------------------------------------------------------------

  
    public function metCuentaListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  pr_b001_tipo_cuenta"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	

    public function metListarIndicePresupuestario($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
	
        $antepresupuesto = $this->_db->query(
            "SELECT *, pbip.num_estatus AS estatus FROM pr_b016_indice_presupuestario  pbip
			  INNER JOIN pr_b008_sector pbs ON (pbs.pk_num_sector=pbip.fk_prb008_num_sector)
			  INNER JOIN pr_b009_programa pbp ON (pbp.pk_num_programa=pbip.fk_prb009_num_programa)
			  INNER JOIN pr_b010_subprograma pbsp ON (pbsp.pk_num_subprograma=pbip.fk_prb010_num_subprograma)
			  INNER JOIN pr_b005_proyecto pbpr ON (pbpr.pk_num_proyecto=pbip.fk_prb005_num_proyecto)
			  INNER JOIN pr_b011_actividad pba ON (pba.pk_num_actividad=pbip.fk_prb011_num_actividad)
			  INNER JOIN pr_b012_unidad_ejecutora pbue ON (pbue.pk_num_unidad_ejecutora=pbip.fk_prb012_unidad_ejecutora)
			$status"
        );
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
		
        return $antepresupuesto->fetchAll();
    }
	
	
	public function metListarIndicePresupuestarioPrograma()
    {
	
        $antepresupuesto = $this->_db->query(
            "SELECT *, pbip.num_estatus AS estatus FROM pr_b016_indice_presupuestario  pbip
			  INNER JOIN pr_b008_sector pbs ON (pbs.pk_num_sector=pbip.fk_prb008_num_sector)
			  INNER JOIN pr_b009_programa pbp ON (pbp.pk_num_programa=pbip.fk_prb009_num_programa)
			  INNER JOIN pr_b010_subprograma pbsp ON (pbsp.pk_num_subprograma=pbip.fk_prb010_num_subprograma)
			  INNER JOIN pr_b005_proyecto pbpr ON (pbpr.pk_num_proyecto=pbip.fk_prb005_num_proyecto)
			  INNER JOIN pr_b011_actividad pba ON (pba.pk_num_actividad=pbip.fk_prb011_num_actividad)
			  INNER JOIN pr_b012_unidad_ejecutora pbue ON (pbue.pk_num_unidad_ejecutora=pbip.fk_prb012_unidad_ejecutora) 
			  WHERE pbip.ind_tipo_presupuesto='G' AND pbip.ind_tipo_categoria='G'"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
		
        return $antepresupuesto->fetchAll();
    }
	
	
	
	public function metObtenerUltimoIndicePresupuestario()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_indice_presupuestario FROM pr_b016_indice_presupuestario ORDER BY ind_cod_indice_presupuestario DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_indice_presupuestario'];
    }

	public function metObtenerIndiceSetor($sector)
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_sector FROM pr_b008_sector WHERE pk_num_sector=$sector"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_sector'];
    }
	
	public function metObtenerIndicePrograma($programa)
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT num_cod_programa FROM pr_b009_programa WHERE pk_num_programa=$programa"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['num_cod_programa'];
    }
	
	public function metObtenerIndiceSubPrograma($subprograma)
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT num_cod_subprograma FROM pr_b010_subprograma WHERE pk_num_subprograma=$subprograma"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['num_cod_subprograma'];
    }
	
	public function metObtenerIndiceActividad($actividad)
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT num_cod_actividad FROM pr_b011_actividad WHERE pk_num_actividad=$actividad"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['num_cod_actividad'];
    }
	
	public function metObtenerIndiceUnidadEjecutora($unidadejecutora)
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT cod_unidad_ejecutora FROM pr_b012_unidad_ejecutora WHERE pk_num_unidad_ejecutora=$unidadejecutora"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['cod_unidad_ejecutora'];
    }
	
	
	 public function metCrearIndicePresupuestario($ind_tipo_presupuesto, $ind_tipo_categoria, $organismo, $sector, $programa, $subprograma, $proyecto, $actividad, $unidadejecutora, $estatus)
    {
	   
	  // var_dump($organismo);
        $this->_db->beginTransaction();
		
		  /*  $registro=$this->metObtenerUltimoIndicePresupuestario();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(3-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;*/
			
			$IndiceSector=$this->metObtenerIndiceSetor($sector);
			$IndicePrograma=$this->metObtenerIndicePrograma($programa);
			$IndiceSubPrograma=$this->metObtenerIndiceSubPrograma($subprograma);
			$IndiceActividad=$this->metObtenerIndiceActividad($actividad);
			$IndiceUnidadEjecutora=$this->metObtenerIndiceUnidadEjecutora($unidadejecutora);
		
		    if ($ind_tipo_categoria=='G')
			  $tipo_categoria='PG';
			if ($ind_tipo_categoria=='O')
			  $tipo_categoria='PY';
			if ($ind_tipo_categoria=='C')
			  $tipo_categoria='CE';
			
			$IndicePresupuestario=$tipo_categoria.$IndiceSector.$IndicePrograma.$IndiceSubPrograma.$IndiceActividad.$IndiceUnidadEjecutora;
			
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b016_indice_presupuestario
                  SET
				    ind_cod_indice_presupuestario=:ind_cod_indice_presupuestario,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					fec_ultima_modificacion=NOW(),    
					ind_tipo_presupuesto=:ind_tipo_presupuesto,
					ind_tipo_categoria=:ind_tipo_categoria,
					num_estatus=:num_estatus,	 	
					fk_a001_num_organismo=:fk_a001_num_organismo, 
					fk_prb008_num_sector=:fk_prb008_num_sector, 
					fk_prb009_num_programa=:fk_prb009_num_programa, 	
					fk_prb010_num_subprograma=:fk_prb010_num_subprograma,
					fk_prb005_num_proyecto=:fk_prb005_num_proyecto,
					fk_prb011_num_actividad=:fk_prb011_num_actividad,
					fk_prb012_unidad_ejecutora=:fk_prb012_unidad_ejecutora");
					
		    $nuevoRegistro->execute(array(
			    
				'ind_cod_indice_presupuestario'=>$IndicePresupuestario,
				'ind_tipo_presupuesto'=>$ind_tipo_presupuesto,
				'ind_tipo_categoria'=>$ind_tipo_categoria,
                'num_estatus'=>$estatus,
				'fk_a001_num_organismo'=>$organismo,
				'fk_prb008_num_sector'=>$sector,
				'fk_prb009_num_programa'=>$programa,
				'fk_prb010_num_subprograma'=>$subprograma,
				'fk_prb005_num_proyecto'=>$proyecto,
				'fk_prb011_num_actividad'=>$actividad,
				'fk_prb012_unidad_ejecutora'=>$unidadejecutora,	
						
            ));
			
            $idRegistro= $this->_db->lastInsertId();
			  
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }




public function metModificarIndicePresupuestario($ind_tipo_presupuesto, $ind_tipo_categoria, $organismo, $sector, $programa, $subprograma, $proyecto, $actividad,$unidadejecutora, $estatus, $idindicepresupuestario)
    {
	
	        $IndiceSector=$this->metObtenerIndiceSetor($sector);
			$IndicePrograma=$this->metObtenerIndicePrograma($programa);
			$IndiceSubPrograma=$this->metObtenerIndiceSubPrograma($subprograma);
			$IndiceActividad=$this->metObtenerIndiceActividad($actividad);
			$IndiceUnidadEjecutora=$this->metObtenerIndiceUnidadEjecutora($unidadejecutora);
			
			if ($ind_tipo_categoria=='G')
			  $tipo_categoria='PG';
			if ($ind_tipo_categoria=='O')
			  $tipo_categoria='PY';
			if ($ind_tipo_categoria=='C')
			  $tipo_categoria='CE';
			
			$IndicePresupuestario=$tipo_categoria.$IndiceSector.$IndicePrograma.$IndiceSubPrograma.$IndiceActividad.$IndiceUnidadEjecutora;
			
	    
	  $this->_db->beginTransaction();
      	
            $nuevoRegistro=$this->_db->prepare("
                  UPDATE
                    pr_b016_indice_presupuestario
                  SET
				    ind_cod_indice_presupuestario=:ind_cod_indice_presupuestario,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					fec_ultima_modificacion=NOW(),    
					ind_tipo_presupuesto=:ind_tipo_presupuesto,
					ind_tipo_categoria=:ind_tipo_categoria,
					num_estatus=:num_estatus,	 	
					fk_a001_num_organismo=:fk_a001_num_organismo, 
					fk_prb008_num_sector=:fk_prb008_num_sector, 
					fk_prb009_num_programa=:fk_prb009_num_programa, 	
					fk_prb010_num_subprograma=:fk_prb010_num_subprograma,
					fk_prb005_num_proyecto=:fk_prb005_num_proyecto,
					fk_prb011_num_actividad=:fk_prb011_num_actividad,
					fk_prb012_unidad_ejecutora=:fk_prb012_unidad_ejecutora
				WHERE  pk_num_indice_presupuestario='$idindicepresupuestario'
					");
					
 
	
		    $nuevoRegistro->execute(array(
			    'ind_cod_indice_presupuestario'=>$IndicePresupuestario,
				'ind_tipo_presupuesto'=>$ind_tipo_presupuesto,
				'ind_tipo_categoria'=>$ind_tipo_categoria,
                'num_estatus'=>$estatus,
				'fk_a001_num_organismo'=>$organismo,
				'fk_prb008_num_sector'=>$sector,
				'fk_prb009_num_programa'=>$programa,
				'fk_prb010_num_subprograma'=>$subprograma,
				'fk_prb005_num_proyecto'=>$proyecto,
				'fk_prb011_num_actividad'=>$actividad,
				'fk_prb012_unidad_ejecutora'=>$unidadejecutora,	
						
            ));
			
            //$idRegistro= $this->_db->lastInsertId();
			  
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return  $idindicepresupuestario;
            }
    }


    public function metEliminarIndicePresupuestario($idindicepresupuestario)
    {
        $this->_db->beginTransaction();
		
		   $eliminar = $this->_db->query(
            "DELETE FROM pr_b016_indice_presupuestario WHERE pk_num_indice_presupuestario='$idindicepresupuestario'"
			);
		
             $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idindicepresupuestario;
            }
    }


/*	 public function metCrearIndicePresupuestario($ind_tipo_presupuesto, $organismo, $sector, $programa, $subprograma, $proyecto, $actividad,$aespecificapresupuesto, $acentralizada, $unidadejecutora)
    {
	   
	   
	  // var_dump($organismo);
        $this->_db->beginTransaction();
		
		    $registro=$this->metObtenerUltimoIndicePresupuestario();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(3-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			 
			
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b016_indice_presupuestario
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					fec_ultima_modificacion=NOW(),    
					ind_tipo_presupuesto=:ind_tipo_presupuesto,
					num_estatus=:num_estatus,	 	
					fk_a001_num_organismo=:fk_a001_num_organismo, 
					fk_prb008_num_sector=:fk_prb008_num_sector, 
					fk_prb009_num_programa=:fk_prb009_num_programa, 	
					fk_prb010_num_subprograma=:fk_prb010_num_subprograma,
					fk_prb005_num_proyecto=:fk_prb005_num_proyecto,
					fk_prb011_num_actividad=:fk_prb011_num_actividad,
					fk_prb012_unidad_ejecutora=:fk_prb012_unidad_ejecutora,
					fk_prb006_aespecifica=:fk_prb006_aespecifica,
					fk_prb007_acentralizada=:fk_prb007_acentralizada");
					
 
	
		    $nuevoRegistro->execute(array(
			    
				'ind_tipo_presupuesto'=>$ind_tipo_presupuesto,
                'num_estatus'=>'PR',
				'fk_a001_num_organismo'=>$organismo,
				'fk_prb008_num_sector'=>$sector,
				'fk_prb009_num_programa'=>$programa,
				'fk_prb010_num_subprograma'=>$subprograma,
				'fk_prb005_num_proyecto'=>$proyecto,
				'fk_prb011_num_actividad'=>$actividad,
				'fk_prb012_unidad_ejecutora'=>$unidadejecutora,	
				'fk_prb006_aespecifica'=>$aespecificapresupuesto,	
				'fk_prb007_acentralizada'=>$acentralizada,
						
            ));
			
            $idRegistro= $this->_db->lastInsertId();
			  
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }*/
}
