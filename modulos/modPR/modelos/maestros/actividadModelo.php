<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |         Liduvica Bastardo                 |         liduvica@hotmail.com       |        0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class actividadModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarActividad($idActividad)
    {
        $tipoCuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b011_actividad 
          WHERE
            pk_num_actividad='$idActividad'
        ");
		
	
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetch();
    }

    public function metListarActividad()
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b011_actividad"
        );
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }
	
	
	public function metMostrarActividadSelect($idProyecto)
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b011_actividad WHERE fk_prb005_num_proyecto=$idProyecto"
        );
		
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }



    public function metObtenerUltimaActividad()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_actividad FROM pr_b011_actividad ORDER BY ind_cod_actividad DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_actividad'];
    }




    public function metCrearActividad($nombre,$status,$proyecto, $numactividad, $flags_aespecifica)
    {
	
	   $registro=$this->metObtenerUltimaActividad();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(2-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			
	
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b011_actividad
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_cod_actividad=:ind_cod_actividad,
                    num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion, fk_prb005_num_proyecto=:fk_prb005_num_proyecto, num_cod_actividad=:num_cod_actividad,
					num_flags_aespecifica=:num_flags_aespecifica
                ");
            $nuevoRegistro->execute(array(
                'ind_cod_actividad'=>$registro,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
                'fk_prb005_num_proyecto'=>$proyecto,
				'num_cod_actividad'=>$numactividad,
				'num_flags_aespecifica'=>$flags_aespecifica,
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarActividad($nombre,$status,$proyecto,$numactividad,$flags_aespecifica, $idActividad)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        pr_b011_actividad
                      SET
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                       num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion, fk_prb005_num_proyecto=:fk_prb005_num_proyecto,
					   num_cod_actividad=:num_cod_actividad, num_flags_aespecifica=:num_flags_aespecifica
                      WHERE
                        pk_num_actividad='$idActividad'
            ");
            $nuevoRegistro->execute(array(
                
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
				'fk_prb005_num_proyecto'=>$proyecto,
				'num_cod_actividad'=>$numactividad,
				'num_flags_aespecifica'=>$flags_aespecifica,
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idActividad;
            }
    }

    public function metEliminarActividad($idActividad)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM  pr_b011_actividad WHERE pk_num_actividad=:pk_num_actividad
            ");
            $elimar->execute(array(
                'pk_num_actividad'=>$idActividad
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idActividad;
            }

    }

}
