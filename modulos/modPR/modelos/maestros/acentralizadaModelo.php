<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |         Liduvica Bastardo                 |         liduvica@hotmail.com       |        0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class acentralizadaModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarAcentralizada($idAcentralizada)
    {
        $tipoCuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b007_acentralizada 
          WHERE
            pk_num_acentralizada='$idAcentralizada'
        ");
		
	
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetch();
    }

    public function metListarAcentralizada()
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b007_acentralizada"
        );
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }


    public function metObtenerUltimaAcentralizada()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_acentralizada FROM pr_b007_acentralizada ORDER BY ind_cod_acentralizada DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_acentralizada'];
    }




    public function metCrearAcentralizada($nombre,$status)
    {
	
	   $registro=$this->metObtenerUltimaAcentralizada();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(2-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			
	
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b007_acentralizada
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_cod_acentralizada=:ind_cod_acentralizada,
                    num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion 
                ");
            $nuevoRegistro->execute(array(
                'ind_cod_acentralizada'=>$registro,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
               
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarAcentralizada($nombre,$status,$idAcentralizada)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        pr_b007_acentralizada
                      SET
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                       num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion 
                      WHERE
                        pk_num_acentralizada='$idAcentralizada'
            ");
            $nuevoRegistro->execute(array(
                
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAcentralizada;
            }
    }

    public function metEliminarAcentralizada($idAcentralizada)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM  pr_b007_acentralizada WHERE pk_num_acentralizada=:pk_num_acentralizada
            ");
            $elimar->execute(array(
                'pk_num_acentralizada'=>$idAcentralizada
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAcentralizada;
            }

    }

}
