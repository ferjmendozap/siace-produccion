<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Presupuesto
 * Antepresupuesto: Modelo
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |            Liduvica Bastardo              |lbastardo@contraloriadebolivar.gob.ve   |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class antepresupuestoModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }


    public function metMostrarAntepresupuesto($idAntepresupuesto)
    {
        $antepresupuesto = $this->_db->query("
         SELECT * FROM pr_b003_antepresupuesto pba
		 INNER JOIN a018_seguridad_usuario asu ON asu.pk_num_seguridad_usuario=pba.fk_a018_num_seguridad_usuario
		 WHERE pba.pk_num_antepresupuesto='$idAntepresupuesto'
		 
        ");
		
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $antepresupuesto->fetch();
    }
	
	
	
	 public function metMostrarPresupuestoSelect($ind_tipo_presupuesto, $ind_tipo_categoria)
    {
        $cuenta = $this->_db->query("
          SELECT * FROM pr_b016_indice_presupuestario pbip
		    WHERE pbip.ind_tipo_presupuesto='$ind_tipo_presupuesto' AND pbip.ind_tipo_categoria='$ind_tipo_categoria' ");
			
		$cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
		
    }
	
	
	    public function metListarIndicePresupuestario()
    {
        $indice = $this->_db->query(
            "SELECT * FROM pr_b016_indice_presupuestario"
        );
        $indice->setFetchMode(PDO::FETCH_ASSOC);
        return $indice->fetchAll();
    }
	
	
	public function metMostrarIndicePresupuestarioSelect($idIndicePresupuestario)
    {
      
		$antepresupuesto = $this->_db->query("
         SELECT *, pbs.ind_descripcion as sector_descripcion, pbp.ind_descripcion as programa_descripcion, pbsp.ind_descripcion as subprograma_descripcion,
		   pbpr.ind_descripcion as proyecto_descripcion, pba.ind_descripcion as actividad_descripcion, pbue.ind_descripcion as unidad_descripcion
		   FROM pr_b016_indice_presupuestario  pbip
			  INNER JOIN pr_b008_sector pbs ON (pbs.pk_num_sector=pbip.fk_prb008_num_sector)
			  INNER JOIN pr_b009_programa pbp ON (pbp.pk_num_programa=pbip.fk_prb009_num_programa)
			  INNER JOIN pr_b010_subprograma pbsp ON (pbsp.pk_num_subprograma=pbip.fk_prb010_num_subprograma)
			  INNER JOIN pr_b005_proyecto pbpr ON (pbpr.pk_num_proyecto=pbip.fk_prb005_num_proyecto)
			  INNER JOIN pr_b011_actividad pba ON (pba.pk_num_actividad=pbip.fk_prb011_num_actividad)
			  INNER JOIN pr_b012_unidad_ejecutora pbue ON (pbue.pk_num_unidad_ejecutora=pbip.fk_prb012_unidad_ejecutora) 
			  WHERE pbip.pk_num_indice_presupuestario='$idIndicePresupuestario'
        ");
		
		
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
	
        return $antepresupuesto->fetchAll();
    }

    //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarAntepresupuestoDetalle($idAntepresupuesto)
    {
        $partida = $this->_db->query(
           "SELECT
              *
             FROM
               pr_b003_antepresupuesto pba
               INNER JOIN pr_c001_antepresupuesto_det pca ON pba.pk_num_antepresupuesto = pca.fk_prb003_num_antepresupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
            WHERE pba.pk_num_antepresupuesto='$idAntepresupuesto'
            ORDER by pbp.cod_partida ASC
          "
        );
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------

     //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarAntepresupuestoTitulo()
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------

      
	public function metMostrarAntepresupuestoTituloEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T' and pk_num_partida_presupuestaria='$id'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	//-------------------------------------------------------------------------------------------------------

      
	public function metMostrarAntepresupuestoAespecificaEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b006_aespecifica 
            WHERE  pk_num_aespecifica='$id'
            ORDER by pk_num_aespecifica ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	//-------------------------------------------------------------------------------------------------------

    
	public function metExisteAespecificaEs($id,$idAntepresupuesto)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_c001_antepresupuesto_det 
            WHERE  fk_pr_b006_aespecifica='$id' AND fk_prb003_num_antepresupuesto='$idAntepresupuesto'
            
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	//-------------------------------------------------------------------------------------------------------

	
	
  
    public function metCuentaListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  pr_b001_tipo_cuenta"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	
	
	
	public function metListarOrganismoInt()
    {
        $organismoint= $this->_db->query("SELECT * FROM a001_organismo WHERE ind_tipo_organismo='I'");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetchAll();
		
    }
	
	//---------------------------------------------------------------------------------------------------

    public function metListarAntepresupuesto($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
        $antepresupuesto = $this->_db->query(
            "SELECT * FROM pr_b003_antepresupuesto $status"
        );
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $antepresupuesto->fetchAll();
    }
	
	
    public function metObtenerUltimoAntepresupuesto()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT cod_antepresupuesto FROM pr_b003_antepresupuesto ORDER BY cod_antepresupuesto DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['cod_antepresupuesto'];
    }


    public function metCrearAntepresupuesto($fec_anio, $fec_antepresupuesto,$fec_inicio, $dec_fin, $ind_numero_gaceta, $fec_gaceta, $ind_numero_decreto, $fec_decreto,$ind_tipo_presupuesto, $num_monto_presupuestado, $num_monto_generado, $partidas=false, $monto=false, $aespecifica=false, $organismo, $sector, $programa, $subprograma, $proyecto, $actividad, $unidadejecutora, $indicepresupuestario)
    {
	   
        $this->_db->beginTransaction();
		
		    $registro=$this->metObtenerUltimoAntepresupuesto();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(3-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			 
			
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b003_antepresupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fk_rhb001_num_empleado_creado_por='$this->atIdUsuario',fec_ultima_modificacion=NOW(),  
					cod_antepresupuesto=:cod_antepresupuesto, fec_anio=:fec_anio,fec_antepresupuesto=:fec_antepresupuesto, fec_inicio=:fec_inicio, dec_fin=:dec_fin,  
					ind_numero_gaceta=:ind_numero_gaceta, fec_gaceta=:fec_gaceta,ind_numero_decreto=:ind_numero_decreto,fec_decreto=:fec_decreto,    
					ind_tipo_presupuesto=:ind_tipo_presupuesto,fec_creado=CURDATE(),  
					num_monto_presupuestado=:num_monto_presupuestado, num_monto_generado=:num_monto_generado, ind_estado=:ind_estado, fec_aprobado=:fec_aprobado, 		 	
					fk_a001_num_organismo=:fk_a001_num_organismo, fk_prb008_num_sector=:fk_prb008_num_sector, fk_prb009_num_programa=:fk_prb009_num_programa, 	
					fk_prb010_num_subprograma=:fk_prb010_num_subprograma, fk_prb005_num_proyecto=:fk_prb005_num_proyecto, fk_prb011_num_actividad=:fk_prb011_num_actividad,
					fk_prb012_unidad_ejecutora=:fk_prb012_unidad_ejecutora, fk_prb016_indice_presupuestario=:fk_prb016_indice_presupuestario
                ");
            $nuevoRegistro->execute(array(
			    'cod_antepresupuesto'=>$registro,
				'fec_antepresupuesto'=>$fec_antepresupuesto,
				'fec_anio'=>$fec_anio,
				'fec_inicio'=>$fec_inicio,
				'dec_fin'=>$dec_fin,
				'ind_numero_gaceta'=>$ind_numero_gaceta,
				'fec_gaceta'=>$fec_gaceta,
				'ind_numero_decreto'=>$ind_numero_decreto,
				'fec_decreto'=>$fec_decreto,
				'ind_tipo_presupuesto'=>$ind_tipo_presupuesto,
                'num_monto_presupuestado'=>$num_monto_presupuestado,
				'num_monto_generado'=>$num_monto_generado,
                'ind_estado'=>'PR',
				'fec_aprobado'=>'0000-00-00',
				'fk_a001_num_organismo'=>$organismo,
				'fk_prb008_num_sector'=>$sector,
				'fk_prb009_num_programa'=>$programa,
				'fk_prb010_num_subprograma'=>$subprograma,
				'fk_prb005_num_proyecto'=>$proyecto,
				'fk_prb011_num_actividad'=>$actividad,
				'fk_prb012_unidad_ejecutora'=>$unidadejecutora,	
				'fk_prb016_indice_presupuestario'=>$indicepresupuestario,	
				
						
            ));
            $idRegistro= $this->_db->lastInsertId();
			
			
		$cont=0;
		if(isset($partidas) && $partidas!=false){	
		  for($i=0;$i<count($partidas);$i++) {
		  $id=$partidas[$i];
		  $indicador=0;
		  
		  $indicador=count($this->metMostrarAntepresupuestoAespecificaEs($id));
		  $indicador=$indicador+count($this->metMostrarAntepresupuestoTituloEs($id));
		  
		  if ($indicador<=0)
		  {
		    $monto[$i]=$this->FormatoMonto_modelo($monto[$i]); 
			
			if(isset($aespecifica[$cont]))
			  $ae= $aespecifica[$cont]/100;
			else  
			  $ae=0;
			  
			   $cont=$cont+1;
		  
			$nuevaPartida=$this->_db->prepare("
                  INSERT INTO
                    pr_c001_antepresupuesto_det
                  SET
                    fk_prb003_num_antepresupuesto=:fk_prb003_num_antepresupuesto,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,  
					num_monto_presupuestado=:num_monto_presupuestado,
					fk_pr_b006_aespecifica=:fk_pr_b006_aespecifica,
					num_monto_aprobado=:num_monto_aprobado,
					fec_ultima_modificacion=NOW(),
					fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            ");
			
            $nuevaPartida->execute(array(
			    'fk_prb003_num_antepresupuesto'=>$idRegistro,
                'fk_prb002_num_partida_presupuestaria'=> $partidas[$i],
				'num_monto_presupuestado'=> $monto[$i],
				'fk_pr_b006_aespecifica'=> $ae,
				'num_monto_aprobado'=>0,
            ));
		    }
		  }	
       }  
			  
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }



    public function metModificarAntepresupuesto($fec_anio, $fec_antepresupuesto,$fec_inicio, $dec_fin, $ind_numero_gaceta, $fec_gaceta, $ind_numero_decreto, $fec_decreto,
	$ind_tipo_presupuesto, $num_monto_presupuestado, $num_monto_generado, $partidas=false, $monto=false, $aespecifica=false, $estado , $organismo, $sector, $programa, $subprograma, $proyecto, $actividad , $unidadejecutora, $indicepresupuestario, $idAntepresupuesto)
    {
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                         pr_b003_antepresupuesto
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),fec_anio=:fec_anio,fec_antepresupuesto=:fec_antepresupuesto,    
						fec_inicio=:fec_inicio, dec_fin=:dec_fin, ind_numero_gaceta=:ind_numero_gaceta, fec_gaceta=:fec_gaceta,ind_numero_decreto=:ind_numero_decreto,  
					    fec_decreto=:fec_decreto, num_monto_presupuestado=:num_monto_presupuestado, num_monto_generado=:num_monto_generado, 
						ind_tipo_presupuesto=:ind_tipo_presupuesto, ind_estado=:ind_estado, fk_a001_num_organismo=:fk_a001_num_organismo, 
						fk_prb008_num_sector=:fk_prb008_num_sector, fk_prb009_num_programa=:fk_prb009_num_programa, 	
						fk_prb010_num_subprograma=:fk_prb010_num_subprograma, fk_prb005_num_proyecto=:fk_prb005_num_proyecto, fk_prb011_num_actividad=:fk_prb011_num_actividad,
						fk_prb012_unidad_ejecutora=:fk_prb012_unidad_ejecutora, fk_prb016_indice_presupuestario=:fk_prb016_indice_presupuestario
					  WHERE 
					    pk_num_antepresupuesto='$idAntepresupuesto' 
            ");
			
            $nuevoRegistro->execute(array(
			    'fec_anio'=>$fec_anio,
				'fec_antepresupuesto'=>$fec_antepresupuesto,
				'fec_inicio'=>$fec_inicio,
				'dec_fin'=>$dec_fin,
				'ind_numero_gaceta'=>$ind_numero_gaceta,
				'fec_gaceta'=>$fec_gaceta,
				'ind_numero_decreto'=> $partidas[2],
				'fec_decreto'=>$fec_decreto,
                'num_monto_presupuestado'=>$num_monto_presupuestado,
				'num_monto_generado'=>$num_monto_generado,
				'ind_tipo_presupuesto'=>$ind_tipo_presupuesto, 
				'ind_estado'=>$estado,
				'fk_a001_num_organismo'=>$organismo,
				'fk_prb008_num_sector'=>$sector,
				'fk_prb009_num_programa'=>$programa,
				'fk_prb010_num_subprograma'=>$subprograma,
				'fk_prb005_num_proyecto'=>$proyecto,
				'fk_prb011_num_actividad'=>$actividad,
				'fk_prb012_unidad_ejecutora'=>$unidadejecutora,	
				'fk_prb016_indice_presupuestario'=>$indicepresupuestario,	
            ));
			
			
		  $eliminarAntePresupuestoDet = $this->_db->query(
            "DELETE FROM pr_c001_antepresupuesto_det WHERE fk_prb003_num_antepresupuesto='$idAntepresupuesto'"
        );	
			
			
	      $cont=0;
          if(isset($partidas) && $partidas!=false){	
		  for($i=0;$i<count($partidas);$i++) {
		   $id=$partidas[$i];
		  
		  $indicador=0;
		  $indicador1=0;
		  
		 
		  $indicador=count($this->metMostrarAntepresupuestoAespecificaEs($id));
		 
		  $indicador=$indicador+count($this->metMostrarAntepresupuestoTituloEs($id));
		  
		  if ($indicador<=0)
		  {
		         $monto[$i]=$this->FormatoMonto_modelo($monto[$i]); 
			
				if(isset($aespecifica[$cont]))
				{
				if ($aespecifica[$cont]<99)
				  $ae=$aespecifica[$cont];
				 else 
				  $ae= $aespecifica[$cont]/100;
				}  
				else 
				  $ae=0;
				  
				  $cont=$cont+1;
		  
			$nuevaPartida=$this->_db->prepare("
                  INSERT INTO
                    pr_c001_antepresupuesto_det
                  SET
                    fk_prb003_num_antepresupuesto=:fk_prb003_num_antepresupuesto,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,  
					fk_pr_b006_aespecifica=:fk_pr_b006_aespecifica,
					num_monto_presupuestado=:num_monto_presupuestado,
					num_monto_aprobado=:num_monto_aprobado,
					fec_ultima_modificacion=NOW(),
					fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            ");
			
			
            $nuevaPartida->execute(array(
			    'fk_prb003_num_antepresupuesto'=>$idAntepresupuesto,
                'fk_prb002_num_partida_presupuestaria'=> $partidas[$i],
				'num_monto_presupuestado'=> $monto[$i],
				'fk_pr_b006_aespecifica'=> $ae,
				'num_monto_aprobado'=>0,
				
				
            ));
			
		    }
		  }	
       }  
			  
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAntepresupuesto;
            }
			
    }


    public function metEliminarAntepresupuesto($idAntepresupuesto)
    {
        $this->_db->beginTransaction();
		
		   $eliminarAntePresupuestoDet = $this->_db->query(
            "DELETE FROM pr_c001_antepresupuesto_det WHERE fk_prb003_num_antepresupuesto='$idAntepresupuesto'"
			);
		
			
		    $eliminar = $this->_db->query(
            "DELETE FROM pr_b003_antepresupuesto WHERE pk_num_antepresupuesto='$idAntepresupuesto'"
			);
	
			
			
             $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAntepresupuesto;
            }
    }
	


//------------------------------------------------------------
     public function metMostrarDetalleAntepresupuesto($idAntepresupuesto, $partida_presupuestaria)
    {
        $antepresupuesto = $this->_db->query("
         SELECT COUNT(*) AS registros FROM pr_c001_antepresupuesto_det WHERE fk_prb003_num_antepresupuesto='$idAntepresupuesto' and   
		 fk_prb002_num_partida_presupuestaria='$partida_presupuestaria'
        ");
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['registros'];
    }

//---------------------------------------------------------------
	
	 public function metObtenerEstadoAntepresupuesto($idAntepresupuesto)
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_estado FROM pr_b003_antepresupuesto  WHERE pk_num_antepresupuesto='$idAntepresupuesto'"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_estado'];
    }
	
	
	public function metAccionesAntepresupuesto($idAntepresupuesto, $status, $partidas=false, $monto=false)
    {
	
	    $monto_total=0;
		$generar=0;
        $this->_db->beginTransaction();
		
		 if ($status=='RV') {
            $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_b003_antepresupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_estado=:ind_estado
                  WHERE
                    pk_num_antepresupuesto='$idAntepresupuesto'
            ");
        }
	   
        if ($status=='AP') {
            $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_b003_antepresupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_rhb001_num_empleado_aprobado_por='$this->atIdUsuario', fec_aprobado=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_antepresupuesto='$idAntepresupuesto'
            ");
       
	    }elseif ($status=='GE'){
	  
            $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_b003_antepresupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fec_generado=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_antepresupuesto='$idAntepresupuesto'
            ");
      
	   
	   }elseif ($status=='AN'){
	   
	      $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_b003_antepresupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_antepresupuesto='$idAntepresupuesto'
            ");
	        	
	   }   
	   
	   if ($status=='RE'){
	   
	     $estado=$this->metObtenerEstadoAntepresupuesto($idAntepresupuesto);
		  
		  if ($estado=='RV')
		      $status='PR';
		  elseif ($estado=='AP')	  
		      $status='RV';
		  
		   
	      $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_b003_antepresupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_antepresupuesto='$idAntepresupuesto'
            ");
			
	   }
	   
		$aprobarregistro->execute(array(
            'ind_estado' => $status,
            ));
			
		
	    if ($status=='GE') {
		
		//---------------------------------------
		if(isset($partidas) && $partidas!=false){	
		  for($i=0;$i<count($partidas);$i++) {
		  $id=$partidas[$i];
		  $indicador=0;
		  
		  $indicador=count($this->metMostrarAntepresupuestoAespecificaEs($id));
		  $indicador=$indicador+count($this->metMostrarAntepresupuestoTituloEs($id));
		  if ($indicador<=0)
		  {
		   
		   $monto_det=$this->FormatoMonto_modelo($monto[$id]); 
		   $monto_total=$monto_total+$monto_det;
		   
			$detalle=0;  
			
			$detalle=$this->metMostrarDetalleAntepresupuesto($idAntepresupuesto, $partidas[$i]);  
			if ($detalle>0)
			{
			    $nuevaPartida=$this->_db->prepare("
                  UPDATE
                    pr_c001_antepresupuesto_det
                  SET
					num_monto_aprobado=:num_monto_aprobado
				    WHERE fk_prb002_num_partida_presupuestaria='$partidas[$i]' AND  fk_prb003_num_antepresupuesto='$idAntepresupuesto'
                ");
			
                $nuevaPartida->execute(array(
				'num_monto_aprobado'=> $monto_det,
               ));
			   
		}else {
			
			$nuevoDetalle=$this->_db->prepare("
                  INSERT INTO
                    pr_c001_antepresupuesto_det
                  SET
                    fk_prb003_num_antepresupuesto=:fk_prb003_num_antepresupuesto,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,  
					num_monto_presupuestado=:num_monto_presupuestado,
					num_monto_aprobado=:num_monto_aprobado,
					fec_ultima_modificacion=NOW(),
					fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            ");
			
            $nuevoDetalle->execute(array(
			    'fk_prb003_num_antepresupuesto'=>$idAntepresupuesto,
                'fk_prb002_num_partida_presupuestaria'=>$partidas[$i],
				'num_monto_presupuestado'=> 0,
				'num_monto_aprobado'=>$monto_det,
            ));
			
		  } 
			
			
		    }// fin if $indicador
		  }	//fin if for
       } //fin  
	//-------------------------------------------------------	
		
		   $generar_presupuesto=$this->_db->prepare("
                  UPDATE
                    pr_b003_antepresupuesto
                  SET
					num_monto_generado=:num_monto_generado
				    WHERE pk_num_antepresupuesto='$idAntepresupuesto'
            ");
			
			
	        $generar_presupuesto->execute(array(
			 	'num_monto_generado'=> $monto_total,
            ));
			
				
		    $codigo=count($this->metListarPresupuesto())+1;
            $mun="0";
            for($i=0;$i<(3-strlen($codigo));$i++){
                $mun.="0";
            }
			
			
		    $codigo=$mun.$codigo; 
			$generar = $this->_db->prepare("SELECT u_pr_generar_presupuesto('".$idAntepresupuesto."','".$codigo."','".$this->atIdUsuario."')");
			$generar->execute();
			//$error =  $generar->errorInfo();
	    }	
	
            $error =  $aprobarregistro->errorInfo();
			
			if ($generar)
			   $error =  $generar->errorInfo();
						
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAntepresupuesto;
            }
    }
	
	
	
	
	public function metListarPresupuesto($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
        $antepresupuesto = $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto $status"
        );
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $antepresupuesto->fetchAll();
    }
	
	
	
    public function FormatoMonto_modelo($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
}
