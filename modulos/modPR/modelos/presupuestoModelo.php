<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * Antepresupuesto: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                 lbastardo@contraloriadebolivar.gob.ve   |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class presupuestoModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarPresupuesto($idPresupuesto)
    {
        $antepresupuesto = $this->_db->query("
         SELECT *, pbip.ind_tipo_categoria FROM pr_b004_presupuesto pbp
		  INNER JOIN a018_seguridad_usuario asu ON asu.pk_num_seguridad_usuario=pbp.fk_a018_num_seguridad_usuario
		  INNER JOIN pr_b016_indice_presupuestario pbip ON (pbp.fk_prb016_indice_presupuestario = pbip.pk_num_indice_presupuestario)
		 WHERE pbp.pk_num_presupuesto='$idPresupuesto'
        ");
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
	
        return $antepresupuesto->fetch();
    }

    //AGREGADO PARA LISTAR  PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoDetalle($idPresupuesto)
    {
        $partida = $this->_db->query(
            "SELECT
              *, pca.num_monto_ajustado-pca.num_monto_compromiso AS num_monto_disponible
            FROM
               pr_b004_presupuesto pba
               INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
            WHERE pba.pk_num_presupuesto='$idPresupuesto'
            ORDER by pbp.cod_partida ASC
          " );
		  
		  //var_dump($partida);
		  // exit;
       
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------
	
	//AGREGADO PARA LISTAR  PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoDetallepc($idPresupuesto, $idpartida, $aespecifica=false)
    {
	    $year_actual=date("Y");
		
		if ($aespecifica)
			$partida = $this->_db->query(
				"SELECT
				  SUM(cdoc.num_monto) AS precompromiso
				FROM
				   pr_b004_presupuesto pba
				   INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
				   INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
				   LEFT JOIN cp_d013_obligacion_cuentas cdoc ON pbp.pk_num_partida_presupuestaria = cdoc.fk_prb002_num_partida_presupuestaria
				   INNER JOIN cp_d001_obligacion cdo ON cdoc.fk_cpd001_num_obligacion = cdo.pk_num_obligacion
				WHERE pba.pk_num_presupuesto='$idPresupuesto' AND pbp.pk_num_partida_presupuestaria=$idpartida AND pba.fec_anio='$year_actual' AND cdo.fec_anio='$year_actual'
				AND pca.fk_pr006_num_aespecifica='$aespecifica'
				ORDER by pbp.cod_partida ASC"
			);
		else 
		   $partida = $this->_db->query(
            "SELECT
              SUM(cdoc.num_monto) AS precompromiso
            FROM
               pr_b004_presupuesto pba
               INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
			   LEFT JOIN cp_d013_obligacion_cuentas cdoc ON pbp.pk_num_partida_presupuestaria = cdoc.fk_prb002_num_partida_presupuestaria
               INNER JOIN cp_d001_obligacion cdo ON cdoc.fk_cpd001_num_obligacion = cdo.pk_num_obligacion
            WHERE pba.pk_num_presupuesto='$idPresupuesto' AND pbp.pk_num_partida_presupuestaria=$idpartida AND pba.fec_anio='$year_actual' AND cdo.fec_anio='$year_actual'
            ORDER by pbp.cod_partida ASC"
        );
		//var_dump($partida);
		//exit;
		
        $partida->setFetchMode(PDO::FETCH_ASSOC);
		$registro=$partida->fetch();
        return $registro['precompromiso'];
		
    }
	//-------------------------------------------------------------------------------------------------------

     //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoTitulo()
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------

      //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoTituloEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T' and pk_num_partida_presupuestaria='$id'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------


    
	public function metExisteAespecificaEs($id,$idPresupuesto)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_c002_presupuesto_det 
            WHERE  fk_pr006_num_aespecifica='$id' AND fk_prb004_num_presupuesto='$idPresupuesto'
            
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	//-------------------------------------------------------------------------------------------------------

  
    public function metCuentaListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  pr_b001_tipo_cuenta"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	

    public function metListarPresupuesto($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
        $antepresupuesto = $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto $status"
        );
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
		
        return $antepresupuesto->fetchAll();
    }
	

	
}
