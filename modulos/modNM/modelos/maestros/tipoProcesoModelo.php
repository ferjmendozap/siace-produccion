<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class tipoProcesoModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoProceso($idProceso)
    {
        $tipoProceso = $this->_db->query("
          SELECT
            nm_b003.*,
            a018.ind_usuario
          FROM
            nm_b003_tipo_proceso nm_b003
          INNER JOIN
            a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = nm_b003.fk_a018_num_seguridad_usuario
          WHERE
            nm_b003.pk_num_tipo_proceso='$idProceso'
        ");
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetch();
    }

    public function metListarTipoProceso()
    {
        $tipoProceso = $this->_db->query(
            "SELECT * FROM nm_b003_tipo_proceso"
        );
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetchAll();
    }

    public function metCrearTipoProceso($codProceso,$nombre,$flag,$status,$flagIndividual)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    nm_b003_tipo_proceso
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), cod_proceso=:cod_proceso,
                    num_estatus=:num_estatus ,ind_nombre_proceso=:ind_nombre_proceso , num_flag_adelanto=:num_flag_adelanto, num_flag_individual=:num_flag_individual
                ");
            $nuevoRegistro->execute(array(
                'cod_proceso'=>$codProceso,
                'num_estatus'=>$status,
                'ind_nombre_proceso'=>$nombre,
                'num_flag_adelanto'=>$flag,
                'num_flag_individual'=>$flagIndividual,
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarTipoPorceso($codProceso,$nombre,$flag,$status,$flagIndividual,$idProceso)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        nm_b003_tipo_proceso
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), cod_proceso=:cod_proceso,
                        num_estatus=:num_estatus ,ind_nombre_proceso=:ind_nombre_proceso , num_flag_adelanto=:num_flag_adelanto,
                        num_flag_individual=:num_flag_individual
                      WHERE
                        pk_num_tipo_proceso='$idProceso'
            ");
            $nuevoRegistro->execute(array(
                'cod_proceso'=>$codProceso,
                'num_estatus'=>$status,
                'ind_nombre_proceso'=>$nombre,
                'num_flag_adelanto'=>$flag,
                'num_flag_individual'=>$flagIndividual,
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idProceso;
            }
    }

    public function metEliminarTipoProceso($idProceso)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM nm_b003_tipo_proceso WHERE pk_num_tipo_proceso=:pk_num_tipo_proceso
            ");
            $elimar->execute(array(
                'pk_num_tipo_proceso'=>$idProceso
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idProceso;
            }

    }

}
