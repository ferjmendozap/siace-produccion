<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        04-01-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class interfazCxPModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metProcesoNomina($idProcesoNomina)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              concat_ws(
                '-',
                nm_c001_tipo_nomina_periodo.fec_anio,
                nm_c001_tipo_nomina_periodo.fec_mes
              ) AS PERIODO,
              concat_ws(
                '',
                nm_b003_tipo_proceso.cod_proceso,
                nm_c001_tipo_nomina_periodo.fec_anio,
                nm_c001_tipo_nomina_periodo.fec_mes
              ) AS codigo,
              concat_ws(
                '',
                  nm_b001_tipo_nomina.cod_tipo_nomina,
                  nm_b003_tipo_proceso.cod_proceso,
                  concat_ws(
                    '',
                    nm_c001_tipo_nomina_periodo.fec_anio,
                    nm_c001_tipo_nomina_periodo.fec_mes
                  )
              ) AS procesoPeriodo,
              fk_000_num_tipo_documento,
              nm_b001_tipo_nomina.ind_titulo_boleta,
              nm_b003_tipo_proceso.ind_nombre_proceso,
              nm_b003_tipo_proceso.cod_proceso,
              nm_b003_tipo_proceso.num_flag_individual
            FROM
              nm_c002_proceso_periodo
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
            INNER JOIN nm_d004_proceso_tipo_nomina ON (
              nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_d004_proceso_tipo_nomina.fk_nmb001_num_tipo_nomina AND
              nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d004_proceso_tipo_nomina.fk_nmb003_num_tipo_proceso
            )
            WHERE
              pk_num_proceso_periodo='$idProcesoNomina'

        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metObligacionListar($procesoNomina)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d001_obligacion
            INNER JOIN nm_d006_nomina_obligacion ON nm_d006_nomina_obligacion.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago
            WHERE
              cp_d001_obligacion.ind_tipo_procedencia='NM' AND
              nm_d006_nomina_obligacion.fk_nmc002_num_proceso_periodo=$procesoNomina 
            ORDER BY pk_num_obligacion
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metMostrarSelect($codMaestro, $codDetalle)
    {
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='$codMaestro' AND
              a006.cod_detalle='$codDetalle'
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetch();
    }

    public function metListarAporteRetenciones($prq=0)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              nm_b005_interfaz_cxp
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle=nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_obligacion
            WHERE
              num_flag_prq='$prq'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metObtenerAporteRetencionesDet($idInterfaz)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              nm_c004_interfas_cxp_det
            WHERE
              fk_nmb005_num_interfaz_cxp='$idInterfaz'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metObligacionesGeneradas($pk_num_proceso_periodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              pk_num_obligacion
            FROM
              cp_d001_obligacion
            WHERE
              fk_nmc002_num_proceso_periodo='$pk_num_proceso_periodo'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metConceptosCalcularIngresos($idProcesoNomina, $arrayEmpleadosCuentas, $proceso = false)
    {
        if ($proceso=='BVC' || $proceso=='RBV' ) {
            $proceso='nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,';
        }
        $array = join(',', array_fill(0, count($arrayEmpleadosCuentas), '?'));
        $sql = "
            SELECT
              SUM(round(nm_d005_tipo_nomina_empleado_concepto.num_monto,2)) AS num_monto,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
              a006_miscelaneo_detalle.cod_detalle,
              IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
              IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
              nm_b003_tipo_proceso.cod_proceso,
              nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina,
              nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
              )
              LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
              LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
              LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoNomina' AND 
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado IN ($array)AND 
              ( a006_miscelaneo_detalle.cod_detalle='I' OR
                a006_miscelaneo_detalle.cod_detalle='T' OR
                a006_miscelaneo_detalle.cod_detalle='AS' OR
                a006_miscelaneo_detalle.cod_detalle='B')
            GROUP BY $proceso pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
            ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
        ";
        $resultado = $this->_db->prepare($sql);
        $resultado->execute($arrayEmpleadosCuentas);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
    }

    public function metAportesRetenciones($periodo,$proceso, $tipo)
    {
        $periodo = str_getcsv($periodo, '-');
        if($tipo=='A'){
            $obligacionListar = $this->_db->query("
                SELECT
                  round(nm_d005_tipo_nomina_empleado_concepto.num_monto,2) AS num_monto,
                  pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
                  pr_b002_partida_presupuestaria.ind_denominacion,
                  a006_miscelaneo_detalle.cod_detalle,
                  IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
                  IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
                  IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
                  IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20,
                  nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
                FROM
                  nm_d005_tipo_nomina_empleado_concepto
                  INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
                  INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
                  INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
                  INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
                  INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
                  INNER JOIN nm_d001_concepto_perfil_detalle ON (
                    nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                    nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                    nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
                  )
                  INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
                  LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
                  LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
                  LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
                  LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
                WHERE
                  nm_c001_tipo_nomina_periodo.fec_anio='$periodo[0]' AND
                  nm_c001_tipo_nomina_periodo.fec_mes='$periodo[1]' AND
                  nm_b003_tipo_proceso.cod_proceso='$proceso' AND 
              ( a006_miscelaneo_detalle.cod_detalle='I' OR
                a006_miscelaneo_detalle.cod_detalle='T' OR
                a006_miscelaneo_detalle.cod_detalle='AS' OR
                a006_miscelaneo_detalle.cod_detalle='B')

                ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
            ");
//            var_dump($obligacionListar);
            $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
            return $obligacionListar->fetchAll();
        }elseif($tipo=='R'){
            $obligacionListar = $this->_db->query("
                SELECT
                  round(nm_d005_tipo_nomina_empleado_concepto.num_monto,2) AS num_monto,
                  NULL AS pk_num_partida_presupuestaria,
                  a006_miscelaneo_detalle.cod_detalle,
                  NULL AS debe,
                  NULL AS debe20,
                  IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
                  IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20,
                  nm_b002_concepto.ind_abrebiatura,
                  nm_b002_concepto.pk_num_concepto,
                  nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
                FROM
                  nm_d005_tipo_nomina_empleado_concepto
                  INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
                  INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
                  INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
                  INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
                  INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
                  INNER JOIN nm_d001_concepto_perfil_detalle ON (
                    nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                    nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                    nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
                  )
                  LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
                  LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
                WHERE
                  nm_c001_tipo_nomina_periodo.fec_anio='$periodo[0]' AND
                  nm_c001_tipo_nomina_periodo.fec_mes='$periodo[1]' AND
                  nm_b003_tipo_proceso.cod_proceso='$proceso' AND 
              ( a006_miscelaneo_detalle.cod_detalle='D' OR
                a006_miscelaneo_detalle.cod_detalle='R')
                ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
            ");
#            var_dump($obligacionListar);
            $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
            return $obligacionListar->fetchAll();
        }elseif($tipo=='AR'){
            $aporrte = $this->_db->query("
                SELECT
                  SUM(round(nm_d005_tipo_nomina_empleado_concepto.num_monto,2)) AS num_monto,
                  pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
                  a006_miscelaneo_detalle.cod_detalle,
                  IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
                  IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
                  IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
                  IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20
                FROM
                  nm_d005_tipo_nomina_empleado_concepto
                  INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
                  INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
                  INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
                  INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
                  INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
                  INNER JOIN nm_d001_concepto_perfil_detalle ON (
                    nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                    nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto  AND
                    nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
                  )
                  LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
                  LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
                  LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
                  LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
                  LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
                WHERE
                  nm_c001_tipo_nomina_periodo.fec_anio='$periodo[0]' AND
                  nm_c001_tipo_nomina_periodo.fec_mes='$periodo[1]' AND
                  nm_b003_tipo_proceso.cod_proceso='$proceso'
                GROUP BY pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
                ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
            ");
            $aporrte->setFetchMode(PDO::FETCH_ASSOC);
            $arregloAporte=$aporrte->fetchAll();
            $retenciones = $this->_db->query("
                SELECT
                  SUM(round(nm_d005_tipo_nomina_empleado_concepto.num_monto,2)) AS num_monto,
                  NULL AS pk_num_partida_presupuestaria,
                  a006_miscelaneo_detalle.cod_detalle,
                  NULL AS debe,
                  NULL AS debe20,
                  IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
                  IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20
                FROM
                  nm_d005_tipo_nomina_empleado_concepto
                  INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
                  INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
                  INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
                  INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
                  INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
                  INNER JOIN nm_d001_concepto_perfil_detalle ON (
                    nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                    nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto  AND
                    nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
                  )
                  LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
                  LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
                WHERE
                  nm_c001_tipo_nomina_periodo.fec_anio='$periodo[0]' AND
                  nm_c001_tipo_nomina_periodo.fec_mes='$periodo[1]' AND
                  nm_b003_tipo_proceso.cod_proceso='$proceso' 
                GROUP BY haber.cod_cuenta, nm_b002_concepto.pk_num_concepto
                ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
            ");
            $retenciones->setFetchMode(PDO::FETCH_ASSOC);
            $arregloRetenciones=$retenciones->fetchAll();
            return array_merge($arregloAporte,$arregloRetenciones);
        }

    }

    public function metBuscarPartidaCuentaAportes($idObligacion,$idPartida,$idCuenta,$idCuenta20)
    {
        if($idPartida!=null){
            $partida="fk_prb002_num_partida_presupuestaria='$idPartida' AND";
        }else{
            $partida="fk_prb002_num_partida_presupuestaria IS NULL AND";
        }
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d013_obligacion_cuentas
            WHERE
              fk_cpd001_num_obligacion='$idObligacion' AND
              $partida
              fk_cbb004_num_cuenta='$idCuenta' AND
              fk_cbb004_num_cuenta_pub20='$idCuenta20'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();

    }

    public function metConceptosCalcularDeducciones($idProcesoNomina, $arrayEmpleadosCuentas, $proceso = false)
    {
        if ($proceso=='BVC' || $proceso=='RBV' ) {
            $proceso='nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,';
        }
        $array = join(',', array_fill(0, count($arrayEmpleadosCuentas), '?'));
        $sql = "
            SELECT
              SUM(round(nm_d005_tipo_nomina_empleado_concepto.num_monto,2)) AS num_monto,
              a006_miscelaneo_detalle.cod_detalle,
              IF( haber.pk_num_cuenta,haber.pk_num_cuenta, null ) AS haber,
              IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta, null) AS haber20,
              pk_num_concepto,
              nm_b003_tipo_proceso.cod_proceso,
              nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina,
              nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
              )
              LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
              LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoNomina' AND
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado IN ($array) AND 
              (a006_miscelaneo_detalle.cod_detalle = 'D' OR 
              a006_miscelaneo_detalle.cod_detalle = 'R')
            GROUP BY $proceso haber.cod_cuenta, nm_b002_concepto.pk_num_concepto
            ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
        ";

        $resultado = $this->_db->prepare($sql);
        $resultado->execute($arrayEmpleadosCuentas);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
    }

    public function metBuscarObligacionesGeneradas($periodo, $procesoNomina)
    {
        $periodo = str_getcsv($periodo, '-');
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d001_obligacion
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago
            WHERE
              cp_d001_obligacion.ind_tipo_procedencia='NM' AND
              cp_d001_obligacion.fec_anio='$periodo[0]' AND
              cp_d001_obligacion.ind_nro_factura  = '$procesoNomina'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metBuscarPartidaCuenta($idObligacion,$idPartida,$idCuenta,$idCuenta20)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d013_obligacion_cuentas
            WHERE
              fk_cpd001_num_obligacion='$idObligacion' AND
              fk_prb002_num_partida_presupuestaria='$idPartida' AND
              fk_cbb004_num_cuenta='$idCuenta' AND
              fk_cbb004_num_cuenta_pub20='$idCuenta20'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();

    }

    public function metBuscarImpuestosObligacion($idObligacion,$idConcepto,$idCuenta,$idCuenta20)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d012_obligacion_impuesto
            WHERE
              fk_cpd001_num_obligacion='$idObligacion' AND
              fk_nmb002_num_concepto='$idConcepto' AND
              fk_cbb004_num_cuenta='$idCuenta' AND
              fk_cbb004_num_cuenta_pub20='$idCuenta20'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();

    }

    public function metVerificarPresupuesto($idObligacion)
    {
        $this->_db->beginTransaction();
        $registroConcepto = $this->_db->prepare("
                      UPDATE
                        cp_d001_obligacion
                      SET
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                          num_flag_presupuesto=:num_flag_presupuesto,
                          num_flag_verificado=:num_flag_verificado
                      WHERE
                        pk_num_obligacion='$idObligacion'
            ");
        $registroConcepto->execute(array(
            'num_flag_presupuesto' => 1,
            'num_flag_verificado' => 1
        ));
        $error = $registroConcepto->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }

    }

    public function metTransferirObligacion($idObligacion)
    {
        $this->_db->beginTransaction();
        $registroConcepto = $this->_db->prepare("
                      UPDATE
                        cp_d001_obligacion
                      SET
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                          ind_estado=:ind_estado
                      WHERE
                        pk_num_obligacion='$idObligacion'
            ");
        $registroConcepto->execute(array(
            'ind_estado' => 'PR'
        ));
        $error = $registroConcepto->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }

    }

    ### Nuevo
    public function metEmpleados($pk_num_proceso_periodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
              rh_b001_empleado.fk_a003_num_persona,
              rh_b001_empleado.pk_num_empleado
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
              )
              LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
              LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
              LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$pk_num_proceso_periodo'
            GROUP BY nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metCuentaEmpleado($arrayEmpleado)
    {
        $array = join(',', array_fill(0, count($arrayEmpleado), '?'));
        $sql = "SELECT
                  rh_c012_empleado_banco.fk_rhb001_num_empleado
                FROM
                  rh_c012_empleado_banco
                WHERE
                    rh_c012_empleado_banco.fk_rhb001_num_empleado IN ($array) AND
                    rh_c012_empleado_banco.num_flag_principal = '1'
                GROUP BY rh_c012_empleado_banco.fk_rhb001_num_empleado
                ";
        $resultado = $this->_db->prepare($sql);
        $resultado->execute($arrayEmpleado);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
    }

    public function metEmpleado($pk_empleado)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              a003_persona.ind_cedula_documento,
              a003_persona.pk_num_persona
            FROM
              rh_b001_empleado
              INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            WHERE
              rh_b001_empleado.pk_num_empleado = '$pk_empleado'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metBuscarObligacionCodigo($tipo,$idPersona,$documento,$cual=false)
    {

        if($cual==1){
            $obligacionListar = $this->_db->query("
            SELECT
              COUNT(*) AS nro
            FROM
              cp_d001_obligacion
            INNER JOIN nm_d006_nomina_obligacion ON nm_d006_nomina_obligacion.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
            WHERE
            cp_d001_obligacion.fk_a003_num_persona_proveedor = $idPersona AND 
            cp_d001_obligacion.fk_cpb002_num_tipo_documento LIKE '$documento' 
            ORDER BY pk_num_obligacion DESC
        ");
        } else {
            $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d001_obligacion
            INNER JOIN nm_d006_nomina_obligacion ON nm_d006_nomina_obligacion.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
            WHERE
            cp_d001_obligacion.fk_a003_num_persona_proveedor = $idPersona AND 
            cp_d001_obligacion.fk_cpb002_num_tipo_documento LIKE '$documento'
            ORDER BY pk_num_obligacion DESC
        ");
        }
//        var_dump($obligacionListar);
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metModificarObligacion($idObligacion, $form)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro = $this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a003_num_persona_proveedor_a_pagar=:fk_a003_num_persona_proveedor_a_pagar,
                fk_a023_num_centro_de_costo=:fk_a023_num_centro_de_costo,
                fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                fk_a006_num_miscelaneo_tipo_pago=:fk_a006_num_miscelaneo_tipo_pago,
                num_flag_obligacion_directa=:num_flag_obligacion_directa,
                ind_comentarios=:ind_comentarios,
                ind_comentarios_adicional=:ind_comentarios_adicional,
                fk_cpb014_num_cuenta=:fk_cpb014_num_cuenta,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                num_monto_obligacion=:num_monto_obligacion,
                num_monto_impuesto_otros=:num_monto_impuesto_otros,
                num_monto_no_afecto=:num_monto_no_afecto,
                num_monto_afecto=:num_monto_afecto,
                num_flag_compromiso=:num_flag_compromiso
            WHERE
              pk_num_obligacion='$idObligacion'
        ");
        $NuevoRegistro->execute(array(
            'fk_a003_num_persona_proveedor' => $form['fk_a003_num_persona_proveedor'],
            'fk_a003_num_persona_proveedor_a_pagar' => $form['fk_a003_num_persona_proveedor_a_pagar'],
            'fk_a023_num_centro_de_costo' => $form['fk_a023_num_centro_de_costo'],
            'fk_cpb002_num_tipo_documento' => $form['fk_cpb002_num_tipo_documento'],
            'fk_cpb017_num_tipo_servicio' => $form['fk_cpb017_num_tipo_servicio'],
            'fk_a006_num_miscelaneo_tipo_pago' => $form['fk_a006_num_miscelaneo_tipo_pago'],
            'num_flag_obligacion_directa' => $form['num_flag_obligacion_directa'],
            'ind_comentarios' => $form['ind_comentarios'],
            'ind_comentarios_adicional' => $form['ind_comentarios_adicional'],
            'fk_cpb014_num_cuenta' => $form['fk_cpb014_num_cuenta'],
            'num_monto_obligacion' => $form['num_monto_obligacion'],
            'num_monto_impuesto_otros' => $form['num_monto_impuesto_otros'],
            'num_monto_no_afecto' => $form['num_monto_no_afecto'],
            'num_monto_afecto' => $form['num_monto_afecto'],
            'num_flag_compromiso' => $form['num_flag_compromiso']
        ));

        #PARTIDAS partidaCuenta
        unset($form['partidaCuenta']['ind_secuencia']);
        $partidaCuenta = $form['partidaCuenta'];
        $secuencia = 1;
        $this->_db->query("DELETE FROM cp_d013_obligacion_cuentas WHERE fk_cpd001_num_obligacion='$idObligacion'");
        if(isset($partidaCuenta)){
            foreach ($partidaCuenta AS $ids) {
//                $ids = $partidaCuenta[$i];

                $registrarCuentas = $this->_db->prepare("
                INSERT INTO
                    cp_d013_obligacion_cuentas
                SET
                    fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                    fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                    fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                    fec_ultima_modificacion=NOW(),
                    ind_descripcion=:ind_descripcion,
                    num_flag_no_afecto=:num_flag_no_afecto,
                    num_monto=:num_monto,
                    num_secuencia=:num_secuencia
            ");
                if ($ids['ind_descripcion'] == '') {
                    $ids['ind_descripcion'] = $form['ind_comentarios'];
                }
                if (!isset($ids['num_flag_no_afecto'])) {
                    $ids['num_flag_no_afecto'] = 0;
                }
                if ($ids['fk_cbb004_num_cuenta'] == '') {
                    $ids['fk_cbb004_num_cuenta'] = null;
                }
                if ($ids['fk_cbb004_num_cuenta_pub20'] == '') {
                    $ids['fk_cbb004_num_cuenta_pub20'] = null;
                }
                if ($ids['fk_prb002_num_partida_presupuestaria'] == '') {
                    $ids['fk_prb002_num_partida_presupuestaria'] = null;
                }
                $registrarCuentas->execute(array(
                    'fk_a003_num_persona_proveedor' => $ids['fk_a003_num_persona_proveedor'],
                    'fk_a023_num_centro_costo' => $ids['fk_a023_num_centro_costo'],
                    'fk_cbb004_num_cuenta' => $ids['fk_cbb004_num_cuenta'],
                    'fk_cbb004_num_cuenta_pub20' => $ids['fk_cbb004_num_cuenta_pub20'],
                    'fk_prb002_num_partida_presupuestaria' => $ids['fk_prb002_num_partida_presupuestaria'],
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'ind_descripcion' => $ids['ind_descripcion'],
                    'num_flag_no_afecto' => $ids['num_flag_no_afecto'],
                    'num_monto' => $ids['num_monto'],
                    'num_secuencia' => $secuencia
                ));
                $secuencia++;
            }
        }

        #Impuestos impuestosRetenciones
        unset($form['impuestosRetenciones']['ind_secuencia']);
        if (!isset($form['impuestosRetenciones'])) {
            $impuestoRetenciones = false;
        } else {
            $impuestoRetenciones = $form['impuestosRetenciones'];
        }

        $secuencia = 1;

        $this->_db->query("DELETE FROM cp_d012_obligacion_impuesto WHERE fk_cpd001_num_obligacion='$idObligacion'");

        if ($impuestoRetenciones) {
            foreach ($impuestoRetenciones AS $key=>$val) {
                $i = $impuestoRetenciones[$key];
                $registrarImpuestos = $this->_db->prepare("
                INSERT INTO
                    cp_d012_obligacion_impuesto
                SET
                    fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                    fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                    fk_nmb002_num_concepto=:fk_nmb002_num_concepto,
                    fk_cpb015_num_impuesto=:fk_cpb015_num_impuesto,
                    fec_ultima_modificacion=NOW(),
                    ind_secuencia=:ind_secuencia,
                    num_monto_impuesto=:num_monto_impuesto,
                    num_monto_afecto=:num_monto_afecto
            ");
                $i['fk_cpb015_num_impuesto'] = null;
                if (!isset($i['fk_nmb002_num_concepto'])) {
                    $i['fk_nmb002_num_concepto'] = null;
                }

                if ($i['fk_cbb004_num_cuenta'] == '') {
                    $i['fk_cbb004_num_cuenta'] = null;
                }
                if ($i['fk_cbb004_num_cuenta_pub20'] == '') {
                    $i['fk_cbb004_num_cuenta_pub20'] = null;
                }

                $registrarImpuestos->execute(array(
                    'fk_a003_num_persona_proveedor' => $form['fk_a003_num_persona_proveedor'],
                    'fk_cbb004_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                    'fk_cbb004_num_cuenta_pub20' => $i['fk_cbb004_num_cuenta_pub20'],
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_nmb002_num_concepto' => $i['fk_nmb002_num_concepto'],
                    'fk_cpb015_num_impuesto' => $i['fk_cpb015_num_impuesto'],
                    'ind_secuencia' => $secuencia,
                    'num_monto_impuesto' => $i['num_monto_impuesto'],
                    'num_monto_afecto' => $i['num_monto_afecto']
                ));
                $secuencia++;
            }
        }


        if (!isset($form['documentos'])) {
            $documentos = false;
        } else {
            $documentos = $form['documentos'];
        }

        $registrarDocumentos = $this->_db->prepare("
            INSERT INTO
                cp_d002_documento_obligacion
            SET
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
              fk_lgb019_num_orden=:fk_lgb019_num_orden,
              fec_ultima_modificacion=NOW()
        ");
        if ($documentos) {
            $this->_db->query("DELETE FROM cp_d002_documento_obligacion WHERE fk_cpd001_num_obligacion='$idObligacion'");
            foreach ($documentos['ind_secuencia'] AS $i) {
                $ids = $documentos[$i];
                $registrarDocumentos->execute(array(
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_lgb019_num_orden' => $ids['fk_lgb019_num_orden'],
                ));
            }
        }

        $fallaTansaccion = $NuevoRegistro->errorInfo();
        if(isset($registrarCuentas)){
            $fallaTansaccion2 = $registrarCuentas->errorInfo();
        } else {
            $fallaTansaccion2 = NULL;
        }
        if ($impuestoRetenciones) {
            $fallaTansaccion3 = $registrarImpuestos->errorInfo();
        } else {
            $fallaTansaccion3 = array(1 => null, 2 => null);
        }
        if ($documentos) {
            $fallaTansaccion4 = $registrarDocumentos->errorInfo();
        } else {
            $fallaTansaccion4 = array(1 => null, 2 => null);
        }

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            var_dump($fallaTansaccion);
            return $fallaTansaccion;
        } elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])) {
            $this->_db->rollBack();
            var_dump($fallaTansaccion2);
            return $fallaTansaccion2;
        } elseif ((!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2]))) {
            $this->_db->rollBack();
            var_dump($fallaTansaccion3);
            return $fallaTansaccion3;
        } elseif ((!empty($fallaTansaccion4[1]) && !empty($fallaTansaccion4[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion4;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }
    }

    public function metBuscarDatosObligacion($idObligacion)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d001_obligacion
            WHERE
              cp_d001_obligacion.pk_num_obligacion='$idObligacion'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metBuscarDatosObligacionCuentas($idObligacion)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d013_obligacion_cuentas
            WHERE
              cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion='$idObligacion'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metBuscarDatosObligacionImpuestos($idObligacion)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d012_obligacion_impuesto
            WHERE
              cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion='$idObligacion'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metNominaObligacion($idObligacion, $idNomina,$idTipoNomina)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro = $this->_db->prepare("
            INSERT INTO
                nm_d006_nomina_obligacion
            SET
                fk_nmc002_num_proceso_periodo=:fk_nmc002_num_proceso_periodo,
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina
        ");
        $NuevoRegistro->execute(array(
            'fk_nmc002_num_proceso_periodo' => $idNomina,
            'fk_cpd001_num_obligacion' => $idObligacion,
            'fk_nmb001_num_tipo_nomina' => $idTipoNomina
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();
        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }
    }

    public function metProveedores()
    {
        $obligacionListar = $this->_db->query("
            SELECT
              cxp.pk_num_interfaz_cxp,
              cxp.fk_000_tipo_servicio,
              cxp.fk_a006_num_miscelaneo_det_tipo_pago,
              cxp.ind_num_cuenta,
              documento.pk_num_tipo_documento,
              cxp.ind_comentario,
              '0' AS num_flag_compromiso,
              
              persona.pk_num_persona,
              CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS proveedor,
              documento.cod_tipo_documento,
              (SELECT
              cp_d001_obligacion.pk_num_obligacion
            FROM
              cp_d001_obligacion
            WHERE
            cp_d001_obligacion.fk_a003_num_persona_proveedor = persona.pk_num_persona AND 
            cp_d001_obligacion.fk_cpb002_num_tipo_documento = documento.pk_num_tipo_documento AND 
            cp_d001_obligacion.ind_estado = 'NM'
            ORDER BY pk_num_obligacion DESC LIMIT 0,1) AS obligacionConglomerar
            FROM
              nm_b005_interfaz_cxp AS cxp 
              INNER JOIN a003_persona AS persona ON cxp.fk_a003_num_persona_proveedor = persona.pk_num_persona
              INNER JOIN cp_b002_tipo_documento AS documento ON cxp.fk_cpb002_num_tipo_documento = documento.pk_num_tipo_documento
            WHERE
              1
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metProveedorPredeterminado($idPersona,$idDocumento,$idServicio,$idTipoPago,$cuenta,$comentario)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              '0' AS pk_num_interfaz_cxp,
              (SELECT
              cp_b017_tipo_servicio.pk_num_tipo_servico
            FROM
              cp_b017_tipo_servicio
            WHERE
              cp_b017_tipo_servicio.cod_tipo_servicio='$idServicio')
               AS fk_000_tipo_servicio,
              '$idTipoPago' AS fk_a006_num_miscelaneo_det_tipo_pago,
              (SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='TDPLG' AND
              a006.cod_detalle='$idTipoPago') AS fk_a006_num_miscelaneo_det_tipo_pago,
              '$cuenta' AS ind_num_cuenta,
              '$idDocumento' AS pk_num_tipo_documento,
              '$comentario' AS ind_comentario,
              '1' AS num_flag_compromiso,
              
              persona.pk_num_persona,
              CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS proveedor,
              (SELECT cod_tipo_documento FROM cp_b002_tipo_documento WHERE pk_num_tipo_documento='$idDocumento') AS cod_tipo_documento,
              (SELECT
              cp_d001_obligacion.pk_num_obligacion
            FROM
              cp_d001_obligacion
            INNER JOIN nm_d006_nomina_obligacion ON nm_d006_nomina_obligacion.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
            WHERE
            cp_d001_obligacion.fk_a003_num_persona_proveedor = $idPersona AND 
            cp_d001_obligacion.fk_cpb002_num_tipo_documento LIKE '$idDocumento' AND 
            cp_d001_obligacion.ind_estado = 'NM'
            ORDER BY pk_num_obligacion DESC LIMIT 0,1) AS obligacionConglomerar
            FROM
               a003_persona AS persona
            WHERE
              persona.pk_num_persona = '$idPersona'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metBuscarObligacionPersona($idPersona)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d001_obligacion
            WHERE
            cp_d001_obligacion.fk_a003_num_persona_proveedor='$idPersona' AND 
            cp_d001_obligacion.ind_estado = 'NM'
            ORDER BY pk_num_obligacion DESC LIMIT 0,1
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metBuscarConceptosPorTipoDocumento($idProcesoPeriodo,$listaTipos)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              SUM(ROUND(nm_d005_tipo_nomina_empleado_concepto.num_monto, 2)) AS num_monto,
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
              pr_b002_partida_presupuestaria.ind_denominacion,
              nm_b002_concepto.ind_impresion,
              a006_miscelaneo_detalle.cod_detalle,
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
              nm_b005_interfaz_cxp.pk_num_interfaz_cxp,
              nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor,
              nm_b005_interfaz_cxp.fk_000_tipo_servicio,
              nm_b005_interfaz_cxp.ind_num_cuenta,
              nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_pago,
              nm_b005_interfaz_cxp.fk_cpb002_num_tipo_documento,
              nm_b005_interfaz_cxp.ind_comentario,
              IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
              IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
              IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
              IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
              )
              LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
              LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
              LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
              LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
              LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
              LEFT JOIN nm_b005_interfaz_cxp ON nm_b005_interfaz_cxp.pk_num_interfaz_cxp = nm_b002_concepto.fk_nmb005_num_interfaz_cxp
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              a006_miscelaneo_detalle.cod_detalle IN ($listaTipos)
            GROUP BY pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria, nm_b002_concepto.cod_concepto 
            ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metBuscarObligacionCodigo2($codigo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *
            FROM
              cp_d001_obligacion
            WHERE
            cp_d001_obligacion.ind_nro_control='$codigo'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }
}

