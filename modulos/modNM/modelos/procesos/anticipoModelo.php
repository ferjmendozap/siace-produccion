<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
class anticipoModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarAnticipos($idEmpleado,$idAnticipo = false,$ultimoAcumulado=false,$listado=false)
    {
        if($ultimoAcumulado AND $idAnticipo == false){
            $anio=date('Y');
            $mes=date('m');
            $sql = "SELECT 
            concat_ws(
                '-', '$anio', 
                '$mes'
            ) AS ind_periodo,
            (
                (
                    (
                        SELECT 
                            IF(
                                sum(
                                    nm_e002_prestaciones_sociales_calculo.num_monto_trimestral
                                ) + SUM(
        
        
                                    nm_e002_prestaciones_sociales_calculo.num_monto_anual
                                ), 
                                sum(
                                    nm_e002_prestaciones_sociales_calculo.num_monto_trimestral
                                ) + SUM(
                                    nm_e002_prestaciones_sociales_calculo.num_monto_anual
                                ), 
                                0
                            ) AS muntoPrestaciones 
                        FROM 
                            rh_b001_empleado 
                            INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado 
                        WHERE 
                            concat_ws(
                                '-', nm_e002_prestaciones_sociales_calculo.fec_anio, 
                                nm_e002_prestaciones_sociales_calculo.fec_mes
                            ) <= ind_periodo 
                            AND nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado = '$idEmpleado'
                            AND nm_e002_prestaciones_sociales_calculo.num_estatus = 1 
                            AND nm_e002_prestaciones_sociales_calculo.num_monto_acumulado = 0
                    ) + (
                        SELECT 
                            IF(
                                sum(
                                    nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_trimestral
                                )+ SUM(
                                    nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_anual
                                ), 
                                sum(
                                    nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_trimestral
                                )+ SUM(
                                    nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_anual
                                ), 
                                0
                            ) AS muntoPrestaciones1 
                        FROM 
                            rh_b001_empleado 
                            INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado 
                        WHERE 
                            concat_ws(
                                '-', nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio, 
                                nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes
                            ) <= ind_periodo 
                            AND nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado = '$idEmpleado'
        
                            AND nm_e001_prestaciones_sociales_calculo_retroactivo.num_estatus = 1
                    )
                ) - (
                    SELECT 
                        IF(
                            sum(anticipo.num_anticipo), 
                            sum(anticipo.num_anticipo), 
                            0
                        ) AS muntoPrestaciones1 
                    FROM 
                        rh_b001_empleado 
                        INNER JOIN nm_e003_prestaciones_sociales_anticipo AS anticipo ON rh_b001_empleado.pk_num_empleado = anticipo.fk_rhb001_num_empleado 
                    WHERE 
                        concat_ws(
                            '-', anticipo.fec_anio, anticipo.fec_mes
                        ) <= ind_periodo 
                        AND anticipo.fk_rhb001_num_empleado = '$idEmpleado'
                        AND anticipo.num_estatus = 1
                )
            ) AS num_monto_acumulado";
        }else{
            $empleado="nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado='$idEmpleado'";
            if($ultimoAcumulado){
                $ultimoAcumulado="ORDER BY
              nm_e003_prestaciones_sociales_anticipo.pk_num_prestaciones_sociales_anticipo DESC
            LIMIT 1";
                $operador="<=";
            }else{
                $operador="<";
            }

            if($idAnticipo){
                $idAnticipo=" AND nm_e003_prestaciones_sociales_anticipo.pk_num_prestaciones_sociales_anticipo='$idAnticipo'";
            }

            if($listado){
                $operador="<=";
                $empleado='nm_e003_prestaciones_sociales_anticipo.num_estatus = 0';
            }

            $sql = "
            SELECT
                concat_ws(
                    '-', nm_e003_prestaciones_sociales_anticipo.fec_anio,
                    nm_e003_prestaciones_sociales_anticipo.fec_mes
                ) AS ind_periodo,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                a006_miscelaneo_detalle.cod_detalle,
                nm_e003_prestaciones_sociales_anticipo.num_anticipo,
                nm_e003_prestaciones_sociales_anticipo.ind_porcentaje,
                nm_e003_prestaciones_sociales_anticipo.fec_anticipo,
                nm_e003_prestaciones_sociales_anticipo.num_estatus,
                nm_e003_prestaciones_sociales_anticipo.pk_num_prestaciones_sociales_anticipo,
                nm_e003_prestaciones_sociales_anticipo.fk_a006_num_miscelaneo_detalle_justificativo,
                a003_persona.ind_cedula_documento,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                (
                    (
                        (
                            SELECT
                                IF(
                                    sum(
                                        nm_e002_prestaciones_sociales_calculo.num_monto_trimestral
                                    ) + SUM(
                                        nm_e002_prestaciones_sociales_calculo.num_monto_anual
                                    ),
                                    sum(
                                        nm_e002_prestaciones_sociales_calculo.num_monto_trimestral
                                    ) + SUM(
                                        nm_e002_prestaciones_sociales_calculo.num_monto_anual
                                    ),
                                    0
                                ) AS muntoPrestaciones
                            FROM
                                rh_b001_empleado
                                INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                            WHERE
                                concat_ws(
                                    '-', nm_e002_prestaciones_sociales_calculo.fec_anio,
                                    nm_e002_prestaciones_sociales_calculo.fec_mes
                                ) <= ind_periodo
                                AND nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado = nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado
                                AND nm_e002_prestaciones_sociales_calculo.num_estatus = 1
                                AND nm_e002_prestaciones_sociales_calculo.num_monto_acumulado=0
                        ) + (
                            SELECT
                                IF(
                                    sum(
                                        nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_trimestral
                                    )+ SUM(
                                        nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_anual
                                    ),
                                    sum(
                                        nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_trimestral
                                    )+ SUM(
                                        nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_anual
                                    ),
                                    0
                                ) AS muntoPrestaciones1
                            FROM
                                rh_b001_empleado
                                INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                            WHERE
                                concat_ws(
                                    '-', nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio,
                                    nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes
                                ) <= ind_periodo
                                AND nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado = nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado
                                AND nm_e001_prestaciones_sociales_calculo_retroactivo.num_estatus = 1
                        )
                    ) - (
                        SELECT
                            IF(
                                sum(anticipo.num_anticipo),
                                sum(anticipo.num_anticipo),
                                0
                            ) AS muntoPrestaciones1
                        FROM
                            rh_b001_empleado
                            INNER JOIN nm_e003_prestaciones_sociales_anticipo AS anticipo ON rh_b001_empleado.pk_num_empleado = anticipo.fk_rhb001_num_empleado
                        WHERE
                            concat_ws(
                                '-', anticipo.fec_anio, anticipo.fec_mes
                            ) $operador ind_periodo
                            AND anticipo.fk_rhb001_num_empleado = nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado
                            AND anticipo.num_estatus = 1
                    )
                ) AS num_monto_acumulado
            FROM
                a003_persona
                INNER JOIN rh_b001_empleado ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN nm_e003_prestaciones_sociales_anticipo ON rh_b001_empleado.pk_num_empleado = nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado
                INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_e003_prestaciones_sociales_anticipo.fk_a006_num_miscelaneo_detalle_justificativo
	        WHERE
             $empleado  $idAnticipo
            $ultimoAcumulado
            ";
        }
        $anticipo = $this->_db->query($sql);
        $anticipo->setFetchMode(PDO::FETCH_ASSOC);
        if($ultimoAcumulado){
            return $anticipo->fetch();
        }else{
            return $anticipo->fetchAll();
        }
    }

    public function metValidarAnticipos($idEmpleado)
    {

        $anticipo = $this->_db->query("
            SELECT
                ADDDATE(
                  nm_e003_prestaciones_sociales_anticipo.fec_anticipo,
                  INTERVAL '1' YEAR
                ) AS fec_anticipo,
                nm_e003_prestaciones_sociales_anticipo.num_estatus
            FROM
                rh_b001_empleado
                INNER JOIN nm_e003_prestaciones_sociales_anticipo ON rh_b001_empleado.pk_num_empleado = nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado
                INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_e003_prestaciones_sociales_anticipo.fk_a006_num_miscelaneo_detalle_justificativo
	        WHERE
              nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado='$idEmpleado'
            ORDER BY
              nm_e003_prestaciones_sociales_anticipo.pk_num_prestaciones_sociales_anticipo DESC
            LIMIT 1
        ");
        return $anticipo->fetch();
    }

    public function metCrearAnticipo($idEmpleado, $idJustificativo, $periodo, $montoAnticipo, $porcentaje, $fecAnticipo)
    {
        $periodo = str_getcsv($periodo,'-');
        $this->_db->beginTransaction();
        $registrarAnticipo = $this->_db->prepare("
                  INSERT INTO
                    nm_e003_prestaciones_sociales_anticipo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_estatus=0,num_anticipo=:num_anticipo, fec_anio=:fec_anio, fec_mes=:fec_mes,
                    fec_anticipo=:fec_anticipo, fk_a006_num_miscelaneo_detalle_justificativo=:fk_a006_num_miscelaneo_detalle_justificativo,
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado,ind_porcentaje=:ind_porcentaje
        ");
        $registrarAnticipo->execute(array(
            'num_anticipo' => $montoAnticipo,
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1],
            'fec_anticipo' => $fecAnticipo,
            'fk_a006_num_miscelaneo_detalle_justificativo' => $idJustificativo,
            'fk_rhb001_num_empleado' => $idEmpleado,
            'ind_porcentaje' => $porcentaje
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $registrarAnticipo->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarAnticipo($idEmpleado, $idJustificativo, $periodo, $montoAnticipo, $porcentaje, $fecAnticipo,$idAnticipo)
    {
        $periodo = str_getcsv($periodo,'-');
        $this->_db->beginTransaction();
        $registrarAnticipo = $this->_db->prepare("
                  UPDATE
                    nm_e003_prestaciones_sociales_anticipo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_estatus=0,num_anticipo=:num_anticipo, fec_anio=:fec_anio, fec_mes=:fec_mes,
                    fec_anticipo=:fec_anticipo, fk_a006_num_miscelaneo_detalle_justificativo=:fk_a006_num_miscelaneo_detalle_justificativo,
                    ind_porcentaje=:ind_porcentaje
                  WHERE
                    pk_num_prestaciones_sociales_anticipo='$idAnticipo' AND
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado
        ");
        $registrarAnticipo->execute(array(
            'num_anticipo' => $montoAnticipo,
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1],
            'fec_anticipo' => $fecAnticipo,
            'fk_a006_num_miscelaneo_detalle_justificativo' => $idJustificativo,
            'fk_rhb001_num_empleado' => $idEmpleado,
            'ind_porcentaje' => $porcentaje
        ));

        $error = $registrarAnticipo->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idAnticipo;
        }
    }

    public function metAprobarAnularAnticipo($idAnticipo,$anular = false)
    {
        if(!$anular){
            $anular=1;
        }
        $this->_db->beginTransaction();
        $registrarAnticipo = $this->_db->prepare("
                  UPDATE
                    nm_e003_prestaciones_sociales_anticipo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_estatus=:num_estatus
                  WHERE
                    pk_num_prestaciones_sociales_anticipo='$idAnticipo'
        ");
        $registrarAnticipo->execute(array(
            'num_estatus' => $anular
        ));

        $error = $registrarAnticipo->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idAnticipo;
        }
    }

}
