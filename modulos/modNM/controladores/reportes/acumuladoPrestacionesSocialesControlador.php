<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class acumuladoPrestacionesSocialesControlador extends Controlador
{
    private $atAnticipo;

    public function __construct()
    {
        parent::__construct();
        $this->atAnticipo = $this->metCargarModelo('acumuladoPrestacionesSociales', 'reportes');
    }

    public function metIndex()
    {
        $this->atVista->metRenderizar('seleccion');
    }

    public function metListadoPrestaciones()
    {
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $prestacionesSociales = array();
        $acumuladoAnterior = 0;
        $acumulado = 0;

        $anticipo = 0;
        $conversionAcumulado = 0;
        $aportes = 0;
        $conversion = 0;
        $conversion2 = 0;
        $acumuladoPorPagado = 0;
        $anticipo2 = 0;
        $anticipoAcumulado = 0;

        foreach ($this->atAnticipo->metPrestacionesSociales($idEmpleado) AS $i) {

            if($i['periodo']<=Session::metObtener('periodoManualNM')){
                $prestacionesSociales[$i['periodo']] = array(
                    'periodo' => $i['periodo'],
                    'num_sueldo_base' => $i['num_sueldo_base'],
                    'num_total_asignaciones' => $i['num_total_asignaciones'],
                    'num_sueldo_normal' => $i['num_sueldo_normal'],
                    'num_alicuota_vacacional' => $i['num_alicuota_vacacional'],
                    'num_alicuota_fin_anio' => $i['num_alicuota_fin_anio'],
                    'num_bono_especial' => $i['num_bono_especial'],
                    'num_bono_fiscal' => $i['num_bono_fiscal'],
                    'num_dias_trimestre' => $i['num_dias_trimestre'],
                    'num_dias_anual' => $i['num_dias_anual'],
                    'num_remuneracion_mensual' => $i['num_remuneracion_mensual'],
                    'num_remuneracion_diaria' => $i['num_remuneracion_diaria'],
                    'num_monto_trimestral' => $i['num_monto_trimestral'],
                    'num_monto_anual' => $i['num_monto_anual'],
                    'num_estatus' => $i['num_estatus'],
                    'num_aportes' => '',
                    'num_anticipo' => '',
                    'num_monto_acumulado' => $i['num_monto_acumulado']
                );

                $acumuladoAnterior = $i['num_monto_acumulado'];
            }else{
                $prestacionesSocialesRetroactivo = $this->atAnticipo->metPrestacionesSocialesRetroactivo($idEmpleado,$i['periodo']);
                $prestacionesSocialesAnticipo = $this->atAnticipo->metPrestacionesSocialesAnticipo($idEmpleado,$i['periodo']);
                $prestacionesSocialesIntereses = $this->atAnticipo->metPrestacionesSocialesIntereses($idEmpleado,$i['periodo']);
                $acumuladoPrestaciones = $this->atAnticipo->metPrestacionesSocialesAcumulado($idEmpleado,$i['periodo']);
                $acumuladoPrestacionesRetroactivo = $this->atAnticipo->metPrestacionesSocialesAcumuladoRetroactivo($idEmpleado,$i['periodo']);

                $acumuladoPrestacionesAnticipo = $this->atAnticipo->metPrestacionesSocialesAcumuladoAnticipo($idEmpleado,$i['periodo']);

                $acumuladoPrestacionesIntereses = $this->atAnticipo->metPrestacionesSocialesAcumuladoIntereses($idEmpleado,$i['periodo']);
                $acumuladoPrestacionesPorPagar = $this->atAnticipo->metPrestacionesSocialesAcumulado($idEmpleado,$i['periodo'],'0');
                $acumuladoPrestacionesRetroactivoPorPagar = $this->atAnticipo->metPrestacionesSocialesAcumuladoRetroactivo($idEmpleado,$i['periodo'],'0');
                $acumuladoPrestacionesInteresesPorPagar = $this->atAnticipo->metPrestacionesSocialesAcumuladoIntereses($idEmpleado,$i['periodo'],'0');

                $anticipoAcumulado = $anticipoAcumulado + $prestacionesSocialesAnticipo['num_anticipo'];
                //var_dump($prestacionesSocialesAnticipo['num_anticipo'] );


                $aportes = $acumuladoAnterior +
                    $acumuladoPrestaciones['num_monto_trimestral'] +
                    $acumuladoPrestaciones['num_monto_anual'] +
                    $acumuladoPrestacionesRetroactivo['num_monto_trimestral'] +
                    $acumuladoPrestacionesRetroactivo['num_monto_anual'] +
                    $acumuladoPrestacionesIntereses['num_monto_interes'];



                $acumulado =
                    $acumuladoAnterior +
                    $acumuladoPrestaciones['num_monto_trimestral'] +
                    $acumuladoPrestaciones['num_monto_anual'] +
                    $acumuladoPrestacionesRetroactivo['num_monto_trimestral'] +
                    $acumuladoPrestacionesRetroactivo['num_monto_anual'] +
                    $acumuladoPrestacionesIntereses['num_monto_interes'] -
                    $anticipoAcumulado
                ;



                if($i['periodo'] >= '2008-01' && $i['fec_ingreso']<'2008-01-01'  ){
                    $conversion += $i['num_monto_trimestral'] + $i['num_monto_anual'];
                    $conversion2 += $prestacionesSocialesAnticipo['num_anticipo'];

                    if($prestacionesSocialesAnticipo['periodo'] >= '2008-01'){
                        $anticipo2 += $prestacionesSocialesAnticipo['num_anticipo'];
                        $anticipoAcumulado -= $conversion2;
                        $anticipoAcumulado = $anticipoAcumulado/100;
                        $anticipoAcumulado += $conversion2;
                    }


                    $acumulado -= $conversion;
                    $acumulado = $acumulado/100;
                    $acumulado += $conversion - $anticipo2;

                    $aportes -= $conversion;
                    $aportes = $aportes/100;
                    $aportes += $conversion;
                }




                $acumuladoPorPagar =
                    $acumuladoPrestacionesPorPagar['num_monto_trimestral'] +
                    $acumuladoPrestacionesPorPagar['num_monto_anual'] +
                    $acumuladoPrestacionesRetroactivoPorPagar['num_monto_trimestral'] +
                    $acumuladoPrestacionesRetroactivoPorPagar['num_monto_anual']  +
                    $acumuladoPrestacionesInteresesPorPagar['num_monto_interes']
                ;


                if($i['num_dias_trimestre']==5 && $i['periodo']>=Session::metObtener('periodoManualNM') && $i['num_monto_trimestral']!='0.000000') {
                    $trimestre = 5;
                }elseif($i['num_dias_trimestre']==5 && $i['periodo']>=Session::metObtener('periodoManualNM')){
                    $trimestre = 0;
                }elseif($i['num_dias_trimestre']==10 && $i['periodo']>=Session::metObtener('periodoManualNM') && $i['num_monto_trimestral']!='0.000000'){
                    $trimestre = 10;
                }elseif($i['num_dias_trimestre']==10 && $i['periodo']>=Session::metObtener('periodoManualNM')){
                    $trimestre = 0;
                }else{
                    $trimestre = $i['num_dias_trimestre'];
                }

                $prestacionesSociales[$i['periodo']] = array(
                    'periodo' => $i['periodo'],
                    'num_sueldo_base' => number_format($i['num_sueldo_base']+$prestacionesSocialesRetroactivo['num_sueldo_base'],2,',','.'),
                    'num_total_asignaciones' => number_format($i['num_total_asignaciones']+$prestacionesSocialesRetroactivo['num_total_asignaciones'],2,',','.'),
                    'num_sueldo_normal' => number_format($i['num_sueldo_normal']+$prestacionesSocialesRetroactivo['num_sueldo_normal'],2,',','.'),
                    'num_alicuota_vacacional' => number_format($i['num_alicuota_vacacional']+$prestacionesSocialesRetroactivo['num_alicuota_vacacional'],2,',','.'),
                    'num_alicuota_fin_anio' => number_format($i['num_alicuota_fin_anio']+$prestacionesSocialesRetroactivo['num_alicuota_fin_anio'],2,',','.'),
                    'num_bono_especial' => number_format($i['num_bono_especial']+$prestacionesSocialesRetroactivo['num_bono_especial'],2,',','.'),
                    'num_bono_fiscal' => number_format($i['num_bono_fiscal']+$prestacionesSocialesRetroactivo['num_bono_fiscal'],2,',','.'),
                    'num_dias_trimestre' => $trimestre,
                    'num_dias_anual' => number_format($i['num_dias_anual'],0),
                    'num_remuneracion_mensual' => number_format($i['num_remuneracion_mensual']+$prestacionesSocialesRetroactivo['num_remuneracion_mensual'],2,',','.'),
                    'num_remuneracion_diaria' => number_format($i['num_remuneracion_diaria']+$prestacionesSocialesRetroactivo['num_remuneracion_diaria'],2,',','.'),
                    'num_monto_trimestral' => number_format($i['num_monto_trimestral']+$prestacionesSocialesRetroactivo['num_monto_trimestral'],2,',','.'),
                    'num_monto_anual' => number_format($i['num_monto_anual']+$prestacionesSocialesRetroactivo['num_monto_anual'],2,',','.'),
                    'num_estatus' => number_format($i['num_estatus'],2,',','.'),
                    'num_aportes' => number_format($aportes,2,',','.'),
                    'num_anticipo' => number_format($prestacionesSocialesAnticipo['num_anticipo'],2,',','.'),
                    'num_monto_interes' => number_format($prestacionesSocialesIntereses['num_monto_interes'],2,',','.'),
                    'num_monto_acumulado' => number_format( $acumulado,2,',','.'),
                );
            }
        }


        $this->atVista->assign('lista', $prestacionesSociales);
        $this->atVista->assign('pagado', number_format( $anticipoAcumulado,2,',','.'));
        $this->atVista->assign('porPagar', number_format( $acumulado,2,',','.'));
        $this->atVista->assign('idEmpleado', $idEmpleado);
        $this->atVista->metRenderizar('listado');
    }

    public function metImprimirExcel($idEmpleado)
    {

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=reporte.xls");
        header("Cache-Control: max-age=0");

        $prestacionesSociales = array();
        $acumuladoAnterior = 0;
        $acumulado = 0;
        $acumuladoPorPagar = 0;
        $anticipo = 0;
        $aportes = 0;
        $anticipoAcumulado = 0;
        foreach ($this->atAnticipo->metPrestacionesSociales($idEmpleado) AS $i) {
            if($i['periodo']<=Session::metObtener('periodoManualNM')){
                $prestacionesSociales[$i['periodo']] = array(
                    'periodo' => $i['periodo'],
                    'num_sueldo_base' => $i['num_sueldo_base'],
                    'num_total_asignaciones' => $i['num_total_asignaciones'],
                    'num_sueldo_normal' => $i['num_sueldo_normal'],
                    'num_alicuota_vacacional' => $i['num_alicuota_vacacional'],
                    'num_alicuota_fin_anio' => $i['num_alicuota_fin_anio'],
                    'num_bono_especial' => $i['num_bono_especial'],
                    'num_bono_fiscal' => $i['num_bono_fiscal'],
                    'num_dias_trimestre' => $i['num_dias_trimestre'],
                    'num_dias_anual' => $i['num_dias_anual'],
                    'num_remuneracion_mensual' => $i['num_remuneracion_mensual'],
                    'num_remuneracion_diaria' => $i['num_remuneracion_diaria'],
                    'num_monto_trimestral' => $i['num_monto_trimestral'],
                    'num_monto_anual' => $i['num_monto_anual'],
                    'num_estatus' => $i['num_estatus'],
                    'num_aportes' => '',
                    'num_anticipo' => '',
                    'num_monto_acumulado' => $i['num_monto_acumulado']
                );
                $acumuladoAnterior = $i['num_monto_acumulado'];
            }else{
                $prestacionesSocialesRetroactivo = $this->atAnticipo->metPrestacionesSocialesRetroactivo($idEmpleado,$i['periodo']);
                $prestacionesSocialesAnticipo = $this->atAnticipo->metPrestacionesSocialesAnticipo($idEmpleado,$i['periodo']);
                $prestacionesSocialesIntereses = $this->atAnticipo->metPrestacionesSocialesIntereses($idEmpleado,$i['periodo']);
                $acumuladoPrestaciones = $this->atAnticipo->metPrestacionesSocialesAcumulado($idEmpleado,$i['periodo']);
                $acumuladoPrestacionesRetroactivo = $this->atAnticipo->metPrestacionesSocialesAcumuladoRetroactivo($idEmpleado,$i['periodo']);

                $acumuladoPrestacionesAnticipo = $this->atAnticipo->metPrestacionesSocialesAcumuladoAnticipo($idEmpleado,$i['periodo']);

                $acumuladoPrestacionesIntereses = $this->atAnticipo->metPrestacionesSocialesAcumuladoIntereses($idEmpleado,$i['periodo']);
                $acumuladoPrestacionesPorPagar = $this->atAnticipo->metPrestacionesSocialesAcumulado($idEmpleado,$i['periodo'],'0');
                $acumuladoPrestacionesRetroactivoPorPagar = $this->atAnticipo->metPrestacionesSocialesAcumuladoRetroactivo($idEmpleado,$i['periodo'],'0');
                $acumuladoPrestacionesInteresesPorPagar = $this->atAnticipo->metPrestacionesSocialesAcumuladoIntereses($idEmpleado,$i['periodo'],'0');

                $anticipoAcumulado = $anticipoAcumulado + $prestacionesSocialesAnticipo['num_anticipo'];


                $aportes = $acumuladoAnterior +
                    $acumuladoPrestaciones['num_monto_trimestral'] +
                    $acumuladoPrestaciones['num_monto_anual'] +
                    $acumuladoPrestacionesRetroactivo['num_monto_trimestral'] +
                    $acumuladoPrestacionesRetroactivo['num_monto_anual'] +
                    $acumuladoPrestacionesIntereses['num_monto_interes'];

                $acumulado =
                    $acumuladoAnterior +
                    $acumuladoPrestaciones['num_monto_trimestral'] +
                    $acumuladoPrestaciones['num_monto_anual'] +
                    $acumuladoPrestacionesRetroactivo['num_monto_trimestral'] +
                    $acumuladoPrestacionesRetroactivo['num_monto_anual'] +
                    $acumuladoPrestacionesIntereses['num_monto_interes'] -
                    $anticipoAcumulado
                ;


                $acumuladoPorPagar =
                    $acumuladoPrestacionesPorPagar['num_monto_trimestral'] +
                    $acumuladoPrestacionesPorPagar['num_monto_anual'] +
                    $acumuladoPrestacionesRetroactivoPorPagar['num_monto_trimestral'] +
                    $acumuladoPrestacionesRetroactivoPorPagar['num_monto_anual']  +
                    $acumuladoPrestacionesInteresesPorPagar['num_monto_interes']
                ;


                if($i['num_dias_trimestre']==5 && $i['periodo']>=Session::metObtener('periodoManualNM') && $i['num_monto_trimestral']!='0.000000') {
                    $trimestre = 5;
                }elseif($i['num_dias_trimestre']==5 && $i['periodo']>=Session::metObtener('periodoManualNM')){
                    $trimestre = 0;
                }elseif($i['num_dias_trimestre']==10 && $i['periodo']>=Session::metObtener('periodoManualNM') && $i['num_monto_trimestral']!='0.000000'){
                    $trimestre = 10;
                }elseif($i['num_dias_trimestre']==10 && $i['periodo']>=Session::metObtener('periodoManualNM')){
                    $trimestre = 0;
                }else{
                    $trimestre = $i['num_dias_trimestre'];
                }

                $prestacionesSociales[$i['periodo']] = array(
                    'periodo' => $i['periodo'],
                    'num_sueldo_base' => number_format($i['num_sueldo_base']+$prestacionesSocialesRetroactivo['num_sueldo_base'],2,',','.'),
                    'num_total_asignaciones' => number_format($i['num_total_asignaciones']+$prestacionesSocialesRetroactivo['num_total_asignaciones'],2,',','.'),
                    'num_sueldo_normal' => number_format($i['num_sueldo_normal']+$prestacionesSocialesRetroactivo['num_sueldo_normal'],2,',','.'),
                    'num_alicuota_vacacional' => number_format($i['num_alicuota_vacacional']+$prestacionesSocialesRetroactivo['num_alicuota_vacacional'],2,',','.'),
                    'num_alicuota_fin_anio' => number_format($i['num_alicuota_fin_anio']+$prestacionesSocialesRetroactivo['num_alicuota_fin_anio'],2,',','.'),
                    'num_bono_especial' => number_format($i['num_bono_especial']+$prestacionesSocialesRetroactivo['num_bono_especial'],2,',','.'),
                    'num_bono_fiscal' => number_format($i['num_bono_fiscal']+$prestacionesSocialesRetroactivo['num_bono_fiscal'],2,',','.'),
                    'num_dias_trimestre' => $trimestre,
                    'num_dias_anual' => number_format($i['num_dias_anual'],0),
                    'num_remuneracion_mensual' => number_format($i['num_remuneracion_mensual']+$prestacionesSocialesRetroactivo['num_remuneracion_mensual'],2,',','.'),
                    'num_remuneracion_diaria' => number_format($i['num_remuneracion_diaria']+$prestacionesSocialesRetroactivo['num_remuneracion_diaria'],2,',','.'),
                    'num_monto_trimestral' => number_format($i['num_monto_trimestral']+$prestacionesSocialesRetroactivo['num_monto_trimestral'],2,',','.'),
                    'num_monto_anual' => number_format($i['num_monto_anual']+$prestacionesSocialesRetroactivo['num_monto_anual'],2,',','.'),
                    'num_estatus' => number_format($i['num_estatus'],2,',','.'),
                    'num_aportes' => number_format($aportes,2,'.',','),
                    'num_anticipo' => number_format($prestacionesSocialesAnticipo['num_anticipo'],2,',','.'),
                    'num_monto_interes' => number_format($prestacionesSocialesIntereses['num_monto_interes'],2,',','.'),
                    'num_monto_acumulado' => number_format( $acumulado,2,',','.'),
                );
            }
        }

        $this->atVista->assign('lista', $prestacionesSociales);
        $this->atVista->assign('pagado', number_format( $anticipoAcumulado,2,',','.'));
        $this->atVista->assign('porPagar', number_format( $acumulado,2,',','.'));
        $this->atVista->metRenderizar('formato_excel');
    }

}