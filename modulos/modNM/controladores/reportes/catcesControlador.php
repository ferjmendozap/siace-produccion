<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class catcesControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->metObtenerLibreria('headerNominaConsolidada', 'modNM');
        $this->atFPDF = new pdfNominaConsolidada('L', 'mm', 'a2');
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina(1));
        $this->atVista->metRenderizar('index');
    }


    public function metPdfNominaConsolidadaCatces($idPeriodoProceso)
    {
        $nomina=$this->atEjecucionProcesos->metObtenerProceso($idPeriodoProceso);
        $empleado = $this->atConsultasComunes->metEmpleadosNomina($idPeriodoProceso);
        for ($i = 0; $i < count($empleado); $i++) {
            $empleados[$i] = array(
                'idEmpleado' => $empleado[$i]['pk_num_empleado'],
                'ci' => $empleado[$i]['ind_cedula_documento'],
                'nombre' => $empleado[$i]['ind_nombre1'] . ' ' . $empleado[$i]['ind_nombre2'] . ' ' . $empleado[$i]['ind_apellido1'] . ' ' . $empleado[$i]['ind_apellido2'],
            );
            $empleados[$i]['deducciones'] = $this->atConsultasComunes->metEmpleadosDeducciones($idPeriodoProceso, $empleado[$i]['pk_num_empleado']);
            $codDeducciones = $empleados[$i]['deducciones'];
            $totalDeduccionesEmpleado = 0;
            foreach ($codDeducciones AS $codDeduccion) {
                $empleados[$i]['codDeducciones'][] = $codDeduccion['cod_concepto'];
                $totalDeduccionesEmpleado = $totalDeduccionesEmpleado + $codDeduccion['num_monto'];
            }
            $empleados[$i]['totalDeduccionesEmpleado'] = $totalDeduccionesEmpleado;

        }

        $tituloDeducciones = $this->atConsultasComunes->metTituloDeduccionesCatces($idPeriodoProceso);


        #### PDF ####
        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));
        $this->atFPDF->setTipoHeader('nomina');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 5);
        $this->atFPDF->SetAutoPageBreak(10);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);


####### DEDUCCIONES ########
        if ($tituloDeducciones) {
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(190, 5, ("DEDUCCIONES"), 0, 1, 'C');
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 10);
            $this->atFPDF->Cell(9, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
            $this->atFPDF->Cell(20, 6, ('CEDULA'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(90, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            foreach ($tituloDeducciones AS $titulos) {
                $this->atFPDF->Cell(28, 6, utf8_decode($titulos['ind_reporte']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(28, 6, ('T.DEDUC.'), 1, 1, 'R', 1);
            $ln = 0;
            $N = 1;
            foreach ($empleados AS $empleado) {
                if ($empleado['deducciones']) {
                    $ln++;
                    $y = $this->atFPDF->GetY();
                    if ($y > 170) {
                        $this->atFPDF->AddPage();
                        $this->atFPDF->SetFont('Arial', 'B', 9);
                        $this->atFPDF->Cell(190, 5, ("DEDUCCIONES"), 0, 1, 'C');
                        $this->atFPDF->SetFillColor(200, 200, 200);
                        $this->atFPDF->SetFont('Arial', 'B', 9);
                        $this->atFPDF->Cell(9, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                        $this->atFPDF->Cell(20, 6, ('CEDULA'), 1, 0, 'R', 1);
                        $this->atFPDF->Cell(90, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                        $this->atFPDF->SetFont('Arial', 'B', 8);

                        foreach ($tituloDeducciones AS $titulos) {
                            $this->atFPDF->Cell(28, 6, utf8_decode($titulos['ind_reporte']), 1, 0, 'R', 1);
                        }
                        $this->atFPDF->Cell(28, 6, ('T.DEDUC.'), 1, 1, 'R', 1);
                    } else {
                        $this->atFPDF->SetY($y);
                    }
                    if ($ln % 2 == 0) {
                        $this->atFPDF->SetFillColor(240, 240, 240);
                    } else {
                        $this->atFPDF->SetFillColor(255, 255, 255);
                    }
                    $this->atFPDF->SetFont('Arial', '', 9);
                    $this->atFPDF->Cell(9, 6, $N++, 1, 0, 'C', 1);
                    $this->atFPDF->Cell(20, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Cell(90, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
                    $this->atFPDF->SetFont('Arial', '', 9);

                    $deducciones = $empleado['deducciones'];
                    $codDeducciones = $empleado['codDeducciones'];
                    $total = 0;
                    foreach ($tituloDeducciones AS $titulos) {
                        if (in_array($titulos['cod_concepto'], $codDeducciones)) {
                            foreach ($deducciones AS $deduccion) {
                                if ($titulos['cod_concepto'] == $deduccion['cod_concepto']) {
                                    $total = $total + $deduccion['num_monto'];
                                    $this->atFPDF->Cell(28, 6, number_format($deduccion['num_monto'], 2, ',', '.'), 1, 0, 'R', 1);
                                }
                            }
                        } else {
                            $this->atFPDF->Cell(28, 6, number_format(0, 2, ',', '.'), 1, 0, 'R', 1);
                        }
                    }

                    $this->atFPDF->SetFont('Arial', 'B', 7);
                    $this->atFPDF->Cell(28, 6, number_format($total, 2, ',', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Ln();
                }
            }
            ### Totales ###
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->Cell(9, 6, '', 1, 0, 'C', 1);
            $this->atFPDF->Cell(20, 6, ' ', 1, 0, 'R', 1);
            $this->atFPDF->Cell(90, 6, 'Totales: ', 1, 0, 'R', 1);
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $totalDeducciones = 0;
            foreach ($tituloDeducciones AS $titulos) {
                $totalDeducciones = $totalDeducciones + $titulos['TOTAL'];
                $this->atFPDF->Cell(28, 6, number_format($titulos['TOTAL'],2,',','.'), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(28, 6, number_format($totalDeducciones, 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Ln(6);
            ### Leyenda ###
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
            $this->atFPDF->Cell(95, 5, 'DESCRIPCION', 1, 1, 'C', 1);
            foreach ($tituloDeducciones AS $titulos) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 7);
                $this->atFPDF->Cell(30, 5, utf8_decode($titulos['ind_reporte']), 1, 0, 'L', 1);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 7);
                $this->atFPDF->Cell(50, 5, utf8_decode($titulos['ind_impresion']), 1, 1, 'L', 1);
            }
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(30, 5, utf8_decode('T.DEDUC'), 1, 0, 'L', 1);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->Cell(150, 5, utf8_decode('TOTAL DEDUCCIÓN'), 1, 1, 'L', 1);
    
        }
####### FIN ########

        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($nomina['fk_rhb001_num_empleado_procesa']);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');





        $yf = $this->atFPDF->GetY();
        if($yf>160) $yf = 160;
        $this->atFPDF->Rect(10, $y + 6, 70, 0.1, "DF");

        $this->atFPDF->SetXY(200, $yf + 10);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(200, 3, '______________________________________________                                    ___________________________________________', 0, 1, 'L');

        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(200, $yf + 15);
        $this->atFPDF->Cell(100, 3, ('Elaborado Por:                                                                                                         Conformado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(100, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(200, $yf + 18);
        $this->atFPDF->MultiCell(100, 3, utf8_decode($preparadoPor['nombre']), 0, 'L');//nombre de quien elabora
        $this->atFPDF->SetXY(300, $yf + 18);
        $this->atFPDF->Cell(100, 3, utf8_decode($revisadoPor['nombre']), 0, 1, 'L');//nombre de quien revisa
        $this->atFPDF->SetXY(200, $yf + 20);
        $this->atFPDF->MultiCell(100, 3, utf8_decode($preparadoPor['ind_descripcion_cargo']), 0, 'L');//cargo de quien elabora
        $this->atFPDF->SetXY(300, $yf + 20);
        $this->atFPDF->Cell(100, 3, utf8_decode($revisadoPor['ind_descripcion_cargo']), 0, 1, 'L');//Cargo de quien revisa

        /*$this->atFPDF->Rect(10, $y + 20, 70, 0.1, "DF");
        $this->atFPDF->SetXY(200, $yf + 35);
        $this->atFPDF->Cell(100, 3, ' ____________________________________________', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(200, $yf + 40);
        $this->atFPDF->Cell(100, 3, ('Aprobado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(100, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(200, $yf + 43);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['nombre']), 0, 0, 'L');//nombre de quien conforma
        $this->atFPDF->SetXY(200, $yf + 45);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['ind_descripcion_cargo']), 0, 0, 'L');//cargo de quien conforma*/

        $this->atFPDF->Output();
    }
}
