<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class tasaInteresesControlador extends Controlador
{
    private $atTasaIntereses;

    public function __construct()
    {
        parent::__construct();
        $this->atTasaIntereses = $this->metCargarModelo('tasaIntereses', 'maestros');
    }

    public function metIndex($lista = false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $valido = $this->metObtenerInt('valido');
        $idTasa = $this->metObtenerInt('idTasa');
        if ($valido == 1) {
            $this->metValidarToken();
            $Excceccion = array('num_estatus');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'txt');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excceccion);
            if ($alphaNum != null && $ind == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($alphaNum, $ind);
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }
            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = 0;
            }
            if ($idTasa == 0) {
                $id = $this->atTasaIntereses->metCrearTasaInteres($validacion);
                $validacion['status'] = 'nuevo';
            } else {
                $validacion['pk_num_tasa_interes'] = $idTasa;
                $id = $this->atTasaIntereses->metModificarTasaInteres($validacion);
                $validacion['status'] = 'modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idTasa'] = $id;

            echo json_encode($validacion);
            exit;
        }

        if ($idTasa != 0) {
            $this->atVista->assign('formDB', $this->atTasaIntereses->metMostrarTasaInteres($idTasa));
            $this->atVista->assign('idTasa', $idTasa);
        }

        $this->atVista->metRenderizar('CrearModificar', 'modales');
    }

    public function metEliminar()
    {
        $idTasa = $this->metObtenerInt('idTasa');
        if ($idTasa != 0) {
            $id = $this->atTasaIntereses->metEliminarTasaInteres($idTasa);
            if (is_array($id)) {
                $valido = array(
                    'status' => 'error',
                    'mensaje' => 'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            } else {
                $valido = array(
                    'status' => 'ok',
                    'idProceso' => $id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol = Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM nm_b007_tasa_interes ";

        if ($busqueda['value']) {
            $sql .= "WHERE
                    ( 
                      periodo LIKE '%$busqueda[value]%' OR 
                      num_gaceta LIKE '%$busqueda[value]%' OR 
                      fec_gaceta LIKE '%$busqueda[value]%'
                    )
                    ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('periodo', 'num_gaceta', 'fec_gaceta', 'num_activa', 'num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_tasa_interes';
        #los valores del listado con flag 'md md-check'
        $flags = array();
        #construyo el listado de botones
        if (in_array('NM-01-03-01-02-M', $rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" idTasa="' . $clavePrimaria . '" title="Editar"
                            descipcion="El Usuario a Modificado una tasa de interes de Nomina" titulo="Modificar Tasa Interes">
                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('NM-01-03-01-03-E', $rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTasa="' . $clavePrimaria . '" boton="si, Eliminar" title="Eliminar"
                            descipcion="El usuario a eliminado una Tasa de Interes" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Tasa de interes!!">
                        <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql, $campos, $clavePrimaria, false, $flags);
    }

}
