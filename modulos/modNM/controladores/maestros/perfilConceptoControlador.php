<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class perfilConceptoControlador extends Controlador
{
    private $atTipoNomina;
    private $atPerfilConcepto;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->atPerfilConcepto = $this->metCargarModelo('perfilConcepto', 'maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('listado');
    }

    public function metPerfilDetalleNominaProceso()
    {
        $this->atVista->assign('tr',$this->metObtenerInt('tr'));
        $this->atVista->metRenderizar('perfilDetalleNominaProceso', 'modales');
    }

    public function metModificarPerfilDetalle()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idNomina = $this->metObtenerInt('idNomina');

        if ($valido == 1) {
            $Excceccion = array('num_estatus', 'ind_formula', 'num_flag_automatico', 'num_flag_bono', 'num_flag_incidencia', 'partida', 'debe', 'debepub20', 'haber', 'haberPub20');
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$Excceccion);
            $ind = $this->metValidarFormArrayDatos('form', 'int',$Excceccion);

            if ($txt != null && $ind == null) {
                $validacion = $txt;
            } elseif ($txt == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($txt, $ind);
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            $id = $this->atPerfilConcepto->metModificarPerfilConcepto($idNomina, $validacion['fk_nmb003_num_tipo_proceso'], $validacion['fk_nmb002_num_concepto'], $validacion['ind_cod_partida'],
                $validacion['ind_cod_cuenta_contable_debe'], $validacion['ind_cod_cuenta_contable_haber'], $validacion['ind_cod_cuenta_contable_debe_pub20'], $validacion['ind_cod_cuenta_contable_haber_pub20']);
            $validacion['status'] = 'modificacion';
            echo json_encode($validacion);
            exit;
        }

        $perfilConcepto = $this->atPerfilConcepto->metMostrarPerfilConcepto($idNomina);
        $proceso = 0;
        for ($i = 0; $i < count($perfilConcepto); $i++) {
            $arrayProceso[$perfilConcepto[$i]['fk_nmb003_num_tipo_proceso']]['id'] = $perfilConcepto[$i]['fk_nmb003_num_tipo_proceso'];
            $arrayProceso[$perfilConcepto[$i]['fk_nmb003_num_tipo_proceso']]['nombre'] = substr($perfilConcepto[$i]['ind_nombre_proceso'],0,40);
            $arrayProceso[$perfilConcepto[$i]['fk_nmb003_num_tipo_proceso']]['array'][$perfilConcepto[$i]['fk_a006_num_tipo_concepto']]['tipo'] = $perfilConcepto[$i]['ind_nombre_detalle'];
            $arrayProceso[$perfilConcepto[$i]['fk_nmb003_num_tipo_proceso']]['array'][$perfilConcepto[$i]['fk_a006_num_tipo_concepto']]['detalle'][] = $perfilConcepto[$i];
        }
        if(!isset($arrayProceso)){
            $arrayProceso=false;
        }
        //exit;
        $this->atVista->assign('idNomina', $idNomina);
        $this->atVista->assign('ver', $this->metObtenerInt('ver'));
        $this->atVista->assign('n', 1);
        $this->atVista->assign('n2', 1);
        $this->atVista->assign('nomina', $this->atTipoNomina->metMostrarTipoNomina($idNomina));
        $this->atVista->assign('perfilConcepto', $arrayProceso);
        $this->atVista->metRenderizar('modificarPerfilConcepto', 'modales');
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM nm_b001_tipo_nomina ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                    ( 
                    cod_tipo_nomina LIKE '%$busqueda[value]%' OR
                    ind_nombre_nomina LIKE '%$busqueda[value]%' OR
                    ind_titulo_boleta LIKE '%$busqueda[value]%'
                    )
            ";
        }
        
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_tipo_nomina','ind_nombre_nomina','ind_titulo_boleta','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_tipo_nomina';
        #construyo el listado de botones
        
        if (in_array('NM-01-03-04-01-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idNomina="'.$clavePrimaria.'" title="Editar"
                        descipcion="El Usuario a Modificado el Perfil de Detalle" titulo="Modificar Perfil Detalle">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('NM-01-03-04-02-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idNomina="'.$clavePrimaria.'"
                        descipcion="El Usuario esta viendo un Perfil de Concepto" titulo="<i class=\'icm icm-calculate2\'></i> Ver Perfil de Concepto">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}
