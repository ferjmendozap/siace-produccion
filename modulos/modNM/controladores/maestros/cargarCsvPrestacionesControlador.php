<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class cargarCsvPrestacionesControlador extends Controlador
{
    private $atCargarCsvPrestaciones;

    public function __construct()
    {
        parent::__construct();
        $this->atCargarCsvPrestaciones = $this->metCargarModelo('cargarCsvPrestaciones', 'maestros');
    }

    public function metIndex()
    {
        $this->atVista->metRenderizar('seleccion');
    }

    public function metCsvEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $this->atVista->assign('idEmpleado', $idEmpleado);
        $this->atVista->metRenderizar('listaCsv', 'modales');
    }

    public function metCsv()
    {
        if($this->metObtenerInt('validoCsv')){
            $form = $this->metValidarFormArrayDatos('form', 'int');
            if (in_array('error', $form)) {
                $form['status'] = 'error';
                echo json_encode($form);
                exit;
            }
            $id = $this->atCargarCsvPrestaciones->metCrearModificar($form);
            if (is_array($id)) {
                foreach ($form as $titulo => $valor) {
                    if (strpos($id[2], $form[$titulo])) {
                        $form[$titulo] = 'error';
                    }
                }
                $form['status'] = 'errorSQL';
                echo json_encode($form);
                exit;
            }
            $form['idPretaciones'] = $id;
            $form['status'] = 'OK';
            echo json_encode($form);
            exit;
        }
        $tipo = $_FILES['csv']['type'];
        $tamanio = $_FILES['csv']['size'];
        $archivotmp = $_FILES['csv']['tmp_name'];
        $lineas = file($archivotmp);
        $i=0;
        $csv = array();
        foreach ($lineas as $linea_num => $linea)
        {
            if($i != 0)
            {
                $linea = str_replace('"', '', $linea);
                $linea = str_replace(';', ',', $linea);
                $csv[] = explode(",",$linea);
            }
            $i++;
        }
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $Empleado = $this->atCargarCsvPrestaciones->metBuscarEmpleadoCsv($idEmpleado);
        $this->atVista->assign('idEmpleado', $Empleado['pk_num_empleado']);
        $this->atVista->assign('idNomina', $Empleado['fk_nmb001_num_tipo_nomina']);
        $this->atVista->assign('csv', $csv);
        $this->atVista->metRenderizar('csv', 'modales');
    }

}
