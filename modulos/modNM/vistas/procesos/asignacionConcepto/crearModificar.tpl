<form action="{$_Parametros.url}modNM/procesos/asignacionConceptoCONTROL/crearModificarMET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idConceptoAsignado}" name="idConceptoAsignado"/>
        <input type="hidden" value="{$idEmpleado}" name="idEmpleado" id="idEmpleado"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" id="fk_nmb002_num_conceptoError">
                    <label for="fk_nmb002_num_concepto"><i class="md md-map"></i> Concepto</label>
                    <select id="fk_nmb002_num_concepto" name="form[int][fk_nmb002_num_concepto]" class="form-control select2-list select2">
                        <option value="">Seleccione el Concepto</option>
                        {foreach item=i from=$conceptos}
                            {if isset($formDB.fk_nmb002_num_concepto)}
                                {if $i.pk_num_concepto==$formDB.fk_nmb002_num_concepto}
                                    <option selected value="{$i.pk_num_concepto}">#{$i.cod_concepto} - {$i.ind_descripcion}</option>
                                {else}
                                    <option value="{$i.pk_num_concepto}">#{$i.cod_concepto} - {$i.ind_descripcion}</option>
                                {/if}
                            {else}
                                <option value="{$i.pk_num_concepto}">#{$i.cod_concepto} - {$i.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group control-width-normal" id="fec_periodo_desdeError">
                    <div class="input-group date" id="fec_periodo_desde">
                        <div class="input-group-content">
                            <input type="text" id="fec_periodo_desde" class="form-control" name="form[txt][fec_periodo_desde]" value="{if isset($formDB.fec_periodo_desde)}{$formDB.fec_periodo_desde}{/if}">
                            <label>Periodo Desde</label>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group control-width-normal" id="fec_periodo_hastaError">
                    <div class="input-group date" id="fec_periodo_hasta">
                        <div class="input-group-content">
                            <input type="text" id="fec_periodo_hasta" class="form-control" name="form[txt][fec_periodo_hasta]" value="{if isset($formDB.fec_periodo_hasta)}{$formDB.fec_periodo_hasta}{/if}">
                            <label>Periodo Hasta</label>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group" id="num_montoError">
                    <label for="num_monto"><i class="fa fa-list-alt"></i> Monto</label>
                    <input type="text" class="form-control" value="{if isset($formDB.num_monto)}{$formDB.num_monto}{/if}" name="form[int][num_monto]" id="num_monto">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="num_cantidadError">
                    <label for="num_cantidad"><i class="fa fa-list-alt"></i> Cantidad</label>
                    <input type="text" class="form-control text-center" value="{if isset($formDB.num_cantidad)}{$formDB.num_cantidad}{/if}" name="form[int][num_cantidad]" id="num_cantidad">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-head card-head-xs style-primary">
                        <header>Procesos</header>
                    </div>
                    <div class="card-body" style="padding: 4px;">
                        <div class="table-responsive">
                            <table class="table no-margin" id="tipoProcesoNomina">
                                <thead>
                                <tr>
                                    <th>Cod</th>
                                    <th>Procesos</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {if isset($formDBDet)}
                                    {foreach item=i from=$formDBDet}
                                        <tr class="idProceso{$i.fk_nmb005_num_tipo_proceso}">
                                            <input type="hidden" value="{$i.fk_nmb005_num_tipo_proceso}" name="form[int][fk_nmb003_num_tipo_proceso][]" class="tipoProcesoInput" proceso="{$i.cod_proceso}" />
                                            <td>{$i.cod_proceso}</td>
                                            <td>{$i.ind_nombre_proceso}</td>
                                            <td>{if isset($ver)} {else} <button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idProceso{$i.fk_nmb005_num_tipo_proceso}"><i class="md md-delete" style="color: #ffffff;"></i></button>{/if}</td>
                                        </tr>
                                    {/foreach}
                                {/if}
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="3" align="center">
                                        {if isset($ver)} {else}
                                            <button type="button"
                                                    class="btn btn-primary ink-reaction btn-raised"
                                                    id="agregarProceso"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    titulo="Agregar Procesos"
                                                    url="{$_Parametros.url}modNM/maestros/tipoProcesoCONTROL/indexMET/MODAL"
                                                    >Agregar Proceso
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_procesoError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    var app = new  AppFunciones();
    $("#formAjax").submit(function(){
        return false;
    });
    $('#modalAncho').css( "width", "35%" );
    $('#fec_periodo_desde').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es' });
    $('#fec_periodo_hasta').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es' });
    $('.select2').select2({ allowClear: true });
    $('#agregarProceso').click(function () {
        $('#formModalLabel2').html($(this).attr('titulo'));
        $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
            $('#ContenidoModal2').html($dato);
        });
    });
    $('#tipoProcesoNomina').on('click', '.borrar', function () {
        var borrar = $(this);
        $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
    });
    $('#accion').click(function(){
        swal({
            title: "¡Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
        $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
            var arrayCheck = ["num_estatus"];
            var arrayMostrarOrden = ['fk_nmb002_num_concepto','fk_nmb002_num_concepto','fec_periodo_desde','fec_periodo_hasta','num_monto','num_cantidad','','num_estatus'];
            if(dato['status']=='error'){
                app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
            }else if(dato['status']=='modificar'){
                app.metActualizarRegistroTabla(dato,dato['idConceptoAsignado'],'idConceptoAsignado',arrayCheck,arrayMostrarOrden,'La aplicacion fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
            }else if(dato['status']=='nuevo'){
                app.metNuevoRegistroTabla(dato,dato['idConceptoAsignado'],'idConceptoAsignado',arrayCheck,arrayMostrarOrden,'La aplicacion fue guardado satisfactoriamente.','cerrarModal','ContenidoModal','conceptoAsignadoEmpleado');
            }
        },'json');
    });

    $('#fk_nmb002_num_concepto').change(function(){
        $.post('{$_Parametros.url}modNM/procesos/asignacionConceptoCONTROL/evaluarFormulaMET',{ idConcepto: $('#fk_nmb002_num_concepto').val(),idEmpleado: $('#idEmpleado').val() },function(dato){
            console.log(dato);
            if(dato['monto'] && dato['cantidad']){
                $('#num_monto').val(dato['monto']);
                $('#num_cantidad').val(dato['cantidad']);
            }
        },'json');
    });

});
</script>