<div class="col-sm-12">
    <section class="style-default-bright">
        <div class="section-header">

        </div>
        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12 contain-lg">
                    <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="100" class="text-center"> # </th>
                                    <th width="100" class="text-center"> TIPO PAGO </th>
                                    <th width="85" class="text-center"> PROVEEDOR </th>
                                    <th class="text-center"> NOMBRE DEL PROVEEDOR </th>
                                    <th width="50" class="text-center"> DOC. </th>
                                    <th width="140" class="text-center"> NRO. REGISTRO </th>
                                    <th width="40" class="text-center"> VERF. </th>
                                    <th width="35" class="text-center"> TRF. </th>
                                    <th width="200" class="text-center"> MONTO OBLIGACION </th>
                                    <th width="10" class="text-center"> ESTADO </th>
                                    <th width="100" class="text-center"> ACCIONES </th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach item=i from=$obligaciones}
                                    <tr id="idObligacion{$i.pk_num_obligacion}">
                                        <th width="100" class="text-center"> {$i.pk_num_obligacion} </th>
                                        <th width="100" class="text-center"> {$i.ind_nombre_detalle} </th>
                                        <th width="85" class="text-center"> {$i.fk_a003_num_persona_proveedor} </th>
                                        <th class="text-center"> {$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2} </th>
                                        <th width="50" class="text-center"> {$i.cod_tipo_documento} </th>
                                        <th width="140" class="text-center"> {$i.ind_nro_factura} </th>
                                        <th width="40" class="text-center"> <i class="{if $i.num_flag_presupuesto==1}md md-check{else}md md-not-interested{/if}"></i> </th>
                                        <th width="35" class="text-center"> <i class="{if $i.ind_estado!='NM'}md md-check{else}md md-not-interested{/if}"></i> </th>
                                        <th width="200" class="text-center"> {number_format($i.num_monto_obligacion,2,',','.')} </th>
                                        <th width="10" class="text-center"> {$i.ind_estado} </th>
                                        <th width="100" class="text-center">
                                            {if in_array('NM-01-01-07-02-V',$_Parametros.perfil) && $i.num_flag_presupuesto==0}
                                                <button class="verificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idObligacion="{$i.pk_num_obligacion}"
                                                        descipcion="" title="Verificar Presupuesto">
                                                    <i class="icm icm-stats2" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            {if in_array('NM-01-01-07-01-T',$_Parametros.perfil) && $i.num_flag_presupuesto==1 && $i.ind_estado=='NM'}
                                                <button class="transferir logsUsuario btn ink-reaction btn-raised btn-xs btn-success"
                                                        idObligacion="{$i.pk_num_obligacion}"
                                                        titulo="Transferir Obligacion"
                                                        mensaje="Esta Seguro que desea Transferir la obligacion??"
                                                        descipcion="el Usuario a transferido una Obligaciones de Nomina a Cuentas Por Pagar">
                                                    <i class="icm icm-checkmark3" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            {if in_array('NM-01-01-07-03-VER',$_Parametros.perfil)}
                                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" id="ver" idObligacion="{$i.pk_num_obligacion}" title="Consultar"
                                                        descipcion="El Usuario esta viendo una Obligacion" titulo="<i class='md md-remove-red-eye'></i> Consultar Obligacion">
                                                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </th>
                                    </tr>
                                {/foreach}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8" class="text-left">
                                        {if in_array('NM-01-01-05-01-01-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info calcular"
                                                    titulo="Calcular Obligaciones"
                                                    mensaje="Esta Seguro que desea Calcular las obligaciones??"
                                                    descipcion="el Usuario a calculado las Obligaciones de Nomina"
                                                    url="{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/calcularObligacionesMET">
                                                Calcular Obligacion &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                        {if in_array('NM-01-01-05-01-01-N',$_Parametros.perfil)}
                                            <!--button class="logsUsuario btn ink-reaction btn-raised btn-info calcular"
                                                    titulo="Calcular Aportes y Retenciones"
                                                    mensaje="Esta Seguro que desea Calcular las obligaciones??"
                                                    descipcion="el Usuario a calculado las Obligaciones de Nomina"
                                                    url="{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/calcularAportesRetencionesMET">
                                                Calcular Aportes y Retenciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button-->
                                        {/if}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable1 tfoot').on( 'click', '.calcular', function () {
            var url=$(this).attr('url');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post(url, { pk_num_proceso_periodo:$('#fk_nmb003_num_tipo_proceso').val(),pk_num_tipo_nomina:$('#fk_nmb001_num_tipo_nomina').val() },function(dato){
                    if (dato['status'] == 'listo') {
                        swal("Obligacion Calculada!", 'La Obligacion fue calculada satisfactoriamente', "success");
                        $.post('{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/listadoObligacionesMET',{ pk_num_proceso_periodo:$('#fk_nmb003_num_tipo_proceso').val(),pk_num_tipo_nomina:$('#fk_nmb001_num_tipo_nomina').val() },function(dato){
                            $('#listadoObligaciones').html(dato);
                        });
                    }
                },'json');
            });
        });
        var $url='{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/';
        $('#datatable1 tbody').on( 'click', '.verificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url+'verificarObligacionMET',{ idObligacion: $(this).attr('idObligacion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        var url='{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/nuevaObligacionMET';
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{ idObligacion: $(this).attr('idObligacion') , estado: $(this).attr('id'), ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.transferir', function () {
            var idObligacion = $(this).attr('idObligacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($url+'transferirObligacionMET', { idObligacion: idObligacion},function(dato){
                    if (dato['status'] == 'ok') {
                        swal("Obligacion Transferida!", 'La Obligacion fue transferida satisfactoriamente', "success");
                        $.post('{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/listadoObligacionesMET',{ pk_num_proceso_periodo:$('#fk_nmb003_num_tipo_proceso').val(),pk_num_tipo_nomina:$('#fk_nmb001_num_tipo_nomina').val() },function(dato){
                            $('#listadoObligaciones').html(dato);
                        });
                    }
                },'json');
            });

        });
    });
</script>
