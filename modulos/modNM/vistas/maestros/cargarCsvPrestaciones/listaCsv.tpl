<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <span class="clearfix"></span>
            <div class="table-responsive no-margin" id="contenidoCsv">
                <table class="table table-striped no-margin" id="conceptoAsignadoEmpleado">
                    <thead>
                        <tr>
                            <th> Año </th>
                            <th> Mes </th>
                            <th> Sueldo Base </th>
                            <th> Total Asignaciones </th>
                            <th> Sueldo Normal </th>
                            <th> Alic. Vacacional </th>
                            <th> Alic. Fin Año </th>
                            <th> Bono Especial </th>
                            <th> Bono Fiscal </th>
                            <th> Dias Trimestre </th>
                            <th> Dias Anual </th>
                            <th> Remuneración Mensual </th>
                            <th> Remuneración Diaria </th>
                            <th> Monto Trimestal </th>
                            <th> Monto Anual </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="15">
                                <form enctype="multipart/form-data" id="formAjax" method="post">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="csv">Adjuntar un CSV</label>
                                            <input type="hidden" name="idEmpleado" value="{$idEmpleado}">
                                            <input type="file" name="csv" id="csv" accept=".csv" >
                                        </div>
                                    </div>
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info pull-right" type="button"
                                            descipcion=""  titulo="" id="nuevo" >
                                        <i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cargar csv
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modNM/maestros/cargarCsvPrestacionesCONTROL/csvMET';
        $('#nuevo').click(function(){
            $('#formAjax').submit();
        });
        $('#formAjax').submit(function (e) {
            var dato = new FormData(this);
            $.ajax({
                url: url,
                type: 'POST',
                data: dato,
                contentType: false,
                processData: false,
                success: function (respuesta) {
                    $('#contenidoCsv').html(respuesta);
                }
            });
            e.preventDefault();
        });
    });
</script>