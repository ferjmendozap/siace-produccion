<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo de Procesos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="tablaInformacionContable">
                        <thead>
                            <tr>
                                <th style="width: 180px;" class="text-center">Tipo Nomina</th>
                                <th style="width: 180px;" class="text-center" valign="middle">Tipo Proceso</th>
                                <th class="text-center" valign="middle">Partida Presupuestaria</th>
                                <th class="text-center" valign="middle">Debe</th>
                                <th class="text-center" valign="middle">Debe Pub20</th>
                                <th class="text-center" valign="middle">Haber</th>
                                <th class="text-center" valign="middle">Haber Pub20</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" class="btn btn-default ink-reaction btn-raised" id="cancelarModal">Cancelar</button>
    <button type="button" class="btn btn-primary ink-reaction btn-raised" id="agregarInformacionContable">Agregar</button>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var tablaNomina = $('#tipoNomina tbody tr');
        var tablaProcesos = $('#tipoProcesoNomina tbody tr');
        var tablaInformacionContable = $('#informacionContable tbody');
        tablaNomina.each(function(){
            var nomina = $(this).find(".tipoNominaInput");
            tablaProcesos.each(function(){
                var proceso = $(this).find(".tipoProcesoInput");
                var trInformacionContable = $(document.getElementById('idNM'+nomina.val()+'idPRO'+proceso.val()+'List'));
                if(trInformacionContable.length){
                    var trInput = trInformacionContable.find(".trInformacion");
                    $(document.getElementById('tablaInformacionContable')).append(
                        '<tr id="idNM'+nomina.val()+'idPRO'+proceso.val()+'">'+
                            '<input type="hidden" proceso="'+proceso.attr('proceso')+'" nomina="'+nomina.attr('nomina')+'" idNomina="'+nomina.val()+'" idProceso="'+proceso.val()+'" partida="'+trInput.attr('partida')+'" debe="'+trInput.attr('debe')+'" debePub20="'+trInput.attr('debePub20')+'" haber="'+trInput.attr('haber')+'" haberPub20="'+trInput.attr('haberPub20')+'" class="trInformacion">'+
                        '<input type="hidden" name="form[txt][partida][' + nomina.val() + proceso.val() + ']" value="' + trInput.attr('partida') + '">' +
                        '<input type="hidden" name="form[txt][debe][' + nomina.val() + proceso.val() + ']" value="' + trInput.attr('debe') + '">' +
                        '<input type="hidden" name="form[txt][debePub20][' + nomina.val() + proceso.val() + ']" value="' + trInput.attr('debePub20') + '">' +
                        '<input type="hidden" name="form[txt][haber][' + nomina.val() + proceso.val() + ']" value="' + trInput.attr('haber') + '">' +
                        '<input type="hidden" name="form[txt][haberPub20][' + nomina.val() + proceso.val() + ']" value="' + trInput.attr('haberPub20') + '">' +
                            '<td>'+nomina.attr('nomina')+'</td>'+
                            '<td>'+proceso.attr('proceso')+'</td>'+
                            '<td><input type="text" size="10"  class="form-control partida" style="height: 20px;margin-bottom: 0px;" value="'+trInput.attr('partida')+'" /></td>'+
                            '<td><input type="text" size="10"  class="form-control debe" style="height: 20px;margin-bottom: 0px;" value="'+trInput.attr('debe')+'" /></td>'+
                            '<td><input type="text" size="10"  class="form-control debePub20" style="height: 20px;margin-bottom: 0px;" value="'+trInput.attr('debePub20')+'" /></td>'+
                            '<td><input type="text" size="10"  class="form-control haber" style="height: 20px;margin-bottom: 0px;" value="'+trInput.attr('haber')+'" /></td>'+
                            '<td><input type="text" size="10"  class="form-control haberPub20" style="height: 20px;margin-bottom: 0px;" value="'+trInput.attr('haberPub20')+'" /></td>'+
                        '</tr>'
                    );
                }else{
                    $(document.getElementById('tablaInformacionContable')).append(
                            '<tr id="idNM'+nomina.val()+'idPRO'+proceso.val()+'">'+
                                '<input type="hidden" proceso="'+proceso.attr('proceso')+'" nomina="'+nomina.attr('nomina')+'" idNomina="'+nomina.val()+'" idProceso="'+proceso.val()+'" class="trInformacion">'+
                                '<td>'+nomina.attr('nomina')+'</td>'+
                                '<td>'+proceso.attr('proceso')+'</td>'+

                                '<td><input type="text" size="10" class="form-control text-center partida accionModal" id="idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}partida" readonly ' +
                                'data-toggle="modal" data-target="#formModal3"'+
                                'data-keyboard="false" data-backdrop="static"'+
                                'titulo="Buscar Partidas"'+
                                'url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/partidasCuentasMET/conceptos/P/idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}partida"></td>' +

                                '<td><input type="text" size="10" class="form-control text-center debe accionModal" id="idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}debe" readonly ' +
                                'data-toggle="modal" data-target="#formModal3"'+
                                'data-keyboard="false" data-backdrop="static"'+
                                'titulo="Buscar Partidas"'+
                                'url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/partidasCuentasMET/conceptos/C/idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}debe/cuenta"></td>' +

                                '<td><input type="text" size="10" class="form-control text-center debePub20 accionModal" id="idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}debePub20" readonly ' +
                                'data-toggle="modal" data-target="#formModal3"'+
                                'data-keyboard="false" data-backdrop="static"'+
                                'titulo="Buscar Partidas"'+
                                'url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/partidasCuentasMET/conceptos/C/idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}debePub20/cuentaPub20"></td>' +

                                '<td><input type="text" size="10" class="form-control text-center haber accionModal" id="idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}haber" readonly ' +
                                'data-toggle="modal" data-target="#formModal3"'+
                                'data-keyboard="false" data-backdrop="static"'+
                                'titulo="Buscar Partidas"'+
                                'url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/partidasCuentasMET/conceptos/C/idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}haber/cuenta"></td>' +

                                '<td><input type="text" size="10" class="form-control text-center haberPub20 accionModal" id="idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}haberPub20" readonly ' +
                                'data-toggle="modal" data-target="#formModal3"'+
                                'data-keyboard="false" data-backdrop="static"'+
                                'titulo="Buscar Partidas"'+
                                'url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/partidasCuentasMET/conceptos/C/idNM'+nomina.val()+'idPRO'+proceso.val()+'{$tr}haberPub20/cuentaPub20"></td>' +
                            '</tr>'
                    );
                }
            });
        });


        $('#agregarInformacionContable').click(function(){
            var registros = $('#tablaInformacionContable tbody tr');
            registros.each(function() {
                var trNominaProceso = $(this).find(".trInformacion");
                var trNomina = trNominaProceso.attr('nomina');
                var trIdNomina = trNominaProceso.attr('idNomina');
                var trProceso = trNominaProceso.attr('proceso');
                var trIdProceso = trNominaProceso.attr('idProceso');

                var partida = $(this).find(".partida").val();
                var debe = $(this).find(".debe").val();
                var debepub20 = $(this).find(".debePub20").val();
                var haber = $(this).find(".haber").val();
                var haberPub20 = $(this).find(".haberPub20").val();
                $(document.getElementById('idNM' + trIdNomina + 'idPRO' + trIdProceso+'List')).remove();

                $(document.getElementById('informacionContable')).append(
                        '<tr id="idNM' + trIdNomina + 'idPRO' + trIdProceso + 'List">' +
                        '<input type="hidden" proceso="' + trProceso + '" nomina="' + trNomina + '" idNomina="' + trIdNomina + '" idProceso="' + trIdProceso + '" partida="' + partida + '" debe="' + debe + '" debepub20="' + debepub20 + '" haber="' + haber + '" haberPub20="' + haberPub20 + '" class="trInformacion">' +
                        '<input type="hidden" name="form[txt][partida][' + trIdNomina + trIdProceso + ']" value="' + partida + '">' +
                        '<input type="hidden" name="form[txt][debe][' + trIdNomina + trIdProceso + ']" value="' + debe + '">' +
                        '<input type="hidden" name="form[txt][debepub20][' + trIdNomina + trIdProceso + ']" value="' + debepub20 + '">' +
                        '<input type="hidden" name="form[txt][haber][' + trIdNomina + trIdProceso + ']" value="' + haber + '">' +
                        '<input type="hidden" name="form[txt][haberPub20][' + trIdNomina + trIdProceso + ']" value="' + haberPub20 + '">' +
                        '<td>' + trNomina + '</td>' +
                        '<td>' + trProceso + '</td>' +
                        '<td><input type="text" size="10"  class="form-control partida" style="height: 20px;margin-bottom: 0px;" readonly value="' + partida + '" /></td>' +
                        '<td><input type="text" size="10"  class="form-control debe" style="height: 20px;margin-bottom: 0px;" readonly value="' + debe + '" /></td>' +
                        '<td><input type="text" size="10"  class="form-control debepub20" style="height: 20px;margin-bottom: 0px;" readonly value="' + debepub20 + '" /></td>' +
                        '<td><input type="text" size="10"  class="form-control haber" style="height: 20px;margin-bottom: 0px;" readonly value="' + haber + '" /></td>' +
                        '<td><input type="text" size="10"  class="form-control haberPub20" style="height: 20px;margin-bottom: 0px;" readonly value="' + haberPub20 + '" /></td>' +
                        '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idNM' + trIdNomina + 'idPRO' + trIdProceso + 'List"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                        '</tr>'
                );

            });
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });

        $('#cancelarModal').click(function(){
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });

        $('#modalAncho3').css("width", "60%");
        $('.accionModal').click(function () {
            $('#formModalLabel3').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function (dato) {
                $('#ContenidoModal3').html(dato);
            });
        });
    });
</script>