<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Perfil de Conceptos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Cod</th>
                            <th>Nomina</th>
                            <th>Titulo Boleta</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modNM/maestros/perfilConceptoCONTROL/modificarPerfilDetalleMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/maestros/perfilConceptoCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_tipo_nomina" },
                    { "data": "ind_nombre_nomina" },
                    { "data": "ind_titulo_boleta" },
                    { "orderable": false,"data": "num_estatus",'width':30 },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idNomina: $(this).attr('idNomina')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idNomina: $(this).attr('idNomina') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

    });
</script>