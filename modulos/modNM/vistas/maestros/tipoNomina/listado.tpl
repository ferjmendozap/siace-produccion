<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo De Nomina - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Cod</th>
                            <th>Nomina</th>
                            <th>Pago Mensual</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('NM-01-03-02-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una Nueva Nomina"  titulo="<i class='icm icm-cog3'></i> Crear Nomina" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nueva Nomina
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modNM/maestros/tipoNominaCONTROL/crearModificarMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/maestros/tipoNominaCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_tipo_nomina" },
                    { "data": "ind_nombre_nomina" },
                    { "orderable": false,"data": "num_flag_pago_mensual",'width':30 },
                    { "orderable": false,"data": "num_estatus",'width':30 },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idNomina:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idNomina: $(this).attr('idNomina')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idNomina=$(this).attr('idNomina');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modNM/maestros/tipoNominaCONTROL/eliminarMET';
                $.post($url, { idNomina: idNomina },function(dato){
                    if(dato['status']=='ok'){
                        app.metEliminarRegistroJson('dataTablaJson','"El Tipo de Nomina fue eliminado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>