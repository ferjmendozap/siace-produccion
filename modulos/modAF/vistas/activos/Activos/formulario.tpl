<form action="{$_Parametros.url}modAF/activos/ActivosCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post" autocomplete="off">
    <input type="hidden" name="pk_num_activo" id="pk_num_activo" value="{$form.pk_num_activo}">
    <input type="hidden" name="pk_num_activo_fijo" id="pk_num_activo_fijo" value="{$form.pk_num_activo_fijo}">

    <div class="modal-body">
        <div id="rootwizard2" class="form-wizard form-wizard-horizontal">
            <div class="form-wizard-nav">
                <div class="progress">
                    <div class="progress-bar progress-bar-primary"></div>
                </div>

                <ul class="nav nav-justified">
                    <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">Informaci&oacute;n General </span></a></li>
                    <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Informaci&oacute;n Adicional</span></a></li>
                    <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Voucher Ingreso</span></a></li>
                </ul>
            </div>

            <hr>

            <div class="tab-content clearfix">
                <div class="tab-pane active" id="step1">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label">
                                    <input type="text" name="num_activo" id="num_activo" value="{$form.pk_num_activo}" class="form-control input-sm" disabled>
                                    <label for="num_activo">N&uacute;mero</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_a006_num_naturalezaError">
                                    <input type="hidden" name="fk_a006_num_naturaleza" id="fk_a006_num_naturaleza" value="{$form.fk_a006_num_naturaleza}">
                                    <input type="text" name="num_naturaleza" id="num_naturaleza" value="{Select::miscelaneo('AFGRUPCAT', $form.fk_a006_num_naturaleza, 3)}" class="form-control input-sm" disabled>
                                    <label for="num_naturaleza">Naturaleza</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_a006_num_origenError">
                                    <input type="hidden" name="fk_a006_num_origen" id="fk_a006_num_origen" value="{$form.fk_a006_num_origen}">
                                    <input type="text" name="num_origen" id="num_origen" value="{Select::miscelaneo('AFORIGENAC', $form.fk_a006_num_origen, 3)}" class="form-control input-sm" disabled>
                                    <label for="num_origen">Origen</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="ind_estadoError">
                                    <input type="hidden" name="ind_estado" id="ind_estado" value="{$form.ind_estado}">
                                    <input type="text" name="estado" id="estado" value="{Select::options('af_estado_activo', $form.ind_estado, 2)}" class="form-control input-sm" disabled>
                                    <label for="estado">Estado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm floating-label" id="ind_descripcionError">
                                    <textarea name="ind_descripcion" id="ind_descripcion" class="form-control input-sm" style="height:50px;" {$disabled.ver}>{$form.ind_descripcion}</textarea>
                                    <label for="ind_descripcion">Descripci&oacute;n</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_a001_num_organismoError">
                                    <select name="fk_a001_num_organismo" id="fk_a001_num_organismo" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a004_dependencia', $('#fk_a004_num_dependencia'), 'fk_a001_num_organismo='+$(this).val(), ['fk_a023_num_centro_costo']);" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',$form.fk_a001_num_organismo)}
                                    </select>
                                    <label for="fk_a001_num_organismo">Organismo</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="cod_codigo_internoError">
                                    <input type="text" name="cod_codigo_interno" id="cod_codigo_interno" value="{$form.cod_codigo_interno}" class="form-control input-sm" maxlength="45" disabled>
                                    <label for="cod_codigo_interno">C&oacute;digo Interno</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="cod_codigo_barraError">
                                    <input type="text" name="cod_codigo_barra" id="cod_codigo_barra" value="{$form.cod_codigo_barra}" class="form-control input-sm" maxlength="45" {$disabled.ver}>
                                    <label for="cod_codigo_barra">C&oacute;digo Barras</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_a004_num_dependenciaError">
                                    <select name="fk_a004_num_dependencia" id="fk_a004_num_dependencia" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a023_centro_costo', $('#fk_a023_num_centro_costo'), 'fk_a004_num_dependencia='+$(this).val());" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia',$form.fk_a004_num_dependencia,0,['fk_a001_num_organismo'=>$form.fk_a001_num_organismo])}
                                    </select>
                                    <label for="fk_a004_num_dependencia">Dependencia</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_afc007_num_situacionError">
                                    <select name="fk_afc007_num_situacion" id="fk_afc007_num_situacion" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('af_c007_situacion','pk_num_situacion','ind_nombre_situacion',$form.fk_afc007_num_situacion)}
                                    </select>
                                    <label for="fk_afc007_num_situacion">Situaci&oacute;n</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_a006_num_tipo_activoError">
                                    <select name="fk_a006_num_tipo_activo" id="fk_a006_num_tipo_activo" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::miscelaneo('AFTIPOACT',$form.fk_a006_num_tipo_activo)}
                                    </select>
                                    <label for="fk_a006_num_tipo_activo">Tipo de Activo</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_a023_num_centro_costoError">
                                    <select name="fk_a023_num_centro_costo" id="fk_a023_num_centro_costo" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo',$form.fk_a023_num_centro_costo,0,['fk_a004_num_dependencia'=>$form.fk_a004_num_dependencia])}
                                    </select>
                                    <label for="fk_a023_num_centro_costo">Centro de Costo</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_a006_num_tipo_mejoraError">
                                    <select name="fk_a006_num_tipo_mejora" id="fk_a006_num_tipo_mejora" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::miscelaneo('AFTIPOMEJ',$form.fk_a006_num_tipo_mejora)}
                                    </select>
                                    <label for="fk_a006_num_tipo_mejora">Tipo de Mejora</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_a006_num_estado_conservacionError">
                                    <select name="fk_a006_num_estado_conservacion" id="fk_a006_num_estado_conservacion" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::miscelaneo('AFESTCONS',$form.fk_a006_num_estado_conservacion)}
                                    </select>
                                    <label for="fk_a006_num_estado_conservacion">Estado Conservaci&oacute;n</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_afc019_num_categoriaError">
                                    <select name="fk_afc019_num_categoria" id="fk_afc019_num_categoria" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('af_c019_categoria','pk_num_categoria','ind_nombre_categoria',$form.fk_afc019_num_categoria,0,[],'ind_codigo')}
                                    </select>
                                    <label for="fk_afc019_num_categoria">Categoria</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_afc017_num_tipo_vehiculoError">
                                    <select name="fk_afc017_num_tipo_vehiculo" id="fk_afc017_num_tipo_vehiculo" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('af_c017_tipo_vehiculo','pk_num_tipo_vehiculo','ind_nombre_tipo_vehiculo',$form.fk_afc017_num_tipo_vehiculo)}
                                    </select>
                                    <label for="fk_afc017_num_tipo_vehiculo">Tipo de Veh&iacute;culo</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_afc016_num_tipo_seguroError">
                                    <select name="fk_afc016_num_tipo_seguro" id="fk_afc016_num_tipo_seguro" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('af_c016_tipo_seguro','pk_num_tipo_seguro','ind_nombre_tipo_seguro',$form.fk_afc016_num_tipo_seguro)}
                                    </select>
                                    <label for="fk_afc016_num_tipo_seguro">Tipo de Seguro</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group form-group-sm" id="fk_afc008_num_clasificacionError">
                                    <input type="hidden" name="fk_afc008_num_clasificacion" id="fk_afc008_num_clasificacion" value="{$form.fk_afc008_num_clasificacion}">
                                    <input type="text" name="cod_clasificacion" id="cod_clasificacion" value="{$form.cod_clasificacion}" class="form-control input-sm" disabled>
                                    <label for="cod_clasificacion">Clasificaci&oacute;n.</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="ind_nombre_clasificacion" id="ind_nombre_clasificacion" value="{$form.ind_nombre_clasificacion}" class="form-control input-sm" disabled>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Clasificaci&oacute;n"
                                        onclick="selector($(this), '{$_Parametros.url}modAF/maestros/ClasificacionCONTROL/selectorMET', ['fk_afc008_num_clasificacion','cod_clasificacion','ind_nombre_clasificacion']);" {$disabled.ver}>
                                    <i class="md md-search"></i>
                                </button>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group form-group-sm" id="fk_afb001_num_activoError">
                                    <input type="hidden" name="fk_afb001_num_activo" id="fk_afb001_num_activo" value="{$form.fk_afb001_num_activo}">
                                    <input type="text" name="cod_activo" id="cod_activo" value="{$form.cod_activo}" class="form-control input-sm" disabled>
                                    <label for="cod_activo">Activo Principal</label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Activos"
                                        onclick="selector($(this), '{$_Parametros.url}modAF/activos/ActivosCONTROL/selectorMET', ['fk_afb001_num_activo','cod_activo'], 'opcion=');" {$disabled.ver}>
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group form-group-sm" id="fk_afc032_num_clasificacion_sudebipError">
                                    <input type="hidden" name="fk_afc032_num_clasificacion_sudebip" id="fk_afc032_num_clasificacion_sudebip" value="{$form.fk_afc032_num_clasificacion_sudebip}">
                                    <input type="text" name="cod_clasificacion_sudebip" id="cod_clasificacion_sudebip" value="{$form.cod_clasificacion_sudebip}" class="form-control input-sm" disabled>
                                    <label for="cod_clasificacion_sudebip">Clasificaci&oacute;n SUDEBIP</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="ind_nombre_clasificacion_sudebip" id="ind_nombre" value="{$form.ind_nombre}" class="form-control input-sm" disabled>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>                             

                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Clasificaci&oacute;n SUDEBIP"
                                        onclick="selector($(this), '{$_Parametros.url}modAF/maestros/ClasificacionSudebipCONTROL/selectorMET', ['fk_afc032_num_clasificacion_sudebip','cod_clasificacion_sudebip','ind_nombre']);" {$disabled.ver}>
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group form-group-sm" id="fk_a003_num_persona_usuarioError">
                                    <input type="hidden" name="fk_a003_num_persona_usuario" id="fk_a003_num_persona_usuario" value="{$form.fk_a003_num_persona_usuario}">
                                    <input type="text" name="persona_usuario" id="persona_usuario" value="{$form.persona_usuario}" class="form-control input-sm" disabled>
                                    <label for="persona_usuario">Empleado Usuario</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Empleados"
                                        onclick="selector($(this), '{$_Parametros.url}modAF/ListasCONTROL/selectorEmpleadosMET', ['fk_a003_num_persona_usuario','persona_usuario']);" {$disabled.ver}>
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_afc009_num_movimientoError">
                                    <select name="fk_afc009_num_movimiento" id="fk_afc009_num_movimiento" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {if $form.metodo == 'crear'}
                                            {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',$form.fk_afc009_num_movimiento,0,['fk_a006_num_miscelaneo_detalle_tipo_movimiento'=>$form.AFTIPOMOV])}
                                        {else}
                                            {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',$form.fk_afc009_num_movimiento)}
                                        {/if}
                                    </select>
                                    <label for="fk_afc009_num_movimiento">Tipo de Movimiento</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group form-group-sm" id="fk_a003_num_persona_responsableError">
                                    <input type="hidden" name="fk_a003_num_persona_responsable" id="fk_a003_num_persona_responsable" value="{$form.fk_a003_num_persona_responsable}">
                                    <input type="text" name="persona_responsable" id="persona_responsable" value="{$form.persona_responsable}" class="form-control input-sm" maxlength="15" disabled>
                                    <label for="persona_responsable">Empleado Responsable</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Empleados"
                                        onclick="selector($(this), '{$_Parametros.url}modAF/ListasCONTROL/selectorEmpleadosMET', ['fk_a003_num_persona_responsable','persona_responsable']);" {$disabled.ver}>
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_afc005_num_ubicacionError">
                                    <select name="fk_afc005_num_ubicacion" id="fk_afc005_num_ubicacion" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion',$form.fk_afc005_num_ubicacion)}
                                    </select>
                                    <label for="fk_afc005_num_ubicacion">Ubicaci&oacute;n</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="checkbox checkbox-styled" style="margin: 20px;">
                                    <label>
                                        <input type="checkbox" name="num_flag_mantenimiento" id="num_flag_mantenimiento" value="1" {if $form.num_flag_mantenimiento==1}checked{/if} {$disabled.ver}>
                                        <span>Disponible para Mantenimiento</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="checkbox checkbox-styled" style="margin: 20px;">
                                    <label>
                                        <input type="checkbox" name="num_flag_activo_tecnologico" id="num_flag_activo_tecnologico" value="1" {if $form.num_flag_activo_tecnologico==1}checked{/if} {$disabled.ver}>
                                        <span>Activo Tecnol&oacute;gico</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="step2">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-head card-head-xs style-primary text-center">
                                <header class="text-center">Informaci&oacute;n del Activo</header>
                            </div>
                            <div class="card-body" style="padding: 4px;">
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="fk_a006_num_marcaError">
                                        <select name="fk_a006_num_marca" id="fk_a006_num_marca" class="form-control input-sm" {$disabled.ver}>
                                            <option value="">&nbsp;</option>
                                            {Select::miscelaneo('MARCAS',$form.fk_a006_num_marca)}
                                        </select>
                                        <label for="fk_a006_num_marca">Fabricante (Marca)</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_placaError">
                                        <input type="text" name="ind_placa" id="ind_placa" value="{$form.ind_placa}" class="form-control input-sm" maxlength="15" {$disabled.ver}>
                                        <label for="ind_placa">Nro. Placa</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_dimensionesError">
                                        <input type="text" name="ind_dimensiones" id="ind_dimensiones" value="{$form.ind_dimensiones}" class="form-control input-sm" maxlength="45" {$disabled.ver}>
                                        <label for="ind_dimensiones">Dimensiones</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="num_anioError">
                                        <input type="text" name="num_anio" id="num_anio" value="{$form.num_anio}" class="form-control input-sm year" maxlength="4" {$disabled.ver}>
                                        <label for="num_anio">A&ntilde;o Fabricaci&oacute;n</label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_modeloError">
                                        <input type="text" name="ind_modelo" id="ind_modelo" value="{$form.ind_modelo}" class="form-control input-sm" maxlength="45" {$disabled.ver}>
                                        <label for="ind_modelo">Modelo</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_motorError">
                                        <input type="text" name="ind_motor" id="ind_motor" value="{$form.ind_motor}" class="form-control input-sm" maxlength="45" {$disabled.ver}>
                                        <label for="ind_motor">Marca Motor</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_nro_parteError">
                                        <input type="text" name="ind_nro_parte" id="ind_nro_parte" value="{$form.ind_nro_parte}" class="form-control input-sm" maxlength="45" {$disabled.ver}>
                                        <label for="ind_nro_parte">Nro. Parte</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="fec_ingresoError">
                                        <input type="text" name="fec_ingreso" id="fec_ingreso" value="{Fecha::formatFecha($form.fec_ingreso,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver} onchange="periodoDepreciacion(this.value);">
                                        <label for="fec_ingreso">Fecha Ingreso</label>
                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_serieError">
                                        <input type="text" name="ind_serie" id="ind_serie" value="{$form.ind_serie}" class="form-control input-sm" maxlength="45" {$disabled.ver}>
                                        <label for="ind_serie">Nro. de Serie</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="num_asientosError">
                                        <input type="text" name="num_asientos" id="num_asientos" value="{$form.num_asientos}" class="form-control input-sm" maxlength="3" {$disabled.ver}>
                                        <label for="num_asientos">Nro. Asientos</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="fk_a006_num_colorError">
                                        <select name="fk_a006_num_color" id="fk_a006_num_color" class="form-control input-sm" {$disabled.ver}>
                                            <option value="">&nbsp;</option>
                                            {Select::miscelaneo('COLORES',$form.fk_a006_num_color)}
                                        </select>
                                        <label for="fk_a006_num_color">Color</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_periodo_inicio_depreciacionError">
                                        <input type="text" name="ind_periodo_inicio_depreciacion" id="ind_periodo_inicio_depreciacion" value="{$form.ind_periodo_inicio_depreciacion}" class="form-control input-sm periodo" maxlength="7" {$disabled.ver}>
                                        <label for="ind_periodo_inicio_depreciacion">Depreciaci&oacute;n</label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_serie_motorError">
                                        <input type="text" name="ind_serie_motor" id="ind_serie_motor" value="{$form.ind_serie_motor}" class="form-control input-sm" maxlength="45" {$disabled.ver}>
                                        <label for="ind_serie_motor">Nro. Serie Motor</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_materialError">
                                        <input type="text" name="ind_material" id="ind_material" value="{$form.ind_material}" class="form-control input-sm" maxlength="45" {$disabled.ver}>
                                        <label for="ind_material">Material</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="fk_a008_num_paisError">
                                        <select name="fk_a008_num_pais" id="fk_a008_num_pais" class="form-control input-sm" {$disabled.ver}>
                                            <option value="">&nbsp;</option>
                                            {Select::lista('a008_pais','pk_num_pais','ind_pais',$form.fk_a008_num_pais)}
                                        </select>
                                        <label for="fk_a008_num_pais">Pais Fabricaci&oacute;n</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-group-sm floating-label" id="ind_periodo_inicio_revaluacionError">
                                        <input type="text" name="ind_periodo_inicio_revaluacion" id="ind_periodo_inicio_revaluacion" value="{$form.ind_periodo_inicio_revaluacion}" class="form-control input-sm periodo" maxlength="7" {$disabled.ver}>
                                        <label for="ind_periodo_inicio_revaluacion">Ajust. x Inflac.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-head card-head-xs style-primary text-center">
                                <header class="text-center">Informaci&oacute;n Adicional</header>
                            </div>
                            <div class="card-body" style="padding: 4px;">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="fk_a001_num_organismoError">
                                        <select name="fk_afc018_num_poliza_seguro" id="fk_afc018_num_poliza_seguro" class="form-control input-sm" {$disabled.ver}>
                                            <option value="">&nbsp;</option>
                                            {Select::lista('af_c018_poliza_seguro','pk_num_poliza_seguro','ind_nombre_poliza_seguro',$form.fk_afc018_num_poliza_seguro)}
                                        </select>
                                        <label for="fk_afc018_num_poliza_seguro">P&oacute;liza de Seguro</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="num_unidadesError">
                                        <input type="text" name="num_unidades" id="num_unidades" value="{$form.num_unidades}" class="form-control input-sm" maxlength="3" {$disabled.ver}>
                                        <label for="num_unidades">Nro. Unidades</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="fk_lgb004_num_unidadError">
                                        <select name="fk_lgb004_num_unidad" id="fk_lgb004_num_unidad" class="form-control input-sm" {$disabled.ver}>
                                            <option value="">&nbsp;</option>
                                            {Select::lista('lg_b004_unidades','pk_num_unidad','ind_descripcion',$form.fk_lgb004_num_unidad)}
                                        </select>
                                        <label for="fk_lgb004_num_unidad">Unidad de Medida</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="fec_inventarioError">
                                        <input type="text" name="fec_inventario" id="fec_inventario" value="{Fecha::formatFecha($form.fec_inventario,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                        <label for="fec_inventario">Fecha Inventario</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-head card-head-xs style-primary text-center">
                                <header class="text-center">Informaci&oacute;n Monetaria</header>
                            </div>
                            <div class="card-body" style="padding: 4px;">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="num_montoError">
                                        <input type="text" name="num_monto" id="num_monto" value="{number_format($form.num_monto,2,',','.')}" class="form-control input-sm money" style="text-align:right;" {$disabled.ver}>
                                        <label for="num_monto">Monto Local</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="num_valor_mercadoError">
                                        <input type="text" name="num_valor_mercado" id="num_valor_mercado" value="{number_format($form.num_valor_mercado,2,',','.')}" class="form-control input-sm money" style="text-align:right;" {$disabled.ver}>
                                        <label for="num_valor_mercado">Valor Mercado</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="num_monto_referencialError">
                                        <input type="text" name="num_monto_referencial" id="num_monto_referencial" value="{number_format($form.num_monto_referencial,2,',','.')}" class="form-control input-sm money" style="text-align:right;" {$disabled.ver}>
                                        <label for="num_monto_referencial">Monto Referencial</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-styled" style="margin-top:15px;">
                                        <label>
                                            <input type="checkbox" name="num_flag_depreciacion" id="num_flag_depreciacion" value="1" {if $form.num_flag_depreciacion==1}checked{/if} {$disabled.ver}>
                                            <span>Depreciaci&oacute;n Espec&iacute;fica</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-head card-head-xs style-primary text-center">
                                <header class="text-center">Informaci&oacute;n del Voucher</header>
                            </div>
                            <div class="card-body" style="padding: 4px;">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="ind_periodo_bajaError">
                                        <input type="text" name="ind_periodo_baja" id="ind_periodo_baja" value="{$form.voucher_periodo}" class="form-control input-sm" disabled>
                                        <label for="ind_periodo_baja">Periodo de Baja</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="fk_cbb001_num_voucher_bajaError">
                                        <input type="hidden" name="fk_cbb001_num_voucher_baja" id="fk_cbb001_num_voucher_baja" class="form-control input-sm">
                                        <input type="text" name="voucher_baja" id="voucher_baja" value="{$form.voucher_baja}" class="form-control input-sm" disabled>
                                        <label for="voucher_baja">Voucher de Baja</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm floating-label" id="fk_cbb001_num_voucher_ingresoError">
                                        <input type="hidden" name="fk_cbb001_num_voucher_ingreso" id="fk_cbb001_num_voucher_ingreso">
                                        <input type="text" name="voucher_ingreso" id="voucher_ingreso" value="{$form.voucher_ingreso}" class="form-control input-sm" disabled>
                                        <label for="voucher_ingreso">Voucher de Ingreso</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-styled" style="margin-top:15px;">
                                        <label>
                                            <input type="checkbox" name="num_flag_voucher_ingreso" id="num_flag_voucher_ingreso" value="1" {if $form.num_flag_voucher_ingreso==1}checked{/if} {$disabled.ver}>
                                            <span>Generar Voucher de Ingreso del Activo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-head card-head-xs style-primary text-center">
                                <header class="text-center">Informaci&oacute;n Documentaria</header>
                            </div>
                            <div class="card-body" style="padding: 4px;">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-sm" id="fk_a003_num_proveedorError">
                                            <input type="hidden" name="fk_a003_num_proveedor" id="fk_a003_num_proveedor" value="{$form.fk_a003_num_proveedor}">
                                            <input type="text" name="proveedor" id="proveedor" value="{$form.proveedor}" class="form-control input-sm" maxlength="15" disabled>
                                            <label for="proveedor">Proveedor</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                                data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Proveedores"
                                                onclick="selector($(this), '{$_Parametros.url}modAF/ListasCONTROL/selectorPersonasMET', ['fk_a003_num_proveedor','proveedor']);" {$disabled.ver}>
                                            <i class="md md-search"></i>
                                        </button>
                                    </div>
                                    <div class="col-sm-3 col-md-offset-2">
                                        <div class="form-group form-group-sm floating-label" id="ind_nro_orden_compraError">
                                            <input type="text" name="ind_nro_orden_compra" id="ind_nro_orden_compra" value="{$form.ind_nro_orden_compra}" class="form-control input-sm" maxlength="20" {$disabled.ver}>
                                            <label for="ind_nro_orden_compra">Orden Compra</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group form-group-sm floating-label" id="fec_orden_compraError">
                                            <input type="text" name="fec_orden_compra" id="fec_orden_compra" value="{Fecha::formatFecha($form.fec_orden_compra,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                            <label for="fec_orden_compra">Fecha</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-5">
                                        <div class="form-group form-group-sm floating-label" id="fk_cpb002_num_tipo_documentoError">
                                            <select name="fk_cpb002_num_tipo_documento" id="fk_cpb002_num_tipo_documento" class="form-control input-sm" {$disabled.ver}>
                                                <option value="">&nbsp;</option>
                                                {Select::lista('cp_b002_tipo_documento','pk_num_tipo_documento','ind_descripcion',$form.fk_cpb002_num_tipo_documento,0,[],'cod_tipo_documento')}
                                            </select>
                                            <label for="fk_cpb002_num_tipo_documento">Tipo de Documento</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-offset-2">
                                        <div class="form-group form-group-sm floating-label" id="ind_nro_guia_remisionError">
                                            <input type="text" name="ind_nro_guia_remision" id="ind_nro_guia_remision" value="{$form.ind_nro_guia_remision}" class="form-control input-sm" maxlength="20" {$disabled.ver}>
                                            <label for="ind_nro_guia_remision">Guia Remisi&oacute;n</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group form-group-sm floating-label" id="fec_guia_remisionError">
                                            <input type="text" name="fec_guia_remision" id="fec_guia_remision" value="{Fecha::formatFecha($form.fec_guia_remision,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                            <label for="fec_guia_remision">Fecha</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <div class="form-group form-group-sm floating-label" id="ind_nro_controlError">
                                            <input type="text" name="ind_nro_control" id="ind_nro_control" value="{$form.ind_nro_control}" class="form-control input-sm" maxlength="20" {$disabled.ver}>
                                            <label for="ind_nro_control">Nro. Control</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group form-group-sm floating-label" id="fec_fecha_obligacionError">
                                            <input type="text" name="fec_fecha_obligacion" id="fec_fecha_obligacion" value="{Fecha::formatFecha($form.fec_fecha_obligacion,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                            <label for="fec_fecha_obligacion">Fecha</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-offset-2">
                                        <div class="form-group form-group-sm floating-label" id="ind_nro_documento_almacenError">
                                            <input type="text" name="ind_nro_documento_almacen" id="ind_nro_documento_almacen" value="{$form.ind_nro_documento_almacen}" class="form-control input-sm" maxlength="20" {$disabled.ver}>
                                            <label for="ind_nro_documento_almacen">Doc. Almacen</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group form-group-sm floating-label" id="fec_documento_almacenError">
                                            <input type="text" name="fec_documento_almacen" id="fec_documento_almacen" value="{Fecha::formatFecha($form.fec_documento_almacen,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                            <label for="fec_documento_almacen">Fecha</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <div class="form-group form-group-sm floating-label" id="ind_nro_facturaError">
                                            <input type="text" name="ind_nro_factura" id="ind_nro_factura" value="{$form.ind_nro_factura}" class="form-control input-sm" maxlength="20" {$disabled.ver}>
                                            <label for="ind_nro_factura">Nro. Factura</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group form-group-sm floating-label" id="fec_facturaError">
                                            <input type="text" name="fec_factura" id="fec_factura" value="{Fecha::formatFecha($form.fec_factura,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                            <label for="fec_factura">Fecha</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-sm floating-label" id="ind_observacionesError">
                                            <textarea name="ind_observaciones" id="ind_observaciones" class="form-control input-sm" style="height:60px;" {$disabled.ver}>{$form.ind_observaciones}</textarea>
                                            <label for="ind_observaciones">Observaciones</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="step3">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_afc021_num_tipo_transaccionError">
                                    <select name="fk_afc021_num_tipo_transaccion" id="fk_afc021_num_tipo_transaccion" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('af_c021_tipo_transaccion','pk_num_tipo_transaccion','ind_nombre_transaccion',$form.fk_afc021_num_tipo_transaccion,0,['ind_tipo'=>'A'],'ind_codigo')}
                                    </select>
                                    <label for="fk_afc021_num_tipo_transaccion">Tipo de Transacci&oacute;n</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card card-underline">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Contabilidad</th>
                                                <th>Cuenta</th>
                                                <th style="text-align:left;">Descripci&oacute;n</th>
                                                <th>Signo</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lista_tipo_transaccion_cuentas">
                                            {foreach item=registro from=$form.lista_tipo_transaccion_cuentas}
                                                <tr>
                                                    <td>
                                                        <div class="col-sm-12">
                                                            {$registro.nombre_contabilidad}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-12">
                                                            {$registro.num_cuenta} - {$registro.nombre_cuenta}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-12">
                                                            {$registro.ind_descripcion}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-12">
                                                            {$registro.ind_signo}
                                                        </div>
                                                    </td>
                                                </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    {if $form.metodo != ''}
        <div class="modal-footer">
            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
                <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
            </button>
            {if $form.metodo == 'aprobar'}
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                    <span class="md md-done-all"></span> Aprobar
                </button>
            {else}
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                    <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                </button>
            {/if}
        </div>
    {/if}
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();
        inicializar();
        
        //  tama�o de ventana
        $('#modalAncho').css("width", "90%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('AF-01-01-01-02',$_Parametros.perfil)}", function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if ('{$form.metodo}' == 'aprobar') {
                        app.metEliminarRegistroJson('dataTablaJson',dato['mensaje'],'cerrarModal','ContenidoModal');
                    }
                    else if (dato['status'] == 'crear') {
                        app.metNuevoRegistroTablaJson('dataTablaJson',dato['mensaje'],'cerrarModal','ContenidoModal');
                    }
                    else if (dato['status'] == 'modificar') {
                        app.metActualizarRegistroTablaJson('dataTablaJson',dato['mensaje'],'cerrarModal','ContenidoModal');
                    }
                    else {
                        cargarUrl("{$_Parametros.url}modAF/activos/ActivosCONTROL/");
                    }
                }
            }, 'json');
        });

        $('#fk_afc021_num_tipo_transaccion').change(function() {
            $.post("{$_Parametros.url}modAF/activos/ActivosCONTROL/TipoTransaccionCuentasMET", "fk_afc021_num_tipo_transaccion="+$(this).val(), function(dato) {
                $('#lista_tipo_transaccion_cuentas').html(dato);
            });
        });
    });

    //  obtener el periodo de depreciación a partir de la fecha de ingreso
    function periodoDepreciacion(fec_ingreso){
        $.post("{$_Parametros.url}modAF/activos/ActivosCONTROL/PeriodoDepreciacionMET", "fec_ingreso="+fec_ingreso, function(dato) {
            $('#ind_periodo_inicio_depreciacion').val(dato['ind_periodo_inicio_depreciacion']);
        }, 'json');
    }
</script>
