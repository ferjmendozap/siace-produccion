<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary pull-left">Transferir Activos de Log&iacute;stica</h2>
        <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary pull-right" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                        
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ingresos" aria-controls="ingresos" role="tab" data-toggle="tab">Ingresos x Activar</a></li>
                    <li role="presentation"><a href="#transferidos" aria-controls="transferidos" role="tab" data-toggle="tab">Activos Transferidos</a></li>
                </ul>
                <br>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="ingresos">
                        <div class="card card-underline">
                            <div class="card-head">
                                <div class="tools">
                                    <div class="btn-group">
                                        {if in_array('AF-01-01-02',$_Parametros.perfil)}
                                            <button id="bt_transferir" class="btn ink-reaction btn-raised btn-info btn-sm"
                                                data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                titulo="Transferir Activo" onclick="editar('{$_Parametros.url}modAF/activos/ActivosCONTROL/formMET/transferir', $(this));">
                                                <i class="fa fa-upload"></i> Transferir
                                            </button>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover datatable1 seleccionable">
                                        <thead>
                                            <tr>
                                                <th width="50">Id.</th>
                                                <th width="150">Orden de Compra</th>
                                                <th width="100">Secuencia</th>
                                                <th width="50">#</th>
                                                <th>Descripcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach item=registro from=$listado_pendientes}
                                                <tr id="id{$registro.pk_num_activo_fijo}">
                                                    <td>
                                                        <input type="checkbox" name="idRegistro[]" value="{$registro.pk_num_activo_fijo}" class="seleccionable" style="display:none;">
                                                        <label>{$registro.pk_num_activo_fijo}</label>
                                                    </td>
                                                    <td><label>{$registro.ind_orden}</label></td>
                                                    <td><label>{$registro.num_secuencia_orden}</label></td>
                                                    <td><label>{$registro.num_secuencia_orden}</label></td>
                                                    <td><label>{$registro.ind_descripcion}</label></td>
                                                </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="transferidos">
                        <div class="card card-underline">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover datatable1">
                                        <thead>
                                            <tr>
                                                <th style="width:50px; max-width:50px;">Id.</th>
                                                <th style="width:100px; max-width:100px;">C&oacute;digo</th>
                                                <th style="width:500px; min-width:500px;">Descripcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach item=registro from=$listado_transferidos}
                                                <tr id="idTransferidos{$registro.pk_num_activo}">
                                                    <td><label>{$registro.pk_num_activo}</label></td>
                                                    <td><label>{$registro.cod_codigo_interno}</label></td>
                                                    <td><label>{$registro.ind_descripcion_activo}</label></td>
                                                </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="offcanvas">
    <div id="offcanvas-filtro" class="offcanvas-pane width-7">
        <div class="offcanvas-head">
            <header>Filtro</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form action="{$_Parametros.url}modAF/ActivosTransferirCONTROL/indexMET" id="formFiltro" class="form" role="form" method="post" autocomplete="off">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm floating-label">
                            <select name="fpk_num_organismo" id="fpk_num_organismo" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a004_dependencia', $('#fpk_num_dependencia'), 'fk_a001_num_organismo='+$(this).val(), ['fpk_num_centro_costo']);">
                                <option value="">&nbsp;</option>
                                {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',{$filtro.fpk_num_organismo})}
                            </select>
                            <label for="fpk_num_organismo">Organismo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm floating-label">
                            <select name="fpk_num_dependencia" id="fpk_num_dependencia" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a023_centro_costo', $('#fpk_num_centro_costo'), 'fk_a004_num_dependencia='+$(this).val());">
                                <option value="">&nbsp;</option>
                                {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia',{$filtro.fpk_num_dependencia},0,['fk_a001_num_organismo'=>$filtro.fpk_num_organismo])}
                            </select>
                            <label for="fpk_num_dependencia">Dependencia</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm floating-label">
                            <select name="fpk_num_centro_costo" id="fpk_num_centro_costo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo',{$filtro.fpk_num_centro_costo},0,['fk_a004_num_dependencia'=>$filtro.fpk_num_dependencia])}
                            </select>
                            <label for="fpk_num_centro_costo">Centro de Costo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm floating-label">
                            <select name="fpk_num_categoria" id="fpk_num_categoria" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('af_c019_categoria','pk_num_categoria','ind_nombre_categoria',{$filtro.fpk_num_categoria})}
                            </select>
                            <label for="fpk_num_categoria">Categor&iacute;a</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group form-group-sm">
                            <input type="hidden" name="fpk_num_clasificacion" id="fpk_num_clasificacion" value="{$filtro.fpk_num_clasificacion}">
                            <input type="text" name="fcod_clasificacion" id="fcod_clasificacion" value="{$filtro.fcod_clasificacion}" class="form-control input-sm" readonly>
                            <label for="fcod_clasificacion">Clasificaci&oacute;n</label>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group form-group-sm">
                            <input type="text" name="find_nombre_clasificacion" id="find_nombre_clasificacion" value="{$filtro.find_nombre_clasificacion}" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -15px;"
                                data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Clasificaci&oacute;n"
                                onclick="selector($(this), '{$_Parametros.url}modAF/ClasificacionCONTROL/selectorMET', ['fpk_num_clasificacion','fcod_clasificacion','find_nombre_clasificacion']);">
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm floating-label">
                            <select name="fpk_num_movimiento" id="fpk_num_movimiento" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',$filtro.fpk_num_movimiento)}
                            </select>
                            <label for="fpk_num_movimiento">Tipo de Movimiento</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm floating-label">
                            <select name="fpk_num_ubicacion" id="fpk_num_ubicacion" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion',$filtro.fpk_num_ubicacion)}
                            </select>
                            <label for="fpk_num_ubicacion">Ubicaci&oacute;n</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm floating-label">
                            <select name="fpk_num_situacion" id="fpk_num_situacion" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('af_c007_situacion','pk_num_situacion','ind_nombre_situacion',{$filtro.fpk_num_situacion})}
                            </select>
                            <label for="fpk_num_situacion">Situaci&oacute;n</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm floating-label">
                            <select name="fnum_tipo_activo" id="fnum_tipo_activo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::miscelaneo('AFTIPOACT',$filtro.fnum_tipo_activo)}
                            </select>
                            <label for="fnum_tipo_activo">Tipo de Activo</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modAF/ActivosTransferirCONTROL/';
    });
</script>