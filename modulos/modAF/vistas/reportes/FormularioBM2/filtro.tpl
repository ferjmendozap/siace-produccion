<form id="formAjax" class="form" role="form" method="post">
    <div class="card card-underline">
        <div class="card-head">
            <div class="col-sm-12">
                <h2 class="text-primary">Relación de Movimientos de Bienes Muebles</h2>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-group-sm" id="ffk_a001_num_organismoError">
                        <select name="ffk_a001_num_organismo" id="ffk_a001_num_organismo" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a004_dependencia', $('#ffk_a004_num_dependencia'), 'fk_a001_num_organismo='+$(this).val(), ['ffk_a023_num_centro_costo']);">
                            <option value="">&nbsp;</option>
                            {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa')}
                        </select>
                        <label for="ffk_a001_num_organismo">Organismo</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-group-sm floating-label" id="ffk_afc009_num_movimientoError">
                        <select name="ffk_afc009_num_movimiento" id="ffk_afc009_num_movimiento" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento')}
                        </select>
                        <label for="ffk_afc009_num_movimiento">Tipo de Movimiento</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-group-sm" id="ffk_a004_num_dependenciaError">
                        <select name="ffk_a004_num_dependencia" id="ffk_a004_num_dependencia" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a023_centro_costo', $('#ffk_a023_num_centro_costo'), 'fk_a004_num_dependencia='+$(this).val());">
                            <option value="">&nbsp;</option>
                            {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia','',0,['fk_a001_num_organismo'=>''])}
                        </select>
                        <label for="ffk_a004_num_dependencia">Dependencia</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-group-sm floating-label" id="ffk_afc005_num_ubicacionError">
                        <select name="ffk_afc005_num_ubicacion" id="ffk_afc005_num_ubicacion" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion')}
                        </select>
                        <label for="ffk_afc009_num_movimiento">Ubicaci&oacute;n</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-group-sm" id="ffk_a023_num_centro_costoError">
                        <select name="ffk_a023_num_centro_costo" id="ffk_a023_num_centro_costo" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo','',0,['fk_a004_num_dependencia'=>''])}
                        </select>
                        <label for="ffk_a023_num_centro_costo">Centro de Costo</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm floating-label" id="ffk_afc007_num_situacionError">
                        <select name="ffk_afc007_num_situacion" id="ffk_afc007_num_situacion" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::lista('af_c007_situacion','pk_num_situacion','ind_nombre_situacion')}
                        </select>
                        <label for="ffk_afc007_num_situacion">Situaci&oacute;n</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="fperiodoError">
                        <input type="text" name="fperiodo" id="fperiodo" value="{$filtro.fperiodo}" class="form-control input-sm periodo" maxlength="7">
                        <label for="fperiodo">Periodo</label>
                    </div>
                </div>
            </div>

            <center>
                <div class="btn-group">
                    <button type="button" id="imprimir" class="btn ink-reaction btn-raised btn-info btn-sm">
                        Buscar
                    </button>
                    <button type="button" id="imprimir_lanzador" class="btn ink-reaction btn-raised btn-info btn-sm hidden"
                            data-toggle="modal"
                            data-target="#formModal"
                            data-keyboard="false"
                            data-backdrop="static" titulo="Formulario BM-2">
                    </button>
                </div>
            </center>
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modAF/reportes/FormularioBM2CONTROL/';

        //  mostrar pdf
        $('#imprimir').click(function() {
            if ($('#fperiodo').val() == '') {
                swal("Error!", 'Periodo Obligatorio', "error");
            }
            else {
                $('#imprimir_lanzador').click();
            }
        });
        $('#imprimir_lanzador').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'FrameMET', $('#formAjax').serialize(), function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>
