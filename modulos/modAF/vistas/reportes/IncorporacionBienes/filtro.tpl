<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary pull-left">{$data.titulo}</h2>
        <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary pull-right" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="100">Acta</th>
                                        <th width="100">Fecha</th>
                                        <th>Responsable</th>
                                        <th>Aprobado Por</th>
                                        <th width="75" style="text-align: center;">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {foreach item=registro from=$listado}
                                    <tr id="id{$registro.pk_num_acta_incorporacion}">
                                        <td><label>{$registro.ind_nro_acta}-{$registro.num_anio}</label></td>
                                        <td><label>{Fecha::formatFecha($registro.fec_fecha,'Y-m-d','d-m-Y')}</label></td>
                                        <td><label>{$registro.persona_responsable}</label></td>
                                        <td><label>{$registro.persona_aprobado}</label></td>
                                        <td nowrap="true" style="text-align: center;">
                                            <button idRegistro="{$registro.pk_num_acta_incorporacion}" class="btn ink-reaction btn-raised btn-xs btn-info" id="imprimir"
                                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Imprimir Acta" title="Imprimir Acta">
                                                <i class="fa fa-print" style="color: #ffffff;"></i>
                                            </button>
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="offcanvas">
    <div id="offcanvas-filtro" class="offcanvas-pane width-7">
        <div class="offcanvas-head">
            <header>Filtro</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form action="{$_Parametros.url}modAF/reportes/IncorporacionBienesCONTROL/" id="formFiltro" class="form" role="form" method="post" autocomplete="off" onsubmit="return cargarPagina();">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_organismo" id="fpk_num_organismo" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a004_dependencia', $('#fpk_num_dependencia'), 'fk_a001_num_organismo='+$(this).val(), ['fpk_num_centro_costo']);">
                                <option value="">&nbsp;</option>
                                {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',{$filtro.fpk_num_organismo})}
                            </select>
                            <label for="fpk_num_organismo">Organismo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_dependencia" id="fpk_num_dependencia" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a023_centro_costo', $('#fpk_num_centro_costo'), 'fk_a004_num_dependencia='+$(this).val());">
                                <option value="">&nbsp;</option>
                                {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia',{$filtro.fpk_num_dependencia},0,['fk_a001_num_organismo'=>$filtro.fpk_num_organismo])}
                            </select>
                            <label for="fpk_num_dependencia">Dependencia</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_centro_costo" id="fpk_num_centro_costo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo',{$filtro.fpk_num_centro_costo},0,['fk_a004_num_dependencia'=>$filtro.fpk_num_dependencia])}
                            </select>
                            <label for="fpk_num_centro_costo">Centro de Costo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group form-group-sm">
                            <input type="text" name="ffec_fechad" id="ffec_fechad" value="{$filtro.ffec_fechad}" class="form-control input-sm date">
                            <label for="ffec_fechad">Desde</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-sm">
                            <input type="text" name="ffec_fechah" id="ffec_fechah" value="{$filtro.ffec_fechah}" class="form-control input-sm date">
                            <label for="ffec_fechah">Hasta</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modAF/reportes/IncorporacionBienesCONTROL/';

        //  imprimir registro
        $('.datatable1 tbody').on( 'click', '#imprimir', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'MostrarActaIncorporacionMET/', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>
