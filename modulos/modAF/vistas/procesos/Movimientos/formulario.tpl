<form action="{$_Parametros.url}modAF/procesos/MovimientosCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" name="pk_num_movimiento" id="pk_num_movimiento" value="{$form.pk_num_movimiento}">
    <div class="modal-body">
        <div class="panel panel-default">
            <div class="panel-heading">Informaci&oacute;n General</div>
            <div class="panel-body">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm">
                                <input type="text" name="ind_nro_acta" id="ind_nro_acta" value="{$form.ind_nro_acta}-{$form.num_anio}" class="form-control input-sm" disabled>
                                <label for="ind_nro_acta">N&uacute;mero</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm floating-label" id="ind_estadoError">
                                <input type="hidden" name="ind_estado" id="ind_estado" value="{$form.ind_estado}">
                                <input type="text" name="estado" id="estado" value="{Select::options('af_estado_movimiento', $form.ind_estado, 2)}" class="form-control input-sm" disabled>
                                <label for="estado">Estado</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm floating-label" id="fec_fechaError">
                                <input type="text" name="fec_fecha" id="fec_fecha" value="{Fecha::formatFecha($form.fec_fecha,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                <label for="fec_fecha">Fecha</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm floating-label" id="ind_tipoError">
                                <select name="ind_tipo" id="ind_tipo" class="form-control input-sm" onchange="select('{$_Parametros.url}modAF/procesos/MovimientosCONTROL/SelectMotivoTrasladoMET', $('#fk_a006_num_motivo'), 'ind_tipo='+$(this).val());" {$disabled.ver}>
                                    <option value="">&nbsp;</option>
                                    {Select::options('af_tipo_movimiento',$form.ind_tipo)}
                                </select>
                                <label for="ind_tipo">Tipo de Movimiento</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm floating-label" id="fk_a001_num_organismoError">
                                <select name="fk_a001_num_organismo" id="fk_a001_num_organismo" class="form-control input-sm" {$disabled.editar}>
                                    <option value="">&nbsp;</option>
                                    {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',$form.fk_a001_num_organismo)}
                                </select>
                                <label for="fk_a001_num_organismo">Organismo</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm floating-label" id="fk_a006_num_motivoError">
                                <select name="fk_a006_num_motivo" id="fk_a006_num_motivo" class="form-control input-sm" {$disabled.ver}>
                                    <option value="">&nbsp;</option>
                                    {if $form.ind_tipo == 'I'}
                                        {Select::miscelaneo('AFMOTTRASI',$form.fk_a006_num_motivo)}
                                    {elseif $form.ind_tipo == 'E'}
                                        {Select::miscelaneo('AFMOTTRASE',$form.fk_a006_num_motivo)}
                                    {/if}
                                </select>
                                <label for="fk_a006_num_motivo">Motivo de Traslado</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-11 col-md-offset-1">
                            <div class="form-group form-group-sm floating-label" id="ind_comentariosError">
                                <textarea name="ind_comentarios" id="ind_comentarios" class="form-control input-sm" style="height:95px;" {$disabled.ver}>{$form.ind_comentarios}</textarea>
                                <label for="ind_comentarios">Comentarios</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Activos Afectados
                <div class="pull-right">
                    <button id="btInsertar" class="btn ink-reaction btn-raised btn-info btn-xs" data-lista="detalle" onclick="insertar();" {$disabled.ver}>
                        <i class="md md-add"></i>
                    </button>
                    <button id="btQuitar" class="btn ink-reaction btn-raised btn-danger btn-xs" onclick="$('#lista_detalle tr.ui-selected').remove();" {$disabled.ver}>
                        <i class="md md-delete"></i>
                    </button>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive" style="height:610px;">
                    <table class="table table-hover table-condensed seleccionable">
                        <tbody id="lista_detalle">
                            {$numero = 0}
                            {foreach item=registro from=$form.lista_detalle}
                                {$numero=$numero+1}
                                <tr id="detalle{$numero}">
                                    <td>
                                        <div class="container-fluid">
                                            <div class="row" style="background-color:#DDD; border-radius:5px; padding-top:10px;">
                                                <div class="col-sm-2">
                                                    <div class="form-group form-group-sm" id="detalle_fk_afb001_num_activo{$numero}Error">
                                                        <input type="hidden" name="detalle_fk_afb001_num_activo[]" id="detalle_fk_afb001_num_activo{$numero}" value="{$registro.fk_afb001_num_activo}">
                                                        <input type="text" name="detalle_cod_activo[]" id="detalle_cod_activo{$numero}" value="{$registro.cod_activo}" class="form-control input-sm" disabled>
                                                        <label for="detalle_cod_activo{$numero}">Activo</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Activos"
                                                            onclick="selector($(this), '{$_Parametros.url}modAF/activos/ActivosCONTROL/selectorMET', ['detalle_fk_afb001_num_activo{$numero}','detalle_cod_activo{$numero}','detalle_desc_activo{$numero}','detalle_fk_a001_num_organismo_ant{$numero}','detalle_fk_a004_num_dependencia_ant{$numero}','detalle_fk_a023_num_centro_costo_ant{$numero}','detalle_fk_afc005_num_ubicacion_ant{$numero}','detalle_fk_a003_num_usuario_ant{$numero}','detalle_persona_usuario_ant{$numero}','detalle_fk_a003_num_responsable_ant{$numero}','detalle_persona_responsable_ant{$numero}'], 'opcion=af_movimientos');" {$disabled.ver}>
                                                        <i class="md md-search"></i>
                                                    </button>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="form-group form-group-sm" id="detalle_desc_activo{$numero}Error">
                                                        <textarea name="detalle_desc_activo[]" id="detalle_desc_activo{$numero}" class="form-control input-sm" disabled>{$registro.desc_activo}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-6"><span class="label label-default">Datos Anteriores</span></div>
                                                <div class="col-sm-6"><span class="label label-default">Datos Actuales</span></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select id="detalle_fk_a001_num_organismo_ant{$numero}" class="form-control input-sm" disabled>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',$registro.fk_a001_num_organismo_ant)}
                                                        </select>
                                                        <label for="detalle_fk_a001_num_organismo_ant{$numero}">Organismo</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select name="detalle_fk_a001_num_organismo[]" id="detalle_fk_a001_num_organismo{$numero}" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a004_dependencia', $('#detalle_fk_a004_num_dependencia{$numero}'), 'fk_a001_num_organismo='+$(this).val(), ['detalle_fk_a023_num_centro_costo{$numero}']);" {$disabled.ver}>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',$registro.fk_a001_num_organismo)}
                                                        </select>
                                                        <label for="detalle_fk_a001_num_organismo{$numero}">Organismo</label>
                                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select id="detalle_fk_a004_num_dependencia_ant{$numero}" class="form-control input-sm" disabled>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia',$registro.fk_a004_num_dependencia_ant,0,['fk_a001_num_organismo'=>$registro.fk_a001_num_organismo_ant])}
                                                        </select>
                                                        <label for="detalle_fk_a004_num_dependencia_ant{$numero}">Dependencia</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select name="detalle_fk_a004_num_dependencia[]" id="detalle_fk_a004_num_dependencia{$numero}" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a023_centro_costo', $('#detalle_fk_a023_num_centro_costo{$numero}'), 'fk_a004_num_dependencia='+$(this).val());" {$disabled.ver}>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia',$registro.fk_a004_num_dependencia,0,['fk_a001_num_organismo'=>$registro.fk_a001_num_organismo])}
                                                        </select>
                                                        <label for="detalle_fk_a004_num_dependencia{$numero}">Dependencia</label>
                                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select id="detalle_fk_a023_num_centro_costo_ant{$numero}" class="form-control input-sm" disabled>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo',$registro.fk_a023_num_centro_costo_ant)}
                                                        </select>
                                                        <label for="detalle_fk_a023_num_centro_costo_ant{$numero}">Centro de Costo</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select name="detalle_fk_a023_num_centro_costo[]" id="detalle_fk_a023_num_centro_costo{$numero}" class="form-control input-sm" {$disabled.ver}>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo',$registro.fk_a023_num_centro_costo,0,['fk_a004_num_dependencia'=>$registro.fk_a004_num_dependencia])}
                                                        </select>
                                                        <label for="detalle_fk_a023_num_centro_costo{$numero}">Centro de Costo</label>
                                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select id="detalle_fk_afc005_num_ubicacion_ant{$numero}" class="form-control input-sm" disabled>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion',$registro.fk_afc005_num_ubicacion_ant)}
                                                        </select>
                                                        <label for="detalle_fk_afc005_num_ubicacion_ant{$numero}">Ubicaci&oacute;n</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select name="detalle_fk_afc005_num_ubicacion[]" id="detalle_fk_afc005_num_ubicacion{$numero}" class="form-control input-sm" {$disabled.ver}>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion',$registro.fk_afc005_num_ubicacion)}
                                                        </select>
                                                        <label for="detalle_fk_afc005_num_ubicacion{$numero}">Ubicaci&oacute;n</label>
                                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group form-group-sm">
                                                        <input type="hidden" id="detalle_fk_a003_num_usuario_ant{$numero}" value="{$registro.fk_a003_num_usuario_ant}">
                                                        <input type="text" id="detalle_persona_usuario_ant{$numero}" value="{$registro.persona_usuario_ant}" class="form-control input-sm" disabled>
                                                        <label for="detalle_persona_usuario_ant{$numero}">Empleado Usuario</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-offset-1">
                                                    <div class="form-group form-group-sm">
                                                        <input type="hidden" name="detalle_fk_a003_num_usuario[]" id="detalle_fk_a003_num_usuario{$numero}" value="{$registro.fk_a003_num_usuario}">
                                                        <input type="text" name="detalle_persona_usuario[]" id="detalle_persona_usuario{$numero}" value="{$registro.persona_usuario}" class="form-control input-sm" disabled>
                                                        <label for="detalle_persona_usuario{$numero}">Empleado Usuario</label>
                                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Empleados"
                                                            onclick="selector($(this), '{$_Parametros.url}modAF/ListasCONTROL/selectorEmpleadosMET', ['detalle_fk_a003_num_usuario{$numero}','detalle_persona_usuario{$numero}']);" {$disabled.ver}>
                                                        <i class="md md-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group form-group-sm">
                                                        <input type="hidden" id="detalle_fk_a003_num_responsable_ant{$numero}" value="{$registro.fk_a003_num_responsable_ant}">
                                                        <input type="text" id="detalle_persona_responsable_ant{$numero}" value="{$registro.persona_responsable_ant}" class="form-control input-sm" maxlength="15" disabled>
                                                        <label for="detalle_persona_responsable_ant{$numero}">Empleado Responsable</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-offset-1">
                                                    <div class="form-group form-group-sm">
                                                        <input type="hidden" name="detalle_fk_a003_num_responsable[]" id="detalle_fk_a003_num_responsable{$numero}" value="{$registro.fk_a003_num_responsable}">
                                                        <input type="text" name="detalle_persona_responsable[]" id="detalle_persona_responsable{$numero}" value="{$registro.persona_responsable}" class="form-control input-sm" maxlength="15" disabled>
                                                        <label for="detalle_persona_responsable{$numero}">Empleado Responsable</label>
                                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Empleados"
                                                            onclick="selector($(this), '{$_Parametros.url}modAF/ListasCONTROL/selectorEmpleadosMET', ['detalle_fk_a003_num_responsable{$numero}','detalle_persona_responsable{$numero}']);" {$disabled.ver}>
                                                        <i class="md md-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select name="detalle_fk_afc009_num_movimiento_inc[]" id="detalle_fk_afc009_num_movimiento_inc{$numero}" class="form-control input-sm" {$disabled.ver}>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',$registro.fk_afc009_num_movimiento_inc)}
                                                        </select>
                                                        <label for="detalle_fk_afc009_num_movimiento_inc{$numero}">Tipo de Movimiento</label>
                                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group form-group-sm">
                                                        <select name="detalle_fk_afc009_num_movimiento_des[]" id="detalle_fk_afc009_num_movimiento_des{$numero}" class="form-control input-sm" {$disabled.ver}>
                                                            <option value="">&nbsp;</option>
                                                            {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',$registro.fk_afc009_num_movimiento_des)}
                                                        </select>
                                                        <label for="detalle_fk_afc009_num_movimiento_des{$numero}">Tipo de Movimiento</label>
                                                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    {if $form.metodo != ''}
        <div class="modal-footer">
            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
                <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
            </button>
            {if $form.metodo == 'anular'}
                <button type="button" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="accion">
                     <i class="fa fa-thumbs-down"></i> Anular
                </button>
            {elseif $form.metodo == 'revisar' || $form.metodo == 'aprobar'}
                <button type="button" class="btn btn-info ink-reaction btn-raised logsUsuarioModal" id="accion">
                     <i class="fa fa-thumbs-up"></i> Procesar
                </button>
            {else}
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                    <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                </button>
            {/if}
        </div>
    {/if}
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tamaño de ventana
        $('#modalAncho').css("width", "75%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('AF-01-02-01-01-02',$_Parametros.perfil)}&perfilAN={in_array('AF-01-02-01-01-03',$_Parametros.perfil)}", function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        /*var table = $('.datatable1').DataTable();
                        table.row.add(dato.tr).draw(false);
                        $('.datatable1 tbody tr').not('[id]').attr('id','id'+dato['id']);*/
                    }
                    else if (dato['status'] == 'modificar') {
                        //$('#id'+dato['id']).html(dato['tr']);
                    }
                    else if (dato['status'] == 'revisar') {
                        /*var table = $('.datatable1').DataTable();
                        table.row('#id'+dato['id']).remove().draw(false);*/
                        //$('#id'+dato['id']).remove();
                    }
                    else if (dato['status'] == 'aprobar') {
                        /*var table = $('.datatable1').DataTable();
                        table.row('#id'+dato['id']).remove().draw(false);*/
                        //$('#id'+dato['id']).remove();
                    }
                    cargarPagina($('#formFiltro'), "{$_Parametros.url}modAF/procesos/MovimientosCONTROL/");
                }
            }, 'json');
        });

        inicializar();
    });
</script>
