<form action="{$_Parametros.url}modAF/maestros/PolizaSeguroCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" name="pk_num_poliza_seguro" id="pk_num_poliza_seguro" value="{$form.pk_num_poliza_seguro}">
    <div class="modal-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-9">
                    <div class="form-group form-group-sm floating-label" id="ind_nombre_poliza_seguroError">
                        <input type="text" name="ind_nombre_poliza_seguro" id="ind_nombre_poliza_seguro" value="{$form.ind_nombre_poliza_seguro}" class="form-control" maxlength="50">
                        <label for="ind_nombre_poliza_seguro">Nombre</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm floating-label" id="fec_fecha_vencimientoError">
                        <input type="text" name="fec_fecha_vencimiento" id="fec_fecha_vencimiento" value="{Fecha::formatFecha($form.fec_fecha_vencimiento,'Y-m-d','d-m-Y')}" class="form-control date" maxlength="10">
                        <label for="fec_fecha_vencimiento">Fecha de Venc.</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label" id="ind_empresa_aseguradoraError">
                        <input type="text" name="ind_empresa_aseguradora" id="ind_empresa_aseguradora" value='{$form.ind_empresa_aseguradora}' class="form-control" maxlength="100">
                        <label for="ind_empresa_aseguradora">Empresa Aseguradora</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label" id="ind_agente_segurosError">
                        <input type="text" name="ind_agente_seguros" id="ind_agente_seguros" value="{$form.ind_agente_seguros}" class="form-control" maxlength="100">
                        <label for="ind_agente_seguros">Agente de Seguros</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group form-group-sm floating-label" id="num_monto_coberturaError">
                        <input type="text" name="num_monto_cobertura" id="num_monto_cobertura" value="{number_format($form.num_monto_cobertura,2,',','.')}" class="form-control money" style="text-align:right;">
                        <label for="num_monto_cobertura">Monto Cobertura</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
                <div class="col-sm-3 col-md-offset-6">
                    <div class="form-group form-group-sm floating-label" id="num_costo_polizaError">
                        <input type="text" name="num_costo_poliza" id="num_costo_poliza" value="{number_format($form.num_costo_poliza,2,',','.')}" class="form-control money" style="text-align:right;">
                        <label for="num_costo_poliza">Costo P&oacute;liza</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_estatus" id="num_estatus" value="1" {if $form.num_estatus==1}checked{/if}>
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
        </button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        inicializar();

        //  tama�o de ventana
        $('#modalAncho').css("width", "50%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('AF-01-90-02-03-02',$_Parametros.perfil)}&perfilE={in_array('AF-01-90-02-03-03',$_Parametros.perfil)}", function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        /*var table = $('.datatable1').DataTable();
                        table.row.add(dato.tr).draw(false);
                        $('.datatable1 tbody tr').not('[id]').attr('id','id'+dato['id']);*/
                    }
                    else if (dato['status'] == 'modificar') {
                        //$('#id'+dato['id']).html(dato['tr']);
                    }
                    cargarUrl("{$_Parametros.url}modAF/maestros/PolizaSeguroCONTROL");
                }
            }, 'json');
        });
    });
</script>
