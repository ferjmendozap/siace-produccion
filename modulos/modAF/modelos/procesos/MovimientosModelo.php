	<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Movimientos de Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |08-03-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class MovimientosModelo extends Modelo
{
    public function __construct()
    {
		parent::__construct();

		$this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($filtro=null)
    {
		$filtrar = "";
		if (isset($filtro['fnum_organismo'])) if ($filtro['fnum_organismo'] != "") $filtrar .= " AND m.fk_a001_num_organismo = '$filtro[fnum_organismo]'";
		if (isset($filtro['find_tipo'])) if ($filtro['find_tipo'] != "") $filtrar .= " AND m.ind_tipo = '$filtro[find_tipo]'";
		if (isset($filtro['fnum_motivo'])) if ($filtro['fnum_motivo'] != "") $filtrar .= " AND m.fk_a006_num_motivo = '$filtro[fnum_motivo]'";
		if (isset($filtro['ffec_fechad'])) if ($filtro['ffec_fechad'] != "") $filtrar .= " AND m.fec_fecha >= '".Fecha::formatFecha($filtro['ffec_fechad'])."'";
		if (isset($filtro['ffec_fechah'])) if ($filtro['ffec_fechah'] != "") $filtrar .= " AND m.fec_fecha <= '".Fecha::formatFecha($filtro['ffec_fechah'])."'";
		if (isset($filtro['find_estado'])) if ($filtro['find_estado'] != "") $filtrar .= " AND m.ind_estado = '$filtro[find_estado]'";

		$sql = "SELECT m.*
				FROM af_c012_movimiento m
				WHERE 1 $filtrar";
		$db = $this->_db->query($sql);
		$db->setFetchMode(PDO::FETCH_ASSOC);
		return $db->fetchAll();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
		##  Inicia una transacción
		$this->_db->beginTransaction();

		$fk_a006_num_tipo_acta = Select::miscelaneo('AFTIPOACTA', 'AE', 2);
		$num_anio = substr($data['fec_fecha'], 0, 4);
		$ind_nro_acta = Number::codigo('af_c012_movimiento', 'ind_nro_acta', 5, ['num_anio'=>$num_anio, 'fk_a006_num_tipo_acta'=>$fk_a006_num_tipo_acta]);

		##  datos generales
		$sql = "INSERT INTO af_c012_movimiento
				SET
					fk_a001_num_organismo = '$data[fk_a001_num_organismo]',
					fec_fecha = '$data[fec_fecha]',
					fk_a006_num_tipo_acta = '$fk_a006_num_tipo_acta',
					num_anio = '$num_anio',
					ind_nro_acta = '$ind_nro_acta',
					ind_tipo = '$data[ind_tipo]',
					fk_a006_num_motivo = '$data[fk_a006_num_motivo]',
					ind_comentarios = '$data[ind_comentarios]',
					fk_a003_num_preparado_por = '$this->idUsuario',
					fecha_preparado = NOW(),
					ind_estado = '$data[ind_estado]',
					fk_a018_num_seguridad_usuario = '$this->idUsuario',
					fec_ultima_modificacion = NOW()";
		$db = $this->_db->prepare($sql);
		$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
		##
		$id = $this->_db->lastInsertId();
		##  detalles
		for ($i=0; $i < count($data['detalle_fk_afb001_num_activo']); $i++) {
			$secuencia = $i + 1;
			$pk_num_movimiento_detalle = $id . $secuencia;
			##
			$sql = "SELECT * FROM af_b001_activo WHERE pk_num_activo = '".$data['detalle_fk_afb001_num_activo'][$i]."'";
			$query = $this->_db->query($sql);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$field = $query->fetch();
			##	
			$sql = "INSERT INTO af_c023_movimiento_detalle
					SET
						pk_num_movimiento_detalle = '$pk_num_movimiento_detalle',
						fk_afc012_num_movimiento = '$id',
						fk_afb001_num_activo = '".$data['detalle_fk_afb001_num_activo'][$i]."',
						fk_a023_num_centro_costo_ant = '".$field['fk_a023_num_centro_costo']."',
						fk_afc005_num_ubicacion_ant = '".$field['fk_afc005_num_ubicacion']."',
						fk_a003_num_usuario_ant = '".$field['fk_a003_num_persona_usuario']."',
						fk_a003_num_responsable_ant = '".$field['fk_a003_num_persona_responsable']."',
						fk_a023_num_centro_costo = '".$data['detalle_fk_a023_num_centro_costo'][$i]."',
						fk_afc005_num_ubicacion = '".$data['detalle_fk_afc005_num_ubicacion'][$i]."',
						fk_a003_num_usuario = '".$data['detalle_fk_a003_num_usuario'][$i]."',
						fk_a003_num_responsable = '".$data['detalle_fk_a003_num_responsable'][$i]."',
						fk_afc009_num_movimiento_inc = '".$data['detalle_fk_afc009_num_movimiento_inc'][$i]."',
						fk_afc009_num_movimiento_des = '".$data['detalle_fk_afc009_num_movimiento_des'][$i]."'";
			$db = $this->_db->prepare($sql);
			$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
		}

		##  Consigna una transacción
		$this->_db->commit();

		return $id;
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $id = $data['pk_num_movimiento'];

        ##  datos generales
        $sql = "UPDATE af_c012_movimiento
				SET
					fec_fecha = '$data[fec_fecha]',
					ind_tipo = '$data[ind_tipo]',
					fk_a006_num_motivo = '$data[fk_a006_num_motivo]',
					ind_comentarios = '$data[ind_comentarios]',
					ind_estado = '$data[ind_estado]',
					fk_a018_num_seguridad_usuario = '$this->idUsuario',
					fec_ultima_modificacion = NOW()
				WHERE pk_num_movimiento = '$id'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        ##  detalles
        $db = $this->_db->prepare("DELETE FROM af_c023_movimiento_detalle WHERE fk_afc012_num_movimiento = '$id'");
        $db->execute();
        for ($i=0; $i < count($data['detalle_fk_afb001_num_activo']); $i++) {
			$secuencia = $i + 1;
			$pk_num_movimiento_detalle = $id . $secuencia;
			##
			$sql = "SELECT * FROM af_b001_activo WHERE pk_num_activo = '".$data['detalle_fk_afb001_num_activo'][$i]."'";
			$query = $this->_db->query($sql);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$field = $query->fetch();
			##
			$sql = "INSERT INTO af_c023_movimiento_detalle
					SET
						pk_num_movimiento_detalle = '$pk_num_movimiento_detalle',
						fk_afc012_num_movimiento = '$id',
						fk_afb001_num_activo = '".$data['detalle_fk_afb001_num_activo'][$i]."',
						fk_a023_num_centro_costo_ant = '".$field['fk_a023_num_centro_costo']."',
						fk_afc005_num_ubicacion_ant = '".$field['fk_afc005_num_ubicacion']."',
						fk_a003_num_usuario_ant = '".$field['fk_a003_num_persona_usuario']."',
						fk_a003_num_responsable_ant = '".$field['fk_a003_num_persona_responsable']."',
						fk_a023_num_centro_costo = '".$data['detalle_fk_a023_num_centro_costo'][$i]."',
						fk_afc005_num_ubicacion = '".$data['detalle_fk_afc005_num_ubicacion'][$i]."',
						fk_a003_num_usuario = '".$data['detalle_fk_a003_num_usuario'][$i]."',
						fk_a003_num_responsable = '".$data['detalle_fk_a003_num_responsable'][$i]."',
						fk_afc009_num_movimiento_inc = '".$data['detalle_fk_afc009_num_movimiento_inc'][$i]."',
						fk_afc009_num_movimiento_des = '".$data['detalle_fk_afc009_num_movimiento_des'][$i]."'";
			$db = $this->_db->prepare($sql);
			$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        }

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metAnular($data)
    {
		##  Inicia una transacción
		$this->_db->beginTransaction();

		$id = $data['pk_num_movimiento'];

		##  registro
		$sql = "SELECT * FROM af_c012_movimiento WHERE pk_num_movimiento = '$id'";
		$query = $this->_db->query($sql);
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$field = $query->fetch();
		if ($field['ind_estado'] == 'AN') die(json_encode(['status'=>'error', 'mensaje'=>['No puede Anular un registro Anulado']]));
		elseif ($field['ind_estado'] == 'AP' || $field['ind_estado'] == 'RV') $ind_estado = "PR";
		elseif ($field['ind_estado'] == 'PR') $ind_estado = "AN";

		##  datos generales
		$sql = "UPDATE af_c012_movimiento
				SET
					ind_estado = 'AN',
					fk_a003_num_anulado_por = '$this->idUsuario',
					fecha_anulado = NOW(),
					fk_a018_num_seguridad_usuario = '$this->idUsuario',
					fec_ultima_modificacion = NOW()
				WHERE pk_num_movimiento = '$id'";
		$db = $this->_db->prepare($sql);
		$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

		##  Consigna una transacción
		$this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metRevisar($data)
    {
		##  Inicia una transacción
		$this->_db->beginTransaction();

		$id = $data['pk_num_movimiento'];

		##  registro
		$sql = "SELECT * FROM af_c012_movimiento WHERE pk_num_movimiento = '$id'";
		$query = $this->_db->query($sql);
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$field = $query->fetch();
		if ($field['ind_estado'] <> 'PR') die(json_encode(['status'=>'error', 'mensaje'=>['No puede Revisar un registro '.Select::options('af_estado_movimiento', $field['ind_estado'], 2)]]));

		##  datos generales
		$sql = "UPDATE af_c012_movimiento
				SET
					ind_estado = 'RV',
					fk_a003_num_revisado_por = '$this->idUsuario',
					fecha_revisado = NOW(),
					fk_a018_num_seguridad_usuario = '$this->idUsuario',
					fec_ultima_modificacion = NOW()
				WHERE pk_num_movimiento = '$id'";
		$db = $this->_db->prepare($sql);
		$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

		##  Consigna una transacción
		$this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metAprobar($data)
    {
		##  Inicia una transacción
		$this->_db->beginTransaction();

		$id = $data['pk_num_movimiento'];

		##  datos generales
		$sql = "UPDATE af_c012_movimiento
				SET
					ind_estado = 'AP',
					fk_a003_num_aprobado_por = '$this->idUsuario',
					fecha_aprobado = NOW(),
					fk_a018_num_seguridad_usuario = '$this->idUsuario',
					fec_ultima_modificacion = NOW()
				WHERE pk_num_movimiento = '$id'";
		$db = $this->_db->prepare($sql);
		$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

		$fm = $this->metObtener($id);

		##	detalle
		$lista_detalle = $this->metObtenerActivos($id);
		foreach ($lista_detalle as $f) {
	        ##  usuario
	        $sql = "SELECT el.fk_rhc063_num_puestos_cargo
	                FROM
	                  a003_persona p
	                  INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona)
	                  INNER JOIN rh_c005_empleado_laboral el ON (el.fk_rhb001_num_empleado = e.pk_num_empleado)
	                WHERE p.pk_num_persona = '$this->idUsuario'";
	        $row_usuario = $this->_db->query($sql);
	        $row_usuario->setFetchMode(PDO::FETCH_ASSOC);
	        $field_usuario = $row_usuario->fetch();

	        ##  conformado
	        $conformado_por = Select::parametros('CONFAFDEF');
	        $sql = "SELECT el.fk_rhc063_num_puestos_cargo
	                FROM
	                  a003_persona p
	                  INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona)
	                  INNER JOIN rh_c005_empleado_laboral el ON (el.fk_rhb001_num_empleado = e.pk_num_empleado)
	                WHERE p.pk_num_persona = '$conformado_por'";
	        $row_conformado = $this->_db->query($sql);
	        $row_conformado->setFetchMode(PDO::FETCH_ASSOC);
	        $field_conformado = $row_conformado->fetch();

			##	activo
			$sql = "UPDATE af_b001_activo
					SET
						fk_a023_num_centro_costo = '$f[fk_a023_num_centro_costo]',
						fk_afc005_num_ubicacion = '$f[fk_afc005_num_ubicacion]',
						fk_a003_num_persona_responsable = '$f[fk_a003_num_responsable]',
						fk_a003_num_persona_usuario = '$f[fk_a003_num_usuario]',
						fk_afc009_num_movimiento = '$f[fk_afc009_num_movimiento_des]'
					WHERE pk_num_activo = '$f[fk_afb001_num_activo]'";
			$db = $this->_db->prepare($sql);
			$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

			##	acta de entrega
	        $num_anio = date('Y');
	        $ind_nro_acta = Number::codigo('af_c028_acta_entrega', 'ind_nro_acta', 5, ['num_anio'=>$num_anio]);
	        $sql = "INSERT INTO af_c028_acta_entrega
	                SET
	                  fk_afb001_num_activo = '$f[fk_afb001_num_activo]',
	                  fk_a003_num_aprobado_por = '$this->idUsuario',
	                  fk_rhc063_num_aprobado_por_puesto = '$field_usuario[fk_rhc063_num_puestos_cargo]',
	                  fk_a003_num_conformado_por = '$conformado_por',
	                  fk_rhc063_num_conformado_por_puesto = '$field_conformado[fk_rhc063_num_puestos_cargo]',
	                  fk_a004_num_dependencia = '$f[fk_a004_num_dependencia]',
	                  num_anio = '$num_anio',
	                  ind_nro_acta = '$ind_nro_acta',
	                  fk_a018_num_seguridad_usuario = '$this->idUsuario',
	                  fec_ultima_modificacion = NOW()";
	        $db = $this->_db->prepare($sql);
	        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

	        ##  historico
	        $sql = "INSERT INTO af_c029_activo_historico
	                SET
	                    fk_afb001_num_activo = '$f[fk_afb001_num_activo]',
	                    fk_a023_num_centro_costo = '$f[fk_a023_num_centro_costo]',
	                    fk_afc007_num_situacion = '$f[fk_afc007_num_situacion]',
	                    fk_afc009_num_movimiento = '$f[fk_afc009_num_movimiento_des]',
	                    fk_afc005_num_ubicacion = '$f[fk_afc005_num_ubicacion]',
	                    fk_a006_num_motivo = '$fm[fk_a006_num_motivo]',
	                    cod_codigo_interno = '$f[cod_codigo_interno]',
	                    fec_fecha_ingreso = '$f[fec_ingreso]',
	                    fec_fecha_transaccion = '$fm[fec_fecha]',
	                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
	                    fec_ultima_modificacion = NOW()";
	        $db = $this->_db->prepare($sql);
	        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
		}

		##  Consigna una transacción
		$this->_db->commit();
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
		$sql = "SELECT
					m.*,
					o.ind_descripcion_empresa AS organismo,
					o.ind_direccion AS organismo_direccion
				FROM
					af_c012_movimiento m
					INNER JOIN a001_organismo o ON (o.pk_num_organismo = m.fk_a001_num_organismo)
				WHERE m.pk_num_movimiento = '$id'";
		$row = $this->_db->query($sql);
		$row->setFetchMode(PDO::FETCH_ASSOC);
		return $row->fetch();
    }

    /**
     * Método para obtener los activos afectados.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerActivos($fk_afc012_num_movimiento)
    {
		$sql = "SELECT
					md.*,
					a.cod_codigo_interno AS cod_activo,
					a.ind_descripcion AS desc_activo,
					a.ind_serie,
					a.ind_modelo,
					a.fk_afc007_num_situacion,
					a.cod_codigo_interno,
					a.fec_ingreso,
					c.ind_codigo AS cod_clasificacion,
					cc1.fk_a004_num_dependencia,
					cc2.fk_a004_num_dependencia AS fk_a004_num_dependencia_ant,
					cc1.ind_abreviatura AS cod_centro_costo,
					cc2.ind_abreviatura AS cod_centro_costo_ant,
					cc1.ind_descripcion_centro_costo AS centro_costo,
					cc2.ind_descripcion_centro_costo AS centro_costo_ant,
					d1.fk_a001_num_organismo,
					d2.fk_a001_num_organismo AS fk_a001_num_organismo_ant,
					d1.ind_codinterno AS cod_dependencia,
					d2.ind_codinterno AS cod_dependencia_ant,
					d1.ind_dependencia AS dependencia,
					d2.ind_dependencia AS dependencia_ant,
					u1.ind_codigo AS cod_ubicacion,
					u2.ind_codigo AS cod_ubicacion_ant,
					u1.ind_nombre_ubicacion AS ubicacion,
					u2.ind_nombre_ubicacion AS ubicacion_ant,
					CONCAT_WS(' ', pu1.ind_nombre1, pu1.ind_nombre2, pu1.ind_apellido1, pu1.ind_apellido2) AS persona_usuario,
					CONCAT_WS(' ', pu2.ind_nombre1, pu2.ind_nombre2, pu2.ind_apellido1, pu2.ind_apellido2) AS persona_usuario_ant,
					CONCAT_WS(' ', pr1.ind_nombre1, pr1.ind_nombre2, pr1.ind_apellido1, pr1.ind_apellido2) AS persona_responsable,
					CONCAT_WS(' ', pr2.ind_nombre1, pr2.ind_nombre2, pr2.ind_apellido1, pr2.ind_apellido2) AS persona_responsable_ant,
					misc.ind_nombre_detalle AS marca
				FROM
					af_c023_movimiento_detalle md
					INNER JOIN af_b001_activo a ON (a.pk_num_activo = md.fk_afb001_num_activo)
					INNER JOIN a023_centro_costo cc1 ON (cc1.pk_num_centro_costo = md.fk_a023_num_centro_costo)
					INNER JOIN a023_centro_costo cc2 ON (cc2.pk_num_centro_costo = md.fk_a023_num_centro_costo_ant)
					INNER JOIN a004_dependencia d1 ON (d1.pk_num_dependencia = cc1.fk_a004_num_dependencia)
					INNER JOIN a004_dependencia d2 ON (d2.pk_num_dependencia = cc2.fk_a004_num_dependencia)
					INNER JOIN a003_persona pu1 ON (pu1.pk_num_persona = md.fk_a003_num_usuario)
					INNER JOIN a003_persona pu2 ON (pu2.pk_num_persona = md.fk_a003_num_usuario_ant)
					INNER JOIN a003_persona pr1 ON (pr1.pk_num_persona = md.fk_a003_num_responsable)
					INNER JOIN a003_persona pr2 ON (pr2.pk_num_persona = md.fk_a003_num_responsable_ant)
					INNER JOIN af_c005_ubicacion u1 ON (u1.pk_num_ubicacion = md.fk_afc005_num_ubicacion)
					INNER JOIN af_c005_ubicacion u2 ON (u2.pk_num_ubicacion = md.fk_afc005_num_ubicacion_ant)
					INNER JOIN af_c008_clasificacion c ON (c.pk_num_clasificacion = a.fk_afc008_num_clasificacion)
					LEFT JOIN a006_miscelaneo_detalle misc ON (misc.pk_num_miscelaneo_detalle = a.fk_a006_num_marca)
				WHERE md.fk_afc012_num_movimiento = '$fk_afc012_num_movimiento'";
		$db = $this->_db->query($sql);
		$db->setFetchMode(PDO::FETCH_ASSOC);
		return $db->fetchAll();
    }
}
