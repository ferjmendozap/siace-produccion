<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Categoria de Depreciación
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |17-11-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class CategoriaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar()
    {
        $db = $this->_db->query(
            "SELECT * FROM af_c019_categoria"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
        $sql = "SELECT
                    c.*,
                    pc1.cod_cuenta AS num_cuenta_historica,
                    pc2.cod_cuenta AS num_cuenta_historica_variacion,
                    pc3.cod_cuenta AS num_cuenta_historica_revaluacion,
                    pc4.cod_cuenta AS num_cuenta_depreciacion,
                    pc5.cod_cuenta AS num_cuenta_depreciacion_variacion,
                    pc6.cod_cuenta AS num_cuenta_depreciacion_revaluacion,
                    pc7.cod_cuenta AS num_cuenta_gastos,
                    pc8.cod_cuenta AS num_cuenta_gastos_revaluacion,
                    pc9.cod_cuenta AS num_cuenta_neto,
                    pc10.cod_cuenta AS num_cuenta_rei,
                    pc11.cod_cuenta AS num_cuenta_resultado
                 FROM
                    af_c019_categoria c
                    INNER JOIN cb_b004_plan_cuenta pc1 ON (pc1.pk_num_cuenta = c.fk_cbb004_num_cuenta_historica)
                    INNER JOIN cb_b004_plan_cuenta pc2 ON (pc2.pk_num_cuenta = c.fk_cbb004_num_cuenta_historica_variacion)
                    INNER JOIN cb_b004_plan_cuenta pc3 ON (pc3.pk_num_cuenta = c.fk_cbb004_num_cuenta_historica_revaluacion)
                    INNER JOIN cb_b004_plan_cuenta pc4 ON (pc4.pk_num_cuenta = c.fk_cbb004_num_cuenta_depreciacion)
                    INNER JOIN cb_b004_plan_cuenta pc5 ON (pc5.pk_num_cuenta = c.fk_cbb004_num_cuenta_depreciacion_variacion)
                    INNER JOIN cb_b004_plan_cuenta pc6 ON (pc6.pk_num_cuenta = c.fk_cbb004_num_cuenta_depreciacion_revaluacion)
                    INNER JOIN cb_b004_plan_cuenta pc7 ON (pc7.pk_num_cuenta = c.fk_cbb004_num_cuenta_gastos)
                    INNER JOIN cb_b004_plan_cuenta pc8 ON (pc8.pk_num_cuenta = c.fk_cbb004_num_cuenta_gastos_revaluacion)
                    INNER JOIN cb_b004_plan_cuenta pc9 ON (pc9.pk_num_cuenta = c.fk_cbb004_num_cuenta_neto)
                    INNER JOIN cb_b004_plan_cuenta pc10 ON (pc10.pk_num_cuenta = c.fk_cbb004_num_cuenta_rei)
                    INNER JOIN cb_b004_plan_cuenta pc11 ON (pc11.pk_num_cuenta = c.fk_cbb004_num_cuenta_resultado)
                 WHERE pk_num_categoria = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    /**
     * Método para obtener las contabilidades asociadas al registro.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerContabilidades($fk_afc019_num_categoria)
    {
        $db = $this->_db->query(
            "SELECT * FROM af_c020_categoria_contabilidad WHERE fk_afc019_num_categoria = '$fk_afc019_num_categoria'"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        //  registro
        $sql = "INSERT INTO af_c019_categoria
                SET
					ind_codigo = '$data[ind_codigo]',
					ind_nombre_categoria = '$data[ind_nombre_categoria]',
					fk_cbb004_num_cuenta_historica = '$data[fk_cbb004_num_cuenta_historica]',
					fk_cbb004_num_cuenta_historica_variacion = '$data[fk_cbb004_num_cuenta_historica_variacion]',
					fk_cbb004_num_cuenta_historica_revaluacion = '$data[fk_cbb004_num_cuenta_historica_revaluacion]',
					fk_cbb004_num_cuenta_depreciacion = '$data[fk_cbb004_num_cuenta_depreciacion]',
					fk_cbb004_num_cuenta_depreciacion_variacion = '$data[fk_cbb004_num_cuenta_depreciacion_variacion]',
					fk_cbb004_num_cuenta_depreciacion_revaluacion = '$data[fk_cbb004_num_cuenta_depreciacion_revaluacion]',
					fk_cbb004_num_cuenta_gastos = '$data[fk_cbb004_num_cuenta_gastos]',
					fk_cbb004_num_cuenta_gastos_revaluacion = '$data[fk_cbb004_num_cuenta_gastos_revaluacion]',
					fk_cbb004_num_cuenta_neto = '$data[fk_cbb004_num_cuenta_neto]',
					fk_cbb004_num_cuenta_rei = '$data[fk_cbb004_num_cuenta_rei]',
					fk_cbb004_num_cuenta_resultado = '$data[fk_cbb004_num_cuenta_resultado]',
					num_flag_inventariable = '$data[num_flag_inventariable]',
					fk_a006_num_tipo = '$data[fk_a006_num_tipo]',
					fk_a006_num_grupo = '$data[fk_a006_num_grupo]',
					num_estatus = '$data[num_estatus]',
					fk_a018_num_seguridad_usuario = '$this->idUsuario',
					fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        $id = $this->_db->lastInsertId();

        //  contabilidades
        if (isset($data['contabilidades_fk_cbb005_num_contabilidades'])) {
			for ($i=0; $i < count($data['contabilidades_fk_cbb005_num_contabilidades']); $i++) {
				##  consulto si ya existe
				if (count($data['contabilidades_fk_cbb005_num_contabilidades']) != count(array_unique($data['contabilidades_fk_cbb005_num_contabilidades'])))
				{
					die(json_encode(['status'=>'error', 'mensaje'=>['Se encontraron contabilidades repetidas en la lista de Contabilidades Validas']]));
				}  
				/*$sql = "SELECT *
				        FROM af_c020_categoria_contabilidad
				        WHERE
				          fk_afc019_num_categoria = '$id' AND
				          fk_cbb005_num_contabilidades = '".$data['contabilidades_fk_cbb005_num_contabilidades'][$i]."'";
				$row = $this->_db->query($sql);
				if ($row->rowCount() > 0) die(json_encode(['status'=>'error', 'mensaje'=>['Se encontraron contabilidades repetidas en la lista de Contabilidades Válidas']]));*/

				##  registro
				$sql = "INSERT INTO af_c020_categoria_contabilidad
				        SET
							fk_afc019_num_categoria = '$id',
							fk_cbb005_num_contabilidades = '".$data['contabilidades_fk_cbb005_num_contabilidades'][$i]."',
							num_depreciacion = '".$data['contabilidades_num_depreciacion'][$i]."'";
				$db = $this->_db->prepare($sql);
				$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
			}
        }

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $id = $data['pk_num_categoria'];

        //  registro
        $sql = "UPDATE af_c019_categoria
                SET
					ind_codigo = '$data[ind_codigo]',
					ind_nombre_categoria = '$data[ind_nombre_categoria]',
					fk_cbb004_num_cuenta_historica = '$data[fk_cbb004_num_cuenta_historica]',
					fk_cbb004_num_cuenta_historica_variacion = '$data[fk_cbb004_num_cuenta_historica_variacion]',
					fk_cbb004_num_cuenta_historica_revaluacion = '$data[fk_cbb004_num_cuenta_historica_revaluacion]',
					fk_cbb004_num_cuenta_depreciacion = '$data[fk_cbb004_num_cuenta_depreciacion]',
					fk_cbb004_num_cuenta_depreciacion_variacion = '$data[fk_cbb004_num_cuenta_depreciacion_variacion]',
					fk_cbb004_num_cuenta_depreciacion_revaluacion = '$data[fk_cbb004_num_cuenta_depreciacion_revaluacion]',
					fk_cbb004_num_cuenta_gastos = '$data[fk_cbb004_num_cuenta_gastos]',
					fk_cbb004_num_cuenta_gastos_revaluacion = '$data[fk_cbb004_num_cuenta_gastos_revaluacion]',
					fk_cbb004_num_cuenta_neto = '$data[fk_cbb004_num_cuenta_neto]',
					fk_cbb004_num_cuenta_rei = '$data[fk_cbb004_num_cuenta_rei]',
					fk_cbb004_num_cuenta_resultado = '$data[fk_cbb004_num_cuenta_resultado]',
					num_flag_inventariable = '$data[num_flag_inventariable]',
					fk_a006_num_tipo = '$data[fk_a006_num_tipo]',
					fk_a006_num_grupo = '$data[fk_a006_num_grupo]',
					num_estatus = '$data[num_estatus]',
					fk_a018_num_seguridad_usuario = '$this->idUsuario',
					fec_ultima_modificacion = NOW()
                WHERE pk_num_categoria = '$data[pk_num_categoria]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        //  contabilidades
        if (isset($data['contabilidades_fk_cbb005_num_contabilidades'])) 
        {
			$sql = "DELETE FROM af_c020_categoria_contabilidad
					WHERE
						fk_afc019_num_categoria = '$id'
						AND pk_num_categoria_contabilidad NOT IN (".implode(",",$data['contabilidades_pk_num_categoria_contabilidad']).")";
			$db = $this->_db->prepare($sql);
			$db->execute();
			##  
			for ($i=0; $i < count($data['contabilidades_fk_cbb005_num_contabilidades']); $i++) {
				##  consulto si ya existe
				if (count($data['contabilidades_fk_cbb005_num_contabilidades']) != count(array_unique($data['contabilidades_fk_cbb005_num_contabilidades'])))
				{
					die(json_encode(['status'=>'error', 'mensaje'=>['Se encontraron contabilidades repetidas en la lista de Contabilidades Validas']]));
				}
				/*$sql = "SELECT *
				        FROM af_c020_categoria_contabilidad
				        WHERE
				          fk_afc019_num_categoria = '$id' AND
				          fk_cbb005_num_contabilidades = '".$data['contabilidades_fk_cbb005_num_contabilidades'][$i]."'";
				$row = $this->_db->query($sql);
				if ($row->rowCount() > 0) die(json_encode(['status'=>'error', 'mensaje'=>['Se encontraron contabilidades repetidas en la lista de Contabilidades Validas']]));*/


				##  registro
				$sql = "REPLACE INTO af_c020_categoria_contabilidad
				        SET
							pk_num_categoria_contabilidad = '".$data['contabilidades_pk_num_categoria_contabilidad'][$i]."',
							fk_afc019_num_categoria = '$id',
							fk_cbb005_num_contabilidades = '".$data['contabilidades_fk_cbb005_num_contabilidades'][$i]."',
							num_depreciacion = '".$data['contabilidades_num_depreciacion'][$i]."'";
				$db = $this->_db->prepare($sql);
				$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
			}
        }
        else
        {
			$sql = "DELETE FROM af_c020_categoria_contabilidad WHERE fk_afc019_num_categoria = '$id'";
			$db = $this->_db->prepare($sql);
			$db->execute();
        }

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para eliminar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "DELETE FROM af_c019_categoria WHERE pk_num_categoria = '$data[pk_num_categoria]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }
}
