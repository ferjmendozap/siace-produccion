<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Activos Fijos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Edgar Bolívar        | bedgar3000@gmail.com                 | 0424-9201982
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
trait dataAFMiscelaneo
{

      /**
      * Miscelaneo
      *
      * @return Array
      */
      public function metAFMiscelaneo()
      {
            $data = array(
                  array('ind_nombre_maestro' => 'TIPO DE MOVIMIENTO ACTIVO FIJO', 'ind_descripcion' => 'TIPO DE MOVIMIENTO ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPOMOV',
                        'Det' => array(
                              array('cod_detalle' => 'I', 'ind_nombre_detalle' => 'INCORPORACIÓN', 'num_estatus' => '1'),
                              array('cod_detalle' => 'D', 'ind_nombre_detalle' => 'DESINCORPORACIÓN', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'TIPO DE DEPRECIACIÓN ACTIVO FIJO', 'ind_descripcion' => 'TIPO DE DEPRECIACIÓN ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPODEPR',
                        'Det' => array(
                              array('cod_detalle' => 'LI', 'ind_nombre_detalle' => 'LINEAL', 'num_estatus' => '1'),
                              array('cod_detalle' => 'PU', 'ind_nombre_detalle' => 'POR UNIDADES', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'GRUPO CATEGORIA ACTIVO FIJO', 'ind_descripcion' => 'GRUPO CATEGORIA ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFGRUPCAT',
                        'Det' => array(
                              array('cod_detalle' => 'AM', 'ind_nombre_detalle' => 'ACTIVO MENOR', 'num_estatus' => '1'),
                              array('cod_detalle' => 'AN', 'ind_nombre_detalle' => 'ACTIVO NORMAL', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'ORIGEN DEL ACTIVO', 'ind_descripcion' => 'ORIGEN DEL ACTIVO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFORIGENAC',
                        'Det' => array(
                              array('cod_detalle' => 'MA', 'ind_nombre_detalle' => 'MANUAL', 'num_estatus' => '1'),
                              array('cod_detalle' => 'AT', 'ind_nombre_detalle' => 'AUTOMÁTICO', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'TIPO DE ACTIVO', 'ind_descripcion' => 'TIPO DE ACTIVO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPOACT',
                        'Det' => array(
                              array('cod_detalle' => 'I', 'ind_nombre_detalle' => 'INDIVIDUAL', 'num_estatus' => '1'),
                              array('cod_detalle' => 'P', 'ind_nombre_detalle' => 'PRINCIPAL', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'TIPO DE MEJORA DEL ACTIVO FIJO', 'ind_descripcion' => 'TIPO DE MEJORA DEL ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPOMEJ',
                        'Det' => array(
                              array('cod_detalle' => 'NA', 'ind_nombre_detalle' => 'NO APLICABLE', 'num_estatus' => '1'),
                              array('cod_detalle' => 'AD', 'ind_nombre_detalle' => 'ADICIÓN', 'num_estatus' => '1'),
                              array('cod_detalle' => 'RV', 'ind_nombre_detalle' => 'REVALUACIÓN', 'num_estatus' => '1'),
                              array('cod_detalle' => 'VO', 'ind_nombre_detalle' => 'VOLUNTARIA', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'ESTADO DE ACONSERVACIÓN DEL ACTIVO FIJO', 'ind_descripcion' => 'ESTADO DE ACONSERVACIÓN DEL ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFESTCONS',
                        'Det' => array(
                              array('cod_detalle' => 'B', 'ind_nombre_detalle' => 'BUENO', 'num_estatus' => '1'),
                              array('cod_detalle' => 'M', 'ind_nombre_detalle' => 'MALO', 'num_estatus' => '1'),
                              array('cod_detalle' => 'O', 'ind_nombre_detalle' => 'OBSOLETO', 'num_estatus' => '1'),
                              array('cod_detalle' => 'R', 'ind_nombre_detalle' => 'REGULAR', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'MARCAS', 'ind_descripcion' => 'MARCAS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MARCAS',
                        'Det' => array(
                              array('cod_detalle' => '1', 'ind_nombre_detalle' => 'SAMSUNG', 'num_estatus' => '1'),
                              array('cod_detalle' => '2', 'ind_nombre_detalle' => 'LIFE´S GOOD (LG)', 'num_estatus' => '1'),
                              array('cod_detalle' => '3', 'ind_nombre_detalle' => 'GENERAL ELECTRIC', 'num_estatus' => '1'),
                              array('cod_detalle' => '4', 'ind_nombre_detalle' => 'PANASONIC', 'num_estatus' => '1'),
                              array('cod_detalle' => '5', 'ind_nombre_detalle' => 'PHILLIPS', 'num_estatus' => '1'),
                              array('cod_detalle' => '6', 'ind_nombre_detalle' => 'ADMIRAL', 'num_estatus' => '1'),
                              array('cod_detalle' => '7', 'ind_nombre_detalle' => 'BLACK-DECKER', 'num_estatus' => '1'),
                              array('cod_detalle' => '8', 'ind_nombre_detalle' => 'BOSCH', 'num_estatus' => '1'),
                              array('cod_detalle' => '9', 'ind_nombre_detalle' => 'COMPAQ', 'num_estatus' => '1'),
                              array('cod_detalle' => '10', 'ind_nombre_detalle' => 'CANON', 'num_estatus' => '1'),
                              array('cod_detalle' => '11', 'ind_nombre_detalle' => 'DAEWOO', 'num_estatus' => '1'),
                              array('cod_detalle' => '12', 'ind_nombre_detalle' => 'ELECTROLUX', 'num_estatus' => '1'),
                              array('cod_detalle' => '13', 'ind_nombre_detalle' => 'EPSON', 'num_estatus' => '1'),
                              array('cod_detalle' => '14', 'ind_nombre_detalle' => 'GREE', 'num_estatus' => '1'),
                              array('cod_detalle' => '15', 'ind_nombre_detalle' => 'HEWLETT-PACKARD (HP)', 'num_estatus' => '1'),
                              array('cod_detalle' => '16', 'ind_nombre_detalle' => 'LENOVO', 'num_estatus' => '1'),
                              array('cod_detalle' => '17', 'ind_nombre_detalle' => 'LUFERCA', 'num_estatus' => '1'),
                              array('cod_detalle' => '18', 'ind_nombre_detalle' => 'MABE', 'num_estatus' => '1'),
                              array('cod_detalle' => '19', 'ind_nombre_detalle' => 'OSTER', 'num_estatus' => '1'),
                              array('cod_detalle' => '20', 'ind_nombre_detalle' => 'REGINA', 'num_estatus' => '1'),
                              array('cod_detalle' => '21', 'ind_nombre_detalle' => 'SONY', 'num_estatus' => '1'),
                              array('cod_detalle' => '22', 'ind_nombre_detalle' => 'TEKA', 'num_estatus' => '1'),
                              array('cod_detalle' => '23', 'ind_nombre_detalle' => 'TOSHIBA', 'num_estatus' => '1'),
                              array('cod_detalle' => '24', 'ind_nombre_detalle' => 'XEROX', 'num_estatus' => '1'),
                              array('cod_detalle' => '25', 'ind_nombre_detalle' => 'NEC', 'num_estatus' => '1'),
                              array('cod_detalle' => '26', 'ind_nombre_detalle' => 'PLANTRONICS', 'num_estatus' => '1'),
                              array('cod_detalle' => '27', 'ind_nombre_detalle' => 'HP', 'num_estatus' => '1'),
                              array('cod_detalle' => '28', 'ind_nombre_detalle' => 'FRIGILUX', 'num_estatus' => '1'),
                              array('cod_detalle' => '29', 'ind_nombre_detalle' => 'CASIO', 'num_estatus' => '1'),
                              array('cod_detalle' => '30', 'ind_nombre_detalle' => 'CANYON', 'num_estatus' => '1'),
                              array('cod_detalle' => '31', 'ind_nombre_detalle' => 'TRIPP-LITE', 'num_estatus' => '1'),
                              array('cod_detalle' => '32', 'ind_nombre_detalle' => 'GENIUS', 'num_estatus' => '1'),
                              array('cod_detalle' => '33', 'ind_nombre_detalle' => 'KEYTON', 'num_estatus' => '1'),
                              array('cod_detalle' => '34', 'ind_nombre_detalle' => 'SHARP', 'num_estatus' => '1'),
                              array('cod_detalle' => '35', 'ind_nombre_detalle' => 'CYBERLUX', 'num_estatus' => '1'),
                              array('cod_detalle' => '36', 'ind_nombre_detalle' => 'FORZA', 'num_estatus' => '1'),
                              array('cod_detalle' => '37', 'ind_nombre_detalle' => 'TOPFLO', 'num_estatus' => '1'),
                              array('cod_detalle' => '38', 'ind_nombre_detalle' => 'SMART COOR', 'num_estatus' => '1'),
                              array('cod_detalle' => '39', 'ind_nombre_detalle' => 'TAURUS', 'num_estatus' => '1'),
                              array('cod_detalle' => '40', 'ind_nombre_detalle' => 'LINK NET', 'num_estatus' => '1'),
                              array('cod_detalle' => '41', 'ind_nombre_detalle' => 'CESIO', 'num_estatus' => '1'),
                              array('cod_detalle' => '42', 'ind_nombre_detalle' => 'MICROSOFT', 'num_estatus' => '1'),
                              array('cod_detalle' => '43', 'ind_nombre_detalle' => 'DELL OFFICE', 'num_estatus' => '1'),
                              array('cod_detalle' => '44', 'ind_nombre_detalle' => 'BESTEC', 'num_estatus' => '1'),
                              array('cod_detalle' => '45', 'ind_nombre_detalle' => 'INTEL', 'num_estatus' => '1'),
                              array('cod_detalle' => '46', 'ind_nombre_detalle' => 'SEAGATE', 'num_estatus' => '1'),
                              array('cod_detalle' => '47', 'ind_nombre_detalle' => 'ACER', 'num_estatus' => '1'),
                              array('cod_detalle' => '48', 'ind_nombre_detalle' => 'L & C', 'num_estatus' => '1'),
                              array('cod_detalle' => '49', 'ind_nombre_detalle' => 'KINGSTON', 'num_estatus' => '1'),
                              array('cod_detalle' => '50', 'ind_nombre_detalle' => 'DATA STORAGE', 'num_estatus' => '1'),
                              array('cod_detalle' => '51', 'ind_nombre_detalle' => 'KODE', 'num_estatus' => '1'),
                              array('cod_detalle' => '52', 'ind_nombre_detalle' => 'SUPER TALENT', 'num_estatus' => '1'),
                              array('cod_detalle' => '53', 'ind_nombre_detalle' => 'CITIZEN', 'num_estatus' => '1'),
                              array('cod_detalle' => '54', 'ind_nombre_detalle' => 'HYNIX', 'num_estatus' => '1'),
                              array('cod_detalle' => '55', 'ind_nombre_detalle' => 'WESTERN DIGITAL', 'num_estatus' => '1'),
                              array('cod_detalle' => '56', 'ind_nombre_detalle' => 'EMERALD', 'num_estatus' => '1'),
                              array('cod_detalle' => '57', 'ind_nombre_detalle' => 'AC BEL', 'num_estatus' => '1'),
                              array('cod_detalle' => '58', 'ind_nombre_detalle' => 'ANATEL', 'num_estatus' => '1'),
                              array('cod_detalle' => '59', 'ind_nombre_detalle' => 'LITEON', 'num_estatus' => '1'),
                              array('cod_detalle' => '60', 'ind_nombre_detalle' => 'ITACHI', 'num_estatus' => '1'),
                              array('cod_detalle' => '61', 'ind_nombre_detalle' => 'ASROCK', 'num_estatus' => '1'),
                              array('cod_detalle' => '62', 'ind_nombre_detalle' => 'AIPRO', 'num_estatus' => '1'),
                              array('cod_detalle' => '63', 'ind_nombre_detalle' => 'BENQ', 'num_estatus' => '1'),
                              array('cod_detalle' => '64', 'ind_nombre_detalle' => 'TEMP', 'num_estatus' => '1'),
                              array('cod_detalle' => '65', 'ind_nombre_detalle' => 'FOXCONN', 'num_estatus' => '1'),
                              array('cod_detalle' => '66', 'ind_nombre_detalle' => 'WRITE MASTER', 'num_estatus' => '1'),
                              array('cod_detalle' => '67', 'ind_nombre_detalle' => 'LITE-ON', 'num_estatus' => '1'),
                              array('cod_detalle' => '68', 'ind_nombre_detalle' => 'JENIP', 'num_estatus' => '1'),
                              array('cod_detalle' => '69', 'ind_nombre_detalle' => 'VALUE SERIES', 'num_estatus' => '1'),
                              array('cod_detalle' => '70', 'ind_nombre_detalle' => 'BIO START', 'num_estatus' => '1'),
                              array('cod_detalle' => '71', 'ind_nombre_detalle' => 'PATRIOT', 'num_estatus' => '1'),
                              array('cod_detalle' => '72', 'ind_nombre_detalle' => 'AMD', 'num_estatus' => '1'),
                              array('cod_detalle' => '73', 'ind_nombre_detalle' => 'HIPRO', 'num_estatus' => '1'),
                              array('cod_detalle' => '74', 'ind_nombre_detalle' => 'ADATA', 'num_estatus' => '1'),
                              array('cod_detalle' => '75', 'ind_nombre_detalle' => 'MAXTONE', 'num_estatus' => '1'),
                              array('cod_detalle' => '76', 'ind_nombre_detalle' => 'AEVISION', 'num_estatus' => '1'),
                              array('cod_detalle' => '77', 'ind_nombre_detalle' => 'TALAMO', 'num_estatus' => '1'),
                              array('cod_detalle' => '78', 'ind_nombre_detalle' => 'WASH', 'num_estatus' => '1'),
                              array('cod_detalle' => '79', 'ind_nombre_detalle' => 'HP COMPAQ', 'num_estatus' => '1'),
                              array('cod_detalle' => '80', 'ind_nombre_detalle' => 'C D P', 'num_estatus' => '1'),
                              array('cod_detalle' => '81', 'ind_nombre_detalle' => 'AVTEK', 'num_estatus' => '1'),
                              array('cod_detalle' => '82', 'ind_nombre_detalle' => 'OFITECH', 'num_estatus' => '1'),
                              array('cod_detalle' => '83', 'ind_nombre_detalle' => 'X-ACTO', 'num_estatus' => '1'),
                              array('cod_detalle' => '84', 'ind_nombre_detalle' => 'KW-TRIO', 'num_estatus' => '1'),
                              array('cod_detalle' => '85', 'ind_nombre_detalle' => 'TECH', 'num_estatus' => '1'),
                              array('cod_detalle' => '86', 'ind_nombre_detalle' => 'GOLDEN DREAMS', 'num_estatus' => '1'),
                              array('cod_detalle' => '87', 'ind_nombre_detalle' => 'PREMIER', 'num_estatus' => '1'),
                              array('cod_detalle' => '88', 'ind_nombre_detalle' => 'APC', 'num_estatus' => '1'),
                              array('cod_detalle' => '89', 'ind_nombre_detalle' => 'ENERGIZER', 'num_estatus' => '1'),
                              array('cod_detalle' => '90', 'ind_nombre_detalle' => 'VOSS', 'num_estatus' => '1'),
                              array('cod_detalle' => '91', 'ind_nombre_detalle' => 'SUECO', 'num_estatus' => '1'),
                              array('cod_detalle' => '92', 'ind_nombre_detalle' => 'L G', 'num_estatus' => '1'),
                              array('cod_detalle' => '93', 'ind_nombre_detalle' => 'SIEMENS', 'num_estatus' => '1'),
                              array('cod_detalle' => '94', 'ind_nombre_detalle' => 'RUCOPI', 'num_estatus' => '1'),
                              array('cod_detalle' => '95', 'ind_nombre_detalle' => 'EAGLE', 'num_estatus' => '1'),
                              array('cod_detalle' => '96', 'ind_nombre_detalle' => 'CHICAGO DIGITAL POWER (CDP)', 'num_estatus' => '1'),
                              array('cod_detalle' => '97', 'ind_nombre_detalle' => 'TP LINK', 'num_estatus' => '1'),
                              array('cod_detalle' => '98', 'ind_nombre_detalle' => 'CLON', 'num_estatus' => '1'),
                              array('cod_detalle' => '99', 'ind_nombre_detalle' => 'GEHA', 'num_estatus' => '1'),
                              array('cod_detalle' => '100', 'ind_nombre_detalle' => 'SMART COOK', 'num_estatus' => '1'),
                              array('cod_detalle' => '101', 'ind_nombre_detalle' => 'SONCVICV', 'num_estatus' => '1'),
                              array('cod_detalle' => '102', 'ind_nombre_detalle' => 'TRANSCA', 'num_estatus' => '1'),
                              array('cod_detalle' => '103', 'ind_nombre_detalle' => 'TRAVELER', 'num_estatus' => '1'),
                              array('cod_detalle' => '104', 'ind_nombre_detalle' => 'AUSE', 'num_estatus' => '1'),
                              array('cod_detalle' => '105', 'ind_nombre_detalle' => '3M', 'num_estatus' => '1'),
                              array('cod_detalle' => '106', 'ind_nombre_detalle' => 'AXESS.TEL', 'num_estatus' => '1'),
                              array('cod_detalle' => '107', 'ind_nombre_detalle' => 'D-Link', 'num_estatus' => '1'),
                              array('cod_detalle' => '108', 'ind_nombre_detalle' => 'AIWA', 'num_estatus' => '1'),
                              array('cod_detalle' => '109', 'ind_nombre_detalle' => 'CRAFFT', 'num_estatus' => '1'),
                              array('cod_detalle' => '110', 'ind_nombre_detalle' => 'GEELY', 'num_estatus' => '1'),
                              array('cod_detalle' => '111', 'ind_nombre_detalle' => 'DECTECTO', 'num_estatus' => '1'),
                              array('cod_detalle' => '112', 'ind_nombre_detalle' => 'ELECTROSONIC', 'num_estatus' => '1'),
                              array('cod_detalle' => '113', 'ind_nombre_detalle' => 'RUN', 'num_estatus' => '1'),
                              array('cod_detalle' => '114', 'ind_nombre_detalle' => 'YAMAHA', 'num_estatus' => '1'),
                              array('cod_detalle' => '115', 'ind_nombre_detalle' => 'RIDGID', 'num_estatus' => '1'),
                              array('cod_detalle' => '116', 'ind_nombre_detalle' => 'BLACKBERRY', 'num_estatus' => '1'),
                              array('cod_detalle' => '117', 'ind_nombre_detalle' => 'MOTOROLA', 'num_estatus' => '1'),
                              array('cod_detalle' => '118', 'ind_nombre_detalle' => 'HUAWEL', 'num_estatus' => '1'),
                              array('cod_detalle' => '119', 'ind_nombre_detalle' => 'TOPCON', 'num_estatus' => '1'),
                              array('cod_detalle' => '120', 'ind_nombre_detalle' => 'BMI', 'num_estatus' => '1'),
                              array('cod_detalle' => '121', 'ind_nombre_detalle' => 'ALESSIS', 'num_estatus' => '1'),
                              array('cod_detalle' => '122', 'ind_nombre_detalle' => 'LINCE', 'num_estatus' => '1'),
                              array('cod_detalle' => '123', 'ind_nombre_detalle' => 'BOSTON', 'num_estatus' => '1'),
                              array('cod_detalle' => '124', 'ind_nombre_detalle' => 'ACWELDER', 'num_estatus' => '1'),
                              array('cod_detalle' => '125', 'ind_nombre_detalle' => 'BLACK DECKER', 'num_estatus' => '1'),
                              array('cod_detalle' => '126', 'ind_nombre_detalle' => 'TOP-FLO', 'num_estatus' => '1'),
                              array('cod_detalle' => '127', 'ind_nombre_detalle' => 'TECIVEN', 'num_estatus' => '1'),
                              array('cod_detalle' => '128', 'ind_nombre_detalle' => 'TYCOS', 'num_estatus' => '1'),
                              array('cod_detalle' => '129', 'ind_nombre_detalle' => 'AGUAJET', 'num_estatus' => '1'),
                              array('cod_detalle' => '130', 'ind_nombre_detalle' => 'EON', 'num_estatus' => '1'),
                              array('cod_detalle' => '131', 'ind_nombre_detalle' => 'TRUPPER', 'num_estatus' => '1'),
                              array('cod_detalle' => '132', 'ind_nombre_detalle' => 'DEWALT', 'num_estatus' => '1'),
                              array('cod_detalle' => '133', 'ind_nombre_detalle' => '3COM VASELINE', 'num_estatus' => '1'),
                              array('cod_detalle' => '134', 'ind_nombre_detalle' => 'TAKIMA', 'num_estatus' => '1'),
                              array('cod_detalle' => '135', 'ind_nombre_detalle' => 'KLIP', 'num_estatus' => '1'),
                              array('cod_detalle' => '136', 'ind_nombre_detalle' => 'NEXXT', 'num_estatus' => '1'),
                              array('cod_detalle' => '137', 'ind_nombre_detalle' => 'TARGUS', 'num_estatus' => '1'),
                              array('cod_detalle' => '138', 'ind_nombre_detalle' => 'PROFINA', 'num_estatus' => '1'),
                              array('cod_detalle' => '139', 'ind_nombre_detalle' => 'CHEVROLET', 'num_estatus' => '1'),
                              array('cod_detalle' => '140', 'ind_nombre_detalle' => 'ALADINO', 'num_estatus' => '1'),
                              array('cod_detalle' => '141', 'ind_nombre_detalle' => 'SPEEP', 'num_estatus' => '1'),
                              array('cod_detalle' => '142', 'ind_nombre_detalle' => 'DIPLOMAT', 'num_estatus' => '1'),
                              array('cod_detalle' => '143', 'ind_nombre_detalle' => 'LINKSYS', 'num_estatus' => '1'),
                              array('cod_detalle' => '144', 'ind_nombre_detalle' => 'AMBICO', 'num_estatus' => '1'),
                              array('cod_detalle' => '145', 'ind_nombre_detalle' => 'MANAPLAS', 'num_estatus' => '1'),
                              array('cod_detalle' => '146', 'ind_nombre_detalle' => 'FELLOWES', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'TABLA DE COLORES', 'ind_descripcion' => 'TABLA DE COLORES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'COLORES',
                        'Det' => array(
                              array('cod_detalle' => '1', 'ind_nombre_detalle' => 'AMARILLO', 'num_estatus' => '1'),
                              array('cod_detalle' => '2', 'ind_nombre_detalle' => 'AZUL', 'num_estatus' => '1'),
                              array('cod_detalle' => '3', 'ind_nombre_detalle' => 'PALO ROSA', 'num_estatus' => '1'),
                              array('cod_detalle' => '4', 'ind_nombre_detalle' => 'NARANJA', 'num_estatus' => '1'),
                              array('cod_detalle' => '5', 'ind_nombre_detalle' => 'VERDE', 'num_estatus' => '1'),
                              array('cod_detalle' => '6', 'ind_nombre_detalle' => 'BEIGE', 'num_estatus' => '1'),
                              array('cod_detalle' => '7', 'ind_nombre_detalle' => 'CROMÁTICO', 'num_estatus' => '1'),
                              array('cod_detalle' => '8', 'ind_nombre_detalle' => 'VINOTINTO', 'num_estatus' => '1'),
                              array('cod_detalle' => '9', 'ind_nombre_detalle' => 'GRIS / NEGRO', 'num_estatus' => '1'),
                              array('cod_detalle' => '10', 'ind_nombre_detalle' => 'PLATEADO', 'num_estatus' => '1'),
                              array('cod_detalle' => '11', 'ind_nombre_detalle' => 'BEIGE AUSTRALIA', 'num_estatus' => '1'),
                              array('cod_detalle' => '12', 'ind_nombre_detalle' => 'PLATEADO FERROSO', 'num_estatus' => '1'),
                              array('cod_detalle' => '13', 'ind_nombre_detalle' => 'PERLA', 'num_estatus' => '1'),
                              array('cod_detalle' => '14', 'ind_nombre_detalle' => 'BEIGE OLÍMPICO', 'num_estatus' => '1'),
                              array('cod_detalle' => '15', 'ind_nombre_detalle' => 'ARENA METALIZADO', 'num_estatus' => '1'),
                              array('cod_detalle' => '16', 'ind_nombre_detalle' => 'PLATA', 'num_estatus' => '1'),
                              array('cod_detalle' => '17', 'ind_nombre_detalle' => 'ROJO', 'num_estatus' => '1'),
                              array('cod_detalle' => '18', 'ind_nombre_detalle' => 'AMARILLO', 'num_estatus' => '1'),
                              array('cod_detalle' => '19', 'ind_nombre_detalle' => 'BEIGE LUNA', 'num_estatus' => '1'),
                              array('cod_detalle' => '20', 'ind_nombre_detalle' => 'MARRÓN / NEGRO', 'num_estatus' => '1'),
                              array('cod_detalle' => '21', 'ind_nombre_detalle' => 'AZUL / BEIGE', 'num_estatus' => '1'),
                              array('cod_detalle' => '22', 'ind_nombre_detalle' => 'MARRÓN / BEIGE', 'num_estatus' => '1'),
                              array('cod_detalle' => '23', 'ind_nombre_detalle' => 'VERDE ESMERALDA', 'num_estatus' => '1'),
                              array('cod_detalle' => '24', 'ind_nombre_detalle' => 'PLATA CLARO', 'num_estatus' => '1'),
                              array('cod_detalle' => '25', 'ind_nombre_detalle' => 'PLATEADO BRILLANTE', 'num_estatus' => '1'),
                              array('cod_detalle' => '26', 'ind_nombre_detalle' => 'MARRÓN PARDILLO BICAPA', 'num_estatus' => '1'),
                              array('cod_detalle' => '27', 'ind_nombre_detalle' => 'GRIS PALMERA', 'num_estatus' => '1'),
                              array('cod_detalle' => '28', 'ind_nombre_detalle' => 'DORADO', 'num_estatus' => '1'),
                              array('cod_detalle' => '29', 'ind_nombre_detalle' => 'MADERA NATURAL', 'num_estatus' => '1'),
                              array('cod_detalle' => '30', 'ind_nombre_detalle' => 'NEGRO/AMARILLO MOSTAZA', 'num_estatus' => '1'),
                              array('cod_detalle' => '31', 'ind_nombre_detalle' => 'MARRÓN', 'num_estatus' => '1'),
                              array('cod_detalle' => '32', 'ind_nombre_detalle' => 'BLANCO', 'num_estatus' => '1'),
                              array('cod_detalle' => '33', 'ind_nombre_detalle' => 'GRIS', 'num_estatus' => '1'),
                              array('cod_detalle' => '34', 'ind_nombre_detalle' => 'AZUL / GRIS', 'num_estatus' => '1'),
                              array('cod_detalle' => '35', 'ind_nombre_detalle' => 'AZUL / NEGRO', 'num_estatus' => '1'),
                              array('cod_detalle' => '36', 'ind_nombre_detalle' => 'ACERO', 'num_estatus' => '1'),
                              array('cod_detalle' => '37', 'ind_nombre_detalle' => 'OTRO COLOR', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'TIPO DE ACTAS MOVIMIENTOS DE ACTIVO FIJO', 'ind_descripcion' => 'TIPO DE ACTAS MOVIMIENTOS DE ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPOACTA',
                        'Det' => array(
                              array('cod_detalle' => 'AE', 'ind_nombre_detalle' => 'ACTA DE ENTRAEGA', 'num_estatus' => '1'),
                              array('cod_detalle' => 'AA', 'ind_nombre_detalle' => 'ACTA DE ADJUDICACIÓN', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'MOTIVOS DE TRASPASO (INTERNO)', 'ind_descripcion' => 'MOTIVOS DE TRASPASO (INTERNO)', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFMOTTRASI',
                        'Det' => array(
                              array('cod_detalle' => 'II', 'ind_nombre_detalle' => 'INGRESO INICIAL', 'num_estatus' => '1'),
                              array('cod_detalle' => 'TD', 'ind_nombre_detalle' => 'TRASLADO ENTRE DEPENDENCIAS', 'num_estatus' => '1'),
                              array('cod_detalle' => 'PM', 'ind_nombre_detalle' => 'PRESTAMO POR MOVIMIENTO DE BIENES', 'num_estatus' => '1'),
                              array('cod_detalle' => 'IN', 'ind_nombre_detalle' => 'INSERVIBILIDAD', 'num_estatus' => '1'),
                              array('cod_detalle' => 'DT', 'ind_nombre_detalle' => 'DETERIORO', 'num_estatus' => '1'),
                              array('cod_detalle' => 'OT', 'ind_nombre_detalle' => 'OTROS', 'num_estatus' => '1')
                        )
                  ),
                  array('ind_nombre_maestro' => 'MOTIVOS DE TRASPASO (EXTERNO)', 'ind_descripcion' => 'MOTIVOS DE TRASPASO (EXTERNO)', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFMOTTRASE',
                        'Det' => array(
                              array('cod_detalle' => 'DE', 'ind_nombre_detalle' => 'DESINCORPORACION', 'num_estatus' => '1'),
                              array('cod_detalle' => 'TE', 'ind_nombre_detalle' => 'TRASLADO A OTRAS ENTIDADES', 'num_estatus' => '1'),
                              array('cod_detalle' => 'TM', 'ind_nombre_detalle' => 'TRASLADO POR MANTENIMIENTO EXTERNO', 'num_estatus' => '1'),
                              array('cod_detalle' => 'OT', 'ind_nombre_detalle' => 'OTROS', 'num_estatus' => '1')
                        )
                  )
            ); 

            return $data;
      }
}
