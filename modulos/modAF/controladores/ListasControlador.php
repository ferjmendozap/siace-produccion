<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Selector Cuentas Contables
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |16-02-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';

class ListasControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->Listas = $this->metCargarModelo('Listas');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
    }

    /**
     * Método para cargar datos para un selector.
     *
     * @return Void
     */
    public function metSelectorCuentasContables()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado', $this->Listas->metListarCuentasContables());
        $this->atVista->metRenderizar('selectorCuentasContables','modales');
    }

    /**
     * Método para cargar datos para un selector.
     *
     * @return Void
     */
    public function metSelectorEmpleados()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado', $this->Listas->metListarEmpleados());
        $this->atVista->metRenderizar('selectorEmpleados','modales');
    }

    /**
     * Método para cargar datos para un selector.
     *
     * @return Void
     */
    public function metSelectorPersonas()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado', $this->Listas->metListarPersonas());
        $this->atVista->metRenderizar('selectorPersonas','modales');
    }
}
