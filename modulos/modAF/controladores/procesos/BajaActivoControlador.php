<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Baja de Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |04-04-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class BajaActivoControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->BajaActivo = $this->metCargarModelo('BajaActivo','procesos');
        $this->Activos = $this->metCargarModelo('Activos','activos');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex($lista='listar')
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'jquery-ui-selectable/jquery-ui',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        if ($_POST) $filtro = $_POST;
        else {
            if ($lista == 'revisar') $find_estado = 'PR';
            elseif ($lista == 'aprobar') $find_estado = 'RV';
            else $find_estado = '';
            $filtro = [
                'fnum_organismo' => '',
                'fnum_dependencia' => '',
                'fnum_centro_costo' => '',
                'fnum_motivo' => '',
                'find_estado' => $find_estado,
                'ffec_fecha_bajad' => '',
                'ffec_fecha_bajah' => '',
            ];
        }
        if ($lista == 'listar') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Listado',
            ];
        }
        elseif ($lista == 'revisar') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Revisar',
            ];
        }
        elseif ($lista == 'aprobar') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Aprobar',
            ];
        }
        $this->atVista->assign('data', $data);
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->assign('listado', $this->BajaActivo->metListar($filtro));
        $this->atVista->metRenderizar('listado');
    }

    /**
     * Método para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metForm($opcion='nuevo')
    {
        ##  valores del formulario
        if ($opcion == 'nuevo') {
            $form = [
                'metodo' => 'crear',
                'pk_num_baja_activo' => null,
                'fk_afb001_num_activo' => null,
                'fk_afc021_num_tipo_transaccion' => null,
                'fk_afc009_num_movimiento' => null,
                'fk_a006_num_motivo' => Select::miscelaneo('AFMOTTRASE', 'DE', 2),
                'fec_fecha' => date('Y-m-d'),
                'fec_fecha_baja' => date('Y-m-d'),
                'ind_nro_resolucion' => null,
                'ind_comentarios' => null,
                'cod_activo' => null,
                'desc_activo' => null,
                'ind_descripcion_empresa' => null,
                'ind_dependencia' => null,
                'ind_descripcion_centro_costo' => null,
                'persona_responsable' => null,
                'ind_nombre_categoria' => null,
                'ind_nombre_ubicacion' => null,
                'num_monto' => 0.00,
                'tipo_movimiento' => Select::miscelaneo('AFTIPOMOV', 'D', 2),
                'ind_estado' => 'PR',
                'lista_tipo_transaccion_cuentas' => [],
            ];
            $disabled = [
                'editar' => '',
                'ver' => '',
            ];
        }
        elseif ($opcion == 'editar' || $opcion == 'ver' || $opcion == 'anular' || $opcion == 'revisar' || $opcion == 'aprobar') {
            $form = $this->BajaActivo->metObtener($_POST['id']);
            $form['tipo_movimiento'] = Select::miscelaneo('AFTIPOMOV', 'D', 2);
            $form['lista_tipo_transaccion_cuentas'] = $this->BajaActivo->metObtenerCuentas($form['pk_num_baja_activo']);
            ##
            if ($opcion == 'editar') {
                $form['metodo'] = 'modificar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => '',
                ];
            }
            elseif ($opcion == 'ver') {
                $form['metodo'] = '';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
            elseif ($opcion == 'anular') {
                $form['metodo'] = 'anular';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
            elseif ($opcion == 'revisar') {
                $form['metodo'] = 'revisar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
            elseif ($opcion == 'aprobar') {
                $form['metodo'] = 'aprobar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
        }

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->assign('disabled', $disabled);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['fec_fecha_baja'] = Fecha::formatFecha($_POST['fec_fecha_baja']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'fk_afb001_num_activo' => 'required',
            'fk_afc021_num_tipo_transaccion' => 'required',
            'fk_afc009_num_movimiento' => 'required',
            'fk_a006_num_motivo' => 'required',
            'fec_fecha_baja' => 'required|date',
            'ind_comentarios' => 'required',
        ]);
        $gump->set_field_name("fk_afb001_num_activo", "Activo");
        $gump->set_field_name("fk_afc021_num_tipo_transaccion", "Tipo de Baja");
        $gump->set_field_name("fk_afc009_num_movimiento", "Tipo de Movimiento");
        $gump->set_field_name("fk_a006_num_motivo", "Motivo de Traslado");
        $gump->set_field_name("ind_comentarios", "Comentarios");

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->BajaActivo->metCrear($_POST);
            $field = $this->BajaActivo->metObtener($id);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' => [
                    '<td><label>'.$id.'</label></td>',
                    '<td><label>'.Select::options('af_estado_baja', $field['ind_estado'], 2).'</label></td>',
                    '<td><label>'.Fecha::formatFecha($field['fec_fecha_baja'],'Y-m-d','d-m-Y').'</label></td>',
                    '<td><label>'.$field['ind_comentarios'].'</label></td>',
                    '<td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-primary" id="modificar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            <button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-warning" id="ver"
                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Ver Registro">
                                <i class="fa fa-eye" style="color: #ffffff;"></i>
                            </button>
                            '.(($_POST['perfilAN'])?'<button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-danger" id="anular"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Anular Registro"
                                                            title="Anular Registro Nro. '.$id.'">
                                                        <i class="fa fa-thumbs-down" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>',
                ],
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        $_POST['fec_fecha_baja'] = Fecha::formatFecha($_POST['fec_fecha_baja']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'fk_afb001_num_activo' => 'required',
            'fk_afc021_num_tipo_transaccion' => 'required',
            'fk_afc009_num_movimiento' => 'required',
            'fk_a006_num_motivo' => 'required',
            'fec_fecha_baja' => 'required|date',
            'ind_comentarios' => 'required',
        ]);
        $gump->set_field_name("fk_afb001_num_activo", "Activo");
        $gump->set_field_name("fk_afc021_num_tipo_transaccion", "Tipo de Baja");
        $gump->set_field_name("fk_afc009_num_movimiento", "Tipo de Movimiento");
        $gump->set_field_name("fk_a006_num_motivo", "Motivo de Traslado");
        $gump->set_field_name("ind_comentarios", "Comentarios");

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $_POST['pk_num_baja_activo'];
            $this->BajaActivo->metModificar($_POST);
            $field = $this->BajaActivo->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => [
                    '<td><label>'.$id.'</label></td>',
                    '<td><label>'.Select::options('af_estado_baja', $field['ind_estado'], 2).'</label></td>',
                    '<td><label>'.Fecha::formatFecha($field['fec_fecha_baja'],'Y-m-d','d-m-Y').'</label></td>',
                    '<td><label>'.$field['ind_comentarios'].'</label></td>',
                    '<td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-primary" id="modificar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            <button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-warning" id="ver"
                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Ver Registro">
                                <i class="fa fa-eye" style="color: #ffffff;"></i>
                            </button>
                            '.(($_POST['perfilAN'])?'<button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-danger" id="anular"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Anular Registro"
                                                            title="Anular Registro Nro. '.$id.'">
                                                        <i class="fa fa-thumbs-down" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>',
                ],
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para anular un registro.
     *
     * @return Void
     */
    public function metAnular()
    {
        ##  crear registro
        $this->BajaActivo->metAnular($_POST);

        ##  devolver registro creado
        $jsondata = [
            'status' => 'anular',
            'id' => $_POST['pk_num_baja_activo'],
            'mensaje' => 'Registro anulado exitosamente',
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para revisar un registro.
     *
     * @return Void
     */
    public function metRevisar()
    {
        ##  crear registro
        $this->BajaActivo->metRevisar($_POST);

        ##  devolver registro creado
        $jsondata = [
            'status' => 'revisar',
            'id' => $_POST['pk_num_baja_activo'],
            'mensaje' => 'Registro revisado exitosamente',
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para aprobar un registro.
     *
     * @return Void
     */
    public function metAprobar()
    {
        ##  crear registro
        $fb = $this->BajaActivo->metObtener($_POST['pk_num_baja_activo']);
        $fa = $this->Activos->metObtener($fb['fk_afb001_num_activo']);
        $this->BajaActivo->metAprobar($_POST, $fa);

        ##  devolver registro creado
        $jsondata = [
            'status' => 'aprobar',
            'id' => $_POST['pk_num_baja_activo'],
            'mensaje' => 'Registro aprobado exitosamente',
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para cargar vista Imprimir Registro.
     *
     * @return Response
     */
    public function metImprimir()
    {
        ##  valores del formulario
        $form['id'] = $_POST['id'];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('reporte', 'modales');
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metPdf($id)
    {
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  ----------------------------------
        $db = new db();
        ##  Obtengo la data a imprimir
        $field = $this->BajaActivo->metObtener($id);
        ##  dependencia
        $sql = "SELECT
                    a4.ind_dependencia,
                    a1.ind_descripcion_empresa,
                    CONCAT_WS(' ', a3.ind_nombre1, a3.ind_nombre2, a3.ind_apellido1, a3.ind_apellido2) AS persona_dependencia,
                    rhc63.ind_descripcion_cargo AS persona_dependencia_cargo
                FROM
                    a004_dependencia a4
                    INNER JOIN a001_organismo a1 ON (a1.pk_num_organismo = a4.fk_a001_num_organismo)
                    LEFT JOIN a003_persona a3 ON (a3.pk_num_persona = a4.fk_a003_num_persona_responsable)
                    LEFT JOIN rh_b001_empleado rhb1 ON (rhb1.fk_a003_num_persona = a3.pk_num_persona)
                    LEFT JOIN rh_c005_empleado_laboral rhc5 ON (rhc5.fk_rhb001_num_empleado = rhb1.pk_num_empleado)
                    LEFT JOIN rh_c063_puestos rhc63 ON (rhc63.pk_num_puestos = rhc5.fk_rhc063_num_puestos_cargo)
                WHERE a4.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $field_dependencia = $query_dependencia->fetch();
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultL('L','mm','Letter');
        $pdf->Organismo = $field_dependencia['ind_descripcion_empresa'];
        $pdf->Dependencia = $field_dependencia['ind_dependencia'];
        $pdf->Titulo = 'SUSTENTO DE DESINCORPORACIÓN';
        $pdf->Head = "
            \$this->SetFont('Arial','B',10);
            \$this->Cell(195,5,utf8_decode('Documento: " . $field['ind_nro_resolucion'] . "'),0,1,'L');
            \$this->MultiCell(195,5,utf8_decode('Comentario: " . $field['ind_comentarios'] . "'),0,'L');
        ";
        ##
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##
        $i = 0;
        $pdf->Ln(1);
        //foreach ($field['activos'] as $f) {
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(0,0,0); $pdf->Rect(10, $pdf->GetY(), 260, 0.1, "D");
            $pdf->Ln(1);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFont('Arial','B',9);
            $pdf->SetWidths(array(10,140,30,40,40));
            $pdf->SetAligns(array('C','L','C','C','R'));
            $pdf->Row(array(utf8_decode('#'),
                            utf8_decode('Descripción'),
                            utf8_decode('Código Interno'),
                            utf8_decode('# Serie'),
                            utf8_decode('Monto Local')
                    ));
            $pdf->SetDrawColor(0,0,0); $pdf->Rect(10, $pdf->GetY(), 260, 0.1, "D");
            $pdf->Ln(2);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFont('Arial','B',9);
            $pdf->Row(array(++$i,
                            utf8_decode($field['desc_activo']),
                            utf8_decode($field['cod_activo']),
                            utf8_decode($field['ind_serie']),
                            number_format($field['num_monto'],2,',','.')
                    ));
        //}
        if ($pdf->GetY() > 160) $pdf->AddPage(); else $pdf->SetY(160);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Rect(39, $pdf->GetY(), 70, 0.1, "D");
        $pdf->Rect(170, $pdf->GetY(), 70, 0.1, "D");
        $pdf->SetFont('Arial','B',10);
        //$pdf->Cell(130,5,utf8_decode($f['persona_usuario']),0,0,'C');
        //$pdf->Cell(130,5,utf8_decode($f['persona_responsable']),0,0,'C');
        $pdf->Ln(20);
        $pdf->Rect(104, $pdf->GetY(), 70, 0.1, "D");
        $pdf->Cell(260,5,utf8_decode($field_dependencia['persona_dependencia']),0,0,'C');
        ##
        $pdf->Output();
    }
}
