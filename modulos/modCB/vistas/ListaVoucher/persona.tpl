<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                            <div class="table-responsive">
                                <table id="" class="table table-striped table-hover datatable1">
                                    <thead>
                                        <tr>
                                            <th>Id.</th>
                                            <th>Cédula</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                           {foreach item=registro from=$listado_persona}
                                           <tr id="id{$registro.pk_num_persona}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}'], ['{$registro.pk_num_persona}','{$registro.ind_cedula_documento}']);">
                                               <td><label>{$registro.pk_num_persona}</label></td>
                                                <td><label>{$registro.ind_cedula_documento}</label></td>
                                                <td><label>{$registro.ind_nombre1} {$registro.ind_apellido1}</label></td>
                                            </tr>
                                          {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        inicializar();
    });
</script>
