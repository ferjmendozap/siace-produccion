<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo Voucher</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id.</th>
                                <th>Código</th>
                                <th>Descripcion</th>
                                <th>Flag. Manual</th>
                                <th>Estado</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=registro from=$listado}
                                <tr id="id{$registro.pk_num_voucher}">
                                    <td><label>{$registro.pk_num_voucher}</label></td>
                                    <td><label>{$registro.cod_voucher}</label></td>
                                    <td><label>{$registro.ind_descripcion}</label></td>
                                    <td><i class="{if $registro.num_flag_manual==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td><i class="{if $registro.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td align="center">
                                        {if in_array('CB-01-90-03-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_voucher}" title="Editar"
                                                    descipcion="El Usuario a Modificado un post" titulo="Editar Tipo Voucher">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}

                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_voucher}" title="Ver"
                                                descipcion="El Usuario esta viendo Tipo Voucher" titulo="Ver Tipo Voucher">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                                        {if in_array('CB-01-90-03-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idRegistro="{$registro.pk_num_voucher}"  boton="si, Eliminar" title="Eliminar"
                                                    descipcion="El usuario a eliminado un Registro" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('CB-01-90-03-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  descipcion="el Usuario a creado un post"  titulo="Nuevo Registro" id="nuevo" ><i class="md md-create"></i>&nbsp;Nuevo Tipo Voucher</button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
   $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modCB/TipoVoucherCONTROL/';

        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'nuevoMET', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        // modificar registro
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'editarMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        // ver registro
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'verMET', { id: $(this).attr('idRegistro'), estado: 'ver' }, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        // eliminar registro
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var id = $(this).attr('idRegistro');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                $.post($base+'eliminarMET', { id: id }, function(dato){

                    if(dato['status']=='error' && dato['flag_mensaje']==1) {
                        swal("No puede ser Eliminado, es usado en otro proceso!", dato['mensaje'], "warning");
                    }else{
                        //  mensaje
                        $(document.getElementById('id'+id)).remove();
                        swal("Registro Eliminado!", dato['mensaje'], "success");
                        $('#cerrar').click();
                    }

                },'json');
            });
        });
    });
</script>
