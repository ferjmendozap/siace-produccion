<style type="text/css">
body {
    margin-top:0px;
}
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 69%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style>

<div class="container">

  <form action="{$_Parametros.url}modCB/PlanCuentaCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
   <input type="hidden" value="{if isset($form.metodo)}{$form.metodo}{/if}" name="metodo"/>

      <input type="hidden" value="{if isset($form.pk_num_cuenta)}{$form.pk_num_cuenta}{/if}" name="pk_num_cuenta" id="pk_num_cuenta" />
      <div class="col-sm-8">
          <h3> Datos de la Cuenta</h3>

            <div class="row">
               <!-- Nivel -->
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="num_nivelError">
                        <select id="num_nivel" name="num_nivel" class="form-control">
                            {Select::options('nivel_cuenta',$form.num_nivel,0)}
                        </select>
                        <label for="num_nivel">Seleccione el Nivel</label>
                    </div>
                </div>
               <!-- Número de Cuenta -->
                <div class="col-sm-3">
                    <div class="form-group floating-label" id="cod_cuentaError">
                        <input type="text" class="form-control" value="{if isset($form.cod_cuenta)}{$form.cod_cuenta}{/if}" name="cod_cuenta" id="cod_cuenta" maxlength="15" requiered >
                        <label for="cod_cuenta">Cuenta Número</label>
                    </div>
                </div>
                <!-- Cuenta Onco/Pub20 -->
                <div class="col-sm-2">
                 <div class="radio">
                  <label>
                    <input type="radio" {if isset($form.num_flag_tipo_cuenta) and $form.num_flag_tipo_cuenta==1} checked{/if} name="num_flag_tipo_cuenta" id="pub20" value="1" checked onclick="cargar_tipo_cuenta(this.value);" >
                   <span>PUB20</span>
                  </label>
                 </div>
                </div>

                <div class="col-sm-1">
                 <div class="radio">
                  <label>
                    <input type="radio" {if isset($form.num_flag_tipo_cuenta) and $form.num_flag_tipo_cuenta==2} checked{/if} name="num_flag_tipo_cuenta" id="onco" value="2" onclick="cargar_tipo_cuenta(this.value);" >
                   <span>ONCOP</span>
                  </label>
                 </div>
                </div>
            </div>

            <div class="row">
                <!-- Nivel -->
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalleError">
                        <input type="hidden" name="valor_miscelaneo" id="valor_miscelaneo" value="">
                        <select id="fk_a006_num_miscelaneo_detalle" name="fk_a006_num_miscelaneo_detalle" class="form-control">
                            {if ($form.num_flag_tipo_cuenta==1)}
                              {Select::miscelaneo('CUENTPUB20',$form.fk_a006_num_miscelaneo_detalle,0)}
                            {else}
                              {Select::miscelaneo('CUENTONCOP',$form.fk_a006_num_miscelaneo_detalle,0)}
                            {/if}
                        </select>
                        <label for="fk_a006_num_miscelaneo_detalle">Seleccione Tipo Cuenta</label>
                    </div>
                </div>

                <!-- Descripción -->
                <div class="col-sm-6">
                    <div class="form-group floating-label"  id="ind_descripcionError">
                        <input type="text" class="form-control" value="{if isset($form.ind_descripcion)}{$form.ind_descripcion}{/if}" name="ind_descripcion" id="ind_descripcion" requiered>
                        <label for="ind_descripcion">Descripción</label>
                    </div>
                </div>

            </div>

            <div class="row">
               <div class="col-sm-3">
                  <div class="radio">
                    <label>Naturaleza</label>

                  </div>
               </div>

               <div class="col-sm-2">
                 <div class="radio">
                    <input type="radio" {if isset($form.ind_tipo_saldo) and $form.ind_tipo_saldo==P} checked{/if} name="ind_tipo_saldo" id="opciones_1" value="D" checked>
                    <span>Deudora</span>
                 </div>
               </div>

               <div class="col-sm-2">
                 <div class="radio">
                   <label>
                    <input type="radio" {if isset($form.ind_tipo_saldo) and $form.ind_tipo_saldo==A} checked{/if} name="ind_tipo_saldo" id="opciones_2" value="A">
                   <span>Acreedora</span>
                   </label>
                 </div>
               </div>

               <div class="col-sm-4">
                 <div class="form-group floating-label" id="fk_cbb005_num_contabilidadesError">
                     <select id="fk_cbb005_num_contabilidades" name="fk_cbb005_num_contabilidades" class="form-control" required>
                     <option value="">&nbsp;</option>
                       {Select::lista('cb_b005_contabilidades', 'pk_num_contabilidades', 'ind_descripcion', $form.fk_cbb005_num_contabilidades)}
                     </select>
                     <label for="fk_a006_num_miscelaneo_detalle">Seleccione Tipo Contabilidad</label>
                    </div>
               </div>

            </div>

            <div class="row">
               <div class="col-sm-3">
                <div class="radio">
                  <label>Nivel de la Cuenta</label>

                </div>
              </div>

              <div class="col-sm-2">
                <div class="radio">
                    <input type="radio" {if isset($form.num_flag_tipo) and $form.num_flag_tipo==1} checked{/if} name="num_flag_tipo" id="principal" value="1" checked>
                    <span>Principal</span>

                </div>
              </div>

              <div class="col-sm-2">
                <div class="radio">
                  <label>
                    <input type="radio" {if isset($form.num_flag_tipo) and $form.num_flag_tipo==2} checked{/if} name="num_flag_tipo" id="auxiliar" value="2">
                   <span>Auxiliar</span>
                  </label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-5">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($form.num_flag_req_ccosto) and $form.num_flag_req_ccosto==1} checked{/if} value="1" name="num_flag_req_ccosto">
                        <span>Requiere Centro de Costo</span>
                    </label>
                </div>
              </div>

              <div class="col-sm-4">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($form.num_flag_req_activo) and $form.num_flag_req_activo==1} checked{/if} value="1" name="num_flag_req_activo">
                        <span>Requiere Activo</span>
                    </label>
                </div>
              </div>

              <div class="col-sm-3">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($form.num_estatus) and $form.num_estatus==1} checked{/if} value="1" name="num_estatus" id="num_estatus">
                        <span>Estatus</span>
                    </label>
                </div>
              </div>
            </div>

      </div>

    <div class="row content">
        <div class="col-sm-9">
          <div class="modal-footer">
              <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
              {if $form.estado != "ver"}
              <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
              {/if}
          </div>
        </div>
    </div>

</form>

</div>

<script type="text/javascript">
  $(document).ready(function () {

  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          //allWells= $('.modal-body');
          allNextBtn = $('.nextBtn');
          //allPrevBtn = $('.prevBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");

      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');

  $("#formAjax").submit(function(){
            return false;
  });

  $('#modalAncho').css( "width", "75%" );

  //  envio formulario
  $('#accion').click(function() {

      $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('CB-01-90-04-02-M',$_Parametros.perfil)}&perfilE={in_array('CB-01-90-04-03-E',$_Parametros.perfil)}", function(dato) {

          if (dato['status'] == 'error') {
              for (var i in dato.input) {
                  $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                  $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                  $(document.getElementById(dato+'Error')).append('<span id="'+dato+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
              }
          }else{
              //  mensaje
              swal("Registro Guardado!", dato['mensaje'], "success");
              $(document.getElementById('cerrarModal')).click();
              $(document.getElementById('ContenidoModal')).html('');

              //  actualizo tabla
              if (dato['status'] == 'crear') {
                  $('#datatable1 tbody').append(dato['tr']);
              }
              else if (dato['status'] == 'modificar') {
                  $('#id'+dato['id']).html(dato['tr']);
              }
          }
        },'json');
  });

  $('#pub20').click(function(){
      $('#valor_miscelaneo').val('CUENTPUB20');
  });

  $('#onco').click(function(){
      $('#valor_miscelaneo').val('CUENTONCO');
  });

});

  function cargar_tipo_cuenta(v_tipo_cuenta){
  $.post("{$_Parametros.url}modCB/PlanCuentaCONTROL/CargarTipoCuentaMET", "v_tipo_cuenta="+v_tipo_cuenta, function(dato) {
        $("#fk_a006_num_miscelaneo_detalle").html(dato['valor']);
  },'json');
}

</script>
