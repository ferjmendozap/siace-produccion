<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">C&oacute;digo</th>
                                        <th>Descripci&oacute;n</th>
                                        <th width="75">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {foreach item=registro from=$listado}
                                    {if $campos.caso == 'cuenta'}
                                        <tr id="id{$registro.pk_num_cuenta}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}'], ['{$registro.pk_num_cuenta}','{$registro.cod_cuenta}']);">
                                    {else}
                                        <tr id="id{$registro.pk_num_cuenta}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}'], ['{$registro.pk_num_cuenta}','{$registro.cod_cuenta} - {$registro.ind_descripcion}']);">
                                    {/if}
                                        <td><label>{$registro.pk_num_cuenta}</label></td>
                                        <td><label>{$registro.cod_cuenta}</label></td>
                                        <td><label>{$registro.ind_descripcion}</label></td>
                                        <td><i class="{if $registro.num_estatus=='1'}md md-check{else}md md-not-interested{/if}"></i></td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        inicializar();
    });
</script>
