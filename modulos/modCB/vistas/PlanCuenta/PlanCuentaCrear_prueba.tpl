<style type="text/css">
body{
    margin-top:40px;
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 50%; /* TAMAÑO DE ESPACIO ENTRE LOS TAB */
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
/* MANEJAMOS LA LINEA DE CONEXION HORIZONTAL */
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 69%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>

<div class="container">
  <div class="stepwizard">
      <div class="stepwizard-row setup-panel">
          <div class="stepwizard-step">
              <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
              <p>Datos de la Cuenta</p>
          </div>
          <div class="stepwizard-step">
              <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
              <p>Partidas Presupuestarias</p>
          </div>
      </div>
  </div>

  <form action="{$_Parametros.url}modCB/SistemaFuenteCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
      <div class="row setup-content" id="step-1">
          <div class="col-xs-12">
              <div class="col-md-12">
                  <div class="form-group col-sm-2">
                      <label class="control-label">First Name</label>
                      <input  maxlength="100" type="text" required="required" class="form-control" placeholder="Enter First Name" />
                  </div>
                  <div class="form-group col-sm-2">
                      <label class="control-label">Last Name</label>
                      <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" />
                  </div>
              </div>
          </div>
      </div>

      <div class="row setup-content" id="step-2">
          <div class="col-xs-12">
              <div class="col-md-12">
                  <h3> Step 2</h3>
                  <div class="form-group">
                      <label class="control-label">Company Name</label>
                      <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Name" />
                  </div>
                  <div class="form-group">
                      <label class="control-label">Company Address</label>
                      <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Address"  />
                  </div>
              </div>
          </div>
      </div>

  </form>
</div>
<script type="text/javascript">
 $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>
