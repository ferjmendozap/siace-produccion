<form action="{$_Parametros.url}modCB/GrupoCentroCostoCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
   <div class="modal-body">
      <input type="hidden" value="{if isset($form.metodo)}{$form.metodo}{/if}" name="metodo"/>
      <input type="hidden" value="{if isset($form.pk_num_grupo_centro_costo)}{$form.pk_num_grupo_centro_costo}{/if}" name="pk_num_grupo_centro_costo" id="pk_num_grupo_centro_costo" />

     <div class="col-sm-12">
       <div class="row">

          <!-- Grupo -->
          <div class="col-sm-2">
              <div class="form-group floating-label" id="cod_grupo_centro_costoError">
                  <input type="text" id="cod_grupo_centro_costo" name="cod_grupo_centro_costo" class="form-control" value="{if isset($form.cod_grupo_centro_costo)}{$form.cod_grupo_centro_costo}{/if}" maxlength="4" required >
                  <label for="cod_grupo_centro_costo">Grupo</label>
              </div>
          </div>

          <!-- Descripción -->
          <div class="col-sm-7">
              <div class="form-group floating-label"  id="ind_descripcionError">
                  <input type="text" id="ind_descripcion" name="ind_descripcion" class="form-control" value="{if isset($form.ind_descripcion)}{$form.ind_descripcion}{/if}" maxlength="100" required>
                  <label for="ind_descripcion">Descripción</label>
              </div>
          </div>

          <!-- Estado -->
          <div class="col-sm-3">
               <div class="checkbox checkbox-styled">
                   <label>
                     <input type="checkbox" {if isset($form.num_estatus) and $form.num_estatus==1} checked{/if} value="1" name="num_estatus" id="num_estatus"  >
                     <span>Estatus</span>
                    </label>
               </div>
          </div>

       </div>

        <!-- Sub-Grupos -->
            <div class="row">
                <div class="col-sm-12">
                     <div class="card card-underline">
                         <div class="card-head"><label><b>Sub-Grupos</b></label>
                             <div class="tools">
                                 <button type="button" id="insertar" class="btn ink-reaction btn-raised btn-info btn-xs" data-lista="detalle">
                                 <i class="md md-add"></i>
                                 </button>
                             </div>
                         </div>

                         <div class="card-body">
                             <div class="table-responsive" style="height: 170px;">
                             <table id="datatable1" class="table table-striped table-hover table-condensed" style="min-width: 320px;">
                                 <thead>
                                     <tr>
                                        <th class="text-center" width="70">Código</th>
                                        <th class="text-center" width="270">Descripción</th>
                                        <th class="text-center" width="70">Estado</th>
                                        <th width="25">Eliminar</th>
                                     </tr>
                                 </thead>
                                 <tbody id="lista_detalle">
                                {$numero= 0}
                                {foreach item=registro from=$form.listado_detalle}
                                    {$numero=$numero+1}
                                    <tr id="detalle{$numero}">
                                        <td>
                                            <div class="col-sm-10">
                                                <div class="form-group form-group-sm floating-label">
                                                    <!--<input type="hidden" name="pk_num_subgrupo_centro_costo" value="{$registro.pk_num_subgrupo_centro_costo}">-->
                                                    <input type="text" name="detalle_pk_num_subgrupo_centro_costo[]" id="detalle_pk_num_subgrupo_centro_costo{$numero}" value="{$registro.pk_num_subgrupo_centro_costo}" class="form-control input-sm" style="text-align:center;" disabled>
                                                    <label for="pk_num_subgrupo_centro_costo">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-sm">
                                                    <input type="text" name="detalle_ind_descripcion[]" id="detalle_ind_descripcion{$numero}" value="{$registro.ind_descripcion}" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                          <div class="col-sm-3">
                                             <div class="checkbox checkbox-styled">
                                                   <label>
                                                     <input type="checkbox" {if isset($registro.num_estatus) and $registro.num_estatus==1} checked{/if} value="1" name="detalle_num_estatus[]" id="detalle_num_estatus{$numero}">
                                                    </label>
                                               </div>
                                          </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-2">
                                              <button class="btn ink-reaction btn-raised btn-xs btn-danger"  onclick="$('#detalle{$numero}').remove();">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                             </button>
                                           </div>
                                        </td>
                                    </tr>
                                {/foreach}
                                 </tbody>
                             </table>
                             </div>
                         </div>
                     </div>
                </div>
            </div>
        <!--  -->
     </div>
     <span class="clearfix"></span>
   </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $form.estado != "ver"}
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
        {/if}
    </div>

</form>

<script type="text/javascript">

  $(document).ready(function(){

     $('#modalAncho').css( "width", "60%" );

     //  insertar linea
     $('#insertar').click(function() {
        var tbody = $('#lista_' + $(this).data('lista'));
        var numero = $('#lista_' + $(this).data('lista') + ' tr').length + 1;

        $.post("{$_Parametros.url}modCB/GrupoCentroCostoCONTROL/insertarMET", "numero="+numero, function(dato) {
            tbody.append(dato);
        }, '');
     });

      //  envio formulario
      $('#accion').click(function() {

          $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('CB-01-90-06-02-M',$_Parametros.perfil)}&perfilE={in_array('CB-01-90-06-03-E',$_Parametros.perfil)}", function(dato) {

              if (dato['status'] == 'error') {
                  for (var i in dato.input) {
                      $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                      $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                      $(document.getElementById(dato+'Error')).append('<span id="'+dato+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                  }
              } else {
                  //  mensaje
                  //swal("Registro Guardado!", dato['mensaje'], "success");
                  swal(dato['swal'], dato['mensaje'], "success");
                  $(document.getElementById('cerrarModal')).click();
                  $(document.getElementById('ContenidoModal')).html('');

                  //  actualizo tabla
                  if(dato['status']=='crear') {
                      $('#datatable1 tbody').append(dato['tr']);
                  }else if (dato['status'] == 'modificar') {
                      $('#id'+dato['id']).html(dato['tr']);
                  }
              }
          },'json');
      });
  });

</script>
