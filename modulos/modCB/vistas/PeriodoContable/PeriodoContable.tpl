<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Control de Cierres Mensuales</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id.</th>
                                <th>Tipo Registro</th>
                                <th>Período</th>
                                <th>Libro Contable</th>
                                <th>Estado</th>
                                <th width="200">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=registro from=$listado}
                                <tr id="id{$registro.pk_num_control_cierre}">
                                    <td><label>{$registro.pk_num_control_cierre}</label></td>
                                    <td><label>{$registro.ind_tipo_registro}</label></td>
                                    <td><label>{$registro.fec_anio}-{$registro.ind_mes}</label></td>
                                    <td><label>{ConsultarDescripcion::consultar($registro.fk_cbb002_cod_libro_contable,'pk_num_libro_contable','cb_b002_libro_contable')}</label></td>

                                    <td><i class="{if $registro.ind_estatus==A}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td align="center">
                                        {if in_array('CB-01-01-01-02-M',$_Parametros.perfil) and $registro.ind_estatus==A}
                                          <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" title="Editar"
                                                    data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_control_cierre}"
                                                    descipcion="El Usuario a Modificado un post" titulo="Editar Registro">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                          </button>
                                        {/if}

                                        {if $registro.ind_estatus==C}
                                         <button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idRegistro="{$registro.pk_num_control_cierre}"  boton="Abrir"
                                                    descipcion="El usuario a eliminado un post" titulo="Estas Seguro?" mensaje="Esta seguro que desea abrir el Período!!" id="abrir_periodo" title="Abrir"> <i class="md md-open-in-browser" style="color: #ffffff;"></i>
                                         </button>
                                        {/if}

                                        {if $registro.ind_estatus==A}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idRegistro="{$registro.pk_num_control_cierre}"  boton="Cerrar"
                                                    descipcion="El usuario a eliminado un post" titulo="Estas Seguro?" mensaje="Estas seguro que desea cerrar el Periodo!!" id="cerrar_periodo" title="Cerrar"> <i class="md md-close" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}

                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_control_cierre}" title="Ver"
                                                descipcion="El Usuario esta viendo Periodo" titulo="Ver Período">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('CB-01-01-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  descipcion="el Usuario a creado un post"  titulo="Nuevo Registro" id="nuevo" >
                                    <i class="md md-create"></i>&nbsp;Nuevo Período
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
   $(document).ready(function() {

        //  base
        var $base = '{$_Parametros.url}modCB/PeriodoContableCONTROL/';

        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'nuevoMET', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'editarMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'verMET', { id: $(this).attr('idRegistro'), estado: 'ver' }, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        /*
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var id = $(this).attr('idRegistro');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                $.post($base+'eliminarMET', { id: id }, function(dato){
                    //  mensaje
                    $(document.getElementById('id'+id)).remove();
                    swal("Registro Eliminado!", dato['mensaje'], "success");
                    $('#cerrar').click();
                },'json');
            });
        });
        */

        //prueba
        // Cerrar Periodo
        $('#datatable1 tbody').on( 'click', '#cerrar_periodo', function () {
            var id = $(this).attr('idRegistro');
                swal({
                    title: $(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    $.post($base+'cerrarPeriodoMET', { id: id, perfilM: "{in_array('CB-01-01-01-02-M',$_Parametros.perfil)}", perfilE: "{in_array('CB-01-01-01-03-E',$_Parametros.perfil)}" }, function(dato){

                        $('#id'+dato['id']).html(dato['tr']);
                        swal("Registro Cerrado!", dato['mensaje'], "success");
                        $('#cerrar').click();

                    },'json');
                });
        });

        // Abrir Periodo
        $('#datatable1 tbody').on( 'click', '#abrir_periodo', function () {
            var id = $(this).attr('idRegistro');
                swal({
                    title: $(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    $.post($base+'abrirPeriodoMET', { id: id, perfilM: "{in_array('CB-01-01-01-02-M',$_Parametros.perfil)}", perfilE: "{in_array('CB-01-01-01-03-E',$_Parametros.perfil)}" }, function(dato){

                        $('#id'+dato['id']).html(dato['tr']);
                        swal("Registro Abierto!", dato['mensaje'], "success");
                        $('#cerrar').click();

                    },'json');
                });
        });

    });
</script>
