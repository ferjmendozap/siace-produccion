<form action="{$_Parametros.url}modCB/CentroCostoCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
   <div class="modal-body">
      <input type="hidden" value="{if isset($form.metodo)}{$form.metodo}{/if}" name="metodo"/>
      <input type="hidden" value="{if isset($form.pk_num_centro_costo)}{$form.pk_num_centro_costo}{/if}" name="pk_num_centro_costo" id="pk_num_centro_costo" />

     <div class="col-sm-12">
       <div class="row">
          <div class="col-sm-3">
            <div class="form-group floating-label" id="cod_centro_costo">
              <input type="text" id="cod_centro_costo" name="cod_centro_costo" class="form-control" value="{if isset($form.cod_centro_costo)}{$form.cod_centro_costo}{/if}" maxlength="4" style="text-align:center;" required>
              <label for="cod_centro_costo">Cód. CentroCosto</label>
            </div>
          </div>
       </div>

       <div class="row">

          <!-- Abreviatura Centro Costo -->
          <div class="col-sm-2">
              <div class="form-group floating-label" id="ind_abreviaturaError">
                  <input type="text" id="ind_abreviatura" name="ind_abreviatura" class="form-control" value="{if isset($form.ind_abreviatura)}{$form.ind_abreviatura}{/if}" maxlength="10" style="text-align:center;" required>
                  <label for="ind_abreviatura">Abreviatura</label>
              </div>
          </div>

          <!-- Descripción -->
          <div class="col-sm-7">
              <div class="form-group floating-label"  id="ind_descripcion_centro_costoError">
                  <input type="text" id="ind_descripcion_centro_costo" name="ind_descripcion_centro_costo" class="form-control" value="{if isset($form.ind_descripcion_centro_costo)}{$form.ind_descripcion_centro_costo}{/if}" required maxlength="100"/>
                  <label for="ind_descripcion_centro_costo">Descripción</label>
              </div>
          </div>

          <!-- Estado -->
          <div class="col-sm-3">
               <div class="checkbox checkbox-styled">
                   <label>
                     <input type="checkbox" {if isset($form.num_estatus) and $form.num_estatus==1} checked{/if} value="1" name="num_estatus" id="num_estatus">
                     <span>Estatus</span>
                    </label>
               </div>
          </div>
       </div>

       <div class="row">
          <!-- Dependencia -->
          <div class="col-sm-5">
              <div class="form-group floating-label" id="fk_a004_num_dependenciaError">
                  <select id="fk_a004_num_dependencia" name="fk_a004_num_dependencia" class="form-control" onchange="cargar_responsable_dependencia(this.value);" required>
                    <option value="">&nbsp;</option>
                       {Select::lista('a004_dependencia', 'pk_num_dependencia', 'ind_dependencia', $form.fk_a004_num_dependencia)}
                  </select>
                  <label for="fk_a004_num_dependencia">Dependencia</label>
              </div>
          </div>

          <!-- Listado Empleado -->
          <div class="col-sm-5">
               <div class="form-group" id="ind_nombre1Error">
                  <input type="hidden" name="pk_num_persona" id="pk_num_persona" value="{$form.fk_a003_num_persona}">
                  <input type="text" name="ind_nombre1" id="ind_nombre1" value="{$form.nombre} {$form.apellido}" class="form-control input-sm" maxlength="15" data-id="{$numero}" onchange="obtenerPersona($(this));" readonly>
                  <label for="ind_nombre1">Empleado</label>
              </div>
          </div>
          <div class="col-sm-2">
            <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                    style="margin: 0px 0px -45px -10px;"
                    data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Empleados"
                    onclick="selector($(this),'{$_Parametros.url}modCB/CentroCostoCONTROL/selectorPersonaMET', ['pk_num_persona','detalle_num_persona','ind_nombre1']);">
                <i class="md md-search"></i>
            </button>
          </div>
       </div>

       <div class="row">
         <!-- Grupo Centro costo -->
         <div class="col-sm-5">
           <div class="form-group" id="fk_grupo_centro_costoError">
             <select id="fk_grupo_centro_costo" name="fk_grupo_centro_costo" class="form-control" onchange="cargar_subgrupo_centro_costo(this.value);">
               <option value="">&nbsp;</option>
                       {Select::lista('a024_grupo_centro_costo', 'pk_num_grupo_centro_costo', 'ind_descripcion', $form.fk_grupo_centro_costo)}
             </select>
             <label for="fk_grupo_centro_costo">Grupo Centro Costo</label>
           </div>
         </div>

          <!-- SubGrupo Centro costo -->
          <div class="col-sm-5">
               <div class="form-group" id="fk_a025_num_subgrupo_centro_costoError">
                  <select id="fk_a025_num_subgrupo_centro_costo" name="fk_a025_num_subgrupo_centro_costo" class="form-control">
                     {if ($form.fk_a025_num_subgrupo_centro_costo!='')}
                       {Select::lista2('a025_subgrupo_centro_costo', 'pk_num_subgrupo_centro_costo', 'ind_descripcion', 'fk_a024_num_grupo_centro_costo', $form.fk_grupo_centro_costo,'1')}
                     {/if}
                  </select>
                  <label for="fk_a025_num_subgrupo_centro_costo">SubGrupo Centro Costo</label>
              </div>
          </div>
       </div>

       <div class="row">
          <div class="col-sm-3">
              <div class="checkbox checkbox-styled">
                   <label>
                     <input type="checkbox" {if isset($form.num_flag_administrativo) and $form.num_flag_administrativo==1} checked{/if} value="1" name="num_flag_administrativo" id="num_flag_administrativo">
                     <span>Administrativo</span>
                   </label>
              </div>
          </div>
          <div class="col-sm-2">
               <div class="checkbox checkbox-styled">
                   <label>
                     <input type="checkbox" {if isset($form.num_flag_ventas) and $form.num_flag_ventas==1} checked{/if} value="1" name="num_flag_ventas" id="num_flag_ventas">
                     <span>Ventas</span>
                   </label>
               </div>
          </div>
          <div class="col-sm-2">
               <div class="checkbox checkbox-styled">
                   <label>
                     <input type="checkbox" {if isset($form.num_flag_financiera) and $form.num_flag_financiera==1} checked{/if} value="1" name="num_flag_financiera" id="num_flag_financiera">
                     <span>Financiera</span>
                   </label>
               </div>
          </div>
          <div class="col-sm-2">
               <div class="checkbox checkbox-styled">
                   <label>
                     <input type="checkbox" {if isset($form.num_flag_produccion) and $form.num_flag_produccion==1} checked{/if} value="1" name="num_flag_produccion" id="num_flag_produccion">
                     <span>Producción</span>
                   </label>
               </div>
          </div>
          <div class="col-sm-3">
               <div class="checkbox checkbox-styled">
                   <label>
                     <input type="checkbox" {if isset($form.num_flag_centro_ingresos) and $form.num_flag_centro_ingresos==1} checked{/if} value="1" name="num_flag_centro_ingresos" id="num_flag_centro_ingresos">
                     <span>Centro Ingresos</span>
                   </label>
               </div>
          </div>
       </div>

     </div>
     <span class="clearfix"></span>
   </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $form.estado != "ver"}
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
        {/if}
    </div>

</form>

<script type="text/javascript">

  $(document).ready(function(){

     $('#modalAncho').css( "width", "60%" );
     /**
     *Envío de formulario
     */
     $('#accion').click(function() {

          $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('CB-01-90-07-02-M',$_Parametros.perfil)}&perfilE={in_array('CB-01-90-07-03-E',$_Parametros.perfil)}", function(dato) {

              if (dato['status'] == 'error') {
                  for (var i in dato.input) {
                      $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                      $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                       $(document.getElementById(dato+'Error')).append('<span id="'+dato+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                  }
              }else{
                  //  mensaje
                  swal(dato['swal'], dato['mensaje'], "success");
                  $(document.getElementById('cerrarModal')).click();
                  $(document.getElementById('ContenidoModal')).html('');

                  //  actualizo tabla
                  if(dato['status']=='crear') {
                      $('#datatable1 tbody').append(dato['tr']);
                  }else if (dato['status'] == 'modificar') {
                      $('#id'+dato['id']).html(dato['tr']);
                  }
              }
          },'json');
     });

     /**
     *Perimite cargar listado de empleados
     */
     function obtenerPersona(detalle_num_persona) {
        var pk_num_persona = $('#pk_num_persona' + detalle_num_persona.data('id'));
        var input = detalle_num_persona.attr('id');

        $.post("{$_Parametros.url}modCB/CentroCostoCONTROL/ObtenerPersonaMET", "detalle_num_persona="+detalle_num_persona.val(), function(dato) {
            if (dato == '') {
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
            }
            else {
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
            }
            pk_num_persona.val(dato);
        }, '');
     }
  });

  /**
  *Permite cargar selector subgrupo centro costo
  */
  function cargar_subgrupo_centro_costo(grupo_centro_costo){
        $.post("{$_Parametros.url}modCB/CentroCostoCONTROL/CargarSubGrupoCentroCostoMET", "grupo_centro_costo="+grupo_centro_costo, function(dato) {
              $("#fk_a025_num_subgrupo_centro_costo").html(dato['valor']);
       },'json');
  }

  /**
  *Permite cargar responsable de la dependencia seleccionada
  */
  function cargar_responsable_dependencia(pk_num_dependencia){
     $.post("{$_Parametros.url}modCB/CentroCostoCONTROL/CargarResponsableDependenciaMET", "pk_num_dependencia="+pk_num_dependencia, function(dato){
        $("#ind_nombre1").val(dato['nombre_persona']);
        $("#pk_num_persona").val(dato['valor_pk_num_persona']);
     },'json');
  }

</script>
