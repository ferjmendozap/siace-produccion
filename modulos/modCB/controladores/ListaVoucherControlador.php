<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';
require_once ROOT.'librerias'.DS.'Number.php';

class ListaVoucherControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->ListaVoucher = $this->metCargarModelo('ListaVoucher');
    }

    public function metIndex()
    {
          $complementosCss = array(
             'DataTables/jquery.dataTables',
             'DataTables/extensions/dataTables.colVis941e',
             'DataTables/extensions/dataTables.tableTools4029',
             'bootstrap-datepicker/datepicker',
         );
         $complementoJs = array(
             'mask/jquery.mask',
             'bootstrap-datepicker/bootstrap-datepicker',
         );
         $js[] = 'materialSiace/core/demo/DemoTableDynamic';
         $js[] = 'Scripts/Form';

         if ($_POST) $filtro = $_POST;
         else {
            $filtro = [
                'fnum_contabilidades' => '',
                'fperiodo' => '',
                'fnum_voucher' => '',
                'festado_voucher' => '',
            ];
         }

         $this->atVista->assign('filtro', $filtro);
         $this->atVista->metCargarCssComplemento($complementosCss);
         $this->atVista->metCargarJsComplemento($complementoJs);
         $this->atVista->metCargarJs($js);
         $this->atVista->assign('listado',$this->ListaVoucher->metListar($filtro));
         $this->atVista->metRenderizar('ListaVoucher');
    }

    public function metIcono()
    {
        $this->atVista->metRenderizar('icono','modales');
    }

    /**
    * Carga formulario para crear nuevo voucher
    */
    public function metNuevo()
    {
      ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_voucher_mast' => null,
            'ind_mes' => null,
            'ind_anio' => null,
            'ind_voucher' => null,
            'ind_nro_voucher' => null,
            'num_creditos' => null,
            'num_debitos' => null,
            'listado_detalle' => [],
            'estado' => 'nuevo',
        ];

         $js = array( 'modCB/jquery.formatCurrency-1.4.0', 'modCB/jquery.formatCurrency.all',) ;
         $this->atVista->metCargarJs($js);

        $this->idUsuario = Session::metObtener('idUsuario');
        $this->Contabilidades = Select::parametros('CONTABILIDAD');
        $id = $this->ListaVoucher->metContabilidades($this->Contabilidades);

        $form['fk_cbb005_num_contabilidades'] = $id;
        $form['fk_a003_num_preparado_por']= $this->ListaVoucher->metConsultaPersona($this->idUsuario);
        $form['ind_mes']= date("Y-m");
        $form['periodo']= date("Y-m");
        $form['fec_fecha_voucher']= date("d-m-Y");
        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('ListaVoucherCrear','modales');
    }

    /**
    * Inicia el proceso de registro del voucher creado
    */
    public function metCrear()
    {
        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'periodo' => 'required|alpha_dash|max_len,7',
            'fec_fecha_voucher' => 'required|alpha_dash|max_len,10',
            'txt_titulo_voucher' => 'required|alpha_space|max_len,300',
        ]);

        ##  validación exitosa
        if($validate === TRUE){

            ## Obteniendo datos del Periodo
            $valor_x= explode("-", $_POST['periodo']);
            $_POST['ind_mes']=  $valor_x[1];
            $_POST['ind_anio']=  $valor_x[0];

            // cambio de formato de fecha
            $date_fec = new DateTime($_POST['fec_fecha_voucher']);
            $_POST['fec_fecha_voucher']= $date_fec->format('Y-m-d H:i:s');

            $this->idUsuario = Session::metObtener('idUsuario');

            // datos default
            $_POST['fk_cbc002_num_sistema_fuente']= "06";
            $_POST['num_flag_transferencia']= "0";
            //obtengo el pk_num_persona
            $_POST['fk_a003_num_preparado_por']= $this->ListaVoucher->metConsultaPersona($this->idUsuario);

            ##  crear registro
            $id = $this->ListaVoucher->metNuevo($_POST);
            $field= $this->ListaVoucher->metMostrar($id);


            $db2= $this->ListaVoucher->metTipoVoucher($field['fk_cbc003_num_voucher']);
            if ($id=="no-periodo") {
                # code...
                ##  devolver mensaje
                $jsondata = [
                    'status' => 'notificacion',
                    'mensaje' => 'Abrir o Crear el período a utilizar',
                    'input' => $validate
                ];

            }
            elseif ($id=="no-igual") {
                # code...
                ##  devolver mensaje
                $jsondata = [
                    'status' => 'no-igual',
                    'mensaje' => 'Corregir los montos en las Columnas Debe/Haber',
                    'input' => $validate
                ];

            }
            else{
                $jsondata = [
                    'status' => 'crear',
                    'mensaje' => 'Registro creado exitosamente',
                    'id' => $id,
                    'tr' =>'<tr id="id'.$id.'">
                              <td><label>'.$id.'</label></td>
                              <td><label>'.$_POST['periodo'].'</label></td>
                              <td><label>'.$field['ind_voucher'].'</label></td>
                              <td><label>'.$db2['ind_descripcion'].'</label></td>
                              <td><label>'.$_POST['txt_titulo_voucher'].'</label></td>
                              <td><label>'.ConsultarDescripcion::options("cb_estado_voucher",$field['ind_estatus'],"0").'</label></td>
                              <td align="center">
                                '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                                data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Registro" title="Editar">
                                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                        </button>':'').'

                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Voucher" titulo="Consultar Voucher">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                                '.(($_POST['perfilE'] and $field['ind_estatus']=="AB")?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                                boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!" title="Eliminar">
                                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>':'').'

                                <button  idRegistro="'.$id.'"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="Anular"
                                                    titulo="Estas Seguro?" mensaje="Estas seguro que desea anular el Voucher!!" id="anular_voucher" title="Anular">
                                                    <i class="md md-not-interested" style="color: #ffffff;"></i>
                                </button>

                                '.(($field['ind_estatus']=="MA")?'<button idRegistro="'.$id.'" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info" boton="Imprimir" id="imprimir_voucher" >
                                                <i class="icm icm-print" style="color: #ffffff;"></i>
                                </button>':'').'
                              </td>
                    </tr>',
                ];
            }
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Carga formulario para actualización o modificación de un registro
    */
    public function metEditar()
    {
         $form= $this->ListaVoucher->metMostrar($_POST['id']);
         $form['listado_detalle']= $this->ListaVoucher->metListarVoucher($form['pk_num_voucher_mast']);
         $form1= $this->ListaVoucher->metLibroContabilidad($form['fk_cbb003_num_libro_contabilidad']);

         $form['metodo']= 'modificar';
         $form['estado']= 'editar';

         $periodo= $form['ind_anio'].'-'.$form['ind_mes'];

         $valor_x= explode(" ", $form['fec_fecha_voucher']);

            $hora=  $valor_x[1];
            $fecha=  $valor_x[0];

         $form['periodo']= $periodo;

         // cambio de formato de fecha
         $date_fec = new DateTime($form['fec_fecha_voucher']);
         $form['fec_fecha_voucher']= $date_fec->format('d-m-Y');

         $js = array('modCB/jquery.formatCurrency-1.4.0', 'modCB/jquery.formatCurrency.all') ;
         $this->atVista->metCargarJs($js);

         $this->atVista->assign('form', $form);
         $this->atVista->assign('form1', $form1);
         //$this->atVista->assign('form',$form);
         //$this->atVista->assign('listado_detalle',$this->ListaVoucher->metListarVoucher($_POST['id']));
         $this->atVista->metRenderizar('ListaVoucherCrear', 'modales');
    }

    /**
    * Carga formulario para vizualizar el voucher
    */
    public function metVer()
    {
         $form= $this->ListaVoucher->metMostrar($_POST['id']);
         $form['listado_detalle']= $this->ListaVoucher->metListarVoucher($form['pk_num_voucher_mast']);
         $form1= $this->ListaVoucher->metLibroContabilidad($form['fk_cbb003_num_libro_contabilidad']);

         $form['metodo']= 'ver';
         $form['estado']= $_POST['estado'];

         $periodo= $form['ind_anio'].'-'.$form['ind_mes'];

         $valor_x= explode(" ", $form['fec_fecha_voucher']);

            $hora=  $valor_x[1];
            $fecha=  $valor_x[0];

         $form['periodo']= $periodo;

         // cambio de formato de fecha
         $date_fec = new DateTime($form['fec_fecha_voucher']);
         $form['fec_fecha_voucher']= $date_fec->format('d-m-Y');

         $js = array('modCB/jquery.formatCurrency-1.4.0', 'modCB/jquery.formatCurrency.all') ;
         $this->atVista->metCargarJs($js);

         $this->atVista->assign('form', $form);
         $this->atVista->assign('form1', $form1);
         $this->atVista->metRenderizar('ListaVoucherCrear', 'modales');
    }

    /**
    * Inicia el proceso de modificación
    */
    public function metModificar()
    {
        $id = $_POST['pk_num_voucher_mast'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'periodo' => 'required|alpha_dash|max_len,7',
            'fec_fecha_voucher' => 'required|alpha_dash|max_len,10',
            'txt_titulo_voucher' => 'required|alpha_space|max_len,300',
        ]);

        ##  validación exitosa
        if($validate === TRUE) {

            $valor_x= explode("-", $_POST['periodo']);
            $_POST['ind_mes']=  $valor_x[1];
            $_POST['fec_anio']=  $valor_x[0];

            // cambio de formato de fecha
            $date_fec = new DateTime($_POST['fec_fecha_voucher']);
            $_POST['fec_fecha_voucher']= $date_fec->format('Y-m-d H:i:s');

            ##  crear registro
            $respuesta= $this->ListaVoucher->metEditar($_POST);
            $db2= $this->ListaVoucher->metTipoVoucher($_POST['fk_cbc003_num_voucher']);

            $db= $this->ListaVoucher->metMostrar($id);
            $periodo= $_POST['fec_anio'].'-'.$_POST['ind_mes'];

            ##  devolver registro modificado
            if ($respuesta=="no-igual"){
                # code...
                ##  devolver mensaje
                $jsondata = [
                    'status' => 'no-igual',
                    'mensaje' => 'Corregir los montos en las Columnas Debe/Haber',
                    'input' => $validate
                ];

            }elseif($respuesta=="no-estatus-permitido"){
                # code...
                ##  devolver mensaje
                $jsondata = [
                    'status' => 'no-estatus-permitido',
                    'mensaje' => 'Puede ser modificado en Estatus Abierto',
                    'input' => $validate
                ];

            }elseif($respuesta=="si-modificar"){
                $jsondata = [
                    'status' => 'modificar',
                    'mensaje' => 'Registro actualizado exitosamente',
                    'id' => $id,
                    'tr' => '
                              <td><label>'.$id.'</label></td>
                              <td><label>'.$periodo.'</label></td>
                              <td><label>'.$db['ind_voucher'].'</label></td>
                              <td><label>'.$db2['ind_descripcion'].'</label></td>
                              <td><label>'.$_POST['txt_titulo_voucher'].'</label></td>
                              <td><label>'.ConsultarDescripcion::options("cb_estado_voucher",$db['ind_estatus'],"0").'</label></td>
                              <td align="center">
                                '.(($_POST['perfilM'] and $db['ind_estatus']=="AB")?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                                data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Registro" title="Editar">
                                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                        </button>':'').'

                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                    descipcion="El Usuario esta viendo Voucher" titulo="Ver Voucher">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                            </button>

                                '.(($_POST['perfilE'] and $db['ind_estatus']=="AB")?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                                boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!" title="Eliminar">
                                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                                        </button>':'').'

                                <button  idRegistro="'.$id.'"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="Anular"
                                                    titulo="Estas Seguro?" mensaje="Estas seguro que desea anular el Voucher!!" id="anular_voucher" title="Anular">
                                                    <i class="md md-not-interested" style="color: #ffffff;"></i>
                                </button>

                                '.(($db['ind_estatus']=="MA")?'<button idRegistro="'.$id.'" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info" boton="Imprimir" id="imprimir_voucher" title="Imprimir">
                                                    <i class="icm icm-print" style="color: #ffffff;"></i>
                                </button>':'').'

                              </td>
                    ',
                ];

            }else{
               ##  devolver errores
               $jsondata = [
                             'status' => 'error',
                             'input' => $validate
                           ];
            }
        }
        echo json_encode($jsondata);
        exit();
    }

    /**
    * Inicia el proceso de eliminado
    */
    public function metEliminar()
    {

        $this->ListaVoucher->metEliminar(['pk_num_voucher_mast' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

    /**
    * Permite insertar líneas detalle para el voucher
    */
    public function metInsertar()
    {
        $tr = '
        <tr id="detalle'.$_POST['numero'].'">
            <td>
                <div class="col-sm-10">
                    <div class="form-group form-group-sm floating-label" id="detalle_num_cuenta'.$_POST['numero'].'Error">
                        <input type="hidden" name="detalle_fk_cbb004_num_cuenta[]" id="detalle_fk_cbb004_num_cuenta'.$_POST['numero'].'">
                        <input type="text" name="detalle_num_cuenta[]" id="detalle_num_cuenta'.$_POST['numero'].'" class="form-control input-sm" maxlength="15" data-id="'.$_POST['numero'].'" onchange="obtenerCuenta($(this));" readonly>
                        <label for="detalle_num_cuenta">&nbsp;</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), \''.BASE_URL.'modCB/PlanCuentaCONTROL/selectorCuentasContablesMET\', [\'detalle_fk_cbb004_num_cuenta'.$_POST['numero'].'\',\'detalle_num_cuenta'.$_POST['numero'].'\'], \'Contabilidad='.$_POST['Contabilidad'].'\');">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </td>

            <td>
                <div class="col-sm-10">
                    <div class="form-group form-group-sm floating-label" id="detalle_num_persona'.$_POST['numero'].'Error">
                        <input type="hidden" name="detalle_fk_a003_num_persona[]" id="detalle_fk_a003_num_persona'.$_POST['numero'].'">
                        <input type="text" name="detalle_num_persona[]" id="detalle_num_persona'.$_POST['numero'].'" class="form-control input-sm" maxlength="15" data-id="'.$_POST['numero'].'" onchange="obtenerPersona($(this));" readonly>
                        <label for="detalle_num_persona">&nbsp;</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado Personas"
                            onclick="selector($(this), \''.BASE_URL.'modCB/ListaVoucherCONTROL/selectorPersonaMET\', [\'detalle_fk_a003_num_persona'.$_POST['numero'].'\',\'detalle_num_persona'.$_POST['numero'].'\']);">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </td>

            <td>
                <div class="col-sm-8">
                    <div class="form-group form-group-sm floating-label">
                        <input type="text" name="detalle_num_debe[]" id="detalle_num_debe'.$_POST['numero'].'" class="form-control input-sm" style="text-align:right;" onclick="monto_anterior($(this));" onchange="actualizarMonto($(this),\'debe\');">
                        <label for="detalle_num_debe">&nbsp;</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="col-sm-8">
                    <div class="form-group form-group-sm floating-label">
                        <input type="text" name="detalle_num_haber[]" id="detalle_num_haber'.$_POST['numero'].'" class="form-control input-sm" style="text-align:right;" onclick="monto_anterior($(this));" onchange="actualizarMonto($(this),\'haber\');">
                        <label for="detalle_num_haber">&nbsp;</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="col-sm-10">
                    <div class="form-group form-group-sm floating-label" id="detalle_num_centro_costo'.$_POST['numero'].'Error">
                        <input type="hidden" name="detalle_fk_a023_num_centro_costo[]" id="detalle_fk_a023_num_centro_costo'.$_POST['numero'].'">
                        <input type="text" name="detalle_num_centro_costo[]" id="detalle_num_centro_costo'.$_POST['numero'].'" class="form-control input-sm" maxlength="15" data-id="'.$_POST['numero'].'" onchange="obtenerCcosto($(this));" readonly>
                        <label for="detalle_num_centro_costo">&nbsp;</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado Centro Costos"
                            onclick="selector($(this), \''.BASE_URL.'modCB/ListaVoucherCONTROL/selectorCcostoMET\', [\'detalle_fk_a023_num_centro_costo'.$_POST['numero'].'\',\'detalle_num_centro_costo'.$_POST['numero'].'\']);">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </td>

            <td>
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label">
                        <input type="text" name="detalle_ind_descripcion[]" class="form-control input-sm">
                        <label for="detalle_ind_descripcion">&nbsp;</label>
                    </div>
                </div>
            </td>

            <td>
                <button class="btn ink-reaction btn-raised btn-xs btn-danger" onclick="monto_menos($(\'#detalle_num_debe'.$_POST['numero'].'\'),$(\'#detalle_num_haber'.$_POST['numero'].'\'))|$(\'#detalle'.$_POST['numero'].'\').remove();">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                </button>
            </td>
        </tr>';

        echo $tr;
    }

    /**
    *Consultar los datos de una cuenta
    */
    public function metObtenerCuenta()
    {
        $db = new db();
        $sql = "SELECT * FROM cb_b004_plan_cuenta WHERE cod_cuenta= '".$_POST['detalle_num_cuenta']."'";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        echo $field['pk_num_cuenta'];
    }

    /**
    * Carga Listado de Personas
    */
    public function metSelectorPersona()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado_persona',$this->ListaVoucher->metMostrar_persona());
        $this->atVista->metRenderizar('persona','modales');
    }

    /**
    * Obtiene datos centro costo
    */
    public function metObtenerPersona()
    {
        $db = new db();
        $sql = "SELECT * FROM a003_persona WHERE pk_num_persona = '".$_POST['detalle_num_persona']."'";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        echo $field['pk_num_persona'];
    }

    /**
    * Carga listado centro de costo
    */
    public function metSelectorCcosto()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado_ccosto',$this->ListaVoucher->metMostrar_ccosto());
        $this->atVista->metRenderizar('centro_costo','modales');
    }

    /**
    * Obtiene datos centro costo
    */
    public function metObtenerCentroCosto()
    {
        $db = new db();
        $sql = "SELECT * FROM a023_centro_costo WHERE pk_num_centro_costo = '".$_POST['detalle_num_centro_costo']."'";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        echo $field['pk_num_centro_costo'];
    }

    /**
    * Inicia el proceso de anulado del registro
    */
    public function metAnularVoucher()
    {

        $db= $this->ListaVoucher->metMostrar($_POST['id']);

        if($db['ind_estatus']!="AN"){
            $this->ListaVoucher->metAnularVoucher(['pk_num_voucher_mast' => $_POST['id']]);

            $db= $this->ListaVoucher->metMostrar($_POST['id']);
            $db2= $this->ListaVoucher->metTipoVoucher($db['fk_cbc003_num_voucher']);
            $periodo= $db['ind_anio'].'-'.$db['ind_mes'];

            $jsondata = [
                'status' => 'OK',
                'mensaje' => '',
                'id' => $_POST['id'],
                'tr' => '
                      <td><label>'.$_POST['id'].'</label></td>
                      <td><label>'.$periodo.'</label></td>
                      <td><label>'.$db['ind_voucher'].'</label></td>
                      <td><label>'.$db2['ind_descripcion'].'</label></td>
                      <td><label>'.$db['txt_titulo_voucher'].'</label></td>
                      <td><label>'.ConsultarDescripcion::options("cb_estado_voucher",$db['ind_estatus'],"0").'</label></td>
                      <td align="center">
                        '.(($_POST['perfilM'])?'<button idRegistro="'.$_POST['id'].'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro" title="Editar">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>':'').'

                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$_POST['id'].'" title="Ver"
                                                descipcion="El Usuario esta viendo Voucher" titulo="Ver Voucher">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                        '.(($_POST['perfilE'])?'<button idRegistro="'.$_POST['id'].'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                        boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!" title="Eliminar">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>':'').'

                        <button  idRegistro="'.$_POST['id'].'"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="Anular"
                                                    titulo="Estas Seguro?" mensaje="Estas seguro que desea anular el Voucher!!" id="anular_voucher" title="Anular">
                                                    <i class="md md-not-interested" style="color: #ffffff;"></i>
                                            </button>

                        '.(($db['ind_estatus']=="MA")?'<button idRegistro="'.$_POST['id'].'" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info" boton="Imprimir" id="imprimir_voucher" title="Imprimir" >
                                                <i class="icm icm-print" style="color: #ffffff;"></i>
                            </button>':'').'

                      </td>
                ',
            ];
        }
        else{
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'mensaje' => 'El registro ya se encuentra Anulado',
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Imprime voucher
    */
    public function metImprimirVoucher()
    {
        $valores = $this->ListaVoucher->metConsulta($_GET['id']);
        $valores2= $this->ListaVoucher->metConsulta2($_GET['id']);

        $_POST['valores']= $valores;

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('CabecerasReportePdf','modCB');

        $pdf=new ImprimirVoucher('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        //---------------------------------------------------
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetDrawColor(255, 255, 255);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetFont('Arial', '', 8);

        $total_debe = 0;
        $total_haber = 0;

        $valores3= $this->ListaVoucher->metListarVoucher($_GET['id']);

        foreach ($valores3 as $field) {

            $total_debe+= $field['num_debe'];
            $total_haber+= $field['num_haber'];

            $pdf->Row(array($field['num_cuenta'], utf8_decode($field['descripcion_cuenta']),
                            //$field['ReferenciaTipoDocumento'].'-'.$field['ReferenciaNroDocumento'],
                            '',
                            $field['nombre'].' '.$field['apellido'],
                            $field['num_centro_costo'],
                            number_format($field['num_debe'], 2, ',', '.'),
                            number_format($field['num_haber'], 2, ',', '.')));
        }

        //---------------------------------------------------
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(0, 0, 0);
        $y = $pdf->GetY() + 1;
        $pdf->Rect(220, $y, 50, 0.1, "FD");
        $pdf->SetY($y+2);
        $pdf->SetDrawColor(255, 255, 255);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Row(array('', '', '', '', '', number_format($total_debe, 2, ',', '.'), number_format($total_haber, 2, ',', '.')));
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(0, 0, 0);
        $y = $pdf->GetY() + 1;
        $pdf->Rect(220, $y, 50, 0.1, "FD");
        //---------------------------------------------------
        $pdf->Ln(30);
        $y = $pdf->GetY();
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(0, 0, 0);
        $pdf->Rect(70, $y, 55, 0.1, "FD");  $pdf->Rect(135, $y, 55, 0.1, "FD"); $pdf->Rect(200, $y, 55, 0.1, "FD");
        $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetXY(70, $y+1); $pdf->Cell(55, 5, 'PREPARADO POR', 0, 0, 'C');
        $pdf->SetXY(135, $y+1); $pdf->Cell(55, 5, 'CONFORMADO POR', 0, 0, 'C');
        $pdf->SetXY(200, $y+1); $pdf->Cell(55, 5, 'APROBADO POR', 0, 0, 'C');
        //---------------------------------------------------

        //  Muestro el contenido del pdf.
        $pdf->Output();
    }

}
