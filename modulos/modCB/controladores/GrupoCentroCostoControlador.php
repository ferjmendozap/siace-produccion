<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';

class GrupoCentroCostoControlador extends Controlador
{

    function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->GrupoCentroCosto= $this->metCargarModelo('GrupoCentroCosto');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->GrupoCentroCosto->metListar());
        $this->atVista->metRenderizar('GrupoCentroCostoLista');
    }

    /**
    * Función que permite cargar formulario para el ingreso del nuevo registro
    */
    public function metNuevo()
    {

      ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_grupo_centro_costo' => null,
            'cod_grupo_centro_costo' => null,
            'ind_descripcion' => null,
            'num_estatus' => 1,
            'listado_detalle' => [],
            'estado' => 'nuevo',
        ];

        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('GrupoCentroCostoCrear','modales');
    }

    /**
    * Función que permite cargar el proceso para el ingreso del nuevo registro
    */
    public function metCrear()
    {
        $valid = "";
        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'cod_grupo_centro_costo' => 'required|alpha_numeric|max_len,4',
            'ind_descripcion' => 'required|alpha_space|max_len,100',
        ]);
        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        if (isset($_POST['detalle_ind_descripcion'])) {
          for($i=0; $i<count($_POST['detalle_ind_descripcion']); $i++){
            $_POST['detalle_detalle']= $_POST['detalle_ind_descripcion'][$i];
            $validate = $gump->validate($_POST, [
                   'detalle_detalle'=> 'required|alpha_space|max_len,100',
            ]);
            if($_POST['detalle_detalle']==="") $valid= "error";
          }
        }else{
          $valid= "error";
        }


        ##  validación exitosa
        if(($validate === TRUE) and ($valid!="error")) {
            ##  crear registro
            $id= $this->GrupoCentroCosto->metNuevo($_POST);
            $swal= "Registro Guardado!";
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'swal' => $swal,
                'id' => $id,
                'tr' =>'<tr id="id'.$id.'">
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['cod_grupo_centro_costo'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion'].'</label></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Grupo Centro Costo" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                             <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Grupo Centro Costo" titulo="Ver Grupo Centro Costo">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                       </tr>
                   ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el formulario para la actualización de un registro
    */
    public function metEditar()
    {
         $form= $this->GrupoCentroCosto->metMostrar($_POST['id']);
         $form['listado_detalle']= $this->GrupoCentroCosto->metSubGrupoCentroCosto($form['pk_num_grupo_centro_costo']);
         $form['metodo'] = 'modificar';
         $form['estado'] = 'editar';

         $this->atVista->assign('form', $form);
         $this->atVista->metRenderizar('GrupoCentroCostoCrear', 'modales');
    }

    /**
    * Función que permite cargar el proceso para el editado de un registro
    */
    public function metModificar()
    {
        $valid = "";
        $id = $_POST['pk_num_grupo_centro_costo'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'cod_grupo_centro_costo' => 'required|alpha_numeric|max_len,4',
            'ind_descripcion' => 'required|alpha_space|max_len,100',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        if (isset($_POST['detalle_ind_descripcion'])) {
          for($i=0; $i<count($_POST['detalle_ind_descripcion']); $i++){
            $_POST['detalle_detalle']= $_POST['detalle_ind_descripcion'][$i];
            $validate = $gump->validate($_POST, [
                   'detalle_detalle'=> 'required|alpha_space|max_len,100',
            ]);
            if($_POST['detalle_detalle']==="") $valid= "error";
          }
        }else{
          $valid= "error";
        }

        ##  validación exitosa
        if(($validate === TRUE) and ($valid!="error")) {
            ##  crear registro
            $valores= $this->GrupoCentroCosto->metEditar($_POST);

            $valor_x= explode("|", $valores);
            $mensaje= $valor_x[0];
            $swal= $valor_x[1];
            $status= $valor_x[2];

            ##  devolver registro creado
            $jsondata = [
                'status' => $status,
                'mensaje' => $mensaje,
                'swal' => $swal,
                'id' => $id,
                'tr' =>'
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['cod_grupo_centro_costo'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion'].'</label></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Grupo Centro Costo" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                             <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Grupo Centro Costo" titulo="Ver Grupo Centro Costo">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                   ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el formulario mostrando datos del registro
    */
    public function metVer()
    {
        ## datos formulario
         $form= $this->GrupoCentroCosto->metMostrar($_POST['id']);
         $form['listado_detalle']= $this->GrupoCentroCosto->metSubGrupoCentroCosto($form['pk_num_grupo_centro_costo']);
         $form['metodo'] = 'ver';
         $form['estado'] = $_POST['estado'];

        ## vista
         $this->atVista->assign('form', $form);
         $this->atVista->metRenderizar('GrupoCentroCostoCrear', 'modales');
    }

    /**
    * Función que permite insertar lienas de detalle
    */
    public function metInsertar()
    {
      $tr='
        <tr id="detalle'.$_POST['numero'].'" >
           <td>
                <div class="col-sm-10">
                    <div class="form-group form-group-sm floating-label">
                        <!--<input type="text" value="'.$_POST['numero'].'"/>-->
                        <input type="text" name="detalle_pk_num_subgrupo_centro_costo[]"  id="detalle_pk_num_subgrupo_centro_costo'.$_POST['numero'].'" class="form-control input-sm" style="text-align:center;" disabled >
                        <label for="detalle_pk_num_subgrupo_centro_costo">&nbsp;</label>
                    </div>
                </div>
           </td>

           <td>
                <div class="col-sm-12">
                    <div class="form-group form-group-sm">
                        <input type="text" name="detalle_ind_descripcion[]" id="detalle_ind_descripcion'.$_POST['numero'].'" class="form-control" />
                    </div>
                </div>
           </td>

           <td>
                <div class="col-sm-3">
                   <div class="checkbox checkbox-styled">
                     <label>
                         <input type="checkbox" {if isset($form.num_estatus) and $form.num_estatus==1} checked{/if} value="1" name="detalle_num_estatus[]" id="detalle_num_estatus'.$_POST['numero'].'" checked/>
                         <span></span>
                     </label>
                   </div>
                </div>
           </td>

           <td>
                <div class="col-sm-2">
                 <button class="btn ink-reaction btn-raised btn-xs btn-danger"  onclick="$(\'#detalle'.$_POST['numero'].'\').remove();">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                 </button>
                </div>
           </td>
        </tr>';

        echo $tr;
    }

    /**
    * Función que permite cargar el proceso de eliminado de un registro
    */
    public function metEliminar()
    {

        $mensaje= $this->GrupoCentroCosto->metEliminar(['pk_num_grupo_centro_costo' => $_POST['id']]);

         $valor_x= explode("|", $mensaje);
            $mensaje= $valor_x[0];
            $valor= $valor_x[1];

        $jsondata = [
            'status' => 'OK',
            //'mensaje' => 'Registro eliminado exitosamente',
            'mensaje' =>  $mensaje,
            'valor'=> $valor,
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

}
