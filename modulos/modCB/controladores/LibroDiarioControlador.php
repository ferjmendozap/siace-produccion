<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';

class LibroDiarioControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->LibroDiario = $this->metCargarModelo('LibroDiario');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );

        $complementosJs = array(
                'bootstrap-datepicker/bootstrap-datepicker',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->Contabilidades = Select::parametros('CONTABILIDAD');
        $id = $this->LibroDiario->metContabilidades($this->Contabilidades);
        $form['fk_cbb005_num_contabilidades'] = $id;
        $this->atVista->assign('form',$form);

        $this->atVista->metRenderizar('LibroDiario');
    }

    /**
    * Crea el reporte Libro Diario
    */
    public function metLibroDiarioReportePdf()
    {
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('CabecerasReportePdf','modCB');
        $pdf=new ReporteLibroDiario('P','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $filtro= '';

        if ($_POST['periodo']!="") {
            $valor_x= explode("-", $_POST['periodo']);
            $anio= $valor_x[0];
            $mes=  $valor_x[1];

            $filtro.= " AND a.fec_anio= '$anio' AND a.ind_mes= '$mes' ";
        }else{
            $valor_x= explode("-", $_POST['anio_fecha']);
            $fanio= $valor_x[0];
            $fmes=  $valor_x[1];
            $filtro.= " AND a.fec_anio= '$fanio' AND a.ind_mes>='00' AND a.ind_mes<='$fmes'  ";
        }

        if ($_POST['voucher']!="") {
           $consultar_voucher= $this->LibroDiario->metConsultaVoucher($_POST['voucher']);

           foreach ($consultar_voucher as $f_voucher) {
              $filtro.= " AND a.fk_cbb001_num_voucher_mast= '".$f_voucher['pk_num_voucher_mast']."' ";
           }
        }

        $consulta= $this->LibroDiario->metLibroDiarioReportePdf($_POST['fk_cbb005_num_contabilidades'], $filtro);
        $cantidad= $this->LibroDiario->metLibroDiarioCantidadPdf($_POST['fk_cbb005_num_contabilidades'], $filtro);

        $debe01= 0; $haber01= 0; $debe= 0; $haber= 0; $codVoucherCapturada= 0;
        $t_debe= 0; $t_haber= 0; $cont= 0; $contMax= 1; $f_vocucher= '';

        foreach ($consulta as $f) {

              $valor_x= explode("-", $f['fec_fecha_voucher']);
              $valor_x1= explode(" ", $valor_x[2]);

              $f_vocucher = $valor_x1[0].'-'.$valor_x[1].'-'.$valor_x[0];

                if($f['fk_cbb001_num_voucher_mast']!=$codVoucherCapturada){

                      if($cont==1){

                         $t_debe = number_format($t_debe,2,',','.');
                         $t_haber = number_format($t_haber,2,',','.');

                         $pdf->SetFont('Arial', 'B', 7);
                         $pdf->Cell(192,6, '______________________________',0,1,'R');

                         $pdf->SetFillColor(202, 202, 202);
                         $pdf->SetFont('Arial', 'B', 7);
                         $pdf->Cell(104,2, '',0,0,'L');
                         $pdf->Cell(48,2,'TOTAL VOUCHER ->',0,0,'R');
                         $pdf->Cell(22,2,$t_debe,0,0,'C');
                         $pdf->Cell(18,2,$t_haber,0,1,'C');
                         $pdf->ln(3);
                         $t_debe=''; $t_haber='';
                      }

                      $pdf->SetDrawColor(255, 255, 255);$pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                      $pdf->SetFont('Arial', 'B', 8);
                      $pdf->SetWidths(array(30, 135, 30));
                      $pdf->SetAligns(array('L','L','L'));
                      $pdf->Row(array("Voucher #: ".$f['ind_voucher'], "", "Fecha: ".$f_vocucher));

                      $pdf->SetWidths(array(25, 135, 30));
                      $pdf->SetAligns(array('L','L','L'));
                      $pdf->Row(array(utf8_decode("Descripción: "), utf8_decode($f['txt_titulo_voucher']), ""));

                      $codVoucherCapturada = $f['fk_cbb001_num_voucher_mast'];
                }

                $cont= 1;

                $t_debe+= $f['num_debe'];
                $t_haber+= $f['num_haber'];
                //***********
                $contMax+= 1;

                $consulta2= $this->LibroDiario->metConsulta2Pdf($f['fk_cbb004_num_cuenta']);
                foreach ($consulta2 as $f2) { $cod_cuenta= $f2['cod_cuenta'];}

                $consulta3= $this->LibroDiario->metCentroCosto($f['fk_a023_num_centro_costo']);
                foreach ($consulta3 as $f3) { $ind_descripcion_centro_costo= $f3['ind_descripcion_centro_costo'];}

                if($f['num_haber']>0)$signo="-"; else $signo="";

                $pdf->SetDrawColor(255, 255, 255);$pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont('Arial', '', 7);
                $pdf->SetWidths(array(6, 20, 75, 15, 35, 20, 20));
                $pdf->SetAligns(array('C','C','L','C','C','R','R'));
                $pdf->Row(array($f['num_linea'], $cod_cuenta, utf8_decode($f['txt_titulo_voucher']),
                                $f['fk_a023_num_centro_costo'], '', number_format($f['num_debe'],2,',','.'),
                                $signo.number_format($f['num_haber'],2,',','.')));


                if($contMax>$cantidad){

                       $pdf->SetFont('Arial', 'B', 7);
                       $pdf->Cell(192,6, '________________________________',0,1,'R');

                       $pdf->SetFillColor(202, 202, 202);
                       $pdf->SetFont('Arial', 'B', 7);
                       $pdf->Cell(104,3, '',0,0,'L');
                       $pdf->Cell(48,3,'TOTAL VOUCHER1 ->',0,0,'R');
                       $pdf->Cell(22,3,number_format($t_debe,2,',','.'),0,0,'C');
                       $pdf->Cell(18,3,"-".number_format($t_haber,2,',','.'),0,1,'C');
                       $pdf->ln(3);
                       $t_debe=0; $t_haber=0; $contMax=0; $signo="";
                }
        }
        //salida
        $pdf->Output();
    }
}
