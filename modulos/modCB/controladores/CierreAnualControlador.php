<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-03-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';

class CierreAnualControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->CierreAnual = $this->metCargarModelo('CierreAnual');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );

        $complementosJs = array(
                'bootstrap-datepicker/bootstrap-datepicker',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';

        $this->Contabilidades = Select::parametros('CONTABILIDAD');
        $id = $this->CierreAnual->metContabilidades($this->Contabilidades);
        $form['fk_cbb005_num_contabilidades'] = $id;

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        
        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('CierreAnual');
    }

    /**
    *Permite imprimir en formato "PDF" la información del Cierre Anual ya ejecutado o por ejecutar
    */
    public function metCierreAnualPdf()
    {
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('CabecerasReportePdf','modCB');
        $pdf=new ProcesoCierreAnual('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        //filtro que viene de datos de busqueda insertados y seleccionados
        $filtro= " AND vd.fk_cbb005_num_contabilidades= '".$_POST['fk_cbb005_num_contabilidades']."' AND vd.fec_anio= '".$_POST['fk_cbc005_num_control_cierre']."' ";

          $Asiento= 0; $Monto= "";

          $db = new db();
          $sql = "SELECT *,
                        (CASE WHEN num_monto > 0 THEN 0 ELSE 1 END) AS 'Pos'
                    FROM cb_c008_cierre_anual
                   WHERE
                         fec_anio = '".$_POST['fk_cbc005_num_control_cierre']."' AND
                         fk_cbb005_num_contabilidades = '".$_POST['fk_cbb005_num_contabilidades']."'
                ORDER BY num_asiento ASC, Pos ASC ";
          $row = $db->query($sql);
          $row->setFetchMode(PDO::FETCH_ASSOC);
          $field = $row->fetchAll();

        if (count($field)) {
          foreach($field as $f) {
            if ($Asiento != $f['num_asiento']) {
              //  totales
              if ($Asiento) {
                $pdf->SetDrawColor(0,0,0);
                $pdf->Line(205, $pdf->GetY(), 275, $pdf->GetY());
                $pdf->Ln(1);
                $pdf->SetDrawColor(255,255,255);
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Row(array('', '', number_format($TotalDebe,2,',','.'), number_format($TotalHaber,2,',','.')));
                $pdf->Ln(5);
              }
              $Asiento = $f['num_asiento'];
              $TotalDebe = 0.00;
              $TotalHaber = 0.00;
              //  Asiento.
              $pdf->SetFont('Arial', 'B', 8);
              $pdf->Cell(205, 5, utf8_decode('ASIENTO '.$f['num_asiento'].'.'), 0, 1, 'L');
              $pdf->Ln(3);
            }
            if ($f['num_monto'] >= 0) {
              $MontoDebe = $f['num_monto'];
              $MontoHaber = 0.00;
            }else{
              $MontoDebe = 0.00;
              $MontoHaber = $f['num_monto'];
            }
            $pdf->SetTextColor(0,0,0);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Row(array($f['fk_cbb004_num_cuenta'], utf8_decode($f['ind_descripcion']), number_format($MontoDebe,2,',','.'), number_format($MontoHaber,2,',','.')));
            $pdf->Ln(1);
            ##
            $TotalDebe += $MontoDebe;
            $TotalHaber += $MontoHaber;
          }
          $pdf->SetDrawColor(0,0,0);
          $pdf->Line(205, $pdf->GetY(), 275, $pdf->GetY());
          $pdf->Ln(1);
          $pdf->SetDrawColor(255,255,255);
          $pdf->SetFont('Arial', 'B', 8);
          $pdf->Row(array('', '', number_format($TotalDebe,2,',','.'), number_format($TotalHaber,2,',','.')));
          $pdf->Ln(5);
        }
        else{

          $Descripcion23309 = $this->CierreAnual->metValor("SELECT ind_descripcion FROM cb_b004_plan_cuenta WHERE cod_cuenta = '23309' AND fk_cbb005_num_contabilidades= '$_POST[fk_cbb005_num_contabilidades]' ", "B");
          $Descripcion232990101 = $this->CierreAnual->metValor("SELECT ind_descripcion FROM cb_b004_plan_cuenta WHERE cod_cuenta = '232990101' AND fk_cbb005_num_contabilidades= '$_POST[fk_cbb005_num_contabilidades]' ", "B");

          //#  Asiento 1.
          $pdf->SetFont('Arial', 'B', 8);
          $pdf->Cell(205, 5, utf8_decode('ASIENTO 1.'), 0, 1, 'L');
          $pdf->Ln(3);
          if ($_POST['fk_cbb005_num_contabilidades'] == '1') {

            $db = new db();
            $sql = "SELECT pc.pk_num_cuenta,
                           pc.ind_descripcion,
                           ABS(SUM(vd.num_debe)) AS Monto_debe,
                           ABS(SUM(vd.num_haber)) AS Monto_haber
                      FROM
                           cb_c001_voucher_det vd
                           INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta)
                     WHERE vd.ind_estatus='MA' AND vd.fk_cbb004_num_cuenta = '506' $filtro
                  ORDER BY vd.fk_cbb004_num_cuenta ";
          }
          $row = $db->query($sql);
          $row->setFetchMode(PDO::FETCH_ASSOC);
          $field = $row->fetchAll();
          $Monto51301 = 0;
          foreach($field as $f) {

            $Monto= $f['Monto_debe'] + $f['Monto_haber'];

            $pdf->SetTextColor(0,0,0);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Row(array($f['pk_num_cuenta'], utf8_decode($f['ind_descripcion']), number_format($Monto,2,',','.'), ''));
            $pdf->Ln(1);
            ##
            $Monto51301 += $Monto;
          }
          $pdf->Row(array('23309', utf8_decode($Descripcion23309), '', number_format(-$Monto,2,',','.')));
          $pdf->SetDrawColor(0,0,0);
          $pdf->Line(200, $pdf->GetY(), 270, $pdf->GetY());
          $pdf->Ln(1);
          $pdf->SetDrawColor(255,255,255);
          $pdf->SetFont('Arial', 'B', 8);
          $pdf->Row(array('', '', number_format($Monto,2,',','.'), number_format(-$Monto,2,',','.')));
          $pdf->Ln(5);

          //#  Asiento 2.
          $pdf->SetFont('Arial', 'B', 8);
          $pdf->Cell(205, 5, utf8_decode('ASIENTO 2.'), 0, 1, 'L');
          $pdf->Ln(3);
          if ($_POST['fk_cbb005_num_contabilidades'] == '1') {

            $db = new db();
            $sql = "SELECT pc.pk_num_cuenta,
                           pc.ind_descripcion,
                           ABS(SUM(vd.num_debe)) AS Monto_debe,
                           ABS(SUM(vd.num_haber)) AS Monto_haber
                      FROM
                           cb_c001_voucher_det vd
                           INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta)
                     WHERE vd.ind_estatus='MA' AND vd.fk_cbb004_num_cuenta = '511' $filtro
                  ORDER BY vd.fk_cbb004_num_cuenta ";
          }
          $row = $db->query($sql);
          $row->setFetchMode(PDO::FETCH_ASSOC);
          $field = $row->fetchAll();
          $Monto51303 = 0;
          foreach($field as $f) {
            $Monto= $f['Monto_debe'] + $f['Monto_haber'];

            $pdf->SetTextColor(0,0,0);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Row(array($f['pk_num_cuenta'], utf8_decode($f['ind_descripcion']), number_format($Monto,2,',','.'), ''));
            $pdf->Ln(1);
            ##
            $Monto51303 += $Monto;
          }
          $pdf->Row(array('23309', utf8_decode($Descripcion23309), '', number_format(-$Monto,2,',','.')));
          $pdf->SetDrawColor(0,0,0);
          $pdf->Line(200, $pdf->GetY(), 270, $pdf->GetY());
          $pdf->Ln(1);
          $pdf->SetDrawColor(255,255,255);
          $pdf->SetFont('Arial', 'B', 8);
          $pdf->Row(array('', '', number_format($Monto,2,',','.'), number_format(-$Monto,2,',','.')));
          $pdf->Ln(5);

          //#  Asiento 3.
          $Monto_debe="0";
          $pdf->SetFont('Arial', 'B', 8);
          $pdf->Cell(205, 5, utf8_decode('ASIENTO 3.'), 0, 1, 'L');
          $pdf->Ln(3);
          if ($_POST['fk_cbb005_num_contabilidades'] == '1') {

              $db = new db();
              /*$sql = "SELECT
                             ABS(SUM(vd.num_debe)) AS Monto_debe,
                             ABS(SUM(vd.num_haber)) AS Monto_haber
                        FROM
                             cb_c001_voucher_det vd
                       WHERE vd.ind_estatus='MA' AND vd.fk_cbb004_num_cuenta LIKE '61300%' $filtro
                    ORDER BY vd.fk_cbb004_num_cuenta ";
              */
              $sql = "SELECT
                             ABS(SUM(vd.num_debe)) AS Monto_debe,
                             ABS(SUM(vd.num_haber)) AS Monto_haber
                        FROM
                             cb_c001_voucher_det vd
                             INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta AND pc.cod_cuenta LIKE '61300%')
                       WHERE vd.ind_estatus='MA' $filtro
                    ORDER BY vd.fk_cbb004_num_cuenta ";
          }
          $row = $db->query($sql);
          $row->setFetchMode(PDO::FETCH_ASSOC);
          $field = $row->fetchAll();
          foreach($field as $f) {
            $Monto+= $f['Monto_debe'] + $f['Monto_haber'];
            $Monto_debe+= $Monto;
          }
          $pdf->SetTextColor(0,0,0);
          $pdf->SetDrawColor(255,255,255);
          $pdf->SetFillColor(255,255,255);
          $pdf->SetFont('Arial', '', 8);
          $pdf->Row(array('23309', utf8_decode($Descripcion23309), number_format($Monto,2,',','.'), ''));
          $pdf->Ln(1);
          if ($_POST['fk_cbb005_num_contabilidades'] == '1') {

            $db = new db();
            /*
            $sql = "SELECT pc.pk_num_cuenta,
                           pc.ind_descripcion,
                           SUM(vd.num_debe) AS Monto_debe,
                           SUM(vd.num_haber) AS Monto_haber
                      FROM
                           cb_c001_voucher_det vd
                           INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta)
                     WHERE vd.ind_estatus='MA' AND vd.fk_cbb004_num_cuenta LIKE '61300%' $filtro
                  GROUP BY vd.fk_cbb004_num_cuenta
                  ORDER BY vd.fk_cbb004_num_cuenta ";
            */
            $sql = "SELECT pc.cod_cuenta,
                           pc.ind_descripcion,
                           SUM(vd.num_debe) AS Monto_debe,
                           SUM(vd.num_haber) AS Monto_haber
                      FROM
                           cb_c001_voucher_det vd
                           INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta AND pc.cod_cuenta LIKE '61300%')
                     WHERE vd.ind_estatus='MA'  $filtro
                  GROUP BY vd.fk_cbb004_num_cuenta
                  ORDER BY vd.fk_cbb004_num_cuenta ";
          }
          $row = $db->query($sql);
          $row->setFetchMode(PDO::FETCH_ASSOC);
          $field = $row->fetchAll();
          $Monto61300 = 0;
          $Monto = 0;
          foreach($field as $f) {
            $Monto= $f['Monto_debe'] + $f['Monto_haber'];

            $pdf->SetTextColor(0,0,0);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Row(array($f['cod_cuenta'], utf8_decode($f['ind_descripcion']), '', number_format(-$Monto,2,',','.')));
            $pdf->Ln(1);
            ##
            $Monto61300 += $Monto;
          }
          $pdf->SetDrawColor(0,0,0);
          $pdf->Line(200, $pdf->GetY(), 270, $pdf->GetY());
          $pdf->Ln(1);
          $pdf->SetDrawColor(255,255,255);
          $pdf->SetFont('Arial', 'B', 8);
          //$pdf->Row(array('', '', number_format($Monto,2,',','.'), number_format(-$Monto61300,2,',','.')));
          $pdf->Row(array('', '', number_format($Monto_debe,2,',','.'), number_format(-$Monto61300,2,',','.')));

          $pdf->Ln(5);

          //#  Asiento 4.
          $pdf->SetFont('Arial', 'B', 8);
          $pdf->Cell(205, 5, utf8_decode('ASIENTO 4.'), 0, 1, 'L');
          $pdf->Ln(3);
          $MontoDebe = $Monto51301 + $Monto51303 - $Monto61300;
          $MontoHaber = -$MontoDebe;
          $pdf->SetTextColor(0,0,0);
          $pdf->SetDrawColor(255,255,255);
          $pdf->SetFillColor(255,255,255);
          $pdf->SetFont('Arial', '', 8);
          $pdf->Row(array('23309', utf8_decode($Descripcion23309), number_format($MontoDebe,2,',','.'), ''));
          $pdf->Ln(1);
          $pdf->Row(array('232990101', utf8_decode($Descripcion232990101), '', number_format($MontoHaber,2,',','.')));
          $pdf->SetDrawColor(0,0,0);
          $pdf->Line(200, $pdf->GetY(), 270, $pdf->GetY());
          $pdf->Ln(1);
          $pdf->SetDrawColor(255,255,255);
          $pdf->SetFont('Arial', 'B', 8);
          $pdf->Row(array('', '', number_format($MontoDebe,2,',','.'), number_format($MontoHaber,2,',','.')));
        }

        //salida
        $pdf->Output();
    }

    /**
    *Realiza la acción del ejecutese del cierre anual una vez hecho click en botón "Ejecutar Cierre"
    *@param DEP_CIERRE_ANUAL y CCOSTO_CIERRE_ANUAL utilizados sólo para el cierre anual
    */
    public function metEjecutarCierre()
    {
       $CodDependencia= $this->CodDependencia = Select::parametros('DEP_CIERRE_ANUAL'); 
       $ccosto_cierreAnual= $this->ccosto_cierreAnual = Select::parametros('CCOSTO_CIERRE_ANUAL'); 
       $this->CierreAnual->metEjecutar($_POST['fperiodo'], $_POST['fcontabilidad'], $CodDependencia, $ccosto_cierreAnual);
    }

}
