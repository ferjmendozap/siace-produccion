<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodr�guez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';

class LibroContableControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->LibroContable = $this->metCargarModelo('LibroContable');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->LibroContable->metListar());
        $this->atVista->metRenderizar('LibroContable');
    }

    /**
    * Función que permite cargar formulario para el ingreso del nuevo registro
    */
    public function metNuevo()
    {

      ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'cod_libro' => null,
            'ind_descripcion' => null,
            'num_estatus' => null,
            'pk_num_libro_contable' => null,
            'estado' => 'nuevo',

        ];

        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('LibroContableCrear','modales');
    }

    /**
    * Función que permite cargar el proceso para el ingreso del nuevo registro
    */
    public function metCrear()
    {
      ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'cod_libro' => 'required|alpha|max_len,2',
            'ind_descripcion' => 'required|alpha_space|max_len,30',
        ]);

        ##  validación exitosa
        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        if($validate === TRUE) {
           # code...
           ##  crear registro
            $id = $this->LibroContable->metNuevo($_POST);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' => '<tr id="id'.$id.'">
                                <td><label>'.$id.'</label></td>
                                <td><label>'.$_POST['cod_libro'].'</label></td>
                                <td><label>'.$_POST['ind_descripcion'].'</label></td>
                                <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                                <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Registro" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Libro Contable" titulo="Ver Libro Contable">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                               </td>
                        </tr>',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el formulario para la actualización de un registro
    */
    public function metEditar()
    {
        ##  valores del formulario
        $db = $this->LibroContable->metMostrar($_POST['id']);
        $db['metodo'] = 'modificar';
        $db['estado'] = 'editar';
        ##  vista
        $this->atVista->assign('form', $db);
        $this->atVista->metRenderizar('LibroContableCrear', 'modales');
    }

    /**
    * Función que permite cargar el formulario mostrando datos del registro
    */
    public function metVer()
    {
        ##  valores del formulario
        $db = $this->LibroContable->metMostrar($_POST['id']);
        $db['metodo'] = 'ver';
        $db['estado']= $_POST['estado'];
        ##  vista
        $this->atVista->assign('form', $db);
        $this->atVista->metRenderizar('LibroContableCrear', 'modales');
    }

    /**
    * Función que permite cargar el proceso para el editado de un registro
    */
    public function metModificar()
    {
        $id = $_POST['pk_num_libro_contable'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'cod_libro' => 'required|alpha|max_len,2',
            'ind_descripcion' => 'required|alpha_space|max_len,30',
        ]);

        ##  validación exitosa
        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        if($validate === TRUE) {

           ##  crear registro
           $this->LibroContable->metEditar($_POST);
           ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                        <td><label>'.$id.'</label></td>
                        <td><label>'.$_POST['cod_libro'].'</label></td>
                        <td><label>'.$_POST['ind_descripcion'].'</label></td>
                        <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                        <td align="center">
                    '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Libro Contable" title="Editar">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>':'').'

                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Libro Contable" titulo="Ver Libro Contable">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                    '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                    boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>':'').'
                       </td>
                    ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el proceso de eliminado de un registro
    */
    public function metEliminar()
    {

        $this->LibroContable->metEliminar(['pk_num_libro_contable' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

}
