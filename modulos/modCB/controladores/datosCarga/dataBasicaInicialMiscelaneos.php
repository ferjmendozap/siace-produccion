<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION

 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************************************************************/
trait dataBasicaInicialMiscelaneos
{

    public function metMiscelaneos()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo CB<br>';
        $a005_miscelaneo_maestro = array(
          array('num_nombre_maestro' => 'CUENTPUB20', 'num_descripcion' => 'TIPOS DE CUENTAS PUBLICACION 20', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '10', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CUENTPUB20',
                'Det' => array(
                array('cod_detalle' => 'CD', 'num_nombre_detalle' => 'CUENTAS DEL PATRIMONIO', 'num_estatus' => '1'),
                array('cod_detalle' => 'CH', 'num_nombre_detalle' => 'CUENTAS DE LA HACIENDA', 'num_estatus' => '1'),
                array('cod_detalle' => 'CP', 'num_nombre_detalle' => 'CUENTAS DEL PRESUPUESTO', 'num_estatus' => '1'),
                array('cod_detalle' => 'CR', 'num_nombre_detalle' => 'CUENTAS DEL RESULTADO DEL PRESUPUESTO', 'num_estatus' => '1'),
                array('cod_detalle' => 'CT', 'num_nombre_detalle' => 'CUENTAS DEL TESORO', 'num_estatus' => '1'),
                )
          ),
          array('num_nombre_maestro' => 'CUENTONCOP', 'num_descripcion' => 'TIPOS DE CUENTAS CONTABILIDAD', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '10', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CUENTONCO',
                'Det' => array(
                array('cod_detalle' => 'BA', 'num_nombre_detalle' => 'BALANCE ACTIVO', 'num_estatus' => '1'),
                array('cod_detalle' => 'BP', 'num_nombre_detalle' => 'BALANCE PASIVO', 'num_estatus' => '1'),
                array('cod_detalle' => 'CO', 'num_nombre_detalle' => 'CUENTA DE ORDEN', 'num_estatus' => '1'),
                array('cod_detalle' => 'CP', 'num_nombre_detalle' => 'CAPITAL', 'num_estatus' => '1'),
                array('cod_detalle' => 'EG', 'num_nombre_detalle' => 'EGRESO', 'num_estatus' => '1'),
                array('cod_detalle' => 'IN', 'num_nombre_detalle' => 'INGRESO', 'num_estatus' => '1'),
                array('cod_detalle' => 'OT', 'num_nombre_detalle' => 'OTROS', 'num_estatus' => '1'),
                )
         ),
        );

        return $a005_miscelaneo_maestro;
    }

    public function metDataMiscelaneos()
    {
        $a005_miscelaneo_maestro = array_merge(
            $this->metMiscelaneos()
        );
        return $a005_miscelaneo_maestro;
    }

}
