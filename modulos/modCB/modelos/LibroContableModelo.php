<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class LibroContableModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * Obtengo datos de tabla para cargar listado de registros
    */
    public function metListar()
    {
        $db = $this->_db->query("SELECT * FROM cb_b002_libro_contable");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Obtengo datos de tabla con búsqueda específica de libro contable
    */
    public function metMostrar($idLibroContable)
    {
        $db = $this->_db->query("SELECT * FROM cb_b002_libro_contable WHERE pk_num_libro_contable='$idLibroContable'");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    /*
    * Ejecuta el proceso de registro del nuevo libro contable
    */
    public function metNuevo($data)
    {
        $this->_db->beginTransaction();

             $sql ="INSERT INTO cb_b002_libro_contable
                            SET
                                cod_libro= '$data[cod_libro]',
                                ind_descripcion= '$data[ind_descripcion]',
                                num_estatus= '$data[num_estatus]',
                                fk_a018_num_seguridad_usuario= '$this->idUsuario',
                                fec_ultima_modificacion= NOW() ";

        $db = $this->_db->prepare($sql);
        $db->execute();

        $id = $this->_db->lastInsertId();

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /*
    * Ejecuta el proceso de editado de un libro contable
    */
    public function metEditar($data)
    {
         $this->_db->beginTransaction();

             $sql ="UPDATE cb_b002_libro_contable
                       SET
                          cod_libro='$data[cod_libro]',
                          ind_descripcion=  '$data[ind_descripcion]',
                          num_estatus= '$data[num_estatus]',
                          fk_a018_num_seguridad_usuario = '$this->idUsuario',
                          fec_ultima_modificacion = NOW()
                    WHERE
                          pk_num_libro_contable='$data[pk_num_libro_contable]'";

         $db = $this->_db->prepare($sql);
         $db->execute();

         ##  Consigna una transacción
         $this->_db->commit();
    }

    /*
    * Ejecuta el proceso de eliminar un libro contable
    */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "DELETE FROM cb_b002_libro_contable WHERE pk_num_libro_contable = '$data[pk_num_libro_contable]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }
}
