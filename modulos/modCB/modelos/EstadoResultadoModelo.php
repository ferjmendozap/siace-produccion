<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class EstadoResultadoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * permite consultar el contabilidad del voucher
    * @return pk_num_contabilidades
    */
    public function metContabilidades($data)
    {
       $db= $this->_db->query("SELECT pk_num_contabilidades FROM cb_b005_contabilidades WHERE  ind_contabilidad_acronimo='$data' ");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       $field= $db->fetch();
       return $field['pk_num_contabilidades'];
    }

    /*
    * Obtengo saldo de las cuentas involucradas según condiciones
    */
    public function metObtenerSaldo($Rubro, $fContabilidad, $fanio, $fmes, $filtro1)
    {

        $db= $this->_db->query("SELECT a.*,
                                       b.ind_descripcion,
                                       b.num_nivel
                                  FROM cb_c007_balance_cuenta a
                                       INNER JOIN cb_b004_plan_cuenta b on (b.pk_num_cuenta=a.fk_cbb004_num_cuenta and b.ind_rubro='$Rubro')
                                 WHERE
                                       a.fec_anio= '$fanio' and
                                       a.fk_cbb005_num_contabilidades= '$fContabilidad'  $filtro1
                              ORDER BY a.fk_cbb004_num_cuenta");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       $field= $db->fetchAll();

       $saldo="0";

           foreach ($field as $field_a) {

              if($fmes>='01') $saldo+= $field_a['num_saldo_inicial'] + $field_a['num_saldo_balance01'];
                if($fmes>='02') $saldo+= $field_a['num_saldo_balance02'];
                if($fmes>='03') $saldo+= $field_a['num_saldo_balance03'];
                if($fmes>='04') $saldo+= $field_a['num_saldo_balance04'];
                if($fmes>='05') $saldo+= $field_a['num_saldo_balance05'];
                if($fmes>='06') $saldo+= $field_a['num_saldo_balance06'];
                if($fmes>='07') $saldo+= $field_a['num_saldo_balance07'];
                if($fmes>='08') $saldo+= $field_a['num_saldo_balance08'];
                if($fmes>='09') $saldo+= $field_a['num_saldo_balance09'];
                if($fmes>='10') $saldo+= $field_a['num_saldo_balance10'];
                if($fmes>='11') $saldo+= $field_a['num_saldo_balance11'];
                if($fmes=='12') $saldo+= $field_a['num_saldo_balance12'];
           }

     return $saldo;
    }

    /*
    * Obtengo saldo de las cuentas involucradas según condiciones
    */
    public function metObtenerSaldo2($Rubro, $fContabilidad, $fanio, $fmes, $filtro1, $cod_cuenta)
    {

        $db= $this->_db->query("SELECT a.*,
                                       b.ind_descripcion,
                                       b.num_nivel
                                  FROM cb_c007_balance_cuenta a
                                       INNER JOIN cb_b004_plan_cuenta b on (b.pk_num_cuenta=a.fk_cbb004_num_cuenta and b.ind_rubro='$Rubro' and b.Cod_Cuenta='$cod_cuenta')
                                 WHERE
                                       a.fec_anio= '$fanio' and
                                       a.fk_cbb005_num_contabilidades= '$fContabilidad'  $filtro1
                              ORDER BY a.fk_cbb004_num_cuenta");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       $field= $db->fetchAll();

       $saldo="0";

           foreach ($field as $field_a) {

              if($fmes>='01') $saldo+= $field_a['num_saldo_inicial'] + $field_a['num_saldo_balance01'];
                if($fmes>='02') $saldo+= $field_a['num_saldo_balance02'];
                if($fmes>='03') $saldo+= $field_a['num_saldo_balance03'];
                if($fmes>='04') $saldo+= $field_a['num_saldo_balance04'];
                if($fmes>='05') $saldo+= $field_a['num_saldo_balance05'];
                if($fmes>='06') $saldo+= $field_a['num_saldo_balance06'];
                if($fmes>='07') $saldo+= $field_a['num_saldo_balance07'];
                if($fmes>='08') $saldo+= $field_a['num_saldo_balance08'];
                if($fmes>='09') $saldo+= $field_a['num_saldo_balance09'];
                if($fmes>='10') $saldo+= $field_a['num_saldo_balance10'];
                if($fmes>='11') $saldo+= $field_a['num_saldo_balance11'];
                if($fmes=='12') $saldo+= $field_a['num_saldo_balance12'];
           }

     return $saldo;
    }

    /*
    * Obtengo datos de las cuentas
    */
    public function metPlanCuenta($tipo_cuenta, $nivel, $grupo, $subgrupo)
    {
        $db= $this->_db->query("SELECT *
                                  FROM
                                       cb_b004_plan_cuenta
                                 WHERE
                                       num_flag_tipo_cuenta= '$tipo_cuenta' AND
                                       num_nivel= '$nivel' AND
                                       ind_grupo= '$grupo'  AND
                                       ind_subgrupo= '$subgrupo' AND
                                       num_estatus= '1' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Obtengo datos de las cuentas
    */
    public function metPlanCuenta2($tipo_cuenta, $nivel, $grupo, $subgrupo, $valor)
    {
        $db= $this->_db->query("SELECT *
                                  FROM
                                       cb_b004_plan_cuenta
                                 WHERE
                                       num_flag_tipo_cuenta= '$tipo_cuenta' AND
                                       num_nivel= '$nivel' AND
                                       ind_grupo= '$grupo'  AND
                                       ind_subgrupo= '$subgrupo' AND
                                       cod_cuenta= '$valor' AND
                                       num_estatus= '1' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Obtengo datos de las cuentas
    */
    public function metPlanCuenta3($tipo_cuenta, $grupo, $subgrupo, $cod_cuenta, $valor)
    {
        $db= $this->_db->query("SELECT *
                                  FROM
                                       cb_b004_plan_cuenta
                                 WHERE
                                       num_flag_tipo_cuenta= '$tipo_cuenta' AND
                                       ind_grupo= '$grupo'  AND
                                       ind_subgrupo= '$subgrupo' AND
                                       Cod_Cuenta= '$cod_cuenta' AND
                                       num_estatus= '1'  $valor ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

}
