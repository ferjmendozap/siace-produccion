<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class TipoVoucherModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * Obtengo datos de tabla para cargar listado de registros
    */
    public function metListar()
    {
        $db = $this->_db->query("SELECT * FROM cb_c003_tipo_voucher");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Ejecuta el proceso de registro del nuevo sistema fuente
    */
    public function metNuevo($data)
    {
        $this->_db->beginTransaction();

             $sql ="INSERT INTO cb_c003_tipo_voucher
                             SET
                                cod_voucher= '$data[cod_voucher]',
                                num_flag_manual= '$data[num_flag_manual]',
                                ind_descripcion= '$data[ind_descripcion]',
                                num_estatus= '$data[num_estatus]',
                                fk_a018_num_seguridad_usuario = '$this->idUsuario',
                                fec_ultima_modificacion = NOW() ";

        $db = $this->_db->prepare($sql);
        $db->execute();

        $id = $this->_db->lastInsertId();

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /*
    * Obtengo datos de tabla con búsqueda específica de sistema fuente
    */
    public function metMostrar($id)
    {
        $db = $this->_db->query("SELECT * FROM cb_c003_tipo_voucher WHERE pk_num_voucher='$id'");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    /*
    * Ejecuta el proceso de editado de registro de sistema fuente
    */
    public function metEditar($data)
    {
        $this->_db->beginTransaction();

             $sql ="UPDATE cb_c003_tipo_voucher
                       SET
                          cod_voucher= '$data[cod_voucher]',
                          ind_descripcion= '$data[ind_descripcion]',
                          num_flag_manual= '$data[num_flag_manual]',
                          num_estatus= '$data[num_estatus]',
                          fk_a018_num_seguridad_usuario = '$this->idUsuario',
                          fec_ultima_modificacion = NOW()
                    WHERE
                          pk_num_voucher='$data[pk_num_voucher]'";

         $db = $this->_db->prepare($sql);
         $db->execute();

         ##  Consigna una transacción
         $this->_db->commit();
    }

    /*
    * Ejecute el proceso de eliminar registro
    */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ## verifico si esta siendo utilizado por algún voucher
        $s_con= $this->_db->query("SELECT *
                                     FROM cb_b001_voucher
                                    WHERE fk_cbc003_num_voucher= '$data[pk_num_voucher]' ");
        $s_con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado= $s_con->rowCount();

        if ($resultado!=0) {
            $id=1;
        }else {

            $sql = "DELETE FROM cb_c003_tipo_voucher WHERE pk_num_voucher= '$data[pk_num_voucher]'";
            $db = $this->_db->prepare($sql);
            $db->execute();

            $id=0;
        }
        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

}
