<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class BalanceComprobacionModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
    * permite consultar el contabilidad del voucher
    * @return pk_num_contabilidades
    */
    public function metContabilidades($data)
    {
       $db= $this->_db->query("SELECT pk_num_contabilidades FROM cb_b005_contabilidades WHERE  ind_contabilidad_acronimo='$data' ");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       $field= $db->fetch();
       return $field['pk_num_contabilidades'];
    }

    public function metBalanceReportePdf($contabilidad, $tipo_cuenta, $fec_anio, $filtro_cuenta)
    {
        $db = $this->_db->query("SELECT a.*,
                                        b.ind_descripcion,
                                        b.num_nivel,
                                        b.cod_cuenta,
                                        b.pk_num_cuenta,
                                        c.ind_tipo_contabilidad,
                                        d.pk_num_libro_contabilidad
                                  FROM
                                        cb_c007_balance_cuenta a
                                        INNER JOIN cb_b004_plan_cuenta b ON (b.pk_num_cuenta=a.fk_cbb004_num_cuenta AND
                                                                             b.num_flag_tipo_cuenta='$tipo_cuenta')
                                        INNER JOIN cb_b005_contabilidades c ON (c.pk_num_contabilidades= a.fk_cbb005_num_contabilidades)
                                        INNER JOIN cb_b003_libro_contabilidad d ON (d.pk_num_libro_contabilidad=a.fk_cbb003_num_libro_contabilidad)
                                 WHERE
                                        a.fec_anio= '$fec_anio' AND
                                        a.fk_cbb005_num_contabilidades= '$contabilidad' $filtro_cuenta
                              GROUP BY
                                        b.cod_cuenta 
                                        ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    public function metDetalleReporte($mes, $anio, $num_libro_contabilidad, $pk_num_cuenta)
    {
        $db= $this->_db->query("SELECT 
                                    sum(a.num_debe) AS num_debe,
                                    sum(a.num_haber) AS num_haber
                                FROM cb_c001_voucher_det a
                                     INNER JOIN cb_b001_voucher b on (b.pk_num_voucher_mast=a.fk_cbb001_num_voucher_mast and
                                                                      b.ind_mes=a.ind_mes and
                                                                      b.ind_anio=a.fec_anio)
                               WHERE a.ind_mes='$mes'  and
                                     a.fec_anio='$anio' and
                                     a.ind_estatus='MA' and
                                     a.fk_cbb004_num_cuenta='$pk_num_cuenta' and
                                     a.fk_cbb003_num_libro_contabilidad='$num_libro_contabilidad' ");

        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    public function metConsulta2Pdf($cod_cuenta, $tipo_cuenta)
    {
        $db= $this->_db->query("SELECT ind_descripcion,
                                       cod_cuenta
                                  FROM cb_b004_plan_cuenta
                                 WHERE cod_cuenta= '$cod_cuenta' AND
                                       num_flag_tipo_cuenta= '$tipo_cuenta' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    public function metConsulta3Pdf($mes, $anio, $pk_num_cuenta, $contabilidad, $tipo_cuenta)
    {
        /*
        $db= $this->_db->query("SELECT
                                      DISTINCT(a1.fk_cbb004_num_cuenta),
                                      sum(a1.num_debe) as Deudor,
                                      (Select
                                             sum(a2.num_haber)
                                         From
                                             cb_c001_voucher_det a2
                                        Where
                                             a1.fk_cbb004_num_cuenta=a2.fk_cbb004_num_cuenta and
                                             a1.fec_anio= a2.fec_anio and
                                             a1.ind_mes= a2.ind_mes and
                                             a2.num_haber>0 and
                                             a1.fk_cbb005_num_contabilidades= a2.fk_cbb005_num_contabilidades and
                                             a1.ind_estatus=a2.ind_estatus
                                     Group By
                                             a2.fk_cbb004_num_cuenta, a2.fec_anio, a2.ind_mes) as Acreedor
                                 FROM
                                     cb_c001_voucher_det a1
                                WHERE
                                  a1.fk_cbb004_num_cuenta= '$pk_num_cuenta' AND
                                  a1.fec_anio= '$anio' AND
                                  a1.ind_mes= '$mes'  AND
                                  a1.num_debe>'0' OR a1.num_haber>'0' AND
                                  a1.fk_cbb005_num_contabilidades= '$contabilidad' AND
                                  a1.ind_estatus= 'MA'
                                GROUP BY
                                  a1.fk_cbb004_num_cuenta, a1.fec_anio, a1.ind_mes ");
        $db->setFetchMode(PDO::FETCH_ASSOC);*/

        $db= $this->_db->query("SELECT
                                      sum(a.num_debe) as Deudor, 
                                      sum(a.num_haber) as Acreedor
                                 FROM
                                     cb_c001_voucher_det a
                                     inner join cb_b004_plan_cuenta b on (b.pk_num_cuenta=a.fk_cbb004_num_cuenta and
                                                                          b.num_flag_tipo_cuenta='$tipo_cuenta')
                                WHERE
                                     a.fk_cbb004_num_cuenta= '$pk_num_cuenta' AND
                                     a.fec_anio= '$anio' AND
                                     a.ind_mes= '$mes'  AND
                                     a.fk_cbb005_num_contabilidades= '$contabilidad' AND
                                     a.ind_estatus= 'MA'
                                GROUP BY
                                     a.fk_cbb004_num_cuenta, a.fec_anio, a.ind_mes ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }
}
