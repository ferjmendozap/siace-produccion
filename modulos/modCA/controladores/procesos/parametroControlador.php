<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación
class parametroControlador extends Controlador
{
    private $atParametroModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atParametroModelo = $this->metCargarModelo('parametro');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('_PruebaPost', $this->atParametroModelo->metListar());
        $this->atVista->metRenderizar('listado');
    }


    //Método que permite eliminar
    public function metEliminar()
    {
        $pk_grupo_tecnica = $this->metObtenerInt('pk_grupo_tecnica');
        $this->atParametroModelo->metEliminar($pk_grupo_tecnica);
        $arrayPost = array(
            'status' => 'OK',
            'pkNumSolicitud' => $pk_grupo_tecnica
        );
        echo json_encode($arrayPost);
    }


    public function metNuevo()
    {
        // La Funcion metObtenerInt valida que los datos enviados por post hacia el controlador sean solo Alpha-Numerico
        if (isset($_POST['valido'])) {
            $valido = $_POST['valido'];
        }
        // Verifica que el formulario esté definido y que sea válido
        if (isset($valido) && $valido == 1) {
            //$this->metValidarToken();
            $ind_dependencia = $_POST['ind_dependencia'];
            $ind_descripcion = $_POST['ind_descripcion'];
            $usuario = Session::metObtener('idUsuario');
            $fechaHora = date('Y-m-d H:i:s');
            // Se cargan los métodos del modelo a utilizar y los campos del formulario con datos
            $pk_registro = $this->atParametroModelo->metGuardarGrupo($ind_dependencia, $ind_descripcion);
            // Se crea un array con el nombre de los campos que se encuentran en la base de datos y se le asignan las variables recibidas en el formulario
            $grupo = $this->atParametroModelo->metListaGrupo($pk_registro);
            $dependencia = $this->atParametroModelo->metDependenciaGrupo($pk_registro);
            $arrayPost = array(
                'grupo' => $grupo['grupo'],
                'dependenciass' => $dependencia['ind_dependencia'],
            );
            // Se retorna el array en forma de JSON
            echo json_encode($arrayPost);
            // Salimos de la instrucción
            exit;
        }

        // Si el formulario se va a cargar vacio para proceder al llenado; entonces se le asigna el valor null a cada campo
        $form = array(
            'status' => 'nuevo',
            'fk_a003_num_persona' => null,
            'fk_a004_num_dependencia' => null,
        );
        // Se llaman a los métodos del modelo a utilizar. En este caso metListarUsuario que consulta el listado de empleados, los cuales serán los futuros usuarios de los equipos.
        // y el metodo metListarDependencia, el cual lista las dependencias internas de la Contraloría.
        $usuario = $this->atParametroModelo->metListarUsuario();
        $this->atVista->assign('usuario', $usuario);

        $dependencia = $this->atParametroModelo->metDependencia();
        $this->atVista->assign('dependencia', $dependencia);
        $this->atVista->assign('form', $form);

        $this->atVista->metRenderizar('nuevo', 'modales');
    }


    public function metEditar()
    {
        $valido = $this->metObtenerInt('valido');
        $pk_num_actividad = $this->metObtenerInt('pk_num_actividad');
        if (isset($valido) && $valido == 1) {
            $ind_nombre_actividad = $this->metObtenerTexto('ind_nombre_actividad');
            $ind_orden_actividad = $this->metObtenerInt('ind_orden_actividad');
            $ind_estatus = $this->metObtenerInt('ind_estatus');
            $this->metValidarToken();
            $this->atParametroModelo->metEditar($ind_nombre_actividad, $ind_estatus, $pk_num_actividad);
            $arrayPost = array(
                'status' => 'modificar',
                'pk_num_actividad' => $pk_num_actividad,
                'ind_nombre_actividad' => $ind_nombre_actividad,
                'ind_estatus' => $ind_estatus
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pk_num_actividad)) {
            $form = $this->atParametroModelo->metVer($pk_num_actividad, 1);
            $form = array(
                'status' => '1',
                'pk_num_actividad' => $pk_num_actividad,
                'ind_nombre_actividad' => $form['ind_nombre_actividad'],
                'ind_orden_actividad' => $form['ind_orden_actividad'],
                'ind_estatus' => $form['ind_estatus']
            );
            $selector = $this->atParametroModelo->metSelector();
            $this->atVista->assign('selector', $selector);

            $this->atVista->assign('form', $form);

            $this->atVista->metRenderizar('editar', 'modales');
        }
    }


}

?>