<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de tipos de documento
class reporteSolicitudControlador extends Controlador
{
    private $atReporteModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo de solicitud.
        $this->atReporteModelo = $this->metCargarModelo('reporte');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('listado', $this->atReporteModelo->metListarTodo($usuario));
        $this->atVista->metRenderizar('listReporte');
    }




    // Método que permite ver el reporte de solicitudes
        public function metImprimirSolicitudes()
        {
            $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
            $pdf = new pdfSolicitud('L', 'mm', 'Letter');
            $pdf->AliasNbPages();
            $pdf->AddPage();
            $pdf->SetAutoPageBreak(true, 10);
            $pdf->SetFont('Arial', '', 10);
            $listarArchivo = $this->atReporteModelo->metListarTodoReporte();


            $pdf->SetWidths(array(10,15,20,20,30, 80, 30, 30,30));
            foreach ($listarArchivo as $listarArchivo) {

                if ($listarArchivo['fec_cerrado'] !=null)
                {
                    $ultimafecha=$listarArchivo['fec_cerrado'];
                }
              else if ($listarArchivo['fec_evaluado'] !=null)
                {
                    $ultimafecha=$listarArchivo['fec_evaluado'];
                }
                else if ($listarArchivo['fec_culminado'] !=null)
                {
                    $ultimafecha=$listarArchivo['fec_culminado'];
                }
                else if ($listarArchivo['fec_ejecutado'] !=null)
                {
                    $ultimafecha=$listarArchivo['fec_ejecutado'];
                }
                else if ($listarArchivo['fec_asignado'] !=null)
                {
                    $ultimafecha=$listarArchivo['fec_asignado'];
                }
                else if ($listarArchivo['fec_revisado'] !=null)
                {
                    $ultimafecha=$listarArchivo['fec_revisado'];
                }
                else if ($listarArchivo['fec_aprobado'] !=null)
                {
                    $ultimafecha=$listarArchivo['fec_aprobado'];
                }
               else
                {
                    $ultimafecha=$listarArchivo['fec_creado'];
                }


                $pdf->Row(array(
                    utf8_decode($listarArchivo['pk_num_solicitud']),
                    utf8_decode($listarArchivo['fecha_solicitud']),
                    utf8_decode($listarArchivo['dependencia']),
                    utf8_decode($listarArchivo['funcionario']),
                    utf8_decode($listarArchivo['descrip']),
                    utf8_decode($listarArchivo['ind_detalles']),
                    $listarArchivo['nombres'],
                    $listarArchivo['ind_estatus'],
                    $ultimafecha
                ), 6);
            }

            $pdf->Output();
        }
        // Método que verifica la existencia de un tipo de documento
    }


