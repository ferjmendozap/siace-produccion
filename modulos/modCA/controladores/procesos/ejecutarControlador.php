<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación
class ejecutarControlador extends Controlador
{
    private $atEjecutarModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atEjecutarModelo = $this->metCargarModelo('ejecutar');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('_PruebaPost', $this->atEjecutarModelo->metListar());
        $this->atVista->metRenderizar('listado');
    }


    //Método que permite actualizar el estado de la solicitud
    public function metEditar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $valido = $this->metObtenerInt('valido');
        $complementosCss = array(
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array('bootstrap-datepicker/bootstrap-datepicker');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        if (isset($valido) && $valido == 1) {
            $ind_detalles_analista = $this->metObtenerTexto('ind_detalles_analista');
            $ind_fecha_inicio = $this->metObtenerFormulas('ind_fecha_inicio');
            $ind_fecha_fin = $this->metObtenerFormulas('ind_fecha_fin');
            $this->metValidarToken();
            $this->atEjecutarModelo->metEditar($pkNumSolicitud, $ind_detalles_analista, $ind_fecha_inicio, $ind_fecha_fin);
            $arrayPost = array(
                'status' => 'modificar',
                'ind_detalles_analista' => $ind_detalles_analista,
                'pk_num_solicitud' => $pkNumSolicitud,
                'ind_fecha_inicio' => $ind_fecha_inicio,
                'ind_fecha_fin' => $ind_fecha_fin
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pkNumSolicitud)) {
            $form = $this->atEjecutarModelo->metVer($pkNumSolicitud, 1);
            $form = array(
                'status' => '1',
                'ind_detalles' => $form['ind_detalles'],
                'ind_detalles_analista' => $form['ind_detalles_analista'],
                'ind_detalles_supervisor' => $form['ind_detalles_supervisor'],
                'pk_num_solicitud' => $pkNumSolicitud
            );
            $usuario = $this->atEjecutarModelo->metListarUsuario();
            $this->atVista->assign('usuario', $usuario);
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('editar', 'modales');
        }
    }

    //Método que permite eliminar
    public function metEliminar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pkNumSolicitud');
        $this->atEjecutarModelo->metEliminar($pkNumSolicitud);
        $arrayPost = array(
            'status' => 'OK',
            'pkNumSolicitud' => $pkNumSolicitud
        );
        echo json_encode($arrayPost);
    }

    //Método que permite visualizar
    public function metVer()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $almacen = $this->atEjecutarModelo->metVer($pkNumSolicitud, 2);
        $usuario = $this->atEjecutarModelo->metVerUsuario($pkNumSolicitud);
        $this->atVista->assign('usuario', $usuario);
        $this->atVista->assign('almacen', $almacen);
        $this->atVista->metRenderizar('ver', 'modales');
    }

    // Método que permite verificar
    public function metVerificar()
    {
        $pk_num_solicitud = $this->metObtenerInt("pk_num_solicitud");
        $almacenExiste = $this->atEjecutarModelo->metBuscar($pk_num_solicitud);
        echo json_encode(!$almacenExiste);
    }
}

?>