<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de solicitudes por evaluar
class evaluarControlador extends Controlador
{
    private $atEvaluarModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atEvaluarModelo = $this->metCargarModelo('evaluar');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('_PruebaPost', $this->atEvaluarModelo->metListar());
        $this->atVista->metRenderizar('listado');
    }

    //Método que permite actualizar el estado de la solicitud por evaluar
    public function metEditar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $ind_evaluacion = $this->metObtenerTexto('ind_evaluacion');
            $ind_sugerencias = $this->metObtenerTexto('ind_sugerencias');
            $this->metValidarToken();
            $this->atEvaluarModelo->metEditar($pkNumSolicitud, $ind_evaluacion, $ind_sugerencias);
            $arrayPost = array(
                'status' => 'modificar',
                'pk_num_solicitud' => $pkNumSolicitud,
                'ind_evaluacion' => $ind_evaluacion,
                'ind_sugerencias' => $ind_sugerencias
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pkNumSolicitud)) {
            $form = $this->atEvaluarModelo->metVer($pkNumSolicitud, 1);
            $form = array(
                'status' => '1',
                'ind_detalles' => $form['ind_detalles'],
                'pk_num_solicitud' => $pkNumSolicitud,
                'ind_detalles_analista_final' => $form['ind_detalles_analista_final']
            );

            $evaluacion = $this->atEvaluarModelo->metResultadoEvaluacion();
            $this->atVista->assign('evaluacion', $evaluacion);

            $usuario = $this->atEvaluarModelo->metListarUsuario();
            $this->atVista->assign('usuario', $usuario);
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('editar', 'modales');


        }
    }

    //Método que permite eliminar
    public function metEliminar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pkNumSolicitud');
        $this->atEvaluarModelo->metEliminar($pkNumSolicitud);
        $arrayPost = array(
            'status' => 'OK',
            'pkNumSolicitud' => $pkNumSolicitud
        );
        echo json_encode($arrayPost);
    }

    //Método que permite visualizar
    public function metVer()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $almacen = $this->atEvaluarModelo->metVer($pkNumSolicitud, 2);
        $usuario = $this->atEvaluarModelo->metVerUsuario($pkNumSolicitud);
        $this->atVista->assign('usuario', $usuario);
        $this->atVista->assign('almacen', $almacen);
        $this->atVista->metRenderizar('ver', 'modales');
    }


    // Método que permite verificar
    public function metVerificar()
    {
        $pk_num_solicitud = $this->metObtenerInt("pk_num_solicitud");
        $almacenExiste = $this->atEvaluarModelo->metBuscar($pk_num_solicitud);
        echo json_encode(!$almacenExiste);
    }
}

?>