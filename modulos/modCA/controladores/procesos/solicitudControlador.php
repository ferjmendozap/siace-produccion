<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class solicitudControlador extends Controlador
{
    private $atSolicitudes;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSolicitudes = $this->metCargarModelo('solicitud');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('listado', $this->atSolicitudes->metListarSolicitud());
        $this->atVista->metRenderizar('listado');
    }

    //Método que permite crear la solicitud
    public function metSolicitud()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery'
        );
        $js = array(//'materialSiace/core/demo/DemoTableDynamic',
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
            'materialSiace/core/demo/DemoFormEditors'

        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $fk_a003_num_persona = $_POST['fk_a003_num_persona'];
            if ( isset($_POST['ind_equipo']))
            {
                $ind_equipo =$_POST['ind_equipo'];
            }
            else
            {
                $ind_equipo ='0';
            }
            $estatus = 'preparado';
            $ind_dependencia = $_POST['ind_dependencia'];
            $ind_detalles = $_POST['ind_detalles'];
            $fk_num_tipo_solicitud = $_POST['ind_tipo'];
            $idRegistro = $this->atSolicitudes->metGuardarNueva($fk_a003_num_persona, $ind_equipo, $ind_dependencia, $ind_detalles, $fk_num_tipo_solicitud);
            $fecha = $this->atSolicitudes->metFechaSolicitud($idRegistro);
            $equipo = $this->atSolicitudes->metEquipoSolicitud($idRegistro);
            $arrayPost = array(
                'fecha' => $fecha['fecha_solicitud'],
                'equipo' => $equipo['ind_descripcion'],
                'ind_dependencia' => $ind_dependencia,
                'ind_detalles' => $ind_detalles,
                'pk_num_solicitud' => $idRegistro,
                'estatus' => $estatus
            );
            echo json_encode($arrayPost);
            exit;
        }

        $form = array(
            'status' => '0',
            'fk_a003_num_persona' => null,
            'ind_equipo' => null,
            'fk_a004_num_dependencia' => null,
            'ind_detalles' => null
        );

        $usuario = Session::metObtener('idUsuario');
        $dependencia = $this->atSolicitudes->metDependenciaDependiente($usuario);
        $this->atVista->assign('dependencia', $dependencia);

        $equipo = $this->atSolicitudes->metEquipo();
        $this->atVista->assign('equipo', $equipo);

        $tipo = $this->atSolicitudes->metTipo();
        $this->atVista->assign('tipo', $tipo);

        $empleado=$this->atSolicitudes->metBuscarPersona();
        $this->atVista->assign('empleado',$empleado);



        $this->atVista->assign('form', $form);

        $this->atVista->metRenderizar('crearModificar', 'modales');
    }


    //Método que permite listar todas las solicitudes
    public function metListarTodo()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->atSolicitudes->metListarTodo());
        $this->atVista->metRenderizar('listarTodo');
    }


    public function metImprimirSolicitudes()
    {
        $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
        $pdf = new pdfSolicitud('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 10);
        $listarArchivo = $this->atSolicitudes->metListarArchivo();
        $pdf->SetWidths(array(35, 30, 30, 50, 30, 15));
        foreach ($listarArchivo as $listarArchivo) {
            $pdf->Row(array(
                utf8_decode($listarArchivo['fecha_solicitud']),
                utf8_decode($listarArchivo['ind_dependencia']),
                utf8_decode($listarArchivo['ind_descripcion']),
                utf8_decode($listarArchivo['ind_detalles']),
                $listarArchivo['ind_nombre1'],
                $listarArchivo['ind_estatus']
            ), 6);

        }
        $pdf->Output();
    }


    public function metImprimirSolicitudesDuracion()
    {
        $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
        $pdf = new pdfSolicitud('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 10);
        $listarArchivo = $this->atSolicitudes->metListarArchivo();
        $pdf->SetWidths(array(32, 35, 108, 46, 41));
        foreach ($listarArchivo as $listarArchivo) {
            $pdf->Row(array(
                utf8_decode($listarArchivo['fecha_solicitud']),
                utf8_decode($listarArchivo['ind_modalidad']),
                utf8_decode($listarArchivo['ind_equipo']),
                $listarArchivo['ind_detalles'],
            ), 4);

        }
        $pdf->Output();
    }

    public function metImprimirSolicitudesPlanificacion()
    {
        $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
        $pdf = new pdfSolicitud('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 10);
        $listarArchivo = $this->atSolicitudes->metListarArchivo();
        $pdf->SetWidths(array(32, 35, 108, 46, 41));
        foreach ($listarArchivo as $listarArchivo) {
            $pdf->Row(array(
                utf8_decode($listarArchivo['fecha_solicitud']),
                utf8_decode($listarArchivo['ind_modalidad']),
                utf8_decode($listarArchivo['ind_equipo']),
                $listarArchivo['ind_detalles'],
            ), 4);

        }
        $pdf->Output();
    }

    public function metImprimirSolicitudesEjecucion()
    {
        $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
        $pdf = new pdfSolicitud('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 10);
        $listarArchivo = $this->atSolicitudes->metListarArchivo();
        $pdf->SetWidths(array(32, 35, 108, 46, 41));
        foreach ($listarArchivo as $listarArchivo) {
            $pdf->Row(array(
                utf8_decode($listarArchivo['fecha_solicitud']),
                utf8_decode($listarArchivo['ind_modalidad']),
                utf8_decode($listarArchivo['ind_equipo']),
                $listarArchivo['ind_detalles'],
            ), 4);

        }
        $pdf->Output();
    }

    public function metRegistro()
    {
        $pkNumRegistroDocumento = $_GET['pk_num_registro_documento'];
        $usuario = Session::metObtener('idUsuario');
        $obtenerUsuario = $this->atSolicitudes->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraMaestros', 'modAD');
        $pdf = new pdfRegistroDocumento('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetMargins(18, 25, 30);
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(18);
        $pdf->Cell(185, 5, utf8_decode('Elaborado por: ' . $obtenerUsuario['ind_nombre1'] . ' ' . $obtenerUsuario['ind_apellido1']), 0, 0, 'L', 0);
        $pdf->Ln();
        $pdf->Ln();
        $verRegistro = $this->atSolicitudes->metVisualizarRegistro($pkNumRegistroDocumento, 2);
        foreach ($verRegistro as $verRegistro) {
            $pdf->Cell(185, 5, utf8_decode('Datos de Almacenamiento'), 0, 0, 'C', 0);
            $pdf->Line(20, 66, 200, 66);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Almacén: ' . $verRegistro['ind_descripcion_almacen']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Pasillo: ' . $verRegistro['ind_pasillo']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Estante: ' . $verRegistro['ind_descripcion_estante']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Dependencia: ' . $verRegistro['ind_dependencia']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Caja: ' . $verRegistro['pk_num_caja'] . ' - ' . $verRegistro['ind_descripcion_caja']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Datos de Registro'), 0, 0, 'C', 0);
            $pdf->Line(20, 106, 200, 106);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Número de Registro del Documento: ' . $verRegistro['num_registro']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Documento: ' . $verRegistro['ind_documento']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Fecha del Documento: ' . $verRegistro['fecha_documento']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Remitente: ' . $verRegistro['ind_nombre1'] . ' ' . $verRegistro['ind_apellido1']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(185, 5, utf8_decode('Descripción: ' . $verRegistro['ind_descripcion']), 0, 0, 'L', 0);
            $pdf->Ln();
            $pdf->MultiCell(185, 5, utf8_decode('Resumen: ' . $verRegistro['ind_resumen']), 0);
        }
        $pdf->Output();
    }


    //Método que permite ver una solicitud
    public function metVer()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $idSolicitud = $this->metObtenerInt('idSolicitud');
        $estatus = $this->metObtenerInt('estatus');
        $this->atVista->assign('formDB', $this->atSolicitudes->metBuscarSolicitud($idSolicitud));
        $this->atVista->assign('idSolicitud', $idSolicitud);
        $this->atVista->assign('ver', '1');
        $this->atVista->assign('ap', $estatus);
        $this->atVista->metRenderizar('ver', 'modales');
    }


    public function metConsultarEmpleado()
    {
        $listarEmpleado = $this->atSolicitudes->metListarEmpleado();
        $this->atVista->assign('listarEmpleado', $listarEmpleado);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');
    }



    public function metEliminar()
    {
        $pk_num_solicitud = $this->metObtenerInt('pk_num_solicitud');
        $this->atSolicitudes->metEliminar($pk_num_solicitud);
        $arrayPost = array(
            'status' => 'OK',
            'pk_num_solicitud' => $pk_num_solicitud
        );
        echo json_encode($arrayPost);
    }


}
