<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de tipos de documento
class duracionControlador extends Controlador
{
    private $atDuracionModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo de solicitud.
        $this->atDuracionModelo = $this->metCargarModelo('duracion');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('listado', $this->atDuracionModelo->metListarTodo());
        $this->atVista->metRenderizar('listReporte');
    }

    //Metodo que permite registrar un nuevo tipo de documento


    // Método que permite ver el reporte de solicitudes
    public function metImprimirSolicitudes()
    {
        $this->metObtenerLibreria('cabeceraMaestros','modCA');
        $pdf= new pdfDuracion('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 10);
        $listarArchivo = $this->atDuracionModelo->metListarTodo();
        $pdf->SetWidths(array(18,18,50,18,18,18,18,18,18,18,18,18,18,18,18,18));
        foreach ($listarArchivo as $listarArchivo) {
            $pdf->Row(array(
                $listarArchivo['fec_creado'],
                $listarArchivo['ind_nombre1'],
                $listarArchivo['ind_detalles'],
                $listarArchivo['fec_aprobado'],
                $listarArchivo['fec_revisado'],
                $listarArchivo['fec_asignado'],
                $listarArchivo['fec_ejecutado'],
                $listarArchivo['fec_culminado'],
                $listarArchivo['fec_evaluado'],
                $listarArchivo['fec_cerrado'],
                $listarArchivo['ind_estatus']
            ), 6);

        }
        $pdf->Output();
    }

    // Método que verifica la existencia de un tipo de documento





public function metImprimir()
    {
        $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
        $pdf = new pdfEjecucion('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 10);
        $listarDuracion = $this->atReporteModelo->metListarTodo();
        $pdf->SetWidths(array(10,10,10,10,10,10,10,10,10,10,10,10,10));
        foreach ($listarDuracion as $listarDuracion) {
            $pdf->Row(array(
                utf8_decode($listarDuracion['fec_creado']),
                utf8_decode($listarDuracion['ind_descripcion']),
                $listarDuracion['ind_nombre1'],
                utf8_decode($listarDuracion['ind_detalles']),
                $listarDuracion['fec_aprobado'],
                $listarDuracion['fec_revisado'],
                $listarDuracion['fec_asignado'],
                $listarDuracion['fec_ejecutado'],
                $listarDuracion['fec_culminado'],
                $listarDuracion['fec_evaluado'],
                $listarDuracion['fec_cerrado'],
                $listarDuracion['ind_estatus']
            ), 12);

        }
        $pdf->Output();
    }

    // Método que verifica la existencia de un tipo de documento

}
