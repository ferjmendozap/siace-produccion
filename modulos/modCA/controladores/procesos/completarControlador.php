<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación
class completarControlador extends Controlador
{
    private $atCompletarModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atCompletarModelo = $this->metCargarModelo('completar');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('_PruebaPost', $this->atCompletarModelo->metListar());
        $this->atVista->metRenderizar('listado');
    }

    //Método que permite actualizar la solicitud
    public function metEditar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $ind_detalles_analista_final = $this->metObtenerTexto('ind_detalles_analista_final');
            $fk_num_modalidad = $this->metObtenerTexto('fk_num_modalidad');
            $fk_num_tipo_solicitud = $_POST['ind_tipo'];
            $this->metValidarToken();
            $this->atCompletarModelo->metEditar($pkNumSolicitud, $ind_detalles_analista_final, $fk_num_modalidad, $fk_num_tipo_solicitud);
            $arrayPost = array(
                'status' => 'modificar',
                'pk_num_solicitud' => $pkNumSolicitud,
                'ind_detalles_analista_final' => $ind_detalles_analista_final,
                'fk_num_modalidad' => $fk_num_modalidad
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pkNumSolicitud)) {
            $form = $this->atCompletarModelo->metVer($pkNumSolicitud, 1);
            $form = array(
                'status' => '1',
                'ind_detalles' => $form['ind_detalles'],
                'ind_detalles_analista' => $form['ind_detalles_analista'],
                'ind_detalles_supervisor' => $form['ind_detalles_supervisor'],
                'fk_num_modalidad' => $form['fk_num_modalidad'],
                'ind_equipo' => $form['ind_equipo'],
                'pk_num_solicitud' => $pkNumSolicitud
            );
            $usuario = $this->atCompletarModelo->metListarUsuario();
            $this->atVista->assign('usuario', $usuario);

            $modalidad = $this->atCompletarModelo->metModalidad();
            $this->atVista->assign('modalidad', $modalidad);

            $tipo = $this->atCompletarModelo->metTipo();
            $this->atVista->assign('tipo', $tipo);

            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('editar', 'modales');
        }
    }


    public function metActividad()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $ind_actividad = $this->metObtenerTexto('ind_actividad');
            $ind_porcentaje_ejecucion= $_POST['ind_porcentaje_ejecucion'];
            $this->metValidarToken();
            $this->atCompletarModelo->metGuardarActividad($pkNumSolicitud, $ind_actividad, $ind_porcentaje_ejecucion);
            $arrayPost = array(
                'status' => 'modificar',
                'pk_num_solicitud' => $pkNumSolicitud,
                'ind_actividad ' => $ind_actividad,
                'ind_porcentaje_ejecucion' => $ind_porcentaje_ejecucion
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pkNumSolicitud)) {
            $form = $this->atCompletarModelo->metVerActividad($pkNumSolicitud, 1);
            $form = array(
                'status' => '1',
                'ind_actividad' => $form['ind_actividad'],
                'ind_porcentaje_ejecucion' => $form['ind_porcentaje_ejecucion'],
            );
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('actividad', 'modales');
        }
    }


    //Método que permite eliminar
    public function metEliminar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pkNumSolicitud');
        $this->atCompletarModelo->metEliminar($pkNumSolicitud);
        $arrayPost = array(
            'status' => 'OK',
            'pkNumSolicitud' => $pkNumSolicitud
        );
        echo json_encode($arrayPost);
    }

    //Método que permite visualizar la solicitud
    public function metVer()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $var = $this->atCompletarModelo->metVer($pkNumSolicitud, 2);
        $this->atVista->assign('var', $var);
        $this->atVista->metRenderizar('ver', 'modales');
    }


    // Método que permite verificar la solicitud
    public function metVerificar()
    {
        $pk_num_solicitud = $this->metObtenerInt("pk_num_solicitud");
        $almacenExiste = $this->atCompletarModelo->metBuscar($pk_num_solicitud);
        echo json_encode(!$almacenExiste);
    }
}

?>