<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class crearControlador extends Controlador
{
    private $atSolicitudModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSolicitudModelo = $this->metCargarModelo('solicitud');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('solicitud', $this->atSolicitudModelo->metListarSolicitud());//$this->_requerimientoModelo->getAllRequerimientoPost()
        $this->atVista->metRenderizar('listar');
    }


    public function metListar()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
        );
        $complementoCss[] = 'wizard/wizardfa6c';
        $js[] = 'materialSiace/core/demo/DemoFormWizard';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idPost = $this->metObtenerInt('requerimientoPost');
        $form = $this->metObtenerInt('valido');

        if (isset($form['valido']) && $form['valido'] == 1) {
            $this->metValidarToken();
            if ($form['status'] == 'nuevo') {
                $idPost = $this->_requerimientoModelo->setRequerimientoPost($form['titulo'], $form['descripcion'], Session::metObtener('idUsuario'));
                $arrayPost = array(
                    'status' => 'nuevo',
                    'titulo' => $form['titulo'],
                    'descripcion' => $form['descripcion'],
                    'idPost' => $idPost
                );
            } else {
                $this->_requerimientoModelo->updateRequerimientoPost($form['titulo'], $form['descripcion'], Session::metObtener('idUsuario'), $form['requerimientoPost']);
                $arrayPost = array(
                    'status' => 'modificar',
                    'titulo' => $form['titulo'],
                    'descripcion' => $form['descripcion'],
                    'idPost' => $form['requerimientoPost']

                );
            }
            echo json_encode($arrayPost);
            exit;
        }


        if (!empty($idPost)) {
            $form = $this->_requerimientoModelo->getRequerimientoPost($idPost);
            $form = array(
                'status' => 'modificar',
                'titulo' => $form['titulo'],
                'descripcion' => $form['descripcion'],
                'idPost' => $form['idPost'],
            );
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('modifica', 'modales');
        } else {
            $form = array(
                'status' => 'nuevo',
                'titulo' => null,
                'descripcion' => null,
            );
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('listar');
        }


    }

    public function metNuevo()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
        );
        $complementoCss[] = 'wizard/wizardfa6c';
        $js[] = 'materialSiace/core/demo/DemoFormWizard';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idPost = $this->metObtenerInt('requerimientoPost');
        $form = $this->metObtenerInt('valido');

        if (isset($form['valido']) && $form['valido'] == 1) {
            $this->metValidarToken();
            if ($form['status'] == 'nuevo') {
                $idPost = $this->_requerimientoModelo->setRequerimientoPost($form['titulo'], $form['descripcion'], Session::metObtener('idUsuario'));
                $arrayPost = array(
                    'status' => 'nuevo',
                    'titulo' => $form['titulo'],
                    'descripcion' => $form['descripcion'],
                    'idPost' => $idPost
                );
            } else {
                $this->_requerimientoModelo->updateRequerimientoPost($form['titulo'], $form['descripcion'], Session::metObtener('idUsuario'), $form['requerimientoPost']);
                $arrayPost = array(
                    'status' => 'modificar',
                    'titulo' => $form['titulo'],
                    'descripcion' => $form['descripcion'],
                    'idPost' => $form['requerimientoPost']

                );
            }
            echo json_encode($arrayPost);
            exit;
        }


        if (!empty($idPost)) {
            $form = $this->_requerimientoModelo->getRequerimientoPost($idPost);
            $form = array(
                'status' => 'modificar',
                'titulo' => $form['titulo'],
                'descripcion' => $form['descripcion'],
                'idPost' => $form['idPost'],
            );
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('modifica', 'modales');
        } else {
            $form = array(
                'status' => 'nuevo',
                'titulo' => null,
                'descripcion' => null,
            );
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('nuevo', 'modales');
        }


    }

    public function metCrearAsistencia()
    {
        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, ['ind_nombre_situacion' => 'required|alpha_space|max_len,45']);
        $gump->set_field_name("ind_nombre_situacion", "Nombre");

        ##  validaci�n exitosa
        if ($validate === TRUE) {
            ##  crear registro
            $id = $this->Situacion->metCrear($_POST);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' => '
                    <tr id="id' . $id . '">
                        <td><label>' . $id . '</label></td>
                        <td><label>' . $_POST['ind_nombre_situacion'] . '</label></td>
                        <td align="center">
                            ' . (($_POST['perfilM']) ? '<button idRegistro="' . $id . '" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>' : '') . '
                            &nbsp;&nbsp;
                            ' . (($_POST['perfilE']) ? '<button idRegistro="' . $id . '" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>' : '') . '
                        </td>
                    </tr>
                ',
            ];
        } else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }


}