<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de almacenes
class revisarModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    public function metListar()
    {
        $lista =  $this->_db->query(


          " SELECT
         *
          FROM st_d001_solicitud 
		  LEFT JOIN
          af_b001_activo ON st_d001_solicitud.ind_equipo=af_b001_activo.pk_num_activo
      
          WHERE
          st_d001_solicitud.num_estatus='1'

"

        );
        $lista->setFetchMode(PDO::FETCH_ASSOC);
        return $lista->fetchAll();
    }




    public function metVer($pkNumSolicitud, $metodo)
    {
        $verAprobar =  $this->_db->query(
            "
            SELECT
            *
            FROM st_d001_solicitud 
		LEFT JOIN 
          af_b001_activo ON st_d001_solicitud.ind_equipo=af_b001_activo.pk_num_activo
      LEFT JOIN a004_dependencia ON st_d001_solicitud.fk_a004_num_dependencia=a004_dependencia.pk_num_dependencia
          WHERE
         
          pk_num_solicitud='$pkNumSolicitud'
 "
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }
    }

    public function metVerUsuario($pkNumAlmacen)
    {
        $verUsuario =  $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1 from ad_c001_almacen as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetchAll();

    }
    public function metVerALL()
    {
        $usuario =  $this->_db->query(
            "select * from 
              st_d001_solicitud,
              rh_b001_empleado,
              a003_persona,
              a004_dependencia
              where 
              st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado
              AND rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona  
              AND st_d001_solicitud.num_estatus='1'
              AND a004_dependencia.pk_num_dependencia=st_d001_solicitud.fk_a004_num_dependencia
              "
        );
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();

    }

    public function metUsuarioAlmacen($pkNumEmpleado){
        $usuario = $this->_db->query(
            "SELECT  b.ind_nombre1, b.ind_apellido1, a.fk_a003_num_persona from rh_b001_empleado as a, a003_persona as b
             where a.fk_a003_num_persona=b.pk_num_persona and a.pk_num_empleado='$pkNumEmpleado'"
        );
        $nombre_usuario = $usuario->fetch();
        return $nombre_usuario[0].' '.$nombre_usuario[1];
    }

    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT *
 from rh_b001_empleado,
 a003_persona,
 rh_c076_empleado_organizacion,
 a004_dependencia,
 st_d001_grupo_tecnica,
 rh_c005_empleado_laboral
         where
         rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
          AND rh_b001_empleado.pk_num_empleado=rh_c076_empleado_organizacion.fk_rhb001_num_empleado 
          AND rh_c076_empleado_organizacion.fk_a004_num_dependencia=a004_dependencia.pk_num_dependencia 
          AND a004_dependencia.pk_num_dependencia=st_d001_grupo_tecnica.fk_a004_num_dependencia 
          AND rh_b001_empleado.num_estatus='1'
          AND rh_c005_empleado_laboral.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado 
          AND rh_c005_empleado_laboral.fec_egreso IS NULL
         AND rh_b001_empleado.fk_a018_num_seguridad_usuario != '0'
         order by ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }


    public function metEditar( $pk_num_empleado,$pkNumSolicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update st_d001_solicitud set num_estatus='2',fk_rhb001_num_empleado_supervisor='$pk_num_empleado'  where pk_num_solicitud='$pkNumSolicitud'"
        );
        $this->_db->query(
            "update st_d002_registro set st_d002_registro.fec_revisado=NOW()  where st_d002_registro.fk_std001_num_relacion='$pkNumSolicitud'"
        );
        $this->_db->commit();
    }

    public function metEliminar($pkNumSolicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_solicitud where pk_num_solicitud = '$pkNumSolicitud'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metBuscarAlmacen($pk_num_solicitud)
    {
        if($pk_num_solicitud == false) {
            $buscarSolicitud = $this->_db->query(
                "select st_d001_solicitud.pk_num_solicitud where pk_num_solicitud='$pk_num_solicitud'"
            );
            $buscarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarSolicitud->fetch();
            $arreglo = $buscarSolicitud->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $buscarSolicitud = $this->_db->query(
                "select pk_num_solicitud from st_d001_solicitud where  pk_num_solicitud<>'$pk_num_solicitud'"
            );
            $buscarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarSolicitud->fetch();
            $arreglo = $buscarSolicitud->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
}// fin de la clase
?>
