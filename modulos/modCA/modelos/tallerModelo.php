<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class tallerModelo extends Modelo
{
    private $atIdUsuario;
    private $atIdEmpleado;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = null;
    }





    public function metListarEquipos($estatus=false)
    {
        if($estatus){
            $sql = "ind_estatus='$estatus'";
        }else{
            $sql ="";
        }
        $solicitud = $this->_db->query(
            "SELECT * FROM  st_a001_equipo WHERE cod_equipo='$this->atCodEquipo' AND $sql"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }





//lista todas las solicitudes del usuario
    public function metListarTodo($idSolicitud)
    {
        $solicitud = $this->_db->query(
            "SELECT * FROM  st_a001_equipo WHERE cod_equipo='$this->atCodEquipo'"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }


//Buscar las solicitudes
    public function metBuscarSolicitud($idSolicitud)
    {
        $Solicitud = $this->_db->query(
            "SELECT * FROM st_a001_equipo where pk_num_solicitud='".$idSolicitudEquipo."'"
        );
        $Solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $Solicitud->fetch();
    }


    //crear

    public function metCrearSolicitud($descripcion, $numGaceta, $numResolucion, $periodo, $sueldoMinimo)
    {
        $this->_db->beginTransaction();
        $registrarSolicitud = $this->_db->prepare("
                  INSERT INTO
                    st_d001_solicitud
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                     fec_solicitud=NOW(),
                    fec_a004_num_dependencia=
                    fk_a018_num_seguridad_usuario='$this->atIdEmpleado',
                    fk_rhb001_num_empleado_analista='$this->atIdEmpleado',
                    fk_rhb001_num_empleado_solicitante='$this->atIdEmpleado',
                     fk_rhb001_num_empleado_supervisor='$this->atIdEmpleado',
                     ind_detalles='',
                     ind_detalles_analista='',
                     ind_detalles_final='',
                    ind_equipo=:'',
                     ind_estatus=:'',
                      ind_estatus_usuario='',
                       ind_evaluacion='',
                        ind_modalidad=''

        ");
        $idRegistro = $this->_db->lastInsertId();
        $error = $registrarSolicitud->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }







    public function getPersonas()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT
        rh_b001_empleado.fk_a003_num_persona,
        rh_b001_empleado.pk_num_empleado,
        a003_persona.ind_nombre1,
        a003_persona.ind_apellido1,
        a003_persona.pk_num_persona

        FROM rh_b001_empleado

        inner join  a003_persona on ( a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)

where NOT EXISTS (SELECT pk_num_chofer FROM pa_b012_chofer where pa_b012_chofer.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado)"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



}
