<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ejecucionModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListarSolicitud()
    {


        $solicitud = $this->_db->query(
            "SELECT * FROM  st_d001_solicitud,af_b001_activo,rh_b001_empleado,a003_persona  WHERE af_b001_activo.pk_num_activo=ind_equipo  AND st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado AND rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
AND st_d001_solicitud.fk_rhb001_num_empleado_solicitante='" . $this->atIdEmpleado . "'
"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }





    public function metEliminar($pk_num_solicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_solicitud " .
            "where pk_num_solicitud = '$pk_num_solicitud'"
        );
        $this->_db->commit();
    }


//lista todas las solicitudes del usuario WHERE fk_rhb001_num_empleado_solicitante='$this->atIdUsuario'
    public function metListarTodo()
    {
        $solicitud = $this->_db->query(
            "
            SELECT 
			st_d001_solicitud.*,
			a004.ind_dependencia AS dependencia ,
			st_b002.ind_tipo_solicitud AS tiposolicitud,
			st_b003.ind_soporte_realizado AS  soporte,
			st_d001e.ind_evaluacion AS evaluacion,
			CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_apellido1)  AS nombres	

			  FROM st_d001_solicitud 
			  LEFT JOIN
            st_d001_evaluacion st_d001e ON st_d001e.pk_num_evaluacion = st_d001_solicitud.fk_num_evaluacion
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  st_d001_solicitud.fk_a004_num_dependencia
			  LEFT JOIN
			st_b002_tipo_solicitud st_b002 ON st_b002.pk_num_tipo_solicitud = st_d001_solicitud.fk_num_tipo_solicitud
		      LEFT JOIN
			st_b002_tipo_soporte st_b003 ON st_b003.pk_num_tipo_soporte= st_d001_solicitud.fk_num_tipo_soporte
            INNER JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_empleado = st_d001_solicitud.fk_rhb001_num_empleado_solicitante
                  
                   "
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }

    public function metListarRespuesta($pk_num_empleado,$estatus)
    {
        $solicitud = $this->_db->query(
            "
            SELECT 
			st_d001_solicitud.*,
			a004.ind_dependencia AS dependencia ,
			st_b002.ind_tipo_solicitud AS tiposolicitud,
			st_b003.ind_soporte_realizado AS  soporte,
			st_d001e.ind_evaluacion AS evaluacion,
			CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_apellido1)  AS nombres	

			  FROM st_d001_solicitud 
			  LEFT JOIN
            st_d001_evaluacion st_d001e ON st_d001e.pk_num_evaluacion = st_d001_solicitud.fk_num_evaluacion
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  st_d001_solicitud.fk_a004_num_dependencia
			  LEFT JOIN
			st_b002_tipo_solicitud st_b002 ON st_b002.pk_num_tipo_solicitud = st_d001_solicitud.fk_num_tipo_solicitud
		      LEFT JOIN
			st_b002_tipo_soporte st_b003 ON st_b003.pk_num_tipo_soporte= st_d001_solicitud.fk_num_tipo_soporte
            INNER JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_empleado = st_d001_solicitud.fk_rhb001_num_empleado_solicitante
                  WHERE st_d001_solicitud.fk_rhb001_num_empleado_asignado= '$pk_num_empleado'
                  AND st_d001_solicitud.num_estatus='$estatus'
                   "
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }
//Buscar las solicitudes
    public function metBuscarSolicitud($idSolicitud)
    {
        $Solicitud = $this->_db->query(
            "SELECT 
                solicitud.*,
                dependencia.ind_dependencia,
                CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS funcionario
            FROM 
                st_d001_solicitud AS solicitud 
                INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = solicitud.fk_a004_num_dependencia 
                INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = solicitud.fk_rhb001_num_empleado_solicitante
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            WHERE 
                solicitud.pk_num_solicitud ='" . $idSolicitud . "'"
        );
        $Solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $Solicitud->fetch();
    }


    public function metDependenciaDependiente()
    {
        $dependencia = $this->_db->query(
            "SELECT * FROM 
a004_dependencia,
rh_c076_empleado_organizacion,
rh_b001_empleado
WHERE
a004_dependencia.pk_num_dependencia=rh_c076_empleado_organizacion.fk_a004_num_dependencia 
AND rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado

AND rh_b001_empleado.pk_num_empleado='" . $this->atIdEmpleado . "'

"
        );
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }


    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT *
         from rh_b001_empleado,a003_persona,st_d001_solicitud
         where 
         rh_b001_empleado.pk_num_empleado=st_d001_solicitud.fk_rhb001_num_empleado_asignado
         AND  
        rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona GROUP BY rh_b001_empleado.pk_num_empleado
      
         
         order by ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }

//Buscar la dependencia del solicitante
    public function metGetPersona()
    {
        $empleado = $this->_db->query("
          select * from st_d001_solicitud,rh_b001_empleado,a003_persona 
          WHERE  
          st_d001_solicitud.fk_rhb001_num_empleado_asignado=rh_b001_empleado.pk_num_empleado
          AND a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona
          "
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }




    public function metGuardarNueva($fk_a003_num_persona, $ind_equipo, $ind_dependencia, $ind_detalles)
    {
        $this->_db->beginTransaction();
        $registroUnidades = $this->_db->prepare("
          INSERT INTO
            st_d001_solicitud
          SET
          
            fecha_solicitud=NOW(),
            fk_num_modalidad='1',
            ind_equipo=:ind_equipo,
            ind_detalles=:ind_detalles,
            ind_detalles_analista=NULL ,
            ind_detalles_supervisor=NULL ,
            num_estatus='0',
            ind_estatus='preparado',
            fk_num_evaluacion='1' ,
            ind_aprobado_por=NULL ,
            ind_ip=NULL ,
            fk_rhb001_num_empleado_asignado=NULL ,
            fk_rhb001_num_empleado_supervisor=NULL ,
            ind_detalles_analista_final=NULL ,
            fk_a004_num_dependencia=:ind_dependencia,
            fk_rhb001_num_empleado_solicitante=:empleado,
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
             ind_fecha_inicio=NULL ,
            ind_fecha_fin=NULL 
        
          ");
        $registroUnidades->execute(array(
            'empleado'=>$fk_a003_num_persona,
            'ind_equipo'=>$ind_equipo,
            'ind_dependencia'=>$ind_dependencia,
            'ind_detalles'=>$ind_detalles
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroUnidades->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }






    public function metActEstadoSolicitud($idSolicitud, $estatus, $campo)
    {

        $this->_db->beginTransaction();
        $actSolicitud1 = $this->_db->prepare("
                  UPDATE
                    st_d001_solicitud
                  SET
                   num_estatus=:num_estatus,
                   fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                  WHERE
                     pk_num_solicitud='" . $idSolicitud . "'
        ");
        $actSolicitud1->execute(array(
            'num_estatus' => $estatus + 1
        ));

        $actSolicitud2 = $this->_db->query("
                    UPDATE 
                        st_d002_registro
                    SET 
                        $campo = NOW()
                    WHERE 
                        fk_std001_num_relacion = '" . $idSolicitud . "'
        ");

        $error = $actSolicitud1->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idSolicitud;
        }
    }








    public function metListar()
    {
        $usuarios = $this->_db->query(
            "SELECT *
    FROM a003_persona,af_b001_activo
         WHERE 
        a003_persona.pk_num_persona=af_b001_activo.fk_a003_num_persona_responsable
         ORDER BY ind_nombre1 ASC");
        $usuarios->setFetchMode(PDO::FETCH_ASSOC);
        return $usuarios->fetchAll();
    }





}
