<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class duracionModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListarSolicitud()
    {


        $solicitud = $this->_db->query(
            "SELECT * FROM  st_d001_solicitud,af_b001_activo,rh_b001_empleado,a003_persona,st_d002_registro  WHERE af_b001_activo.pk_num_activo=ind_equipo AND st_d002_registro.fk_std001_num_relacion=st_d001_solicitud.pk_num_solicitud AND st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado AND rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
AND st_d001_solicitud.fk_rhb001_num_empleado_solicitante='" . $this->atIdEmpleado . "'
"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }



    public function metListarArchivo()
    {
        $solicitud = $this->_db->query(
            "SELECT * FROM  st_d001_solicitud,af_b001_activo,rh_b001_empleado,a003_persona  WHERE af_b001_activo.pk_num_activo=ind_equipo  AND st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado AND rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
AND st_d001_solicitud.fk_rhb001_num_empleado_solicitante='" . $this->atIdEmpleado . "'
"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }




    public function metEliminar($pk_num_solicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_solicitud " .
            "where pk_num_solicitud = '$pk_num_solicitud'"
        );
        $this->_db->commit();
    }


//lista todas las solicitudes del usuario WHERE fk_rhb001_num_empleado_solicitante='$this->atIdUsuario'
    public function metListarTodo()
    {
        $solicitud = $this->_db->query(
            "SELECT * FROM 
 st_d001_solicitud,
 rh_b001_empleado,
 a003_persona,st_d001_evaluacion,st_d001_modalidad,st_d002_registro 
  WHERE st_d001_solicitud.pk_num_solicitud=st_d002_registro.fk_std001_num_relacion
AND st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado 
AND rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
AND st_d001_solicitud.fk_num_modalidad=st_d001_modalidad.pk_num_modalidad 
AND st_d001_solicitud.fk_num_evaluacion=st_d001_evaluacion.pk_num_evaluacion

"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }

//Buscar las solicitudes
    public function metBuscarSolicitud($idSolicitud)
    {
        $Solicitud = $this->_db->query(
            "SELECT 
                solicitud.*,
                dependencia.ind_dependencia,
                CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS funcionario
            FROM 
                st_d001_solicitud AS solicitud 
                INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = solicitud.fk_a004_num_dependencia 
                INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = solicitud.fk_rhb001_num_empleado_solicitante
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            WHERE 
                solicitud.pk_num_solicitud ='" . $idSolicitud . "'"
        );
        $Solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $Solicitud->fetch();
    }


    public function metDependenciaDependiente()
    {
        $dependencia = $this->_db->query(
            "SELECT * FROM 
a004_dependencia,
rh_c076_empleado_organizacion,
rh_b001_empleado
WHERE
a004_dependencia.pk_num_dependencia=rh_c076_empleado_organizacion.fk_a004_num_dependencia 
AND rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado

AND rh_b001_empleado.pk_num_empleado='" . $this->atIdEmpleado . "'

"
        );
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }


//Buscar la dependencia del solicitante
    public function metBuscarPersona()
    {
        $empleado = $this->_db->query("
          SELECT * FROM 
          rh_b001_empleado,a018_seguridad_usuario,a003_persona
          WHERE 
        a018_seguridad_usuario.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
          AND
          rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
          AND 
          rh_b001_empleado.pk_num_empleado='" . $this->atIdUsuario . "'"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }











    public function metEquipo()
    {
        $equipo = $this->_db->query(
            "SELECT * FROM rh_b001_empleado,a003_persona,af_b001_activo
WHERE 
             rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona 
             AND af_b001_activo.fk_a003_num_persona_responsable=a003_persona.pk_num_persona
          AND rh_b001_empleado.pk_num_empleado='" . $this->atIdUsuario . "'

"
        );
        $equipo->setFetchMode(PDO::FETCH_ASSOC);
        return $equipo->fetchAll();
    }








    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT a.pk_num_empleado, b.ind_nombre1, b.ind_apellido1 FROM rh_b001_empleado AS a, a003_persona AS b
             WHERE a.fk_a003_num_persona=b.pk_num_persona  ORDER BY b.ind_nombre1 ASC");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $usuario->fetchAll();
    }


    public function metListarDependencia()
    {
        // Consulto las dependencias de la contraloría y se las asigno a la variable $dependencia
        $dependencia = $this->_db->query(
            "SELECT pk_num_dependencia, ind_dependencia, num_piso FROM a004_dependencia ORDER BY ind_dependencia"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $dependencia->fetchAll();
    }










}
