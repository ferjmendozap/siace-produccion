<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<h2 class="text-primary">&nbsp;Cerrar Solicitud</h2>

	</div>
    <div class="section-body ">
        <div class="row">
            <div class="col-sm-12">
                <table id="datatable1" class="table table-hover table-responsive">
                    <thead>
                        <tr align="center">
                            <th>N° de Solicitud</th>
                            <th>Detalles</th>
                            <th>Dependencia</th>
                            <th>Equipo</th>
                            <th>Modalidad</th>
                            <th>Tipo</th>
                            <th>Clasif</th>
                            <th>Detalles Supervisor</th>
                            <th>Detalles Analista</th>
                            <th>Evaluacion</th>
                            <th>Acciones</th>

                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_solicitud{$post.pk_num_solicitud}">
                            <td>{$post.pk_num_solicitud}</td>
                            <td>
                                <textarea readonly>
                                       {$post.ind_detalles}
                                </textarea>
                             </td>
                            <td>{$post.dependencia}</td>
                            <td>{$post.descrip}</td>
                                <td>{$post.modalidad}</td>
                                <td>{$post.tiposolicitud}</td>
                                <td>{$post.soporte}</td>
                                <td>{$post.ind_detalles_supervisor}</td>
                                <td>{$post.ind_detalles_analista_final}</td>
                                <td>{$post.evaluacion}</td>
                            <td><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud="{$post.pk_num_solicitud}" title="Ver Solicitud" titulo="Ver Solicitud" id="ver"><i class="fa fa-eye" style="color: #ffffff;"></i></button>
                           <button class="cerrar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud="{$post.pk_num_solicitud}" descipcion="El Usuario ha actualizado la solicitud" title="Cerrar Solicitud"  titulo="Cerrar Solicitud"><i class="md md-lock"></i></button></td>
                        </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {
                var $url_modificar='{$_Parametros.url}modCA/procesos/cerrarCONTROL/EditarMET';
                $('#datatable1 tbody').on( 'click', '.cerrar', function () {
		        $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $.post($url_modificar,{ pk_num_solicitud: $(this).attr('pk_num_solicitud')},function($dato){
                $('#ContenidoModal').html($dato);
                });
                });


                $('#datatable1 tbody').on( 'click', '.eliminar', function () {
                    var pk_num_solicitud=$(this).attr('pk_num_solicitud');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modCA/procesos/cerrarCONTROL/EliminarMET';
                        $.post($url, { pk_num_solicitud: pk_num_solicitud},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_solicitud'+$dato['pk_num_solicitud'])).html('');
                                swal("Eliminado!", "El almacén fue eliminado.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });

            var $urlVer='{$_Parametros.url}modCA/procesos/cerrarCONTROL/VerMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_solicitud: $(this).attr('pk_num_solicitud')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
