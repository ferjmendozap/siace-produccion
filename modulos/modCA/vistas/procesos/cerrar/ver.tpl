<div class="form floating-label">
	<div class="modal-body" >
		{foreach item=alm from=$almacen}
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.pk_num_solicitud}" size="45%" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> N° de Solicitud </label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.ind_detalles}" size="45%"  disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Detalles</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.ind_descripcion}" size="45%"  disabled="disabled">
				<label for="regular2"><i class="md-location-searching"></i> Equipo</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.ind_detalles_analista_final}" size="45%"  disabled="disabled">
				<label for="regular2"><i class="md-location-searching"></i> Equipo</label>
			</div>

			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.fecha_solicitud}" size="45%"  disabled="disabled">
				<label for="regular2">Última modificación</label>
			</div>
		{/foreach}
</div>

<div align="right">
				<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
			</div>
</div>