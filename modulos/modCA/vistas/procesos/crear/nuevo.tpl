<form action="{$_Parametros.url}modCA/controladores/procesos/crearCONTROL/CrearAsistenciaMET" id="formAjax"
      method="post">
    <input type="hidden" value="1" name="valido"/>

    <div class="modal-body">

        <div class="col-lg-12">
            <div class="col-lg-6">
                <label for="Dependencia">Dependencia</label>

                <input type="text" name="dependenciass" value="" class="form-control" id="codigo">
            </div>
            <div class="col-lg-6">
                <label for="Solicitante">Solicitante</label>
                <input type="text" name="solicitante" value="" class="form-control" id="descripcion">
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-6">
                <label for="fecha">Fecha y hora de solicitud</label>
                <input type="text" name="fecha" value="" class="form-control" id="descripcion">
            </div>
            <div class="col-lg-6">
                <label for="Ubicacion">Ubicacion</label>
                <input type="text" name="ubicacion" value="" class="form-control" id="descripcion">
            </div>
            <div class="col-lg-12">

                <label for="Equipo">Equipo</label>
                <input type="text" name="descripcion" value="" class="form-control" id="descripcion">
            </div>

            <label for="Detalles">Detalles o Descripcion del Problema</label>
                <textarea name="detalles">

                </textarea>

            <div class="modal-footer">
                <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="Cancelado"
                        data-dismiss="modal">Cancelar
                </button>
                <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">Guardar</button>
            </div>
        </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $("#formAjax").submit(function () {
            return false;
        });
        $('#modalAncho').css("width", "35%");
        $('#accion').click(function () {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                if (dato['status'] == 'crear') {
                    $(document.getElementById('datatable1')).append('<tr id="idRegimen' + dato['idRegimen'] + '">' +
                            '<td>' + dato['cod_regimen_fiscal'] + '</td>' +
                            '<td>' + dato['txt_descripcion'] + '</td>' +
                            '<td>' + '<i class="' + dato['ico_estatus'] + '"></i></td>' +
                            '<td><button class="btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"data-keyboard="false" data-backdrop="static" idRegimen="' + dato['idRegimen'] + '" descripcion="El Usuario a Modificado un regimen fiscal" titulo="Modificar Regimen Fiscal">' + '<i class="fa fa-edit" style="color: #ffffff;">' + '</i>' + '</button>' +
                            '</tr>');
                    swal("Registro Exitoso!", "La solicitud se ha realizado exitosamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


                else if (dato['status'] == 'modificar') {
                    $(document.getElementById('idRegimen' + dato['idRegimen'])).html('<td>' + dato['cod_regimen_fiscal'] + '</td>' +
                            '<td>' + dato['txt_descripcion'] + '</td>' +
                            '<td>' + '<i class="' + dato['ico_estatus'] + '"></i></td>' +
                            '<td><button class="modificar regimenFiscal btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"data-keyboard="false" data-backdrop="static" idRegimen="' + dato['idRegimen'] + '" descripcion="El Usuario a Modificado un regimen fiscal" titulo="Modificar Regimen Fiscal">' + '<i class="fa fa-edit" style="color: #ffffff;">' + '</i>' + '</button>' +
                            '</tr>');
                    swal("Modificación Exitosa!", "La solicitud se ha realizado exitosamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
                else if (dato['status'] == 'error') {

                    swal("Error Verificar!", "Campos Obligatorios.", "success");

                }
            }, 'json');
        });

    });
</script>