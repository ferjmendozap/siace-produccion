<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">Grupo Tecnico</h2>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th>Dependencia</th>
                            <th>Grupo</th>
                            <th>Accion</th>
                        </tr>
                    </thead>
                      <tbody>
                          {foreach item=post from=$_PruebaPost}
                         <tr id="pk_grupo_tecnica{$post.pk_grupo_tecnica}">
                            <td>{$post.ind_dependencia}</td>
                            <td>{$post.grupo}</td>
                                <td colspan="6">
                          <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_grupo_tecnica="{$post.pk_grupo_tecnica}"  boton="si, Eliminar" descripcion="El usuario ha eliminado un grupo" titulo="¿Estás Seguro?" title="Eliminar" mensaje="¿Desea eliminar?" title="Eliminar"><i class="md md-delete"></i></button>

                                </td>
                        </tr>
                          {/foreach}
                      </tbody>
                    <tfoot>
                    <tr>
                    <th>
                        <button  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info" descripcion="El usuario ha creado un pasillo" data-toggle="modal" data-target="#formModal" titulo="Registrar" id="nuevox"><i class="md md-create"></i></button>
                    </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modCA/procesos/grupoCONTROL/NuevoMET';
        var $url_modificar='{$_Parametros.url}modCA/procesos/grupoCONTROL/EditarMET';
        $('#nuevox').click(function(){
            $('#modalAncho').css( "width", "45%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
            $('#ContenidoModal').html($dato);
            });
        });


        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var pk_grupo_tecnica=$(this).attr('pk_grupo_tecnica');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCA/procesos/grupoCONTROL/EliminarMET';
                $.post($url, { pk_grupo_tecnica: pk_grupo_tecnica},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('pk_grupo_tecnica'+$dato['pk_grupo_tecnica'])).html('');
                        swal("Eliminado!", "el post fue eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });


    });

</script>