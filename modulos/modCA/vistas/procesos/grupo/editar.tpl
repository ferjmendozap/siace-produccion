<form id="formAjax" action="{$_Parametros.url}modCA/procesos/grupoCONTROL/EditarMET"  class="form" role="form" novalidate="novalidate">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
	{if isset($form.pk_grupo_tecnica)}
		<input type="hidden" value="{$form.pk_grupo_tecnica}" name="pk_num_solicitud" id="pk_num_solicitud" />
	{/if}
	<div class="form-group floating-label">
		<input type="text" class="form-control" value="{$form.ind_detalles}" name="ind_detalles" id="ind_detalles">
		<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
	</div>
	<div class="form-group floating-label">
		<select id="pk_num_empleado" name="pk_num_empleado"
				class="select2-container form-control select2">
			<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
				<option value="">&nbsp;</option>
				{foreach item=us from=$usuario}
					<option value="{$us.pk_num_empleado}">{$us.ind_nombre1} {$us.ind_apellido1}</option>
				{/foreach}
		</select>
		<label for="fk_a003_num_persona"><i class="md md-person"></i> Usuario</label>
	</div>

	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="md md-view-list" style="color: #ffffff;"></i>Revisar</button>
	</div>
</div>
</form>


<script type="text/javascript">
	$("#formAjax").submit(function () {
		return false;
	});

	$(document).ready(function () {
		//validation rules
		$("#accion").click(function (form) {
			$.post($("#formAjax").attr('action'), $("#formAjax").serialize(), function (dato) {
				$(document.getElementById('pk_num_solicitud'+dato['pk_num_solicitud'])).html('');
				swal("Registro Guardado", "revisado satisfactoriamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
			}, 'json');
		});

	});






	var placeholder = "";



	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult(repo) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if (repo.description) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}
	function repoFormatSelection(repo) {
		return repo.full_name;
	}

	$("button[data-select2-open]").click(function () {
		$("#" + $(this).data("select2-open")).select2("open");
	});
</script>


