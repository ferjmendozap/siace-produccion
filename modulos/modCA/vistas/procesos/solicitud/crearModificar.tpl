<form id="formAjax" action="{$_Parametros.url}modCA/procesos/solicitudCONTROL/SolicitudMET" class="form floating-label" novalidate>
    <input type="hidden" value="1" name="valido"/>
    <div class="modal-body">
        <div class="row">
            <div style="text-align: center; background-color: #0aa49a; color: #ffffff;">
                <header>Información General</header>
            </div>
            <span class="clearfix"></span>

                <div class="form-group floating-label">

                        <label for="pk_num_solicitud"><i class="fa fa-eye"></i> Nro. Solicitud:</label>
                </div>

            <div class="form-group floating-label">
                <select id="fk_a003_num_persona" name="fk_a003_num_persona"
                        class="select2-container form-control select2" required>
                    <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                        {foreach item=us from=$empleado}
                            <option selected value="{$us.pk_num_empleado}">{$us.ind_nombre1} {$us.ind_apellido1}</option>
                        {/foreach}
                </select>
                <label for="fk_a003_num_persona"><i class="md md-person"></i>Usuario</label>
            </div>


            <div class="form-group floating-label">
                    <select name="ind_equipo" id="ind_equipo"
                            class="select2-container form-control select2" required>
                            {foreach item=us from=$equipo}
                                <option value="{$us.pk_num_activo}">{$us.ind_descripcion} {$us.ind_descripcion}</option>
                            {/foreach}
                    </select>
                <label for="ind_equipo">Equipo</label>
            </div>

            <div class="form-group floating-label">
                <select name="ind_tipo" id="ind_tipo"
                        class="select2-container form-control select2" required>
                    {foreach item=us from=$tipo}
                        <option value="{$us.pk_num_tipo_solicitud}">{$us.ind_tipo_solicitud}</option>
                    {/foreach}
                </select>
                <label for="ind_tipo">Tipo de solicitud</label>
            </div>



            <div class="form-group floating-label">
                <select name="ind_dependencia" id="ind_dependencia"
                        class="select2-container form-control select2" required>
                    <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                        {foreach item=us from=$dependencia}
                            <option value="{$us.pk_num_dependencia}">{$us.ind_dependencia}</option>
                        {/foreach}
                </select>
                <label for="ind_dependencia"><i class="md md-person"></i>Dependencia</label>
            </div>




            <div class="form-group" >
                <textarea id="ckeditor" name="ind_detalles" class="form-control control-12-rows" style="visibility: hidden; display: none;" data-msg-required="Indique su Solicitud" aria-required="true" aria-invalid="true" required></textarea>
                <p class="help-block">Opcional</p>
            </div>
        <span class="clearfix"></span>

        <div class="modal-footer">

            <div align="right">
                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
                <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="action"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
            </div>

            </div>

</form>


<script type="text/javascript">
    $(document).ready(function () {
        //validation rules
        $("#formAjax").validate({

            submitHandler: function(form) {
                $.post($(form).attr('action'), $(form).serialize(),function(dato){
                    $(document.getElementById('datatable1')).append('<tr id="pk_num_solicitud'+dato['pk_num_solicitud']+'"><td>'+dato['pk_num_solicitud']+'</td><td>'+dato['fecha']+'</td><td>'+dato['equipo']+'</td><td>'+dato['ind_detalles']+'</td><td>'+dato['estatus']+'</td>' +
                            '{if in_array('CA-01',$_Parametros.perfil)}<td><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado una solicitud" titulo="¿Estás Seguro?" title="Eliminar Solicitud" mensaje="¿Estás seguro de eliminar la solicitud?" boton="si, Eliminar" pk_num_solicitud="'+dato['pk_num_solicitud']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
                    swal("Solicitud Guardada", "Solicitud guardada satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                },'json');
            }

        });
    });
    var placeholder = "";

    $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
    $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult( repo ) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    function repoFormatSelection( repo ) {
        return repo.full_name;
    }

    $( "button[data-select2-open]" ).click( function() {
        $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
    });


    function buscarFuncionario(){
        var $urlFuncionario = '{$_Parametros.url}modCA/procesos/solicitudCONTROL/ConsultarEmpleadoMET';
        $('#ContenidoModal2').html("");
        $('#formModalLabel2').html('Listado de Empleados');
        $.post($urlFuncionario,"",function($dato){
            $('#ContenidoModal2').html($dato);
        });
    }
</script>
