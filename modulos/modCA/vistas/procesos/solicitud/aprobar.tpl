<form action="{$_Parametros.url}modCA/procesos/solicitudCONTROL/{if isset($ap) }aprobarMET{/if}" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idSolicitud) }
            <input type="hidden" value="{$idSolicitud}" name="idSolicitud"/>
        {/if}

        <div class="row">
            <div class="col-sm-5">

                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_estadoError">
                        <input type="text" class="form-control" value="{if isset($formDB.pk_num_solicitud)}{$formDB.pk_num_solicitud}{/if}" disabled name="form[alphaNum][pk_num_solicitud]" id="pk_num_solicitud">
                        <label for="pk_num_solicitud"><i class="fa fa-eye"></i> Solicitud:</label>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_estadoError">
                        <input type="text" class="form-control" value="{if isset($formDB.fk_rhb001_num_empleado_asignado)}{$formDB.fk_rhb001_num_empleado_asignado}{/if}" disabled name="form[alphaNum][fk_rhb001_num_empleado_asignado]" id="fk_rhb001_num_empleado_asignado">
                        <label for="fk_rhb001_num_empleado_asignado"><i class="fa fa-eye"></i> Empleado:</label>
                    </div>
                </div>
                <br>

                <div class="col-sm-8 col-sm-offset-2">
                    <div class="form-group floating-label" id="num_sueldo_minimoError">
                        <input type="text" class="form-control" value="{if isset($formDB.fk_a004_num_dependencia)}{$formDB.fk_a004_num_dependencia}{/if}" disabled name="form[alphaNum][fk_a004_num_dependencia]" id="fk_a004_num_dependencia">
                        <label for="fk_a004_num_dependencia"><i class="icm icm-coins"></i> Dependencia</label>
                    </div>
                </div>
                <br>
            </div>
            <div class="col-sm-7">
                <div class="form-group floating-label" id="ind_equipo">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_equipo)}{$formDB.ind_equipo}{/if}" name="form[alphaNum][ind_equipo]" disabled id="ind_equipo">
                    <label for="ind_equipo"><i class="md md-person"></i> Equipo Asignado</label>
                </div>
            </div>

            <div class="col-sm-7">
                <div class="form-group floating-label" id="fk_rhb001_num_empleado_creaError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_estatus)}{$formDB.ind_estatus}{/if}" name="form[alphaNum][ind_equipo]" disabled id="ind_equipo">
                    <label for="ind_equipo"><i class="md md-person"></i> estatus</label>
                </div>
            </div>

            <br>



        </div>
    </div>
    <span class="clearfix"></span>

    {if isset($ap)}
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="aprobar">Aprobar</button>
    {/if}


</form>


<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","70%");
        $('#anular').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='anular'){
                    $(document.getElementById('idSueldoMinimo'+dato['idSueldoMinimo'])).remove();
                    swal("Anulado!", "EL Suedo Minimo fue Anulado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
        $('#aprobar').click(function(){
            $.post($("#formAjax").attr("action"), { idSueldoMinimoForm: $('#idSueldoMinimo').val(),valido:1 },function(dato){
                if(dato['status']=='aprobar'){
                    $(document.getElementById('idSueldoMinimo'+dato['idSueldoMinimo'])).remove();
                    swal("Aprobado!", "EL Suedo Minimo fue Aprobado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
        $('#guardar').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = false;
                var arrayMostrarOrden = ['pk_num_solicitud','ind_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['idSolicitud'],'idSolicitud',arrayCheck,arrayMostrarOrden,'La Nomina fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idSolicitud'],'idSolicitud',arrayCheck,arrayMostrarOrden,'La Nomina fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>


