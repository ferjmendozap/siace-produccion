<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary"> LISTAR MANTENIMIENTOS </h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de los mantenimientos realizados.</h5>
        </div><!--end .col -->

    </div><!--end .card -->


    <div class="form-group   col-lg-12 ">

        <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
    </div>

</div><!--end .card -->

<section class="style-default-bright">



    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">
                <div class="card-actionbar-row">
                    {if in_array('PA-01-05-01-01-N',$_Parametros.perfil)}
                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                descripcion="Registro de Mantenimiento"  titulo="Registrar Mantenimiento" id="nuevo" >
                        Registrar Mantenimiento &nbsp;&nbsp;<i class="md md-create"></i>
                        </button>
                     {/if}
                </div>
            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">Fecha Mantenimiento</th>
                        <th class="sort-alpha col-sm-2">Vehículo </th>
                        <th class="sort-alpha col-sm-4">Ocasión </th>

                        <th class="sort-alpha col-sm-3">Observación</th>

                        <th class="  col-sm-1"> Modificar Mantenimiento</th>





                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_MantenimientoPost}
                        <tr id="idPost{$post.pk_num_mantenimiento}" class="gradeA">
                            <td>{$post.fec_mant}</td>
                            <td>{$post.ind_modelo} - {$post.ind_placa}</td>

                            <td>{$post.ind_en_ocacion}</td>
                            <td>{$post.ind_observacion}</td>



                            <td>  {if in_array('PA-01-05-01-02-M',$_Parametros.perfil)}
                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Mantenimiento" descipcion="--" idModificar="{$post.pk_num_mantenimiento}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                        <i class="md md-directions-car" style="color: #ffffff;"></i>
                                    </button>
                                  {/if}
                            </td>




                        </tr>








                    {/foreach}
                    </tbody>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->




    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modPA/mantenimientoCONTROL/RegistrarMantenimientoMET';
            $('#nuevo').click(function(){
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });
            $('#modalAncho').css( "width", "85%" );




            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modPA/mantenimientoCONTROL/ModificarMantenimientoMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idModificar')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

        });
    </script>
    </div>




