<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: REGISTRO Y MANTENIMIENTO DE REPRESENTANTES DE ENTES
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        30-05-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class representantesEnteModelo extends Modelo{
    public function __construct()    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }
    /**
     * Consultas de personal
     * @param $caso
     * @param null $params
     * @return array
     */
    public function metConsultasRepresentantes($caso,$params=NULL){
        $sql_criterio="";$criterio=false;$Union="";
        $tabla="a003_persona"; $campos="*";
        switch($caso){
            case "por_listado"://Para el listado de la grilla del form principal y validar si una persona existe en una organización
                if($params){
                    $sql_criterio.=($criterio)? " AND ": " WHERE ";
                    $sql_criterio.="fk_a003_num_persona_titular=".$params;
                }
                $campos="DISTINCT pk_num_persona AS id_persona, ind_cedula_documento AS cedula_persona, CONCAT_WS(' ', ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_persona,num_estatus AS estatus_persona";
                $Union=" INNER JOIN a041_persona_ente ON a003_persona.pk_num_persona = a041_persona_ente.fk_a003_num_persona_titular";
                break;
            case "valida_persona"://Valida si una persona existe al ingresar
                if(isset($params["txt_cedula"])){
                    $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="ind_cedula_documento='".$params["txt_cedula"]."'"; $criterio=true;
                }
                if(isset($params["idRpstte"])){
                    $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="pk_num_persona=".$params["idRpstte"];
                }
                $campos="a003_persona.*, a006_miscelaneo_detalle.ind_nombre_detalle AS desc_tipopersona";
                $Union=" LEFT JOIN a006_miscelaneo_detalle ON a003_persona.fk_a006_num_miscelaneo_det_tipopersona = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle";
                break;
            case "valida_modificar"://Valida si una persona existe al modificar
                $sql_criterio.=($criterio)? " AND ": " WHERE ";
                $sql_criterio.="pk_num_persona!=".$params['idRpstte']." AND ind_cedula_documento='".$params['txt_cedula']."'";
                break;
        }
        $sql_criterio.=" ORDER BY ind_nombre1 ASC";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Ingresa el registro de una persona
     * @param $params
     * @return int|string
     */
    public function metIngresaPersona($params){
        $arrValores=array(
            'ind_cedula_documento'=>$params["txt_cedula"],
            'ind_documento_fiscal'=>$params["txt_documento_fiscal"],
            'ind_nombre1'=>$params["txt_nombrepers1"],
            'ind_nombre2'=>$params["txt_nombrepers2"]?$params["txt_nombrepers2"]:null,
            'ind_apellido1'=>$params["txt_apellido1"],
            'ind_apellido2'=>$params["txt_apellido2"]?$params["txt_apellido2"]:null,
            'fk_a006_num_miscelaneo_detalle_nacionalidad'=>$params["cbox_nacionalidad"],
            'ind_email'=>$params["txt_email"],
            'ind_tipo_persona'=>$params["cbox_situacionjuridica"],
            'num_estatus'=>$params["chk_estatuspers"],
            'fk_a006_num_miscelaneo_det_tipopersona'=>$params["cbox_tipopersona"],
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s'),
            'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario
        );
        $this->_db->beginTransaction();
        $nuevoRegistro=$this->_db->prepare("INSERT INTO a003_persona SET
            ind_cedula_documento=:ind_cedula_documento,
            ind_documento_fiscal=:ind_documento_fiscal,
            ind_nombre1=:ind_nombre1,
            ind_nombre2=:ind_nombre2,
            ind_apellido1=:ind_apellido1,
            ind_apellido2=:ind_apellido2,
            fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
            ind_email=:ind_email,
            ind_tipo_persona=:ind_tipo_persona,
            num_estatus=:num_estatus,
            fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
            fec_ultima_modificacion=:fec_ultima_modificacion,
            fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
        ");
        $nuevoRegistro->execute($arrValores);
        $idRegistro=$this->_db->lastInsertId();
        $error = $nuevoRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$idRegistro=0;
        }else{
            $this->_db->commit();
        }
        return $idRegistro;
    }
    /**
     * Actualiza el registro de una persona
     * @param $params
     * @return int
     */
    public function metModificaRepresentante($params){
        $arrValores=array(
            'ind_cedula_documento'=>$params["txt_cedula"],
            'ind_documento_fiscal'=>$params["txt_documento_fiscal"],
            'ind_nombre1'=>$params["txt_nombrepers1"],
            'ind_nombre2'=>$params["txt_nombrepers2"]?$params["txt_nombrepers2"]:null,
            'ind_apellido1'=>$params["txt_apellido1"],
            'ind_apellido2'=>$params["txt_apellido2"]?$params["txt_apellido2"]:null,
            'fk_a006_num_miscelaneo_detalle_nacionalidad'=>$params["cbox_nacionalidad"],
            'ind_email'=>$params["txt_email"],
            'ind_tipo_persona'=>$params["cbox_situacionjuridica"],
            'num_estatus'=>$params["chk_estatuspers"],
            'fk_a006_num_miscelaneo_det_tipopersona'=>$params["cbox_tipopersona"],
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s'),
            'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario,
            'pk_num_persona'=>$params["idRpstte"]
        );
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("UPDATE a003_persona SET
            ind_cedula_documento=:ind_cedula_documento,
            ind_documento_fiscal=:ind_documento_fiscal,
            ind_nombre1=:ind_nombre1,
            ind_nombre2=:ind_nombre2,
            ind_apellido1=:ind_apellido1,
            ind_apellido2=:ind_apellido2,
            fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
            ind_email=:ind_email,
            ind_tipo_persona=:ind_tipo_persona,
            num_estatus=:num_estatus,
            fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
            fec_ultima_modificacion=:fec_ultima_modificacion,
            fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
        WHERE pk_num_persona=:pk_num_persona");

        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado=0;
        }else{
            $this->_db->commit();$reg_afectado=1;
        }
        return $reg_afectado;
    }
    /**
     * Extrae las organizaciones en las cuales está adscrita una persona en particular.
     * @param idReprstnte
     * @return array
     */
    public function metListaOrganizacionPers($idReprstnte){
        $result = $this->_db->query(
            "SELECT pk_num_persona_ente AS id_personaente,fk_a003_num_persona_titular AS id_Reprstnte,fk_a039_num_ente AS id_ente,
                fk_a006_num_miscdet_cargo_pers AS id_cargopers,fk_a006_num_miscdetalle_situacion AS id_situacionpers,num_estatus_titular AS estatus_Reprstnte,
                tb_cargos.ind_nombre_detalle AS nombre_cargo, tb_situacion.ind_nombre_detalle AS nombre_situacion
                FROM a041_persona_ente
                INNER JOIN a006_miscelaneo_detalle AS tb_cargos ON a041_persona_ente.fk_a006_num_miscdet_cargo_pers = tb_cargos.pk_num_miscelaneo_detalle
                INNER JOIN a006_miscelaneo_detalle AS tb_situacion ON a041_persona_ente.fk_a006_num_miscdetalle_situacion = tb_situacion.pk_num_miscelaneo_detalle
            WHERE fk_a003_num_persona_titular=".$idReprstnte
        );
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Permite validar si la persona ya existe en el ente con el mismo cargo.
     * @param $params
     * @return mixed
     */
    public function metValidaIngresoOrganizacion($params){
        $result = $this->_db->query("
                  SELECT * FROM a041_persona_ente
                  WHERE fk_a003_num_persona_titular=".$params['idPersona']." AND fk_a039_num_ente=".$params['cbox_ente']."
                        AND fk_a006_num_miscdet_cargo_pers=".$params['cbox_cargo'] );
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Permite validar si una persona direferente al que se está actualizando está activo en el ente con el mismo cargo y situación (Titular(PFST) ó Encargado(PFSE))
     * @param $params
     * @return mixed
     * Sólo debe haber una sola persona como Titular ó Encargado para un mísmo ente(institución, organísmo, dependencia etc) y cargo.
     */
    public function metValidaOrganizacion($params){
        $result = $this->_db->query("
            SELECT pk_num_persona_ente,fk_a003_num_persona_titular,fk_a039_num_ente,fk_a006_num_miscdet_cargo_pers,
                fk_a006_num_miscdetalle_situacion,num_estatus_titular,a006_miscelaneo_detalle.cod_detalle, ind_cedula_documento AS cedula_persona,
                CONCAT_WS(' ', ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_persona
            FROM a041_persona_ente
                INNER JOIN a006_miscelaneo_detalle ON a041_persona_ente.fk_a006_num_miscdetalle_situacion = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                INNER JOIN a003_persona ON a041_persona_ente.fk_a003_num_persona_titular = a003_persona.pk_num_persona
            WHERE fk_a003_num_persona_titular!=".$params['idPersona']." AND fk_a039_num_ente=".$params['cbox_ente']."
                AND fk_a006_num_miscdet_cargo_pers=".$params['cbox_cargo']." AND num_estatus_titular=1 AND (cod_detalle='PFST' OR cod_detalle='PFSE') LIMIT 1"
        );
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Agrega una persona a una organización.
     * @param $params
     * @return int|string
     */
    public function metIngresaOrganizacion($params){
        $arrValores=array(
            'fk_a003_num_persona_titular'=>$params["idPersona"],
            'fk_a039_num_ente'=>$params["cbox_ente"],
            'fk_a006_num_miscdet_cargo_pers'=>$params["cbox_cargo"],
            'fk_a006_num_miscdetalle_situacion'=>$params["cbox_situacion"],
            'num_estatus_titular'=>$params["cbox_estatus"],
            'fk_a018_num_seg_usermod'=>$this->atIdUsuario,
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s')
        );
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("INSERT INTO a041_persona_ente SET
            fk_a003_num_persona_titular=:fk_a003_num_persona_titular,
            fk_a039_num_ente=:fk_a039_num_ente,
            fk_a006_num_miscdet_cargo_pers=:fk_a006_num_miscdet_cargo_pers,
            fk_a006_num_miscdetalle_situacion=:fk_a006_num_miscdetalle_situacion,
            num_estatus_titular=:num_estatus_titular,
            fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
            fec_ultima_modificacion=:fec_ultima_modificacion
        ");
        $sql_query->execute($arrValores);
        $idRegistro=$this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); $idRegistro=0;
        }else{
            $this->_db->commit();
        }
        return $idRegistro;
    }
    /**
     * Actualiza el registro de adscripción de una persona representante de ente.
     * @param $params
     * @return int
     */
    public function metModificaOrganizacion($params){
        $arrValores=array(
            'fk_a006_num_miscdetalle_situacion'=>$params["cbox_situacion"],
            'num_estatus_titular'=>$params["cbox_estatus"],
            'fk_a018_num_seg_usermod'=>$this->atIdUsuario,
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s'),
            'pk_num_persona_ente'=>$params["idPersonaEnte"]
        );
        $this->_db->beginTransaction(); $reg_afectado=0;
        $sql_query=$this->_db->prepare("UPDATE a041_persona_ente SET
            fk_a006_num_miscdetalle_situacion=:fk_a006_num_miscdetalle_situacion,
            num_estatus_titular=:num_estatus_titular,
            fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
            fec_ultima_modificacion=:fec_ultima_modificacion
        WHERE pk_num_persona_ente=:pk_num_persona_ente");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
    /**
     * Elimina el registro de adscripción de una persona de un ente.
     * @param IdReprstnteEnte
     * @return array|bool
     */
    public function metEliminaAdscripcion($IdReprstnteEnte){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM a041_persona_ente WHERE pk_num_persona_ente=:pk_num_persona_ente");
        $sql_query->execute(array('pk_num_persona_ente'=>$IdReprstnteEnte));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); $reg_afectado=$error;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}