<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación fiscal
 * PROCESO: Ingreso y mantenimiento de cargos de personal externo.
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        08-12-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class cargosPersExternosModelo extends Modelo{
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }
    /**
     * Busca el cargo a montar en el form para modificar
     * @param $idCargo
     * @return mixed
     */
    public function metMostrarCargo($idCargo){
        $tipoProceso = $this->_db->query("
          SELECT a040.*,DATE_FORMAT(a040.fec_ultima_modificacion, '%d-%m-%Y %T') AS fecha_modificacion, a018.ind_usuario FROM a040_cargo_personal_externo a040
          INNER JOIN a018_seguridad_usuario a018 ON a040.fk_a018_num_seg_useregistra = a018.pk_num_seguridad_usuario
          WHERE pk_num_cargo_personal_externo='$idCargo'");
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetch();
    }
    /**
     * Busca los cargos para montarlos en la grilla.
     * @return array
     */
    public function metListarCargos(){
        $sqlQuery = $this->_db->query("SELECT * FROM a040_cargo_personal_externo ORDER BY ind_cargo_personal_externo ASC");
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $sqlQuery->fetchAll();
    }
    /**
     * Validación para el ingreso y actualización
     * @param $case
     * @param string $params
     * @return array
     */
    function metConsultaCargo($case, $params=''){
        $sql_criterio = ""; $tabla="a040_cargo_personal_externo";$campos="*";
        switch($case){
            case "valida_ingreso":
                $sql_criterio = " WHERE ind_cargo_personal_externo='".$params."'";
                break;
            case "valida_modificar":
                $sql_criterio = " WHERE pk_num_cargo_personal_externo!=".$params['idCargo']." AND ind_cargo_personal_externo='".$params['nombre_cargo']."'";
                break;
        }
        $sql_query ="SELECT $campos FROM $tabla".$sql_criterio;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Permite el ingreso de un cargo
     */
    public function metCrearCargo($nombre_cargo){
        $arrValores=array(
            'ind_cargo_personal_externo'=>$nombre_cargo,
            'fk_a018_num_seg_useregistra'=>$this->atIdUsuario,
            'fec_registro'=>date('Y-m-d H:i:s'),
            'fk_a018_num_seg_usermod'=>$this->atIdUsuario,
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s')
        );
        $this->_db->beginTransaction();
        $nuevoRegistro=$this->_db->prepare("
            INSERT INTO a040_cargo_personal_externo SET
            ind_cargo_personal_externo=:ind_cargo_personal_externo,
            fk_a018_num_seg_useregistra=:fk_a018_num_seg_useregistra,
            fec_registro=:fec_registro,
            fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
            fec_ultima_modificacion=:fec_ultima_modificacion
        ");
        $nuevoRegistro->execute($arrValores);
        $idRegistro=$this->_db->lastInsertId();
        $error = $nuevoRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$idRegistro=0;
        }else{
            $this->_db->commit();
        }
        return $idRegistro;
    }
    /**
     * Permite actualizar un cargo
     */
    public function metModificarCargo($params){
        $arrValores=array(
            'ind_cargo_personal_externo'=>$params['ind_cargo_personal_externo'],
            'fk_a018_num_seg_usermod'=>$this->atIdUsuario,
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s'),
            'pk_num_cargo_personal_externo'=>$params['idCargo']
        );
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("
            UPDATE a040_cargo_personal_externo SET
                ind_cargo_personal_externo=:ind_cargo_personal_externo,
                fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
                fec_ultima_modificacion=:fec_ultima_modificacion
            WHERE pk_num_cargo_personal_externo=:pk_num_cargo_personal_externo");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado=0;
        }else{
            $this->_db->commit();$reg_afectado=1;
        }
        return $reg_afectado;
    }
    /**
     * Elimina un cargo siempre y cuando no esté relacionado.
     * @param $idCargo
     * @return array|bool
     */
    public function metEliminaCargo($idCargo){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM a040_cargo_personal_externo WHERE pk_num_cargo_personal_externo=:pk_num_cargo_personal_externo");
        $sql_query->execute(array('pk_num_cargo_personal_externo'=>$idCargo));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            $reg_afectado=$error;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}
?>