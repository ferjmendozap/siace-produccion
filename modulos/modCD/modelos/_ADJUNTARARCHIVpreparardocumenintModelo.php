<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Preparar Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'listaModelo.php';
class preparardocumenintModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
               SELECT
            cd_b001.*,
			cd_c001.*,
			a018.ind_usuario,
			rh_c006.pk_num_cargo,
			rh_c006.ind_nombre_cargo,
			a004.ind_dependencia,
	  		a003.ind_nombre1,
		  a003.ind_nombre2,
		  a003.ind_apellido1,
		  a003.ind_apellido2,
		  CONCAT(ind_nombre1,'  ',ind_nombre2,'  ',ind_apellido1,'  ',ind_apellido2)  AS nombre_apellidos
          FROM
            cd_b001_documento_interno cd_b001
			 INNER JOIN
           cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			INNER JOIN
			a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_b001.fk_a018_num_seguridad_usuario
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.fk_a004_num_dependencia
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente
			LEFT JOIN
			rh_c006_tipo_cargo rh_c006 ON rh_c006.pk_num_cargo = a003.pk_num_persona 
		  WHERE
            cd_b001.pk_num_documento='$idDocumento'
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
	
    public function metListarTipoDocumento()
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			cd_b011.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia
			FROM cd_b001_documento_interno cd_b011
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_b011.fk_a001_num_organismo
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b011.fk_a004_num_dependencia
			 WHERE
            cd_b011.ind_estado='PR'
		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
		

    public function metModificarTipoDocumento($contenido,$mediafirma,$ind_ruta_pdf,$status,$idDocumento)
																				
	
    {
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_b001_documento_interno
                      SET
                	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), txt_contenido=:txt_contenido,
					ind_media_firma=:ind_media_firma, ind_ruta_archivo=:ind_ruta_archivo, ind_estado=:ind_estado
                      WHERE
                    pk_num_documento='$idDocumento'
            ");
				//$codcompletoer=
   	         $nuevoRegistro->execute(array(
				'txt_contenido'=>$contenido,
                'ind_media_firma'=>$mediafirma,
				'ind_ruta_archivo'=>$ind_ruta_pdf,
				'ind_estado'=>'PP',

            ));
            
			
			 $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c001_distribucion_interno
                      SET
                	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), ind_estado=:ind_estado
                      WHERE
                    fk_cdb001_num_documento='$idDocumento'
            ");
			
			$nuevoRegistro->execute(array(
				'ind_estado'=>'PE',
           ));
	
	
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }
/*
   public function metEliminarTipoDocumento($idDocumento)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                 UPDATE
                cd_c011_documento_salida
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), ind_estado=:ind_estado  
				 WHERE
                    pk_num_documento='$idDocumento'
            ");
            $elimar->execute(array(
                'ind_estado'=>'AN'
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }

    }
	
	public function metJsonOrganismo($idDependencia)
    {
        $dependencia = $this->_db->query(
            "SELECT
              *
            FROM
              cd_c004_documento_entrada
            WHERE
              num_depend_ext='$idDependencia'
            ");
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }
*/
}
