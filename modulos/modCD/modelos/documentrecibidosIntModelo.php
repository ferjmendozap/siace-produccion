<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Distribucion Salida Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
  
require_once 'listaModelo.php';
class documentrecibidosIntModelo extends  listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atIdPersona=Session::metObtener('idPersona');
    }
	
	public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
               SELECT 
			cd_c004.*,
			cd_c005.*,
			a004.ind_dependencia,
		  a003.ind_nombre1,
		  a003.ind_nombre2,
		  a003.ind_apellido1,
		  a003.ind_apellido2,
		  CONCAT(ind_nombre1,'  ',ind_nombre2,'  ',ind_apellido1,'  ',ind_apellido2)  AS nombre_apellidos,
			cd_c003.ind_descripcion
			FROM 
			 cd_c005_distribucion_entrada cd_c005 
			INNER JOIN
			 cd_c004_documento_entrada cd_c004 ON cd_c004.pk_num_documento =  cd_c005.fk_cdc004_num_documento
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c005.ind_dependencia_destinataria
			LEFT JOIN
			a003_persona  a003 ON a003.pk_num_persona =  cd_c005.ind_persona_destinataria
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c004.fk_cdc003_num_tipo_documento  
		  WHERE
            cd_c004.pk_num_documento='$idDocumento'
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
   
    public function metListarTipoDocumento($usuario,$persona)
    {
            $tipoDocumento = $this->_db->query(
                "SELECT 
			cd_c011.*,
			cd_c001.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			cd_c003.ind_descripcion AS TipoDocumento,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM cd_c001_distribucion_interno cd_c001
			INNER JOIN
			cd_b001_documento_interno cd_c011  ON cd_c001.fk_cdb001_num_documento = cd_c011.pk_num_documento
			LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c011.fk_a001_num_organismo
			LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento
            LEFT JOIN 
		    a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c001.ind_dependencia_destinataria	
		
		  	WHERE
            a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
			AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
		");
	
             $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
             return $tipoDocumento->fetchAll();
    }
    public function metListarTipoDocumentoEnviado($usuario,$persona)
    {
        $tipoDocumento = $this->_db->query(
            "SELECT 
			cd_c011.*,
			cd_c001.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			cd_c003.ind_descripcion AS TipoDocumento,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM cd_c001_distribucion_interno cd_c001
			INNER JOIN
			cd_b001_documento_interno cd_c011  ON cd_c001.fk_cdb001_num_documento = cd_c011.pk_num_documento
			LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c011.fk_a001_num_organismo
			LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento
            LEFT JOIN 
		    a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c001.ind_dependencia_destinataria	
		
		  	WHERE
            a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
			AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
			AND 
        cd_c001.ind_estado='EV'
        ORDER BY cd_c011.pk_num_documento DESC

		");

        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
	

		 public function metAcuseReciboInt($idDocumento)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                 UPDATE
                cd_c001_distribucion_interno
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  fec_acuse=:fec_acuse, ind_estado=:ind_estado  
				 WHERE
                    pk_num_distribucion='$idDocumento'
            ");
            $elimar->execute(array(
              	'fec_acuse'=>date('Y-m-d H:i:s'),
			    'ind_estado'=>'RE'
            ));
			
			$sql = "SELECT fk_cdb001_num_documento FROM cd_c001_distribucion_interno WHERE pk_num_distribucion = '$idDocumento'";
			$query = $this->_db->query($sql);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$field = $query->fetch();
			
			$elimar=$this->_db->prepare("
                      UPDATE
                        cd_b001_documento_interno
                      SET
                	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),ind_estado=:ind_estado
                      WHERE
                   pk_num_documento='".$field['fk_cdb001_num_documento']."'
            ");
		
   	         $elimar->execute(array(
				'ind_estado'=>'RE',

            ));

            $error=$elimar->errorInfo();
	
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }

 
}
