<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Preparar Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'listaModelo.php';
class entdocumenexternoModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
           SELECT
            cd_c004.*,
			a003.ind_cedula_documento,
			persona.ind_documento_fiscal,
			a018.ind_usuario,
			a039.pk_num_ente,
			dep.pk_num_ente,
			dep.ind_nombre_ente AS Dependencia,
			a039.ind_nombre_ente AS Organismo,
		  CONCAT(a003.ind_nombre1,'  ',a003.ind_apellido1)  AS nombre_apellidos,
		   concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor
          FROM
            cd_c004_documento_entrada cd_c004
			 INNER JOIN
            a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_c004.fk_a018_num_seguridad_usuario
			 LEFT JOIN
           a039_ente  dep ON dep.pk_num_ente =  cd_c004.num_depend_ext
		    LEFT JOIN
           a039_ente  a039 ON a039.pk_num_ente =  cd_c004.num_org_ext
			LEFT JOIN		
		a003_persona a003 ON a003.pk_num_persona = cd_c004.num_particular_rem 
		
		LEFT JOIN a003_persona AS persona ON persona.pk_num_persona =  cd_c004.num_empresa
		  WHERE
            cd_c004.pk_num_documento='$idDocumento'
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
		
		public function metListarDodumentExt()
    {
        $Dodumentext= $this->_db->query("SELECT * FROM cd_c004_documento_entrada");
        $Dodumentext->setFetchMode(PDO::FETCH_ASSOC);
        return $Dodumentext->fetchAll();
		
    }
    public function metListarDependencias()
    {
        $dependencias = $this->_db->query("
                SELECT
                  dependencia.*
                FROM
                  a004_dependencia AS dependencia
                INNER JOIN
                  a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = dependencia.pk_num_dependencia
                LEFT JOIN
                  rh_c076_empleado_organizacion AS organizacion ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                WHERE 
                  (
                  seguridad.fk_a018_num_seguridad_usuario = '$this->atIdUsuario' 
                  AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                  ) 
                  OR dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                  GROUP BY dependencia.pk_num_dependencia
                ");
        $dependencias->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencias->fetchAll();
    }

    public function metListarTipoDocumento()
    {
        $tipoDocumento = $this->_db->query(
            "SELECT 
			cd_c004.*,
			a001.ind_descripcion_empresa,
			a021.ind_descripcion
			FROM cd_c004_documento_entrada cd_c004
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c004.num_org_ext
			  LEFT JOIN
            a021_dependencia_ext  a021 ON a021.pk_num_dependencia_ext =  cd_c004.num_depend_ext
			 WHERE
            cd_c004.ind_estado!='EV' ORDER BY cd_c004.pk_num_documento DESC
            
    ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }


    // Método  para listar las dependencias de la Contraloría
    public function metConsultarSeguridadAlterna($usuario)
    {
        $seguridadAlterna = $this->_db->query(
            "select b.pk_num_dependencia, b.ind_dependencia from a019_seguridad_dependencia as a, a004_dependencia as b where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_a018_num_seguridad_usuario=$usuario "
        );
        $seguridadAlterna->setFetchMode(PDO::FETCH_ASSOC);
        return $seguridadAlterna->fetchAll();
    }


    public function metJsonCentroCosto($idDependencia)
    {

        $centros = $this->_db->query(
            "SELECT
              pk_num_ente,
              ind_nombre_ente
            FROM
              a039_ente
            WHERE
              num_ente_padre ='$idDependencia' AND num_estatus=1
            ");
        $centros->setFetchMode(PDO::FETCH_ASSOC);
        return $centros->fetchAll();

    }
	
	    public function metListarOrganismoEnte()
    {
        $menu = $this->_db->query("SELECT pk_num_ente, ind_nombre_ente FROM a039_ente 
        where num_ente_padre=0 AND ind_nombre_ente!='Particular' ORDER BY ind_nombre_ente ASC ");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
		
    }
	
	  // M�todo que permite listar los centros de costos
    public function metListarCentroCosto()
    {
        $centroCosto =  $this->_db->query(
            "SELECT pk_num_ente, ind_nombre_ente FROM a039_ente where num_ente_padre>0"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

    public function metListarProveedor()
    {
        $proveedorPost = $this->_db->query("
          SELECT persona.*, 
          concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor, 
          persona.ind_documento_fiscal, 
          persona.num_estatus 
          FROM a003_persona AS persona 
          INNER JOIN lg_b022_proveedor on (persona.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_proveedor) 
          WHERE 1 ORDER BY `nomProveedor` ASC 
              ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetchAll();
    }
    public function metListarProveedor2()
    {
        $proveedorPost = $this->_db->query("
          SELECT
              persona.*,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor,
              persona.ind_documento_fiscal,
              persona.num_estatus
			  
          FROM
            a003_persona AS persona WHERE ind_tipo_persona='J'
              ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetchAll();
    }

    public function metListarEnte()
    {
        $proveedorPost = $this->_db->query("
          SELECT
             *
             FROM a039_ente
              ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetchAll();
    }
	
	
	
	
    public function metCrearTipoDocumento($numdocumento,
                                          $fecdocumento,
                                          $fecregistro,
										  $textasunto,
                                          $txtdescripcionanexo,
                                          $personarecibido,
                                          $nfolio,
                                          $nanexo,
                                          $ncarpeta,
                                          $descripcionanexo,
                                          $nombremensajero,
                                          $cedulamensajero,
                                          $estatus,
                                          $num_particular,
                                          $num_dependencia,
                                          $tipodocumento,
                                          $numorganismo,$numempresa)
    {
        
		$this->_db->beginTransaction();
		
		$codigo = count($this->metListarTipoDocumento()) + 1;
        $mun = "0";
        for ($i = 0; $i < (3 - strlen($codigo)); $i++) {
            $mun .= "0";
        }
        $codigo = $mun . $codigo;
		
            $nuevoRegistro=$this->_db->prepare("
                    INSERT INTO
                    cd_c004_documento_entrada
                  SET
				  	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), 
					fec_registro=:fec_registro, 
					fec_documento=:fec_documento , 
					num_secuencia=:num_secuencia, 
					num_regist_interno=:num_regist_interno, 
					num_documento=:num_documento,
					text_asunto=:text_asunto,
					txt_descripcion_asunto=:txt_descripcion_asunto, 
					ind_persona_recibido=:ind_persona_recibido, 
					num_folio=:num_folio, num_anexo=:num_anexo, 
					num_carpeta=:num_carpeta, 
					ind_descripcion_anexo=:ind_descripcion_anexo ,
				    ind_nombre_mensajero=:ind_nombre_mensajero,
					ind_cedula_mensajero=:ind_cedula_mensajero,
					ind_estado=:ind_estado ,
					fec_mes=:fec_mes,
					fec_annio=:fec_annio,
					num_particular_rem=:num_particular_rem,
					num_depend_ext=:num_depend_ext ,
					fk_cdc003_num_tipo_documento=:fk_cdc003_num_tipo_documento,
					num_org_ext=:num_org_ext,
					num_empresa=:num_empresa,
					ind_representante=:ind_representante,
					ind_cargo=:ind_cargo 
                ");
            $nuevoRegistro->execute(array(
				'num_secuencia'=>$codigo,
                'num_regist_interno'=>$codigo."-".date('Y'),
                'fec_registro'=>$fecregistro,
                'fec_documento'=>$fecdocumento,
                'num_documento'=>$numdocumento,
                'text_asunto'=>$textasunto,
				'txt_descripcion_asunto'=>$txtdescripcionanexo,
                'ind_persona_recibido'=>$personarecibido,
				'num_folio'=>$nfolio,
				'num_anexo'=>$nanexo,
				'num_carpeta'=>$ncarpeta,
				'ind_descripcion_anexo'=>$descripcionanexo,
				'ind_nombre_mensajero'=>$nombremensajero,
                'ind_cedula_mensajero'=>$cedulamensajero,
                'ind_estado'=>'PE',
                'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
                'num_particular_rem'=>$num_particular,
                'num_depend_ext'=>$num_dependencia,
				'fk_cdc003_num_tipo_documento'=>$tipodocumento,
				'num_org_ext'=>$numorganismo,
				'num_empresa'=>$numempresa,
				'ind_representante'=>NULL,
				'ind_cargo'=>NULL
            ));
			
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarTipoDocumento($numdocumento,
                                              $fecdocumento,
                                              $fecregistro,
                                              $textasunto,
                                              $txtdescripcionanexo,
                                              $personarecibido,
                                              $nfolio,
                                              $nanexo,
                                              $ncarpeta,
                                              $descripcionanexo,
                                              $nombremensajero,
                                              $cedulamensajero,
                                              $estatus,
                                              $num_particular,
                                              $num_dependencia,
                                              $tipodocumento,
                                              $numorganismo,
                                              $numempresa,
                                              $idDocumento)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c004_documento_entrada
                      SET
                       num_documento=:num_documento,
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  		fec_ultima_modificacion=NOW(), 
						fec_registro=:fec_registro, 
					    fec_documento=:fec_documento , 
						text_asunto=:text_asunto,
					 	txt_descripcion_asunto=:txt_descripcion_asunto, 
						ind_persona_recibido=:ind_persona_recibido, 
						num_folio=:num_folio, 
						num_anexo=:num_anexo, 
						num_carpeta=:num_carpeta, 
					 	ind_descripcion_anexo=:ind_descripcion_anexo , 
					  	ind_nombre_mensajero=:ind_nombre_mensajero, 
						ind_cedula_mensajero=:ind_cedula_mensajero, 
						ind_estado=:ind_estado , 
						num_particular_rem=:num_particular_rem, 
						num_depend_ext=:num_depend_ext,
					 	fk_cdc003_num_tipo_documento=:fk_cdc003_num_tipo_documento, 
						num_org_ext=:num_org_ext,
						num_empresa=:num_empresa,
						ind_representante=:ind_representante,
						ind_cargo=:ind_cargo 
                      WHERE
                        pk_num_documento='$idDocumento'
            ");
   	         $nuevoRegistro->execute(array(
                 'fec_registro'=>$fecregistro,
                 'fec_documento'=>$fecdocumento,
                'text_asunto'=>$textasunto,
				'txt_descripcion_asunto'=>$txtdescripcionanexo,
                'ind_persona_recibido'=>$personarecibido,
				'num_folio'=>$nfolio,
				'num_anexo'=>$nanexo,
				'num_carpeta'=>$ncarpeta,
                'ind_descripcion_anexo'=>$descripcionanexo,
              	'ind_nombre_mensajero'=>$nombremensajero,
                'ind_cedula_mensajero'=>$cedulamensajero,
                'ind_estado'=>'PE',
                 'num_documento'=>$numdocumento,
                'num_particular_rem'=>$num_particular,
                'num_depend_ext'=>$num_dependencia,
                'fk_cdc003_num_tipo_documento'=>$tipodocumento,
				'num_org_ext'=>$numorganismo,
				'num_empresa'=>$numempresa,
				'ind_representante'=>NULL,
				'ind_cargo'=>NULL
            ));
            
			//num_empresa=:num_empresa ,ind_representante=:ind_representante ,ind_cargo=:ind_cargo 
			
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }
/*
 	public function metJsonOrganismo($idDependencia)
    {
        $dependencia = $this->_db->query(
            "SELECT
              *
            FROM
              cd_c004_documento_entrada
            WHERE
              num_depend_ext='$idDependencia'
            ");
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }
*/
}
