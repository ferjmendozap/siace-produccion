<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Envio de Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 
require_once 'listaModelo.php';
class enviardocumentintModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

	
	public function metMostrarCopiasDocumento($idDocumento)
    {
	
        $tipoDocumento = $this->_db->query(" 
		
		SELECT
          cd_b001.*,
		  cd_c001.*,
		  rh_c063.ind_descripcion_cargo AS cargo_especial,
		  rh_c063.pk_num_puestos,
		  a004.ind_dependencia,
		  CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_apellidos
          FROM
          cd_b001_documento_interno cd_b001
		  INNER JOIN
          cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
		  LEFT JOIN
          a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
		  LEFT JOIN
		  a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente
		  LEFT JOIN
		  rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = cd_b001.ind_encargaduria_especial 
		  WHERE
          cd_b001.pk_num_documento='$idDocumento'  ORDER BY cd_c001.ind_con_copia ASC
   ");
   			
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }

    public function metDocDist($pk_num_documento)
    {
        $dist= $this->_db->query(
            "
          SELECT
        *
         from
         cd_c001_distribucion_interno
         where
         cd_c001_distribucion_interno.fk_cdb001_num_documento='$pk_num_documento' AND ind_con_copia='1'
          ");
        $dist->setFetchMode(PDO::FETCH_ASSOC);
        return $dist->fetchAll();
    }


    public function metDocDistOnly($pk_num_documento)
    {
        $dist= $this->_db->query(
            "
        SELECT
        *
        from
        cd_c001_distribucion_interno
        where
        cd_c001_distribucion_interno.fk_cdb001_num_documento='$pk_num_documento'
          ");
        $dist->setFetchMode(PDO::FETCH_ASSOC);
        return $dist->fetch();
    }
    public function metCountDist($pk_num_documento)
    {
        $Tipo= $this->_db->query(
            "
        SELECT
        count(*) AS cantidad from
        cd_c001_distribucion_interno
        where
        cd_c001_distribucion_interno.fk_cdb001_num_documento='$pk_num_documento'
         ");
        $Tipo->setFetchMode(PDO::FETCH_ASSOC);
        return $Tipo->fetch();
    }


    public function metDoccc($pk_num_documento)
    {
        $Tipo= $this->_db->query(
            "
        SELECT
        * from
        cd_c001_distribucion_interno
        where
        cd_c001_distribucion_interno.fk_cdb001_num_documento='$pk_num_documento'
        AND cd_c001_distribucion_interno.ind_con_copia='0'");
        $Tipo->setFetchMode(PDO::FETCH_ASSOC);
        return $Tipo->fetch();
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
	
        $tipoDocumento = $this->_db->query(" 
		SELECT
            cd_b001.*,
			cd_c001.*,
			rh_c063.ind_descripcion_cargo AS cargo_especial,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
		    CONCAT(ind_nombre1,' ',ind_nombre2,' ',ind_apellido1,' ',ind_apellido2)  AS nombre_apellidos,
		    a004.ind_codinterno
            FROM
            cd_b001_documento_interno cd_b001
			INNER JOIN
            cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = cd_b001.ind_encargaduria_especial 
		    WHERE
            cd_c001.pk_num_distribucion='$idDocumento' 
   ");
			
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
	 public function metMostrarTipoDocumentoDet($idDocumento)
    {
        $concepto = $this->_db->query(
            "SELECT
			cd_b001.*,
            cd_c001.*
			FROM
            cd_c001_distribucion_interno cd_c001
			INNER JOIN
            cd_b001_documento_interno cd_b001 ON cd_b001.pk_num_documento= cd_c001.fk_cdb001_num_documento 
            WHERE cd_c001.fk_cdb001_num_documento='$idDocumento' 
           
          "
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }
	
	
    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			 cd_b001.*,
			 cd_c001.*,
			 cd_c003.ind_descripcion,
			 a004.ind_dependencia,
			 a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			 FROM cd_b001_documento_interno cd_b001
			 INNER JOIN
             cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			 LEFT JOIN
             cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento
			 LEFT JOIN
             a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.fk_a004_num_dependencia
			 LEFT JOIN 
		     a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_b001.fk_a004_num_dependencia
			 WHERE
             cd_b001.ind_estado='PP'
             AND a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
             AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7' ORDER BY cd_b001.pk_num_documento DESC
		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
		

    public function metModificarTipoDocumento($responsable,$idDocumento)
	    {
            $this->_db->beginTransaction();
			$nuevoRegistro=$this->_db->prepare("
            UPDATE
            cd_c001_distribucion_interno
            SET
            fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
			fec_ultima_modificacion=NOW(), fec_envio=:fec_envio, ind_estado=:ind_estado, fk_a003_num_persona=:fk_a003_num_persona
            WHERE
            pk_num_distribucion='$idDocumento'
            ");
			$nuevoRegistro->execute(array(
				'fec_envio'=>date('Y-m-d H:i:s'),
				'ind_estado'=>'EV',
				'fk_a003_num_persona'=>$responsable,
            ));
			$sql = "SELECT fk_cdb001_num_documento FROM cd_c001_distribucion_interno WHERE pk_num_distribucion = '$idDocumento'";
			$query = $this->_db->query($sql);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$field = $query->fetch();
		    $nuevoRegistro=$this->_db->prepare("
            UPDATE
            cd_b001_documento_interno
            SET
            fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
			fec_ultima_modificacion=NOW(),ind_estado=:ind_estado
            WHERE
            pk_num_documento='".$field['fk_cdb001_num_documento']."'
            ");
		        $nuevoRegistro->execute(array(
				'ind_estado'=>'EV',
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
	}
	
	
}
