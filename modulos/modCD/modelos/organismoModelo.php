<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Organismos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'listaModelo.php';
class organismoModelo extends listaModelo
{
	
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoOrganismo($idOrganismo)
    {
        $organismo = $this->_db->query(" 
        SELECT 
	a008.ind_pais,
	a009.ind_estado,
	a011.ind_municipio,
	a010.ind_ciudad,
	a008.pk_num_pais,
	a009.pk_num_estado,
	a011.pk_num_municipio,
	a010.pk_num_ciudad,
		  a001.*,
		  a002.*,
		  a018.ind_usuario,
		  CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_apellidos
          FROM
		a001_organismo AS a001
           INNER JOIN
        a002_organismo_detalle AS a002 on a001.pk_num_organismo=a002.fk_a001_num_organismo
			INNER JOIN
	  a010_ciudad AS a010  on a002.fk_a010_num_ciudad=a010.pk_num_ciudad
			INNER JOIN
	a014_ciudad_municipio AS  a014 on a014.fk_a010_num_ciudad=a010.pk_num_ciudad
		INNER JOIN
		a011_municipio AS a011 on a014.fk_a011_num_municipio=a011.pk_num_municipio
		INNER JOIN
		a009_estado AS a009 on a010.fk_a009_num_estado=a009.pk_num_estado
		INNER JOIN
		a008_pais AS a008  on a009.fk_a008_num_pais=a008.pk_num_pais
		INNER JOIN
			a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = a001.fk_a018_num_seguridad_usuario
			INNER JOIN
		a003_persona a003 ON a003.pk_num_persona = a002.fk_a003_num_persona_representante
         WHERE
            pk_num_organismo='$idOrganismo'
        ");
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetch();
    }
	
	 public function metMostrarSelect($idOrganismo = false)
    {

        $miscelaneoDetalle = $this->_db->query("
            SELECT
			a029.pk_num_organismo_telefono,
			a029.fk_a001_num_organismo,
              a029.ind_telefono
            FROM
              a029_organismo_telefono AS a029
            INNER JOIN
              a001_organismo AS a001 ON a001.pk_num_organismo=a029.fk_a001_num_organismo
            WHERE  a029.fk_a001_num_organismo='$idOrganismo'

        ");
    $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetchAll();
    }
	
	
	
	public function metListarOrganismoInt()
    {
        $organismoint= $this->_db->query("SELECT * FROM a001_organismo WHERE
             pk_num_organismo='1'");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetchAll();
		
    }
	
	
	public function metListarOrganismoPK()
    {
        $organismoint= $this->_db->query("SELECT MAX(pk_num_organismo)  FROM a001_organismo'");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetch();
		
    }
	

    public function metListarTipoOrganismo()
    {
        $organismo = $this->_db->query(
            "SELECT 
			a001.*,
		  	a002.*,
		  CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_apellidos
			FROM a001_organismo AS a001
                LEFT JOIN
                a002_organismo_detalle AS a002 on a001.pk_num_organismo=a002.fk_a001_num_organismo
				LEFT JOIN
				a003_persona a003 ON a003.pk_num_persona = a002.fk_a003_num_persona_representante WHERE
             ind_tipo_organismo='I' ORDER BY pk_num_organismo"
       );
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetchAll();
    }

	public function metListarOrgaExt()
    {
        $organismoint= $this->_db->query("SELECT * FROM a039_ente WHERE
             num_ente_padre='0' AND ind_nombre_ente!='Particular'");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetchAll();
		
    }
	
	
	
  public function metListarOrganismoExt()
    {
        $organismo = $this->_db->query(
            "
         SELECT a039.*, CONCAT_WS(' ',a003.ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2)
         AS nombre_apellidos, a006.ind_nombre_detalle as ind_cargo_personal_externo
          FROM a039_ente a039  
          LEFT JOIN 
          a041_persona_ente a041 ON a041.fk_a039_num_ente=a039.pk_num_ente
          LEFT JOIN    
          a003_persona a003 ON a041.fk_a003_num_persona_titular=a003.pk_num_persona
          LEFT JOIN a006_miscelaneo_detalle a006 on a006.pk_num_miscelaneo_detalle=a041.fk_a006_num_miscdet_cargo_pers

            WHERE num_ente_padre=0   GROUP BY a039.pk_num_ente "
       );
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetchAll();
    }

	#
     public function metCrearTipoOrganismo($descripcion,$docfiscal,$nregistro,$tomoregistro,$ind_ruta_img,$ind_ruta,$status,$tiporganismo,$direccion,$numcaracter,
	$cargorepresent,$paginaweb,$numpersona,$numciudad,$telefono = false)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                     a001_organismo
                  SET
                     	 fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  
						  ind_descripcion_empresa=:ind_descripcion_empresa,  ind_documento_fiscal=:ind_documento_fiscal, 
                   		 ind_numero_registro=:ind_numero_registro, ind_tomo_registro=:ind_tomo_registro , ind_logo=:ind_logo,
						 ind_logo_secundario=:ind_logo_secundario, num_estatus=:num_estatus, ind_tipo_organismo=:ind_tipo_organismo ,  ind_direccion=:ind_direccion, 
						 fk_a006_num_miscelaneo_detalle_carcater_social=:fk_a006_num_miscelaneo_detalle_carcater_social
                ");
            $nuevoRegistro->execute(array(
                'ind_descripcion_empresa'=>$descripcion,
				'ind_documento_fiscal'=>$docfiscal,
                'ind_numero_registro'=>$nregistro,
                'ind_tomo_registro'=>$tomoregistro,
                'ind_logo'=>$ind_ruta_img,
				'ind_logo_secundario'=>$ind_ruta,
				'num_estatus'=>$status,
                'ind_tipo_organismo'=>$tiporganismo,
				'ind_direccion'=>$direccion,
                'fk_a006_num_miscelaneo_detalle_carcater_social'=>$numcaracter
            ));
			
		  $idOrganismo=$this->_db->lastInsertId();
			
             $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                     a002_organismo_detalle
                  SET
                   ind_cargo_representante=:ind_cargo_representante, ind_pagina_web=:ind_pagina_web,
					fk_a001_num_organismo='$idOrganismo', 
					fk_a003_num_persona_representante=:fk_a003_num_persona_representante,fk_a010_num_ciudad=:fk_a010_num_ciudad
                ");
            $nuevoRegistro->execute(array(
                'ind_cargo_representante'=>$cargorepresent,
                'ind_pagina_web'=>$paginaweb,
               	'ind_sujeto_control'=>NULL,
                'fk_a003_num_persona_representante'=>$numpersona,
				'fk_a010_num_ciudad'=>$numciudad
             ));

		
		if ($telefono) {
            $nuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                a029_organismo_telefono
              SET
                ind_telefono=:ind_telefono, fk_a001_num_organismo=$idOrganismo
            ");
            for ($i = 0; $i < count($telefono); $i++) {
                   $nuevoRegistro->execute(array(
                    'ind_telefono' => $telefono[$i]

                ));
            }
        }
		
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarTipoOrganismo($descripcion,$docfiscal,$nregistro,$tomoregistro,$ind_ruta_img,$ind_ruta,$status,$tiporganismo,$direccion,$numcaracter,
	$cargorepresent,$paginaweb,$numpersona,$numciudad,$telefono = false, $idDetalle = false, $idOrganismo)
	
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        a001_organismo
                      SET
                     	 fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  
						  ind_descripcion_empresa=:ind_descripcion_empresa, ind_documento_fiscal=:ind_documento_fiscal,
                   		 ind_numero_registro=:ind_numero_registro, ind_tomo_registro=:ind_tomo_registro , ind_logo=:ind_logo,
						 ind_logo_secundario=:ind_logo_secundario, num_estatus=:num_estatus,  ind_tipo_organismo=:ind_tipo_organismo,  ind_direccion=:ind_direccion, 
						 fk_a006_num_miscelaneo_detalle_carcater_social=:fk_a006_num_miscelaneo_detalle_carcater_social
                      WHERE
                        pk_num_organismo='$idOrganismo'
            ");
            $nuevoRegistro->execute(array(
                'ind_descripcion_empresa'=>$descripcion,
				'ind_documento_fiscal'=>$docfiscal,
                'ind_numero_registro'=>$nregistro,
                'ind_tomo_registro'=>$tomoregistro,
                'ind_logo'=>$ind_ruta_img,
				'ind_logo_secundario'=>$ind_ruta,
				'num_estatus'=>$status,
                'ind_tipo_organismo'=>$tiporganismo,
				'ind_direccion'=>$direccion,
                'fk_a006_num_miscelaneo_detalle_carcater_social'=>$numcaracter
            ));

            
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        a002_organismo_detalle
                      SET
                            ind_cargo_representante=:ind_cargo_representante, ind_pagina_web=:ind_pagina_web,
							fk_a003_num_persona_representante=:fk_a003_num_persona_representante,fk_a010_num_ciudad=:fk_a010_num_ciudad
                      WHERE
                        fk_a001_num_organismo='$idOrganismo'
            ");
            $nuevoRegistro->execute(array(
                'ind_cargo_representante'=>$cargorepresent,
                'ind_pagina_web'=>$paginaweb,
              //  'ind_sujeto_control'=>NULL,
                'fk_a003_num_persona_representante'=>$numpersona,
				'fk_a010_num_ciudad'=>$numciudad
            ));
            
			
				 $nuevoRegistro = $this->_db->query(
            "DELETE FROM a029_organismo_telefono WHERE fk_a001_num_organismo='$idOrganismo'"
        );
		
		 $nuevoDetalle = $this->_db->prepare("
                        INSERT INTO
                          a029_organismo_telefono
                        SET
                          fk_a001_num_organismo=$idOrganismo, ind_telefono=:ind_telefono
                    "); 
		
		if($telefono){
			foreach($telefono AS $i){
			  $nuevoDetalle->execute(array(
					  'ind_telefono' =>$i,
                ));
			
                }
        }
			
		 $error = $nuevoRegistro->errorInfo();
        if (isset($nuevoDetalle)) {
            $error1 = $nuevoDetalle->errorInfo();
        } else {
            $error1 = array(1 => null, 2 => null);
        }

        if (!empty($error[1]) && !empty($error[2]) && !empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error;
        }else{
                $this->_db->commit();
                return $idOrganismo;
            }
    }

    public function metEliminarTipoOrganismo($idOrganismo)	
    {
        $this->_db->beginTransaction();
		
		  $elimar=$this->_db->prepare("
                DELETE FROM a002_organismo_detalle  WHERE fk_a001_num_organismo=:fk_a001_num_organismo
            ");
            $elimar->execute(array(
                'fk_a001_num_organismo'=>$idOrganismo
            ));
			
			 $elimar=$this->_db->prepare("
                DELETE  FROM a029_organismo_telefono WHERE fk_a001_num_organismo=:fk_a001_num_organismo
            ");
            $elimar->execute(array(
                'fk_a001_num_organismo'=>$idOrganismo
            ));
			
            $elimar=$this->_db->prepare("
                DELETE  FROM a001_organismo WHERE pk_num_organismo=:pk_num_organismo
            ");
            $elimar->execute(array(
                'pk_num_organismo'=>$idOrganismo
            ));
			
			 
			
			
			
			$error=$elimar->errorInfo();
			
			 if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idOrganismo;
            }

    }

}