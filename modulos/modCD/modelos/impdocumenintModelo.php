<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 
 *****************************************************************************************************************************************/
require_once 'listaModelo.php';
class impdocumenintModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarCopiasDocumento($idDocumento)
    {
	
        $tipoDocumento = $this->_db->query(" 
		
		SELECT
            cd_b001.*,
			cd_c001.*,
			rh_c063.ind_descripcion_cargo AS cargo_especial,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
		  CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_apellidos
          FROM
            cd_b001_documento_interno cd_b001
			 INNER JOIN
           cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = cd_b001.ind_encargaduria_especial 
		  WHERE
           cd_b001.pk_num_documento='$idDocumento'  ORDER BY cd_c001.ind_con_copia ASC
   ");
   			
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
	public function metMostrarTipoDocumento($idDocumento)
    {
	
        $tipoDocumento = $this->_db->query(" 
		
		SELECT
            cd_b001.*,
			cd_c001.*,
			rh_c063.ind_descripcion_cargo AS cargo_especial,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
		  CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_apellidos
          FROM
            cd_b001_documento_interno cd_b001
			 INNER JOIN
           cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = cd_b001.ind_encargaduria_especial 
		  WHERE
            cd_c001.pk_num_distribucion='$idDocumento' 
   ");
			
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
	
	 public function metMostrarTipoDocumentoDet($idDocumento)
    {
        $concepto = $this->_db->query(
            "SELECT
			cd_b001.*,
              cd_c001.*
			FROM
               cd_c001_distribucion_interno cd_c001
			   		 INNER JOIN
           cd_b001_documento_interno cd_b001 ON cd_b001.pk_num_documento= cd_c001.fk_cdb001_num_documento 
            WHERE cd_c001.fk_cdb001_num_documento='$idDocumento' 
           
          "
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }
	
	
	 public function metListarTipoDocumentoImp($idDocumento)
    {
        $concepto = $this->_db->query(
            "SELECT
              cd_c001.*,
			  cd_b001.*,
			 rh_c063.ind_descripcion_cargo AS cargodepen,
			a004.ind_dependencia,
			cd_c003.ind_descripcion AS tipoDocumento,
		  CONCAT(a003.ind_nombre1,'  ',a003.ind_nombre2,'  ',a003.ind_apellido1,'  ',a003.ind_apellido2)  AS nombre_apellidos
			FROM
               cd_c001_distribucion_interno cd_c001
			    LEFT JOIN
           cd_b001_documento_interno cd_b001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			 LEFT JOIN
			 a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c001.ind_dependencia_destinataria
			 LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = a004.fk_a003_num_persona_responsable
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = a003.pk_num_persona
			
            WHERE cd_c001.fk_cdb001_num_documento='$idDocumento'
           
          "
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }
	 
	 /*
	  public function metMostrarTipoDocumentoDet($idDocumento)
    {
        $concepto = $this->_db->query(
            "SELECT
              cd_c001.*,
			  cd_b001.*,
			 rh_c063.ind_descripcion_cargo AS cargodepen,
			a004.ind_dependencia,
		  CONCAT(a003.ind_nombre1,'  ',a003.ind_nombre2,'  ',a003.ind_apellido1,'  ',a003.ind_apellido2)  AS nombre_apellidos
			FROM
               cd_c001_distribucion_interno cd_c001
			    LEFT JOIN
           cd_b001_documento_interno cd_b001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			 LEFT JOIN
			 a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c001.ind_dependencia_destinataria
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = a004.fk_a003_num_persona_responsable
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = a003.pk_num_persona
            WHERE cd_c001.fk_cdb001_num_documento='$idDocumento'
           
          "
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }
  */  
	
    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			cd_b001.*,
			cd_c001.*,
			cd_c003.ind_descripcion,
			a004.ind_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias,
			cd_c003.ind_descripcion AS tipoDocumento
			FROM cd_b001_documento_interno cd_b001
			 INNER JOIN
          cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			 LEFT JOIN
            cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.fk_a004_num_dependencia
             LEFT JOIN 
		    a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_b001.fk_a004_num_dependencia
			 WHERE
            cd_c001.ind_estado='EV' AND a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario 
            AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
            ORDER BY cd_b001.pk_num_documento DESC
		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
		
		public function getReporteDistribdocumenint($idDocumento)
    {
			
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT 
			cd_c001.*,
			cd_b001.fk_cdc003_num_tipo_documento,
		   cd_b001.ind_documento_completo,
		   cd_b001.ind_asunto,
		   cd_b001.pk_num_documento,
		   cd_b001.ind_descripcion,
		   rh_c063.ind_descripcion_cargo,
			 DATE_FORMAT( cd_b001.fec_registro  ,'%d/%m/%Y') as fec_registro,
			 CONCAT(a003.ind_nombre1,'  ',a003.ind_nombre2,'  ',a003.ind_apellido1,'  ',a003.ind_apellido2)  AS nombre_apellidos,
			a004.ind_dependencia AS descipDependencia,
			cd_c003.ind_descripcion AS tipoDoc
			FROM 
			 cd_c001_distribucion_interno cd_c001 
			INNER JOIN
			 cd_b001_documento_interno cd_b001 ON cd_b001.pk_num_documento =  cd_c001.fk_cdb001_num_documento
			 INNER JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente 
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = a003.pk_num_persona 
				LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento 
			 WHERE cd_c001.fk_cdb001_num_documento='$idDocumento'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll � Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metCrearTipoDocumento($dependremitent,$persremitent,$cargoremitent,$asunto,
										$descripasunto,$plazoatencion,$fecdocument,$anexo,$descripanexo,
										$estatus,$tipodocumento,$numorganismo,
										$depextern=false,$num_particular=false,$cargoextern=false,$copia)
	
    {
        
		$this->_db->beginTransaction();
		
		$codigo = count($this->metListarTipoDocumento()) + 1;
        $mun = "0";
        for ($i = 0; $i < (3 - strlen($codigo)); $i++) {
            $mun .= "0";
        }
        $codigo = $mun . $codigo;
		
            $nuevoRegistro=$this->_db->prepare("
                    INSERT INTO
                    cd_b001_documento_interno
                  SET
				  	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), 
					ind_documento_completo=:ind_documento_completo,
					fec_registro=:fec_registro,
					ind_dependencia_remitente=:ind_dependencia_remitente,
					ind_persona_remitente=:ind_persona_remitente,
					ind_cargo_remitente=:ind_cargo_remitente,
					ind_asunto=:ind_asunto,
					ind_descripcion=:ind_descripcion,
					ind_plazo_atencion=:ind_plazo_atencion,
					fec_documento=:fec_documento,
					txt_contenido=:txt_contenido,
					ind_media_firma=:ind_media_firma,
					ind_motivo_anulado=:ind_motivo_anulado,
					fec_anulado=:fec_anulado,
					ind_persona_anulado=:ind_persona_anulado,
					ind_anexo=:ind_anexo,
					txt_descripcion_anexo=:txt_descripcion_anexo,   
					ind_estado=:ind_estado,
					fec_mes=:fec_mes, 
					fec_annio=:fec_annio,
					fk_cdc003_num_tipo_documento=:fk_cdc003_num_tipo_documento,
					fk_a001_num_organismo=:fk_a001_num_organismo, 
					fk_a004_num_dependencia=:fk_a004_num_dependencia
                ");
			
            $nuevoRegistro->execute(array(
                'ind_documento_completo'=>$codigo."-".$tipodocumento."-".date('Y'),
				'fec_registro'=>date('Y-m-d'),
                'ind_dependencia_remitente'=>$dependremitent,
                'ind_persona_remitente'=>$persremitent,
                'ind_cargo_remitente'=>$cargoremitent,
				'ind_asunto'=>$asunto,
                'ind_descripcion'=>$descripasunto,
				'ind_plazo_atencion'=>$plazoatencion,
				'fec_documento'=>$fecdocument,
				'txt_contenido'=>NULL,
                'ind_media_firma'=>NULL,
				'ind_motivo_anulado'=>NULL,
				'fec_anulado'=>NULL,
				'ind_persona_anulado'=>NULL,
				'ind_anexo'=>$anexo,
				'txt_descripcion_anexo'=>$descripanexo,
				'ind_estado'=>'PR',
                'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
				'fk_cdc003_num_tipo_documento'=>$tipodocumento,
				'fk_a001_num_organismo'=>$numorganismo,
				'fk_a004_num_dependencia'=>$dependremitent
			
            ));
			
			$idDocumento= $this->_db->lastInsertId();
	 
			 $nuevoRegistro=$this->_db->prepare("
                    INSERT INTO
                    cd_c001_distribucion_interno
                  SET
				  	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
					ind_dependencia_destinataria=:ind_dependencia_destinataria, 
					ind_persona_destinataria=:ind_persona_destinataria,
					ind_cargo_destinatario=:ind_cargo_destinatario,
					ind_con_copia=:ind_con_copia,
					ind_plazo_atencion=:ind_plazo_atencion, 
					fec_envio=:fec_envio, 
					fec_acuse=:fec_acuse,
					ind_estado=:ind_estado,
					fec_mes=:fec_mes, 
					fec_annio=:fec_annio, 
					fk_cdb001_num_documento='$idDocumento',
 					fk_a003_num_persona=:fk_a003_num_persona
                ");
			
			 if($depextern){
			foreach($depextern AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>$i,
                'ind_persona_destinataria'=>'',
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_con_copia'=>$copia,
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_envio'=>NULL,
                'fec_acuse'=>NULL,
				'ind_estado'=>'PR',
         		'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
				'fk_a003_num_persona'=>NULL,
           ));
		   	}			
		  }
	 
		   if($num_particular){
			foreach($num_particular AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>'',
                'ind_persona_destinataria'=>$i,
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_con_copia'=>$copia,
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_envio'=>NULL,
                'fec_acuse'=>NULL,
				'ind_estado'=>'PR',
         		'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
				'fk_a003_num_persona'=>NULL,
           ));
		   	}			
		  }
	
		
		    $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
			
    }

    public function metModificarTipoDocumento($dependremitent,$persremitent,$cargoremitent,$asunto,
										$descripasunto,$plazoatencion,$fecdocument,$anexo,$descripanexo,
										$estatus,$tipodocumento,$numorganismo,
										$depextern=false,$num_particular=false,$cargoextern=false,$copia,$idDocumento)
																				
	
    {
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                       cd_b001_documento_interno
                  SET
				  	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), 
					ind_dependencia_remitente=:ind_dependencia_remitente,
					ind_persona_remitente=:ind_persona_remitente,
					ind_cargo_remitente=:ind_cargo_remitente,
					ind_asunto=:ind_asunto,
					ind_descripcion=:ind_descripcion,
					ind_plazo_atencion=:ind_plazo_atencion,
					fec_documento=:fec_documento,
					txt_contenido=:txt_contenido,
					ind_media_firma=:ind_media_firma,
					ind_motivo_anulado=:ind_motivo_anulado,
					fec_anulado=:fec_anulado,
					ind_persona_anulado=:ind_persona_anulado,
					ind_anexo=:ind_anexo,
					txt_descripcion_anexo=:txt_descripcion_anexo,   
					ind_estado=:ind_estado,
					fec_mes=:fec_mes, 
					fec_annio=:fec_annio,
					fk_cdc003_num_tipo_documento=:fk_cdc003_num_tipo_documento,
					fk_a001_num_organismo=:fk_a001_num_organismo, 
					fk_a004_num_dependencia=:fk_a004_num_dependencia
                      WHERE
                    pk_num_documento='$idDocumento'
            ");
				//$codcompletoer=
   	         $nuevoRegistro->execute(array(
      			// 'ind_documento_completo'=>$codigo."-".$tipodocumento."-".date('Y'),
				//'fec_registro'=>date('Y-m-d'),
                'ind_dependencia_remitente'=>$dependremitent,
                'ind_persona_remitente'=>$persremitent,
                'ind_cargo_remitente'=>$cargoremitent,
				'ind_asunto'=>$asunto,
                'ind_descripcion'=>$descripasunto,
				'ind_plazo_atencion'=>$plazoatencion,
				'fec_documento'=>$fecdocument,
				'txt_contenido'=>NULL,
                'ind_media_firma'=>NULL,
				'ind_motivo_anulado'=>NULL,
				'fec_anulado'=>NULL,
				'ind_persona_anulado'=>NULL,
				'ind_anexo'=>$anexo,
				'txt_descripcion_anexo'=>$descripanexo,
				'ind_estado'=>'PR',
                'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
				'fk_cdc003_num_tipo_documento'=>$tipodocumento,
				'fk_a001_num_organismo'=>$numorganismo,
				'fk_a004_num_dependencia'=>$dependremitent      
				));
            
			 $nuevoRegistro = $this->_db->query(
            "DELETE FROM cd_c001_distribucion_interno WHERE fk_cdb001_num_documento='$idDocumento'"
        );
					
			 $nuevoRegistro=$this->_db->prepare("
                      INSERT INTO
                        cd_c001_distribucion_interno
                      SET
                	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
					ind_dependencia_destinataria=:ind_dependencia_destinataria, 
					ind_persona_destinataria=:ind_persona_destinataria,
					ind_cargo_destinatario=:ind_cargo_destinatario,
					ind_con_copia=:ind_con_copia,
					ind_plazo_atencion=:ind_plazo_atencion, 
					fec_envio=:fec_envio, 
					fec_acuse=:fec_acuse,
					ind_estado=:ind_estado,
					fec_mes=:fec_mes, 
					fec_annio=:fec_annio, 
					fk_cdb001_num_documento='$idDocumento',
 					fk_a003_num_persona=:fk_a003_num_persona
            ");
			
			 if($depextern){
			foreach($depextern AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>$i,
                'ind_persona_destinataria'=>'',
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_con_copia'=>$copia,
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_envio'=>NULL,
                'fec_acuse'=>NULL,
				'ind_estado'=>'PR',
         		'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
				'fk_a003_num_persona'=>NULL,
           ));
		   	}			
		  }
	 
		   if($num_particular){
			foreach($num_particular AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>'',
                'ind_persona_destinataria'=>$i,
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_con_copia'=>$copia,
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_envio'=>NULL,
                'fec_acuse'=>NULL,
				'ind_estado'=>'PR',
         		'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
				'fk_a003_num_persona'=>NULL
           ));
		   	}			
		  }
	
	

            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }

     public function metanularTipoDocumento($motivoanulado,$personanulado,$estatus,$idDocumento)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                 UPDATE
                cd_b001_documento_interno
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  ind_motivo_anulado=:ind_motivo_anulado,  fec_anulado=:fec_anulado, 
					ind_persona_anulado=:ind_persona_anulado, ind_estado=:ind_estado  
				 WHERE
                    pk_num_documento='$idDocumento'
            ");
            $elimar->execute(array(
				'ind_motivo_anulado'=>$motivoanulado,
                'fec_anulado'=>date('Y-m-d H:i:s'),
				'ind_persona_anulado'=>$personanulado,
				'ind_estado'=>'AN'
        ));
		
		$elimar=$this->_db->prepare("
                 UPDATE
                cd_c001_distribucion_interno
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  ind_estado=:ind_estado  
				 WHERE
                    fk_cdb001_num_documento='$idDocumento'
            ");
            $elimar->execute(array(
				'ind_estado'=>'AN'
        ));
		
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }

    }
	
	
}
