<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'listaModelo.php';
class documentinternosModelo extends listaModelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
             SELECT
			cd_b001.*,
			cd_c001.*,
			a018.ind_usuario,
			rh_c063.ind_descripcion_cargo,
			rh_c063.pk_num_puestos,
			car.ind_descripcion_cargo AS cargo_especial,
			a004.ind_dependencia,
	  a003.ind_nombre1,
		  CONCAT(a003.ind_nombre1,'  ',a003.ind_apellido1)  AS nombre_apellidos,
		  CONCAT(p.ind_nombre1,'  ',p.ind_apellido1)  AS Persona_anulado
          FROM
            cd_b001_documento_interno cd_b001
			 LEFT JOIN
           cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			LEFT JOIN
			a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_b001.fk_a018_num_seguridad_usuario
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente
			LEFT JOIN
			a003_persona p ON p.pk_num_persona = cd_b001.ind_persona_anulado
			LEFT JOIN
			rh_c063_puestos car ON car.pk_num_puestos = cd_b001.ind_encargaduria_especial 
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = cd_b001.ind_cargo_remitente 
		
		  WHERE
            cd_b001.pk_num_documento='$idDocumento'
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
	 public function metListarTipoDocumentoImp($idDocumento)
    {
        $concepto = $this->_db->query(
            "SELECT
              cd_c001.*,
			  cd_b001.num_secuencia,cd_b001.ind_asunto,cd_b001.fec_registro,cd_b001.pk_num_documento,cd_b001.fk_a004_num_dependencia,cd_b001.ind_descripcion,cd_b001.num_cod_interno,cd_b001.txt_descripcion_anexo,
			 rh_c006.ind_nombre_cargo AS cargodepen,
			a004.ind_dependencia,
			cd_c003.ind_descripcion AS tipoDocumento,
		  CONCAT(a003.ind_nombre1,' ',a003.ind_apellido1)  AS nombre_apellidos
			FROM
               cd_c001_distribucion_interno cd_c001
			    LEFT JOIN
           cd_b001_documento_interno cd_b001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			 LEFT JOIN
			 a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c001.ind_dependencia_destinataria
			 LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = a004.fk_a003_num_persona_responsable
			LEFT JOIN
		rh_c006_tipo_cargo rh_c006 ON rh_c006.pk_num_cargo = a003.pk_num_persona 
			
            WHERE cd_c001.fk_cdb001_num_documento='$idDocumento'
           
          "
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }
	 
	 
	  public function metMostrarTipoDocumentoDet($idDocumento)
    {
        $concepto = $this->_db->query(
            "SELECT
              cd_c001.*,
			a004.ind_dependencia,
		  CONCAT(a003.ind_nombre1,' ',a003.ind_apellido1)  AS nombre_apellidos
			FROM
               cd_c001_distribucion_interno cd_c001

			 LEFT JOIN
			 a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c001.ind_dependencia_destinataria
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = a004.fk_a003_num_persona_responsable
            WHERE cd_c001.fk_cdb001_num_documento='$idDocumento'
           
          "
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }
	
	public function metMostrarTipoempleadoDet($idDocumento)
    {
        $concepto = $this->_db->query(
            "SELECT
              cd_c001.*,
			  vl_rh.*,
			a004.ind_dependencia,
		  CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_apellido1)  AS nombre_particular
			FROM
               cd_c001_distribucion_interno cd_c001
             INNER JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_persona = cd_c001.ind_persona_destinataria
			  LEFT JOIN
			 a004_dependencia  a004 ON a004.pk_num_dependencia =  vl_rh.fk_a004_num_dependencia
			WHERE cd_c001.fk_cdb001_num_documento='$idDocumento'
			 
          "
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }
		


	public function metListarDodumentInt()
    {
        $DodumentInt= $this->_db->query("SELECT * FROM cd_b001_documento_interno");
        $DodumentInt->setFetchMode(PDO::FETCH_ASSOC);
        return $DodumentInt->fetchAll();
		
    }
	
    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			cd_c011.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			cd_c003.ind_descripcion AS tipoDocumento,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM cd_b001_documento_interno cd_c011
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c011.fk_a001_num_organismo
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento
		   LEFT JOIN 
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c011.fk_a004_num_dependencia
				WHERE
            cd_c011.ind_estado!='PE' 
            and a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario AND
             a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7' ORDER BY cd_c011.pk_num_documento DESC

		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }

		public function getReporteDistribdocumenint($idDocumento)
    {
			
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT 
			cd_c001.*,
			cd_b001.fk_cdc003_num_tipo_documento,
		   cd_b001.ind_documento_completo,
		   cd_b001.ind_asunto,
		   cd_b001.pk_num_documento,
		   cd_b001.ind_descripcion,
		   rh_c063.ind_descripcion_cargo,
			 DATE_FORMAT( cd_b001.fec_registro  ,'%d/%m/%Y') as fec_registro,
			 CONCAT(a003.ind_nombre1,' ',a003.ind_apellido1)  AS nombre_apellidos,
			a004.ind_dependencia AS descipDependencia,
			cd_c003.ind_descripcion AS tipoDoc
			FROM 
			 cd_c001_distribucion_interno cd_c001 
			INNER JOIN
			 cd_b001_documento_interno cd_b001 ON cd_b001.pk_num_documento =  cd_c001.fk_cdb001_num_documento
			 INNER JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente 
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = a003.pk_num_persona 
				LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento 
			 WHERE cd_c001.fk_cdb001_num_documento='$idDocumento'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll � Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

		// traer el ultimo numero secuencial del tipo de documento---

		public function metContar($tabla,$anio,$campo,$codigodep,$campo3,$codigocorresp,$campo2)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              max(num_secuencia) AS cantidad
            FROM
            $tabla
            WHERE
            $campo='$anio' AND $campo3='$codigodep'  AND $campo2='$codigocorresp' 
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

	
    public function metBuscarDependencia($id)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
            a004_dependencia
            WHERE
            pk_num_dependencia='$id'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }
	
	public function metBuscarcorrespondencia($id)
    {
        $generartipoCorresp = $this->_db->query("
            SELECT
              *
            FROM
            cd_c003_tipo_correspondencia
            WHERE
            pk_num_tipo_documento='$id'
              ");
        $generartipoCorresp->setFetchMode(PDO::FETCH_ASSOC);
        return $generartipoCorresp->fetch();
    }
	

		
    public function metCrearTipoDocumento($secuencia,
                                          $codigoInt,
                                          $codigoReq,
                                          $codigodep,
                                          $persremitent,
                                          $cargoremitent,
                                          $fec_documento,
                                          $flag_encarg,
                                          $encargaduria,
                                          $asunto,
										  $descripasunto,
                                          $plazoatencion,
                                          $anexo,
                                          $descripanexo,
										  $estatus,
                                          $codigocorresp,
                                          $numorganismo,
										  $depextern=false,
                                          $num_particular=false,
                                          $cargoextern=false,
                                          $descripdep=false,
                                          $descriprep=false,
                                          $copia)
	
    {
	  
		$this->_db->beginTransaction();

            $nuevoRegistro=$this->_db->prepare("
                    INSERT INTO
                    cd_b001_documento_interno
                  SET
				  	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), 
					num_secuencia=:num_secuencia,
					num_cod_interno=:num_cod_interno,
					ind_documento_completo=:ind_documento_completo,
					fec_registro=:fec_documento,
				  	fec_documento=:fec_documento,
					ind_dependencia_remitente=:ind_dependencia_remitente,
					ind_persona_remitente=:ind_persona_remitente,
					ind_cargo_remitente=:ind_cargo_remitente,
					ind_flag_encargaduria=:ind_flag_encargaduria,
					ind_encargaduria_especial=:ind_encargaduria_especial,
					ind_asunto=:ind_asunto,
					ind_descripcion=:ind_descripcion,
					ind_plazo_atencion=:ind_plazo_atencion,
					txt_contenido=:txt_contenido,
					ind_media_firma=:ind_media_firma,
					ind_motivo_anulado=:ind_motivo_anulado,
					fec_anulado=:fec_anulado,
					ind_persona_anulado=:ind_persona_anulado,
					ind_anexo=:ind_anexo,
					txt_descripcion_anexo=:txt_descripcion_anexo,   
					ind_ruta_doc=:ind_ruta_doc,
					ind_estado=:ind_estado,
					fec_mes=:fec_mes, 
					fec_annio=:fec_annio,
					fk_cdc003_num_tipo_documento=:fk_cdc003_num_tipo_documento,
					fk_a001_num_organismo=:fk_a001_num_organismo, 
					fk_a004_num_dependencia=:fk_a004_num_dependencia
                ");

            $nuevoRegistro->execute(array(
             	'num_secuencia'=>$secuencia,
				'num_cod_interno'=>$codigoInt,
			    'ind_documento_completo'=>$codigoReq,
				'fec_registro'=>date('Y-m-d'),
                'fec_documento'=>$fec_documento,
                'ind_dependencia_remitente'=>$codigodep,
                'ind_persona_remitente'=>$persremitent,
                'ind_cargo_remitente'=>$cargoremitent,
				'ind_flag_encargaduria'=>$flag_encarg,
                'ind_encargaduria_especial'=>$encargaduria,
				'ind_asunto'=>$asunto,
                'ind_descripcion'=>$descripasunto,
				'ind_plazo_atencion'=>$plazoatencion,
				'txt_contenido'=>NULL,
                'ind_media_firma'=>NULL,
				'ind_motivo_anulado'=>NULL,
				'fec_anulado'=>NULL,
				'ind_persona_anulado'=>NULL,
				'ind_anexo'=>$anexo,
				'txt_descripcion_anexo'=>$descripanexo,
				'ind_ruta_doc'=>NULL,
				'ind_estado'=>'PR',
                'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
				'fk_cdc003_num_tipo_documento'=>$codigocorresp,
				'fk_a001_num_organismo'=>$numorganismo,
				'fk_a004_num_dependencia'=>$codigodep,
			
            ));
			
			$idDocumento= $this->_db->lastInsertId();
	 
			 $nuevoRegistro=$this->_db->prepare("
                    INSERT INTO
                    cd_c001_distribucion_interno
                  SET
				  	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
					ind_dependencia_destinataria=:ind_dependencia_destinataria, 
					ind_persona_destinataria=:ind_persona_destinataria,
					ind_cargo_destinatario=:ind_cargo_destinatario,
					ind_desc_dependencia=:ind_desc_dependencia,
					ind_desc_representante=:ind_desc_representante,
					ind_con_copia=:ind_con_copia,
					ind_plazo_atencion=:ind_plazo_atencion, 
					fec_envio=:fec_envio, 
					fec_acuse=:fec_acuse,
					ind_estado=:ind_estado,
					fk_cdb001_num_documento='$idDocumento',
 					fk_a003_num_persona=:fk_a003_num_persona
                ");
			
			 
		   if($num_particular && $copia==''){
			foreach($num_particular AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>'',
                'ind_persona_destinataria'=>$i,
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_desc_dependencia'=>$descripdep[$i],
				'ind_desc_representante'=>$descriprep[$i],
				'ind_con_copia'=>'2',
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_envio'=>NULL,
                'fec_acuse'=>NULL,
				'ind_estado'=>'PR',

				'fk_a003_num_persona'=>NULL,
           ));
		   	}			
		  }
		  
		   else{
		    if($num_particular && $copia!=''){
			foreach($num_particular AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>'',
                'ind_persona_destinataria'=>$i,
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_desc_dependencia'=>$descripdep[$i],
				'ind_desc_representante'=>$descriprep[$i],
				'ind_con_copia'=>'0',
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_envio'=>NULL,
                'fec_acuse'=>NULL,
				'ind_estado'=>'PR',
				'fk_a003_num_persona'=>NULL,
           ));
		   	}			
		  }
		    }
	
			 if($depextern!='' && $copia!=''){
			foreach($depextern AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>$i,
                'ind_persona_destinataria'=>'',
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_desc_dependencia'=>$descripdep[$i],
				'ind_desc_representante'=>$descriprep[$i],
				'ind_con_copia'=>(empty($copia[$i])?'0':'1'),
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_envio'=>NULL,
                'fec_acuse'=>NULL,
				'ind_estado'=>'PR',
				'fk_a003_num_persona'=>NULL,
           ));
		   	}			
		  }
		
		 else{
		 if($depextern!='' && $copia==''){
			foreach($depextern AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>$i,
                'ind_persona_destinataria'=>'',
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_desc_dependencia'=>$descripdep[$i],
				'ind_desc_representante'=>$descriprep[$i],
				'ind_con_copia'=>'2',
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_envio'=>NULL,
                'fec_acuse'=>NULL,
				'ind_estado'=>'PR',
         	//	'fec_mes'=>date('m'),
			//	'fec_annio'=>date('Y'),
				'fk_a003_num_persona'=>NULL,
           ));
		   	}			
		  }
		  }
		    $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }

    public function metModificarTipoDocumento($codigodep,
                                              $codigoReq,
                                              $persremitent,
                                              $cargoremitent,
                                              $fec_documento,
                                              $flag_encarg,
                                              $encargaduria,
                                              $asunto,
										      $descripasunto,
                                              $plazoatencion,
                                              $anexo,
                                              $descripanexo,
										      $estatus,
                                              $codigocorresp,
                                              $numorganismo,
										      $depextern=false,
                                              $num_particular=false,
                                              $cargoextern=false,
                                              $descripdep=false,
                                              $descriprep=false,
                                              $copia,
                                              $idDocumento)
    {
        $this->_db->beginTransaction();
        $usuario = Session::metObtener('idUsuario');
		$codigo = count($this->metListarTipoDocumento($usuario));
        $mun = "0";
        for ($i = 0; $i < (3 - strlen($codigo)); $i++) {
            $mun .= "0";
        }
        $codigo = $mun . $codigo;
        $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                       cd_b001_documento_interno
                  SET
				  	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
				  	fec_documento=:fec_documento, 
				  	fec_registro=:fec_documento, 
					ind_dependencia_remitente=:ind_dependencia_remitente,
					ind_persona_remitente=:ind_persona_remitente,
					ind_cargo_remitente=:ind_cargo_remitente,
					ind_flag_encargaduria=:ind_flag_encargaduria,
					ind_encargaduria_especial=:ind_encargaduria_especial,
					ind_asunto=:ind_asunto,
					ind_descripcion=:ind_descripcion,
					ind_plazo_atencion=:ind_plazo_atencion,
					ind_anexo=:ind_anexo,
					txt_descripcion_anexo=:txt_descripcion_anexo,   
					fk_a004_num_dependencia=:fk_a004_num_dependencia
                      WHERE
                    pk_num_documento='$idDocumento'
            ");
				//$codcompletoer=
   	         $nuevoRegistro->execute(array(

                'fec_documento'=>$fec_documento,
                'fec_registro'=>$fec_documento,
                'ind_dependencia_remitente'=>$codigodep,
                'ind_persona_remitente'=>$persremitent,
                'ind_cargo_remitente'=>$cargoremitent,
				'ind_flag_encargaduria'=>$flag_encarg,
                'ind_encargaduria_especial'=>$encargaduria,
				'ind_asunto'=>$asunto,
                'ind_descripcion'=>$descripasunto,
				'ind_plazo_atencion'=>$plazoatencion,
				'ind_anexo'=>$anexo,
				'txt_descripcion_anexo'=>$descripanexo,
				'fk_a004_num_dependencia'=>$codigodep
				));

			 $nuevoRegistro = $this->_db->query(
            "DELETE FROM cd_c001_distribucion_interno WHERE fk_cdb001_num_documento='$idDocumento'"
        );
					
			 $nuevoRegistro=$this->_db->prepare("
                      INSERT INTO
                        cd_c001_distribucion_interno
                      SET
                	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
					ind_dependencia_destinataria=:ind_dependencia_destinataria, 
					ind_persona_destinataria=:ind_persona_destinataria,
					ind_cargo_destinatario=:ind_cargo_destinatario,
					ind_desc_dependencia=:ind_desc_dependencia,
					ind_desc_representante=:ind_desc_representante,
					ind_con_copia=:ind_con_copia,
					ind_plazo_atencion=:ind_plazo_atencion, 
					ind_estado=:ind_estado,
					fk_cdb001_num_documento='$idDocumento'
				  	
            ");
			
			 if($depextern){
			foreach($depextern AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>$i,
                'ind_persona_destinataria'=>'',
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_desc_dependencia'=>$descripdep[$i],
				'ind_desc_representante'=>$descriprep[$i],
				'ind_con_copia'=>(empty($copia[$i])?'0':'1'),
                'ind_plazo_atencion'=>$plazoatencion,
				'ind_estado'=>'PP',
           ));
		   	}			
		  }
	 
		   if($num_particular){
			foreach($num_particular AS $i){
            $nuevoRegistro->execute(array(
                'ind_dependencia_destinataria'=>'',
                'ind_persona_destinataria'=>$i,
                'ind_cargo_destinatario'=>$cargoextern[$i],
				'ind_desc_dependencia'=>$descripdep[$i],
				'ind_desc_representante'=>$descriprep[$i],
				'ind_con_copia'=>'',
                'ind_plazo_atencion'=>$plazoatencion,
				'ind_estado'=>'PP',
           ));
		   	}			
		  }
	


            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }

     public function metanularTipoDocumento($motivoanulado,$personanulado,$estatus,$idDocumento)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                 UPDATE
                cd_b001_documento_interno
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  ind_motivo_anulado=:ind_motivo_anulado,  fec_anulado=:fec_anulado, 
					ind_persona_anulado=:ind_persona_anulado, ind_estado=:ind_estado  
				 WHERE
                    pk_num_documento='$idDocumento'
            ");
            $elimar->execute(array(
				'ind_motivo_anulado'=>$motivoanulado,
                'fec_anulado'=>date('Y-m-d H:i:s'),
				'ind_persona_anulado'=>$personanulado,
				'ind_estado'=>'AN'
        ));
		
		$elimar=$this->_db->prepare("
                 UPDATE
                cd_c001_distribucion_interno
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  ind_estado=:ind_estado  
				 WHERE
                    fk_cdb001_num_documento='$idDocumento'
            ");
            $elimar->execute(array(
				'ind_estado'=>'AN'
        ));
		
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }

    }
	
	
}
