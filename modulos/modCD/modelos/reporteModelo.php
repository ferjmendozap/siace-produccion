<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Reporte de entrada de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/


class reporteModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }


    #ejecuto la consulta a la base de datos

   public function getReporteDisponibilidad($num_org_ext,$ind_estado,$num_depend_ext,$desde,$hasta,$tipodoc)
    {
	if($num_org_ext!="error"){
            $complemento1=" AND cd_c004.num_org_ext='".$num_org_ext."'";
        }else{
            $complemento1=" ";
        }
		if($num_depend_ext!="error"){
            $complemento2=" AND cd_c004.num_depend_ext='".$num_depend_ext."'";
        }else{
            $complemento2=" ";
        }
		if($ind_estado!="error"){
            $complemento3=" AND cd_c004.ind_estado='".$ind_estado."'";
        }else{
            $complemento3=" ";
        }
		if($desde!="error"){
            $complemento4=" AND cd_c004.fec_registro>='".$desde."'";
        }else{
            $complemento4=" ";
        }
        if($hasta!="error"){
            $complemento5=" AND cd_c004.fec_registro<='".$hasta."'";
        }else{
            $complemento5=" ";
        }
        if($tipodoc!="error"){
            $complemento6=" AND cd_c004.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento6=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT 
			cd_c004.*,
			DATE_FORMAT( cd_c004.fec_registro  ,'%d/%m/%Y') as fec_salida,
		    CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_apellidos,
		    cd_c005.ind_desc_representante,
			a039.ind_nombre_ente AS organismo,
			dep.ind_nombre_ente AS descipDependencia
			FROM cd_c004_documento_entrada cd_c004
			LEFT JOIN
            cd_c005_distribucion_entrada  cd_c005 ON  cd_c005.fk_cdc004_num_documento = cd_c004.pk_num_documento
			LEFT JOIN
			a039_ente a039  ON a039.pk_num_ente=cd_c004.num_org_ext
			LEFT JOIN
			a039_ente dep  ON dep.pk_num_ente=cd_c004.num_depend_ext
		    LEFT JOIN
		    a003_persona a003 ON a003.pk_num_persona = cd_c004.num_particular_rem 
			WHERE 1 $complemento1 $complemento2 $complemento3  $complemento4  $complemento5 $complemento6"
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
	
	 #ejecuto la consulta a la base de datos

   public function getReporteDistribucion($ind_dependencia_destinataria,$ind_estado,$num_documento,$desde,$hasta,$tipodoc)
    {

		if($ind_dependencia_destinataria!="error"){
            $complemento1=" AND cd_c005.ind_dependencia_destinataria='".$ind_dependencia_destinataria."'";
        }else{
            $complemento1=" ";
        }

		if($ind_estado!="error"){
            $complemento2=" AND cd_c005.ind_estado='".$ind_estado."'";
        }else{
            $complemento2=" ";
        }

		if($num_documento!="error"){
            $complemento3=" AND cd_c004.num_documento='".$num_documento."'";
        }else{
            $complemento3=" ";
        }

		if($desde!="error"){
            $complemento4=" AND cd_c005.fec_envio>='".$desde."'";
        }else{
            $complemento4=" ";
        }

        if($hasta!="error"){
            $complemento5=" AND cd_c005.fec_envio<='".$hasta."'";
        }else{
            $complemento5=" ";
        }
		 if($tipodoc!="error"){
            $complemento6=" AND cd_c005.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento6=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT 
			cd_c005.*,
			cd_c004.fk_cdc003_num_tipo_documento,
		   cd_c004.num_documento,
		   cd_c004.text_asunto,
		    cd_c004.num_secuencia,
			 DATE_FORMAT( cd_c005.fec_envio  ,'%d/%m/%Y') as fec_salida,
			cd_c003.ind_descripcion
			FROM 
			 cd_c005_distribucion_entrada cd_c005 
			LEFT JOIN
			 cd_c004_documento_entrada cd_c004 ON cd_c004.pk_num_documento =  cd_c005.fk_cdc004_num_documento
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c004.fk_cdc003_num_tipo_documento
			 WHERE 1 $complemento1 $complemento2 $complemento3  $complemento4  $complemento5 $complemento6"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

   public function getReporteDistribucionpordoc($num_org_ext,$num_depend_ext,$ind_estado,$num_documento,$desde,$hasta,$tipodoc)
    {

		if($num_org_ext!="error"){
            $complemento1=" AND cd_c004.num_org_ext='".$num_org_ext."'";
        }else{
            $complemento1=" ";
        }
		if($num_depend_ext!="error"){
            $complemento2=" AND cd_c004.num_depend_ext='".$num_depend_ext."'";
        }else{
            $complemento2=" ";
        }
		if($ind_estado!="error"){
            $complemento3=" AND cd_c004.ind_estado='".$ind_estado."'";
        }else{
            $complemento3=" ";
        }
		 if($num_documento!="error"){
            $complemento4=" AND cd_c004.pk_num_documento='".$num_documento."'";
        }else{
            $complemento4=" ";
        }
		if($desde!="error"){
            $complemento5=" AND cd_c004.fec_registro>='".$desde."'";
        }else{
            $complemento5=" ";
        }
        if($hasta!="error"){
            $complemento6=" AND cd_c004.fec_registro<='".$hasta."'";
        }else{
            $complemento6=" ";
        }
		 if($tipodoc!="error"){
            $complemento7=" AND cd_c004.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento7=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT
            cd_c004.*,
			cd_c003.ind_descripcion AS tipoDoc,
			dep.ind_nombre_ente AS nomDep,
		    nomor.ind_nombre_ente AS nomOrg,
		    CONCAT(a003.ind_nombre1,'   ',a003.ind_apellido1)  AS RepresentanteOrg,
		    CONCAT(pdep.ind_nombre1,'   ',pdep.ind_apellido1)  AS RepresentanteDep,
 		    CONCAT(p.ind_nombre1,'   ',p.ind_apellido1)  AS Particular
          FROM
            cd_c004_documento_entrada cd_c004
					LEFT JOIN
            a039_ente  dep ON dep.pk_num_ente =  cd_c004.num_depend_ext
			 LEFT JOIN
			a041_persona_ente a041 ON a041.fk_a039_num_ente=dep.pk_num_ente
          	LEFT JOIN    
		a039_ente  nomor ON nomor.pk_num_ente =  cd_c004.num_org_ext
			 LEFT JOIN
			a003_persona a003   ON a041.fk_a003_num_persona_titular=a003.pk_num_persona
			LEFT JOIN    
			a003_persona pdep   ON a041.fk_a003_num_persona_titular=pdep.pk_num_persona
		
			LEFT JOIN
			a003_persona p ON p.pk_num_persona = cd_c004.num_particular_rem
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c004.fk_cdc003_num_tipo_documento
			 WHERE 1 $complemento1 $complemento2 $complemento3 $complemento4 $complemento5 $complemento6 $complemento7 GROUP BY cd_c004.pk_num_documento" 
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
	
	public function getReporteDistribucionpordocDet($num_documento)
    {
			

		if($num_documento!="error"){
            $complemento4=" AND cd_c004.pk_num_documento='".$num_documento."'";
        }else{
            $complemento4=" ";
        }
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT
            cd_c004.*,
			cd_c005.*,
			cd_c003.ind_descripcion AS tipoDoc
          FROM
            cd_c004_documento_entrada cd_c004
			 INNER JOIN
           cd_c005_distribucion_entrada cd_c005 ON cd_c005.fk_cdc004_num_documento = cd_c004.pk_num_documento
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c004.fk_cdc003_num_tipo_documento
			 WHERE 1 $complemento4"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
	
	 public function getReporteSalidocumenext($num_org_ext,$num_depend_ext,$ind_estado,$desde,$hasta,$tipodoc)
    {
	
		
		if($num_org_ext!="error"){
            $complemento1=" AND cd_c012.ind_organismo_externo='".$num_org_ext."'";
        }else{
            $complemento1=" ";
        }
		
		if($num_depend_ext!="error"){
            $complemento2=" AND a004.pk_num_dependencia='".$num_depend_ext."'"; 
        }else{
            $complemento2=" ";
        }
		
		if($ind_estado!="error"){
            $complemento3=" AND cd_c012.ind_estado='".$ind_estado."'";
        }else{
            $complemento3=" ";
        }
		
		if($desde!="error"){
            $complemento4=" AND cd_c011.fec_documento>='".$desde."'";
        }else{
            $complemento4=" ";
        }

        if($hasta!="error"){
            $complemento5=" AND cd_c011.fec_documento<='".$hasta."'";
        }else{
            $complemento5=" ";
        }
		
		 if($tipodoc!="error"){
            $complemento6=" AND cd_c011.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento6=" ";
        }
	

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT
            cd_c011.*,
			cd_c012.*,
			a004.ind_dependencia,
			cd_c003.ind_descripcion
            FROM
            cd_c011_documento_salida cd_c011
			INNER JOIN
            cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.ind_dependencia_remitente
			LEFT JOIN
			a039_ente  a039 ON a039.pk_num_ente =  cd_c012.ind_organismo_externo
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento
			WHERE 1 $complemento1 $complemento2 $complemento3  $complemento4  $complemento5 $complemento6"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
	
 public function getReporteDistribucionSalidaext($num_org_ext,$num_depend_ext,$ind_estado,$desde,$hasta,$tipodoc)
    {
	
		
		if($num_org_ext!="error"){
            $complemento1=" AND cd_c012.ind_organismo_externo='".$num_org_ext."'";
        }else{
            $complemento1=" ";
        }
		
		if($num_depend_ext!="error"){
            $complemento2=" AND a004.pk_num_dependencia='".$num_depend_ext."'";
        }else{
            $complemento2=" ";
        }
		
		if($ind_estado!="error"){
            $complemento3=" AND cd_c011.ind_estado='".$ind_estado."'";
        }else{
            $complemento3=" ";
        }
		
		if($desde!="error"){
            $complemento4=" AND cd_c012.fec_envio>='".$desde."'";
        }else{
            $complemento4=" ";
        }

        if($hasta!="error"){
            $complemento5=" AND cd_c012.fec_envio<='".$hasta."'";
        }else{
            $complemento5=" ";
        }
		
		 if($tipodoc!="error"){
            $complemento6=" AND cd_c011.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento6=" ";
        }
	

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT
            cd_c011.*,
			cd_c012.*,
			cd_c003.ind_descripcion,
			a039.ind_nombre_ente AS nomOrg,
			dep.ind_nombre_ente AS nomDep
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			 LEFT JOIN
			a039_ente  a039 ON a039.pk_num_ente =  cd_c012.ind_organismo_externo
			 LEFT JOIN
			a039_ente  dep ON dep.pk_num_ente =  cd_c012.ind_dependencia_externa
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento 
			 WHERE 1 $complemento1 $complemento2 $complemento3  $complemento4  $complemento5 $complemento6 GROUP BY cd_c011.pk_num_documento" 
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

	public function getReporteDistribPorDocSalida($num_org_ext,$num_depend_ext,$ind_estado,$num_documento,$desde,$hasta,$tipodoc)
    {
	
		
		if($num_org_ext!="error"){
            $complemento1=" AND cd_c011.fk_a001_num_organismo='".$num_org_ext."'";
        }else{
            $complemento1=" ";
        }
		
		if($num_depend_ext!="error"){
            $complemento2=" AND cd_c011.ind_dependencia_remitente='".$num_depend_ext."'";
        }else{
            $complemento2=" ";
        }
		
		if($num_documento!="error"){
            $complemento3=" AND cd_c011.pk_num_documento='".$num_documento."'";
        }else{
            $complemento3=" ";
        }
		
		if($ind_estado!="error"){
            $complemento4=" AND cd_c011.ind_estado='".$ind_estado."'";
        }else{
            $complemento4=" ";
        }
		
		if($desde!="error"){
            $complemento5=" AND cd_c011.fec_registro>='".$desde."'";
        }else{
            $complemento5=" ";
        }

        if($hasta!="error"){
            $complemento6=" AND cd_c011.fec_registro<='".$hasta."'";
        }else{
            $complemento6=" ";
        }
		
		 if($tipodoc!="error"){
            $complemento7=" AND cd_c011.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento7=" ";
        }
	

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT 
			cd_c011.*,
			cd_c012.*,
			 DATE_FORMAT( cd_c011.fec_registro  ,'%d/%m/%Y') as fec_registro,
			a004.ind_dependencia AS descipDependencia,
			cd_c003.ind_descripcion AS tipoDoc
			FROM 
			 cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			 INNER JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.ind_dependencia_remitente
				LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento 
			 WHERE 1 $complemento1 $complemento2 $complemento3  $complemento4  $complemento5 $complemento6 $complemento7 GROUP BY cd_c011.pk_num_documento" 
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
		
		public function getReporteDistribPorSalidaDet($num_documento)
    {
			

		if($num_documento!="error"){
            $complemento4=" AND cd_c011.pk_num_documento='".$num_documento."'";
        }else{
            $complemento4=" ";
        }
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT
            cd_c011.*,
			cd_c012.*,
			cd_c003.ind_descripcion AS tipoDoc,
			a039.ind_nombre_ente AS nomOrg,
			dep.ind_nombre_ente AS nomDep,
			concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS nomProveedor
          FROM
            cd_c011_documento_salida cd_c011
			 LEFT JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			 LEFT JOIN
			a039_ente  a039 ON a039.pk_num_ente =  cd_c012.ind_organismo_externo
			  LEFT JOIN
             a039_ente  dep ON dep.pk_num_ente =  cd_c012.ind_dependencia_externa
			  LEFT JOIN
           a003_persona AS persona ON persona.pk_num_persona =  cd_c012.ind_empresa_externa
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento 
			 WHERE 1 $complemento4"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



		public function getReporteDistribHistDocSalida($num_org_ext,$num_depend_ext,$ind_estado,$num_documento,$desde,$hasta,$tipodoc)
    {
	
		
		if($num_org_ext!="error"){
            $complemento1=" AND cd_c011.fk_a001_num_organismo='".$num_org_ext."'";
        }else{
            $complemento1=" ";
        }
		
		if($num_depend_ext!="error"){
            $complemento2=" AND cd_c011.ind_dependencia_remitente='".$num_depend_ext."'";
        }else{
            $complemento2=" ";
        }
		
		if($num_documento!="error"){
            $complemento3=" AND cd_c011.pk_num_documento='".$num_documento."'";
        }else{
            $complemento3=" ";
        }
		
		if($ind_estado!="error"){
            $complemento4=" AND cd_e002.ind_estado='".$ind_estado."'";
        }else{
            $complemento4=" ";
        }
		
		if($desde!="error"){
            $complemento5=" AND cd_c011.fec_registro>='".$desde."'";
        }else{
            $complemento5=" ";
        }

        if($hasta!="error"){
            $complemento6=" AND cd_c011.fec_registro<='".$hasta."'";
        }else{
            $complemento6=" ";
        }
		
		 if($tipodoc!="error"){
            $complemento7=" AND cd_c011.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento7=" ";
        }
	

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
             "SELECT 
			cd_c011.*,
			cd_c012.*,
			cd_e001.*,
			cd_e002.*,
			cd_e002.ind_estado,
			 DATE_FORMAT( cd_c011.fec_registro  ,'%d/%m/%Y') as fec_registro,
			a004.ind_dependencia AS descipDependencia,
			cd_c003.ind_descripcion AS tipoDoc
			FROM 
			 cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			  INNER JOIN
           cd_e001_historico cd_e001 ON cd_e001.fk_cdc012_num_distribucion = cd_c012.pk_num_distribucion
		    INNER JOIN
           cd_e002_historico_detalle cd_e002 ON cd_e002.fk_cde001_num_historico = cd_e001.pk_num_historico
			 LEFT JOIN
			 a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.ind_dependencia_remitente
				LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento 
			 WHERE 1 $complemento1 $complemento2 $complemento3  $complemento4  $complemento5 $complemento6 $complemento7 GROUP BY cd_e001.fk_cdc012_num_distribucion" 
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
		
		public function getReporteDistribHistDocSalidaDet($num_documento)
    {
			

		if($num_documento!="error"){
            $complemento4=" AND cd_c011.pk_num_documento='".$num_documento."'";
        }else{
            $complemento4=" ";
        }
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT
            cd_c011.*,
			cd_c012.*,
			cd_c003.ind_descripcion AS tipoDoc,
			a039.ind_nombre_ente AS nomOrg,
			dep.ind_nombre_ente AS nomDep,
			concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS nomProveedor
          FROM
 			cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
		    INNER JOIN
           cd_e001_historico cd_e001 ON cd_e001.fk_cdc012_num_distribucion = cd_c012.pk_num_distribucion
		    INNER JOIN
           cd_e002_historico_detalle cd_e002 ON cd_e002.fk_cde001_num_historico = cd_e001.pk_num_historico
			 LEFT JOIN
			a039_ente  a039 ON a039.pk_num_ente =  cd_c012.ind_organismo_externo
			  LEFT JOIN
             a039_ente  dep ON dep.pk_num_ente =  cd_c012.ind_dependencia_externa
			  LEFT JOIN
           a003_persona AS persona ON persona.pk_num_persona =  cd_c012.ind_empresa_externa
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento 
			 WHERE 1 $complemento4"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


	  public function getReporteDocumentoint($ind_dependencia_remitente,$ind_dependencia_destinataria,$ind_estado,$num_documento,$desde,$hasta,$tipodoc)

    {
			if($ind_dependencia_remitente!="error"){
            $complemento1=" AND cd_b001.ind_dependencia_remitente='".$ind_dependencia_remitente."'";
        }else{
            $complemento1=" ";
        }
		if($ind_dependencia_destinataria!="error"){
            $complemento2=" AND cd_c001.ind_dependencia_destinataria='".$ind_dependencia_destinataria."'";
        }else{
            $complemento2=" ";
        }
		if($ind_estado!="error"){
            $complemento3=" AND cd_b001.ind_estado='".$ind_estado."'";
        }else{
            $complemento3=" ";
        }
		if($num_documento!="error"){
            $complemento4=" AND cd_b001.ind_documento_completo='".$num_documento."'";
        }else{
            $complemento4=" ";
        }
		if($desde!="error"){
            $complemento5=" AND cd_b001.fec_registro>='".$desde."'";
        }else{
            $complemento5=" ";
        }
        if($hasta!="error"){
            $complemento6=" AND cd_b001.fec_registro<='".$hasta."'";
        }else{
            $complemento6=" ";
        }
		 if($tipodoc!="error"){
            $complemento7=" AND cd_b001.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento7=" ";
        }
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT 
			cd_c001.*,
			cd_b001.fk_cdc003_num_tipo_documento,
		   cd_b001.ind_documento_completo,
		   cd_b001.ind_asunto,
		   cd_b001.fec_annio,
		   cd_b001.ind_descripcion,
			 DATE_FORMAT( cd_b001.fec_registro  ,'%d/%m/%Y') as fec_registro,
			a004.ind_dependencia AS descipDependencia,
			cd_c003.ind_descripcion AS tipoDoc
			FROM 
			 cd_c001_distribucion_interno cd_c001 
			LEFT JOIN
			 cd_b001_documento_interno cd_b001 ON cd_b001.pk_num_documento =  cd_c001.fk_cdb001_num_documento
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento
			 WHERE 1 $complemento1 $complemento2 $complemento3  $complemento4  $complemento5 $complemento6 $complemento7"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


	 public function getReporteDistribdocumenint($ind_dependencia_remitente,$ind_dependencia_destinataria,$ind_estado,$num_documento,$desde,$hasta,$tipodoc)
    {
			
			if($ind_dependencia_remitente!="error"){
            $complemento1=" AND cd_b001.ind_dependencia_remitente='".$ind_dependencia_remitente."'";
        }else{
            $complemento1=" ";
        }
		
		if($ind_dependencia_destinataria!="error"){
            $complemento2=" AND cd_c001.ind_dependencia_destinataria='".$ind_dependencia_destinataria."'";
        }else{
            $complemento2=" ";
        }
		
		if($ind_estado!="error"){
            $complemento3=" AND cd_b001.ind_estado='".$ind_estado."'";
        }else{
            $complemento3=" ";
        }
		
		if($num_documento!="error"){
            $complemento4=" AND cd_b001.pk_num_documento='".$num_documento."'";
        }else{
            $complemento4=" ";
        }
				
		if($desde!="error"){
            $complemento5=" AND cd_c001.fec_envio>='".$desde."'";
        }else{
            $complemento5=" ";
        }

        if($hasta!="error"){
            $complemento6=" AND cd_c001.fec_envio<='".$hasta."'";
        }else{
            $complemento6=" ";
        }
		
		 if($tipodoc!="error"){
            $complemento7=" AND cd_b001.fk_cdc003_num_tipo_documento='".$tipodoc."'";
        }else{
            $complemento7=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT 
			cd_c001.*,
			cd_b001.*,
			 DATE_FORMAT( cd_b001.fec_registro  ,'%d/%m/%Y') as fec_registro,
			a004.ind_dependencia AS descipDependencia,
			cd_c003.ind_descripcion AS tipoDoc
			FROM 
			 cd_c001_distribucion_interno cd_c001 
			INNER JOIN
			 cd_b001_documento_interno cd_b001 ON cd_b001.pk_num_documento =  cd_c001.fk_cdb001_num_documento
			 INNER JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.ind_dependencia_remitente
					LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento 
			 WHERE 1 $complemento1 $complemento2 $complemento3  $complemento4  $complemento5 $complemento6 $complemento7 GROUP BY cd_b001.pk_num_documento"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

	public function getReporteDistribdocumenDet($num_documento)
    {
			

		if($num_documento!="error"){
            $complemento4=" AND cd_b001.pk_num_documento='".$num_documento."'";
        }else{
            $complemento4=" ";
        }
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
           "SELECT 
			cd_c001.*,
			cd_b001.fk_cdc003_num_tipo_documento,
		   cd_b001.ind_documento_completo,
		   cd_b001.ind_asunto,
		   cd_b001.pk_num_documento,
		   cd_b001.ind_descripcion,
		    cd_b001.fec_annio,
			 DATE_FORMAT( cd_b001.fec_registro  ,'%d/%m/%Y') as fec_registro,
			a004.ind_dependencia AS descipDependencia,
			cd_c003.ind_descripcion AS tipoDoc
			FROM 
			 cd_c001_distribucion_interno cd_c001 
			LEFT JOIN
			 cd_b001_documento_interno cd_b001 ON cd_b001.pk_num_documento =  cd_c001.fk_cdb001_num_documento
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c001.ind_dependencia_destinataria
				LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento
			 WHERE 1 $complemento4"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



}

