<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Atender Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 
class atenderdocumentoControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('atenderdocumento');
    }


    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
		
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		$validarjs= array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validarjs);
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento());
        $this->atVista->metRenderizar('listado');
    }
	
 
    public function metCrearModificar()
    {
	$js = array(
			'materialSiace/core/demo/DemoFormWizard',
			'materialSiace/core/demo/DemoFormComponents',
		);
		$complementosJs = array(
			'select2/select2.min',
			'wizard/jquery.bootstrap.wizard.min',
			'bootstrap-datepicker/bootstrap-datepicker',
		);

		$complementosCss = array(
			'wizard/wizardfa6c',
			'jquery-validation/dist/site-demo',
			'select2/select201ef',
		);
		$this->atVista->metCargarJs($js);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarCssComplemento($complementosCss);
		
		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('ind_estatus','num_particular_rem','num_depend_ext','num_org_ext',
			'ind_informe_escrito','ind_hablar_conmigo','ind_coordinar_con','ind_preparar_memorandum',
			'ind_investigar_informar','ind_observacion','ind_tramitar_conclusion','ind_distribuir','ind_conocimiento',
			'ind_preparar_constentacion','ind_archivar','ind_registro_de','ind_preparar_oficio',
			'ind_conocer_opinion','ind_tramitar_caso','ind_acusar_recibo','ind_tramitar_en'	);
			$Excceccion2=array('ind_tramitar_en');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind = $this->metValidarFormArrayDatos('form', 'int',$Excceccion2);
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$Excceccion);
			
          if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        


			if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
			
			if(!isset($validacion['ind_informe_escrito'])){
                $validacion['ind_informe_escrito']=0;
            }
			
			if(!isset($validacion['ind_hablar_conmigo'])){
                $validacion['ind_hablar_conmigo']=0;
            }

			if(!isset($validacion['ind_investigar_informar'])){
                $validacion['ind_investigar_informar']=0;
            }
			
			if(!isset($validacion['ind_tramitar_conclusion'])){
                $validacion['ind_tramitar_conclusion']=0;
            }
			
			if(!isset($validacion['ind_distribuir'])){
                $validacion['ind_distribuir']=0;
            }
			
			if(!isset($validacion['ind_conocimiento'])){
                $validacion['ind_conocimiento']=0;
            }
			
			if(!isset($validacion['ind_preparar_constentacion'])){
                $validacion['ind_preparar_constentacion']=0;
            }
			
			if(!isset($validacion['ind_archivar'])){
                $validacion['ind_archivar']=0;
            }

            if(!isset($validacion['ind_observacion'])){
                $validacion['ind_observacion']=null;
            }
			
			if(!isset($validacion['ind_conocer_opinion'])){
                $validacion['ind_conocer_opinion']=0;
            }
			
			if(!isset($validacion['ind_tramitar_caso'])){
                $validacion['ind_tramitar_caso']=0;
            }
			
			if(!isset($validacion['ind_acusar_recibo'])){
                $validacion['ind_acusar_recibo']=0;
            }
			
			if(!isset($validacion['ind_persona_destinataria'])){
                $validacion['ind_persona_destinataria']=false;
            }
			
			if(!isset($validacion['ind_dependencia_destinataria'])){
                $validacion['ind_dependencia_destinataria']=false;
            }
			
			if(!isset($validacion['ind_desc_dependencia'])){
                $validacion['ind_desc_dependencia']=false;
            }
			
			if(!isset($validacion['ind_desc_representante'])){
                $validacion['ind_desc_representante']=false;
            }
			
			if(!isset($validacion['ind_cargo_destinatario'])){
                $validacion['ind_cargo_destinatario']=false;
            }
			

            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento(
                        $validacion['num_documento'],
                        $validacion['fec_documento'],
                        $validacion['text_asunto'],
                        $validacion['txt_descripcion_asunto'],
                        $validacion['ind_descripcion_anexo'],
                        $validacion['ind_anexo'],
                        $validacion['ind_nombre_mensajero'],
                        $validacion['ind_cedula_mensajero'],
                        $validacion['ind_estado'],
                        $validacion['fec_mes'],
                        $validacion['fec_annio'],
                        $validacion['num_particular_rem'],
                        $validacion['num_depend_ext'],
                        $validacion['fk_cdc003_num_tipo_documento'],
                        $validacion['num_org_ext'],
                        $validacion['ind_informe_escrito'],
                        $validacion['ind_hablar_conmigo'],
                        $validacion['ind_coordinar_con'],
                        $validacion['ind_preparar_memorandum'],
                        $validacion['ind_investigar_informar'],
                        $validacion['ind_tramitar_conclusion'],
                        $validacion['ind_distribuir'],
                        $validacion['ind_conocimiento'],
                        $validacion['ind_preparar_constentacion'],
                        $validacion['ind_archivar'],
                        $validacion['ind_registro_de'],
                        $validacion['ind_preparar_oficio'],
                        $validacion['ind_conocer_opinion'],
                        $validacion['ind_tramitar_caso'],
                        $validacion['ind_acusar_recibo'],
                        $validacion['ind_tramitar_en'],
                        $validacion['ind_observacion']);
                        $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoDocumento->metModificarTipoDocumento(				
                        $validacion['ind_informe_escrito'],
                        $validacion['ind_hablar_conmigo'],
                        $validacion['ind_coordinar_con'],
                        $validacion['ind_preparar_memorandum'],
                        $validacion['ind_investigar_informar'],
                        $validacion['ind_tramitar_conclusion'],
                        $validacion['ind_distribuir'],
                        $validacion['ind_conocimiento'],
                        $validacion['ind_preparar_constentacion'],
                        $validacion['ind_archivar'],
                        $validacion['ind_registro_de'],
                        $validacion['ind_preparar_oficio'],
                        $validacion['ind_conocer_opinion'],
                        $validacion['ind_tramitar_caso'],
                        $validacion['ind_acusar_recibo'],
                        $validacion['ind_tramitar_en'],
                        $validacion['ind_observacion'],
                        $validacion['ind_persona_destinataria'],
                        $validacion['ind_dependencia_destinataria'],
                        $validacion['ind_desc_dependencia'],
                        $validacion['ind_desc_representante'],
                        $validacion['ind_cargo_destinatario'],
				        $idDocumento);
                $validacion['status']='modificar';
            }

         
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
			
			//var_dump($this->atTipoDocumento->metMostrarTipoDocumento($idDocumento)); 
			//exit;
            
             
        }
         $lista=$this->atTipoDocumento->metListarOrganismoEnte();
        $this->atVista->assign('lista',$lista);
		
		  $exdependencia=$this->atTipoDocumento->metListarDependencia();
        $this->atVista->assign('exdependencia',$exdependencia);
		
		$centroCosto = $this->atTipoDocumento->metListarCentroCosto();
		 $this->atVista->assign('centroCosto',$centroCosto);
		
		
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

		   #para armar json centro de costos
    public function metJsonCentroCosto()
    {

        $idDependencia = $this->metObtenerInt('idDependencia');
        $centro_costo  = $this->atTipoDocumento->metJsonCentroCosto($idDependencia);
        echo json_encode($centro_costo);
        exit;

    }
	
    public function metEliminar()
    {
        $idDocumento = $this->metObtenerInt('idDocumento');
                    $id=$this->atTipoDocumento->metEliminarTipoDocumento($idDocumento);
           
                $valido=array(
                    'status'=>'ok',
                    'idDocumento'=>$id
                );

        echo json_encode($valido);
        exit;
    }

}

