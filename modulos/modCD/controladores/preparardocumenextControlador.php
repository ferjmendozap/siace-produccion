<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Preparar Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/


class preparardocumenextControlador extends Controlador
{
    private $atPrepDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atPrepDocumento=$this->metCargarModelo('preparardocumenext');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atPrepDocumento->metListarTipoDocumento(($usuario)));
        $this->atVista->metRenderizar('listado');
    }
	
	
    public function metModificar()
    {
	
	$js = array(
          	'materialSiace/core/demo/DemoFormEditors'
       );

        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'ckeditor/ckeditor',
			'ckeditor/adapters/jquery'
        );
        $complementosCss = array(
			'bootstrap-datepicker/datepicker',
			
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);


		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array('ind_estado','ind_media_firma');
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $formula = $this->metValidarFormArrayDatos('form', 'formula', $Excceccion);
			
       	   if ($alphaNum != null && $formula == null) {
                $validacion = $formula;
            } elseif ($alphaNum == null && $formula != null) {
                $validacion = $formula;
            } else {
               $validacion = array_merge($formula, $alphaNum);
            }
			

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
            $validacion['txt_contenido']=str_replace("font-size:8px", "font-size:+8", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:9px", "font-size:+9", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:10px", "font-size:+10", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:11px", "font-size:+11", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:12px", "font-size:+12", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:14px", "font-size:+14", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:16px", "font-size:+16", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:18px", "font-size:+18", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:20px", "font-size:+20", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:22px", "font-size:+22", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:24px", "font-size:+24", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:26px", "font-size:+26", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:28px", "font-size:+28", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:36px", "font-size:+36", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:48px", "font-size:+48", $validacion['txt_contenido']);
            $validacion['txt_contenido']=str_replace("font-size:72px", "font-size:+72", $validacion['txt_contenido']);
            if($idDocumento==0){
                $id=$this->atPrepDocumento->metCrearTipoDocumento(
				$validacion['txt_contenido'],$validacion['ind_media_firma'],$validacion['ind_estado']);
				
                $validacion['status']='nuevo';
            }else{
                $id=$this->atPrepDocumento->metModificarTipoDocumento($validacion['txt_contenido'],$validacion['ind_media_firma'],$validacion['ind_estado'],$idDocumento);
				$validacion['status']='modificar';
            }

         
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
				
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
				
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atPrepDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }
		

		$organismoint=$this->atPrepDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		
		
        $this->atVista->metRenderizar('Modificar','modales');
    }


}

