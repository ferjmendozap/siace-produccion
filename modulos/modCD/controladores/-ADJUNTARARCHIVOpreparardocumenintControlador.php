<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Preparar Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 

class preparardocumenintControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('preparardocumenint');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento());
        $this->atVista->metRenderizar('listado');
    }
	
	
	
    public function metCrearModificar()
    {
	$js = array(
          	'materialSiace/core/demo/DemoFormEditors'
       );

        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'ckeditor/ckeditor',
			'ckeditor/adapters/jquery'
        );
        $complementosCss = array(
			'bootstrap-datepicker/datepicker',
			
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
          //   $Excceccion=array('ind_estado');
		//	$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            //$ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
           $Excceccion=array('ind_estado','ind_ruta_archivo');
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $formula = $this->metValidarFormArrayDatos('form', 'formula', $Excceccion);
			
       	   if ($alphaNum != null && $formula == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $formula != null) {
                $validacion = $formula;
            } else {
               $validacion = array_merge($formula, $alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
			
			  if(!isset($validacion['ind_ruta_archivo'])){
                $validacion['ind_ruta_archivo']=NULL;
            }
			

	//	$cod_img= "documentos".mt_rand(0,999);
        $destino =  "publico/imagenes/modCD/documentos/";
     //   $band = 0;
        $ind_ruta_pdf = $_POST['ind_ruta_archivo'];

        if(!empty($_FILES["ind_ruta_pdf"]["name"]))
        {
            $pdf_f   = $_FILES["ind_ruta_pdf"]["name"];
            $ruta    = $destino.$pdf_f;

            if (!file_exists($ruta))//False si no existe
            {
                $resultado = move_uploaded_file($_FILES["ind_ruta_pdf"]["tmp_name"], $ruta);
                chmod($ruta, 0777);
                if ($resultado)
                {
                    $ind_ruta_pdf = $pdf_f;
                  //  $band = 1;
                }
              
     
	
                $id=$this->atTipoDocumento->metModificarTipoDocumento($validacion['txt_contenido'],$validacion['ind_media_firma'],$validacion['ind_ruta_archivo'],$validacion['ind_estado'],$idDocumento);
				$validacion['status']='modificar';
  
		   }    //  --------------
	}		
			
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
				
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
				
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
			//var_dump($this->atTipoDocumento->metMostrarTipoDocumento($idDocumento)); 
			//exit;
			
			/*
			//AGREGADO PARA MOSTRAR LOS PARTICULARES-------------------
			 $tipoDetalle = $this->atTipoDocumento->metMostrarTipoDetalle($idDocumento);
            for($i=0;$i<count($tipoDetalle);$i++){
			
			 $num_particular[$conceptoDetalle[$i]['ind_representante_externo']]=array(
                        'id'=>$conceptoDetalle[$i]['ind_representante_externo'],
                        'cod'=>$conceptoDetalle[$i]['ind_cedula_documento'],
                        'tipoParticular'=>$conceptoDetalle[$i]['ind_nombres']
                    );
					
            }
		  if(isset($num_particular)){
                $this->atVista->assign('num_particular', $num_particular);
            }
			*/
       }
		

		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		
		
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idDocumento = $this->metObtenerInt('idDocumento');
                    $id=$this->atTipoDocumento->metEliminarTipoDocumento($idDocumento);
           
                $valido=array(
                    'status'=>'ok',
                    'idDocumento'=>$id
                );

        echo json_encode($valido);
        exit;
    }
	/*
	 public function metJsonOrganismo()
    {
        $idOrganismo=$this->metObtenerInt('idOrganismo');
        $organismo = $this->atTipoDocumento->metJsonOrganismo($idOrganismo);
        echo json_encode($organismo);
        exit;
    }
	*/
}

