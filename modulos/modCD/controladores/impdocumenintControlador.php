<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class impdocumenintControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('impdocumenint');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento($usuario));
        $this->atVista->metRenderizar('listado');
    }

	
    public function metCrearModificar()
    {

		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		//$this->atVista->metCargarJs($js);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
		$idValor=$this->metObtenerInt('idValor');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array('ind_estado','ind_cargo_remitente','ind_plazo_atencion','ind_anexo','txt_descripcion_anexo','fk_a001_num_organismo','ind_con_copia','ind_cargo_destinatario',
			 'ind_persona_destinataria','ind_dependencia_destinataria');
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            //$ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
			
			if(!isset($validacion['ind_anexo'])){
                $validacion['ind_anexo']=0;
            }
				 if(!isset($validacion['ind_dependencia_destinataria'])){
                $validacion['ind_dependencia_destinataria']=false;
            }
			
			 if(!isset($validacion['ind_persona_destinataria'])){
                $validacion['ind_persona_destinataria']=false;
            }
			
            if(!isset($validacion['ind_cargo_destinatario'])){
                $validacion['ind_cargo_destinatario']=false;
            }

            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento(
				$validacion['ind_dependencia_remitente'],$validacion['ind_persona_remitente'],
				$validacion['ind_cargo_remitente'],$validacion['ind_asunto'],$validacion['ind_descripcion'],$validacion['ind_plazo_atencion'],
				$validacion['fec_documento'],$validacion['ind_anexo'],$validacion['txt_descripcion_anexo'],
				$validacion['ind_estado'],$validacion['fk_cdc003_num_tipo_documento'],$validacion['fk_a001_num_organismo'],
				
				
				$validacion['ind_dependencia_destinataria'],
				$validacion['ind_persona_destinataria'],$validacion['ind_cargo_destinatario'],$validacion['ind_con_copia']);
				
		
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoDocumento->metModificarTipoDocumento(
				$validacion['ind_dependencia_remitente'],$validacion['ind_persona_remitente'],
				$validacion['ind_cargo_remitente'],$validacion['ind_asunto'],$validacion['ind_descripcion'],$validacion['ind_plazo_atencion'],
				$validacion['fec_documento'],$validacion['ind_anexo'],$validacion['txt_descripcion_anexo'],
				$validacion['ind_estado'],$validacion['fk_cdc003_num_tipo_documento'],$validacion['fk_a001_num_organismo'],
				
				
				$validacion['ind_dependencia_destinataria'],
				$validacion['ind_persona_destinataria'],$validacion['ind_cargo_destinatario'],$validacion['ind_con_copia'],$idDocumento);
               $validacion['status']='modificar';
            }

         
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
				
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
				
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
       }
		

		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		//$this->atVista->assign('perfilConcepto', $depextern);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }


 // esta es la funcion que llama al modelo e imprimir el documento con copia

		public function metmemorandum()
    {

        $idDocumento=$this->metObtenerInt('idDocumento');

		
		  

        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarCopiasDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
	

	$conceptoDetalle = $this->atTipoDocumento->metMostrarTipoDocumentoDet($idDocumento);
            for ($i = 0; $i < count($conceptoDetalle); $i++) {
	
		if($conceptoDetalle[$i]['ind_dependencia_destinataria']!="0" && $conceptoDetalle[$i]['ind_con_copia']!="0"){
					
                $depextern[$conceptoDetalle[$i]['ind_dependencia_destinataria']] = array(
				 	'id' => $conceptoDetalle[$i]['ind_con_copia'],
                    'dependencia' => $conceptoDetalle[$i]['ind_desc_dependencia'],
					
                );
			
            if (isset($depextern)) {
                $this->atVista->assign('depextern', $depextern);
            }

            $this->atVista->assign('formDBDet', $conceptoDetalle);
        }
		}
//var_dump($conceptoDetalle); 
//exit;
	
       }
        $this->atVista->metRenderizar('memorandum','modales');
    }
	

		public function metdocumento()
    {

        $idDocumento=$this->metObtenerInt('idDocumento');

        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
	
		//var_dump($this->atTipoDocumento->metMostrarTipoDocumento($idDocumento)); 
		//exit;
		
       }
        $this->atVista->metRenderizar('documento','modales');
    }

	
	
}
