<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Registrar Organismos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class organismoControlador extends Controlador
{
    private $atTipoOrganismo;
	private $atPaisModelo;
    private $atEstadoModelo;
    private $atMunicipioModelo;
    private $atCiudadModelo;

    public function __construct()
    {
        parent::__construct();
		Session::metAcceso();
        $this->atTipoOrganismo=$this->metCargarModelo('organismo');
		$this->atPaisModelo   = $this->metCargarModelo('pais',false,'APP');
        $this->atEstadoModelo = $this->metCargarModelo('estado',false,'APP');
        $this->atMunicipioModelo = $this->metCargarModelo('municipio',false,'APP');
        $this->atCiudadModelo = $this->metCargarModelo('ciudad',false,'APP');
		$this->atIdUsuario    = Session::metObtener('idUsuario');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atTipoOrganismo->metListarTipoOrganismo());
		$this->atVista->metRenderizar('listado');
    }
	
	   public function metProveedor($proveedor)
    {
        $this->atVista->assign('lista', $this->atTipoOrganismo->metListaPersona());
        $this->atVista->assign('tipoProveedor', $proveedor);
        $this->atVista->metRenderizar('proveedor', 'modales');
    }
	
	   public function metOrganismo($organismo)
    {
        $this->atVista->assign('listado', $this->atTipoOrganismo->metListarOrganismoExt());
        $this->atVista->metRenderizar('listadoOrganismo', 'modales');
    }
	
	/*
	     $lista=$this->atTipoDependencia->metListarOrganismo();
        $this->atVista->assign('lista',$lista);
        $this->atVista->metRenderizar('CrearModificar','modales');
	*/
    public function metCrearModificar()
    {
		
        //$js = array('Aplicacion/appFunciones','modCD/modCDFunciones');
	  $js[] = 'modCD/modCDFunciones';
	
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
			'select2/select201ef'
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'select2/select2.min',
			 'inputmask/jquery.inputmask.bundle.min'
        );
		
		
		
		$this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		
        $valido=$this->metObtenerInt('valido');
        $idOrganismo=$this->metObtenerInt('idOrganismo');
        if($valido==1){
            $this->metValidarToken();
			 $url=$_POST["form"]["alphaNum"]["ind_pagina_web"];
  
			$Excceccion=array('ind_direccion','ind_pagina_web');
            $Excceccion2=array('ind_logo','ind_logo_secundario','ind_cargo_representante','ind_tomo_registro','ind_numero_registro','ind_sujeto_control','ind_pagina_web','ind_telefono','pk_num_organismo_telefono');
			//$Excceccion3=array('ind_direccion','ind_pagina_web');
	
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion2);
            $formula = $this->metValidarFormArrayDatos('form', 'formula', $Excceccion);
			
       	   if ($alphaNum != null && $formula == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $formula != null) {
                $validacion = $formula;
            } else {
               $validacion = array_merge($formula, $alphaNum);
            }
			
		          //Saltando la validacion de la url por caracteres especiales
            if($validacion['ind_pagina_web']){
                $validacion['ind_pagina_web']=(string)htmlspecialchars(preg_replace('/[^A-Za-z0-9������������.]/i', '', $url));
                $urlValida=$validacion['ind_pagina_web'];//para validar si es valida la url
            }else{
                $urlValida="";
            }
		  
		  $regex ="/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/";
                    if(!empty($urlValida)){
                        $urlWeb = preg_match($regex, $urlValida);
                    }else{
                        $urlWeb = 1;
                    }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
     		
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }
			
			  if(!isset($validacion['ind_sujeto_control'])){
                $validacion['ind_sujeto_control']=0;
            }
			 if(!isset($validacion['fk_a010_num_ciudad'])){
                    $validacion['fk_a010_num_ciudad']=false;
                }
				
			if(!isset($validacion['fk_a006_num_miscelaneo_detalle_carcater_social'])){
                $validacion['fk_a006_num_miscelaneo_detalle_carcater_social']=false;
            }
			
			if(!isset($validacion['ind_logo'])){
                $validacion['ind_logo']=false;
            }
			
			if(!isset($validacion['ind_telefono'])){
                $validacion['ind_telefono']=false;
            }
			
	if(!isset($validacion['pk_num_organismo_telefono'])){
                $validacion['pk_num_organismo_telefono']=false;
            }
			
			
            if($idOrganismo==0){
	
			 $destino =  "publico/imagenes/logos/";

        	$ind_ruta_img = $_POST['ind_logo'];
		
        if(!empty($_FILES["ind_ruta_img"]["name"]))
        {
            $pdf_f   = $_FILES["ind_ruta_img"]["name"];
            $ruta    = $destino.$pdf_f;

            if (!file_exists($ruta))//False si no existe
            {
                $resultado = move_uploaded_file($_FILES["ind_ruta_img"]["tmp_name"], $ruta);
                chmod($ruta, 0777);
                if ($resultado)
                {
				
                $ind_ruta_img = $pdf_f;
         }
						   }    //  --------------
	} 
	
			//$destino2 =  "publico/imagenes/modCD/";
        	$ind_ruta = $_POST['ind_logo_secundario'];
		
        if(!empty($_FILES["ind_ruta"]["name"]))
        {
            $img_f   = $_FILES["ind_ruta"]["name"];
            $ruta2    = $destino.$img_f;

            if (!file_exists($ruta2))//False si no existe
            {
                $resultado2 = move_uploaded_file($_FILES["ind_ruta"]["tmp_name"], $ruta2);
                chmod($ruta2, 0777);
                if ($resultado2)
                {
				
                $ind_ruta = $img_f;
         }
						   }    //  --------------
	} 
		              
                $id=$this->atTipoOrganismo->metCrearTipoOrganismo($validacion['ind_descripcion_empresa'],$validacion['ind_documento_fiscal'],$validacion['ind_numero_registro'],$validacion['ind_tomo_registro'],
				$ind_ruta_img,$ind_ruta,$validacion['num_estatus'],$validacion['ind_tipo_organismo'],$validacion['ind_direccion'],
				$validacion['fk_a006_num_miscelaneo_detalle_carcater_social'],$validacion['ind_cargo_representante'],$validacion['ind_pagina_web'],
				$validacion['fk_a003_num_persona_representante'],$validacion['fk_a010_num_ciudad'],$validacion['ind_telefono']);
                $validacion['status']='nuevo';
				
            }else{
	
		//$cod_img= $idOrganismo;
			 $destino =  "publico/imagenes/logos/";
     //   $band = 0;
        $ind_ruta_img = $_POST['ind_logo'];
		
        if(!empty($_FILES["ind_ruta_img"]["name"]))
        {
            $pdf_f   = $_FILES["ind_ruta_img"]["name"];
            $ruta    = $destino.$pdf_f;

            if (!file_exists($ruta))//False si no existe
            {
                $resultado = move_uploaded_file($_FILES["ind_ruta_img"]["tmp_name"], $ruta);
                chmod($ruta, 0777);
                if ($resultado)
                {
				
                $ind_ruta_img = $pdf_f;
         }
						   }    //  --------------
	} 
		  
		  	$ind_ruta = $_POST['ind_logo_secundario'];
		
        if(!empty($_FILES["ind_ruta"]["name"]))
        {
            $img_f   = $_FILES["ind_ruta"]["name"];
            $ruta2    = $destino.$img_f;

            if (!file_exists($ruta2))//False si no existe
            {
                $resultado2 = move_uploaded_file($_FILES["ind_ruta"]["tmp_name"], $ruta2);
                chmod($ruta2, 0777);
                if ($resultado2)
                {
				
                $ind_ruta = $img_f;
         }
						   }    //  --------------
	} 
		              
                $id=$this->atTipoOrganismo->metModificarTipoOrganismo($validacion['ind_descripcion_empresa'],$validacion['ind_documento_fiscal'],$validacion['ind_numero_registro'],$validacion['ind_tomo_registro'],
				$ind_ruta_img,$ind_ruta,$validacion['num_estatus'],$validacion['ind_tipo_organismo'],$validacion['ind_direccion'],
				$validacion['fk_a006_num_miscelaneo_detalle_carcater_social'],$validacion['ind_cargo_representante'],$validacion['ind_pagina_web'],
				$validacion['fk_a003_num_persona_representante'],
				$validacion['fk_a010_num_ciudad'],$validacion['ind_telefono'],$validacion['pk_num_organismo_telefono'], $idOrganismo);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idOrganismo']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idOrganismo!=0){
		
		    $documento = $this->atTipoOrganismo->metMostrarTipoOrganismo($idOrganismo);
            $telefono = $this->atTipoOrganismo->metMostrarSelect($idOrganismo);
            $this->atVista->assign('numero', 1);
            $this->atVista->assign('n', 0);
            $this->atVista->assign('formDB', $documento);
          	$this->atVista->assign('telefono', $telefono);
			
            //$this->atVista->assign('formDB',$this->atTipoOrganismo->metMostrarTipoOrganismo($idOrganismo));
            //$this->atVista->assign('idOrganismo',$idOrganismo);
			
		//var_dump($this->atTipoOrganismo->metMostrarTipoOrganismo($idOrganismo)); 
			//exit;
		        }
				
		$PAIS = Session::metObtener('DEFAULTPAIS');
        $ESTADO = Session::metObtener('DEFAULTESTADO');
        $MUNICIPIO = Session::metObtener('DEFAULTMUNICIPIO');
        $CIUDAD = Session::metObtener('DEFAULTCIUDAD');
		$this->atVista->assign('DefaultPais',$PAIS);
        $this->atVista->assign('DefaultEstado',$ESTADO);
        $this->atVista->assign('DefaultMunicipio',$MUNICIPIO);
        $this->atVista->assign('DefaultCiudad',$CIUDAD);

	    /*PARA LISTAR SELECT PAISES, ESTADO, MUNICIPIO, CIUDAD*/
        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));
        $this->atVista->assign('listadoEstado',$this->atEstadoModelo->metJsonEstado($PAIS));
        $this->atVista->assign('listadoMunicipio',$this->atMunicipioModelo->metJsonMunicipio($ESTADO));
        $this->atVista->assign('listadoCiudad',$this->atCiudadModelo->metJsonCiudad($ESTADO));	
	//	$ciudad=$this->atTipoOrganismo->metListaCiudad();
     //   $this->atVista->assign('ciudad',$ciudad);
		
		$this->atVista->assign('idOrganismo',$idOrganismo);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idOrganismo = $this->metObtenerInt('idOrganismo');
                    $id=$this->atTipoOrganismo->metEliminarTipoOrganismo($idOrganismo);
           
                $valido=array(
                    'status'=>'ok',
                    'idOrganismo'=>$id
                );

        echo json_encode($valido);
        exit;
    }
}