<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION  
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class documentinternosControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('documentinternos');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento($usuario));
        $this->atVista->metRenderizar('listado');
    }
	
 public function metEncargaduria($Encargaduria)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaEncargEspecial());
        $this->atVista->assign('tipoEncargaduria', $Encargaduria);
        $this->atVista->metRenderizar('Encargaduria', 'modales');
    }
	

public function metProveedor($proveedor)
    {
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaRepresentante($usuario));
        $this->atVista->assign('tipoProveedor', $proveedor);
        $this->atVista->metRenderizar('proveedor', 'modales');
    }
	
		 public function metPersona($persona)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaPersona());
        $this->atVista->assign('tipoPersona', $persona);
        $this->atVista->metRenderizar('persona', 'modales');
    }
	
	  public function rellenarCeros($nro,$cantidad){
	  
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

		
    public function metCrearModificar()
    {
        $usuario = Session::metObtener('idUsuario');
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		//$this->atVista->metCargarJs($js);
		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array('ind_descripcion',
                 'ind_asunto',
                 'ind_estado',
                 'ind_descripcion',
                 'ind_cargo_remitente',
                 'fec_documento',
                 'ind_encargaduria_especial',
                 'num_cod_interno',
                 'ind_plazo_atencion',
                 'ind_anexo',
                 'ind_ruta_doc',
                 'txt_descripcion_anexo',
                 'fk_a001_num_organismo',
                 'ind_con_copia',
                 'ind_cargo_destinatario',
			     'ind_persona_destinataria',
                 'ind_dependencia_destinataria');
			   $Excceccion2=array('ind_documento_completo');

			 $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind = $this->metValidarFormArrayDatos('form', 'int',$Excceccion);
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$Excceccion2);
			
          if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']='Preparacion';
            }
			if(!isset($validacion['ind_anexo'])){
                $validacion['ind_anexo']=0;
            }
			if(!isset($validacion['ind_ruta_doc'])){
                $validacion['ind_ruta_doc']=NULL;
            }
				if(!isset($validacion['num_secuencia'])){
                $validacion['num_secuencia']=0;
            }
			if(!isset($validacion['ind_con_copia'])){
                $validacion['ind_con_copia']=0;
            }
				if(!isset($validacion['num_cod_interno'])){
                $validacion['num_cod_interno']=NULL;
            }
				if(!isset($validacion['ind_flag_encargaduria'])){
                $validacion['ind_flag_encargaduria']=0;
            }
			if(!isset($validacion['txt_descripcion_anexo'])){
                $validacion['txt_descripcion_anexo']=NULL;
            }
			if(!isset($validacion['ind_plazo_atencion'])){
                $validacion['ind_plazo_atencion']=0;
            }
				 if(!isset($validacion['ind_dependencia_destinataria'])){
                $validacion['ind_dependencia_destinataria']=false;
            }
			 if(!isset($validacion['ind_persona_destinataria'])){
                $validacion['ind_persona_destinataria']=false;
            }
            if(!isset($validacion['ind_cargo_destinatario'])){
                $validacion['ind_cargo_destinatario']=false;
            }
			   if(!isset($validacion['ind_desc_dependencia'])){
                $validacion['ind_desc_dependencia']=false;
            }
			 if(!isset($validacion['ind_desc_representante'])){
                $validacion['ind_desc_representante']=false;
            }
			// busco el tipo de documento en la tabla y m traigo el pk
			$documento = $this->atTipoDocumento->metBuscarcorrespondencia($validacion['fk_cdc003_num_tipo_documento']);
			$codigocorresp=$documento['pk_num_tipo_documento'];
			
			// busco en la tabla dependencias  y m traigo el pk y el codigo interno
            $dependencia = $this->atTipoDocumento->metBuscarDependencia($validacion['ind_dependencia_remitente']);
            $codigoInt=$dependencia['ind_codinterno'];
			$codigodep=$dependencia['pk_num_dependencia'];
			
			//  hago la consulta a la tabla
			$orden=$this->atTipoDocumento->metContar('cd_b001_documento_interno',date('Y'),'fec_annio',$codigodep,'ind_dependencia_remitente',$codigocorresp,'fk_cdc003_num_tipo_documento');
			$orden['cantidad'] = $orden['cantidad'] + 1;	 
			$secuencia=$this->rellenarCeros($orden['cantidad'],4);
			// contruyo el codigo completo del documento
			$codigoReq=$dependencia['ind_codinterno']."-".$this->rellenarCeros($orden['cantidad'],4)."-".date('Y');
            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento(
				$secuencia,
                $codigoInt,
                $codigoReq,
                $validacion['ind_dependencia_remitente'],
                $validacion['ind_persona_remitente'],
				$validacion['ind_cargo_remitente'],
                $validacion['fec_documento'],
                $validacion['ind_flag_encargaduria'],
                $validacion['ind_encargaduria_especial'],
                $validacion['ind_asunto'],
                $validacion['ind_descripcion'],
                $validacion['ind_plazo_atencion'],
				$validacion['ind_anexo'],
                $validacion['txt_descripcion_anexo'],
				$validacion['ind_estado'],
                $validacion['fk_cdc003_num_tipo_documento'],
                $validacion['fk_a001_num_organismo'],
				$validacion['ind_dependencia_destinataria'],
				$validacion['ind_persona_destinataria'],
                $validacion['ind_cargo_destinatario'],
                $validacion['ind_desc_dependencia'],
                $validacion['ind_desc_representante'],
                $validacion['ind_con_copia']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoDocumento->metModificarTipoDocumento(
				$validacion['ind_dependencia_remitente'],
                $codigoReq,
                $validacion['ind_persona_remitente'],
				$validacion['ind_cargo_remitente'],
                $validacion['fec_documento'],
                $validacion['ind_flag_encargaduria'],
                $validacion['ind_encargaduria_especial'],
                $validacion['ind_asunto'],
                $validacion['ind_descripcion'],
                $validacion['ind_plazo_atencion'],
				$validacion['ind_anexo'],
                $validacion['txt_descripcion_anexo'],
				$validacion['ind_estado'],
                $validacion['fk_cdc003_num_tipo_documento'],
                $validacion['fk_a001_num_organismo'],
				$validacion['ind_dependencia_destinataria'],
				$validacion['ind_persona_destinataria'],
                $validacion['ind_cargo_destinatario'],
                $validacion['ind_desc_dependencia'],
                $validacion['ind_desc_representante'],
                $validacion['ind_con_copia'],$idDocumento);
                $validacion['status']='modificar';
            }
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if($validacion[$titulo])
                    {
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idDocumento']=$id;
            echo json_encode($validacion);
            exit;
        }
        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
	 	   $conceptoDetalle = $this->atTipoDocumento->metMostrarTipoDocumentoDet($idDocumento);
            for ($i = 0; $i < count($conceptoDetalle); $i++) {
					if($conceptoDetalle[$i]['ind_dependencia_destinataria']!="0"){
                $depextern[$conceptoDetalle[$i]['ind_dependencia_destinataria']] = array(
				 	'id' => $conceptoDetalle[$i]['ind_dependencia_destinataria'],
                    'dependencia' => $conceptoDetalle[$i]['ind_desc_dependencia'],
                    'representantedep' => $conceptoDetalle[$i]['ind_desc_representante'],
                    'cargodep' => $conceptoDetalle[$i]['ind_cargo_destinatario'],
					'copiadep' => $conceptoDetalle[$i]['ind_con_copia']
                );
            if (isset($depextern)) {
                $this->atVista->assign('depextern', $depextern);
            }
            $this->atVista->assign('formDBDet', $conceptoDetalle);
        }
		}
		
		 $empleadoDetalle = $this->atTipoDocumento->metMostrarTipoempleadoDet($idDocumento);
            for ($i = 0; $i < count($empleadoDetalle); $i++) {
				
				if($empleadoDetalle[$i]['ind_persona_destinataria']!="0"){
	
				 $num_particular[$empleadoDetalle[$i]['ind_persona_destinataria']] = array(
				 	'id' => $empleadoDetalle[$i]['ind_persona_destinataria'],
                    'dependenciaEmp' => $empleadoDetalle[$i]['ind_desc_dependencia'],
                    'particular' => $empleadoDetalle[$i]['ind_desc_representante'],
                    'cargo' => $empleadoDetalle[$i]['ind_cargo_destinatario'],
                );
		
			 if (isset($num_particular)) {
                $this->atVista->assign('num_particular', $num_particular);
            }
            $this->atVista->assign('formDBDit', $empleadoDetalle);
        }
		}
	
	
		
       }
		

		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		//$this->atVista->assign('perfilConcepto', $depextern);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }
	
	
	public function metVer()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');

       if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
			
			$conceptoDetalle = $this->atTipoDocumento->metMostrarTipoDocumentoDet($idDocumento);
            for ($i = 0; $i < count($conceptoDetalle); $i++) {
				
					if($conceptoDetalle[$i]['ind_dependencia_destinataria']!="0"){
					
                $depextern[$conceptoDetalle[$i]['ind_dependencia_destinataria']] = array(
				 	'id' => $conceptoDetalle[$i]['ind_dependencia_destinataria'],
                    'dependencia' => $conceptoDetalle[$i]['ind_desc_dependencia'],
                    'representantedep' => $conceptoDetalle[$i]['ind_desc_representante'],
                    'cargodep' => $conceptoDetalle[$i]['ind_cargo_destinatario'],
					'copiadep' => $conceptoDetalle[$i]['ind_con_copia']
                );
			
            if (isset($depextern)) {
                $this->atVista->assign('depextern', $depextern);
            }

            $this->atVista->assign('formDBDet', $conceptoDetalle);
        }
		}
		
		 $empleadoDetalle = $this->atTipoDocumento->metMostrarTipoempleadoDet($idDocumento);
            for ($i = 0; $i < count($empleadoDetalle); $i++) {
				
				if($empleadoDetalle[$i]['ind_persona_destinataria']!="0"){
	
				 $num_particular[$empleadoDetalle[$i]['ind_persona_destinataria']] = array(
				 	'id' => $empleadoDetalle[$i]['ind_persona_destinataria'],
                    'dependenciaEmp' => $empleadoDetalle[$i]['ind_desc_dependencia'],
                    'particular' => $empleadoDetalle[$i]['ind_desc_representante'],
                    'cargo' => $empleadoDetalle[$i]['ind_cargo_destinatario'],
                );
		
			 if (isset($num_particular)) {
                $this->atVista->assign('num_particular', $num_particular);
            }
            $this->atVista->assign('formDBDit', $empleadoDetalle);
        }
		}

       }
		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		
		 $this->atVista->assign('ver', 1);
        $this->atVista->metRenderizar('anular','modales');
    }
	
	public function metVerAnular()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');

       if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
			$conceptoDetalle = $this->atTipoDocumento->metMostrarTipoDocumentoDet($idDocumento);
            for ($i = 0; $i < count($conceptoDetalle); $i++) {
				
					if($conceptoDetalle[$i]['ind_dependencia_destinataria']!="0"){
					
                $depextern[$conceptoDetalle[$i]['ind_dependencia_destinataria']] = array(
				 	'id' => $conceptoDetalle[$i]['ind_dependencia_destinataria'],
                    'dependencia' => $conceptoDetalle[$i]['ind_desc_dependencia'],
                    'representantedep' => $conceptoDetalle[$i]['ind_desc_representante'],
                    'cargodep' => $conceptoDetalle[$i]['ind_cargo_destinatario'],
					'copiadep' => $conceptoDetalle[$i]['ind_con_copia']
                );
			
            if (isset($depextern)) {
                $this->atVista->assign('depextern', $depextern);
            }

            $this->atVista->assign('formDBDet', $conceptoDetalle);
        }
		}
		
		 $empleadoDetalle = $this->atTipoDocumento->metMostrarTipoempleadoDet($idDocumento);
            for ($i = 0; $i < count($empleadoDetalle); $i++) {
				
				if($empleadoDetalle[$i]['ind_persona_destinataria']!="0"){
	
				 $num_particular[$empleadoDetalle[$i]['ind_persona_destinataria']] = array(
					'id' => $empleadoDetalle[$i]['ind_persona_destinataria'],
                    'dependenciaEmp' => $empleadoDetalle[$i]['ind_desc_dependencia'],
                    'particular' => $empleadoDetalle[$i]['ind_desc_representante'],
                    'cargo' => $empleadoDetalle[$i]['ind_cargo_destinatario'],
                );
		
			 if (isset($num_particular)) {
                $this->atVista->assign('num_particular', $num_particular);
            }
            $this->atVista->assign('formDBDit', $empleadoDetalle);
        }
		}

       }

	
		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		
		 $this->atVista->assign('veranular', 1);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }
	
		public function metanular()
    {
	
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
        $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
			
          if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
			
			 if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']='Anulado';
            }
			
           
            if  ($idDocumento!=0)
			{
                $id=$this->atTipoDocumento->metanularTipoDocumento(
				$validacion['ind_motivo_anulado'],$validacion['ind_persona_anulado'],$validacion['ind_estado'],$idDocumento);
				
                $validacion['status']='anular';
            }
       
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
				
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
				
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }

	
		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		
        $this->atVista->metRenderizar('anular','modales');
    }
	
		public function metmemorandum()
    {

        $idDocumento=$this->metObtenerInt('idDocumento');

        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
			
       }
		
        $this->atVista->metRenderizar('memorandum','modales');
    }
	
	
	public function metcredencial()
    {

        $idDocumento=$this->metObtenerInt('idDocumento');

        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }
		
        $this->atVista->metRenderizar('credencial','modales');
    }
	
	public function metpuntocuenta()
    {

        $idDocumento=$this->metObtenerInt('idDocumento');

        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }
		
        $this->atVista->metRenderizar('puntocuenta','modales');
    }


}
