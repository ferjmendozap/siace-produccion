<?php

/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Guidmar Espinoza     | g.espinoza@contraloriamonagas.gob.ve    | 0414-1913443      | MONAGAS
 * | 2 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
class proveedorControlador extends Controlador
{
    private $atProveedorModelo;
    private $atMiscelaneoModelo;
    private $atPaisModelo;
    private $atEstadoModelo;
    private $atDocumentoModelo;
    private $atTipoServicioModelo;
    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atProveedorModelo = $this->metCargarModelo('proveedor');
    }

    public function metIndex($listas=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
		 if(!$listas){
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		}
        $this->atVista->assign('listado', $this->atProveedorModelo->metListarProveedor());
		  if($listas){
		$this->atVista->metRenderizar('listadoProveedor','modales');
			}
		else{  
        $this->atVista->metRenderizar('listado');
    }
 }
    public function metPersona($persona)
    {
        $this->atVista->assign('lista', $this->atProveedorModelo->metListaPersona());
        $this->atVista->assign('persona', $persona);
        $this->atVista->metRenderizar('personas', 'modales');
    }

    public function metServiciosDocumentos($servDoc)
    {
        if($servDoc=='servicios') {
            $this->atVista->assign('lista', $this->atProveedorModelo->atTipoServicioModelo->metServiciosListar());
            $tr = $this->metObtenerInt('tr2');
            $this->atVista->assign('servicio', $servDoc);
        } else if($servDoc=='documentos'){
            $this->atVista->assign('lista', $this->atProveedorModelo->atDocumentoModelo->metDocumentoListar());
            $tr = $this->metObtenerInt('tr1');
            $this->atVista->assign('doc', $servDoc);
        }
        $this->atVista->assign('tr', $tr);
        $this->atVista->metRenderizar($servDoc, 'modales');
    }

    public function metCrearModificarProveedor()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
            'mask/jquery.mask',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js = array(
            'Aplicacion/appFunciones',
            'modRH/modRHFunciones',
            'materialSiace/core/demo/DemoFormWizard');
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $idProveedor = $this->metObtenerInt('idProveedor');
        $idPersona = $this->metObtenerInt('idPersona');
        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        if ($valido == 1) {
            $this->metValidarToken();
            $int = $this->metObtenerInt('form','int');
            $txt = $this->metObtenerTexto('form','txt');
            if (!isset($int['ind_snc'])) {
                $int['ind_snc'] = "0";
            }

            if ($txt['tipopersona'] == 'N' and $int['ind_snc'] == 0) { //indica que es persona natural
                $Excceccion = array('nombre2', 'apellido2', 'dirS', 'ind_snc', 'nroSNC', 'fSNC', 'fValSNC',
                     'calificacion', 'nivel', 'capacidad', 'registro', 'licencia', 'num_estatus','ind_telefono','telEmer','dCant','dId','sCant','sId','fk_a009_num_estado','dirF','idDir');
            } else if ($txt['tipopersona'] == 'N' and $int['ind_snc'] == 1) { //indica que es persona natural
                $Excceccion = array('nombre2', 'apellido2', 'dirS', 'registro', 'licencia',
                    'num_estatus','ind_telefono','telEmer','dCant','dId','sCant','sId','fk_a009_num_estado','dirF','idDir');
            } else if (($txt['tipopersona'] == 'J' or $txt['tipopersona'] == 'F') and $int['ind_snc'] == 0) { //indica que es persona juridica o firma personal
                $Excceccion = array('nombre2', 'apellido1', 'apellido2',  'nacionalidad',
                    'ind_estado_civil', 'dirS', 'ind_snc', 'nroSNC', 'fSNC', 'fValSNC', 'calificacion',
                    'nivel', 'capacidad', 'registro', 'licencia', 'num_estatus','ind_telefono','telEmer','dCant','dId','sCant','sId','fk_a009_num_estado','dirF','idDir');
            } else if (($txt['tipopersona'] == 'J' or $txt['tipopersona'] == 'F') and $int['ind_snc'] == 1) { //indica que es persona juridica o firma personal
                $Excceccion = array('nombre2','apellido1','apellido2','nacionalidad','ind_estado_civil','registro', 'licencia', 'dirS','num_estatus',
                    'ind_telefono','telEmer','dCant','dId','sCant','sId','fk_a009_num_estado','dirF','idDir');
            } else {
                $Excceccion = array('nombre2', 'apellido1', 'apellido2', 'dirS', 'ind_snc', 'registro', 'licencia',
                    'num_estatus','ind_telefono','telEmer','dCant','dId','sCant','sId','fk_a009_num_estado','dirF','idDir');
            }
            $formTxt = $this->metValidarFormArrayDatos('form', 'txt', $Excceccion);
            $formInt = $this->metValidarFormArrayDatos('form', 'int', $Excceccion);
            $formAlphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum', $Excceccion);
            if ($formTxt != null && $formInt == null && $formAlphaNum == null) {
                $validacion = $formTxt;
            } elseif ($formTxt == null && $formInt != null && $formAlphaNum == null) {
                $validacion = $formInt;
            } elseif ($formTxt == null && $formInt == null && $formAlphaNum != null) {
                $validacion = $formAlphaNum;
            } elseif ($formTxt == null && $formInt != null && $formAlphaNum != null) {
                $validacion = array_merge($formAlphaNum, $formInt);
            } elseif ($formTxt != null && $formInt != null && $formAlphaNum == null) {
                $validacion = array_merge($formTxt, $formInt);
            } elseif ($formTxt != null && $formInt == null && $formAlphaNum != null) {
                $validacion = array_merge($formTxt, $formAlphaNum);
            } else {
                $validacion = array_merge($formAlphaNum, $formInt, $formTxt);
            }
            if ($validacion['tipopersona']=='J' or $validacion['tipopersona']=='F') {
                $validacion['ind_estado_civil'] = null;
            }
            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = "0";
            }
            if (!isset($validacion['nombre2'])) {
                $validacion['nombre2'] = null;
            }
            if (!isset($validacion['apellido1'])) {
                $validacion['apellido1'] = null;
            }
            if (!isset($validacion['apellido2'])) {
                $validacion['apellido2'] = null;
            }
            if (!isset($validacion['ind_snc'])) {
                $validacion['ind_snc'] = "0";
                $validacion['nroSNC'] = null;
                $validacion['fSNC'] = null;
                $validacion['fValSNC'] = null;
                $validacion['calificacion'] = "0";
                $validacion['condicion'] = null;
                $validacion['nivel'] = "0";
                $validacion['capacidad'] = null;
            }
            if (isset($validacion['dCant'])) {
                $contador=1;
                while ($contador<=$validacion['dCant']) {
                    $nom1="dId".$contador;
                    $validacion['dId'][$contador] = $validacion[$nom1];
                    $contador=$contador+1;
                }
            } else {
                $validacion['dCant'] = "0";
                $validacion['dId'] = "0";
            }

            if (isset($validacion['sCant'])) {
                $contador=1;
                while ($contador<=$validacion['sCant']) {
                    $nom1="sId".$contador;
                    $validacion['sId'][$contador] = $validacion[$nom1];
                    $contador=$contador+1;
                }
            } else {
                $validacion['sCant'] = "0";
                $validacion['sId'] = "0";
            }

            if (isset($validacion['cant'])) {
                $contador=1;
                while ($contador<=$validacion['cant']) {
                    if(!isset($validacion['pk_num_telefono'][$contador])) $validacion['pk_num_telefono'][$contador]="0";
                    if(!isset($validacion['telEmer'][$contador])) $validacion['telEmer'][$contador]="0";
                    $contador=$contador+1;
                }
            } else {
                $validacion['ind_telefono'] = "0";
                $validacion['telEmer'] = "0";
                $validacion['cant'] = "0";
                $validacion['pk_num_telefono'] = "0";
            }

            $val = $this->metValidarCorreo($validacion['dirE']);

            if($val==0){
                $validacion['dirE'] = 'error';
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }
            if ($idProveedor == 0) {
                $id = $this->atProveedorModelo->metCrearProveedor(
                    $validacion['docfiscal'],$validacion['nombre1'],$validacion['nombre2'],$validacion['apellido1'],
                    $validacion['apellido2'],$validacion['sexo'],$validacion['nacionalidad'],$validacion['ind_estado_civil'],
                    $validacion['dirE'],$validacion['tipopersona'],
                    $validacion['num_estatus'],$validacion['fk_a010_num_ciudad'],$validacion['ind_telefono'],$validacion['telEmer'],
                    $validacion['cant'],$validacion['dCant'],$validacion['sCant'],$validacion['tipoPago'],
                    $validacion['formaPago'],$validacion['dId'],$validacion['sId'],$validacion['diasPago'],
                    $validacion['registro'],$validacion['licencia'],$validacion['fecConst'],$validacion['ind_snc'],
                    $validacion['nroSNC'],$validacion['fSNC'],$validacion['fValSNC'],$validacion['condicion'],
                    $validacion['calificacion'],$validacion['nivel'],$validacion['capacidad'],$validacion['representante'],
                    $validacion['contacto'],$validacion['dirF']);
                $validacion['status'] = 'nuevo';
            } else {
                $id = $this->atProveedorModelo->metModificarProveedor(
                    $validacion['docfiscal'],$validacion['nombre1'],$validacion['nombre2'],$validacion['apellido1'],
                    $validacion['apellido2'],$validacion['sexo'],$validacion['nacionalidad'],$validacion['ind_estado_civil'],
                    $validacion['dirE'],$validacion['tipopersona'],
                    $validacion['num_estatus'],$validacion['fk_a010_num_ciudad'],$validacion['ind_telefono'],$validacion['telEmer'],
                    $validacion['cant'],$validacion['pk_num_telefono'],$validacion['dCant'],$validacion['sCant'],
                    $validacion['tipoPago'],$validacion['formaPago'],$validacion['dId'],$validacion['sId'],
                    $validacion['diasPago'],$validacion['registro'],$validacion['licencia'],$validacion['fecConst'],
                    $validacion['ind_snc'],$validacion['nroSNC'],$validacion['fSNC'],$validacion['fValSNC'],
                    $validacion['condicion'],$validacion['calificacion'],$validacion['nivel'],$validacion['capacidad'],
                    $validacion['representante'],$validacion['contacto'],$idProveedor,$idPersona,$validacion['idDir'],$validacion['dirF']
                );
                $validacion['status'] = 'modificar';
            }

            $validacion['id'] = $id;
            if($id==1){
                $validacion['id'] = 'error';
                $validacion['status'] = 'errorPersona';
                echo json_encode($validacion);
                exit;
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            if($id!=0){
                $estado = $validacion['status'];
                $validacion = $this->atProveedorModelo->metBuscarProveedor($id);
                $validacion['status']=$estado;
            }
            $validacion['idProveedor'] = $id;
            echo json_encode($validacion);
            exit;
        }
        if ($idProveedor != 0) {
            $this->atVista->assign('formDB',$this->atProveedorModelo->metBuscarProveedor($idProveedor));
            $this->atVista->assign('telefono',$this->atProveedorModelo->metListarTelefonos($idPersona));
            $this->atVista->assign('ver',$ver);
            $this->atVista->assign('idProveedor', $idProveedor);
        }
        $this->atVista->assign('selectEstadoCivil', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('EDOCIVIL'));
        $this->atVista->assign('selectCondSNC', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('IRCN'));
        $this->atVista->assign('selectTipoPers', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('TipoPers'));
        $this->atVista->assign('selecttipoPago', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
        $this->atVista->assign('selectformaPago', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('FDPLG'));
        $this->atVista->assign('selectTipoServicio', $this->atProveedorModelo->atTipoServicioModelo->metServiciosListar());
        $this->atVista->assign('tipoServicio', $this->atProveedorModelo->metServiciosProveedor($idProveedor));
        $this->atVista->assign('documento', $this->atProveedorModelo->metDocumentoProveedor($idProveedor));
        $this->atVista->assign('selectTipoDoc', $this->atProveedorModelo->atDocumentoModelo->metDocumentoListar());
        $this->atVista->assign('listadoPais', $this->atProveedorModelo->atPaisModelo->metListarPais());
        $this->atVista->assign('listadoEstado', $this->atProveedorModelo->atEstadoModelo->metListarEstado());
        //$this->atVista->assign('listadoCiudad', $this->atProveedorModelo->atCiudadModelo->metListarCiudad());
        $this->atVista->metRenderizar('CrearModificar', 'modales');
    }

    public function metEliminarProveedor()
    {
        $idProveedor = $this->metObtenerInt('idProveedor');
        $idPersona = $this->metObtenerInt('idPersona');
        if($idProveedor!=0){
            $id=$this->atProveedorModelo->metEliminarProveedor($idProveedor,$idPersona);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Proveedor se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idProveedor
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metEliminarServicio()
    {
        $id1 = $this->metObtenerInt('id1');
        $id2 = $this->metObtenerInt('id2');
        if($id1!=0 and $id2!=0){
            $id=$this->atProveedorModelo->metEliminarServicio($id1,$id2);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Servicio se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metEliminarDocumento()
    {
        $id1 = $this->metObtenerInt('id1');
        $id2 = $this->metObtenerInt('id2');
        if($id1!=0 and $id2!=0){
            $id=$this->atProveedorModelo->metEliminarDocumento($id1,$id2);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Documento se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metEliminarTelefono()
    {
        $idTelf = $this->metObtenerInt('id');
        if($idTelf!=0){
            $id=$this->atProveedorModelo->metEliminarTelefono($idTelf);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Teléfono se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              proveedor.*,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor,
              persona.ind_documento_fiscal,
              persona.num_estatus
          FROM
            lg_b022_proveedor AS proveedor
          INNER JOIN a003_persona AS persona ON persona.pk_num_persona =  proveedor.fk_a003_num_persona_proveedor ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        proveedor.pk_num_proveedor LIKE '%$busqueda[value]%' OR 
                        persona.ind_documento_fiscal LIKE '%$busqueda[value]%' OR 
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR 
                        persona.ind_nombre2 LIKE '%$busqueda[value]%' OR 
                        persona.ind_apellido1 LIKE '%$busqueda[value]%' OR 
                        persona.ind_apellido2 LIKE '%$busqueda[value]%' 
                        )
                        ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_proveedor','nomProveedor','ind_documento_fiscal','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_proveedor';
        #construyo el listado de botones
        $camposExtra = array('fk_a003_num_persona_proveedor');

        if (in_array('LG-01-07-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario a Modificado el Proveedor Nro. '.$clavePrimaria.'"
                        idPost="'.$clavePrimaria.'" titulo="Modificar Proveedor" idPersona="fk_a003_num_persona_proveedor"
                        data-toggle="modal" data-target="#formModal">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario a Visto el Proveedor Nro. '.$clavePrimaria.'"
                        idPost="'.$clavePrimaria.'" titulo="Visualizar Proveedor" idPersona="fk_a003_num_persona_proveedor"
                        data-toggle="modal" data-target="#formModal">
                    <i class="fa fa-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Proveedor Nro. '.$clavePrimaria.'"
                        idProveedor="'.$clavePrimaria.'" idPersona="fk_a003_num_persona_proveedor" titulo="Eliminar Proveedor"
                        mensaje="Estas seguro que desea eliminar el Proveedor!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }

    public function metValidarCorreo($correo)
    {
        $cont = strlen($correo);
        $arroba = '';
        $punto = '';
        for ($i=0; $i<$cont; $i++){
            if(substr($correo,$i,1)=='@'){
                $arroba = 'ok';
            }
            if(substr($correo,$i,4)=='.com'){
                $punto = 'ok';
            }
            if(substr($correo,$i,5)=='@.com'){
                $arroba = '';
                $punto = '';
            }
        }
        if ($punto == 'ok' AND $arroba=='ok'){
            return 1;
        } else {
            return 0;
        }
    }

    public function metObtenetInfoRifSeniat() {
        $rif = $this->metObtenerAlphaNumerico('docfiscal');
        $rif = str_replace('-', '', strtoupper($rif));
        $url = 'http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=';
        $validacion['nombre'] = '';
        $validacion['rif'] = $rif;
        if ($this->metValidarRif($rif)) {
            if(function_exists('curl_init')) {
                $url .= $rif;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $result = curl_exec ($ch);
                if ($result) {
                    if(substr($result,0,1)!= '<' ) {
                        $validacion['code_result'] = "5";
                    }
                    $xml = simplexml_load_string($result);
                    if(!is_bool($xml)) {
                        $elements = $xml->children('rif');
                        $seniat = array();
                        foreach($elements as $key => $node) {
                            $index = strtolower($node->getName());
                            $seniat[$index] = (string)$node;
                        }
                        $validacion['code_result'] = "4";
                        $validacion['nombre'] = $seniat['nombre'];
                    }
                } else {
                    $validacion['code_result'] = "3";
                }
            } else {
                $validacion['code_result'] = "2";
            }
        } else {
            $validacion['code_result'] = "1";
        }
        echo $validacion['code_result']."--".$validacion['nombre']."--".$validacion['rif'];
        exit;
    }

    private function metValidarRif($rif) {
        $retorno = preg_match("/^([VEJPG]{1})([0-9]{9}$)/", $rif);

        if ($retorno) {
            $digitos = str_split($rif);

            $digitos[8] *= 2;
            $digitos[7] *= 3;
            $digitos[6] *= 4;
            $digitos[5] *= 5;
            $digitos[4] *= 6;
            $digitos[3] *= 7;
            $digitos[2] *= 2;
            $digitos[1] *= 3;

            // Determinar dígito especial según la inicial del RIF
            // Regla introducida por el SENIAT
            switch ($digitos[0]) {
                case 'V':
                    $digitoEspecial = 1;
                    break;
                case 'E':
                    $digitoEspecial = 2;
                    break;
                case 'J':
                    $digitoEspecial = 3;
                    break;
                case 'P':
                    $digitoEspecial = 4;
                    break;
                case 'G':
                    $digitoEspecial = 5;
                    break;
            }

            $suma = (array_sum($digitos) - $digitos[9]) + ($digitoEspecial*4);
            $residuo = $suma % 11;
            $resta = 11 - $residuo;

            $digitoVerificador = ($resta >= 10) ? 0 : $resta;

            if ($digitoVerificador != $digitos[9]) {
                $retorno = false;
            }
        }

        return $retorno;
    }



}