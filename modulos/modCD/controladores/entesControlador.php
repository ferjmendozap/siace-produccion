<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Registrar Organismos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class entesControlador extends Controlador
{
    private $atEnte;
    private $atMiscelaneoModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atEnte=$this->metCargarModelo('entes');
    }

    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $listaEntes=$this->atEnte->metListarEnte();

        for ($i=0;$i<(count($listaEntes));$i++){
            $arrEnte=$this->atEnte->atFuncionesGrles->metBuscaRecursivoEntes($listaEntes[$i]['idEnte']);
            $listaEntes[$i]['ind_nombre_ente']=$arrEnte['cadEntes'];
        }

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listadoEnte',$listaEntes);
        $this->atVista->metRenderizar('listado');

    }

    public function metResponsables()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $listaResponsables=$this->atEnte->metListarResponsables();

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listadoResponsables',$listaResponsables);
        $this->atVista->metRenderizar('listadoResponsables');

    }

    public function metAsignarEntes(){
        $valido=$this->metObtenerInt('valido');

        if($valido==1){
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int');
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            $responsable=$this->atEnte->metExisteRegistro($validacion['id_responsable']);
            if(!empty($responsable)) {
                if (isset($_POST['actualizar'])) {
                    $pkPersonaEnte = $responsable[0]["pk_num_persona_ente"];
                }else{
                    $pkPersonaEnte = 0;
                    $this->atEnte->metInactivarResponsablEnte2($responsable[0]["pk_num_persona_ente"]);
                }
            }else{
                $pkPersonaEnte=0;
            }
            $this->atEnte->metInactivarResponsablEnte($_POST['idEnte']);

            $pkEnte = $this->atEnte->metAsignarEnte($validacion['id_responsable'], $_POST['idEnte'], $validacion['ind_cargo_representante'], $validacion['fk_a006_num_miscdetalle_situacion'],$pkPersonaEnte);

            if (is_numeric($pkEnte)) {

                /*Asignando Entes Recursivos*/
                $arrEnte=$this->atEnte->atFuncionesGrles->metBuscaRecursivoEntes($pkEnte);
                $validacion['nombreEnte']=$arrEnte['cadEntes'];
                /***************************/
                $responsable1=$this->atEnte->metMostrarUsuario($validacion['id_responsable']);
                $validacion['responsable'] =$responsable1['responsable'];
                $validacion['idEnte'] = $pkEnte;
                $validacion['resultado'] = 'exito';

                echo json_encode($validacion);
                exit;
            } else {
                $validacion['pkEnte'] = $pkEnte;
                $validacion['resultado'] = 'error';
                echo json_encode($validacion);
                exit;
            }
        }
        $idEnte=$this->metObtenerInt('idEnte');
        /*Rellenar Formulario si existen registro de este ente con responsable activo*/
        $registro=$this->atEnte->metResponsableActivo($idEnte);
        if(!empty($registro)){
            $formDB['pk_num_persona']=$registro['pk_num_persona'];
            $formDB['ind_cedula_documento']=$registro['ind_cedula_documento'];
            $formDB['responsable']=$registro['responsable'];
            $formDB['fk_a006_num_miscdet_cargo_pers']=$registro['fk_a006_num_miscdet_cargo_pers'];
            $formDB['fk_a006_num_miscdetalle_situacion']=$registro['fk_a006_num_miscdetalle_situacion'];
            $this->atVista->assign('formDB',$formDB);
        }
        /**/

        //$this->atVista->assign('cargos',$this->atEnte->metCargo());
        $this->atVista->assign('cargos',$this->atEnte->atMiscelaneoModelo->metMostrarSelect('PFCPEXT'));
        $this->atVista->assign('situacion',$situacionCargo=$this->atEnte->metSituacionCargo());
        $this->atVista->assign('idEnte',$idEnte);
        $this->atVista->metRenderizar('asignacionEntes','modales');
    }

    public function metCrearModPersona(){
        $idResponsable=$this->metObtenerInt('idResponsable');
        //$idEnte=$this->metObtenerInt('id_ente');

        $complementosJs = array(
            'jquery/jquery.numeric.min',
        );
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->metComplementosForm();

        $valido=$this->metObtenerInt('valido');


        if($idResponsable!=0) {
            $formDB = $this->atEnte->metObtenerResponsable($idResponsable);
            $entesAsignados=$this->atEnte->metObtenerEntes($idResponsable);
            /*Formatear Fechas*/
            for($i=0;$i<(count($entesAsignados));$i++){
                /*Asignando Entes Recursivos*/
                $arrEnte=$this->atEnte->atFuncionesGrles->metBuscaRecursivoEntes($entesAsignados[$i]['idEnte']);
                $entesAsignados[$i]['ind_nombre_ente']=$arrEnte['cadEntes'];
                /***************************/

                $entesAsignados[$i]["fec_registro"] = $this->atEnte->atFuncionesGrles->metFormateaFecha($entesAsignados[$i]["fec_registro"]);
                if(!empty($entesAsignados[$i]["fec_ultima_modificacion"])){
                    $entesAsignados[$i]["fec_ultima_modificacion"]= $this->atEnte->atFuncionesGrles->metFormateaFecha($entesAsignados[$i]["fec_ultima_modificacion"]);
                }
            }
            /*******************/
        }else{
            $formDB ='';
            $entesAsignados='';
        }

        if($valido!=1){
            $this->atVista->assign('nacionalidad',$this->atEnte->metMiscelaneo('NACION'));
            $this->atVista->assign('tipo',$this->atEnte->metMiscelaneo('COD_TIPO'));
            $this->atVista->assign('tiponatural',$this->atEnte->metMiscelaneo('TIP_PERSON'));
        }
        else{
            $email=$_POST['form']['alphaNum']['ind_email'];
            $rif=$_POST['form']['alphaNum']['ind_documento_fiscal'];
            $exepcion=array('ind_email','ind_nombre2','ind_apellido2');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$exepcion);
            $ind=$this->metValidarFormArrayDatos('form','int');
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }else{
                if(!empty($email)) {
                    if ($this->metValidaMail($email) != 1) {
                        $validacion['ind_email'] = 'error';
                        echo json_encode($validacion);
                        exit;
                    }
                }
            }

            $ind_cedula_documento=$validacion['ind_cedula_documento'];
            $ind_documento_fiscal=$rif;
            $ind_nombre1=$validacion['ind_nombre1'];
            $ind_nombre2=$validacion['ind_nombre2'];
            $ind_apellido1=$validacion['ind_apellido1'];
            $ind_apellido2=$validacion['ind_apellido2'];
            $fk_a006_num_miscelaneo_detalle_nacionalidad=$validacion['fk_a006_num_miscelaneo_detalle_nacionalidad'];
            $ind_email=$email;
            $num_estatus=isset($validacion['num_estatus'])? $validacion['num_estatus'] : 0;
            $fk_a006_num_miscelaneo_det_tipopersona=$validacion['fk_a006_num_miscelaneo_det_tipopersona'];
            $ind_tipo_persona=$validacion['ind_tipo_persona'];
            if($idResponsable==0) {

                $guardar = $this->atEnte->metRegistraPersona($ind_cedula_documento, $ind_documento_fiscal, $ind_nombre1,
                    $ind_nombre2, $ind_apellido1, $ind_apellido2, $fk_a006_num_miscelaneo_detalle_nacionalidad,
                    $ind_email, $num_estatus, $fk_a006_num_miscelaneo_det_tipopersona, $ind_tipo_persona);
                $validacion['proceso']='nuevo';

            }else{

                $guardar = $this->atEnte->metModificaPersona($ind_cedula_documento, $ind_documento_fiscal, $ind_nombre1,
                    $ind_nombre2, $ind_apellido1, $ind_apellido2, $fk_a006_num_miscelaneo_detalle_nacionalidad,
                    $ind_email, $num_estatus, $fk_a006_num_miscelaneo_det_tipopersona, $ind_tipo_persona,$idResponsable);
                $validacion['proceso']='modificar';
            }

            if(is_int($guardar)){
                $validacion['pk_persona']=$guardar;
                $validacion['resultado']='exito';
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['resultado']='error';
                echo json_encode($validacion);
                exit;
            }

        }
        $this->atVista->assign('entesAsignados',$entesAsignados);
        $this->atVista->assign('idResponsable',$idResponsable);
        $this->atVista->assign('formDB',$formDB);
        $this->atVista->metRenderizar('CrearModPersona', 'modales');
    }

    public function metValidaMail($email)
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL) === false){
            return 1;
        }else{
            return 0;
        }
    }

    public function metComplementosForm(){
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min',
            'jquery/jquery.numeric.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
    }

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idEnte=$this->metObtenerInt('idEnte');
        $this->metComplementosForm();
            if($valido==1){
            $this->metValidarToken();
            $url=$_POST["form"]["alphaNum"]["ind_pagina_web"];
            $fecha=$_POST["form"]["alphaNum"]["fecha_fundacion"];
            $Excceccion=array(
                'id_ente',
                'pk_num_estado',
                'pk_num_municipio',
                'fk_num_parroquia',
                'fk_a010_num_ciudad',
                'tipoCategoria',
                'tipoEnte');
            $Excceccion2=array(
                'num_estatus',
                'num_estatus_representante',
                'ind_pagina_web',
                'ind_sujeto_control',
                'ind_telefono2',
                'ind_telefono3',
                'ind_fax',
                'ind_fax2',
                'ind_gaceta',
                'ind_resolucion',
                'ind_mision',
                'ind_vision',
                'ind_otros',
                'fecha_fundacion',
                'ind_numero_registro');

            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion2);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }
            if(!isset($validacion['num_estatus_representante'])){
                $validacion['num_estatus_representante']=0;
            }
            if(!isset($validacion['ind_sujeto_control'])){
                $validacion['ind_sujeto_control']=0;
            }
            if(!isset($validacion['fk_a006_num_miscelaneo_detalle_carcater_social'])){
                $validacion['fk_a006_num_miscelaneo_detalle_carcater_social']=false;
            }
            //Saltando la validacion de la url por caracteres especiales
            if($validacion['ind_pagina_web']){
                $validacion['ind_pagina_web']=(string)htmlspecialchars(preg_replace('/[^A-Za-z0-9ñÑáéíóúÁÉÍÓÚ.]/i', '', $url));
                $urlValida=$validacion['ind_pagina_web'];//para validar si es valida la url
            }else{
                $urlValida="";
            }
            $ente=empty($validacion['id_ente'])? 0:$validacion['id_ente'];
            $tipo_Ente=$validacion['tipoEnte'];
            if($validacion['tipoCategoria']==0){
                $categoriaEnte= NULL;
                $validacion['tipoCategoria']= NULL;
            } else {
                $categoriaEnte=$validacion['tipoCategoria'];
            }
            $descripcionEnte=$validacion['ind_nombre_ente'];
            $caracterSocial=$validacion['fk_a006_num_miscelaneo_detalle_carcater_social'];
            $doc_fiscal=$validacion['ind_numero_registro'];
            $tomoRegistro=$validacion['ind_tomo_registro'];
            $fechaFuncdacion=!empty($fecha)? date_format(date_create($fecha), 'Y-m-d'):NULL;
            $tel1=$validacion['ind_telefono'].'-N';
            $tel2=empty($validacion['ind_telefono2'])? '': $validacion['ind_telefono2'].'-N';
            $tel3=empty($validacion['ind_telefono3'])? '': $validacion['ind_telefono3'].'-N';
            $fax=empty($validacion['ind_fax'])? '': $validacion['ind_fax'].'-F';
            $fax2=empty($validacion['ind_fax2'])? '': $validacion['ind_fax2'].'-F';
            $faxes=','.$fax.(!empty($fax2)? ','.$fax2:'');
            $telefono=$tel1.(!empty($tel2)? ','.$tel2:'').(!empty($tel3)? ','.$tel3:'');
            $telefonos=$telefono.$faxes;
            $sitioWeb=$validacion['ind_pagina_web'];
            $direccion=$validacion['ind_direccion'];
            $estatus=$validacion['num_estatus'];
            $estatus_representante=$validacion['num_estatus_representante'];
            $sujetoAcontrol=$validacion['ind_sujeto_control'];
            $gaceta=$validacion['ind_gaceta'];
            $resolucion=$validacion['ind_resolucion'];
            $mision=$validacion['ind_mision'];
            $vision=$validacion['ind_vision'];
            $otros=$validacion['ind_otros'];
            if ($validacion['pk_num_estado']==0) {
                $estado = NULL;
            } else {
                $estado = $validacion['pk_num_estado'];
            }
            if ($validacion['pk_num_municipio']==0) {
                $municipio = NULL;
            } else {
                $municipio = $validacion['pk_num_municipio'];
            }
            if ($validacion['fk_num_parroquia']==0) {
                $parroquia = NULL;
            } else {
                $parroquia = $validacion['fk_num_parroquia'];
            }
            if ($validacion['fk_a010_num_ciudad']==0) {
                $ciudad = NULL;
            } else {
                $ciudad = $validacion['fk_a010_num_ciudad'];
            }
            if ($validacion['tipoEnte']==0) {
                $validacion['tipoEnte'] = NULL;
            }

            #entro a crear ente nuevo
            if($idEnte==0)
            {
                $nombreDuplicado=$this->atEnte->metCompararNombre($validacion['ind_nombre_ente']);
                $docFiscalDuplicado=$this->atEnte->metCompararDocFiscal($doc_fiscal);

                $id='';

                if($nombreDuplicado==false AND $docFiscalDuplicado==false) {

                    if(strlen($validacion["ind_telefono"])>11 OR strlen($validacion["ind_telefono"])<11){
                        $validacion['tel_digitos'] = 'error';
                        $validacion['ind_telefono'] = 'error';
                        echo json_encode($validacion);
                        exit;
                    }
                    if(!empty($validacion["ind_fax"])) {
                        if (strlen($validacion["ind_fax"]) > 11 OR strlen($validacion["ind_fax"]) < 11) {
                            $validacion['fax_digitos'] = 'error';
                            $validacion['ind_fax'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                    }

                    if(!empty($validacion["ind_fax2"])){
                        if(strlen($validacion["ind_fax2"])>11 OR strlen($validacion["ind_fax2"])<11){
                            $validacion['fax_digitos'] = 'error';
                            $validacion['ind_fax2'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                        if ($validacion["ind_fax"] == $validacion["ind_fax2"]) {
                            $validacion['fax_repeat'] = 'error';
                            $validacion['ind_fax'] = 'error';
                            $validacion['ind_fax2'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                    }

                    if(!empty($validacion["ind_telefono2"]) OR !empty($validacion["ind_telefono3"])) {

                        if((strlen($validacion["ind_telefono2"])>11 OR strlen($validacion["ind_telefono2"])<11) AND (!empty($validacion["ind_telefono2"]))){
                            $validacion['tel_digitos'] = 'error';
                            $validacion['ind_telefono2'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                        if((strlen($validacion["ind_telefono3"])>11 OR strlen($validacion["ind_telefono3"])<11) AND (!empty($validacion["ind_telefono3"]))){
                            $validacion['tel_digitos'] = 'error';
                            $validacion['ind_telefono3'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }

                        if ($validacion["ind_telefono"] == $validacion["ind_telefono2"]) {
                            $validacion['tel_repeat'] = 'error';
                            $validacion['ind_telefono'] = 'error';
                            $validacion['ind_telefono2'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                        elseif ($validacion["ind_telefono"] == $validacion["ind_telefono3"]) {
                            $validacion['tel_repeat'] = 'error';
                            $validacion['ind_telefono'] = 'error';
                            $validacion['ind_telefono3'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        } elseif ($validacion["ind_telefono2"] == $validacion["ind_telefono3"]) {
                            $validacion['tel_repeat'] = 'error';
                            $validacion['ind_telefono2'] = 'error';
                            $validacion['ind_telefono3'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                    }

                    $regex ="/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/";
                    if(!empty($urlValida)){
                        $urlWeb = preg_match($regex, $urlValida);
                    }else{
                        $urlWeb = 1;
                    }

                    if(!empty($urlValida)) {
                        if ($urlWeb == 0 ) {
                            $validacion['urlWeb'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                    }
                    $id = $this->atEnte->metCrearEnte(
                        $doc_fiscal,
                        $ente,
                        $categoriaEnte,
                        $descripcionEnte,
                        $caracterSocial,
                        $tomoRegistro,
                        $fechaFuncdacion,
                        $telefonos,
                        $sitioWeb,
                        $direccion,
                        $parroquia,
                        $ciudad,
                        $estatus,
                        $sujetoAcontrol,
                        $gaceta,
                        $resolucion,
                        $mision,
                        $vision,
                        $otros
                    );

                    $validacion['status'] = 'nuevo';
                }else{
                    if($nombreDuplicado!=false){
                        $validacion['status'] = 'Duplicado';
                        $validacion['mensaje']='Ya existe un organismo de igual nombre';

                    }else{
                        $validacion['status'] = 'Duplicado';
                        $validacion['mensaje']='Ya existe un documento fiscal indentico';
                    }
                }
            }

                 #comienzo a modificar ente
            else{
                $selectDataComparar=$this->atEnte->metSelectDatoComparar($idEnte);
                $nombreDuplicado=$this->atEnte->metCompararNombre($validacion['ind_nombre_ente']);
                $docFiscalDuplicado=$this->atEnte->metCompararDocFiscal($doc_fiscal);

                if($selectDataComparar['ind_nombre_ente']==$nombreDuplicado['ind_nombre_ente']){
                    $nombreDuplicado=false;
                };

                if($selectDataComparar['ind_numero_registro']==$docFiscalDuplicado['ind_numero_registro']){
                    $docFiscalDuplicado=false;
                };

                $id='';
                if($nombreDuplicado==false AND $docFiscalDuplicado==false) {

                    if(strlen($validacion["ind_telefono"])>11 OR strlen($validacion["ind_telefono"])<11){
                        $validacion['tel_digitos'] = 'error';
                        $validacion['ind_telefono'] = 'error';
                        echo json_encode($validacion);
                        exit;
                    }
                    if(!empty($validacion["ind_fax"])) {
                        if (strlen($validacion["ind_fax"]) > 11 OR strlen($validacion["ind_fax"]) < 11) {
                            $validacion['fax_digitos'] = 'error';
                            $validacion['ind_fax'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                    }

                    if(!empty($validacion["ind_fax2"])){
                        if(strlen($validacion["ind_fax2"])>11 OR strlen($validacion["ind_fax2"])<11){
                            $validacion['fax_digitos'] = 'error';
                            $validacion['ind_fax2'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                        if ($validacion["ind_fax"] == $validacion["ind_fax2"]) {
                            $validacion['fax_repeat'] = 'error';
                            $validacion['ind_fax'] = 'error';
                            $validacion['ind_fax2'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                    }

                    if(!empty($validacion["ind_telefono2"]) OR !empty($validacion["ind_telefono3"])) {

                        if((strlen($validacion["ind_telefono2"])>11 OR strlen($validacion["ind_telefono2"])<11) AND (!empty($validacion["ind_telefono2"]))){
                            $validacion['tel_digitos'] = 'error';
                            $validacion['ind_telefono2'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                        if((strlen($validacion["ind_telefono3"])>11 OR strlen($validacion["ind_telefono3"])<11) AND (!empty($validacion["ind_telefono3"]))){
                            $validacion['tel_digitos'] = 'error';
                            $validacion['ind_telefono3'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }

                        if ($validacion["ind_telefono"] == $validacion["ind_telefono2"]) {
                            $validacion['tel_repeat'] = 'error';
                            $validacion['ind_telefono'] = 'error';
                            $validacion['ind_telefono2'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                        elseif ($validacion["ind_telefono"] == $validacion["ind_telefono3"]) {
                            $validacion['tel_repeat'] = 'error';
                            $validacion['ind_telefono'] = 'error';
                            $validacion['ind_telefono3'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        } elseif ($validacion["ind_telefono2"] == $validacion["ind_telefono3"]) {
                            $validacion['tel_repeat'] = 'error';
                            $validacion['ind_telefono2'] = 'error';
                            $validacion['ind_telefono3'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                    }

                    $regex ="/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/";
                    if(!empty($urlValida)){
                        $urlWeb = preg_match($regex, $urlValida);
                    }else{
                        $urlWeb = 1;
                    }

                    if(!empty($urlValida)) {
                        if ($urlWeb == 0) {
                            $validacion['urlWeb'] = 'error';
                            $validacion['ind_pagina_web'] = 'error';
                            echo json_encode($validacion);
                            exit;
                        }
                    }

                    $id = $this->atEnte->metModificarEnte($doc_fiscal,
                        $idEnte,
                        $ente,
                        $tipo_Ente,
                        $categoriaEnte,
                        $descripcionEnte,
                        $caracterSocial,
                        $tomoRegistro,
                        $fechaFuncdacion,
                        $telefonos,
                        $sitioWeb,
                        $direccion,
                        $estado,
                        $municipio,
                        $parroquia,
                        $ciudad,
                        $estatus,
                        $estatus_representante,
                        $sujetoAcontrol,
                        $gaceta,
                        $resolucion,
                        $mision,
                        $vision,
                        $otros
                    );

                    $validacion['status'] = 'modificar';
                }else{
                    if($nombreDuplicado!=false){
                        $validacion['status'] = 'Duplicado';
                        $validacion['mensaje']='Ya existe un organismo de igual nombre';

                    }else{
                        $validacion['status'] = 'Duplicado';
                        $validacion['mensaje']='Ya existe un documento fiscal indentico';
                    }
                }
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idEnte']=$id;

            $arrEnte=$this->atEnte->atFuncionesGrles->metBuscaRecursivoEntes($id);
            $validacion['ind_nombre_ente']=$arrEnte['cadEntes'];

            echo json_encode($validacion);
            exit;
        }


        $pais=$this->atEnte->metListarPais();

        $entes='';
        $situacionCargo=$this->atEnte->metSituacionCargo();
        $tipoCaracter=$this->atEnte->metCaracterSocial();
        $this->atVista->assign('pais',$pais);
        $this->atVista->assign('tipoEnte',$this->atEnte->atMiscelaneoModelo->metMostrarSelect('PFTIPOENTE'));
        $this->atVista->assign('entes',$entes);
        $this->atVista->assign('cargos',$this->atEnte->metCargo());
        $this->atVista->assign('situacion',$situacionCargo);
        $this->atVista->assign('tipoCaracter',$tipoCaracter);
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    public function metListaEntes()
    {/*Lista de forma recursiva las dependencias de un ente externo al ser seleccionado*/
        $idEnte = $this->metObtenerInt('idEnte');
        $listaEntes = $this->atEnte->metListaEntes($idEnte);
        //print_r($listaDependenciasExt);exit;
        echo json_encode($listaEntes);
        exit;
    }

    /**
     * Lista los entes externos para llenar la grilla al hacer click sobre la etiqueta entes del form modal de planificaciones
     */
    public function metCargaGrillaEntes(){


        if(empty($_POST['idEnte'])){
            $idEnte=0;
        }else{
            $idEnte=$_POST['idEnte'];

        }

        $result=$this->atEnte->metBuscaEntesGrilla($idEnte);
        $data=array();
        if(COUNT($result)){
            foreach ($result as $fila) {
                $fila["ente_padre"]="";
                if($fila["num_ente_padre"]){
                    $fila["ente_padre"]=$this->atEnte->atFuncionesGrles->metBuscaEntesPadre($fila['id_ente']);
                }
                if($fila["situacion_titular"]=="Titular" OR $fila["situacion_titular"]=="Encargado"){
                    $fila["nombre_titular"]=$this->atEnte->atFuncionesGrles->metConsolidaNombrePersEnte($fila);
                }
                unset($fila["ind_nombre1"]);unset($fila["ind_nombre2"]);unset($fila["ind_apellido1"]);unset($fila["ind_apellido2"]);
                $data[]=$fila;
            }
            $result=null;
        }
        $this->atVista->assign('listaentes', $data);
        $this->atVista->metRenderizar('listadoEntes', 'modales');
    }

    public function metCargaGrillaEntes2(){
        $listaResponsables=$this->atEnte->metListarResponsables();

        $this->atVista->assign('listaResponsables', $listaResponsables);
        $this->atVista->metRenderizar('seleccionaResponsable', 'modales');
    }

    public function metListarCategoria(){

        $idTipoEnte=$this->metObtenerInt('ente');
        $categoria=$this->atEnte->metTipoCategorias($idTipoEnte);

        echo "<option value=''></option>";

        foreach($categoria as $categorias){
            echo "<option value=".$categorias['pk_num_categoria_ente'].">".$categorias['ind_categoria_ente']."</option>";
        }
    }

    public function metListarEstado()
    {
        $pais = $this->metObtenerInt('pais');
        $estado = $this->atEnte->metListarEstado($pais);

        echo "<option value=''></option>";

        foreach($estado as $estados){
            echo "<option value=".$estados['pk_num_estado'].">".$estados['ind_estado']."</option>";
        }
    }

    public function metListarMunicipio()
    {
        $estado = $this->metObtenerInt('estado');
        $municipio=$this->atEnte->metListarMunicipio($estado);

        echo "<option value=''></option>";

        foreach($municipio as $municipios){
            echo "<option value=".$municipios['pk_num_municipio'].">".$municipios['ind_municipio']."</option>";
        }
    }

    public function metListarCiudad()
    {
        $estado = $this->metObtenerInt('estado');
        $ciudad=$this->atEnte->metListarCiudad($estado);

        echo "<option value=''></option>";

        foreach($ciudad as $ciudades){
            echo "<option value=".$ciudades['pk_num_ciudad'].">".$ciudades['ind_ciudad']."</option>";
        }
    }

    public function metListarParroquia()
    {
        $municipio = $this->metObtenerInt('municipio');
        $parroquia=$this->atEnte->metListarParroquia($municipio);

        echo "<option value=''></option>";

        foreach($parroquia as $parroquias){
            echo "<option value=".$parroquias['pk_num_parroquia'].">".$parroquias['ind_parroquia']."</option>";
        }
    }

    public function metEliminar()
    {
        $idEnte = $this->metObtenerInt('idEnte');
        $id=$this->atEnte->metEliminarEnte($idEnte);

        if(is_array($id)){
            $valido=array(
                'mensaje'=>'Este Ente no se puede eliminar',
                'status'=>'Error',
                'idEnte'=>$id
            );

        }else {
            $valido = array(
                'status' => 'ok',
                'idEnte' => $id
            );
        }

        echo json_encode($valido);
        exit;
    }

    public function metEliminarResponsable()
    {
        $idResp = $this->metObtenerInt('idResponsable');
        $id=$this->atEnte->metEliminarResponsable($idResp);

        if(is_array($id)){
            $valido=array(
                'mensaje'=>'Este registro no se puede eliminar',
                'status'=>'Error',
                'idResponsable'=>$id
            );

        }else {
            $valido = array(
                'status' => 'ok',
                'idResponsable' => $id
            );
        }

        echo json_encode($valido);
        exit;
    }

    public function metActOInactRespsableEnte(){
        $idRegistro=$this->metObtenerInt('idRegistro');
        $estatus=$this->metObtenerInt('estatus');
        $idResp=$this->metObtenerInt('idResp');;


        if($estatus==1){
            $estatusFinal=0;
        }else{
            $estatusFinal=1;
        }
        /*Inactivar responsables de entes cuando se procede a habilitar el mismo en un ente diferente*/
        if ($estatusFinal==1){
            $entesActivos=$this->atEnte->metResponsableEnteActivo($idResp);

            $array = array_map(function($el){ return $el['pk_num_persona_ente']; }, $entesActivos);
            $entesActivos = implode(',', $array);

            if(!empty($entesActivos)){
                $this->atEnte->metDesactivarResponsablEnte($entesActivos);
            }
        }
        /*-------------------------------------------------------------------------------------*/

        $resultado=$this->atEnte->metActOInactRespsableEnte($estatusFinal,$idRegistro);
        echo json_encode($resultado);
    }

    public function metListadoAsignacion()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $listaResponsables=$this->atEnte->metListarResponsable();

        $ente='';
        $representante='';
        /*obteniendo las id del ente para asignarlos*/
        foreach($listaResponsables AS $listaResponsable){
            $ente[]=$listaResponsable["pk_num_ente"];
            $representante[]=$listaResponsable["representante"];
        }

        $listaEntes=$this->atEnte->metListarEnte();

        /*Asignando nombre de representante*/
        if(!empty($ente)) {
            for ($i = 0; $i < count($ente); $i++) {

                $array = array_map(function ($el) {
                    return $el['idEnte'];
                }, $listaEntes);

                $clave = array_search($ente[$i], $array);
                $listaEntes[$clave]['representante'] = $representante[$i];
                $listaEntes[$clave]['num_estatus_titular'] = 1;

            }
        }

        /*Asignando nombre de ente recursivos*/
        for ($i=0;$i<(count($listaEntes));$i++){
            $arrEnte=$this->atEnte->atFuncionesGrles->metBuscaRecursivoEntes($listaEntes[$i]['idEnte']);
            $listaEntes[$i]['ind_nombre_ente']=$arrEnte['cadEntes'];
        }

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listadoEnte',$listaEntes);
        $this->atVista->metRenderizar('listadoAsignacion');

    }

}


