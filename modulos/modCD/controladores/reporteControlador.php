<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Reporte de Documentos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 
 
 class reporteControlador extends Controlador
{
    private $atReporteModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atReporteModelo = $this->metCargarModelo('reporte');
    }


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {


        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->_TipoDependencia = $this->metCargarModelo('dependencia');
        $this->atVista->assign('_TipoDependencia',$this->atReporteModelo-> metListarTipoDependenciaInt());

        $this->atVista->metRenderizar('Documentoint');


    }

    #Metodo para cargar el listado de reporte lista de documento entrada

 public function metentdocumentextReporte()
    {

   
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',

        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->_TipoOrganismo = $this->metCargarModelo('organismo');
        $this->atVista->assign('_TipoOrganismo',$this->_TipoOrganismo-> metListarOrgaExt());
		$this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarDepExt());
        $this->atTipoDocumento = $this->metCargarModelo('entdocumenexterno');
        $this->atVista->assign('corresp',$this->atTipoDocumento->metListarCorrespon());

        $this->atVista->metRenderizar('Entdocumenext');
    }

                     public function metentdocumentextReportePdf()
                    {
		              #Recibiendo las variables del formulario

                   $formInt=$this->metObtenerInt('form','int');
	               $formTxt=$this->metObtenerTexto('form','txt');

                    if(!empty($formInt)){
                        foreach ($formInt as $tituloInt => $valorInt) {
                            if(!empty($formInt[$tituloInt])){
                                $validacion[$tituloInt]=$valorInt;
                            }else{
                                $validacion[$tituloInt]='error';
                            }
                        }
                    }
                    if(!empty($formTxt)) {
                        foreach ($formTxt as $tituloTxt => $valorTxt) {
                            if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                                $validacion[$tituloTxt] = $valorTxt;
                            } else {
                                $validacion[$tituloTxt] = 'error';
                            }
                        }
                    }

                    if($validacion['desde']!="error"){
                        $desde=$validacion['desde'];
                    }
                    else{
                        $desde="error";
                    }
                    if($validacion['hasta']!="error"){
                        $hasta=$validacion['hasta'];
                    }else{
                        $hasta="error";
                    }
                    ini_set("max_execution_time","9000");
                    $usuario= Session::metObtener('idUsuario');
                    $this->metObtenerLibreria('cabeceraEquipo','modCD');
                    $pdf=new pdfReporteentdocumentext('L','mm','Letter');
                    $pdf->AliasNbPages();
                    $pdf->AddPage();

                    $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
                    $pdf->SetFont('Arial', 'B', 7);
                    $pdf->Cell(5, 5);
                    $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
                    $pdf->Cell(25, 5, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
                    $pdf->Cell(20, 5, utf8_decode('FEC. DOCUMENTO'), 1, 0, 'C', 1);
                    $pdf->Cell(50, 5, utf8_decode('ASUNTO'), 1, 0, 'C', 1);
                    $pdf->Cell(50, 5, utf8_decode('ORGANISMO / PARTICULAR'), 1, 0, 'C', 1);
                    $pdf->Cell(50, 5, utf8_decode('DEPENDENCIA'), 1, 0, 'C', 1);
                    $pdf->Cell(55, 5, utf8_decode('REPRESENTANTE PARTICULAR'), 1, 1, 'C', 1);
                    $pdf->SetFont('Arial', '', 7);
                    $this->_ReporteModelo = $this->metCargarModelo('reporte');
                    $consulta= $this->_ReporteModelo-> getReporteDisponibilidad($validacion['num_org_ext'],$validacion['ind_estado'],$validacion['num_depend_ext'],
                    $desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
                    $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
                    for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
                    {
                        $pdf->SetWidths(array(5,25,20,50,50,50,55));
                        $pdf->SetAligns(array('C','C','C','C','C','C','C'));
                        $pdf->Cell(5,5); //
                         if($consulta[$i]['num_org_ext']!='0'){
                            $organismo=$consulta[$i]['organismo'];
                        }else{
                            $organismo="PARTICULAR";
                        }
                        if($consulta[$i]['num_depend_ext']!='0'){
                            $dependencia=$consulta[$i]['descipDependencia'];
                        }else{
                            $dependencia="PARTICULAR";
                        }
                        $pdf->Row(array($i+1,
                            utf8_decode($consulta[$i]['num_documento']),
                            utf8_decode($consulta[$i]['fec_registro']),
                            utf8_decode($consulta[$i]['text_asunto']),
                            utf8_decode($organismo),
                            utf8_decode($dependencia),
                            utf8_decode($consulta[$i]['ind_desc_representante'])
                        ));


                    }
                    $pdf->Output();

                }

	// Reporte de listado de distribucion externa
	public function metdistribucionextReporte()
    {

   
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarTipoDependenciaInt());
		
        $this->atVista->metRenderizar('Distribdocumenext');
    }

    public function metdistribucionextReportePdf()
    {
		 #Recibiendo las variables del formulario

        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReportedistribucionext ('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(20, 5, utf8_decode('N i°'), 1, 0, 'C', 1);
        $pdf->Cell(20, 5, utf8_decode('CORRELATIVO'), 1, 0, 'C', 1);
        $pdf->Cell(20, 5, utf8_decode('FEC. ENVÍO'), 1, 0, 'C', 1);
		$pdf->Cell(80, 5, utf8_decode('DEPENDENCIA'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5, utf8_decode('REPRESENTANTE / EMPLEADO'), 1, 0, 'C', 1);
        $pdf->Cell(64, 5, utf8_decode('CARGO'), 1, 0, 'C', 1);
        $pdf->Cell(15, 5, utf8_decode('ESTADO
        '), 1, 1, 'C', 1);
        $pdf->SetFont('Arial', '', 7);
        $this->_ReporteModelo = $this->metCargarModelo('reporte');
        $consulta= $this->_ReporteModelo-> getReporteDistribucion($validacion['ind_dependencia_destinataria'],
        $validacion['ind_estado'],
        $validacion['num_documento'],
		$validacion['fk_cdc003_num_tipo_documento'],
        $desde,
        $hasta);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
            $pdf->SetWidths(array(20,20,20,80,40,64,15));
            $pdf->SetAligns(array('C','C','C','C','C','C','C'));

            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['num_secuencia']),
                utf8_decode($consulta[$i]['fec_salida']),
				utf8_decode($consulta[$i]['ind_desc_dependencia']),
                utf8_decode($consulta[$i]['ind_desc_representante']),
                utf8_decode($consulta[$i]['ind_cargo_destinatario']),
				utf8_decode($consulta[$i]['ind_estado'])
            ));
        }
        $pdf->Output();

    }
	
	
	public function metdistribucionpordocextReporte()
    {

		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
    	$this->_TipoOrganismo = $this->metCargarModelo('organismo');
        $this->atVista->assign('_TipoOrganismo',$this->_TipoOrganismo-> metListarOrgaExt());
		$this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarDepExt());
		$this->_TipoDocumento = $this->metCargarModelo('entdocumenexterno');
        $this->atVista->assign('_TipoDocumento',$this->_TipoDocumento-> metListarDodumentExt());
        $this->atVista->metRenderizar('Distribpordocumenext');


    }

    public function metdistribucionpordocextReportePdf()
    {
		 #Recibiendo las variables del formulario

        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReportedistribucionPorDocext('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetFont('Arial', '', 7);
        $this->_ReporteModelo = $this->metCargarModelo('reporte');
        $consulta= $this->_ReporteModelo-> getReporteDistribucionpordoc($validacion['num_org_ext'],$validacion['num_depend_ext'],$validacion['ind_estado'],
		$validacion['pk_num_documento'],$desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		//$pdf->Ln(4);
		
     	for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
		if($consulta[$i]['num_org_ext']!="0"){
        $desde=$consulta[$i]['RepresentanteOrg'];
        }else{
           $desde=$consulta[$i]['RepresentanteDep'];
        }
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->Cell(5,5,'',0,1,'');		
			$pdf->Cell(25, 5, utf8_decode('Tipo Documento:'), 0, 0, 'L');$pdf->Cell(100, 5,$consulta[$i]['tipoDoc'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Remitente:'), 0, 0, 'L'); $pdf->Cell(115, 5,$desde, 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Asunto:'), 0, 0, 'L');$pdf->Cell(200, 5,$consulta[$i]['text_asunto'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Fecha de Registro:'), 0, 0, 'L'); $pdf->Cell(115, 5,$consulta[$i]['fec_registro'], 0, 1, 'L');


		$consultadetalle= $this->_ReporteModelo-> getReporteDistribucionpordocDet($consulta[$i]['pk_num_documento']);
		
		//$pdf->Ln(2);
		//	$pdf->Cell(25,8,'',0,1,'');
  /// -------------------------------------------------
  
   	$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(1, 5);
        $pdf->Cell(23, 5, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
		$pdf->Cell(15, 5, utf8_decode('AÑO'), 1, 0, 'C', 1);
        $pdf->Cell(30, 5, utf8_decode('FEC. ENVÍO'), 1, 0, 'C', 1);
		$pdf->Cell(70, 5, utf8_decode('DEPENDENCIA'), 1, 0, 'C', 1);
        $pdf->Cell(60, 5, utf8_decode('REPRESENTANTE / EMPLEADO'), 1, 0, 'C', 1);
       $pdf->Cell(60, 5, utf8_decode('CARGO'), 1, 1, 'C', 1);
       // $pdf->Cell(20, 5, utf8_decode('ESTADO'), 1, 1, 'C', 1);

				  
            $pdf->SetWidths(array(23,15,30,70,60,60));
            $pdf->SetAligns(array('C','C','C','C','C','C'));
            
	
		   for($d=0; $d< count($consultadetalle); $d++)// recibe las casillas marcadas
        {
						
				 if($consultadetalle[$d]['ind_estado']=='RE'){
                $consultadetalle[$d]['ind_estado']="Recibido";
		 			}
				if($consultadetalle[$d]['ind_estado']=='CO'){
                $consultadetalle[$d]['ind_estado']="Completado";
		 			}
					
		if($consultadetalle[$d]['ind_dependencia_destinataria']!=""){
          $Part=$consultadetalle[$d]['ind_desc_dependencia'];
		  $dep=$consultadetalle[$d]['ind_desc_representante'];
		  $cargo=$consultadetalle[$d]['ind_cargo_destinatario'];
        }
			if($consultadetalle[$d]['ind_persona_destinataria']!=""){
          $Part=$consultadetalle[$d]['ind_desc_dependencia'];
		  $dep=$consultadetalle[$d]['ind_desc_representante'];
          $cargo=$consultadetalle[$d]['ind_cargo_destinatario'];
		}
					
			
			 $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
			 $pdf->SetFont('Arial', '', 7);
			 $pdf->Cell(1,5); //
            	$pdf->Row(array(
				utf8_decode($consultadetalle[$d]['num_documento']),
				utf8_decode($consultadetalle[$d]['fec_annio']),				
                utf8_decode($consultadetalle[$d]['fec_envio']),
				utf8_decode($Part),
                utf8_decode($dep),
                utf8_decode($cargo)
            ));
     }
			 $pdf->Ln(2);
			 $pdf->SetDrawColor(0, 0, 0);
        }
		 $pdf->SetFont('Arial', '', 6);
  			$pdf->Cell(30,8,'Cantidad de Documentos = ');$pdf->Cell(10,8,$i);
        $pdf->Output();

    }
		 
		
	// Reporte de listado de Documentos de salida
	public function metsalidocumenextReporte()
    {

   
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs); 

		$this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarTipoDependenciaInt());
		
		 $this->_TipoOrganismo = $this->metCargarModelo('organismo');
        $this->atVista->assign('_TipoOrganismo',$this->_TipoOrganismo-> metListarOrgaExt());
		
		
        $this->atVista->metRenderizar('Salidocumenext');
    }

    public function metsalidocumenextReportePdf()
    {
		 #Recibiendo las variables del formulario

        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReporteSalidaext ('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(3, 3);
        $pdf->Cell(5, 3,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(22, 3, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
		$pdf->Cell(20, 3, utf8_decode('T DOCUMENTO'), 1, 0, 'C', 1);
        $pdf->Cell(20, 3, utf8_decode('FEC. DOCUMENTO'), 1, 0, 'C', 1);
		$pdf->Cell(70, 3, utf8_decode('REMITENTE'), 1, 0, 'C', 1);
        $pdf->Cell(40, 3, utf8_decode('ASUNTO'), 1, 0, 'C', 1);
        $pdf->Cell(60, 3, utf8_decode('COMENTARIO'), 1, 0, 'C', 1);
        $pdf->Cell(18, 3, utf8_decode('ESTADO'), 1, 1, 'C', 1);
        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> getReporteSalidocumenext($validacion['ind_organismo_externo'],$validacion['ind_dependencia_remitente'],$validacion['ind_estado'],
		$desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
		
	
			 
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
		
				 if($consulta[$i]['ind_estado']=='EV'){
                $consulta[$i]['ind_estado']="Enviado";
		 			}
					
				 if($consulta[$i]['ind_estado']=='PE'){
                $consulta[$i]['ind_estado']="Pendiente";
		 			}
				 if($consulta[$i]['ind_estado']=='PR'){
                $consulta[$i]['ind_estado']="Preparación";
		 			}
				 if($consulta[$i]['ind_estado']=='AN'){
                $consulta[$i]['ind_estado']="Anulado";
		 			}
				
				 if($consulta[$i]['ind_estado']=='RE'){
                $consulta[$i]['ind_estado']="Recibido";
		 			}
				if($consulta[$i]['ind_estado']=='DV'){
                $consulta[$i]['ind_estado']="Devuelto";
		 			}
		 
            $pdf->SetWidths(array(5,22,20,20,70,40,60,18));
            $pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
            $pdf->Cell(3,3); //

            $pdf->Row(array($i+1,
	            utf8_decode($consulta[$i]['ind_cod_completo']),
				 utf8_decode($consulta[$i]['ind_descripcion']),
                utf8_decode($consulta[$i]['fec_documento']),
				utf8_decode($consulta[$i]['ind_dependencia']),
                utf8_decode($consulta[$i]['ind_asunto']),
                utf8_decode($consulta[$i]['txt_descripcion']),
				utf8_decode($consulta[$i]['ind_estado'])
            ));
        }
		
		  $pdf->SetFont('Arial', '', 6);
  			$pdf->Cell(30,8,'Cantidad de Documentos = ');$pdf->Cell(10,8,$i);
        $pdf->Output();

    }

	public function metdistribucionSalidaextReporte()
    {

   
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

    $this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarTipoDependenciaInt());
		
	 $this->_TipoOrganismo = $this->metCargarModelo('organismo');
        $this->atVista->assign('_TipoOrganismo',$this->_TipoOrganismo-> metListarOrgaExt());
		
        $this->atVista->metRenderizar('DistribucionSalidaext');


    }

    public function metdistribucionSalidaextReportePdf()
    {
		 #Recibiendo las variables del formulario

        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
	
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }


        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReporteDistriSalidaext('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(3, 5);
        //$pdf->Cell(3, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('TIPO DOCUMENTO'), 1, 0, 'C', 1);
		$pdf->Cell(48, 5, utf8_decode('ORGANISMO'), 1, 0, 'C', 1);
        $pdf->Cell(48, 5, utf8_decode('DEPENDENCIA'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5, utf8_decode('CARGO'), 1, 0, 'C', 1);
        $pdf->Cell(50, 5, utf8_decode('REPRESENTANTE'), 1, 0, 'C', 1);
		 $pdf->Cell(20, 5, utf8_decode('ESTADO'), 1, 1, 'C', 1);


        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> getReporteDistribucionSalidaext($validacion['ind_organismo_externo'],$validacion['ind_dependencia_remitente'],$validacion['ind_estado'],
		$desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
		
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
		
            $pdf->SetWidths(array(25,25,48,48,40,50,20));
            $pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
            $pdf->Cell(3,5); //

			 if($consulta[$i]['ind_organismo_externo']!='0'){
                $organismo=$consulta[$i]['nomOrg'];
            }
			
			if($consulta[$i]['ind_dependencia_externa']!='0'){
                $dependencia=$consulta[$i]['nomDep'];
            }
			
				if($consulta[$i]['ind_representante_externo']!='0'){
                $cargo=$consulta[$i]['ind_cargo_externo'];
            }
			
			 if($consulta[$i]['ind_estado']=='EV'){
                $consulta[$i]['ind_estado']="Enviado";
		 			}
					
				 if($consulta[$i]['ind_estado']=='PE'){
                $consulta[$i]['ind_estado']="Pendiente";
		 			}
				 if($consulta[$i]['ind_estado']=='PR'){
                $consulta[$i]['ind_estado']="Preparación";
		 			}
				 if($consulta[$i]['ind_estado']=='AN'){
                $consulta[$i]['ind_estado']="Anulado";
		 			}
				
				 if($consulta[$i]['ind_estado']=='RE'){
                $consulta[$i]['ind_estado']="Recibido";
		 			}
				if($consulta[$i]['ind_estado']=='DV'){
                $consulta[$i]['ind_estado']="Devuelto";
		 			}

            $pdf->Row(array(utf8_decode($consulta[$i]['ind_cod_completo']),
                utf8_decode($consulta[$i]['ind_descripcion']),
                utf8_decode($organismo),
                utf8_decode($dependencia),
				utf8_decode($cargo),
				utf8_decode($consulta[$i]['ind_representante_externo']),
				utf8_decode($consulta[$i]['ind_estado'])
            ));


        }
		 $pdf->SetFont('Arial', '', 6);
  			$pdf->Cell(30,8,'Cantidad de Documentos = ');$pdf->Cell(10,8,$i);
        $pdf->Output();

    }
	
		public function metDistribPorDocSalidaReporte()
    {

   
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

    	$this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarTipoDependenciaInt());
		
		 $this->_TipoDocumento = $this->metCargarModelo('salidocumenext');
         $this->atVista->assign('_TipoDocumento',$this->_TipoDocumento-> metListarDodumentSald());
		 
		  $this->_TipoOrganismo = $this->metCargarModelo('organismo');
         $this->atVista->assign('_TipoOrganismo',$this->_TipoOrganismo-> metListarOrganismoInt());

		
		
        $this->atVista->metRenderizar('DistribPorDocSalida');


    }

    public function metDistribPorDocSalidaReportePdf()
    {
		 #Recibiendo las variables del formulario

        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
	
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }


        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReporteDistriPorSalidaext('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> getReporteDistribPorDocSalida($validacion['fk_a001_num_organismo'],$validacion['ind_dependencia_remitente'],$validacion['ind_estado'],
		$validacion['pk_num_documento'],$desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
		
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		//$pdf->Ln(4);
		
		
     		  for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {

			$pdf->SetFont('Arial', 'B', 7);
			$pdf->Cell(5,5,'',0,1,'');		
			$pdf->Cell(25, 5, utf8_decode('Tipo Documento:'), 0, 0, 'L');$pdf->Cell(100, 5,$consulta[$i]['tipoDoc'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Remitente:'), 0, 0, 'L'); $pdf->Cell(115, 5,$consulta[$i]['ind_persona_remitente'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Cargo:'), 0, 0, 'L');$pdf->Cell(200, 5,$consulta[$i]['ind_cargo_remitente'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Asunto:'), 0, 0, 'L');$pdf->Cell(200, 5,$consulta[$i]['ind_asunto'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Fecha de Registro:'), 0, 0, 'L'); $pdf->Cell(115, 5,$consulta[$i]['fec_registro'], 0, 1, 'L');

				
				
		$consultadetalle= $this->_ReporteModelo-> getReporteDistribPorSalidaDet($consulta[$i]['fk_cdc011_num_documento']);
		
		//$pdf->Ln(2);
		//	$pdf->Cell(25,8,'',0,1,'');
  /// -------------------------------------------------
  
   	$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(1, 5);
        //$pdf->Cell(3, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(23, 5, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
		//$pdf->Cell(12, 5, utf8_decode('AÑO'), 1, 0, 'C', 1);
        $pdf->Cell(80, 5, utf8_decode('ORGANISMO / DEPENDENCIA / EMPRESA'), 1, 0, 'C', 1);
		//$pdf->Cell(53, 5, utf8_decode('DEPENDENCIA'), 1, 0, 'C', 1);
        $pdf->Cell(53, 5, utf8_decode('REPRESENTANTE'), 1, 0, 'C', 1);
        $pdf->Cell(50, 5, utf8_decode('CARGO'), 1, 0, 'C', 1);
        //$pdf->Cell(18, 5, utf8_decode('FEC. DISTRIB.'), 1, 0, 'C', 1);
		 $pdf->Cell(45, 5, utf8_decode('ESTADO'), 1, 1, 'C', 1);

				  
            $pdf->SetWidths(array(23,80,53,50,45));
            $pdf->SetAligns(array('C','C','C','C','C'));
            
	
		   for($d=0; $d< count($consultadetalle); $d++)// recibe las casillas marcadas
        {
			 if($consultadetalle[$d]['ind_estado']=='EV'){
                $consultadetalle[$d]['ind_estado']="Enviado";
		 			}
					
				 if($consultadetalle[$d]['ind_estado']=='PE'){
                $consultadetalle[$d]['ind_estado']="Pendiente";
		 			}
				 if($consultadetalle[$d]['ind_estado']=='PR'){
                $consultadetalle[$d]['ind_estado']="Preparación";
		 			}
				 if($consultadetalle[$d]['ind_estado']=='AN'){
                $consultadetalle[$d]['ind_estado']="Anulado";
		 			}
				
				 if($consultadetalle[$d]['ind_estado']=='RE'){
                $consultadetalle[$d]['ind_estado']="Recibido";
		 			}
				if($consultadetalle[$d]['ind_estado']=='DV'){
                $consultadetalle[$d]['ind_estado']="Devuelto";
		 			}
			
			if($consultadetalle[$d]['ind_organismo_externo']!=""){
          $org=$consultadetalle[$d]['nomOrg'];
		  $deprep=$consultadetalle[$d]['ind_representante_externo'];
		  $cargo=$consultadetalle[$d]['ind_cargo_externo'];
        }

		if($consultadetalle[$d]['ind_dependencia_externa']!=""){
		$org=$consultadetalle[$d]['nomDep'];
		$deprep=$consultadetalle[$d]['ind_representante_externo'];
        $cargo=$consultadetalle[$d]['ind_cargo_externo'];
			}	
			
		if($consultadetalle[$d]['ind_empresa_externa']!=""){
			$org=$consultadetalle[$d]['nomProveedor'];
		  $deprep=$consultadetalle[$d]['ind_representante_externo'];
          $cargo=$consultadetalle[$d]['ind_cargo_externo'];
			
			}	
	
			
			 $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
			 $pdf->SetFont('Arial', '', 7);
			 $pdf->Cell(1,5); //
            $pdf->Row(array(utf8_decode($consultadetalle[$d]['ind_cod_completo']),
				//utf8_decode($consultadetalle[$d]['fec_annio']),
				utf8_decode($org),
   				//utf8_decode($dep),
				utf8_decode($deprep),
				utf8_decode($cargo),
				utf8_decode($consultadetalle[$d]['ind_estado'])
            ));
     }
			 $pdf->Ln(2);
			 $pdf->SetDrawColor(0, 0, 0);
        }
		 $pdf->SetFont('Arial', '', 6);
  			$pdf->Cell(30,8,'Cantidad de Documentos = ');$pdf->Cell(10,8,$i);
        $pdf->Output();

    }
		
	public function metDistribHistDocSalidaReporte()
    {

   
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

    	$this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarTipoDependenciaInt());
		
		 $this->_TipoDocumento = $this->metCargarModelo('salidocumenext');
         $this->atVista->assign('_TipoDocumento',$this->_TipoDocumento-> metListarDodumentSald());
		 
		  $this->_TipoOrganismo = $this->metCargarModelo('organismo');
         $this->atVista->assign('_TipoOrganismo',$this->_TipoOrganismo-> metListarOrganismoInt());

		
		
        $this->atVista->metRenderizar('DistribHistDocSalida');


    }

    public function metDistribHistDocSalidaReportePdf()
    {
		 #Recibiendo las variables del formulario

        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
	
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }


        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReporteDistriHistPorSalidaext('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> getReporteDistribHistDocSalida($validacion['fk_a001_num_organismo'],$validacion['ind_dependencia_remitente'],$validacion['ind_estado'],
		$validacion['pk_num_documento'],$desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
		
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		//$pdf->Ln(4);
		
		
     		  for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {

			$pdf->SetFont('Arial', 'B', 7);
			$pdf->Cell(5,5,'',0,1,'');		
			$pdf->Cell(25, 5, utf8_decode('Tipo Documento:'), 0, 0, 'L');$pdf->Cell(100, 5,$consulta[$i]['tipoDoc'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Remitente:'), 0, 0, 'L'); $pdf->Cell(115, 5,$consulta[$i]['ind_persona_remitente'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Cargo:'), 0, 0, 'L');$pdf->Cell(200, 5,$consulta[$i]['ind_cargo_remitente'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Asunto:'), 0, 0, 'L');$pdf->Cell(200, 5,$consulta[$i]['ind_asunto'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Fecha de Registro:'), 0, 0, 'L'); $pdf->Cell(115, 5,$consulta[$i]['fec_registro'], 0, 1, 'L');

				
				
		$consultadetalle= $this->_ReporteModelo-> getReporteDistribPorSalidaDet($consulta[$i]['fk_cdc011_num_documento']);
		
		//$pdf->Ln(2);
		//	$pdf->Cell(25,8,'',0,1,'');
  /// -------------------------------------------------
  
   	$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(1, 5);
        //$pdf->Cell(3, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(23, 5, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
		//$pdf->Cell(12, 5, utf8_decode('AÑO'), 1, 0, 'C', 1);
        $pdf->Cell(80, 5, utf8_decode('ORGANISMO / DEPENDENCIA / EMPRESA'), 1, 0, 'C', 1);
		//$pdf->Cell(53, 5, utf8_decode('DEPENDENCIA'), 1, 0, 'C', 1);
        $pdf->Cell(53, 5, utf8_decode('REPRESENTANTE'), 1, 0, 'C', 1);
        $pdf->Cell(50, 5, utf8_decode('CARGO'), 1, 0, 'C', 1);
        //$pdf->Cell(18, 5, utf8_decode('FEC. DISTRIB.'), 1, 0, 'C', 1);
		 $pdf->Cell(45, 5, utf8_decode('ESTADO'), 1, 1, 'C', 1);

				  
            $pdf->SetWidths(array(23,80,53,50,45));
            $pdf->SetAligns(array('C','C','C','C','C'));
            
	
		   for($d=0; $d< count($consultadetalle); $d++)// recibe las casillas marcadas
        {
			 if($consultadetalle[$d]['ind_estado']=='EV'){
                $consultadetalle[$d]['ind_estado']="Enviado";
		 			}
					
				 if($consultadetalle[$d]['ind_estado']=='PE'){
                $consultadetalle[$d]['ind_estado']="Pendiente";
		 			}
				 if($consultadetalle[$d]['ind_estado']=='PR'){
                $consultadetalle[$d]['ind_estado']="Preparación";
		 			}
				 if($consultadetalle[$d]['ind_estado']=='AN'){
                $consultadetalle[$d]['ind_estado']="Anulado";
		 			}
				
				 if($consultadetalle[$d]['ind_estado']=='RE'){
                $consultadetalle[$d]['ind_estado']="Recibido";
		 			}
				if($consultadetalle[$d]['ind_estado']=='DV'){
                $consultadetalle[$d]['ind_estado']="Devuelto";
		 			}
			
			if($consultadetalle[$d]['ind_organismo_externo']!=""){
          $org=$consultadetalle[$d]['nomOrg'];
		  $deprep=$consultadetalle[$d]['ind_representante_externo'];
		  $cargo=$consultadetalle[$d]['ind_cargo_externo'];
        }

		if($consultadetalle[$d]['ind_dependencia_externa']!=""){
		$org=$consultadetalle[$d]['nomDep'];
		$deprep=$consultadetalle[$d]['ind_representante_externo'];
        $cargo=$consultadetalle[$d]['ind_cargo_externo'];
			}	
			
		if($consultadetalle[$d]['ind_empresa_externa']!=""){
			$org=$consultadetalle[$d]['nomProveedor'];
		  $deprep=$consultadetalle[$d]['ind_representante_externo'];
          $cargo=$consultadetalle[$d]['ind_cargo_externo'];
			
			}	
	
			
			 $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
			 $pdf->SetFont('Arial', '', 7);
			 $pdf->Cell(1,5); //
            $pdf->Row(array(utf8_decode($consultadetalle[$d]['ind_cod_completo']),
				//utf8_decode($consultadetalle[$d]['fec_annio']),
				utf8_decode($org),
   				//utf8_decode($dep),
				utf8_decode($deprep),
				utf8_decode($cargo),
				utf8_decode($consultadetalle[$d]['ind_estado'])
            ));
     }
			 $pdf->Ln(2);
			 $pdf->SetDrawColor(0, 0, 0);
        }
		 $pdf->SetFont('Arial', '', 6);
  			$pdf->Cell(30,8,'Cantidad de Documentos = ');$pdf->Cell(10,8,$i);
        $pdf->Output();

    }
		
	 public function metDocumentointReporte()
    {

   
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

    $this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarTipoDependenciaInt());
		
		 $this->_TipoOrganismo = $this->metCargarModelo('organismo');
        $this->atVista->assign('_TipoOrganismo',$this->_TipoOrganismo-> metListarTipoOrganismo());

        $this->atTipoDocumento = $this->metCargarModelo('entdocumenexterno');
        $this->atVista->assign('corresp',$this->atTipoDocumento->metListarCorrespondencia());

        $this->atVista->metRenderizar('Documentoint');


    }

   public function metDocumentointReportePdf()
    {
		 #Recibiendo las variables del formulario

        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
	
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }


        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReporteentdocumentint('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(3, 5);
        //$pdf->Cell(3, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
		$pdf->Cell(15, 5, utf8_decode('AÑO'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('TIPO DOCUMENTO'), 1, 0, 'C', 1);
		$pdf->Cell(48, 5, utf8_decode('REMITENTE'), 1, 0, 'C', 1);
        $pdf->Cell(48, 5, utf8_decode('ASUNTO'), 1, 0, 'C', 1);
        $pdf->Cell(60, 5, utf8_decode('COMENTARIO'), 1, 0, 'C', 1);
        $pdf->Cell(18, 5, utf8_decode('FEC. DOC.'), 1, 0, 'C', 1);
		 $pdf->Cell(20, 5, utf8_decode('ESTADO'), 1, 1, 'C', 1);


        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> getReporteDocumentoint($validacion['ind_dependencia_remitente'],$validacion['ind_dependencia_destinataria'],$validacion['ind_estado'],
		$validacion['ind_documento_completo'],$desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
		
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
		
            $pdf->SetWidths(array(25,15,25,48,48,60,18,20));
            $pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
            $pdf->Cell(3,5); //
			
			 if($consulta[$i]['ind_estado']=='EV'){
                $consulta[$i]['ind_estado']="Enviado";
		 			}
					
				 if($consulta[$i]['ind_estado']=='PE'){
                $consulta[$i]['ind_estado']="Pendiente";
		 			}
				 if($consulta[$i]['ind_estado']=='PR'){
                $consulta[$i]['ind_estado']="Preparación";
		 			}
            if($consulta[$i]['ind_estado']=='PP'){
                $consulta[$i]['ind_estado']="Preparado";
            }
				 if($consulta[$i]['ind_estado']=='AN'){
                $consulta[$i]['ind_estado']="Anulado";
		 			}
				
				 if($consulta[$i]['ind_estado']=='RE'){
                $consulta[$i]['ind_estado']="Recibido";
		 			}
				if($consulta[$i]['ind_estado']=='DV'){
                $consulta[$i]['ind_estado']="Devuelto";
		 			}

            $pdf->Row(array(utf8_decode($consulta[$i]['ind_documento_completo']),
				utf8_decode($consulta[$i]['fec_annio']),
                utf8_decode($consulta[$i]['tipoDoc']),
   				utf8_decode($consulta[$i]['descipDependencia']),
				utf8_decode($consulta[$i]['ind_asunto']),
				utf8_decode($consulta[$i]['ind_descripcion']),
				utf8_decode($consulta[$i]['fec_registro']),
				utf8_decode($consulta[$i]['ind_estado'])
            ));


        }
		 $pdf->SetFont('Arial', '', 6);
  			$pdf->Cell(30,8,'Cantidad de Documentos = ');$pdf->Cell(10,8,$i);
        $pdf->Output();

    }
	
	
	public function metDistribdocumenintReporte()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
    	$this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarTipoDependenciaInt());
		$this->_TipoDocumento = $this->metCargarModelo('documentinternos');
        $this->atVista->assign('_TipoDocumento',$this->_TipoDocumento-> metListarDodumentInt());
        $this->atVista->metRenderizar('Distribdocumenint');
    }

    public function metDistribdocumenintReportePdf()
    {
		 #Recibiendo las variables del formulario
        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReporteDistriDocumentint('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> getReporteDistribdocumenint($validacion['ind_dependencia_remitente'],$validacion['ind_dependencia_destinataria'],$validacion['ind_estado'],
		$validacion['pk_num_documento'],$desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
		
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		//$pdf->Ln(4);
		
     		  for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
           
			
            $pdf->SetWidths(array(5,25,130,30));
            $pdf->SetAligns(array('C','L','L','R'));
           // $pdf->Cell(5,5); //
			$pdf->Cell(5,5,'',0,1,'');		
			$pdf->Cell(25, 5, utf8_decode('Dependencia:'), 0, 0, 'L'); $pdf->Cell(115, 5,utf8_decode($consulta[$i]['descipDependencia']), 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Remitente:'), 0, 0, 'L'); $pdf->Cell(115, 5,utf8_decode($consulta[$i]['ind_persona_remitente']), 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Cargo:'), 0, 0, 'L');$pdf->Cell(200, 5,$consulta[$i]['ind_cargo_remitente'], 0, 1, 'L');

				
				
		$consultadetalle= $this->_ReporteModelo-> getReporteDistribdocumenDet($consulta[$i]['fk_cdb001_num_documento']);
		
		//$pdf->Ln(2);
		//	$pdf->Cell(25,8,'',0,1,'');
  /// -------------------------------------------------
  
   	$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(1, 5);
        //$pdf->Cell(3, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
		$pdf->Cell(12, 5, utf8_decode('AÑO'), 1, 0, 'C', 1);
        $pdf->Cell(48, 5, utf8_decode('DESTINATARIO'), 1, 0, 'C', 1);
		$pdf->Cell(53, 5, utf8_decode('DEPENDENCIA'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5, utf8_decode('CARGO'), 1, 0, 'C', 1);
        $pdf->Cell(45, 5, utf8_decode('ASUNTO'), 1, 0, 'C', 1);
        $pdf->Cell(18, 5, utf8_decode('FEC. DISTRIB.'), 1, 0, 'C', 1);
		 $pdf->Cell(15, 5, utf8_decode('ESTADO'), 1, 1, 'C', 1);
            $pdf->SetWidths(array(25,12,48,53,40,45,18,15));
            $pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
		   for($d=0; $d< count($consultadetalle); $d++)// recibe las casillas marcadas
        {
			
			 if($consultadetalle[$d]['ind_estado']=='EV'){
                $consultadetalle[$d]['ind_estado']="Enviado";
		 			}
					
					
			 $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
			 $pdf->SetFont('Arial', '', 7);
			 $pdf->Cell(1,5); //
            $pdf->Row(array(utf8_decode($consultadetalle[$d]['ind_documento_completo']),
				utf8_decode($consultadetalle[$d]['fec_annio']),
				utf8_decode($consultadetalle[$d]['ind_desc_representante']),
   				utf8_decode($consultadetalle[$d]['ind_desc_dependencia']),
				utf8_decode($consultadetalle[$d]['ind_cargo_destinatario']),
				utf8_decode($consultadetalle[$d]['ind_asunto']),
				utf8_decode($consultadetalle[$d]['fec_registro']),
				utf8_decode($consultadetalle[$d]['ind_estado'])
            ));
     }
			 $pdf->Ln(2);
			 $pdf->SetDrawColor(0, 0, 0);
        }
		 $pdf->SetFont('Arial', '', 6);
  			$pdf->Cell(30,8,'Cantidad de Documentos = ');$pdf->Cell(10,8,$i);
        $pdf->Output();

    }
	
		public function metDistribPordocumenintReporte()
    {

   
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
			 'bootstrap-datepicker/bootstrap-datepicker',

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

    	$this->_TipoDependencia = $this->metCargarModelo('dependencia');
		$this->atVista->assign('_TipoDependencia',$this->_TipoDependencia-> metListarTipoDependenciaInt());
		
		 $this->_TipoDocumento = $this->metCargarModelo('documentinternos');
         $this->atVista->assign('_TipoDocumento',$this->_TipoDocumento-> metListarDodumentInt());
		
		
        $this->atVista->metRenderizar('DistribPordocumenint');


    }

    public function metDistribPordocumenintReportePdf()
    {
		 #Recibiendo las variables del formulario

        $formInt=$this->metObtenerInt('form','int');
		$formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
	
		if($validacion['desde']!="error"){
            $desde=$validacion['desde'];
        }else{
            $desde="error";
        }
        if($validacion['hasta']!="error"){
            $hasta=$validacion['hasta'];
        }else{
            $hasta="error";
        }


        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfReporteDistriDocumentint('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetFont('Arial', '', 7);
//
        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> getReporteDistribdocumenint($validacion['ind_dependencia_remitente'],$validacion['ind_dependencia_destinataria'],$validacion['ind_estado'],
		$validacion['pk_num_documento'],$desde,$hasta,$validacion['fk_cdc003_num_tipo_documento']);
		
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		//$pdf->Ln(4);
		
     		  for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {

			$pdf->SetFont('Arial', 'B', 7);
			$pdf->Cell(5,5,'',0,1,'');		
			$pdf->Cell(25, 5, utf8_decode('Tipo Documento:'), 0, 0, 'L');$pdf->Cell(100, 5,$consulta[$i]['tipoDoc'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Remitente:'), 0, 0, 'L'); $pdf->Cell(115, 5,$consulta[$i]['ind_persona_remitente'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Cargo:'), 0, 0, 'L');$pdf->Cell(200, 5,$consulta[$i]['ind_cargo_remitente'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Asunto:'), 0, 0, 'L');$pdf->Cell(200, 5,$consulta[$i]['ind_asunto'], 0, 1, 'L');
			$pdf->Cell(25, 5, utf8_decode('Fecha de Registro:'), 0, 0, 'L'); $pdf->Cell(115, 5,$consulta[$i]['fec_registro'], 0, 1, 'L');

				
				
		$consultadetalle= $this->_ReporteModelo-> getReporteDistribdocumenDet($consulta[$i]['fk_cdb001_num_documento']);
		
		//$pdf->Ln(2);
		//	$pdf->Cell(25,8,'',0,1,'');
  /// -------------------------------------------------
  
   	$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(1, 5);
        //$pdf->Cell(3, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
		$pdf->Cell(12, 5, utf8_decode('AÑO'), 1, 0, 'C', 1);
        $pdf->Cell(45, 5, utf8_decode('DESTINATARIO'), 1, 0, 'C', 1);
		$pdf->Cell(53, 5, utf8_decode('DEPENDENCIA'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5, utf8_decode('CARGO'), 1, 0, 'C', 1);
        $pdf->Cell(45, 5, utf8_decode('COMENTARIO'), 1, 0, 'C', 1);
        $pdf->Cell(18, 5, utf8_decode('FEC. DISTRIB.'), 1, 0, 'C', 1);
		 $pdf->Cell(20, 5, utf8_decode('ESTADO'), 1, 1, 'C', 1);

				  
            $pdf->SetWidths(array(25,12,45,53,40,45,18,20));
            $pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
            
	
		   for($d=0; $d< count($consultadetalle); $d++)// recibe las casillas marcadas
        {
			 if($consultadetalle[$d]['ind_estado']=='EV'){
                $consultadetalle[$d]['ind_estado']="Enviado";
		 			}
					
				 if($consultadetalle[$d]['ind_estado']=='PE'){
                $consultadetalle[$d]['ind_estado']="Pendiente";
		 			}
				 if($consultadetalle[$d]['ind_estado']=='PR'){
                $consultadetalle[$d]['ind_estado']="Preparación";
		 			}
				 if($consultadetalle[$d]['ind_estado']=='AN'){
                $consultadetalle[$d]['ind_estado']="Anulado";
		 			}
				
				 if($consultadetalle[$d]['ind_estado']=='RE'){
                $consultadetalle[$d]['ind_estado']="Recibido";
		 			}
				if($consultadetalle[$d]['ind_estado']=='DV'){
                $consultadetalle[$d]['ind_estado']="Devuelto";
		 			}
					
			 $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
			 $pdf->SetFont('Arial', '', 7);
			 $pdf->Cell(1,5); //
            $pdf->Row(array(utf8_decode($consultadetalle[$d]['ind_documento_completo']),
				utf8_decode($consultadetalle[$d]['fec_annio']),
				utf8_decode($consultadetalle[$d]['ind_desc_representante']),
   				utf8_decode($consultadetalle[$d]['ind_desc_dependencia']),
				utf8_decode($consultadetalle[$d]['ind_cargo_destinatario']),
				utf8_decode($consultadetalle[$d]['ind_descripcion']),
				utf8_decode($consultadetalle[$d]['fec_registro']),
				utf8_decode($consultadetalle[$d]['ind_estado'])
            ));
     }
			 $pdf->Ln(2);
			 $pdf->SetDrawColor(0, 0, 0);
        }
		 $pdf->SetFont('Arial', '', 6);
  			$pdf->Cell(30,8,'Cantidad de Documentos = ');$pdf->Cell(10,8,$i);
        $pdf->Output();

    }
	
    protected function FormatoFecha($fecha)
    {  
	 
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
 
		return $resultado;

    }



}