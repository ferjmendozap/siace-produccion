<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class correspondenciaControlador extends Controlador
{
    private $atCorrespondencia;

    public function __construct()
    {
        parent::__construct();
		 Session::metAcceso();
        $this->atCorrespondencia=$this->metCargarModelo('correspondencia');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atCorrespondencia->metListarCorrespondencia());
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificar()
    {
       $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
	   
	    $valido=$this->metObtenerInt('valido');
        $idCorrespondencia=$this->metObtenerInt('idCorrespondencia');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            //$ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }
	
            if($idCorrespondencia==0){
                $id=$this->atCorrespondencia->metCrearCorrespondencia($validacion['ind_descripcion'],$validacion['ind_descripcion_corta'],$validacion['ind_procedencia'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atCorrespondencia->metModificarCorrespondencia($validacion['ind_descripcion'],$validacion['ind_descripcion_corta'],$validacion['ind_procedencia'],$validacion['num_estatus'],$idCorrespondencia);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idCorrespondencia']=$id;

            echo json_encode($validacion);
            exit;
        }

	
        if($idCorrespondencia!=0){
        $this->atVista->assign('formDB',$this->atCorrespondencia->metMostrarCorrespondencia($idCorrespondencia));
           
		 $this->atVista->assign('idCorrespondencia',$idCorrespondencia);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idCorrespondencia = $this->metObtenerInt('idCorrespondencia');
        if($idCorrespondencia!=0){
            $id=$this->atCorrespondencia->metEliminarCorrespondencia($idCorrespondencia);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idCorrespondencia'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


 
	
	 
	
}