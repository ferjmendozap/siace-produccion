<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Dependencias - Listado</h2>
    </div>

        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                     <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="6">Check</th>
                            <th width="100">Dependencia</th>
                            <th width="80">Representante Legal</th>
                            <th width="50">Cargo</th>
						</tr>
                        </thead>
                        <tbody>
                        {foreach item=dependencia from=$listado}
                            <tr id="idDependencia{$dependencia.pk_num_ente}">
                                <td>
                                   <div class="checkbox checkbox-styled">
                                        <label>
                       <input type="checkbox" class="valores" idDependencia="{$dependencia.pk_num_ente}" 
					   dependencia="{$dependencia.ind_nombre_ente}" representantedep="{$dependencia.nombre_apellidos}"
					   cargodep="{$dependencia.ind_cargo_personal_externo}">
					   
												   
                                        </label>
                                    </div>
                                </td>
                                <td><label>{$dependencia.ind_nombre_ente}</label></td>
                                <td><label>{$dependencia.nombre_apellidos}</label></td>
                                <td><label>{$dependencia.ind_cargo_personal_externo}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
      <button type="button"   title="Cancelar"  class="btn btn-default ink-reaction btn-raised" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
  	<button type="button"  title="Agregar"  class="btn btn-primary ink-reaction btn-raised" id="agregarDependenciaSeleccionado"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Agregar</button>
    </button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
	  $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#agregarDependenciaSeleccionado').click(function () {
            var input = $('.valores');
            for (i = 0; i < input.length; i++) {
                if (input[i].checked == true) {
                    $(document.getElementsByClassName('idDependencia' + input[i].getAttribute('idDependencia'))).remove();
                    $(document.getElementById('Dependencia')).append(
                            '<tr class="idDependencia' + input[i].getAttribute('idDependencia') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idDependencia') + '" name="form[alphaNum][ind_dependencia_externa][' + input[i].getAttribute('idDependencia') + ']" class="dependenciaInput" dependencia="' + input[i].getAttribute('idDependencia') + '" />' +
							
														 
							'<input type="hidden" value="' + input[i].getAttribute('representantedep') + '" name="form[alphaNum][ind_representante_externo][' + input[i].getAttribute('idDependencia') + ']" class="representanteInput" representantedep="' + input[i].getAttribute('representantedep') + '" />' +
							
							'<input type="hidden" value="' + input[i].getAttribute('cargodep') + '" name="form[alphaNum][ind_cargo_personal_externo][' + input[i].getAttribute('idDependencia') + ']" class="cargoInput" cargodep="' + input[i].getAttribute('cargodep') + '" />' +
								
							'<td>' + input[i].getAttribute('dependencia') + '</td>' +
                            '<td>' + input[i].getAttribute('representantedep') + '</td>' +
							'<td>' + input[i].getAttribute('cargodep') + '</td>' +
                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idDependencia' + input[i].getAttribute('idDependencia') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>'
							
											
                    );
                }
            }
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>