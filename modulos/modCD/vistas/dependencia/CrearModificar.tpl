<form action="{$_Parametros.url}modCD/dependenciaCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDependencia) }
            <input type="hidden" value="{$idDependencia}" name="idDependencia"/>
        {/if}
        <div class="row"> 
            <div class="col-lg-12">

            <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[alphaNum][ind_descripcion]" id="ind_descripcion">
                        <label for="ind_descripcion"><i class="md md-description"></i>&nbsp;Descripcion</label>
                    </div>
                </div>
			
                  <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_direccionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}" name="form[alphaNum][ind_direccion]" id="ind_direccion">
                        <label for="ind_direccion"><i class="md md-store"></i>&nbsp;Direccion</label>
                    </div>
                </div>
               
			     <div class="col-lg-12">
                   <div class="form-group floating-label" id="fk_a001_num_organismoError">
                        <select id="fk_a001_num_organismo" name="form[alphaNum][fk_a001_num_organismo]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$lista}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_a001_num_organismo"><i class="md md-home"></i>&nbsp;Seleccione el Organismo</label>
                    </div>
                </div>
				
				 
            </div>
			
            <div class="col-sm-12">
         
               <div class="col-sm-3">
                                        <div class="form-group" id="fk_a003_num_persona_representanteError">
                                            <label for="nombreProveedor"><i class="icm icm-calculate2"></i>
                                                Nombre del Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}"
                                                   id="nombreProveedor" disabled readonly>

                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <input id="pk_num_persona" type="hidden" value="{if isset($formDB.fk_a003_num_persona_representante)}{$formDB.fk_a003_num_persona_representante}{/if}" name="form[int][fk_a003_num_persona_representante]">
                                            <button
                                                    class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                    type="button"
                                                    title="Buscar Proveedor"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar Persona"
                                                       url="{$_Parametros.url}modCD/organismoCONTROL/proveedorMET/proveedor/"
                                                    >
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        </div>
                                    </div>
                 

				<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_cargo_representanteError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_cargo_representante)}{$formDB.ind_cargo_representante}{/if}" name="form[alphaNum][ind_cargo_representante]" id="ind_representante">
                        <label for="ind_cargo_representante"><i class="md md-account-circle"></i>&nbsp;Cargo Representante</label>
                    </div>
                </div>
 
                  <div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_documento_fiscalError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_documento_fiscal)}{$formDB.ind_documento_fiscal}{/if}" name="form[int][ind_documento_fiscal]" id="ind_documento_fiscal">
                        <label for="ind_documento_fiscal"><i class="md md-perm-contact-cal"></i>&nbsp;Documento Fiscal</label>
                    </div>
                </div>
                
 
                <div class="col-sm-2">
                    <div class="checkbox checkbox-styled">
                        <label>
							  <input type="checkbox" {if (isset($formDB.num_estatus) and $formDB.num_estatus==1) OR !isset($idDependencia) } checked{/if} value="1" name="form[alphaNum][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
               
            </div>
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" title="Cancelar"  class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
        <button type="button"  title="Guardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		
		  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
        $('#modalAncho').css("width","75%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='modificar'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                $(document.getElementById('idDependencia'+dato['idDependencia'])).html('<td>'+dato['idDependencia']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['ind_cargo_representante']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-02-02-M',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDependencia="'+dato['idDependencia']+'"' +
                            'descipcion="El Usuario a Modificado una Dependencia" titulo="Modificar Dependencia">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('CD-01-03-01-02-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDependencia="'+dato['idDependencia']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado una Dependencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Dependencia!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "La Dependencia fue modificada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/dependenciaCONTROL');
                }else if(dato['status']=='nuevo'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idDependencia'+dato['idDependencia']+'">' +
                            '<td>'+dato['idDependencia']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['ind_cargo_representante']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-02-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDependencia="'+dato['idDependencia']+'"' +
                            'descipcion="El Usuario a Modificado una Dependencia" titulo="Modificar Dependencia">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('CD-01-03-01-02-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDependencia="'+dato['idDependencia']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado una Dependencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Dependencia!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro exitoso!", "La Dependencia fue registrada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/dependenciaCONTROL');
                }
            },'json');
        });
    });
</script>