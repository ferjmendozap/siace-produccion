<form action="{$_Parametros.url}modCD/salidocumenextCONTROL/anularMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
			   <div class="row">
			   <div class="col-sm-3">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                       <select  id="fk_cdc003_num_tipo_documento" readonly  {if isset($veranular)}disabled{/if}   name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control" >
                            
                            {foreach item=app from=$correspon}
                                {if isset($formDB.fk_cdc003_num_tipo_documento)}
                                    {if $app.pk_num_tipo_documento==$formDB.fk_cdc003_num_tipo_documento}
                                        <option selected value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   				</div>
				
				<div class="col-sm-2">
                    <div class="form-group floating-label" id="fec_documentoError">
                              <input type="text"  readonly {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}" name="form[txt][fec_documento]" >
					          <label for="fec_documento"><i class="md md-event"></i>&nbsp;Fecha Documento</label>
                       </div>
                </div>
				
					
					<div class="col-sm-2">
                    <div class="form-group floating-label" id="ind_cod_completoError">
                        <input type="text"  readonly  {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_cod_completo)}{$formDB.ind_cod_completo}{/if}" name="form[txt][ind_cod_completo]" id="ind_cod_completo">
                        <label for="ind_cod_completo"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
                </div>
 
 				<div class="col-sm-5">
                    <div class="form-group floating-label" id="ind_asuntoError">
                        <input type="text" readonly  {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}" name="form[alphaNum][ind_asunto]" id="ind_asunto">
                        <label for="ind_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>
				</div>

			<div class="col-lg-6">
			 <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo"  {if isset($ver)}disabled{/if} name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo Remitente</label>
                    </div>
					</div> 
					
	<div class="col-sm-6">
				<div class="form-group" id="txt_descripcionError">
                <textarea name="form[alphaNum][txt_descripcion]" id="txt_descripcion" readonly  {if isset($veranular)}disabled{/if}  class="form-control input-sm" rows="1" placeholder="" required data-msg-required="Introduzca la Descripci�n del Asunto" >{if isset($formDB.txt_descripcion)}{$formDB.txt_descripcion}{/if}</textarea>
                <label for="txt_descripcion">Descripcion Asunto</label>
            </div>
			</div>
			
										 <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="docProveedor"><i class="md md-contacts"></i>
                                             Cargo Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
                                    </div>
									
									 <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="nombreProveedor"><i class="md md-group"></i>
                                              Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}"
                                                   id="nombreProveedor" disabled readonly>
                                        </div>
                                    </div>
														
              						 <div class="col-sm-6">
                                        <div class="form-group" id="ind_dependenciaError">
                                            <label for="dependencia"><i class="icm icm-calculate2"></i>
                                                Dependencia Remitente</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}"
                                                   id="dependencia" disabled readonly>

                                        </div>
                                    </div>
	
				<div class="col-sm-6">
				  <div   align="left">&nbsp;&nbsp;<font color="#20B2AA"><b>Indique cual es el motivo de la Anulaci&oacute;n</b></font></div>
                    <div class="form-group floating-label" id="ind_motivo_anuladoError">
 				  
					                  <textarea name="form[alphaNum][ind_motivo_anulado]" id="ind_motivo_anulado"  {if isset($ver)}disabled{/if}  class="form-control input-sm" rows="1"  >{if isset($formDB.ind_motivo_anulado)}{$formDB.ind_motivo_anulado}{/if}</textarea>
			
                        <label for="ind_motivo_anulado"><i class="md md-question-answer"></i>&nbsp;Motivo de Anulaci&oacute;n</label>
                    </div>
				</div>			

				 
				 <div class="col-sm-6">
                            <div align="left">&nbsp;&nbsp;<font color="#20B2AA"><b>Seleccione el Responsable de Anulaci&oacute;n</b></font></div>
                                      
               <div class="col-sm-11">
                                        <div class="form-group" id="ind_persona_anuladoError">
                                            <label for="nombrePersona"><i class="icm icm-calculate2"></i>
                                                Nombre del Responsable</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.persona_anula)}{$formDB.persona_anula}{/if}"
                                                   id="nombrePersona" disabled readonly>

                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <input id="pk_num_persona" type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="form[alphaNum][ind_persona_anulado]">
											  {if isset($ver)} {else}
                                          <button type="button"
                                                   class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                  id="responsable"
                                                  data-toggle="modal" data-target="#formModal2"
                                                  data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar responsable"
                                                   url="{$_Parametros.url}modCD/salidocumenextCONTROL/personaMET/persona/"
                                                    >
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
											 {/if}
                                        </div>
                                    </div>

					</div>	  
						  </div>
					  </div>
					   
					

			  
                </div>
        <span class="clearfix"></span>
    </div>
	
    <div class="modal-footer">
	
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">Cancelar</button>
		  {if isset($ver)} {else}
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Anular</button>
			 {/if}
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function() {

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
	
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

		
	
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
      //  $('#fec_anulado').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","90%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
               // if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                $(document.getElementById('idDocumento'+dato['idDocumento'])).html('<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['ind_cod_completo']+'</td>' +
                            '<td>'+dato['ind_asunto']+'</td>' +
                            '<td>'+dato['txt_descripcion']+'</td>' +
							'<td>'+dato['ind_estado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-01-04-A',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Modificar Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '</td>');
                    swal("Registro Modificado!", "El Documento fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='anular'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    //if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idDocumento'+dato['idDocumento']+'">' +
                            '<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['ind_cod_completo']+'</td>' +
                             '<td>'+dato['ind_asunto']+'</td>' +
                            '<td>'+dato['txt_descripcion']+'</td>' +
                             '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-01-04-A',$_Parametros.perfil)}<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a anulado un Documento Externo" titulo="Anular Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '</td>');
                    swal("Registro Modificado!", "El Documento fue Anulado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>
