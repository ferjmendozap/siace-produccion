<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Salida de Documentos Externos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
								 <th  width="70">Fecha Documento</th>
                                 <th width="80">Nro de Documento</th>
                                 <th width="80">Dependencia</th>
                                <th  width="120">Asunto</th>
								<th  width="200">Comentario</th>
								 <th  width="40">Estado</th>
                                <th align="left" width="160">Modificar- Ver -  Anular-  Modif. Rest</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
								    <td><label>{$documento.fec_documento}</label></td>
                                    <td><label>{$documento.num_cod_interno}-{$documento.num_secuencia}-{$documento.fec_annio}</label></td>
                                    <td><label>{$documento.ind_dependencia}</label></td>
                                    <td><label>{$documento.ind_asunto}</label></td>
									 <td><label>{$documento.txt_descripcion} </label></td>
									 <td>
									 <label>
                                         {if $documento.ind_estado=='AN'}Anulado
                                         {elseif $documento.ind_estado=='PR'}Preparacion
                                         {elseif $documento.ind_estado=='PP'}Preparado
                                         {elseif $documento.ind_estado=='EV'}Enviado
                                         {elseif $documento.ind_estado=='RE'}Recibido
                                         {elseif $documento.ind_estado=='PE'}Pendiente
                                         {/if}
                                     </label>
									 
									 </td>
                                   
                                    <td align="left">
									{if $documento.ind_estado=='AN'}
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 {if in_array('CD-01-01-02-01-05-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Ver"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos Externos | Ver Registro">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
										
										
										{elseif $documento.ind_estado=='EV'}
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									{if in_array('CD-01-01-02-01-03-M',$_Parametros.perfil)}
                                           <button class="veranular logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Ver"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos Externos | Ver Registro">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
										
                                        {/if}
										
										
										
										
										{elseif $documento.ind_estado!='AN'  AND $documento.ind_estado!='RE'}
										
									{if in_array('CD-01-01-02-01-03-M',$_Parametros.perfil)}
                                            <button class="actualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Modificar"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Documentos de Salida | Editar Registro">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
										
										&nbsp;&nbsp;&nbsp;
										  {if in_array('CD-01-01-02-01-06-V',$_Parametros.perfil)}
                                        <button class="veranular logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Ver"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos Externos | Ver Registro">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                       &nbsp;&nbsp;&nbsp;
                                   	  {if in_array('CD-01-01-02-01-04-A',$_Parametros.perfil)}
                                            <button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Anular"
                                                    descipcion="El Usuario a Anulado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Documentos de Salida | Anular Registro">
                                                <i class="md md-block" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
									
										&nbsp;&nbsp;&nbsp;
										{elseif $documento.ind_estado=='RE' }
									{if in_array('CD-01-01-02-01-03-M',$_Parametros.perfil)}
                                            <button class="actualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Actualizar"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Documentos de Salida | Editar Registro">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
									 &nbsp;&nbsp;&nbsp;
                                   	  {if in_array('CD-01-01-02-01-06-V',$_Parametros.perfil)}
                                        <button class="veranular logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Ver"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos Externos | Ver Registro">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
           
									 {/if}
									 	{if $documento.ind_estado=='PP'}
										 
										  {if in_array('CD-01-01-02-02-01-N',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Modificar"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Editor de Documentos Salidas">
                                                <i class="md md-description" style="color: #ffffff;"></i>
                                            </button>
                                        {/if} 
																				 
										 {/if} 
										
										
                                    </td>
									
									
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="6">
								<div class="form-group   col-lg-12">
                                    {if in_array('CD-01-01-02-01-02-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado un Documento Externo "  titulo="<i class='icm icm-cog3'></i> Documentos de Salida | Nuevo Registro" id="nuevo" >
                                            <i class="md md-create"></i>&nbsp;Nuevo Documento
                                        </button>
                                    {/if}
									</div>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/salidocumenextCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.actualizar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
		
		 $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/preparardocumenextCONTROL/ModificarMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
		 $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/salidocumenextCONTROL/verMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
		 $('#datatable1 tbody').on( 'click', '.veranular', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/salidocumenextCONTROL/VerAnularMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
	
		 $('#datatable1 tbody').on( 'click', '.anular', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/salidocumenextCONTROL/anularMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
				
				$('#datatable1 tbody').on( 'click', '.documento', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/salidocumenextCONTROL/documentoMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
        
    });
	

	
	
</script>