
<div class="modal-body">
			<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
<form action="{$_Parametros.url}modCD/atenderdocumentoCONTROL/CrearModificarMET" id="formAjax" class="form floating-label form-validation" role="form" novalidate="novalidate">
    
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
			        <div class="form-wizard-nav">
                         <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                            <ul class="nav nav-justified">
								<li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACI&Oacute;N B&Aacute;SICA</span></a></li>
								<li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">INFORMACION GENERAL</span></a></li>
								<li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">DETALLE DE DOCUMENTO</span></a></li>
								<li><a href="#tab4" data-toggle="tab"><span class="step">4</span> <span class="title">ENVIAR A</span></a></li>

                            </ul>
                        </div><!--end .form-wizard-nav -->
                       
					   
			<div class="tab-content clearfix">
				<div class="tab-pane active" id="tab1">
							
			   <div class="row">
			   
			   
					<div class="col-sm-6">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                        <select id="fk_cdc003_num_tipo_documento"  disabled name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control">
                            {if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 2}
                                    <option selected value="2">OFICIO</option>
                                {else}
                                    <option value="2">OFICIO</option>
                            {/if}
                           </select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   				</div>
				
					
				<div class="col-sm-6">
                    <div class="form-group floating-label" id="num_documentoError">
                        <input type="text"  disabled class="form-control" value="{if isset($formDB.num_documento)}{$formDB.num_documento}{/if}" name="form[txt][num_documento]" id="num_documento">
                        <label for="num_documento"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
                </div>
 
			<div class="col-lg-6">
                    <div class="form-group">
                                                        <div class="input-group date">
                                                            <div class="input-group-content">
										<input type="text"  disabled  class="form-control input-sm" name="form[txt][fec_registro]" id="fec_registro" value="{if isset($formDB.fec_registro)}{$formDB.fec_registro}{/if}">
													<label for="fec_registro">Fecha de Registro</label>
															</div>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>

			</div>	
			
			</div>	

				<div class="col-sm-6">
                  <div class="form-group">
                                                        <div class="input-group date" id="fec_documentoError">
                                                            <div class="input-group-content">
															<input type="text"  disabled class="form-control input-sm" name="form[txt][fec_documento]" id="fec_documento" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}">
															
                                                                <label for="fec_documento">Fecha de Documento</label>
															</div>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                    </div>
                </div>
			
			<div class="col-lg-4">
			
			     <center>  <div style="size:6" class="text-primary text-center text-sm-2"><b>Organismo Remitente</b></div></center>
					<hr class="ruler-lg">
                      <div class="form-group">
			<select id="num_org_ext" name="form[alphaNum][num_org_ext]" class="form-control input-sm"  disabled  {if isset($ver)}disabled{/if}  value="{if isset($formDB.DependenciaExt)}{$formDB.DependenciaExt}{/if}">
			
                                                            <option value="">Seleccione el organismo...</option>
                                                            {foreach item=dat from=$lista}
                                                                {if isset($formDB.num_org_ext)}
                                                                    {if $dat.pk_num_ente==$formDB.num_org_ext}
                                                                        <option selected value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
																					
                       
                    </div>
					 
					 <div class="form-group">
                   
                  <select id="num_depend_ext" name="form[alphaNum][num_depend_ext]" disabled  value="{if isset($formDB.DependenciaExt)}{$formDB.DependenciaExt}{/if}"  {if isset($ver)}disabled{/if} class="form-control input-sm">
                                                            <option value="">Seleccione la Dependencia</option>
															 {foreach item=dat from=$centroCosto}
                                                                {if isset($formDB.num_depend_ext)}
                                                                    {if $dat.pk_num_ente==$formDB.num_depend_ext}
                                                                        <option selected value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                {/if}
                                                            {/foreach}
                                        
										</select>
						
		   </div>
		</div>
		
		<div class="col-sm-4">
			<center>
					       <div style="size:8" class="text-primary text-center text-sm-2"><center>&nbsp;
					 <b>Empresa Remitente</b></center></div>
					 
			</center>
					 	<hr class="ruler-lg">
                                        <div class="col-sm-12">
                                
                                        <div class="form-group" id="num_empresaError">
                                            <label for="nomEmpresa"><i class="icm icm-calculate2"></i>
                                                Nombre de empresa</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.nomProveedor)}{$formDB.nomProveedor}{/if}"
                                                   id="nomEmpresa" disabled readonly>

                                        </div>
										
										   <div class="form-group">
                                            <label for="docEmpresa"><i class="icm icm-calculate2"></i>
                                                Doc. Fiscal</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_documento_fiscal)}{$formDB.ind_documento_fiscal}{/if}"
                                                   id="docEmpresa" disabled readonly>
                                        </div>
										
                                    </div>
                                   					
									
									
									 </div>
									
		
		
			    <div class="col-sm-4">
			         <div style="size:8" class="text-primary text-center text-sm-2"><center>
					 <b>Particular Remitente</b></center></div>
					 	<hr class="ruler-lg">
                                        <div class="col-sm-12">
                                
                                        <div class="form-group" id="fk_a003_num_personaError">
                                            <label for="nombreProveedor"><i class="icm icm-calculate2"></i>
                                                Nombre del Particular</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}"
                                                   id="nombreProveedor" disabled readonly>

                                        </div>
										
										   <div class="form-group">
                                            <label for="docProveedor"><i class="icm icm-calculate2"></i>
                                                Doc. Fiscal</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
										
                                    </div>
                                   
									
									 </div>
									 </div>
		 
									 
					  		 </div><!--end #step1 -->
					   

			   				 <!-----------DETALLE DOCUMENTO---------------------------------->		 
    					<div class="tab-pane" id="tab2">
						
										  <div class="col-sm-6">
				    <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Datos de Recepci&oacute;n</b></center></div>
						 	<hr class="ruler-lg">
							
                    <div class="form-group floating-label" id="text_asuntoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.text_asunto)}{$formDB.text_asunto}{/if}" name="form[txt][text_asunto]" id="text_asunto">
                        <label for="text_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>

					<div class="form-group" id="txt_descripcion_asuntoError">
                <textarea name="form[txt][txt_descripcion_asunto]" id="txt_descripcion_asunto" class="form-control" rows="1" disabled>
				{if isset($formDB.txt_descripcion_asunto)}{$formDB.txt_descripcion_asunto}{/if}</textarea>
                <label for="txt_descripcion_asunto">Descripcion Asunto</label>
            </div>
			
			  <div class="col-sm-11">
                                        <div class="form-group" id="ind_persona_recibidoError">
                                            <label for="nombrePersona"><i class="icm icm-calculate2"></i>
                                               Recibido Por</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_recibido)}{$formDB.ind_persona_recibido}{/if}"
                                                   id="nombrePersona" disabled readonly>

                                        </div>
                                    </div>
	
		</div>
		
		<div class="col-sm-6">
				    <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Datos del Mensajero</b></center></div>
						 	<hr class="ruler-lg">
							
				
             <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_nombre_mensajeroError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_nombre_mensajero)}{$formDB.ind_nombre_mensajero}{/if}" name="form[alphaNum][ind_nombre_mensajero]" id="ind_representante">
                        <label for="ind_nombre_mensajero"><i class="md md-assignment-ind"></i>&nbsp;Mensajero</label>
                    </div>
                </div>

			 <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_cedula_mensajeroError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_cedula_mensajero)}{$formDB.ind_cedula_mensajero}{/if}" name="form[alphaNum][ind_cedula_mensajero]" id="ind_cedula_mensajero">
                        <label for="ind_cedula_mensajero"><i class="md md-aspect-ratio"></i>&nbsp;Cedula</label>
                    </div>
                </div>
			 </div>				
				
					
				<div class="col-sm-6">
				    <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Datos del Documento</b></center></div>
						 	<hr class="ruler-lg">
							
            <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_anexo) and $formDB.ind_anexo==1} checked{/if} value="1" name="form[alphaNum][ind_anexo]" disabled>
							
                            <span>Anexo</span>
                        </label>
                    </div>
             
				 <div class="form-group floating-label" id="ind_descripcion_anexoError">
                <textarea name="form[alphaNum][ind_descripcion_anexo]" id="ind_descripcion_anexo" class="form-control input-sm" rows="1"  disabled>{if isset($formDB.ind_descripcion_anexo)}{$formDB.ind_descripcion_anexo}{/if}</textarea>
                <label for="ind_descripcion_anexo">Descripcion Anexo</label>
            </div>
			</div>
			
 </div>			
						
						
						
						 		 </div><!--end #step2 -->


								 <!-----------DETALLE DOCUMENTO---------------------------------->		 
    					<div class="tab-pane" id="tab3">

				<div class="row">
					<div class="col-sm-3">
                    
					<div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_informe_escrito) and $formDB.ind_informe_escrito==1} checked{/if} value="1" name="form[alphaNum][ind_informe_escrito]">
                            <span>Informarme por escrito</span>
                        </label>
                    </div>
  
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_hablar_conmigo) and $formDB.ind_hablar_conmigo==1} checked{/if} value="1" name="form[alphaNum][ind_hablar_conmigo]">
                            <span>Hablar conmigo al respecto</span>
                        </label>
                    </div>
     
	 
	 			 <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_investigar_informar) and $formDB.ind_investigar_informar==1} checked{/if} value="1" name="form[alphaNum][ind_investigar_informar]">
                            <span>Investigar e informar verbalmente</span>
                        </label>
                    </div>
			
                </div>				
							
						
				
				<div class="col-sm-3">
	
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_tramitar_conclusion) and $formDB.ind_tramitar_conclusion==1} checked{/if} value="1" name="form[alphaNum][ind_tramitar_conclusion]">
                            <span>Tramitar hasta su conclusi&oacute;n</span>
                        </label>
                    </div>
     
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_distribuir) and $formDB.ind_distribuir==1} checked{/if} value="1" name="form[alphaNum][ind_distribuir]">
                            <span>Distribuir</span>
                        </label>
                    </div>

                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_conocimiento) and $formDB.ind_conocimiento==1} checked{/if} value="1" name="form[alphaNum][ind_conocimiento]">
                            <span>Para su conocimiento y fines pertinentes</span>
                        </label>
                    </div>
					
                </div>				

				<div class="col-sm-3">
				
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_preparar_constentacion) and $formDB.ind_preparar_constentacion==1} checked{/if} value="1" name="form[alphaNum][ind_preparar_constentacion]">
                            <span>Preparar contestacion para mi firma</span>
                        </label>
                    </div>
  
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_archivar) and $formDB.ind_archivar==1} checked{/if} value="1" name="form[alphaNum][ind_archivar]">
                            <span>Archivar</span>
                        </label>
                    </div>
		
                </div>	
						
					
				<div class="col-sm-3">
                   <div class="checkbox checkbox-styled">
                        <label>
                        <input type="checkbox" {if isset($formDB.ind_conocer_opinion) and $formDB.ind_conocer_opinion==1} checked{/if} value="1" name="form[alphaNum][ind_conocer_opinion]">
                            <span>Para conocer su opinion</span>
                        </label>
                    </div>
  
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_tramitar_caso) and $formDB.ind_tramitar_caso==1} checked{/if} value="1" name="form[alphaNum][ind_tramitar_caso]">
                            <span>Tramitar en caso de proceder</span>
                        </label>
                    </div>
     
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_acusar_recibo) and $formDB.ind_acusar_recibo==1} checked{/if} value="1" name="form[alphaNum][ind_acusar_recibo]">
                            <span>Acusa recibo</span>
                        </label>
                    </div>
			
		   		  </div>	
				  </div>	
				  	
				 <div class="col-sm-2">	
				<div class="form-group floating-label" id="ind_coordinar_conError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_coordinar_con)}{$formDB.ind_coordinar_con}{/if}" name="form[alphaNum][ind_coordinar_con]" 
				  id="ind_coordinar_con">
                        <label for="ind_coordinar_con"><i class="md md-description"></i>Coordinar con:</label>
					 </div>	
					  	</div>	
						
						<div class="col-sm-3">
						<div class="form-group floating-label" id="ind_preparar_memorandumError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_preparar_memorandum)}{$formDB.ind_preparar_memorandum}{/if}" name="form[alphaNum][ind_preparar_memorandum]" 
				  id="ind_preparar_memorandum">
                        <label for="ind_preparar_memorandum"><i class="md md-description"></i>Prepara memo a:</label>
					 </div>	
          					</div>					
							
				
					<div class="col-sm-2">	
				<div class="form-group floating-label" id="ind_registro_deError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_registro_de)}{$formDB.ind_registro_de}{/if}" name="form[alphaNum][ind_registro_de]" 
				  id="ind_registro_de">
                        <label for="ind_registro_de"><i class="md md-description"></i>Registro de:</label>
					 </div>	
					  </div>	
		  
				  <div class="col-sm-2">	
				  <div class="form-group floating-label" id="ind_preparar_oficioError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_preparar_oficio)}{$formDB.ind_preparar_oficio}{/if}" name="form[alphaNum][ind_preparar_oficio]" 
				  id="ind_preparar_oficio">
                        <label for="ind_preparar_oficio"><i class="md md-description"></i>Preparar oficio a:</label>
					 </div>	
					  </div>	
					 
					 
				   <div class="col-sm-3">	
				  	<div class="form-group floating-label" id="ind_tramitar_enError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_tramitar_en)}{$formDB.ind_tramitar_en}{/if}" name="form[int][ind_tramitar_en]" 
				  id="ind_tramitar_en" maxlength="2">
                        <label for="ind_tramitar_en"><i class="md md-description"></i>Tramitar en:  (D&iacute;as)</label>
					 </div>	
					  </div>	
					  
						 
				 				  
				  	<div class="col-lg-12">
	      
	   	<div class="form-group floating-label" id="ind_observacionError">
                <textarea name="form[alphaNum][ind_observacion]"  id="ind_observacion"  class="form-control" rows="2">{if isset($formDB.ind_observacion)}{$formDB.ind_observacion}{/if}</textarea>
			   <label for="lastname">Introduzca Observaciones</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
		</div>
			</div>					
				
				
				
				</div><!--end #step3 -->
				
				 <!-----------DETALLE DOCUMENTO---------------------------------->		 
    		<div class="tab-pane" id="tab4">
			
			  <div class="col-lg-12">
				 <div class="card">
                                   <div class="card-body" style="padding: 4px;">
                                                <div class="table-responsive">
                                                    <table class="table no-margin" id="Dependencia">
                                                        <thead>
                                                        <tr>
                                                            <td>Dependencia</td>
															<td>Representante Legal</td>
															<td>Cargo</td>
															<td>Acciones</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                         {if isset($depextern)}
                                                            {foreach item=i from=$depextern}
                                                                    <tr class="idDependencia{$i.id}">
<input type="hidden" value="{$i.idDependencia}" name="form[alphaNum][ind_dependencia_destinataria][{$i.id}]" class="dependenciaInput"  idDependencia="{$i.idDependencia}" />
<input type="hidden" value="{$i.dependencia}" name="form[alphaNum][ind_desc_dependencia][{$i.id}]" class="descripdepenInput"  dependencia="{$i.dependencia}" />
<input type="hidden" value="{$i.representantedep}" name="form[alphaNum][ind_desc_representante][{$i.id}]" class="represntInput"  representantedep="{$i.representantedep}" />
<input type="hidden" value="{$i.cargodep}" name="form[alphaNum][ind_cargo_destinatario][{$i.id}]" class="cargodepenInput"  cargodep="{$i.cargodep}" />

																   <td>{$i.dependencia}</td>
                                                                    <td>{$i.representantedep}</td>
                                                                    <td>{$i.cargodep}</td>
                                                                    <td> {if isset($ver)} {else}<button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idDependencia{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button> {/if}
																	</td>
                                                                    </tr>
                                                                {/foreach}
                                                            {/if}
															
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="right">
                                                                {if isset($ver)} {else}
                                                                    <button type="button" 
                                                                             class="logsUsuario btn ink-reaction btn-raised btn-info accionModal"
                                                                            id="agregarDependencia"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Dependencias"
                                                                            url="{$_Parametros.url}modCD/dependenciaCONTROL/listaMET/MODAL">
																+ Insertar Nuevo Dep.
                                                                    </button>
					
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
									</div>		
									
					<div class="col-lg-12">
				 <div class="card">
                                   <div class="card-body" style="padding: 4px;">
                                                <div class="table-responsive">
                                                    <table class="table no-margin" id="Particular">
                                                        <thead>
                                                        <tr>
                                                            <td>Cedula</td>
															<td>Nombre y Apellidos</td>
															<td>Dependencia</td>
															<td>Acciones</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                         {if isset($persondest)}
                                                            {foreach item=i from=$persondest}
                                                                    <tr class="idParticular{$i.id}">
										<input type="hidden" value="{$i.idParticular}" name="form[alphaNum][ind_persona_destinataria][{$i.id}]" class="particularInput"  particular="{$i.idParticular}" /> 
										<input type="hidden" value="{$i.cargo}" name="form[alphaNum][ind_cargo_destinatario][{$i.id}]" class="cargookInput"  cargo="{$i.cargo}" />
										<input type="hidden" value="{$i.dependenciaEmp}" name="form[alphaNum][ind_desc_dependencia][{$i.id}]" class="descpedInput"  dependenciaEmp="{$i.dependenciaEmp}" />
										<input type="hidden" value="{$i.particular}" name="form[alphaNum][ind_desc_representante][{$i.id}]" class="empleadonput"  particular="{$i.particular}" />
										
																   <td>{$i.cedula}</td>
																   <td>{$i.particular}</td>
																    <td>{$i.dependenciaEmp}</td>
                                                                   <td> {if isset($ver)} {else}<button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idParticular{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button> {/if}
																	</td>
                                                                    </tr>
                                                                {/foreach}
                                                            {/if}
															
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="right">
                                                                {if isset($ver)} {else}
                                                                    <button type="button" 
                                                                          class="logsUsuario btn ink-reaction btn-raised btn-info accionModal"
                                                                            id="agregarParticular"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Empleado"
                                                                            url="{$_Parametros.url}modCD/particularCONTROL/personaMET/MODAL">
																+ Insertar Nuevo Empl.
                                                                    </button>
					
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
									</div>		
					 
					 <div class="col-sm-12">
					<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  		</div>
               </div>
			   </div>
				 
							
					  	  <div class="modal-footer">
		<button type="button"  title="Cancelar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
        <button type="button"  title="Guardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
		
		
		
		
    </div>		
				
					</div><!--end #step4 -->
					
							
					</div><!--end .tab-content -->
					   <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                                <ul class="pager wizard">
                                    <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>
                                    <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                                </ul>
		
		
        <span class="clearfix"></span>
 
	</div>
  
</form>
	</div><!--end #rootwizard -->
</div><!--end .card-body -->
<script type="text/javascript">


	
    $(document).ready(function() {

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });

		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

		/*   //Script para forzar el avance del wizard por el bot�n suigiente.
        $(".form-wizard-nav").on('click', function () {
            return false;
        });
*/

		 //modales..

        $('#agregarParticular').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });


		  $('#Particular').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		
		 $('#agregarDependencia').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		$('#Dependencia').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		
		
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		
		//var cd = new  ModCdFunciones();
  
       // var idDependencia = '{if isset($formDB.num_org_ext)}{$formDB.num_org_ext}{/if}';
      //  var idCentroCosto = '{if isset($formDB.num_depend_ext)}{$formDB.num_depend_ext}{/if}';
		
	//cd.metJsonCentroCosto('{$_Parametros.url}modCD/atenderdocumentoCONTROL/JsonCentroCostoMET',idDependencia,idCentroCosto);
	
        $('#modalAncho').css("width","80%");
        $('#accion').click(function(){
		 swal({
            title: "�Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                     swal("Registro Modificado!", "El Documento fue Atendido satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/atenderdocumentoCONTROL');
                }
            },'json');
        });
    });
</script>
