<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Atender Documentos Externos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="100">Fecha de registro</th>
                                <th  width="100">Secuencia</th>
                                <th  width="100">N de Documento</th>
								<th  width="210">Asunto</th>
								  <th  width="220">Comentario</th>
								 <th  width="70">Estatus</th>
                                <th  width="70">&nbsp;&nbsp;Atender</th>
								
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.fec_documento}</label></td>
                                    <td><label>{$documento.num_secuencia}</label></td>
                                    <td><label>{$documento.num_documento}</label></td>
									<td><label>{$documento.text_asunto}</label></td>
									 <td><label>{$documento.txt_descripcion_asunto}</label></td>
									 <td><label>Pendiente</label></td>
                                 
                                    <td align="center">
                                        {if in_array('CD-01-01-01-02-01-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Atender Documento"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Documentos Externos | Atender Registro">
                                                <i class="md md-assignment-turned-in" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                     </td>
								 </tr>
                            {/foreach}
                        </tbody>
        
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/atenderdocumentoCONTROL/CrearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
       
    });
</script>