<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Documentos Recibidos Internos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
							<th width="10">Nro</th>
                                <th width="100">Nro de Documento</th>
								<th  width="200">Dependencia</th>
                                <th  width="150">Asunto</th>
								<th  width="60">Estado</th>
								 <th width="100">Documento - Acuse</th>
								
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_distribucion}">
								<td><label>{$documento.pk_num_distribucion}</label></td>
                                    <td><label>{$documento.num_cod_interno}-{$documento.num_secuencia}-{$documento.fec_annio}</label></td>
									<td><label>{$documento.ind_desc_dependencia}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									 <td>
									 <label>{if $documento.ind_estado=='EV'}Enviado{else if $documento.ind_estado=='RE'}Recibido{/if}</label>

									 </td>
									 
									 <td align="center">
										 {if $documento.fk_cdc003_num_tipo_documento=='1' && $documento.ind_con_copia!='2'}
									{if in_array('CD-01-01-02-01-07-I',$_Parametros.perfil)}
                                              <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumCopiaMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_documento}"
                                                        idReq="{$documento.pk_num_documento}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumCopiaMET/">
                                                    <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>
                                        {/if}

									{else if $documento.fk_cdc003_num_tipo_documento=='1' && $documento.ind_con_copia=='2'}
									 {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
									 <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumMET/">
                                                    <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>
									     {/if}


			    				{else if $documento.fk_cdc003_num_tipo_documento=='5'}
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCredencialMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCredencialMET/">
                                                    <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>

                                        {/if}

									{else if $documento.fk_cdc003_num_tipo_documento=='6'}
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCircularMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCircularMET/">
                                                    <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>

                                        {/if}

								 {else}
								{if $documento.fk_cdc003_num_tipo_documento=='3'}
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirPuntoCuentaMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirPuntoCuentaMET/">
                                                   <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>

                                        {/if}
										  {/if}

  		 						 {/if}

										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 {if in_array('CD-01-01-04-01-01-A',$_Parametros.perfil)}
                                             {if $documento.fec_acuse=='' && $documento.ind_estado=='EV' }
                                      <button class="acuseint logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idDocumento="{$documento.pk_num_distribucion}"  boton="si, Acuse"
                                                   title="Dar Acuse" descipcion="El usuario a eliminado un Documento" titulo="Estas Seguro?" mensaje="Estas seguro que desea dar Acuse a un Documento!!">
                                                <i class="md md-assignment-turned-in" style="color: #ffffff;"></i>
                                            </button>

                                        {/if}
                                         {/if}

									 </td>
									 
								  </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript"> 
    $(document).ready(function() {

		  $('#datatable1 tbody').on( 'click', '.acuseint', function () {
            var idDocumento=$(this).attr('idDocumento');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                
				
                var $url='{$_Parametros.url}modCD/documentrecibidosIntCONTROL/AcuseIntMET';
                $.post($url, { idDocumento: idDocumento },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idDocumento'+dato['idDocumento'])).html('');
                        swal("Recibido!", "Acuse satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
		
	 $('#datatable1 tbody').on( 'click', '.memorandum', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/impdocumenintCONTROL/memorandumMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
       
    });
</script>