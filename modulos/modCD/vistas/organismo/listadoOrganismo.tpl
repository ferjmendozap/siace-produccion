<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Organismos - Listado</h2>
    </div>

        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
				
				       <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="6">Check</th>
                            <th width="100">Dependencia</th>
                            <th width="80">Representante Legal</th>
                            <th width="50">Cargo</th>
						</tr>
                        </thead>
                        <tbody>
                        {foreach item=organismo from=$listado}
                            <tr id="idOrganismo{$organismo.pk_num_ente}">
                                <td>
                                   <div class="checkbox checkbox-styled">
                                        <label>
                       <input type="checkbox" class="valores"
                              idOrganismo="{$organismo.pk_num_ente}"
					   organismo="{$organismo.ind_nombre_ente}"
                              representanteor="{$organismo.nombre_apellidos}"
					   cargoor="{$organismo.ind_cargo_personal_externo}">
					   
												   
                                        </label>
                                    </div>
                                </td>
                                <td><label>{$organismo.ind_nombre_ente}</label></td>
                                <td><label>{$organismo.nombre_apellidos}</label></td>
                                <td><label>{$organismo.ind_cargo_personal_externo}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
   		<button type="button"   title="Cancelar"  class="btn btn-default ink-reaction btn-raised" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
		<button type="button"  title="Agregar"  class="btn btn-primary ink-reaction btn-raised" id="agregarOrganismoSeleccionado"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Agregar</button>
		</button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
	  $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#agregarOrganismoSeleccionado').click(function () {
            var input = $('.valores');
            for (i = 0; i < input.length; i++) {
                if (input[i].checked == true) {
                    $(document.getElementsByClassName('idOrganismo' + input[i].getAttribute('idOrganismo'))).remove();
                    $(document.getElementById('Organismo')).append(
                            '<tr class="idOrganismo' + input[i].getAttribute('idOrganismo') + '">' +

                            '<input type="hidden" value="' + input[i].getAttribute('idOrganismo') + '" name="form[alphaNum][ind_organismo_externo][' + input[i].getAttribute('idOrganismo') + ']" class="organismoInput" organismo="' + input[i].getAttribute('idOrganismo') + '" />' +


							'<input type="hidden" value="' + input[i].getAttribute('representanteor') + '" name="form[alphaNum][ind_representante_externo][' + input[i].getAttribute('idOrganismo') + ']" class="representanteInput" representanteor="' + input[i].getAttribute('representanteor') + '" />' +

							'<input type="hidden" value="' + input[i].getAttribute('cargoor') + '" name="form[alphaNum][ind_cargo_personal_externo][' + input[i].getAttribute('idOrganismo') + ']" class="cargoInput" cargoor="' + input[i].getAttribute('cargoor') + '" />' +

							'<td>' + input[i].getAttribute('organismo') + '</td>' +
                            '<td>' + input[i].getAttribute('representanteor') + '</td>' +
							'<td>' + input[i].getAttribute('cargoor') + '</td>' +
                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idOrganismo' + input[i].getAttribute('idOrganismo') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>'
							
											
                    );
                }
            }
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>