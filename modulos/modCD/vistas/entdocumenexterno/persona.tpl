<div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Nro. Documento</th>
                    <th>Dependencia</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=i from=$lista}
                    <tr>
                        <input type="hidden" value="{$i.nombre_apellidos}" class="persona"
                         nombre="{$i.nombre_apellidos}"
                         documento="{$i.ind_cedula_documento}" idPersona="{$i.pk_num_persona}" idDependencia="{$i.ind_dependencia}">
                        <td>{$i.nombre_apellidos}</td>
                        <td>{$i.ind_cedula_documento}</td>
                        <td>{$i.ind_dependencia}</td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script>
/*
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
*/

    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
		
		 $('#datatable2').on('click', 'tbody tr', function () {
//        $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');
            {if $tipoPersona == 'persona'}
                $(document.getElementById('nombrePersona')).val(input.attr('nombre'));
                $(document.getElementById('ind_persona_recibido')).val(input.attr('nombre'));
				
                $('#cerrarModal2').click();
            {/if}
        });
    });
</script>