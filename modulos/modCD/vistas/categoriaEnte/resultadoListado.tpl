<div class="section-body contain-lg">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Categoría</th>
                        <th class="text-center">Tipo ente</th>
                        <th class="text-center">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                        {if $listado|count > 0}
                            {foreach item=fila from=$listado}
                                <tr id="tr_{$fila.pk_num_categoria_ente}">
                                    <td id="td_{$fila.pk_num_categoria_ente}">{$fila.ind_categoria_ente}</td>
                                    <td>{$fila.ind_tipo_ente}</td>
                                    <td align="center">
                                        {if in_array('PF-01-05-02-03-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsCargo btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaCategoria({$fila.pk_num_categoria_ente})"
                                                title="Click para modificar" alt="Click para modifica">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('PF-01-05-02-03-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsCargo btn ink-reaction btn-raised btn-xs btn-danger"
                                                onclick="metEliminaCategoria({$fila.pk_num_categoria_ente})"
                                                title="Click para eliminar" alt="Click para eliminar"">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="3">
                            {if in_array('PF-01-05-02-03-01-N',$_Parametros.perfil)}
                                <a href="#" id="btn_nuevo" class="button logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal"
                                   data-keyboard="false"data-backdrop="static"title="Click para crear una nueva categoría" alt="Click para crear una nueva categoría"
                                   onclick="metCrear()">
                                    <i class="md md-create"></i>&nbsp;Nueva Categoría
                                </a>
                            {/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>