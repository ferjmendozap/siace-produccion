<form action="{$_Parametros.url}modCD/salidocumenextCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
				    <!-- Nav tabs -->
			   <!-- Tab panes -->

			   <div class="row">
					<div class="col-sm-2">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                        <select  id="fk_cdc003_num_tipo_documento"  {if isset($veranular)}disabled{/if}  name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control" >
                            
                            {foreach item=app from=$correspon}
                                {if isset($formDB.fk_cdc003_num_tipo_documento)}
                                    {if $app.pk_num_tipo_documento==$formDB.fk_cdc003_num_tipo_documento}
                                        <option selected value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}                        
						</select>
                           <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   				</div>
					
					<div class="col-sm-2">
                    <div class="form-group floating-label" id="ind_documento_completoError">
                        <input type="text"  {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_documento_completo)}{$formDB.ind_documento_completo}{/if}" name="form[alphaNum][ind_documento_completo]" id="ind_documento_completo">
                        <label for="ind_documento_completo"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
                </div>
 				
								
 				 <div class="col-sm-2">
                    <div class="form-group floating-label" id="ind_plazo_atencionError">
                        <input type="text"  {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_plazo_atencion)}{$formDB.ind_plazo_atencion}{/if}" name="form[alphaNum][ind_plazo_atencion]" id="ind_plazo_atencion">
                        <label for="ind_plazo_atencion"><i class="md md-assignment-ind"></i>&nbsp;Plazo de Atencion</label>
                    </div>
                </div>
				
				<div class="col-lg-3">
                    <div class="form-group floating-label">
                                 <input type="text"  disabled class="form-control" value="{if isset($formDB.fec_registro)}{$formDB.fec_registro}{/if}" name="form[alphaNum][fec_registro]" >
					          <label for="fec_registro">Fecha Registro</label>
                    </div>	
					 </div>	

				<div class="col-sm-3">
                    <div class="form-group floating-label" id="fec_documentoError">
                              <input type="text"  {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}" name="form[alphaNum][fec_documento]" >
					          <label for="fec_documento">Fecha Documento</label>

                    </div>	
                </div>
				
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_asuntoError">
                        <input type="text" {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}" name="form[alphaNum][ind_asunto]" id="ind_asunto">
                        <label for="ind_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>
				</div>
				
				<div class="col-lg-8">
					<div class="form-group floating-label" id="ind_descripcionError">
                  <input type="text" {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[alphaNum][ind_descripcion]" id="ind_descripcion">
                        <label for="ind_descripcion"><i class="md md-description"></i>&nbsp;Descripcion Asunto</label>
                    </div>
				</div>	

			<div class="col-sm-12">
			<div   align="center" style="background:#04B4AE; font-size:9px">&nbsp;&nbsp;<font color="#FFFFFF"><b>REMITENTE</b></font></div>	 
			<div class="col-lg-3">
			 <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo"   {if isset($veranular)}disabled{/if}  name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo Remitente</label>
                    </div>
					</div> 
					
								 <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="docProveedor"><i class="md md-contacts"></i>
                                             Cargo Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
                                    </div>
									
									 <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="nombreProveedor"><i class="md md-group"></i>
                                              Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}"
                                                   id="nombreProveedor" disabled readonly>
                                        </div>
                                    </div>
														
              						 <div class="col-sm-4">
                                        <div class="form-group" id="ind_dependenciaError">
                                            <label for="dependencia"><i class="icm icm-calculate2"></i>
                                                Dependencia Remitente</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}"
                                                   id="dependencia" disabled readonly>

                                        </div>
                                    </div>
                                   
		   
				  </div>
							
				  
				<div class="col-sm-12">
					<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  		</div>
               </div>
				 </div>
				 
						  
						  </div>
		

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
    </div>
</form>

<script type="text/javascript">
/*
		function habilitar(value)
		{
			if(value=="agregarParticular")
			{
				// habilitamos
					document.getElementById("agregarParticular").disabled=true;
				document.getElementById("num_org_ext").disabled=false;
			}
			
		}
		
		*/
		
    $(document).ready(function() {
	/*var app = new  AppFunciones();
    var idDocumento = '{if isset($formDB.num_depend_ext)}{$formDB.num_depend_ext}{/if}';
    app.metJsonDependencia('{$_Parametros.url}dependencia/JsonDependencia',idDocumento);
	*/
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
	
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		//modales..
			$('#agregarOrganismo').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		    
		 $('#agregarDependencia').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		    $('#agregarParticular').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		$('#Organismo').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		$('#Dependencia').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		  $('#tipoparticular').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		
		
				
	
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
      //  $('#fec_anulado').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","90%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='actualizar'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
               // if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                $(document.getElementById('idDocumento'+dato['idDocumento'])).html('<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['fk_a004_num_dependencia']+'</td>' +
                            '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-04-02-01-V',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Modificar Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '</td>');
                    swal("Registro Modificado!", "El Documento fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/documentenviadosCONTROL');
                }else if(dato['status']=='nuevo'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    //if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idDocumento'+dato['idDocumento']+'">' +
                            '<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['fk_a004_num_dependencia']+'</td>' +
                             '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-04-02-01-V',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Modificar Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '</td>');
                    swal("Registro exitoso!", "El Documento fue registrado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/documentenviadosCONTROL');
                }
            },'json');
        });
    });
</script>
