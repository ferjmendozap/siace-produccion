<!--********************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Reporte de entrada de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************
 -->
<div class="card">

    <div class="card-head">
        <header> <h2 class="text-primary"> Reporte | Lista Documentos Salida</h2></header>
    </div>

	
        <form   class="form" action="{$_Parametros.url}modCD/reporteCONTROL/distribucionSalidaextReportePdfMET"   method="post" target="iReporte">
            <div class="col-lg-5">
			 <div class="form-group">   
                         <select  id="ind_organismo_externo"  name="form[int][ind_organismo_externo]" class="form-control" >
                            <option value=""> </option>
                            {foreach item=app from=$_TipoOrganismo}
                                {if isset($formDB.num_org_ext)}
                                    {if $app.pk_num_ente==$formDB.num_org_ext}
                                        <option selected value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                        {else}
                                        <option value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="ind_organismo_externo"><i class="md md-event"></i>&nbsp;Destinatario</label>
 						</div>
					 
					  <div class="form-group">   
                       <select id="ind_dependencia_remitente" name="form[int][ind_dependencia_remitente]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$_TipoDependencia}
                                {if isset($formDB.ind_dependencia_remitente)}
                                    {if $app.pk_num_dependencia==$formDB.ind_dependencia_remitente}
                                        <option selected value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                        {else}
                                        <option value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
						 <label for="pk_num_dependencia"><i class="md md-map"></i>Elaborado Por</label>
					</div>	
			</div>
			
					<div class="col-lg-3">
		<div class="form-group">
				<select  id="ind_estado" name="form[txt][ind_estado]" class="form-control" >
						    <option value="">&nbsp;</option>
						   
						   {if isset($formDB.ind_estado) and $formDB.ind_estado == PR}
						   <option selected value="PR">Preparacion</option>
							  {else}
							  <option value="PR">Preparacion</option>
							    {/if}
							  {if isset($formDB.ind_estado) and $formDB.ind_estado == PP}
                            <option selected value="PP">Preparado</option>
							  {else}
							  <option value="PP">Preparado </option>
							      {/if}
								
								  {if isset($formDB.ind_estado) and $formDB.ind_estado == EV}
						   <option selected value="EV">Enviado</option>
							  {else}
							  <option value="EV">Enviado</option>
							    {/if}
							  {if isset($formDB.ind_estado) and $formDB.ind_estado == RE}
                            <option selected value="RE">Recibido</option>
							  {else}
							  <option value="RE">Recibido</option>
							    {/if}
								
								  {if isset($formDB.ind_estado) and $formDB.ind_estado == AN}
                            <option selected value="AN">Anulado</option>
							  {else}
							  <option value="AN">Anulado</option>
							    {/if}
						
							
						</select>
					   
                        <label for="ind_estado"><i class="md md-event"></i>&nbsp;Estado</label>
						</div>
						
						
					<div class="form-group">
							  <select id="fk_cdc003_num_tipo_documento"  name="form[txt][fk_cdc003_num_tipo_documento]" class="form-control">
							    <option value="">&nbsp;</option>	
                            {if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 2}
                                    <option selected value="2">OFICIO</option>
                                {else}
                                    <option value="2">OFICIO</option>
                            {/if}
							  {if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 4}
                                    <option selected value="4">OFICIO CIRCULAR</option>
                                {else}
                                    <option value="4">OFICIO CIRCULAR</option>
                            {/if}
                           </select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
					</div>
					 </div>
					 	
						
						 <div class="form-group col-lg-4">
                    <div id="fec_documento" class="input-daterange input-group">
					<span class="input-group-addon">Desde</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text" id="desde" name="form[txt][desde]" >
                            <label>Fecha  Registro</label>
                        </div>

					
                        <span class="input-group-addon">Hasta</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text"  id="hasta"  name="form[txt][hasta]">
                            <div class="form-control-line"></div>
                        </div>
						
                    </div>
     </div>

		<div class="form-group   col-lg-12"  align="center">
   			<button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:-20px">Buscar</button>
		</div>  


	    </form>
</div><!--end .card -->

<div class="card">
 <div align="center" style="width:100%; background:#66CCCC">&nbsp;&nbsp;<font color="#FFFFFF"><b>Listado de Documentos</b></font></div>

<div id="contenidoReporteGeneral">
          <center><iframe name="iReporte" id="iReporte" style="border:solid 1px #CDCDCD; width:1000px; height:400px;"></iframe></center>
</div>
 </div>			

    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
           	 //$('#fechas').datepicker({ format: 'dd/mm/yyyy' });
		$("#fec_documento").datepicker({
        format:'yyyy-mm-dd',
        language:'es',
        autoclose: true
    });
	
        });
    </script>


