<!--********************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Reporte de entrada de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************
 -->
<div class="card">

    <div class="card-head">
        <header> <h2 class="text-primary"> Reporte | Lista Documentos Entrada</h2></header>
    </div>

	
        <form   class="form" action="{$_Parametros.url}modCD/reporteCONTROL/entdocumentextReportePdfMET"   method="post" target="iReporte">
            <div class="col-lg-5">
			 <div class="form-group">   
                         <select  id="num_org_ext"  name="form[int][num_org_ext]" class="form-control" >
                            <option value=""> </option>
                            {foreach item=app from=$_TipoOrganismo}
                                {if isset($formDB.num_org_ext)}
                                    {if $app.pk_num_ente==$formDB.num_org_ext}
                                        <option selected value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                        {else}
                                        <option value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="num_org_ext"><i class="md md-event"></i>&nbsp;Organismo</label>
 						</div>
					 
					 <div class="form-group">   
                       <select id="num_depend_ext" name="form[int][num_depend_ext]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$_TipoDependencia}
                                {if isset($formDB.num_depend_ext)}
                                    {if $app.pk_num_ente==$formDB.num_depend_ext}
                                        <option selected value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                        {else}
                                        <option value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_ente}">{$app.ind_nombre_ente}</option>
                                {/if}
                            {/foreach}
                        </select>
						 <label for="num_depend_ext"><i class="md md-map"></i>Dependencia</label>
            </div>
			</div>
			
					<div class="col-lg-3">
		<div class="form-group">
				<select  id="ind_estado" name="form[txt][ind_estado]" class="form-control" >
						 <option selected value="">&nbsp;</option>
						   {if isset($formDB.ind_estado) and $formDB.ind_estado == PE}
						   
						   <option selected value="PE">Pendiente</option>
							  {else}
							  <option value="PE">Pendiente</option>
							    {/if}
							  {if isset($formDB.ind_estado) and $formDB.ind_estado == RE}
                            <option selected value="RE">Recibido</option>
							  {else}
							  <option value="RE">Recibido</option>
							    {/if}
								
								  {if isset($formDB.ind_estado) and $formDB.ind_estado == CO}
                            <option selected value="CO">Completado</option>
							  {else}
							  <option value="CO">Completado</option>
							    {/if}
								</select>
					   
                        <label for="ind_estado"><i class="md md-event"></i>&nbsp;Estado</label>
						</div>
						
						
					<div class="form-group">
							  <select id="fk_cdc003_num_tipo_documento"  name="form[txt][fk_cdc003_num_tipo_documento]" class="form-control">
                         	<option value="">&nbsp;</option>
                              {foreach item=app from=$corresp}
                                  <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                              {/foreach}
                           </select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
					</div>
					 </div>
					 	


						 <div class="form-group col-lg-4">

                   <div id="fec_documento" class="input-daterange input-group">
					<span class="input-group-addon">Desde</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text" id="desde" name="form[txt][desde]" >
                            <label>Fecha a Documento</label>
                        </div>

					
                        <span class="input-group-addon">Hasta</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text"  id="hasta"  name="form[txt][hasta]">
                            <div class="form-control-line"></div>
                        </div>
						
                    </div>
		
                </div>
				
	<div class="form-group   col-lg-12"  align="center">
   <button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:-20px">Buscar</button>
			</div>  



	    </form>
</div><!--end .card -->

<div class="card">
 <div align="center" style="width:100%; background:#66CCCC">&nbsp;&nbsp;<font color="#FFFFFF"><b>Listado de Documentos</b></font></div>

<div id="contenidoReporteGeneral">
          <center><iframe name="iReporte" id="iReporte" style="border:solid 1px #CDCDCD; width:1000px; height:400px;"></iframe></center>
</div>
 </div>			

    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
           	 //$('#fechas').datepicker({ format: 'dd/mm/yyyy' });
		//	$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
      
	   $("#fec_documento").datepicker({
        format:'yyyy-mm-dd',
        language:'es',
        autoclose: true
    });
		
	  
	    });
		
	
		
    </script>


