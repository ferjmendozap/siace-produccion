<!--********************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Reporte de entrada de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************
 -->
<div class="card">

    <div class="card-head">
        <header> <h2 class="text-primary"> Reporte | Distribuci&oacute;n </h2></header>
    </div>

	
        <form   class="form" action="{$_Parametros.url}modCD/reporteCONTROL/distribucionextReportePdfMET"   method="post" target="iReporte">
            <div class="col-lg-4">
                      <div class="form-group">   
                       <select id="ind_dependencia_destinataria" name="form[int][ind_dependencia_destinataria]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$_TipoDependencia}
                                {if isset($formDB.ind_dependencia_destinataria)}
                                    {if $app.pk_num_dependencia==$formDB.ind_dependencia_destinataria}
                                        <option selected value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                        {else}
                                        <option value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
						 <label for="pk_num_dependencia"><i class="md md-map"></i>Dependencia</label>
					</div>	
						 <div class="form-group floating-label">
                        <input type="text" class="form-control" name="form[txt][num_documento]" id="num_documento">
                        <label for="num_documento"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
						 
						 
            </div> 
			<div class="col-lg-4">
                      <div class="form-group"> 
				<select  id="ind_estado" name="form[txt][ind_estado]" class="form-control" >
						  <option value="">&nbsp;</option>
						   {if isset($formDB.ind_estado) and $formDB.ind_estado == EV}
						   <option selected value="EV">Enviado</option>
							  {else}
							  <option value="EV">Enviado</option>
							    {/if}
							  {if isset($formDB.ind_estado) and $formDB.ind_estado == RE}
                            <option selected value="RE">Recibido</option>
							  {else}
							  <option value="RE">Recibido</option>
							    {/if}
							</select>
					 
                        <label for="ind_estado"><i class="md md-event"></i>&nbsp;Estado</label>
					</div>  
						
						<div class="form-group">
							  <select id="fk_cdc003_num_tipo_documento"  name="form[txt][fk_cdc003_num_tipo_documento]" class="form-control">
                                              <option value="">&nbsp;</option>
			                {if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 2}

								    <option selected value="2">OFICIO</option>
                                {else}
                                    <option value="2">OFICIO</option>
                            {/if}
                           </select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
						</div>

					 </div>
					 	
						
						 <div class="form-group col-lg-4">

                    <div id="fec_documento" class="input-daterange input-group">
					<span class="input-group-addon">Desde</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text" id="desde" name="form[txt][desde]" >
                            <label>Fecha de Env&iacute;o</label>
                        </div>

					
                        <span class="input-group-addon">Hasta</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text"  id="hasta"  name="form[txt][hasta]">
                            <div class="form-control-line"></div>
                        </div>
						
                    </div>
					
	    
                </div>
				
				<div class="form-group   col-lg-12"  align="center">
   <button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:-20px">Buscar</button>
			</div>  



	    </form>
</div><!--end .card -->

<div class="card">
 <div align="center" style="width:100%; background:#66CCCC">&nbsp;&nbsp;<font color="#FFFFFF"><b>Listado de Documentos</b></font></div>

<div id="contenidoReporteGeneral">
          <center><iframe name="iReporte" id="iReporte" style="border:solid 1px #CDCDCD; width:1000px; height:400px;"></iframe></center>
</div>
 </div>			

    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
			
			
			 $("#fec_documento").datepicker({
        format:'yyyy-mm-dd',
        language:'es',
        autoclose: true
    });
	
           	 //$('#fechas').datepicker({ format: 'dd/mm/yyyy' });

        });
    </script>


