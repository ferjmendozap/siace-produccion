<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Asignar Responsables</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cedula</th>
                                <th>Doc. Fiscal</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Estatus</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=Responsable from=$listadoResponsables}
                                <tr id="idResponsable{$Responsable.idResponsable}">
                                    <td><label>{$Responsable.cedula}</label></td>
                                    <td><label>{$Responsable.rif}</label></td>
                                    <td><label>{$Responsable.nombres}</label></td>
                                    <td><label>{$Responsable.apellidos}</label></td>
                                    <td>
                                        <i class="{if $Responsable.estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('CD-01-03-01-02-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idResponsable="{$Responsable.idResponsable}"
                                                    descipcion="El Usuario a Modificado un Responsable" titulo="Modificar Responsable">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
		
										
                                        &nbsp;&nbsp;
                                        {if in_array('CD-01-03-01-02-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idResponsable="{$Responsable.idResponsable}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un Responsable" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Responsable!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="6"> 
                                    {if in_array('CD-01-03-01-02-01-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado un Nuevo Responsable"  titulo="<i class='icm icm-cog3'></i> Crear Responsable" id="nuevo" >
                                            Nuevo Responsable &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modCD/entesCONTROL/crearModPersonaMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idResponsable:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idResponsable: $(this).attr('idResponsable')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idResponsable=$(this).attr('idResponsable');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCD/entesCONTROL/eliminarResponsableMET';
                $.post($url, { idResponsable: idResponsable },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idResponsable'+dato['idResponsable'])).html('');
                        swal("Eliminado!", "el Responsable fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>