<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Salida de Documentos Externos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                   <th width="80">Nro de Documento</th>
								  <th  width="170">Remitente</th>
                                <th  width="90">Asunto</th>
								<th  width="200">Comentario</th>
								 <th  width="40">Estado</th>
                                <th align="left" width="60">Imprimir</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.ind_cod_completo}</label></td>
                                    <td><label>{$documento.ind_dependencia}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									 <td><label>{$documento.txt_descripcion} </label></td>
									 <td>
									 <label>{if $documento.ind_estado=='AN'}Anulado{else if $documento.ind_estado=='PR'}Preparacion{else if $documento.ind_estado=='EV'}Enviado{else if $documento.ind_estado=='PE'}Pendiente{else if     

									 $documento.ind_estado=='RE'}Recibido{/if}</label>
									 
									 </td>
                                   
                                    <td align="left">
																	
										 {if $documento.ind_estado!='PR'}
										 {if in_array('CD-01-01-02-01-07-I',$_Parametros.perfil)}
                                          <a href="{$_Parametros.url}modCD/enviardocumentextCONTROL/ImprimirOficioMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentextCONTROL/ImprimirOficioMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>
                                        {/if}
										 &nbsp;&nbsp;&nbsp;
										{/if} 
                                    </td>
									
									
                                </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/salidocumenextCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

    
				$('#datatable1 tbody').on( 'click', '.documento', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/salidocumenextCONTROL/documentoMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
        
    });
	

	
	
</script>