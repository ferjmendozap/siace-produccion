<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Distribuci&oacute;n | Documentos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="100">Fecha de Enviado</th>
                                <th  width="100">Secuencia</th>
                                <th  width="80">N de Documento</th>
								<th  width="100">Tipo Documento</th>
								  <th  width="220">Dependencia</th>
								  <th  width="180">Empleado</th>
								 <th  width="50">Estado</th>

                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.fec_envio}</label></td>
                                    <td><label>{$documento.num_secuencia}</label></td>
                                    <td><label>{$documento.num_documento}</label></td>
									<td><label>{$documento.ind_descripcion} </label></td>
									 <td><label>{if $documento.ind_dependencia_destinataria!=''} {$documento.depres} {else}{$documento.depdes}{/if}</label></td>
									  <td><label>
									  {if $documento.ind_persona_destinataria!=''} {$documento.PersonaDest} {else}{$documento.ResponsableDep}{/if}
									  </label></td>
									 <td>
									 <label>{if $documento.ind_estado=='EV'}Enviado{/if}</label>
									 
									 </td>
         
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/distribdocumenextCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idDocumento=$(this).attr('idDocumento');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                
                var $url='{$_Parametros.url}modCD/entdocumenexternoCONTROL/eliminarMET';
                $.post($url, { idDocumento: idDocumento },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idDocumento'+dato['idDocumento'])).html('');
                        swal("Eliminado!", "el Proceso fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>