<form action="{$_Parametros.url}modCD/particularCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idParticular) }
            <input type="hidden" value="{$idParticular}" name="idParticular"/>
        {/if}
        <div class="row">
                
				
                <div class="col-lg-3">
                    <div class="form-group floating-label" id="ind_nombre1Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}" name="form[txt][ind_nombre1]" id="ind_nombre1" >
                        <label for="ind_nombre1"><i class="md md-account-child"></i>&nbsp;Primer Nombre </label>
                    </div>
                </div>
				  <div class="col-lg-3">
                    <div class="form-group floating-label" id="ind_nombre2Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre2)}{$formDB.ind_nombre2}{/if}" name="form[txt][ind_nombre2]" id="ind_nombre2" >
                        <label for="ind_nombre2"><i class="md md-account-child"></i>&nbsp;Segundo Nombre </label>
                    </div>
                </div>
				
				 <div class="col-lg-3">
                    <div class="form-group floating-label" id="ind_apellido1Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}" name="form[txt][ind_apellido1]" id="ind_apellido1" >
                        <label for="ind_apellido1"><i class="md md-account-child"></i>&nbsp; Primer Apellido </label>
                    </div>
                </div>
				<div class="col-lg-3">
                    <div class="form-group floating-label" id="ind_apellido2Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_apellido2)}{$formDB.ind_apellido2}{/if}" name="form[txt][ind_apellido2]" id="ind_apellido2" >
                        <label for="ind_apellido2"><i class="md md-account-child"></i>&nbsp;Segundo Apellido </label>
                    </div>
                </div>
				
				<div class="col-lg-2">
                    <div class="form-group floating-label" id="ind_cedula_documentoError">
                        <input type="text" class="form-control"  data-inputmask="'mask': '99999999'"  value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" name="form[int][ind_cedula_documento]" id="ind_cedula_documento" maxlength="8">
                        <label for="ind_cedula_documento"><i class="md md-payment"></i>&nbsp;Cedula</label>
                    </div>
                </div>
              
			  <div class="col-lg-2">
					 <div class="form-group floating-label" id="ind_tipo_personaError">
                   
                  <select id="ind_tipo_persona" name="form[txt][ind_tipo_persona]" class="form-control">
                       
                            {foreach item=app from=$miscelaneop}
                                {if isset($formDB.ind_tipo_persona)}
                                    {if $app.pk_num_miscelaneo_detalle==$formDB.ind_tipo_persona}
                                        <option selected value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                        {else}
                                        <option value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
						 <label for="ind_tipo_persona"><i class="md md-assignment-ind"></i>&nbsp;Persona</label>
				
                </div>
				
             </div>

                <div class="col-lg-4">
                    <div class="form-group floating-label" id="ind_emailError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_email)}{$formDB.ind_email}{/if}" name="form[formula][ind_email]" id="ind_email">
                        <label for="ind_email"><i class="md md-email"></i>&nbsp;Email</label>
                    </div>
                </div>
			
			 <div class="col-lg-4">
					 <div class="form-group floating-label" id="fk_a006_num_miscelaneo_det_tipopersonaError">
                   
                  <select id="fk_a006_num_miscelaneo_det_tipopersona" name="form[txt][fk_a006_num_miscelaneo_det_tipopersona]" class="form-control">
                       
                            {foreach item=app from=$miscelaneo}
                                {if isset($formDB.fk_a006_num_miscelaneo_det_tipopersona)}
                                    {if $app.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_det_tipopersona}
                                        <option selected value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                        {else}
                                        <option value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
						 <label for="fk_a006_num_miscelaneo_det_tipopersona"><i class="md md-assignment-ind"></i>&nbsp;Tipo de Persona</label>
				
                </div>				
             </div>
			 
			 	
				<div class="col-md-3">
                                                     <div class="form-group" id="fk_a008_num_paisError">
                                                        <select id="fk_a008_num_pais"  name="form[txt][fk_a008_num_pais]"  class="form-control input-sm" required data-msg-required="Seleccione Pa�s">
                                                           
                                                            {foreach item=i from=$listadoPais}
                                                                {if isset($formDB.fk_a008_num_pais)}
                                                                    {if $i.pk_num_pais==$formDB.fk_a008_num_pais}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_pais==$DefaultPais}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a008_num_pais"><i class="md md-map"></i> Pais</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                  <div class="form-group" id="fk_a009_num_estadoError">
                                                        <select id="fk_a009_num_estado" name="form[txt][fk_a009_num_estado]"   class="form-control input-sm" required data-msg-required="Seleccione Estado">
                                                																 <option selected value="">Seleccione el estado</option>
                                                            {foreach item=i from=$listadoEstado}
                                                                {if isset($formDB.fk_a009_num_estado)}
                                                                    {if $i.pk_num_estado==$formDB.fk_a009_num_estado}
                                                                        <option  value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {/if}
                                                                {else}
                                                                   <option value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                               {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a009_num_estado"><i class="md md-map"></i> Estado</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group" id="fk_a011_num_municipioError">
                                                        <select id="fk_a011_num_municipio" name="form[txt][fk_a011_num_municipio]"  class="form-control input-sm" required data-msg-required="Seleccione Municipio">
                                                     	 <option selected value="">Seleccione el municipio</option>
                                                            {foreach item=i from=$listadoMunicipio}
                                                                {if isset($formDB.fk_a011_num_municipio)}
                                                                    {if $i.pk_num_municipio==$formDB.fk_a011_num_municipio}
                                                                        <option selected value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {/if}
                                                                {else}
                                                                   <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a011_num_municipio"><i class="md md-map"></i> Municipio</label>
														<p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                       

                                                <div class="col-md-3">
                                                   <div class="form-group" id="fk_a010_num_ciudadError">
                                                        <select id="fk_a010_num_ciudad" name="form[txt][fk_a010_num_ciudad]"  class="form-control input-sm" required data-msg-required="Seleccione Ciudad">
                                                      	 <option selected value="">Seleccione la ciudad</option>
                                                            {foreach item=i from=$listadoCiudad}
                                                                {if isset($formDB.fk_a010_num_ciudad)}
                                                                    {if $i.pk_num_ciudad==$formDB.fk_a010_num_ciudad}
                                                                        <option  value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {/if}
                                                               		 {else}
                                                                	<option value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                {/if}
															 {/foreach}
                                                        </select>
                                                        <label for="fk_a010_num_ciudad"><i class="md md-map"></i> Ciudad</label>
														<p class="help-block"><span class="text-xs" style="color: red">*</span></p>

                                                    </div>
                                                </div>
												
				
				
				   
			 
			 
			 <div class="col-lg-2">
			 <div class="checkbox checkbox-styled">
                    <label>
						 <input type="checkbox" {if (isset($formDB.num_estatus) and $formDB.num_estatus==1) OR !isset($idParticular) } checked{/if} value="1" name="form[int][num_estatus]">
                        <span>Estatus</span>
                    </label>
                </div>
         </div>
        </div>

                

            
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button"   title="Cancelar"  class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
        <button type="button"  title="Guardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
	
 		var cd = new  ModCdFunciones();
		
		var idPais   = '{if isset($formDB.pk_num_pais)}{$formDB.pk_num_pais}{/if}';
        var idEstado = '{if isset($formDB.pk_num_estado)}{$formDB.pk_num_estado}{/if}';
        var idCiudad = '{if isset($formDB.pk_num_ciudad)}{$formDB.pk_num_ciudad}{/if}';
        var idMunicipio = '{if isset($formDB.pk_num_municipio)}{$formDB.pk_num_municipio}{/if}';
		
		cd.metJsonEstadoD('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
		//rh.metJsonEstado('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
        cd.metJsonMunicipioN('{$_Parametros.url}municipio/JsonMunicipio',idEstado,idMunicipio);
        cd.metJsonCiudadN('{$_Parametros.url}ciudad/JsonCiudad',idEstado,idCiudad);
		
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		$(":input").inputmask();
		
        $('#modalAncho').css("width","75%");
        $('#accion').click(function(){
		 swal({
            title: "�Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
					
					   if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idParticular'+dato['idParticular'])).html('<td>'+dato['ind_cedula_documento']+'</td>' +
                            '<td>'+dato['ind_nombre1']+'</td>' +
							'<td>'+dato['ind_nombre2']+'</td>' +
							'<td>'+dato['ind_apellido2']+'</td>' +
							'<td>'+dato['ind_apellido1']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +


                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-04-02-M',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idParticular="'+dato['idParticular']+'"' +
                            'descipcion="El Usuario a Modificado un Particular" titulo="Modificar Particular">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('CD-01-03-01-04-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idParticular="'+dato['idParticular']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Particular" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar un Particular!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Particular fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/particularCONTROL');
                }else if(dato['status']=='nuevo'){

                      if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idParticular'+dato['ind_cedula_documento']+'">' +
                            '<td>'+dato['ind_nombre1']+'</td>' +
							'<td>'+dato['ind_nombre2']+'</td>' +
							'<td>'+dato['ind_apellido2']+'</td>' +
							'<td>'+dato['ind_apellido1']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-04-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idParticular="'+dato['idParticular']+'"' +
                            'descipcion="El Usuario a Modificado un Particular de Personas" titulo="Modificar Particular">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
												
                            '{if in_array('CD-01-03-01-04-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idParticular="'+dato['idParticular']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Particular" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Particular!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Particular fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/particularCONTROL');
                }
            },'json');
        });
    });
</script>