<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'almacen' . DS . 'ejecutarTranModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
class despachoModelo extends Modelo
{
    private $atIdUsuario;
    private $atIdEmpleado;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atEjecutarTranModelo = new ejecutarTranModelo();
        $this->atOrdenCompraModelo = new ordenCompraModelo();
    }

    public function metBuscarDetalle($idReqDet)
    {
        $tranPost = $this->_db->query("
            SELECT
              sum(detalle.num_cantidad_transaccion) AS cantidad_total,
              detalle2.num_cantidad_pedida
            FROM
              lg_d002_transaccion_detalle AS detalle
              INNER JOIN lg_c001_requerimiento_detalle AS detalle2 ON detalle.fk_lgc001_num_requerimiento_detalle = detalle2.pk_num_requerimiento_detalle
              WHERE detalle.fk_lgc001_num_requerimiento_detalle = '$idReqDet'
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetch();
    }

    public function metActualizarEstadoDet($idReqDet,$estado)
    {
        $this->_db->beginTransaction();

        $NuevoPost=$this->_db->prepare("
        UPDATE
            lg_c001_requerimiento_detalle
        SET
            ind_estado='$estado',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_requerimiento_detalle=:pk_num_requerimiento_detalle
        ");
        $NuevoPost->execute(array(
            'pk_num_requerimiento_detalle'=>$idReqDet
        ));
        $fallaTansaccion = $NuevoPost->errorInfo();


        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idReqDet;
        }
    }

    public function metBuscarClasificacion(){
        $persona = $this->_db->query("
            SELECT
              *
            FROM lg_b017_clasificacion
            WHERE
            ind_cod_clasificacion = 'RAU'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metNuevoRequerimiento($datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost1=$this->_db->prepare(
            "INSERT INTO lg_b001_requerimiento
              SET
              cod_requerimiento=:cod_requerimiento,
              fk_a004_num_dependencia=:fk_a004_num_dependencia,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
              fk_a006_num_miscelaneos_prioridad=:fk_a006_num_miscelaneos_prioridad,
              fk_rhb001_num_empleado_preparado_por='$this->atIdEmpleado',
              fec_preparacion=NOW(),
              fec_requerida=NOW(),
              ind_comentarios=:ind_comentarios,
              ind_razon_rechazo=:ind_razon_rechazo,
              num_flag_caja_chica=:num_flag_caja_chica,
              ind_estado=:ind_estado,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              ind_verificacion_caja_chica=:ind_verificacion_caja_chica,
              ind_verificacion_presupuestaria=:ind_verificacion_presupuestaria,
              num_flag_aprobacion_despacho=:num_flag_aprobacion_despacho,
              fec_anio=:fec_anio,
              fec_mes=:fec_mes,
              fk_lgb022_num_proveedor_sugerido=:fk_lgb022_num_proveedor_sugerido,
              fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion,
              fk_rhb001_num_empleado_aprueba_despacho='$this->atIdEmpleado'
              ");
        #execute — Ejecuta una sentencia preparada
        $NuevoPost1->execute(array(
            'cod_requerimiento'=> $datos['codigo'],
            'fk_a004_num_dependencia'=> $datos['dependencia'],
            'fk_a023_num_centro_costo'=> $datos['centroCosto'],
            'fk_lgb014_num_almacen'=> $datos['almacen'],
            'fk_a006_num_miscelaneos_prioridad'=> $datos['prioridad'],
            'ind_comentarios'=> $datos['comentarioR'],
            'ind_razon_rechazo'=> '',
            'num_flag_caja_chica'=> 0,
            'ind_estado'=> 'PR',
            'ind_verificacion_caja_chica'=> 0,
            'ind_verificacion_presupuestaria'=> 0,
            'num_flag_aprobacion_despacho'=>0,
            'fec_anio'=>date('Y'),
            'fec_mes'=>date('m'),
            'fk_lgb022_num_proveedor_sugerido'=> $datos['proveedor'],
            'fk_lgb017_num_clasificacion'=> $datos['clasificacion']
        ));
        $numReq=$this->_db->lastInsertId();

        $NuevoPost2=$this->_db->prepare("
            INSERT INTO
                lg_c001_requerimiento_detalle
            SET
                num_secuencia=:num_secuencia,
                num_cantidad_pedida=:num_cantidad_pedida,
                num_flag_exonerado=:num_flag_exonerado,
                ind_comentarios=:ind_comentarios,
                ind_estado=:ind_estado,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_lgb002_num_item=:fk_lgb002_num_item,
                fk_lgb001_num_requerimiento=:fk_lgb001_num_requerimiento,
                fk_lgb003_num_commodity=:fk_lgb003_num_commodity,
                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                fk_lgc003_num_cotizacion=:fk_lgc003_num_cotizacion,
                fk_lgb004_num_unidad=:fk_lgb004_num_unidad
            ");
        $cont = 0;
        foreach ($datos['detalles'] as $key=>$value) {
            $cont = $cont+1;
            #execute — Ejecuta una sentencia preparada
            $NuevoPost2->execute(array(
                'num_secuencia' => $cont,
                'num_cantidad_pedida' => $value['cantidad'],
                'num_flag_exonerado' => 0,
                'ind_comentarios' => $value['comentarioI'],
                'ind_estado' => 'PR',
                'fk_lgb002_num_item' => $value['numItem'],
                'fk_lgb001_num_requerimiento' => $numReq,
                'fk_lgb003_num_commodity' => NULL,
                'fk_a023_num_centro_costo' => $value['numCC'],
                'fk_lgc003_num_cotizacion' => NULL,
                'fk_lgb004_num_unidad' => $value['idUnidad']
            ));
        }

        #commit — Consigna una transacción

        $fallaTansaccion1 = $NuevoPost1->errorInfo();
        $fallaTansaccion2 = $NuevoPost2->errorInfo();

        if(!empty($fallaTansaccion1[1]) && !empty($fallaTansaccion1[2])){
            $this->_db->rollBack();
            return $fallaTansaccion1;

        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;

        }else{
            $this->_db->commit();
            return $numReq;
        }

    }

    public function metContar($where = false)
    {
        $anio = date('Y-m-d');
        $generarOrdenPost = $this->_db->query("
            SELECT
              count(*) AS cantidad
            FROM
            lg_b001_requerimiento
            WHERE
            fec_anio='$anio'
            $where
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metBuscarRequerimiento($idReq)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
            lg_b001_requerimiento AS requerimiento 
            INNER JOIN a004_dependencia AS dependencia ON requerimiento.fk_a004_num_dependencia = dependencia.pk_num_dependencia
            WHERE
            pk_num_requerimiento='$idReq'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metBuscarRequerimientoDetalle($idReq,$idItem)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              detalle.*,
              conversion.num_cantidad,
              item.fk_lgb004_num_unidad_compra
            FROM
            lg_c001_requerimiento_detalle AS detalle
            INNER JOIN lg_b002_item AS item ON detalle.fk_lgb002_num_item = item.pk_num_item
            INNER JOIN lg_c002_unidades_conversion AS conversion ON item.fk_lgc002_num_unidad_conversion = conversion.pk_num_unidad_conversion
            WHERE
            detalle.fk_lgb001_num_requerimiento='$idReq' AND 
            detalle.fk_lgb002_num_item = '$idItem'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metBuscarDependencia()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
            a004_dependencia AS dependencia
            INNER JOIN a023_centro_costo AS costo ON dependencia.pk_num_dependencia = costo.fk_a004_num_dependencia
            WHERE
            dependencia.ind_dependencia = 'DIRECCIÓN DE ADMINISTRACIÓN' AND
            costo.ind_descripcion_centro_costo LIKE '%AREA DE COMPRA%'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metActualizarRequerimiento($idReq)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $NuevoPost=$this->_db->prepare(
            "UPDATE lg_b001_requerimiento
              SET 
              fk_rhb001_num_empleado_aprueba_despacho=:fk_rhb001_num_empleado_aprueba_despacho,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE pk_num_requerimiento =:pk_num_requerimiento
              ");

        $NuevoPost->execute(array(
            'fk_rhb001_num_empleado_aprueba_despacho'=>$this->atIdEmpleado,
            'pk_num_requerimiento'=>$idReq
        ));

        #commit — Consigna una transacción

        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idReq;
        }
    }
}

?>