<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'scriptCargaModelo.php';
class scriptCargaLGModelo extends Modelo
{
    private $atIdUsuario;
    private $atIdEmpleado;
    public function __construct()
    {
        parent::__construct();
        $this->atScriptCargaModelo = new scriptCargaModelo();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atIdEmpleado=Session::metObtener('idEmpleado');
    }

    public function metCargarTipoDocLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                lg_b016_tipo_documento
              SET
                cod_tipo_documento=:cod_tipo_documento,
                ind_descripcion=:ind_descripcion,
                num_flag_documento_fiscal=:num_flag_documento_fiscal,
                num_flag_transaccion_sistema=:num_flag_transaccion_sistema,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW()
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b016_tipo_documento', false, "cod_tipo_documento = '" . $array['cod_tipo_documento'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':cod_tipo_documento' => $array['cod_tipo_documento'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_flag_documento_fiscal' => $array['num_flag_documento_fiscal'],
                    ':num_flag_transaccion_sistema' => $array['num_flag_transaccion_sistema'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarTipoTranLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                lg_b015_tipo_transaccion
              SET
                ind_cod_tipo_transaccion=:ind_cod_tipo_transaccion,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_detalle_tipo_documento=:fk_a006_num_miscelaneo_detalle_tipo_documento,
                fk_lgb016_num_tipo_documento_generado=:fk_lgb016_num_tipo_documento_generado,
                fk_lgb016_num_tipo_documento_transaccion=:fk_lgb016_num_tipo_documento_transaccion,
                num_flag_voucher_consumo=:num_flag_voucher_consumo,
                num_flag_voucher_ajuste=:num_flag_voucher_ajuste,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW()
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b015_tipo_transaccion', false, "ind_cod_tipo_transaccion = '" . $array['ind_cod_tipo_transaccion'] . "'");
            if (!$busqueda) {
                $tipoMovimiento = $this->atScriptCargaModelo->metMostrarSelect('TIPOTRANS',$array['fk_a006_num_miscelaneo_detalle_tipo_documento']);
                $tipoDocGenerado = $this->atScriptCargaModelo->metBusquedaSimple('lg_b016_tipo_documento',false,"cod_tipo_documento = '" . $array['fk_lgb016_num_tipo_documento_generado'] . "'");
                $tipoDocTransaccion = $this->atScriptCargaModelo->metBusquedaSimple('lg_b016_tipo_documento',false,"cod_tipo_documento = '" . $array['fk_lgb016_num_tipo_documento_transaccion'] . "'");
                if($array['num_flag_voucher_consumo']=="S"){
                    $array['num_flag_voucher_consumo'] = 1;
                }else{
                    $array['num_flag_voucher_consumo'] = 0;
                }
                if($array['num_flag_voucher_ajuste']=="S"){
                    $array['num_flag_voucher_ajuste'] = 1;
                }else{
                    $array['num_flag_voucher_ajuste'] = 0;
                }
                if($array['num_estatus']=="A"){
                    $array['num_estatus'] = 1;
                }else{
                    $array['num_estatus'] = 0;
                }
                $registro->execute(array(
                    ':ind_cod_tipo_transaccion' => $array['ind_cod_tipo_transaccion'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_miscelaneo_detalle_tipo_documento' => $tipoMovimiento['pk_num_miscelaneo_detalle'],
                    ':fk_lgb016_num_tipo_documento_generado' => $tipoDocGenerado['pk_num_tipo_documento'],
                    ':fk_lgb016_num_tipo_documento_transaccion' => $tipoDocTransaccion['pk_num_tipo_documento'],
                    ':num_flag_voucher_consumo' => $array['num_flag_voucher_consumo'],
                    ':num_flag_voucher_ajuste' => $array['num_flag_voucher_ajuste'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarAlmacenLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                lg_b014_almacen
              SET
                pk_num_almacen=:pk_num_almacen,
                cod_almacen=:cod_almacen,
                ind_descripcion=:ind_descripcion,

                ind_tipo_almacen=:ind_tipo_almacen,
                ind_direccion=:ind_direccion,
                num_flag_commodity=:num_flag_commodity,
                fk_cbb004_num_cuenta_inventario=:fk_cbb004_num_cuenta_inventario,

                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                fk_rhb001_num_empleado_responsable=:fk_rhb001_num_empleado_responsable,
                fk_a003_num_persona_dependencia_resp=:fk_a003_num_persona_dependencia_resp
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b014_almacen', false, "pk_num_almacen = '" . $array['pk_num_almacen'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_almacen' => $array['pk_num_almacen'],
                    ':cod_almacen' => $array['cod_almacen'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_tipo_almacen' => $array['ind_tipo_almacen'],
                    ':ind_direccion' => $array['ind_direccion'],
                    ':num_flag_commodity' => $array['num_flag_commodity'],
                    ':fk_cbb004_num_cuenta_inventario' => $array['fk_cbb004_num_cuenta_inventario'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fk_rhb001_num_empleado_responsable' => $array['fk_rhb001_num_empleado_responsable'],
                    ':fk_a003_num_persona_dependencia_resp' => $array['fk_a003_num_persona_dependencia_resp']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarUnidadesLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                lg_b004_unidades
              SET
                ind_cod_unidad=:ind_cod_unidad,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneos_tipo_medida=:fk_a006_num_miscelaneos_tipo_medida,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                num_estatus=:num_estatus
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b004_unidades', false, "ind_cod_unidad = '" . $array['ind_cod_unidad'] . "'");
            if (!$busqueda) {
                $tipoMedida = $this->atScriptCargaModelo->metMostrarSelect('TIPOMED',$array['fk_a006_num_miscelaneos_tipo_medida']);

                if($array['num_estatus']=="A"){
                    $array['num_estatus'] = 1;
                }else{
                    $array['num_estatus'] = 0;
                }
                $registro->execute(array(
                    ':ind_cod_unidad' => $array['ind_cod_unidad'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_miscelaneos_tipo_medida' => $tipoMedida['pk_num_miscelaneo_detalle'],
                    ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
                    ':num_estatus' => $array['num_estatus']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarUnidadesConverisionLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                lg_c002_unidades_conversion
              SET
                ind_cod_unidad_conversion=:ind_cod_unidad_conversion,
                num_cantidad=:num_cantidad,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                fk_lgb004_num_unidad=:fk_lgb004_num_unidad
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_c002_unidades_conversion', false, "ind_cod_unidad_conversion = '" . $array['ind_cod_unidad_conversion'] . "'");
            if (!$busqueda) {
                $unidad = $this->atScriptCargaModelo->metBusquedaSimple('lg_b004_unidades', false, "ind_cod_unidad = '" . $array['ind_cod_unidad'] . "'");
                if($array['num_estatus']=="A"){
                    $array['num_estatus'] = 1;
                }else{
                    $array['num_estatus'] = 0;
                }
                $registro->execute(array(
                    ':ind_cod_unidad_conversion' => $array['ind_cod_unidad_conversion'],
                    ':num_cantidad' => $array['num_cantidad'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
                    ':fk_lgb004_num_unidad' => $unidad['pk_num_unidad']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarFamiliasLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                lg_b006_clase_familia
              SET
                ind_cod_familia=:ind_cod_familia,
                ind_descripcion=:ind_descripcion,
                fk_cbb004_cuenta_inventario=:fk_cbb004_cuenta_inventario,
                fk_cbb004_cuenta_gasto=:fk_cbb004_cuenta_gasto,
                fk_prb002_partida_presupuestaria=:fk_prb002_partida_presupuestaria,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                fk_a006_num_miscelaneo_clase_linea=:fk_a006_num_miscelaneo_clase_linea
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b006_clase_familia', false, "ind_cod_familia = '" . $array['ind_cod_familia'] . "'");
            if (!$busqueda) {
                $linea = $this->atScriptCargaModelo->metMostrarSelect('LINEAS',$array['cod_detalle']);
                $cuenta1 = $this->atScriptCargaModelo->metBusquedaSimple('cb_b004_plan_cuenta', false, "cod_cuenta = '" . $array['fk_cbb004_cuenta_inventario'] . "'");
                $cuenta2 = $this->atScriptCargaModelo->metBusquedaSimple('cb_b004_plan_cuenta', false, "cod_cuenta = '" . $array['fk_cbb004_cuenta_gasto'] . "'");
                $partida = $this->atScriptCargaModelo->metBusquedaSimple('pr_b002_partida_presupuestaria', false, "cod_partida = '" . $array['fk_prb002_partida_presupuestaria'] . "'");
                if($array['num_estatus']=="A"){
                    $array['num_estatus'] = 1;
                }else{
                    $array['num_estatus'] = 0;
                }
                $registro->execute(array(
                    ':ind_cod_familia' => $array['ind_cod_familia'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_cbb004_cuenta_inventario' => $cuenta1['pk_num_cuenta'],
                    ':fk_cbb004_cuenta_gasto' => $cuenta2['pk_num_cuenta'],
                    ':fk_prb002_partida_presupuestaria' => $partida['pk_num_partida_presupuestaria'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
                    ':fk_a006_num_miscelaneo_clase_linea' => $linea['pk_num_miscelaneo_detalle']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarSubFamiliasLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                lg_b007_clase_subfamilia
              SET
                ind_cod_subfamilia=:ind_cod_subfamilia,
                ind_descripcion=:ind_descripcion,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                fk_lgb006_num_clase_familia=:fk_lgb006_num_clase_familia
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b007_clase_subfamilia', false, "ind_cod_subfamilia = '" . $array['ind_cod_subfamilia'] . "'");
            if (!$busqueda) {
                $linea = $this->atScriptCargaModelo->metMostrarSelect('LINEAS',$array['ind_cod_linea']);
                $familia = $this->atScriptCargaModelo->metBusquedaSimple('lg_b006_clase_familia', false, "ind_cod_familia = '" . $array['ind_cod_familia'] . "'
                AND fk_a006_num_miscelaneo_clase_linea = '".$linea['pk_num_miscelaneo_detalle']."'");
                if($array['num_estatus']=="A"){
                    $array['num_estatus'] = 1;
                }else{
                    $array['num_estatus'] = 0;
                }
                $registro->execute(array(
                    ':ind_cod_subfamilia' => $array['ind_cod_subfamilia'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
                    ':fk_lgb006_num_clase_familia' => $familia['pk_num_clase_familia']
                ));
//                echo $array['ind_cod_linea']."----".$linea['pk_num_miscelaneo_detalle']."----".$array['ind_cod_familia']."----".$familia['pk_num_clase_familia']."----".$array['ind_cod_subfamilia']."----<br>";
            }
            /*
            $error = $registro->errorInfo();
            var_dump($error);
            echo '<br><br>';
            */
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarClasificacionLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                lg_b017_clasificacion
              SET
                ind_descripcion=:ind_descripcion,
                ind_cod_clasificacion=:ind_cod_clasificacion,
                fk_a006_num_miscelaneos_tipo_requerimiento=:fk_a006_num_miscelaneos_tipo_requerimiento,
                num_flag_recepcion_almacen=:num_flag_recepcion_almacen,
                num_flag_revision=:num_flag_revision,
                num_flag_transaccion=:num_flag_transaccion,
                num_flag_caja_chica=:num_flag_caja_chica,
                num_estatus=:num_estatus,
                num_flag_activo_fijo=:num_flag_activo_fijo,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                fk_lgb014_num_almacen=:fk_lgb014_num_almacen
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b017_clasificacion', false, "ind_cod_clasificacion = '" . $array['ind_cod_clasificacion'] . "'");
            if (!$busqueda) {
                $tipoReq = $this->atScriptCargaModelo->metMostrarSelect('TIPOREQ',$array['fk_a006_num_miscelaneos_tipo_requerimiento']);
                $almacen = $this->atScriptCargaModelo->metBusquedaSimple('lg_b014_almacen', false, "cod_almacen = '" . $array['fk_lgb014_num_almacen'] . "'");
                if($array['num_flag_revision']=="S"){
                    $array['num_flag_revision'] = 1;
                }else{
                    $array['num_flag_revision'] = 0;
                }
                if($array['num_flag_transaccion']=="S"){
                    $array['num_flag_transaccion'] = 1;
                }else{
                    $array['num_flag_transaccion'] = 0;
                }
                if($array['num_flag_caja_chica']=="S"){
                    $array['num_flag_caja_chica'] = 1;
                }else{
                    $array['num_flag_caja_chica'] = 0;
                }
                if($array['num_estatus']=="A"){
                    $array['num_estatus'] = 1;
                }else{
                    $array['num_estatus'] = 0;
                }
                if($array['num_flag_activo_fijo']=="S"){
                    $array['num_flag_activo_fijo'] = 1;
                }else{
                    $array['num_flag_activo_fijo'] = 0;
                }
                $registro->execute(array(
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_cod_clasificacion' => $array['ind_cod_clasificacion'],
                    ':fk_a006_num_miscelaneos_tipo_requerimiento'=>$tipoReq['pk_num_miscelaneo_detalle'],
                    ':num_flag_recepcion_almacen' => $array['num_flag_recepcion_almacen'],
                    ':num_flag_revision' => $array['num_flag_revision'],
                    ':num_flag_transaccion' => $array['num_flag_transaccion'],
                    ':num_flag_caja_chica' => $array['num_flag_caja_chica'],
                    ':num_estatus' => $array['num_estatus'],
                    ':num_flag_activo_fijo' => $array['num_flag_activo_fijo'],
                    ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
                    ':fk_lgb014_num_almacen' => $almacen['pk_num_almacen']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarAspectoLG($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
              lg_e007_aspecto_cualitativo
          SET
              ind_nombre=:ind_nombre,
              ind_cod_aspecto=:ind_cod_aspecto,
              num_puntaje_maximo=:num_puntaje_maximo,
              num_estatus=:num_estatus,
              fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle,
              fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
              fec_ultima_modificacion=NOW()
            ");
        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_e007_aspecto_cualitativo', false, "ind_cod_aspecto = '" . $array['ind_cod_aspecto'] . "'");
            if (!$busqueda) {
                $tipoAspecto = $this->atScriptCargaModelo->metMostrarSelect('TAE',$array['cod_detalle']);

                $registro->execute(array(
                    'ind_nombre'=>$array['ind_nombre'],
                    'ind_cod_aspecto'=>$array['ind_cod_aspecto'],
                    'num_puntaje_maximo'=>$array['num_puntaje_maximo'],
                    'num_estatus'=>$array['num_estatus'],
                    'fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
                    'fk_a006_num_miscelaneo_detalle'=>$tipoAspecto['pk_num_miscelaneo_detalle']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarItemsLG($arrays)
    {
        $this->_db->beginTransaction();

        $registro1 = $this->_db->prepare("
              INSERT INTO
                lg_c007_item_stock
              SET
                num_stock_inicial=:num_stock_inicial,
                num_stock_actual=:num_stock_actual,
                num_stock_comprometido=:num_stock_comprometido,
                num_precio_unitario=:num_precio_unitario,
                fec_mes=:fec_mes,
                fec_anio=:fec_anio,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                fk_lgc002_num_unidad_conversion=:fk_lgc002_num_unidad_conversion,
                fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                fk_rhb001_num_empleado_ingresadopor=:fk_rhb001_num_empleado_ingresadopor
            ");

        $registro2 = $this->_db->prepare(
            "INSERT INTO
                    lg_b002_item
                  SET
                    ind_descripcion=:ind_descripcion,
                    ind_codigo_interno=:ind_codigo_interno,
                    num_flag_lotes=:num_flag_lotes,
                    num_flag_kit=:num_flag_kit,
                    num_flag_impuesto_venta=:num_flag_impuesto_venta,
                    num_flag_auto=:num_flag_auto,
                    num_flag_disponible=:num_flag_disponible,
                    num_flag_verificado_presupuesto=:num_flag_verificado_presupuesto,
                    ind_imagen=:ind_imagen,
                    num_stock_minimo=:num_stock_minimo,
                    num_stock_maximo=:num_stock_maximo,
                    num_punto_reorden=:num_punto_reorden,
                    fk_cbb004_num_plan_cuenta_inventario_oncop=:fk_cbb004_num_plan_cuenta_inventario_oncop,
                    fk_cbb004_num_plan_cuenta_gasto_oncop=:fk_cbb004_num_plan_cuenta_gasto_oncop,
                    fk_cbb004_num_plan_cuenta_inventario_pub_veinte=:fk_cbb004_num_plan_cuenta_inventario_pub_veinte,
                    fk_cbb004_num_plan_cuenta_gasto_pub_veinte=:fk_cbb004_num_plan_cuenta_gasto_pub_veinte,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                    num_estatus=:num_estatus,
                    fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                    fec_ultima_modificacion=NOW(),
                    fk_lgb007_num_clase_subfamilia=:fk_lgb007_num_clase_subfamilia,
                    fk_lgc002_num_unidad_conversion=:fk_lgc002_num_unidad_conversion,
                    fk_lgc007_num_item_stock=:fk_lgc007_num_item_stock,
                    fk_lgb004_num_unidad_compra=:fk_lgb004_num_unidad_compra,
                    fk_lgb004_num_unidad_despacho=:fk_lgb004_num_unidad_despacho,
                    fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                    fk_a006_num_miscelaneo_tipo_item=:fk_a006_num_miscelaneo_tipo_item
                ");

        $c=0;
        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b002_item', false, "ind_codigo_interno = '" . $array['ind_codigo_interno'] . "'");
            if (!$busqueda) {
                $c=$c+1;

                $almacen = $this->atScriptCargaModelo->metBusquedaSimple('lg_b014_almacen', false, "cod_almacen = 'ALCE'");

                $unidadConversion = $this->atScriptCargaModelo->metBusquedaSimple('lg_c002_unidades_conversion', false,
                    "ind_cod_unidad_conversion = '" . $array['fk_lgc002_num_unidad_conversion'] . "'");
                if($unidadConversion['pk_num_unidad_conversion']=='' OR $unidadConversion['pk_num_unidad_conversion']=='0') {
                    $unidadConversion['pk_num_unidad_conversion'] = 1;
                }
                $registro1->execute(array(
                    'num_stock_inicial'=>'0.000000',
                    'num_stock_actual'=>'0.000000',
                    'num_stock_comprometido'=>'0.000000',
                    'num_precio_unitario'=>'0.000000',
                    'fec_mes'=>date('m'),
                    'fec_anio'=>date('Y'),
                    'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario,
                    'fk_lgc002_num_unidad_conversion'=>$unidadConversion['pk_num_unidad_conversion'],
                    'fk_lgb014_num_almacen'=>$almacen['pk_num_almacen'],
                    'fk_rhb001_num_empleado_ingresadopor'=>$this->atIdUsuario
                ));

                $idStock = $this->_db->lastInsertId();

                if($array['num_estatus']=="A"){
                    $array['num_estatus'] = 1;
                }else{
                    $array['num_estatus'] = 0;
                }
                if($array['num_flag_lotes']=="S"){
                    $array['num_flag_lotes'] = 1;
                }else{
                    $array['num_flag_lotes'] = 0;
                }
                if($array['num_flag_kit']=="S"){
                    $array['num_flag_kit'] = 1;
                }else{
                    $array['num_flag_kit'] = 0;
                }
                if($array['num_flag_impuesto_venta']=="S"){
                    $array['num_flag_impuesto_venta'] = 1;
                }else{
                    $array['num_flag_impuesto_venta'] = 0;
                }
                if($array['num_flag_impuesto_venta']=="S"){
                    $array['num_flag_impuesto_venta'] = 1;
                }else{
                    $array['num_flag_impuesto_venta'] = 0;
                }
                if($array['num_flag_auto']=="S"){
                    $array['num_flag_auto'] = 1;
                }else{
                    $array['num_flag_auto'] = 0;
                }
                if($array['num_flag_disponible']=="S"){
                    $array['num_flag_disponible'] = 1;
                }else{
                    $array['num_flag_disponible'] = 0;
                }
                if($array['num_flag_verificado_presupuesto']=="S"){
                    $array['num_flag_verificado_presupuesto'] = 1;
                }else{
                    $array['num_flag_verificado_presupuesto'] = 0;
                }


                $cuentaInventarioO = $this->atScriptCargaModelo->metMostrarCuenta($array['fk_cbb004_num_plan_cuenta_inventario_oncop']);
//                $cuentaGastoO = $this->atScriptCargaModelo->metMostrarCuenta($array['fk_cbb004_num_plan_cuenta_gasto_oncop']);
                $cuentaInventario2 = $this->atScriptCargaModelo->metMostrarCuenta($array['fk_cbb004_num_plan_cuenta_inventario_pub_veinte']);
                $cuentaGasto2 = $this->atScriptCargaModelo->metMostrarCuenta($array['fk_cbb004_num_plan_cuenta_gasto_pub_veinte']);

//                $partida = $this->atScriptCargaModelo->metMostrarPartida($array['fk_prb002_num_partida_presupuestaria']);
                $partida = $this->atScriptCargaModelo->metBusquedaSimple('pr_b002_partida_presupuestaria', false,
                    "cod_partida = '" . $array['fk_prb002_num_partida_presupuestaria'] . "'");
                $subFamilia = $this->atScriptCargaModelo->metBusquedaSimple('lg_b007_clase_subfamilia AS subfamilia', ' 
                    INNER JOIN lg_b006_clase_familia AS familia ON familia.pk_num_clase_familia = subfamilia.fk_lgb006_num_clase_familia
                    INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = familia.fk_a006_num_miscelaneo_clase_linea ',
                    " subfamilia.ind_cod_subfamilia = '" . $array['fk_lgb007_num_clase_subfamilia'] . "' AND
                    familia.ind_cod_familia = '" . $array['cod_familia'] . "' AND
                    detalle.cod_detalle = '" . $array['cod_linea'] . "'
                    ");
                if($subFamilia['pk_num_subfamilia']=='') {
                    $subFamilia['pk_num_subfamilia'] = 1;
                }

                $unidadCompra = $this->atScriptCargaModelo->metBusquedaSimple('lg_b004_unidades', false,
                    "ind_cod_unidad = '" . $array['fk_lgb004_num_unidad_compra'] . "'");
                $unidadDespacho = $this->atScriptCargaModelo->metBusquedaSimple('lg_b004_unidades', false,
                    "ind_cod_unidad = '" . $array['fk_lgb004_num_unidad_despacho'] . "'");
                $tipoItem = $this->atScriptCargaModelo->metMostrarSelect('TIPOITM',$array['fk_a006_num_miscelaneo_tipo_item']);


                $puntoReorden = $array['num_stock_maximo'] - $array['num_stock_minimo'];

                if($unidadCompra['pk_num_unidad']=='') {
                    $unidadCompra['pk_num_unidad'] = 1;
                }
                if($unidadDespacho['pk_num_unidad']=='') {
                    $unidadDespacho['pk_num_unidad'] = 1;
                }
                if($partida['fk_cbb004_num_plan_cuenta_onco']=='' OR $partida['fk_cbb004_num_plan_cuenta_onco']==0) {
                    $cuentaGastoO = 1;
                } else {
                    $cuentaGastoO = $partida['fk_cbb004_num_plan_cuenta_onco'];
                }


                $registro2->execute(array(
                    'ind_descripcion'=>$array['ind_descripcion'],
                    'ind_codigo_interno'=>$array['ind_codigo_interno'],
                    'num_flag_lotes'=>$array['num_flag_lotes'],
                    'num_flag_kit'=>$array['num_flag_kit'],
                    'num_flag_impuesto_venta'=>$array['num_flag_impuesto_venta'],
                    'num_flag_auto'=>$array['num_flag_auto'],
                    'num_flag_disponible'=>$array['num_flag_disponible'],
                    'num_flag_verificado_presupuesto'=>$array['num_flag_verificado_presupuesto'],
                    'ind_imagen'=>$array['ind_imagen'],

                    'num_stock_minimo'=>$array['num_stock_minimo'],
                    'num_stock_maximo'=>$array['num_stock_maximo'],
                    'num_punto_reorden'=>$puntoReorden,
                    'fk_cbb004_num_plan_cuenta_inventario_oncop'=>$cuentaInventarioO['pk_num_cuenta'],
                    'fk_cbb004_num_plan_cuenta_gasto_oncop'=>$cuentaGastoO,
                    'fk_cbb004_num_plan_cuenta_inventario_pub_veinte'=>$cuentaInventario2['pk_num_cuenta'],
                    'fk_cbb004_num_plan_cuenta_gasto_pub_veinte'=>$cuentaGasto2['pk_num_cuenta'],
                    'fk_prb002_num_partida_presupuestaria'=>$partida['pk_num_partida_presupuestaria'],
                    'num_estatus'=>$array['num_estatus'],
                    'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario,

                    'fk_lgb007_num_clase_subfamilia'=>$subFamilia['pk_num_subfamilia'],
                    'fk_lgc002_num_unidad_conversion'=>$unidadConversion['pk_num_unidad_conversion'],
                    'fk_lgc007_num_item_stock'=>$idStock,
                    'fk_lgb004_num_unidad_compra'=>$unidadCompra['pk_num_unidad'],
                    'fk_lgb004_num_unidad_despacho'=>$unidadDespacho['pk_num_unidad'],
                    'fk_lgb014_num_almacen'=>$almacen['pk_num_almacen'],
                    'fk_a006_num_miscelaneo_tipo_item'=>$tipoItem['pk_num_miscelaneo_detalle']
                ));
            }
        }

        $error = $registro2->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarCommodityLG($arrays)
    {
        $this->_db->beginTransaction();

        $registro1 = $this->_db->prepare("
              INSERT INTO
                lg_b003_commodity
              SET
                ind_cod_commodity=:ind_cod_commodity,
                ind_descripcion=:ind_descripcion,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
                num_estatus=:num_estatus,
                num_flag_vericado_presupuesto=:num_flag_vericado_presupuesto,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
                fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
            ");

        $registro2 = $this->_db->prepare("
              INSERT INTO
                lg_c008_commodity_stock
              SET
                num_stock_inicial=:num_stock_inicial,
                num_cantidad=:num_cantidad,
                fec_mes=:fec_mes,
                fec_anio=:fec_anio,
                num_precio_unitario=:num_precio_unitario,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW(),
                fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                fk_rhb001_num_empleado_ingresadopor=:fk_rhb001_num_empleado_ingresadopor,
                fk_lgb003_num_commodity=:fk_lgb003_num_commodity
            ");
        foreach ($arrays as $array) {
            $busqueda = $this->atScriptCargaModelo->metBusquedaSimple('lg_b003_commodity', false, "ind_cod_commodity = '" . $array['ind_cod_commodity'] . "'");
            if (!$busqueda) {

                $almacen = $this->atScriptCargaModelo->metBusquedaSimple('lg_b014_almacen', false, "cod_almacen = 'COCE'");
                if($array['num_estatus']=="A"){
                    $array['num_estatus'] = 1;
                }else{
                    $array['num_estatus'] = 0;
                }
                if($array['num_flag_vericado_presupuesto']=="S"){
                    $array['num_flag_vericado_presupuesto'] = 1;
                }else{
                    $array['num_flag_vericado_presupuesto'] = 0;
                }
                $unidad = $this->atScriptCargaModelo->metBusquedaSimple('lg_b004_unidades', false,
                    "ind_cod_unidad = '" . $array['fk_lgb004_num_unidad'] . "'");
                $clasificacion = $this->atScriptCargaModelo->metBusquedaSimple('lg_b017_clasificacion', false,
                    "ind_cod_clasificacion = '" . $array['fk_lgb017_num_clasificacion'] . "'");

                $cuenta = $this->atScriptCargaModelo->metMostrarCuenta($array['fk_cbb004_num_plan_cuenta']);
                $cuenta2 = $this->atScriptCargaModelo->metMostrarCuenta($array['fk_cbb004_num_plan_cuenta_pub_veinte']);

                $partida = $this->atScriptCargaModelo->metMostrarPartida($array['fk_prb002_num_partida_presupuestaria']);

                if($unidad['pk_num_unidad']=='') {
                    $unidad['pk_num_unidad'] = 1;
                }

                $registro1->execute(array(
                    'ind_cod_commodity'=>$array['ind_cod_commodity'],
                    'ind_descripcion'=>$array['ind_descripcion'],
                    'fk_prb002_num_partida_presupuestaria'=>$partida['pk_num_partida_presupuestaria'],
                    'fk_cbb004_num_plan_cuenta'=>$cuenta['pk_num_cuenta'],
                    'fk_cbb004_num_plan_cuenta_pub_veinte'=>$cuenta2['pk_num_cuenta'],
                    'num_estatus'=>$array['num_estatus'],
                    'num_flag_vericado_presupuesto'=>$array['num_flag_vericado_presupuesto'],
                    'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario,
                    'fk_lgb004_num_unidad'=>$unidad['pk_num_unidad'],
                    'fk_lgb014_num_almacen'=>$almacen['pk_num_almacen'],
                    'fk_lgb017_num_clasificacion'=>$clasificacion['pk_num_clasificacion']
                ));
                $idComm = $this->_db->lastInsertId();

                $registro2->execute(array(
                    'num_stock_inicial'=>'0.000000',
                    'num_cantidad'=>'0.000000',
                    'fec_mes'=>date('m'),
                    'fec_anio'=>date('Y'),
                    'num_precio_unitario'=>'0.000000',
                    'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario,
                    'fk_lgb014_num_almacen'=>$almacen['pk_num_almacen'],
                    'fk_rhb001_num_empleado_ingresadopor'=>$this->atIdEmpleado,
                    'fk_lgb003_num_commodity'=>$idComm
                ));
            }
        }

        $error = $registro2->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarPersonaLG($array)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                a003_persona
              SET
                ind_cedula_documento=:ind_cedula_documento,
                ind_documento_fiscal=:ind_documento_fiscal,
                ind_nombre1=:ind_nombre1,
                ind_email=:ind_email,
                ind_tipo_persona=:ind_tipo_persona,
                num_estatus='1',
                fk_a010_num_ciudad=:fk_a010_num_ciudad,

                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW()
            ");

        $registro->execute(array(
            'ind_cedula_documento'=>$array['ind_documento_fiscal'],
            'ind_documento_fiscal'=>$array['ind_documento_fiscal'],
            'ind_nombre1'=>$array['ind_nombre1'],
            'ind_email'=>$array['ind_email'],
            'ind_tipo_persona'=>$array['ind_tipo_persona'],
            'fk_a010_num_ciudad'=>$array['fk_a010_num_ciudad'],
            'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario
        ));

        $idRegistro = $this->_db->lastInsertId();
        $NuevoPost1 = $this->_db->prepare("
                INSERT INTO
                  a036_persona_direccion
                SET
                  fk_a003_num_persona=:fk_a003_num_persona,
                  fk_a006_num_miscelaneo_detalle_domicilio=250,
                  fk_a006_num_miscelaneo_detalle_tipodir=250,
                  ind_direccion=:ind_direccion
                  ");

        $NuevoPost1->execute(array(
            'fk_a003_num_persona' => $idRegistro,
            'ind_direccion' => $array['ind_direccion']
        ));

        if($array['Telefono1']!=''){
            $NuevoPost2 = $this->_db->prepare(
                "INSERT INTO
                      a007_persona_telefono
                     SET
                      ind_telefono=:ind_telefono,
                      fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                      fk_a003_num_persona=$idRegistro"
            );
            #execute — Ejecuta una sentencia preparada
            $NuevoPost2->execute(array(
                'ind_telefono' => $array['Telefono1'],
                'fk_a006_num_miscelaneo_detalle_tipo_telefono'=> '0'
            ));
        }
        if($array['Telefono2']!=''){
            $NuevoPost2 = $this->_db->prepare(
                "INSERT INTO
                      a007_persona_telefono
                     SET
                      ind_telefono=:ind_telefono,
                      fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                      fk_a003_num_persona=$idRegistro"
            );
            #execute — Ejecuta una sentencia preparada
            $NuevoPost2->execute(array(
                'ind_telefono' => $array['Telefono2'],
                'fk_a006_num_miscelaneo_detalle_tipo_telefono'=> '0'
            ));
        }
        if($array['TelefEmerg1']!=''){
            $NuevoPost2 = $this->_db->prepare(
                "INSERT INTO
                      a007_persona_telefono
                     SET
                      ind_telefono=:ind_telefono,
                      fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                      fk_a003_num_persona=$idRegistro"
            );
            #execute — Ejecuta una sentencia preparada
            $NuevoPost2->execute(array(
                'ind_telefono' => $array['TelefEmerg1'],
                'fk_a006_num_miscelaneo_detalle_tipo_telefono'=> '1'
            ));
        }
        if($array['TelefEmerg2']!=''){
            $NuevoPost2 = $this->_db->prepare(
                "INSERT INTO
                      a007_persona_telefono
                     SET
                      ind_telefono=:ind_telefono,
                      fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                      fk_a003_num_persona=$idRegistro"
            );
            #execute — Ejecuta una sentencia preparada
            $NuevoPost2->execute(array(
                'ind_telefono' => $array['TelefEmerg2'],
                'fk_a006_num_miscelaneo_detalle_tipo_telefono'=> '1'
            ));
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return 'fail';
        } else {
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarProveedorLG($array,$cual)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                lg_b022_proveedor
              SET
                cod_tipo_pago=:cod_tipo_pago,
                cod_forma_pago=:cod_forma_pago,
                cod_tipo_servicio=:cod_tipo_servicio,
                num_dias_pago=:num_dias_pago,
                ind_registro_publico=:ind_registro_publico,
                ind_licencia_municipal=:ind_licencia_municipal,
                fec_constitucion=:fec_constitucion,
                ind_snc=:ind_snc,
                ind_num_inscripcion_snc=:ind_num_inscripcion_snc,
                fec_emision_snc=:fec_emision_snc,
                fec_validacion_snc=:fec_validacion_snc,
                ind_nacionalidad=:ind_nacionalidad,
                ind_condicion_rcn=:ind_condicion_rcn,
                ind_calificacion=:ind_calificacion,
                ind_nivel=:ind_nivel,
                num_capacidad_financiera=:num_capacidad_financiera,
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=NOW()
            ");

        $registro->execute(array(
            'cod_tipo_pago'=>$array['cod_tipo_pago'],
            'cod_forma_pago'=>$array['cod_forma_pago'],
            'cod_tipo_servicio'=>$array['cod_tipo_servicio'],
            'num_dias_pago'=>$array['num_dias_pago'],
            'ind_registro_publico'=>$array['ind_registro_publico'],
            'ind_licencia_municipal'=>$array['ind_licencia_municipal'],
            'fec_constitucion'=>$array['fec_constitucion'],
            'ind_snc'=>$array['ind_snc'],
            'ind_num_inscripcion_snc'=>$array['ind_num_inscripcion_snc'],
            'fec_emision_snc'=>$array['fec_emision_snc'],
            'fec_validacion_snc'=>$array['fec_validacion_snc'],
            'ind_nacionalidad'=>$array['ind_nacionalidad'],
            'ind_condicion_rcn'=>$array['ind_condicion_rcn'],
            'ind_calificacion'=>$array['ind_calificacion'],
            'ind_nivel'=>$array['ind_nivel'],
            'num_capacidad_financiera'=>$array['num_capacidad_financiera'],
            'fk_a003_num_persona_proveedor'=>$array['fk_a003_num_persona_proveedor'],
            'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario
        ));
        $idRegistro = $this->_db->lastInsertId();

        $NuevoPost4 = $this->_db->prepare("
                INSERT INTO
                    lg_e002_proveedor_documento
                SET
                    fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                    fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
                    ");
        $NuevoPost4->execute(array(
            'fk_cpb002_num_tipo_documento' => $array['fk_cpb002_num_tipo_documento'],
            'fk_lgb022_num_proveedor'=> $idRegistro
        ));

        $NuevoPost5 = $this->_db->prepare("
                INSERT INTO
                    lg_e003_proveedor_servicio
                SET
                    fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                    fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
                    ");
        $NuevoPost5->execute(array(
            'fk_cpb017_num_tipo_servicio' => $array['fk_cpb017_num_tipo_servicio'],
            'fk_lgb022_num_proveedor'=> $idRegistro
        ));

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }

    }




    public function metPrestacionesSociales()
    {
        $miscelaneoDetalle = $this->_db2->query("
            SELECT
              persona.Ndocumento,
              persona.DocFiscal,
              persona.Estado,
              prestaciones.TipoNomina AS fk_nmb001_num_tipo_nomina,
              SUBSTRING(prestaciones.Periodo,1, 4) AS fec_anio,
              SUBSTRING(prestaciones.Periodo,6, 2) AS fec_mes,
              prestaciones.CodPersona,
              prestaciones.SueldoBase AS num_sueldo_base,
              prestaciones.TotalAsig AS num_total_asignaciones,
              prestaciones.SueldoNormal AS num_sueldo_normal,
              prestaciones.AliVac AS num_alicuota_vacacional,
              prestaciones.AliFin AS num_alicuota_fin_anio,
              prestaciones.BonoEspecial AS num_bono_especial,
              prestaciones.BonoFiscal AS num_bono_fiscal,
              prestaciones.DiasTrimestral AS num_dias_trimestre,
              prestaciones.DiasAnual AS num_dias_anual,
              prestaciones.TotalRemuMensual AS num_remuneracion_mensual,
              prestaciones.TotalRemuDiaria AS num_remuneracion_diaria,
              prestaciones.MontoTrimestral AS num_monto_trimestral,
              prestaciones.MontoAnual AS num_monto_anual,
              prestaciones.Status AS num_estatus,
              prestaciones.MontoAcumulado AS num_monto_acumulado,
              1 AS fk_a018_num_seguridad_usuario,
              prestaciones.UltimaFecha AS fec_ultima_modificacion
            FROM
              pr_PrestacionesSocialesCalculo AS prestaciones
              INNER JOIN mastpersonas AS persona ON persona.CodPersona = prestaciones.CodPersona
            WHERE
              1
              
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetchAll();
    }

    public function metCargaPrestaciones($array)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
              nm_e002_prestaciones_sociales_calculo
                SET
                  fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                  fec_anio=:fec_anio,
                  fec_mes=:fec_mes,
                  num_sueldo_base=:num_sueldo_base,
                  num_total_asignaciones=:num_total_asignaciones,
                  
                  num_sueldo_normal=:num_sueldo_normal,
                  num_alicuota_vacacional=:num_alicuota_vacacional,
                  num_alicuota_fin_anio=:num_alicuota_fin_anio,
                  num_bono_especial=:num_bono_especial,
                  num_bono_fiscal=:num_bono_fiscal,
                  
                  num_dias_trimestre=:num_dias_trimestre,
                  num_dias_anual=:num_dias_anual,
                  num_remuneracion_mensual=:num_remuneracion_mensual,
                  num_remuneracion_diaria=:num_remuneracion_diaria,
                  num_monto_trimestral=:num_monto_trimestral,
                  
                  num_monto_anual=:num_monto_anual,
                  num_estatus=:num_estatus,
                  num_monto_acumulado=:num_monto_acumulado,
                  fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                  fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
              
              fec_ultima_modificacion=NOW()
            ");

        $registro->execute(array(
            'fk_nmb001_num_tipo_nomina' => $array['fk_nmb001_num_tipo_nomina'],
            'fec_anio' => $array['fec_anio'],
            'fec_mes' => $array['fec_mes'],
            'num_sueldo_base' => $array['num_sueldo_base'],
            'num_total_asignaciones' => $array['num_total_asignaciones'],

            'num_sueldo_normal' => $array['num_sueldo_normal'],
            'num_alicuota_vacacional' => $array['num_alicuota_vacacional'],
            'num_alicuota_fin_anio' => $array['num_alicuota_fin_anio'],
            'num_bono_especial' => $array['num_bono_especial'],
            'num_bono_fiscal' => $array['num_bono_fiscal'],

            'num_dias_trimestre' => $array['num_dias_trimestre'],
            'num_dias_anual' => $array['num_dias_anual'],
            'num_remuneracion_mensual' => $array['num_remuneracion_mensual'],
            'num_remuneracion_diaria' => $array['num_remuneracion_diaria'],
            'num_monto_trimestral' => $array['num_monto_trimestral'],

            'num_monto_anual' => $array['num_monto_anual'],
            'num_estatus' => $array['num_estatus'],
            'num_monto_acumulado' => $array['num_monto_acumulado'],
            'fk_rhb001_num_empleado' => $array['fk_rhb001_num_empleado'],
            'fk_a018_num_seguridad_usuario' => $this->atIdUsuario
        ));


        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metRetroactivoSociales()
    {
        $miscelaneoDetalle = $this->_db2->query("
            SELECT
              persona.Ndocumento,
              persona.DocFiscal,
              persona.Estado,
              Retroactivo.TipoNomina AS fk_nmb001_num_tipo_nomina,
              SUBSTRING(Retroactivo.Periodo,1, 4) AS fec_anio,
              SUBSTRING(Retroactivo.Periodo,6, 2) AS fec_mes,
              Retroactivo.CodPersona,
              Retroactivo.SueldoBase AS num_sueldo_base,
              Retroactivo.TotalAsig AS num_total_asignaciones,
              Retroactivo.SueldoNormal AS num_sueldo_normal,
              Retroactivo.AliVac AS num_alicuota_vacacional,
              Retroactivo.AliFin AS num_alicuota_fin_anio,
              Retroactivo.BonoEspecial AS num_bono_especial,
              Retroactivo.BonoFiscal AS num_bono_fiscal,
              Retroactivo.DiasTrimestral AS num_dias_trimestre,
              Retroactivo.DiasAnual AS num_dias_anual,
              Retroactivo.TotalRemuMensual AS num_remuneracion_mensual,
              Retroactivo.TotalRemuDiaria AS num_remuneracion_diaria,
              Retroactivo.MontoTrimestral AS num_monto_trimestral,
              Retroactivo.MontoAnual AS num_monto_anual,
              Retroactivo.Status AS num_estatus,
              1 AS fk_a018_num_seguridad_usuario,
              Retroactivo.UltimaFecha AS fec_ultima_modificacion
            FROM
              pr_PrestacionesSocialesCalculoRetroactivo AS Retroactivo
              INNER JOIN mastpersonas AS persona ON persona.CodPersona = Retroactivo.CodPersona
            WHERE
              1
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetchAll();
    }

    public function metCargaRetroactivos($array)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
              nm_e001_prestaciones_sociales_calculo_retroactivo
                SET
                  fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                  fec_anio=:fec_anio,
                  fec_mes=:fec_mes,
                  num_sueldo_base=:num_sueldo_base,
                  num_total_asignaciones=:num_total_asignaciones,
                  
                  num_sueldo_normal=:num_sueldo_normal,
                  num_alicuota_vacacional=:num_alicuota_vacacional,
                  num_alicuota_fin_anio=:num_alicuota_fin_anio,
                  num_bono_especial=:num_bono_especial,
                  num_bono_fiscal=:num_bono_fiscal,
                  
                  num_dias_trimestre=:num_dias_trimestre,
                  num_dias_anual=:num_dias_anual,
                  num_remuneracion_mensual=:num_remuneracion_mensual,
                  num_remuneracion_diaria=:num_remuneracion_diaria,
                  num_monto_trimestral=:num_monto_trimestral,
                  
                  num_monto_anual=:num_monto_anual,
                  num_estatus=:num_estatus,
                  num_monto_acumulado=:num_monto_acumulado,
                  fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                  fk_a018_num_seguridadusuario=:fk_a018_num_seguridadusuario,
              
              fec_ultima_modificacion=NOW()
            ");

        $registro->execute(array(
            'fk_nmb001_num_tipo_nomina' => $array['fk_nmb001_num_tipo_nomina'],
            'fec_anio' => $array['fec_anio'],
            'fec_mes' => $array['fec_mes'],
            'num_sueldo_base' => $array['num_sueldo_base'],
            'num_total_asignaciones' => $array['num_total_asignaciones'],

            'num_sueldo_normal' => $array['num_sueldo_normal'],
            'num_alicuota_vacacional' => $array['num_alicuota_vacacional'],
            'num_alicuota_fin_anio' => $array['num_alicuota_fin_anio'],
            'num_bono_especial' => $array['num_bono_especial'],
            'num_bono_fiscal' => $array['num_bono_fiscal'],

            'num_dias_trimestre' => $array['num_dias_trimestre'],
            'num_dias_anual' => $array['num_dias_anual'],
            'num_remuneracion_mensual' => $array['num_remuneracion_mensual'],
            'num_remuneracion_diaria' => $array['num_remuneracion_diaria'],
            'num_monto_trimestral' => $array['num_monto_trimestral'],

            'num_monto_anual' => $array['num_monto_anual'],
            'num_estatus' => $array['num_estatus'],
            'num_monto_acumulado' => NULL,
            'fk_rhb001_num_empleado' => $array['fk_rhb001_num_empleado'],
            'fk_a018_num_seguridadusuario' => $this->atIdUsuario
        ));


        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metAnticipoSociales(){
        $anticipo = $this->_db2->query("
                    SELECT
                      persona.Ndocumento,
                      persona.DocFiscal,
                      persona.Estado,
                      SUBSTRING(anticipo.Periodo,1, 4) AS fec_anio,
                      SUBSTRING(anticipo.Periodo,6, 2) AS fec_mes,                      
                      anticipo.CodPersona,
                      anticipo.Anticipo AS num_anticipo,
                      anticipo.justificativo AS fk_a006_num_miscelaneo_detalle_justificativo,
                      anticipo.Porcentaje AS ind_porcentaje,
                      anticipo.FechaAnticipo AS fec_anticipo,
                      anticipo.UltimaFecha AS fec_ultima_modificacion,
                      1 AS fk_a018_num_seguridad_usuario
                    FROM
                      pr_PrestacionesSocialesAnticipo AS anticipo
                      INNER JOIN mastpersonas AS persona ON persona.CodPersona = anticipo.CodPersona
                    WHERE 1
                    ");
        $anticipo->setFetchMode(PDO::FETCH_ASSOC);
        return $anticipo->fetchAll();

    }

    public function metCargaAnticipo($array)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
                       INSERT INTO
                          nm_e003_prestaciones_sociales_anticipo
                       SET 
                          fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                          fk_a006_num_miscelaneo_detalle_justificativo=:fk_a006_num_miscelaneo_detalle_justificativo,
                          fec_anio=:fec_anio,
                          fec_mes=:fec_mes,
                          num_anticipo=:num_anticipo,
                          ind_porcentaje=:ind_porcentaje,
                          fec_anticipo=:fec_anticipo,
                          fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                        fec_ultima_modificacion=NOW()
                          ");
        $registro->execute(array(
            'fk_rhb001_num_empleado' => $array['fk_rhb001_num_empleado'],
            'fk_a006_num_miscelaneo_detalle_justificativo' => $array['fk_a006_num_miscelaneo_detalle_justificativo'],
            'fec_anio' => $array['fec_anio'],
            'fec_mes' => $array['fec_mes'],
            'num_anticipo' => $array['num_anticipo'],
            'ind_porcentaje' => $array['ind_porcentaje'],
            'fec_anticipo' => $array['fec_anticipo'],
            'fk_a018_num_seguridad_usuario' => $this->atIdUsuario

        ));
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }




    public function metCargarPkEmpleado($persona)
    {
        $empleado = $this->_db->query("
              SELECT
                pk_num_empleado,
                fk_nmb001_num_tipo_nomina
              FROM
                rh_b001_empleado
              INNER JOIN a003_persona AS per ON rh_b001_empleado.fk_a003_num_persona = per.pk_num_persona
              INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
              WHERE
                  per.pk_num_persona = '$persona'
        ");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
              
    }
}