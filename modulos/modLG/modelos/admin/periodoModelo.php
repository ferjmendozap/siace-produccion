<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
class periodoModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metMostrarPeriodo($idPeriodo){
        $mostrarPeriodo = $this->_db->query("
              SELECT
                lg_b018_control_periodo.*,
                a018_seguridad_usuario.ind_usuario
              FROM
                lg_b018_control_periodo,a018_seguridad_usuario
              WHERE
                lg_b018_control_periodo.pk_num_periodo='$idPeriodo' and
                a018_seguridad_usuario.pk_num_seguridad_usuario=lg_b018_control_periodo.fk_a018_num_seguridad_usuario
        ");
        $mostrarPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarPeriodo->fetch();
    }

    public function metCrearPeriodo($mesPeriodo,$anioPeriodo,$tranPeriodo,$estadoPeriodo)
    {

        $this->_db->beginTransaction();
        $registroPeriodo = $this->_db->prepare("
          INSERT INTO
            lg_b018_control_periodo
          SET
              fec_mes=:fec_mes,
              fec_anio=:fec_anio,
              num_flag_transaccion=:num_flag_transaccion,
              num_estatus=:num_estatus,
              fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
              fec_ultima_modificacion=NOW()
           ");

        $registroPeriodo->execute(array(
            'fec_mes'=>$mesPeriodo,
            'fec_anio'=>$anioPeriodo,
            'num_flag_transaccion'=>$tranPeriodo,
            'num_estatus'=>$estadoPeriodo,
            'fk_a018_num_seguridad_usuario'=>$this->atIdUsuario
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroPeriodo->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarPeriodo($mesPeriodo,$anioPeriodo,$tranPeriodo,$estadoPeriodo,$idPeriodo)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
                      UPDATE
                          lg_b018_control_periodo
                      SET
                          fec_mes=:fec_mes,
                          fec_anio=:fec_anio,
                          num_flag_transaccion=:num_flag_transaccion,
                          num_estatus=:num_estatus,
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                          fec_ultima_modificacion=NOW()
                      WHERE
                          pk_num_periodo='$idPeriodo'
            ");
        $modificarRegistro->execute(array(
            'fec_mes'=>$mesPeriodo,
            'fec_anio'=>$anioPeriodo,
            'num_flag_transaccion'=>$tranPeriodo,
            'num_estatus'=>$estadoPeriodo
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idPeriodo;
        }
    }

    public function metEliminarPeriodo($idPeriodo){
        $this->_db->beginTransaction();
        $eliminaridPeriodo=$this->_db->prepare("
            DELETE FROM
            lg_b018_control_periodo
            WHERE
            pk_num_periodo=:pk_num_periodo
            ");
        $eliminaridPeriodo->execute(array(
            'pk_num_periodo'=>$idPeriodo
        ));

        $error = $eliminaridPeriodo->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idPeriodo;
        }
    }

}

?>