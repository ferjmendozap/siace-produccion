<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'familiasModelo.php';
class subFamiliasModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atFamiliasModelo = new familiasModelo();
    }

    public function metListarSubFamilias()
    {
        $subFamiliasPost = $this->_db->query(
            "SELECT
              *
              FROM lg_b007_clase_subfamilia
          ");

        $subFamiliasPost->setFetchMode(PDO::FETCH_ASSOC);

        return $subFamiliasPost->fetchAll();
    }

    public function metMostrarSubFamilias($idSubFamilias){
        $mostrarSubFamilias = $this->_db->query("
          SELECT
            sf.*,
            s.ind_usuario
          FROM
            lg_b007_clase_subfamilia AS sf
            INNER JOIN a018_seguridad_usuario AS s ON sf.fk_a018_num_seguridad_usuario = s.pk_num_seguridad_usuario
          WHERE
            pk_num_subfamilia='$idSubFamilias'
        ");
        $mostrarSubFamilias->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarSubFamilias->fetch();
    }

    public function metCrearSubFamilias($codigo,$desc,$estado,$familia)
    {
        $this->_db->beginTransaction();
        $registroSubFamilias = $this->_db->prepare("
          INSERT INTO
          lg_b007_clase_subfamilia
          SET
            ind_cod_subfamilia=:ind_cod_subfamilia,
            ind_descripcion=:ind_descripcion,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario=$this->atIdUsuario,
            fec_ultima_modificacion=NOW(),
            fk_lgb006_num_clase_familia=:fk_lgb006_num_clase_familia
          ");

        $registroSubFamilias->execute(array(
            'ind_cod_subfamilia'=>$codigo,
            'ind_descripcion'=>$desc,
            'num_estatus'=>$estado,
            'fk_lgb006_num_clase_familia'=>$familia
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroSubFamilias->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarSubFamilias($codigo,$desc,$estado,$familia,$idSubFamilias)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
                UPDATE
                  lg_b007_clase_subfamilia
                SET
            ind_cod_subfamilia=:ind_cod_subfamilia,
            ind_descripcion=:ind_descripcion,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario=$this->atIdUsuario,
            fec_ultima_modificacion=NOW(),
            fk_lgb006_num_clase_familia=:fk_lgb006_num_clase_familia
                WHERE
                  pk_num_subfamilia='$idSubFamilias'
            ");
        $modificarRegistro->execute(array(
            'ind_cod_subfamilia'=>$codigo,
            'ind_descripcion'=>$desc,
            'num_estatus'=>$estado,
            'fk_lgb006_num_clase_familia'=>$familia
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idSubFamilias;
        }
    }

    public function metEliminarSubFamilias($idSubFamilias){
        $this->_db->beginTransaction();
        $eliminarSubFamilias=$this->_db->prepare("
            DELETE FROM
            lg_b007_clase_subfamilia
            WHERE
            pk_num_subfamilia=:pk_num_subfamilia
            ");
        $eliminarSubFamilias->execute(array(
            'pk_num_subfamilia'=>$idSubFamilias
        ));

        $error = $eliminarSubFamilias->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idSubFamilias;
        }
    }

    public function metContarSubFamilias($idFamilia)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              count(*) AS cantidad
            FROM
            lg_b007_clase_subfamilia
            WHERE
            fk_lgb006_num_clase_familia='$idFamilia'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }


}

?>