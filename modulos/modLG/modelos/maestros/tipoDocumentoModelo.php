<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
class tipoDocumentoModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }
    public function metListaPersona()
    {
        $persona = $this->_db->query(
            "SELECT
              pk_num_persona,
              ind_cedula_documento,
              ind_nombre1,
              ind_apellido1
            FROM
              a003_persona"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }
    public function metListarTipoDoc()
    {
        $TipoDocPost = $this->_db->query(
            "SELECT
              *
              FROM lg_b016_tipo_documento
          ");

        $TipoDocPost->setFetchMode(PDO::FETCH_ASSOC);

        return $TipoDocPost->fetchAll();
    }

    public function metMostrarTipoDoc($idTipoDoc){
        $mostrarTipoDoc = $this->_db->query("
              SELECT
                lg_b016_tipo_documento.*,
                a018_seguridad_usuario.ind_usuario
              FROM
                lg_b016_tipo_documento,a018_seguridad_usuario
              WHERE
                lg_b016_tipo_documento.pk_num_tipo_documento='$idTipoDoc' and
                a018_seguridad_usuario.pk_num_seguridad_usuario=lg_b016_tipo_documento.fk_a018_num_seguridad_usuario
        ");
        $mostrarTipoDoc->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTipoDoc->fetch();
    }

    public function metCrearTipoDoc($codTipoDoc,$descTipoDoc,$tranTipoDoc,$fiscalTipoDoc,$estadoTipoDoc)
    {
        $this->_db->beginTransaction();
        $registroTipoDoc = $this->_db->prepare("
          INSERT INTO
          lg_b016_tipo_documento
          SET
          cod_tipo_documento=:cod_tipo_documento,
          ind_descripcion=:ind_descripcion,
          num_flag_documento_fiscal=:num_flag_documento_fiscal,
          num_flag_transaccion_sistema=:num_flag_transaccion_sistema,
          num_estatus=:num_estatus,
          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
          fec_ultima_modificacion=NOW()
          ");

        $registroTipoDoc->execute(array(
            'cod_tipo_documento'=>$codTipoDoc,
            'ind_descripcion'=>$descTipoDoc,
            'num_flag_documento_fiscal'=>$fiscalTipoDoc,
            'num_flag_transaccion_sistema'=>$tranTipoDoc,
            'num_estatus'=>$estadoTipoDoc
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroTipoDoc->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarTipoDoc($codTipoDoc,$descTipoDoc,$tranTipoDoc,$fiscalTipoDoc,$estadoTipoDoc,$idTipoDoc)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
                      UPDATE
                         lg_b016_tipo_documento
                      SET
                        cod_tipo_documento=:cod_tipo_documento,
                        ind_descripcion=:ind_descripcion,
                        num_flag_documento_fiscal=:num_flag_documento_fiscal,
                        num_flag_transaccion_sistema=:num_flag_transaccion_sistema,
                        num_estatus=:num_estatus,
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                      WHERE
                        pk_num_tipo_documento='$idTipoDoc'
            ");
        $modificarRegistro->execute(array(
            'cod_tipo_documento'=>$codTipoDoc,
            'ind_descripcion'=>$descTipoDoc,
            'num_flag_documento_fiscal'=>$fiscalTipoDoc,
            'num_flag_transaccion_sistema'=>$tranTipoDoc,
            'num_estatus'=>$estadoTipoDoc
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idTipoDoc;
        }
    }

    public function metEliminarTipoDoc($idTipoDoc){
        $this->_db->beginTransaction();
        $eliminaridTipoDoc=$this->_db->prepare("
            DELETE FROM
            lg_b016_tipo_documento
            WHERE
            pk_num_tipo_documento=:pk_num_tipo_documento
            ");
        $eliminaridTipoDoc->execute(array(
            'pk_num_tipo_documento'=>$idTipoDoc
        ));

        $error = $eliminaridTipoDoc->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idTipoDoc;
        }
    }


}

?>