<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
class almacenModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
    }

    public function metListaEmpleado()
    {
        $personaPost = $this->_db->query("
            SELECT
              persona.*,
              empleado.pk_num_empleado,
              dependencia.ind_dependencia
            FROM
              a003_persona as persona 
              INNER JOIN rh_b001_empleado AS empleado ON persona.pk_num_persona = empleado.fk_a003_num_persona
              LEFT JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
              LEFT JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
              and empleado.ind_estado_aprobacion = 'ap'
              and empleado.num_estatus = 1
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetchAll();
    }

    public function metSelectPlan()
    {
        $personaPost = $this->_db->query("
            SELECT
              *
            FROM
              cb_b004_plan_cuenta
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetchAll();
    }

    public function metListarAlmacen()
    {
        $almacenPost = $this->_db->query(
            "SELECT
              a.*,
              usu.ind_usuario
              FROM lg_b014_almacen AS a
              INNER JOIN a018_seguridad_usuario AS usu ON usu.pk_num_seguridad_usuario=a.fk_a018_num_seguridad_usuario
          ");
        $almacenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $almacenPost->fetchAll();
    }

    public function metMostrarAlmacen($idAlmacen){
        $mostrarAlmacen = $this->_db->query("
            SELECT
                a.*,
                s.ind_usuario,
                p.pk_num_persona,
                p.ind_nombre1,
                p.ind_apellido1,
                p.ind_cedula_documento,
                e.pk_num_empleado
            FROM
                lg_b014_almacen AS a
                INNER JOIN a018_seguridad_usuario AS s
                INNER JOIN a003_persona AS p
                INNER JOIN rh_b001_empleado AS e ON s.pk_num_seguridad_usuario = a.fk_a018_num_seguridad_usuario
                and p.pk_num_persona = a.fk_a003_num_persona_dependencia_resp
                and e.pk_num_empleado = a.fk_rhb001_num_empleado_responsable
                and p.pk_num_persona=e.fk_a003_num_persona
                and a.pk_num_almacen = '$idAlmacen'
        ");
        $mostrarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarAlmacen->fetch();
    }

    public function metCrearAlmacen($codAlmacen,$descAlmacen,$dirAlmacen,$estadoAlmacen,$tipoAlmacen,$comAlmacen,$cuenta,$empleado,$respon)
    {
        $this->_db->beginTransaction();
        $registroAlmacen = $this->_db->prepare("
          INSERT INTO
          lg_b014_almacen
          SET
          cod_almacen=:cod_almacen,
          ind_descripcion=:ind_descripcion,
          ind_tipo_almacen=:ind_tipo_almacen,
          ind_direccion=:ind_direccion,
          num_flag_commodity=:num_flag_commodity,
          fk_cbb004_num_cuenta_inventario=:fk_cbb004_num_cuenta_inventario,
          num_estatus=:num_estatus,
          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
          fec_ultima_modificacion=NOW(),
          fk_rhb001_num_empleado_responsable=:fk_rhb001_num_empleado_responsable,
          fk_a003_num_persona_dependencia_resp=:fk_a003_num_persona_dependencia_resp
          ");

        $registroAlmacen->execute(array(
            'cod_almacen'=>$codAlmacen,
            'ind_descripcion'=>$descAlmacen,
            'ind_tipo_almacen'=>$tipoAlmacen,
            'ind_direccion'=>$dirAlmacen,
            'num_flag_commodity'=>$comAlmacen,
            'fk_cbb004_num_cuenta_inventario'=>$cuenta,
            'num_estatus'=>$estadoAlmacen,
            'fk_a003_num_persona_dependencia_resp'=>$empleado,
            'fk_rhb001_num_empleado_responsable'=>$respon
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroAlmacen->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarAlmacen($codAlmacen,$descAlmacen,$dirAlmacen,$estadoAlmacen,$tipoAlmacen,$comAlmacen,$cuenta,$empleado,$respon,$idAlmacen)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
                      UPDATE
                         lg_b014_almacen
                      SET
                        cod_almacen=:cod_almacen,
                        ind_descripcion=:ind_descripcion,
                        ind_tipo_almacen=:ind_tipo_almacen,
                        ind_direccion=:ind_direccion,
                        num_flag_commodity=:num_flag_commodity,
                        fk_cbb004_num_cuenta_inventario=:fk_cbb004_num_cuenta_inventario,
                        num_estatus=:num_estatus,
                        fec_ultima_modificacion=NOW(),
                        fk_rhb001_num_empleado_responsable=:fk_rhb001_num_empleado_responsable,
                        fk_a003_num_persona_dependencia_resp=:fk_a003_num_persona_dependencia_resp
                      WHERE
                        pk_num_almacen='$idAlmacen'
            ");
        $modificarRegistro->execute(array(
            'cod_almacen'=>$codAlmacen,
            'ind_descripcion'=>$descAlmacen,
            'ind_tipo_almacen'=>$tipoAlmacen,
            'ind_direccion'=>$dirAlmacen,
            'num_flag_commodity'=>$comAlmacen,
            'fk_cbb004_num_cuenta_inventario'=>$cuenta,
            'num_estatus'=>$estadoAlmacen,
            'fk_a003_num_persona_dependencia_resp'=>$empleado,
            'fk_rhb001_num_empleado_responsable'=>$respon
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idAlmacen;
        }
    }

    public function metEliminarAlmacen($idAlmacen){
        $this->_db->beginTransaction();
        $eliminarAlmacen=$this->_db->prepare("
            DELETE FROM
            lg_b014_almacen
            WHERE
            pk_num_almacen=:pk_num_almacen
            ");
        $eliminarAlmacen->execute(array(
            'pk_num_almacen'=>$idAlmacen
        ));

        $error = $eliminarAlmacen->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idAlmacen;
        }
    }

}

?>