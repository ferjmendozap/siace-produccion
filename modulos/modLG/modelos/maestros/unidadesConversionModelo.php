<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
class unidadesConversionModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarUnidadesC()
    {
        $unidadesPost = $this->_db->query("
            SELECT
                uc.*,
                u.ind_descripcion
            FROM
                lg_c002_unidades_conversion AS uc
                INNER JOIN lg_b004_unidades AS u
            WHERE
                u.pk_num_unidad=uc.fk_lgb004_num_unidad
            ORDER BY (uc.num_cantidad) ASC
          ");
        $unidadesPost->setFetchMode(PDO::FETCH_ASSOC);
        return $unidadesPost->fetchAll();
    }

    public function metListarUnidades()
    {
        $unidadesPost = $this->_db->query("
            SELECT
                *
            FROM
                lg_b004_unidades
          ");
        $unidadesPost->setFetchMode(PDO::FETCH_ASSOC);
        return $unidadesPost->fetchAll();
    }

    public function metMostrarUnidadesC($idUnidades){
        $mostrarUnidades = $this->_db->query("
            SELECT
              u.ind_descripcion,
              uc.*,
              s.ind_usuario
            FROM
              lg_c002_unidades_conversion AS uc
              INNER JOIN lg_b004_unidades AS u
              INNER JOIN a018_seguridad_usuario AS s ON s.pk_num_seguridad_usuario = u.fk_a018_num_seguridad_usuario
              and uc.pk_num_unidad_conversion = '$idUnidades'
        ");
        $mostrarUnidades->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarUnidades->fetch();
    }

    public function metCrearUnidadesC($cantUnidades,$unidad,$estadoUnidades)
    {
        $this->_db->beginTransaction();
        $registroUnidades = $this->_db->prepare("
          INSERT INTO
            lg_c002_unidades_conversion
          SET
            num_cantidad=:num_cantidad,
            fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          ");

        $registroUnidades->execute(array(
            'num_cantidad'=>$cantUnidades,
            'fk_lgb004_num_unidad'=>$unidad,
            'num_estatus'=>$estadoUnidades
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroUnidades->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarUnidadesC($cantUnidades,$unidad,$estadoUnidades,$idUnidades)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
          UPDATE
            lg_c002_unidades_conversion
          SET
            num_cantidad=:num_cantidad,
            fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          WHERE
            pk_num_unidad_conversion='$idUnidades'
            ");
        $modificarRegistro->execute(array(
            'num_cantidad'=>$cantUnidades,
            'fk_lgb004_num_unidad'=>$unidad,
            'num_estatus'=>$estadoUnidades
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idUnidades;
        }
    }

    public function metEliminarUnidadesc($idUnidades){
        $this->_db->beginTransaction();
        $eliminarUnidades=$this->_db->prepare("
            DELETE FROM
            lg_c002_unidades_conversion
            WHERE
            pk_num_unidad_conversion=:pk_num_unidad_conversion
            ");
        $eliminarUnidades->execute(array(
            'pk_num_unidad_conversion'=>$idUnidades
        ));

        $error = $eliminarUnidades->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idUnidades;
        }
    }

}

?>