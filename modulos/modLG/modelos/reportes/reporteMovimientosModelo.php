<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'almacenModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'itemsModelo.php';
class reporteMovimientosModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atAlmacenModelo = new almacenModelo();
        $this->atItemsModelo = new itemsModelo();
    }

    public function metBuscarRecibidoEnviado($idItem,$cual,$where=false)
    {
        $sql = "
            SELECT
              SUM(IF (detalle.fk_lgb004_num_unidad!=item.fk_lgb004_num_unidad_despacho AND detalle2.cod_detalle = 'I', detalle.num_cantidad_transaccion * conversion.num_cantidad, detalle.num_cantidad_transaccion)) AS cantidad
            FROM
              lg_d002_transaccion_detalle AS detalle
            INNER JOIN
              lg_d001_transaccion AS transaccion ON transaccion.pk_num_transaccion = detalle.fk_lgd001_num_transaccion
            INNER JOIN
              lg_b015_tipo_transaccion AS tipo_tran ON tipo_tran.pk_num_tipo_transaccion = transaccion.fk_lgb015_num_tipo_transaccion
            INNER JOIN
              a006_miscelaneo_detalle AS detalle2 ON detalle2.pk_num_miscelaneo_detalle = tipo_tran.fk_a006_num_miscelaneo_detalle_tipo_documento
            INNER JOIN
              lg_b002_item AS item ON detalle.fk_lgb002_num_item = item.pk_num_item
            INNER JOIN
              lg_c002_unidades_conversion AS conversion ON item.fk_lgc002_num_unidad_conversion = conversion.pk_num_unidad_conversion
            WHERE detalle.fk_lgb002_num_item = '$idItem' AND detalle2.cod_detalle = '$cual' 
              $where
              ";
        #var_dump($sql);
        #exit;
        $personaPost = $this->_db->query($sql);
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetch();
    }

    public function metMostrarEmpleado($idEmpleado){
        $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              per.ind_documento_fiscal,
              puesto.ind_descripcion_cargo
            FROM rh_b001_empleado AS emp
            INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona
            LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
              emp.pk_num_empleado='$idEmpleado'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }
}

?>