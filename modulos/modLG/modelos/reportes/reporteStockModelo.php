<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'clasificacionModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'commodityModelo.php';
class reporteStockModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atCommodityModelo = new commodityModelo();
        $this->atClasificacionModelo = new clasificacionModelo();
    }

    public function metListarStock($filtro){

        $actaPost =  $this->_db->query("
            SELECT
              item.*,
              item.ind_descripcion AS descripcionItem,
              unidades.ind_descripcion AS unidad,
              stock.*
            FROM lg_b002_item AS item
            INNER JOIN lg_b004_unidades AS unidades ON item.fk_lgb004_num_unidad_compra = unidades.pk_num_unidad
            INNER JOIN lg_c007_item_stock AS stock ON stock.pk_num_item_stock = item.fk_lgc007_num_item_stock
            INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = item.fk_a006_num_miscelaneo_tipo_item
            WHERE 1 $filtro
             ");

        $actaPost->setFetchMode(PDO::FETCH_ASSOC);
        return $actaPost->fetchAll();
    }
    public function metMostrarEmpleado($idEmpleado){
        $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              per.ind_documento_fiscal,
              puesto.ind_descripcion_cargo
            FROM rh_b001_empleado AS emp
            INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona
            LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
              emp.pk_num_empleado='$idEmpleado'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }


}

?>