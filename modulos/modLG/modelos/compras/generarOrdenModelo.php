<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'compras' . DS . 'requerimientoModelo.php';

class generarOrdenModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atRequerimientoModelo = new requerimientoModelo();
    }

    public function metSumarMontosOrdenesPendientes()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
            min(cotizacion.num_total) AS minimo,
            detalle2.*
            FROM
            lg_c003_cotizacion AS cotizacion
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            INNER JOIN lg_c005_adjudicacion_detalle AS detalle2 ON detalle.pk_num_acta_detalle = detalle2.fk_lgb023_num_acta_detalle
            GROUP BY detalle.pk_num_acta_detalle
          ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetchAll();
    }

    public function metListarOrdenesPendientes()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              adjudicacion.*,
              detalle2.*,
              proveedor.*,
              persona.*,
              orden.pk_num_orden,
              acta.ind_codigo_acta,
              detalle3.ind_nombre_detalle AS adjudicacion
            FROM
              lg_b013_adjudicacion AS adjudicacion
              INNER JOIN a006_miscelaneo_detalle AS detalle3 ON detalle3.pk_num_miscelaneo_detalle = adjudicacion.fk_a006_num_miscelaneo_detalle_tipo_adjsudicacion
            INNER JOIN lg_c005_adjudicacion_detalle AS detalle2 ON adjudicacion.pk_num_adjudicacion = detalle2.fk_lgb013_num_adjudicacion
            INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON recomendacion.pk_num_informe_recomendacion = adjudicacion.fk_lgb012_num_informe_recomendacion
            INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
            INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
            
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
            
            INNER JOIN lg_c006_proveedor_recomendado AS recomendado ON recomendacion.pk_num_informe_recomendacion = recomendado.fk_lgb012_num_informe_recomendacion
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = detalle2.fk_lgb022_num_proveedor_adjudicado
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            INNER JOIN lg_c003_cotizacion AS cotizacion ON detalle2.fk_lgb023_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            LEFT JOIN lg_b019_orden AS orden ON orden.fk_lgb013_num_adjudicacion = adjudicacion.pk_num_adjudicacion
            WHERE acta.ind_estado = 'PR' AND recomendacion.ind_estado = 'AP'
            GROUP BY adjudicacion.pk_num_adjudicacion
            ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetchAll();
    }

    public function metListarOrdenComprasPendientes()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              recomendacion.*,
              acta.pk_num_acta_inicio
            FROM
              lg_b012_informe_recomendacion AS recomendacion
              INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
              WHERE recomendacion.ind_estado <> 'AP'
              AND acta.ind_estado <> 'DS'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetchAll();
    }

    public function metListarDependencias()
{
    $dependencias = $this->_db->query("
                SELECT
                  dependencia.*
                FROM
                  a004_dependencia AS dependencia
                INNER JOIN
                  a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = dependencia.pk_num_dependencia
                LEFT JOIN
                  rh_c076_empleado_organizacion AS organizacion ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                WHERE 
                  (
                  seguridad.fk_a018_num_seguridad_usuario = '$this->atIdUsuario' 
                  AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                  ) 
                  OR dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                  GROUP BY dependencia.pk_num_dependencia
                ");
    $dependencias->setFetchMode(PDO::FETCH_ASSOC);
    return $dependencias->fetchAll();
}

    public function metListarTipoServicio()
    {
        /*
        AS cb017ts
        INNER JOIN lg_e003_proveedor_servicio AS le003ps ON cb017ts.pk_num_tipo_servico = le003ps.fk_cpb017_num_tipo_servicio
        WHERE le003ps.fk_lgb022_num_proveedor='$idProveedor'
        */
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
              cp_b017_tipo_servicio
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetchAll();
    }

    public function metListarFormaPago()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
              a001_organismo
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetchAll();
    }

    public function metBuscarImpuesto($cod)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              impuesto.*
            FROM
              cp_b017_tipo_servicio AS servicio
              INNER JOIN cp_b020_servicio_impuesto AS comun ON servicio.pk_num_tipo_servico = comun.fk_cpb017_num_tipo_servicio
              INNER JOIN cp_b015_impuesto AS impuesto ON impuesto.pk_num_impuesto = comun.fk_cpb015_num_impuesto
            WHERE
              servicio.cod_tipo_servicio = '$cod'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetchAll();
    }

    public function metBuscarPartidaCuenta($codigo)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              *
            FROM
              pr_b002_partida_presupuestaria AS presupuestaria 
              INNER JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = presupuestaria.fk_cbb004_num_plan_cuenta_onco
            WHERE
              cod_partida = '$codigo'
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetch();
    }

    public function metBuscarCentroCosto($id)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              costo.pk_num_centro_costo
            FROM
              a023_centro_costo AS costo
              INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = costo.fk_a004_num_dependencia
            WHERE
              dependencia.pk_num_dependencia = '$id'
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetch();
    }

    public function metNuevaOrden(
        $dependencia,$centro_costo,$clasificacion,$almacen,
        $proveedor,$orden,$tipoOrden,
        $servicio,$bruto,$igv,
        $mTotal,$pendiente,$afecto,
        $noAfecto,$formaPago,$plazo,
        $entrega,$direccion,$comentario,
        $descripcion,$cuenta,
        $partida,$adjudicacion,
        $sec,$cant,$precio,
        $montoPU,$descPorc,$descFijo,
        $exonerado,$total,$cuentaDetalle,
        $partidaDetalle,$unidad,$cCosto,
        $item,$commodity,$actaDet
)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "INSERT INTO
                lg_b019_orden
              SET
                fec_mes=:fec_mes,
                fec_anio=:fec_anio,
                ind_orden=:ind_orden,
                ind_tipo_orden=:ind_tipo_orden,
                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                fec_prometida=:fec_prometida,
                fk_rhb001_num_empleado_creado_por='$this->atIdEmpleado',
                fec_creacion=NOW(),
                fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                num_monto_bruto=:num_monto_bruto,
                num_monto_igv=:num_monto_igv,
                num_monto_total=:num_monto_total,
                num_monto_pendiente=:num_monto_pendiente,

                num_monto_afecto=:num_monto_afecto,
                num_monto_no_afecto=:num_monto_no_afecto,
                fk_a006_num_miscelaneos_forma_pago=:fk_a006_num_miscelaneos_forma_pago,
                fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                num_plazo_entrega=:num_plazo_entrega,
                ind_entregar_en=:ind_entregar_en,
                ind_direccion_entrega=:ind_direccion_entrega,
                ind_comentario=:ind_comentario,
                ind_descripcion=:ind_descripcion,
                ind_estado=:ind_estado,

                fk_cbb004_num_plan_cuenta_oncop=:fk_cbb004_num_plan_cuenta_oncop,
                fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_lgb013_num_adjudicacion=:fk_lgb013_num_adjudicacion,
                fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor,
                fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
              ");
        #execute — Ejecuta una sentencia preparada

        $NuevoPost->execute(array(
                'fec_mes'=>date('m'),
                'fec_anio'=>date('Y'),
                'ind_orden'=>$orden,
                'ind_tipo_orden'=>$tipoOrden,
                'fk_a004_num_dependencia'=>$dependencia,
                'fk_a023_num_centro_costo'=>$centro_costo,
                'fec_prometida'=>date('Y'),
                'fk_cpb017_num_tipo_servicio'=>$servicio,
                'num_monto_bruto'=>$bruto,
                'num_monto_igv'=>$igv,
                'num_monto_total'=>$mTotal,
                'num_monto_pendiente'=>$pendiente,

                'num_monto_afecto'=>$afecto,
                'num_monto_no_afecto'=>$noAfecto,
                'fk_a006_num_miscelaneos_forma_pago'=>$formaPago,
                'fk_lgb014_num_almacen'=>$almacen,
                'num_plazo_entrega'=>$plazo,
                'ind_entregar_en'=>$entrega,
                'ind_direccion_entrega'=>$direccion,
                'ind_comentario'=>$comentario,
                'ind_descripcion'=>$descripcion,
                'ind_estado'=>'PR',

                'fk_cbb004_num_plan_cuenta_oncop'=>$cuenta,
                'fk_cbb004_num_plan_cuenta_pub_veinte'=>$cuenta,
                'fk_prb002_num_partida_presupuestaria'=>$partida,
                'fk_lgb013_num_adjudicacion'=>$adjudicacion,
                'fk_lgb022_num_proveedor'=>$proveedor,
                'fk_lgb017_num_clasificacion'=>$clasificacion
        ));

        $numOrden=$this->_db->lastInsertId();
        $NuevoPostDetalle=$this->_db->prepare(
            "INSERT INTO
                lg_c009_orden_detalle
              SET
                num_secuencia=:num_secuencia,
				fec_anio=:fec_anio,
				fec_mes=:fec_mes,
				num_cantidad=:num_cantidad,
				num_precio_unitario=:num_precio_unitario,

				num_precio_unitario_iva=:num_precio_unitario_iva,
				num_descuento_porcentaje=:num_descuento_porcentaje,
				num_descuento_fijo=:num_descuento_fijo,
				num_monto_base=:num_monto_base,
				num_total=:num_total,
				num_flag_exonerado=:num_flag_exonerado,
				fk_cbb004_plan_cuenta_oncop=:fk_cbb004_plan_cuenta_oncop,

				fk_cbb004_plan_cuenta_pub_veinte=:fk_cbb004_plan_cuenta_pub_veinte,
				fk_prb002_partida_presupuestaria=:fk_prb002_partida_presupuestaria,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
				fk_lgb004_num_unidad=:fk_lgb004_num_unidad,

				fk_lgb019_num_orden='$numOrden',
				fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
				fk_lgb002_num_item=:fk_lgb002_num_item,
				fk_lgb003_num_commodity=:fk_lgb003_num_commodity,
				fk_lgb023_num_acta_detalle=:fk_lgb023_num_acta_detalle
              ");

        $contador=1;
        while ($contador<=$sec) {
            $NuevoPostDetalle->execute(array(
                'num_secuencia'=>$contador,
                'fec_anio'=>date('Y'),
                'fec_mes'=>date('m'),
				'num_cantidad'=>$cant[$contador],
				'num_precio_unitario'=>$precio[$contador],

				'num_precio_unitario_iva'=>$montoPU[$contador],
				'num_descuento_porcentaje'=>$descPorc[$contador],
				'num_descuento_fijo'=>$descFijo[$contador],
                'num_monto_base'=>$precio[$contador]*$cant[$contador],
				'num_total'=>$total[$contador],
				'num_flag_exonerado'=>$exonerado[$contador],
				'fk_cbb004_plan_cuenta_oncop'=>$cuentaDetalle[$contador],

				'fk_cbb004_plan_cuenta_pub_veinte'=>$cuentaDetalle[$contador],
				'fk_prb002_partida_presupuestaria'=>$partidaDetalle[$contador],
				'fk_lgb004_num_unidad'=>$unidad[$contador],
				'fk_a023_num_centro_costo'=>$cCosto,
                'fk_lgb002_num_item'=>$item[$contador],
				'fk_lgb003_num_commodity'=>$commodity[$contador],
				'fk_lgb023_num_acta_detalle'=>$actaDet[$contador]
            ));
            $contador=$contador+1;
        }

        #commit — Consigna una transacción

        $fallaTansaccion1 = $NuevoPost->errorInfo();
        $fallaTansaccion2 = $NuevoPostDetalle->errorInfo();

        if(!empty($fallaTansaccion1[1]) && !empty($fallaTansaccion1[2])){
            $this->_db->rollBack();
            return $fallaTansaccion1;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $NuevoPostDetalle;
        }

    }

    public function metMosrarAdjudicacion($idAdj)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              adjudicacion.*,
              requerimiento.*,
              recomendado.fk_lgb022_num_proveedor,
              persona.ind_documento_fiscal AS provCed,
              persona.ind_apellido1,
              persona.ind_nombre1,
              proveedor.*
            FROM
              lg_b013_adjudicacion AS adjudicacion
              INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON recomendacion.pk_num_informe_recomendacion = adjudicacion.fk_lgb012_num_informe_recomendacion
              INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
              INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON acta.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b001_requerimiento AS requerimiento ON requerimiento.pk_num_requerimiento = acta_requerimiento.fk_lgb001_num_requerimiento
              INNER JOIN lg_c001_requerimiento_detalle AS detalle ON requerimiento.pk_num_requerimiento = detalle.fk_lgb001_num_requerimiento
              INNER JOIN lg_c006_proveedor_recomendado AS recomendado ON recomendado.fk_lgb012_num_informe_recomendacion = recomendacion.pk_num_informe_recomendacion
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = recomendado.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              adjudicacion.pk_num_adjudicacion= '$idAdj'
            GROUP BY adjudicacion.pk_num_adjudicacion
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetch();
    }

    public function metMosrarAdjudicacionDetalle($idAdj)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              detalle.*,
              cotizacion.*,
              item.*,
              item.ind_descripcion AS ind_descripcion_item,
              cuenta1.pk_num_cuenta AS cuentaItem,
              cuenta1.cod_cuenta AS codCuentaItem,
              cuenta1.ind_descripcion AS descCuentaItem,
              presupuestaria1.pk_num_partida_presupuestaria AS partidaItem,
              presupuestaria1.cod_partida AS codPartidaItem,
              presupuestaria1.ind_denominacion AS descPartidaItem,
              unidad1.pk_num_unidad AS uniItem1,
              unidad1.ind_descripcion AS uniItem,
              stock1.num_stock_actual,
              commodity.*,
              commodity.ind_descripcion AS ind_descripcion_commodity,
              cuenta2.pk_num_cuenta AS cuentaComm,
              cuenta2.cod_cuenta AS codCuentaComm,
              cuenta2.ind_descripcion AS descCuentaComm,
              presupuestaria2.pk_num_partida_presupuestaria AS partidaComm,
              presupuestaria2.cod_partida AS codPartidaComm,
              presupuestaria2.ind_denominacion AS descPartidaComm,
              unidad2.ind_descripcion AS uniComm,
              stock2.num_cantidad AS num_stock_actual_comm,
              proveedor.pk_num_proveedor,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre
            FROM
              lg_c005_adjudicacion_detalle  AS detalle
              INNER JOIN lg_b023_acta_detalle AS detalle2 ON detalle2.pk_num_acta_detalle = detalle.fk_lgb023_num_acta_detalle
              INNER JOIN lg_c003_cotizacion AS cotizacion ON detalle2.fk_lgc003_num_cotizacion = cotizacion.pk_num_cotizacion
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = detalle.fk_lgb022_num_proveedor_adjudicado
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
              LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle2.fk_lgb002_num_item
              LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle2.fk_lgb003_num_commodity
              LEFT JOIN lg_c007_item_stock AS stock1 ON stock1.pk_num_item_stock = item.fk_lgc007_num_item_stock
              LEFT JOIN lg_c008_commodity_stock AS stock2 ON stock2.fk_lgb003_num_commodity = commodity.pk_num_commodity
              LEFT JOIN lg_b004_unidades AS unidad1 ON unidad1.pk_num_unidad = item.fk_lgb004_num_unidad_compra
              LEFT JOIN lg_b004_unidades AS unidad2 ON unidad2.pk_num_unidad = commodity.fk_lgb004_num_unidad
              LEFT JOIN cb_b004_plan_cuenta AS cuenta1 ON cuenta1.pk_num_cuenta = item.fk_cbb004_num_plan_cuenta_gasto_oncop
              LEFT JOIN cb_b004_plan_cuenta AS cuenta2 ON cuenta2.pk_num_cuenta = commodity.fk_cbb004_num_plan_cuenta
              LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria1 ON presupuestaria1.pk_num_partida_presupuestaria = item.fk_prb002_num_partida_presupuestaria
              LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria2 ON presupuestaria2.pk_num_partida_presupuestaria = commodity.fk_prb002_num_partida_presupuestaria
            WHERE
              detalle.fk_lgb013_num_adjudicacion = '$idAdj'
              GROUP BY cotizacion.fk_lgb023_num_acta_detalle
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetchAll();
    }

    public function metBuscarPartida($idPartida)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              presupuestoDet.*,
              presupuestaria.*,
              cuenta.*
            FROM
              pr_c002_presupuesto_det AS presupuestoDet
              INNER JOIN pr_b004_presupuesto AS presupuesto ON presupuesto.pk_num_presupuesto = presupuestoDet.fk_prb004_num_presupuesto
              INNER JOIN pr_b002_partida_presupuestaria AS presupuestaria ON presupuestaria.pk_num_partida_presupuestaria = presupuestoDet.fk_prb002_num_partida_presupuestaria
              INNER JOIN cb_b004_plan_cuenta AS cuenta ON presupuestaria.fk_cbb004_num_plan_cuenta_pub20 = cuenta.pk_num_cuenta
              LEFT JOIN lg_b002_item AS item ON cuenta.pk_num_cuenta = item.fk_cbb004_num_plan_cuenta_gasto_oncop
              LEFT JOIN lg_b003_commodity AS commodity ON cuenta.pk_num_cuenta = commodity.fk_cbb004_num_plan_cuenta_pub_veinte
              INNER JOIN lg_c005_adjudicacion_detalle AS detalle2 ON detalle2.fk_lgb023_num_acta_detalle
            WHERE
              presupuestaria.pk_num_partida_presupuestaria = '$idPartida'
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetchAll();
    }

    public function metBuscarPresupuestoDetalle($partida)
    {
        $anio=date('Y');
        $ordenCompraPost = $this->_db->query("
            SELECT
              pc002pd.pk_num_presupuesto_det,
              pc002pd.num_monto_ajustado,
              pc002pd.num_monto_compromiso,
              presupuestaria.cod_partida,
              presupuestaria.pk_num_partida_presupuestaria
            FROM
              pr_c002_presupuesto_det AS pc002pd
            INNER JOIN pr_b004_presupuesto AS presupuesto ON presupuesto.pk_num_presupuesto = pc002pd.fk_prb004_num_presupuesto
            INNER JOIN pr_b002_partida_presupuestaria AS presupuestaria ON presupuestaria.pk_num_partida_presupuestaria = pc002pd.fk_prb002_num_partida_presupuestaria
            WHERE
              presupuesto.fec_anio = '$anio'
              AND presupuestaria.cod_partida = '$partida'
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetch();
    }

    public function metBuscarCotizaciones($idAdj)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              detalle.*,
              detalle2.*,
              proveedor.pk_num_proveedor,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              cotizacion.*
            FROM
              lg_c003_cotizacion AS cotizacion
              INNER JOIN lg_b023_acta_detalle AS detalle2 ON detalle2.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
              INNER JOIN lg_c005_adjudicacion_detalle AS detalle ON detalle.fk_lgb023_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
              INNER JOIN lg_b010_invitacion AS invitacion ON cotizacion.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              detalle.fk_lgb013_num_adjudicacion = '$idAdj'
              ORDER BY detalle2.pk_num_acta_detalle, proveedor.pk_num_proveedor ASC
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetchAll();
    }

    public function metBuscarRequerimientos($idAdj)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              detalle.*,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              requerimiento.*
            FROM
              lg_c001_requerimiento_detalle AS detalle
              INNER JOIN lg_e009_acta_detalle_requerimiento_detalle AS detalle2 ON detalle.pk_num_requerimiento_detalle = detalle2.fk_lgc001_num_requerimiento_detalle
              INNER JOIN lg_b023_acta_detalle AS detalle3 ON detalle3.pk_num_acta_detalle = detalle2.fk_lgb023_num_acta_detalle
              INNER JOIN lg_c005_adjudicacion_detalle AS detalle4 ON detalle3.pk_num_acta_detalle = detalle4.fk_lgb023_num_acta_detalle
              INNER JOIN lg_b001_requerimiento AS requerimiento ON requerimiento.pk_num_requerimiento = detalle.fk_lgb001_num_requerimiento
              INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = requerimiento.fk_rhb001_num_empleado_preparado_por
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            WHERE
              detalle4.fk_lgb013_num_adjudicacion = '$idAdj'
              ORDER BY detalle.pk_num_requerimiento_detalle ASC
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetchAll();
    }

    public function metActualizarEstados($datos)
    {
        $this->_db->beginTransaction();

        $NuevoPost=$this->_db->prepare("
        UPDATE
            lg_b001_requerimiento
        SET
            ind_estado='CO',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_requerimiento=:pk_num_requerimiento
        ");

        $NuevoPost2=$this->_db->prepare("
        UPDATE
            lg_b012_informe_recomendacion
        SET
            ind_estado='CO',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_informe_recomendacion=:pk_num_informe_recomendacion
        ");

        $NuevoPost3=$this->_db->prepare("
        UPDATE
            lg_b009_acta_inicio
        SET
            ind_estado='CO',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_acta_inicio=:pk_num_acta_inicio
        ");
        foreach ($datos as $key=>$value) {
            $NuevoPost->execute(array(
                'pk_num_requerimiento'=>$value['pk_num_requerimiento']
            ));
            $NuevoPost2->execute(array(
                'pk_num_informe_recomendacion'=>$value['pk_num_informe_recomendacion']
            ));
            $NuevoPost3->execute(array(
                'pk_num_acta_inicio'=>$value['pk_num_acta_inicio']
            ));
        }
        $fallaTansaccion = $NuevoPost->errorInfo();


        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return 1;
        }
    }

    public function metBuscarActualizar($idAdj)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              recomendacion.pk_num_informe_recomendacion,
              inicio.pk_num_acta_inicio,
              requerimiento.pk_num_requerimiento
            FROM
              lg_b013_adjudicacion AS adjudicacion 
              INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON adjudicacion.fk_lgb012_num_informe_recomendacion = recomendacion.pk_num_informe_recomendacion
              AND recomendacion.ind_estado='AP'
              INNER JOIN lg_b011_evaluacion AS evaluacion ON recomendacion.fk_lgb011_num_evaluacion = evaluacion.pk_num_evaluacion 
              INNER JOIN lg_b009_acta_inicio AS inicio ON evaluacion.fk_lgb009_num_acta_inicio = inicio.pk_num_acta_inicio AND inicio.ind_estado!='AN'
              INNER JOIN lg_c011_acta_requerimiento AS requerimiento2 ON inicio.pk_num_acta_inicio = requerimiento2.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b001_requerimiento AS requerimiento ON requerimiento.pk_num_requerimiento = requerimiento2.fk_lgb001_num_requerimiento
              INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = requerimiento.fk_rhb001_num_empleado_preparado_por
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            WHERE
              adjudicacion.pk_num_adjudicacion = '$idAdj'
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetchAll();
    }

    public function metActualizarOrdenes($datos)
    {
        $this->_db->beginTransaction();

        $NuevoPost=$this->_db->prepare("
        UPDATE
            lg_b019_orden
        SET
            fk_lgb013_num_adjudicacion=:fk_lgb013_num_adjudicacion,
            num_flag_contrato='1',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_orden=:pk_num_orden
        ");

        foreach ($datos['idOrden'] as $key=>$value) {
            $NuevoPost->execute(array(
                'fk_lgb013_num_adjudicacion'=>$datos['idAdj'],
                'pk_num_orden'=>$value
            ));
        }

        $fallaTansaccion = $NuevoPost->errorInfo();


        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return 1;
        }
    }

    public function metBuscarOrden($idOrden)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              orden.*,
                  concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor,
                  case orden.ind_estado
                    when 'PR' then 'Preparado'
                    when 'RV' then 'Revisado'
                    when 'CN' then 'Conformado'
                    when 'AP' then 'Aprobado'
                    when 'AN' then 'Anulado'
                    when 'CE' then 'Cerrado'
                    when 'CO' then 'Completado'
                  end as ind_estado_nombre,
                  case orden.ind_tipo_orden
                    when 'OC' then 'Compra'
                    when 'OS' then 'Servicio'
                  end as ind_tipo_orden_nombre
            FROM
              lg_b019_orden AS orden 
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              orden.pk_num_orden = '$idOrden'
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetch();
    }

}

?>