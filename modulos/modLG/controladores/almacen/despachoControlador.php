<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class despachoControlador extends Controlador
{
    private $atDespacho;
    private $atOrdenCompraModelo;
    private $atEjecutarTranModelo;
    private $atTransaccionModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atDespacho= $this->metCargarModelo('despacho', 'almacen');
    }

    //Método para listar los requerimientos a despachar
    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        /*
        $this->atVista->assign('listado', $this->atDespacho->atOrdenCompraModelo->atRequerimientoModelo->metListarRequerimiento('AP','3'));
        $this->atVista->assign('listadoDet', $this->atDespacho->atOrdenCompraModelo->atRequerimientoModelo->metListarRequerimientoDetalle());
        */
        $this->atVista->metRenderizar('listado');
    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    }

    //Método para validar las cantidades de la transacción para cambiar su estado
    public function metValidarDetalleTransaccion($cantidadItem,$idReqDet,$cantiPedida,$cantidadNueva,$idReq,$idStock)
    {
        $contador=0;
        $c=0;
        while($contador<$cantidadItem){
            $contador=$contador+1;
            $detalle=$this->atDespacho->metBuscarDetalle($idReqDet[$contador]);

            if ($detalle['cantidad_total'] == $detalle['num_cantidad_pedida']){
                $id=$this->atDespacho->metActualizarEstadoDet($idReqDet[$contador],'CO');
                $c = $c+1;
            }
            $id3=$this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->metActualizarStock($cantidadNueva[$contador],$idStock[$contador],2);
        }
        $id2=$this->atDespacho->atOrdenCompraModelo->atRequerimientoModelo->metBuscarReqDetalle($idReq);
        $c1 = count($id2);
        $c2=0;
        foreach ($id2 AS $key=>$value){
            if($id2[$key]['ind_estado']=='CO'){
                $c2=$c2+1;
            }
        }
        if($c1==$c2){
            $id2=$this->atDespacho->atOrdenCompraModelo->atRequerimientoModelo->metActualizarRequerimiento('CO',$idReq);
        }
    }

    //Método para crear despacho
    public function metCrearDespacho()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $estado = $this->metObtenerTexto('estado');
        $idReq = $this->metObtenerInt('idReq');
        if($valido==1){
            $this->metValidarToken();
            if($estado=='aprobar'){
                $exepcion = array('comen');
            } else {
                $exepcion = array();
            }
            $ind = $this->metValidarFormArrayDatos('form','int');
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',$exepcion);
            $formula = $this->metValidarFormArrayDatos('form','formula');

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $txt);
            }
            $validacion = array_merge($validacion,$formula);
            $es = "1";
            if($estado=="despachar"){
                $es = "2";
            }
            if(isset($validacion['aprobar'])) {
                $c=1;
                $cantiPedida[$c] = array();
                $canti[$c] = array();
                $precio[$c] = array();
                $descripcion[$c] = array();
                $numItem[$c] = array();
                $sAnt[$c] = array();
                $despachada[$c] = array();
                $aprobar[$c] = array();
                $numUni[$c] = array();
                $idStock[$c] = array();
                $idDetalleReq[$c] = array();
                $idDetalleOrden[$c] = array();
                $validacion['cant'] = 0;

                foreach ($validacion['aprobar'] as $key=>$value) {
                    if($validacion['cantiPedida'][$key]=='error' OR $validacion['canti'][$key]==0){
                        $validacion['canti']='error';
                        $validacion['status']='errorCant2';
                        echo json_encode($validacion);
                        exit;
                    }
                    if($validacion['cantiPedida'][$key]>$validacion['canti'][$key]){
                        $validacion['cantidad']='error';
                        $validacion['status']='errorCant';
                        echo json_encode($validacion);
                        exit;
                    }
                    $stock = $this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->metBuscarStock($validacion['idStock'][$key],'I');
                    $disponible = $stock['num_stock_actual'];

                    if($disponible-$validacion['cantiPedida'][$key]<0){
                        $validacion['status']='errorStock';
                        $validacion['disponible']=$disponible;
                        $validacion['num_stock_actual']=$stock['num_stock_actual'][$key];
                        $validacion['icCantidad']=$validacion['canti'][$key];
                        echo json_encode($validacion);
                        exit;
                    }
                    $validacion['precio'][$key] = 0;

                    $cantiPedida[$c] = $validacion['cantiPedida'][$key];
                    $canti[$c] = $validacion['canti'][$key];
                    $precio[$c] = $validacion['precio'][$key];
                    $descripcion[$c] = $validacion['descripcion'][$key];
                    $numItem[$c] = $validacion['numItem'][$key];
                    $sAnt[$c] = $validacion['sAnt'][$key];
                    $despachada[$c] = $validacion['despachada'][$key];
                    $aprobar[$c] = 1;
                    $numUni[$c] = $validacion['numUni'][$key];
                    $idStock[$c] = $validacion['idStock'][$key];
                    $idDetalleReq[$c] = $validacion['idDetalleReq'][$key];

                    $idDetalleOrden[$c]=NULL;

                    $c++;
                }

                $validacion['cant'] = $c-1;
                $validacion['cantiPedida'] = $cantiPedida;
                $validacion['canti'] = $canti;
                $validacion['precio'] = $precio;
                $validacion['descripcion'] = $descripcion;
                $validacion['numItem'] = $numItem;
                $validacion['sAnt'] = $sAnt;
                $validacion['despachada'] = $despachada;
                $validacion['aprobar'] = $aprobar;
                $validacion['numUni'] = $numUni;
                $validacion['idStock'] = $idStock;
                $validacion['idDetalleReq'] = $idDetalleReq;
                $validacion['idDetalleOrden'] = $idDetalleOrden;
            } else {
                $validacion['canti']='error';
                $validacion['status']='errorSeleccion';
                echo json_encode($validacion);
                exit;

            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            $puedeTran = $this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->metBuscarPeriodo();

            if($idReq!=0 AND $es=='1' AND $puedeTran['num_estatus']==1){
                $id=$idReq;
                $contador=0;
                while($contador<$validacion['cant']){
                    $contador=$contador+1;
                    if($validacion['aprobar'][$contador]=="1"){
                        $id2=$this->atDespacho->metActualizarEstadoDet($validacion['idDetalleReq'][$contador],'PE');
                        $id3=$this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->metActualizarStock($validacion['canti'][$contador],$validacion['idStock'][$contador],1);
                    }
                }
                $id4=$this->atDespacho->metActualizarRequerimiento($idReq);

                $validacion['status']='modificar';

            } elseif($puedeTran['num_estatus']==1) {

                $tipoTran = $this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->metBuscarTipoTransaccion($validacion['tipoTran'],1);
                $nro = $this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->metMostrarNro($tipoTran['cod_detalle']);
                $validacion['nro'] = $this->metRellenarCeros(($nro['nro'] + 1),6);

                $id=$this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->metCrearTransaccion(
                    $validacion['nro'],$validacion['fdoc'],$validacion['cCosto'],
                    $validacion['docRef'],$validacion['comen'],0,
                    1,null,$es,
                    $validacion['tipoTran'],$validacion['almacen'],$validacion['tipoDoc'],
                    $validacion['cant'],$validacion['sAnt'],$validacion['cantiPedida'],
                    $validacion['precio'],$validacion['numItem'],0,
                    $validacion['numUni'],$validacion['idDetalleOrden'],$validacion['idDetalleReq']
                );
                if (!is_array($id)) {
                    $id1 = $this->metValidarDetalleTransaccion($validacion['cant'], $validacion['idDetalleReq'], $validacion['cantiPedida'], $validacion['canti'], $idReq, $validacion['idStock']);
                    $validacion['id1']=$id1;
                }

                $validacion['status']='modificar';

            } else {
                $validacion['status'] = 'periodo';
                echo json_encode($validacion);
                exit;
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }

            $validacion['idReq']=$idReq;
            $validacion['estado']=$estado;

            echo json_encode($validacion);
            exit;
        }
        $idDep = '';
        if($idReq!=0){
            $datosReq = $this->atDespacho->atOrdenCompraModelo->atRequerimientoModelo->metBuscarRequerimiento($idReq);
                $idDep = $datosReq['fk_a004_num_dependencia'];
            $this->atVista->assign('formDB',$datosReq);
            $this->atVista->assign('formDBI',$this->atDespacho->atOrdenCompraModelo->atRequerimientoModelo->metBuscarReqDetalle($idReq));
            $this->atVista->assign('idReq',$idReq);
            $this->atVista->assign('estado',$estado);
        }
        $this->atVista->assign('centroCosto',$this->atDespacho->atOrdenCompraModelo->atRequerimientoModelo->metListarCentroCosto($idDep));
        $this->atVista->assign('almacen',$this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('documento',$this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->atTipoDocumentoModelo->metListarTipoDoc());
        $this->atVista->assign('transaccion',$this->atDespacho->atEjecutarTranModelo->atTransaccionModelo->atTipoTransaccionModelo->metListarTipoTran());
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    //Método para crear transacción para redirigir a compras
    public function metRedirigir()
    {
        $this->metValidarToken();
        $ind = $this->metValidarFormArrayDatos('form','int');
        $txt = $this->metValidarFormArrayDatos('form','txt');
        $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');
        $formula = $this->metValidarFormArrayDatos('form','formula');
        $idReq = $this->metObtenerInt('idReq');
        if ($alphaNum != null && $ind == null && $txt == null) {
            $validacion = $alphaNum;
        } elseif ($alphaNum == null && $ind != null && $txt == null) {
            $validacion = $ind;
        } elseif ($alphaNum == null && $ind == null && $txt != null) {
            $validacion = $txt;
        } elseif ($alphaNum == null && $ind != null && $txt != null) {
            $validacion = array_merge($txt, $ind);
        } elseif ($alphaNum != null && $ind == null && $txt != null) {
            $validacion = array_merge($txt, $alphaNum);
        } elseif ($alphaNum != null && $ind != null && $txt == null) {
            $validacion = array_merge($ind, $alphaNum);
        } else {
            $validacion = array_merge($alphaNum, $ind, $txt);
        }
        $validacion = array_merge($validacion,$formula);

        $validacion['status'] = 'ok';

        $clasificacion = $this->atDespacho->metBuscarClasificacion();
        $validacion['clasificacion'] = $clasificacion['pk_num_clasificacion'];
        $dependencia = $this->atDespacho->metBuscarDependencia();
        $validacion['dependencia'] = $dependencia['pk_num_dependencia'];
        $validacion['centroCosto'] = $dependencia['pk_num_centro_costo'];


        $requerimiento = $this->atDespacho->metBuscarRequerimiento($idReq);

        $orden=$this->atDespacho->metContar(' AND fk_a004_num_dependencia = "'.$requerimiento['fk_a004_num_dependencia'].'"');
        $orden['cantidad'] = $orden['cantidad'] + 1;

        $codigo=$requerimiento['ind_codinterno']."-".$this->metRellenarCeros($orden['cantidad'],4)."-".date('Y');
        $validacion['codigo'] = $codigo;
        $validacion['dependencia'] = $requerimiento['fk_a004_num_dependencia'];
        $validacion['centroCosto'] = $requerimiento['fk_a023_num_centro_costo'];
        $validacion['almacen'] = $requerimiento['fk_lgb014_num_almacen'];
        $validacion['prioridad'] = $requerimiento['fk_a006_num_miscelaneos_prioridad'];
        $comentarioR = "Requerimiento para reponer stock de almacén por los items asociados, referenciado en el requerimiento de código ".$requerimiento['cod_requerimiento'];
        $validacion['comentarioR'] = $comentarioR;
        $validacion['proveedor'] = $requerimiento['fk_lgb022_num_proveedor_sugerido'];

        $validacion['detalles'] = array();

        foreach ($validacion['aprobar'] as $key=>$value) {
            $detalle = $this->atDespacho->metBuscarRequerimientoDetalle($idReq,$validacion['numItem'][$key]);
            $validacion['detalles'][$key]['cantidad'] = $validacion['cantiPedida'][$key]/$detalle['num_cantidad'];
            $validacion['detalles'][$key]['comentarioI'] = $validacion['descripcion'][$key];
            $validacion['detalles'][$key]['numItem'] = $validacion['numItem'][$key];
            $validacion['detalles'][$key]['numCC'] = $validacion['cCosto'];
            $validacion['detalles'][$key]['idUnidad'] = $detalle['fk_lgb004_num_unidad_compra'];
        }


        $id = $this->atDespacho->metNuevoRequerimiento($validacion);

        $validacion['id'] = $id;

        if (is_array($id)) {
            $validacion['status'] = 'error';
        }

        echo json_encode($validacion);
        exit;
    }

    //Método para calcular los precios y montos
    public function metCalcular()
    {
        $cantidad = $this->metObtenerInt('cantidad');
        $precio = $this->metObtenerTexto('precio');
        $valor['contador'] = $this->metObtenerInt('contador');
        $valor['total']=$cantidad*$precio;
        echo json_encode($valor);
        exit;
    }

    //Método para paginar los requerimientos a despachar
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  req.*,
                  req.ind_estado AS estatus,
                  c.*,
                  c.num_flag_caja_chica AS flagCaja,
                  c.num_flag_revision AS flagRevi,
                  costo.ind_descripcion_centro_costo,
                  almacen.ind_descripcion AS ind_almacen,
                  almacen.num_flag_commodity,
                  (SELECT COUNT(*) FROM lg_c001_requerimiento_detalle AS detalle 
                  WHERE detalle.fk_lgb001_num_requerimiento = req.pk_num_requerimiento) AS cantReq,
                  (SELECT COUNT(*) FROM lg_c001_requerimiento_detalle AS detalle2 
                  WHERE detalle2.fk_lgb001_num_requerimiento = req.pk_num_requerimiento AND (detalle2.ind_estado = 'PE' OR detalle2.ind_estado = 'CO')) AS cantReqAp
                FROM lg_b001_requerimiento AS req
                INNER JOIN lg_b017_clasificacion AS c ON c.pk_num_clasificacion = req.fk_lgb017_num_clasificacion
                INNER JOIN a023_centro_costo AS costo ON costo.pk_num_centro_costo = req.fk_a023_num_centro_costo
                INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = req.fk_lgb014_num_almacen
                WHERE 1 AND c.ind_descripcion='STOCK DE ALMACEN' AND req.ind_estado = 'AP' AND req.ind_verificacion_caja_chica!=1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (
                        req.cod_requerimiento LIKE '%$busqueda[value]%' OR
                        costo.ind_descripcion_centro_costo LIKE '%$busqueda[value]%' OR
                        req.fec_requerida LIKE '%$busqueda[value]%' OR
                        req.ind_comentarios LIKE '%$busqueda[value]%' OR
                        almacen.ind_descripcion LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_requerimiento','ind_descripcion_centro_costo','fec_requerida','ind_comentarios','ind_almacen');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_requerimiento';
        #construyo el listado de botones
        $camposExtra = array();
        if (in_array('LG-01-02-04-01-A',$rol)) {
            $campos['boton']['Aprobar'] = array("
                <button accion='aprobar' title='Aprobar'
                        class='accion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Aprobado el Despachado del Requerimiento Nro. '
                        titulo='Aprobar el Despachado'
                        idReq='$clavePrimaria'
                        estado='aprobar'
                        data-toggle='modal' data-target='#formModal' id='aprobar$clavePrimaria'>
                    <i class='icm icm-rating3'></i>
                </button>
                ",
                'if( $i["cantReq"] > $i["cantReqAp"]) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('LG-01-02-04-02-D',$rol)) {
            $campos['boton']['Despachar'] = array("
                <button accion='despachar' title='Despachar'
                        class='accion logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Despachado del Almacen el Requerimiento Nro. '
                        titulo='Despachar del Almacen'
                        idReq='$clavePrimaria'
                        estado='despachar'
                        data-toggle='modal' data-target='#formModal' id='despachar$clavePrimaria'>
                    <i class='fa fa-sign-out'></i>
                </button>
                ",
                'if( $i["cantReqAp"] > 0) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Despachar'] = false;
        }
        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);
    }

}
