<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once ROOT. 'publico/procesoCompra/docxpresso/CreateDocument.php';

class adjudicacionControlador extends Controlador
{
    private $atAdjudicacion;
    private $atActaInicioModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atAdjudicacion = $this->metCargarModelo('adjudicacion', 'compras');
    }

    //Método para listar las adjudicaciones
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar las adjudicaciones
    public function metCrearModificarAdjudicacion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min'
        );
        $complementoCss = array(
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idInfor = $this->metObtenerInt('idInfor');
        $idProveedor = $this->metObtenerInt('idProveedor');
        $idAdj = $this->metObtenerInt('idAdj');
        $tipoAdj = $this->metObtenerTexto('tipoAdj');
        $ver = $this->metObtenerInt('ver');
        $secuencia = $this->metObtenerInt('secuencia');
        if($valido==1){
            $this->metValidarToken();

            $ExcepcionInt= array('adj');
            $int = $this->metValidarFormArrayDatos('form','int',$ExcepcionInt);
            $validacion = $int;

            $contador=0;
            while($contador<$validacion['sec']){
                if(!isset($validacion['adj'][$contador])) {
                    $validacion['adj'][$contador] = "0";
                }
                $contador=$contador+1;
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if($idAdj==0){
               $id=$this->atAdjudicacion->metCrearAdjudicacion(
                   $tipoAdj,$idInfor,$validacion['reqDet'],$validacion['coti'],
                   $validacion['adj'],$validacion['proveedor'],$validacion['sec'],
                   $secuencia
               );
                $validacion['status']='nuevo';
            }else{
                $id=$this->atAdjudicacion->metModificarAdjudicacion(
                    $validacion['reqDet'],$validacion['coti'],$validacion['adj'],
                    $validacion['proveedor'],$validacion['sec'],$idAdj
                );
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['id2']='error';
                $validacion['id']=$id;
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['id']=$idInfor.$secuencia;


            echo json_encode($validacion);
            exit;
        }
        if($idAdj!=0){
            $this->atVista->assign('idAdj',$idAdj);
            $this->atVista->assign('adjudicacion',$this->atAdjudicacion->metMostrarAdjudicacion($idAdj));
        }
        if($idInfor!=0){
            $this->atVista->assign('proveedor',$this->atAdjudicacion->metMostrarProveedor($idProveedor));
            $this->atVista->assign('informe',$this->atAdjudicacion->atActaInicioModelo->metMostrarInformeCompleto($idInfor));
            $this->atVista->assign('idInfor',$idInfor);
            $this->atVista->assign('tipoAdj',$tipoAdj);
            $this->atVista->assign('idProveedor',$idProveedor);
            $this->atVista->assign('ver',$ver);
            $this->atVista->assign('secuencia',$secuencia);
        }
        $this->atVista->assign('inviCoti',$this->atAdjudicacion->metListarInvitacionCotizacion());

        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para buscar el subfijo de los numeros
    public function metSubfijo($xx)
    { // esta función regresa un metSubfijo para la cifra
        $xx = trim($xx);
        $xstrlen = strlen($xx);
        if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
            $xsub = "";
        //
        if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
            $xsub = "MIL";
        //
        return $xsub;
    } // END FUNCTION

    //Método para modificar un monto a letras
    public function metNumtoletras($xcifra)
    {
        $xarray = array(0 => "Cero",
            1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
            "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
            "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
            100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
        );
//
        $xcifra = trim($xcifra);
        $xlength = strlen($xcifra);
        $xpos_punto = strpos($xcifra, ".");
        $xaux_int = $xcifra;
        $xdecimales = "00";
        if (!($xpos_punto === false)) {
            if ($xpos_punto == 0) {
                $xcifra = "0" . $xcifra;
                $xpos_punto = strpos($xcifra, ".");
            }
            $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
            $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
        }

        $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
        $xcadena = "";
        for ($xz = 0; $xz < 3; $xz++) {
            $xaux = substr($XAUX, $xz * 6, 6);
            $xi = 0;
            $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
            $xexit = true; // bandera para controlar el ciclo del While
            while ($xexit) {
                if ($xi == $xlimite) // si ya llegó al límite m&aacute;ximo de enteros
                {
                    break; // termina el ciclo
                }

                $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
                $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
                for ($xy = 1; $xy < 4; $xy++) // ciclo para revisar centenas, decenas y unidades, en ese orden
                {
                    switch ($xy) {
                        case 1: // checa las centenas
                            if (substr($xaux, 0, 3) < 100) // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                            {
                            } else {
                                if(substr($xaux, 1, 2)=='00'){
                                    $xseek = $xarray[substr($xaux, 0, 3)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                }else{
                                    $xseek=false;
                                }

                                if ($xseek) {
                                    $xsub = $this->metSubfijo($xaux); // devuelve el $this->metSubfijo correspondiente (Millón, Millones, Mil o nada)
                                    if (substr($xaux, 0, 3) == 100)
                                        $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                                } else // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                {
                                    $xseek = $xarray[substr($xaux, 0, 1) * 100]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 0, 3) < 100)
                            break;
                        case 2: // checa las decenas (con la misma lógica que las centenas)
                            if (substr($xaux, 1, 2) < 10) {
                            } else {
                                if(substr($xaux, 2, 1)=='0'){
                                    $xseek = $xarray[substr($xaux, 1, 2)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                }else{
                                    $xseek=false;
                                }
                                if ($xseek) {
                                    $xsub = $this->metSubfijo($xaux);
                                    if (substr($xaux, 1, 2) == 20)
                                        $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3;
                                } else {
                                    $xseek = $xarray[substr($xaux, 1, 1) * 10];
                                    if (substr($xaux, 1, 1) * 10 == 20)
                                        $xcadena = " " . $xcadena . " " . $xseek;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 1, 2) < 10)
                            break;
                        case 3: // checa las unidades
                            if (substr($xaux, 2, 1) < 1) // si la unidad es cero, ya no hace nada
                            {
                            } else {
                                $xseek = $xarray[substr($xaux, 2, 1)]; // obtengo directamente el valor de la unidad (del uno al nueve)
                                $xsub = $this->metSubfijo($xaux);
                                $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                            } // ENDIF (substr($xaux, 2, 1) < 1)
                            break;
                    } // END SWITCH
                } // END FOR
                $xi = $xi + 3;
            } // ENDDO

            if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
                $xcadena .= " DE";

            if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
                $xcadena .= " DE";

            // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
            if (trim($xaux) != "") {
                switch ($xz) {
                    case 0:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena .= "UN BILLON ";
                        else
                            $xcadena .= " BILLONES ";
                        break;
                    case 1:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena .= "UN MILLON ";
                        else
                            $xcadena .= " MILLONES ";
                        break;
                    case 2:
                        if ($xcifra < 1) {
                            $xcadena = "CERO BOLIVARES CON $xdecimales/100";
                        }
                        if ($xcifra >= 1 && $xcifra < 2) {
                            $xcadena = "UN BOLIVAR CON $xdecimales/100";
                        }
                        if ($xcifra >= 2) {
                            $xcadena .= " BOLIVARES CON $xdecimales/100"; //
                        }
                        break;
                } // endswitch ($xz)
            } // ENDIF (trim($xaux) != "")
            // ------------------      en este caso, para México se usa esta leyenda     ----------------
            $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
        } // ENDFOR	($xz)
        return trim($xcadena);
    } // END FUNCTION

    //Método para modificar un mes a letras
    public function metMesLetras($mes){
        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'Enero',
            '02'=>'Febrero',
            '03'=>'Marzo',
            '04'=>'Abril',
            '05'=>'Mayo',
            '06'=>'Junio',
            '07'=>'Julio',
            '08'=>'Agosto',
            '09'=>'Septiembre',
            '10'=>'Octubre',
            '11'=>'Noviembre',
            '12'=>'Diciembre'
        );

        return $metMesLetras[$mes];
    } // END FUNCTION

    //Método para modificar un dia a letras
    public function metDiasLetras($dia){

        if(strlen($dia)==1){
            $dia="0".$dia;
        }
        $diaLetras=array(
            '01'=>'uno',
            '02'=>'dos',
            '03'=>'tres',
            '04'=>'cuatro',
            '05'=>'cinco',
            '06'=>'seis',
            '07'=>'siete',
            '08'=>'ocho',
            '09'=>'nueve',
            '10'=>'diez',
            '11'=>'once',
            '12'=>'doce',
            '13'=>'trece',
            '14'=>'catorce',
            '15'=>'quince',
            '16'=>'dieciseis',
            '17'=>'diecisiete',
            '18'=>'dieciocho',
            '19'=>'diecinueve',
            '20'=>'veinte',
            '21'=>'veintiuno',
            '22'=>'veintidos',
            '23'=>'veintitres',
            '24'=>'veinticuatro',
            '25'=>'veinticinco',
            '26'=>'veintiseis',
            '27'=>'veintisiete',
            '28'=>'veintiocho',
            '29'=>'veintinueve',
            '30'=>'treinta',
            '31'=>'treinta y uno'
        );

        return $diaLetras[$dia];
    } // END FUNCTION

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para generar el documento de las adjudicaciones
    public function metGenerarInformeAdjudicacion(){
        $idInfor = $this->metObtenerInt('idInfor');

        #busco en la ruta el odt que me sirve de formato
        $this->atDocx = new Docxpresso\CreateDocument(array('template' => ROOT . 'publico' . DS . 'procesoCompra' . DS . 'informe-adjudicacion-plantilla2.odt'));

        $adjudicacion=$this->atAdjudicacion->metMostrarAdjudicacionReporte($idInfor);
        $contralorProvicional=$this->atAdjudicacion->metBuscarCargo('CONTRALOR DEL ESTADO MONAGAS (P)');

        $nroVisual = $this->metRellenarCeros($adjudicacion[0]['num_adjudicacion'],3)."-".$adjudicacion[0]['fec_anio'];
        $diaRecomen = substr($adjudicacion[0]['fec_creacion'],8,2);
        $mesRecomen = $this->metMesLetras(substr($adjudicacion[0]['fec_creacion'],5,2));
        $anioRecomen = substr($adjudicacion[0]['fec_creacion'],0,4);
        $objetoConsulta = $adjudicacion[0]['ind_objeto_consulta'];

        $cont=0;
        $cantidadProv=0;
        $cadenaProveedor="";
        while($cont<count($adjudicacion)){
            $cantidadProv=$cantidadProv+1;
            $cadenaProveedor .= "- ".$adjudicacion[$cont]['nombre'].", RIF: ".$adjudicacion[$cont]['ind_documento_fiscal'].", por un monto de Bs. ".number_format($adjudicacion[$cont]['total'],2,',','.').", ubicada en ".$adjudicacion[$cont]['ind_direccion']."<br/>";
            $cont=$cont+1;
        }
        if ($cantidadProv==1) {
            $tipoAdjudicacionSecuencia = "DECIDO OTORGAR LA ADJUDICACIÓN por medio del presente Informe a la empresa:";
        } else {
            $tipoAdjudicacionSecuencia = "DECIDO OTORGAR LA ADJUDICACIÓN PARCIAL por medio del presente Informe a las empresas:";
        }
        $diaNro = substr($adjudicacion[0]['fec_adjudicacion'],8,2);
        $dia = $this->metDiasLetras($diaNro);
        $mes = $this->metMesLetras(substr($adjudicacion[0]['fec_adjudicacion'],5,2));
        $anio = substr($adjudicacion[0]['fec_adjudicacion'],0,4);

//Colocar logo en los odt
        $headerStyle = array('style' => 'min-height: 3cm');
        $urlImg = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'CES.jpg';
        $urlImg2 = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'LOGOSNCF.png';
        $rowStyle = array('style' => 'min-height: 25px');
        $cellStyle = array('style' => 'margin-left: 0px; vertical-align: middle;');
        $this->atDocx->header($headerStyle)
            ->table(array('grid' => array('295px','290px')))
            ->row($rowStyle)
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg, 'style' => 'width:100px; height:80px;'))
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg2, 'style' => 'margin-left: 190px; width:320px; height:320px;'))
        ;
//Colocar logo en los odt


        if($contralorProvicional['ind_cedula_documento']){
            $contralor = ucwords($contralorProvicional['ind_nombre1']." ".$contralorProvicional['ind_apellido1']);
            $cedula = $contralorProvicional['ind_cedula_documento'];
        } else {
            $contralor = '';
            $cedula = '';
        }
        $this->metReemplazar('nroVisual',$nroVisual);
        $this->metReemplazar('contralor',$contralor);
        $this->metReemplazar('cedula',$cedula);
        $this->metReemplazar('diaRecomen',$diaRecomen);
        $this->metReemplazar('mesRecomen',$mesRecomen);
        $this->metReemplazar('anioRecomen',$anioRecomen);
        $this->metReemplazar('objetoConsulta',$objetoConsulta);
        $this->metReemplazar('tipoAdjudicacionSecuencia',$tipoAdjudicacionSecuencia);
        $this->metReemplazar('proveedores',$cadenaProveedor,1);
        $this->metReemplazar('dia',$dia);
        $this->metReemplazar('diaNmero',$diaNro);
        $this->metReemplazar('mes',$mes);
        $this->metReemplazar('anio',$anio);

        $this->atDocx->render('.'.DS.'publico' .DS.'procesoCompra' .DS.'declararDesierto'.$idInfor.'.odt');
//        $this->atDocx->render('informeAdjudicacion'.$idInfor.'.odt');
        $validacion['idInfor']=$idInfor;
        $validacion['ruta']='.'.DS.'publico' .DS.'procesoCompra' .DS.'declararDesierto'.$idInfor.'.odt';
        echo json_encode($validacion);
        exit;
    }

    //Método para reemplazar las variables en el documento a generar ODT
    public function metReemplazar($nombre,$valor,$extra=false){
        if($extra==1) {
            $this->atDocx->replace(array($nombre => array('value' => $valor)), array('block-type' => true));
        } else {
            $this->atDocx->replace(array($nombre => array('value' => $valor)));
        }
    }

    //Método para paginar las adjudicaciones
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  informe.*,
                  CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
                  proveedor.pk_num_proveedor,
                  recomendado.num_secuencia,
                  evaluacion.ind_cod_evaluacion,
                  adjudicacion.pk_num_adjudicacion,
                  detalle.cod_detalle 
                FROM lg_c006_proveedor_recomendado AS recomendado
                INNER JOIN lg_b012_informe_recomendacion AS informe ON recomendado.fk_lgb012_num_informe_recomendacion = informe.pk_num_informe_recomendacion
                INNER JOIN a006_miscelaneo_detalle AS detalle ON informe.fk_a006_num_miscelaneo_detalle_tipo_adjudicacion = detalle.pk_num_miscelaneo_detalle
                LEFT JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = recomendado.fk_lgb022_num_proveedor
                LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = informe.fk_lgb011_num_evaluacion
                LEFT JOIN lg_b013_adjudicacion AS adjudicacion ON informe.pk_num_informe_recomendacion = adjudicacion.fk_lgb012_num_informe_recomendacion AND
                adjudicacion.num_adjudicacion = recomendado.num_secuencia 
                INNER JOIN lg_b009_acta_inicio AS acta ON evaluacion.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
                INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON acta.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
                INNER JOIN lg_b001_requerimiento AS requerimiento ON acta_requerimiento.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
                INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = requerimiento.fk_a004_num_dependencia
                AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3' 
                WHERE
                  detalle.cod_detalle != 'DS' ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (evaluacion.ind_cod_evaluacion LIKE '%$busqueda[value]%' OR
                        informe.ind_asunto LIKE '%$busqueda[value]%' OR
                        informe.ind_objeto_consulta LIKE '%$busqueda[value]%' OR
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR
                        persona.ind_nombre2 LIKE '%$busqueda[value]%' OR
                        persona.ind_apellido1 LIKE '%$busqueda[value]%' OR
                        persona.ind_apellido2 LIKE '%$busqueda[value]%')  
                        ";
        }
        $sql .= " GROUP BY recomendado.fk_lgb022_num_proveedor,recomendado.fk_lgb012_num_informe_recomendacion ";
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_cod_evaluacion','ind_asunto','ind_objeto_consulta','nombre');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_informe_recomendacion';
        #construyo el listado de botones

        $camposExtra = array(
            'pk_num_proveedor',
            'num_secuencia',
            'pk_num_adjudicacion',
            'fk_a006_num_miscelaneo_detalle_tipo_adjudicacion'
        );
        if (in_array('LG-01-01-03-06-01-N',$rol)) {
            $campos['boton']['Crear'] = array("
                <button accion='crear' title='Crear informe de Adjudicacion'
                        class='accion logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        idInfor='$clavePrimaria' titulo='Crear informe de Adjudicacion'
                        tipoAdj='fk_a006_num_miscelaneo_detalle_tipo_adjudicacion'
                        secuencia='num_secuencia'
                        idProveedor='pk_num_proveedor'
                        data-toggle='modal' data-target='#formModal' id='crear".$clavePrimaria."num_secuencia'>
                    <i class='md md-create'></i>
                </button>
                ",
                'if( !$i["pk_num_adjudicacion"]) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Crear'] = false;
        }

        if (in_array('LG-01-01-03-06-02-M',$rol)) {
            $campos['boton']['Editar'] = array("
                <button accion='modificar' title='Editar informe de Adjudicacion' titulo='Editar informe de Adjudicacion' 
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idInfor='$clavePrimaria' descipcion='El Usuario ha Modificado la Adjudicacion Nro. $clavePrimaria'
                        tipoAdj='fk_a006_num_miscelaneo_detalle_tipo_adjudicacion'
                        secuencia='num_secuencia'
                        idAdj='pk_num_adjudicacion'
                        idProveedor='pk_num_proveedor'
                        data-toggle='modal' data-target='#formModal' id='crear".$clavePrimaria."num_secuencia'>
                    <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["pk_num_adjudicacion"]) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-01-03-06-03-G',$rol)) {
            $campos['boton']['Geenrar'] = array("
                <button title='Imprimir Informe de Adjudicacion'
                        class='imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        idInfor='$clavePrimaria'>
                    <i class='fa fa-print'></i>
                </button>
            ",
                'if( $i["pk_num_adjudicacion"]) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Geenrar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }

    //Método para rechazar las adjudicaciones
    public function metRechazar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idInfor = $this->metObtenerInt('idInfor');
        $motivo = $this->metObtenerAlphaNumerico('motivo');

        if($valido==1){

            $validacion['idInfor'] = $idInfor;
            $validacion['motivo'] = $motivo;

            if($motivo==null){
                $validacion['motivo'] = 'error';
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            $detalles=$this->atAdjudicacion->metMostrarAdjudicacionReporte($idInfor);
            $validacion['detalles']=$detalles;


            $id=$this->atAdjudicacion->metAnularRecomendacion($validacion);
            $validacion['status']='modificar';

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['id2']='error';
                $validacion['id']=$id;
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            echo json_encode($validacion);
            exit;

        }

        $this->atVista->assign('idInfor',$idInfor);
        $this->atVista->metRenderizar('rechazar','modales');
    }
}
