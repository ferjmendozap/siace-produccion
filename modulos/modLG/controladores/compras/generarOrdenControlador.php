<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class generarOrdenControlador extends Controlador
{
    private $atOrden;
    private $atMiscelaneoModelo;
    private $atRequerimientoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atOrden = $this->metCargarModelo('generarOrden', 'compras');
    }

    //Método para listar las ordenes a generar
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
//        $this->atVista->assign('cotizacion', $this->atOrden->metSumarMontosOrdenesPendientes());
//        $this->atVista->assign('listado', $this->atOrden->metListarOrdenesPendientes());
        $this->atVista->metRenderizar('listado');
    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para generar las ordenes
    public function metGenerarOrden()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idAdj = $this->metObtenerInt('idAdj');
        $idOrden = $this->metObtenerInt('idOrden');
        $estado = $this->metObtenerAlphaNumerico('estado');
        $clas = $this->metObtenerAlphaNumerico('clasificacion');
        $ver = $this->metObtenerInt('ver');
        $int = $this->metObtenerInt('form','int');
        if ($valido == 1) {
            $this->metValidarToken();
            $validacion = "";

            $ExcceccionAlphaNum = array('comentario','rechazo');
            if ($int['mAfecto']!=0){
                $ExcceccionInt = array('mNoAfecto','impuestos','oCargos','cCosto');
            } else {
                $ExcceccionInt = array('mAfecto','impuestos','oCargos','cCosto');
            }

            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',$ExcceccionAlphaNum);

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $txt);
            }

            $orden=$this->atOrden->atRequerimientoModelo->metContar('lg_b019_orden',date('y'),'fec_anio','AND ind_tipo_orden="'.$validacion['tipoOrden'].'"');
            $dependencia = $this->atOrden->atRequerimientoModelo->metBuscarDependencia($validacion['dependencia']);
            $orden['cantidad'] = $orden['cantidad'] + 1;
            $orden=$this->metRellenarCeros($orden['cantidad'],10);
            $validacion['orden']=$orden;

            $codigoOrden=$dependencia['ind_codinterno']."-".$orden."-".date('Y');

            $contador=1;
            while ($contador<=$validacion['sec']) {
                if(!isset($validacion['exonerado'][$contador]) OR $validacion['exonerado'][$contador]=='error'){
                    $validacion['exonerado'][$contador]=0;
                }

                if($validacion['descPorc'][$contador]=="error"){
                    $validacion['descPorc'][$contador]=0;
                }

                if($validacion['descFijo'][$contador]=="error"){
                    $validacion['descFijo'][$contador]=0;
                }

                if($validacion['tipoNum'][$contador]=="I"){
                    $item[$contador]=$validacion['num'][$contador];
                    $commodity[$contador]=null;
                } elseif($validacion['tipoNum'][$contador]=="C"){
                    $item[$contador]=null;
                    $commodity[$contador]=$validacion['num'][$contador];
                }

                $idPresupuesto = $this->atOrden->metBuscarPresupuestoDetalle($validacion['partidaDetalle'][$contador]);

                if(!$idPresupuesto){
                    $validacion['idPartida'.$validacion['idPartidaDetalle'][$contador]] = 'error';
                    $validacion['status'] = 'errorPresupuesto';
                    $error=1;
                }else{
                    $validacion['partidaDetalle'][$contador] = $idPresupuesto['pk_num_partida_presupuestaria'];
                }

                if($idPresupuesto['num_monto_ajustado']-$idPresupuesto['num_monto_compromiso']<$validacion['total'][$contador]){
                    $validacion['cod_partida'] = $idPresupuesto['cod_partida'];
                    $validacion['status'] = 'errorPresupuesto2';
                    $validacion['idPartida'.$validacion['partidaDetalle'][$contador]] = 'error';
                    $error=1;
                }
                if($validacion['unidad'][$contador]=='error'){
                    $validacion['unidad'][$contador] = NULL;
                }

                $contador=$contador+1;
            }
            if (isset($error)){
                echo json_encode($validacion);
                exit;

            }

            if(!isset($validacion['comentario'])){
                $validacion['comentario']=0;
            }
            if(!isset($validacion['rechazo'])){
                $validacion['rechazo']='';
            }


            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if ($idOrden == 0) {
                $validacion['status']='nuevo';
                $id=$this->atOrden->metNuevaOrden(
                    $validacion['dependencia'],$validacion['centro_costo'],$validacion['clasificacion'],$validacion['almacen'],
                    $validacion['proveedor'],$orden,$validacion['tipoOrden'],
                    $validacion['tipo_servicio'],$validacion['mBruto'],$validacion['impuestos'],
                    $validacion['mTotal'],$validacion['mPendiente'],$validacion['mAfecto'],
                    $validacion['mNoAfecto'],$validacion['formaPago'],$validacion['plazo'],
                    $validacion['entregar'],$validacion['direccion'],$validacion['comentario'],
                    $validacion['descripcion'],$validacion['cuenta'],
                    $validacion['partida'],$idAdj,
                    $validacion['sec'],$validacion['cant'],$validacion['precio'],
                    $validacion['montoPU'],$validacion['descPorc'],$validacion['descFijo'],
                    $validacion['exonerado'],$validacion['total'],$validacion['cuentaDetalle'],
                    $validacion['partidaDetalle'],$validacion['unidad'],$validacion['cCosto'],
                    $item,$commodity,$validacion['actaDet']
                );
                $datos = $this->atOrden->metBuscarActualizar($idAdj);
                $id=$this->atOrden->metActualizarEstados($datos);
            } else {
                $validacion['status']='modificar';
//                $id=$this->atRequerimientoModelo->metModificarRequerimiento();
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }
            $validacion['idAdj']=$idAdj;
            echo json_encode($validacion);
            exit;
        }

        if($idAdj!=0){
            $this->atVista->assign('formBD',$this->atOrden->metMosrarAdjudicacion($idAdj));
            $this->atVista->assign('formBDD',$this->atOrden->metMosrarAdjudicacionDetalle($idAdj));
            $this->atVista->assign('partidaBD',$this->atOrden->metBuscarPartida($idAdj));
            $this->atVista->assign('idAdj',$idAdj);
            $this->atVista->assign('estado',$estado);
            $this->atVista->assign('clasif',$clas);
            $this->atVista->assign('ver',$ver);
            $this->atVista->assign('cotizaciones',$this->atOrden->metBuscarCotizaciones($idAdj));
            $this->atVista->assign('requerimientos',$this->atOrden->metBuscarRequerimientos($idAdj));
        }
        $this->atVista->assign('predeterminado',$this->atOrden->atRequerimientoModelo->metBuscarPredeterminado());
        $this->atVista->assign('partidaCuenta',$this->atOrden->metBuscarPartidaCuenta('403.18.01.00'));
        $this->atVista->assign('almacen',$this->atOrden->atRequerimientoModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('clas',$this->atOrden->atRequerimientoModelo->atClasificacionModelo->metListarClasificacion());
        $this->atVista->assign('formaPago',$this->atOrden->atMiscelaneoModelo->metMostrarSelect('FDPLG'));
        $this->atVista->assign('tipoServicio',$this->atOrden->metListarTipoServicio());
        $this->atVista->assign('dependencia',$this->atOrden->metListarDependencias());
        $this->atVista->assign('estado',$estado);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    //Método para asignar las ordenes al procedimiento
    public function metAsignarOrden()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idAdj = $this->metObtenerInt('idAdj');

        if ($valido == 1) {
            $this->metValidarToken();

            $int = $this->metValidarFormArrayDatos('form','int');
            $validacion = $int;

            $validacion['idAdj']=$idAdj;

            $id = $this->atOrden->metActualizarOrdenes($validacion);
            $datos = $this->atOrden->metBuscarActualizar($idAdj);
            $id=$this->atOrden->metActualizarEstados($datos);
            $validacion['status'] = 'modificar';

            $validacion['id'] = $id;

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            echo json_encode($validacion);
            exit;
        }

        $this->atVista->assign('idAdj',$idAdj);
        $this->atVista->metRenderizar('Asignar','modales');
    }

    //Método para desplegar listado de ordenes
    public function metOrdenes(){
        $this->atVista->metRenderizar('Ordenes','modales');
    }

    //Método para buscar las ordenes seleccionadas
    public function metBuscarOrden(){
        $idOrden = $this->metObtenerInt('idOrden');
        $datos = $this->atOrden->metBuscarOrden($idOrden);
        echo json_encode($datos);
        exit;
    }

    //Método para calcular los montos de los detalles
    public function metCalcularMontos(){
        $iva = $this->metObtenerTexto('iva');
        $idDetalle = $this->metObtenerInt('sec');
        $cantDetalle = $this->metObtenerTexto('cant');
        $precioDetalle = $this->metObtenerTexto('precio');
        $descPorcDetalle = $this->metObtenerTexto('descPorc');
        $descFijoDetalle = $this->metObtenerTexto('descFijo');
        $exoneradoDetalle = $this->metObtenerTexto('exonerado');

        $resultado['exoneradoDetalle']=$exoneradoDetalle;
        $resultado['precioDetalle']=$precioDetalle;
        $resultado['id']=$idDetalle;
        $resultado['cant']=$cantDetalle;
        $resultado['descPorc']=$descPorcDetalle;
        $resultado['descFijo']=$descFijoDetalle;

        $decuento=0;
        if($descFijoDetalle!=0) {
            $decuento=$descFijoDetalle;
        } elseif ($descPorcDetalle!=0) {
            $decuento=$descPorcDetalle;
        }
        $resultado['iva']=$iva;
        $resultado['precioUnitario']=($precioDetalle+$iva)-$decuento;

        $resultado['total']=($resultado['precioUnitario']*$cantDetalle);
        $resultado['mBruto']=$resultado['mAfecto']+$resultado['mNoAfecto'];
        $resultado['mTotal']=$resultado['mBruto']+$resultado['precioUnitario'];
        $resultado['mPendiente']=$resultado['mTotal'];

        echo json_encode($resultado);
        exit;
    }

    //Método para buscar el iva y la retención de los tipos de servicios del proveedor
    public function metBuscarIvaRetencion(){
        $cod = $this->metObtenerTexto('cod');
        $resultado = $this->atOrden->metBuscarImpuesto($cod);
        echo json_encode($resultado);
        exit;
    }

    //Método para buscar los centros de costo de la dependencia
    public function metBuscarCentroCosto(){
        $id = $this->metObtenerInt('id');
        $resultado = $this->atOrden->metBuscarCentroCosto($id);
        echo json_encode($resultado);
        exit;
    }

    //Método para paginar las ordenes a generar
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  adjudicacion.*,
                  acta.ind_codigo_acta,
                  acta.pk_num_acta_inicio,
                  acta.num_flag_contrato,
                  proveedor.pk_num_proveedor,
                  concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
                  detalle.num_secuencia,
                  detalle3.ind_nombre_detalle AS adjudicacion,
                  (SELECT SUM(cotizacion.num_total) FROM 
                   lg_c003_cotizacion AS cotizacion,
                   lg_c005_adjudicacion_detalle AS detalle4 
                   WHERE cotizacion.pk_num_cotizacion = detalle4.fk_lgc003_num_cotizacion
                   AND detalle4.fk_lgb013_num_adjudicacion = adjudicacion.pk_num_adjudicacion
                  )
                   AS montoTotal
                FROM
                  lg_b013_adjudicacion AS adjudicacion
                  INNER JOIN a006_miscelaneo_detalle AS detalle3 ON detalle3.pk_num_miscelaneo_detalle = adjudicacion.fk_a006_num_miscelaneo_detalle_tipo_adjudicacion
                INNER JOIN lg_c005_adjudicacion_detalle AS detalle2 ON adjudicacion.pk_num_adjudicacion = detalle2.fk_lgb013_num_adjudicacion
                INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON recomendacion.pk_num_informe_recomendacion = adjudicacion.fk_lgb012_num_informe_recomendacion
                INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
                INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
                INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
                INNER JOIN lg_c006_proveedor_recomendado AS recomendado ON recomendacion.pk_num_informe_recomendacion = recomendado.fk_lgb012_num_informe_recomendacion
                INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = detalle2.fk_lgb022_num_proveedor_adjudicado
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                LEFT JOIN lg_b019_orden AS orden ON orden.fk_lgb013_num_adjudicacion = adjudicacion.pk_num_adjudicacion
                INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON acta.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
                INNER JOIN lg_b001_requerimiento AS requerimiento ON acta_requerimiento.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
                INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = requerimiento.fk_a004_num_dependencia
                AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                WHERE acta.ind_estado = 'PR' 
                AND recomendacion.ind_estado = 'AP' 
                AND isnull(orden.pk_num_orden)
                ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (evaluacion.ind_cod_evaluacion LIKE '%$busqueda[value]%' OR
                        informe.ind_asunto LIKE '%$busqueda[value]%' OR
                        informe.ind_objeto_consulta LIKE '%$busqueda[value]%' OR
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR
                        persona.ind_nombre2 LIKE '%$busqueda[value]%' OR
                        persona.ind_apellido1 LIKE '%$busqueda[value]%' OR
                        persona.ind_apellido2 LIKE '%$busqueda[value]%')  
                        ";
        }
        $sql .= " GROUP BY adjudicacion.pk_num_adjudicacion ";
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_codigo_acta','pk_num_proveedor','nombre','montoTotal','adjudicacion');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_adjudicacion';
        #construyo el listado de botones

        $camposExtra = array(
            'pk_num_adjudicacion');

        if (in_array('LG-01-01-03-08-L',$rol)) {
            $campos['boton']['Crear'] = array("
                <button accion='generar' title='Generar Orden de Compra/Servicio'
                        class='generar logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        idAdj='$clavePrimaria'
                        titulo='Generar Orden de Compra/Servicio'
                        clasificacion='1'
                        data-toggle='modal' data-target='#formModal' id='generar'>
                    <i class='md md-create'></i>
                </button>
                ",
                'if( $i["num_flag_contrato"]==0 ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Crear'] = false;
        }

        if (in_array('LG-01-01-03-08-L',$rol)) {
            $campos['boton']['Asignar'] = array("
                <button title='Asignar Ordenes al Procedimiento'
                        class='asignar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idAdj='pk_num_adjudicacion'
                        titulo='Asignar Ordenes al Procedimiento'
                        data-toggle='modal' data-target='#formModal'>
                    <i class='fa fa-arrow-circle-down'></i>
                </button>
                ",
                'if( $i["num_flag_contrato"]==1 ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Asignar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);
    }

}
