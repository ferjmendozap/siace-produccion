<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class invitacionesControlador extends Controlador
{
    private $atInvitacion;
    private $atActaInicioModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atInvitacion = $this->metCargarModelo('invitaciones', 'compras');
        $this->metObtenerLibreria('header','modLG');
        $this->atFPDF = new pdf('P', 'mm', 'Letter');
    }

    //Método para listar las invitaciones de los proveedores
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
//        $this->atVista->assign('listado', $this->atInvitacion->metListarInvitaciones());
        $this->atVista->metRenderizar('listado');
    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para crear y/o modificar la cotizacion por la invitacion del proveedor
    public function metCotizar()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $numInvitacion = $this->metObtenerAlphaNumerico('numInvitacion');
        $anio = $this->metObtenerInt('anio');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();
            $exepcion=array('asignado','exo');

            $ind = $this->metValidarFormArrayDatos('form','int',$exepcion);
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $txt);
            }

            for ($i=1;$i<=$validacion['secuencia'];$i++){
                if(!isset($validacion['asignado'][$i])){
                    $validacion['asignado'][$i] = '0';
                }
                if(!isset($validacion['exo'][$i])){
                    $validacion['exo'][$i] = '0';
                }
                if($validacion['observacion'][$i]=="error"){
                    $validacion['observacion'][$i] = NULL;
                }
                if($validacion['dfijo'][$i]=="error"){
                    $validacion['dfijo'][$i] = '0';
                }
                if($validacion['dpor'][$i]=="error"){
                    $validacion['dpor'][$i] = '0';
                }
                if($validacion['idCotizacion'][$i]=="error"){
                    $validacion['idCotizacion'][$i] = '0';
                    $maximo = $this->atInvitacion->metObtenerMaximaCotizacion();
                    $validacion['ind_num_cotizacion'][$i] = $maximo['maximo'] +$i-1;
                    $validacion['ind_num_cotizacion'][$i] = $this->metRellenarCeros($validacion['ind_num_cotizacion'][$i],10);
                } else {
                    $validacion['ind_num_cotizacion'][$i] = 0;
                }
            }
            if($validacion['dFijo']=="error"){
                $validacion['dFijo'] = '0';
            }
            if($validacion['dPor']=="error"){
                $validacion['dPor'] = '0';
            }
            if($validacion['dias']=="error"){
                $validacion['dias'] = '0';
            }

            if($validacion['vofer']=="error"){
                $validacion['vofer'] = '0';
            }

            if(in_array("error",$validacion)){
                $validacion['status']="error";
                echo json_encode($validacion);
                exit;
            }

            $id=$this->atInvitacion->metCotizar($validacion);
            $validacion['status']='nuevo';

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = "error";
                        }
                    }
                }
                $validacion['id'] = $id;
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['numInivtacion']=$numInvitacion;
            $validacion['anio']=$anio;
            echo json_encode($validacion);
            exit;

        }
        $this->atVista->assign('numInivtacion',$numInvitacion);
        $this->atVista->assign('anio',$anio);
        $this->atVista->assign('cotizaciones',$this->atInvitacion->metBuscarCotizaciones($numInvitacion,$anio));
        $this->atVista->assign('formaPago',$this->atInvitacion->metListarFormaPago());
        $this->atVista->assign('unidades',$this->atInvitacion->metListarUnidades());
        $this->atVista->assign('ver',$ver);
        $this->atVista->metRenderizar('cotizarInvitacion','modales');

    }

    //Método para modificar el formato de las fechas
    public function metFormatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            return "la fecha es incorrecta";
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    }

    public function metFechaFinCotizacion($codigo,$fecha){
        if ($codigo==1){
            $sumaDias=4;
        }elseif ($codigo==2){
            $sumaDias=5;
        }
        $fechaFinPrevio=date("d-m-Y", strtotime($fecha . "+ ".$sumaDias." days"));
        $diasDiferencia = 0;
        for ($i = $fecha; $i <= $fechaFinPrevio; $i = date("d-m-Y", strtotime($i . "+ 1 days"))) {
            $diaFeriado = $this->atInvitacion->metFeriado($i);
            $feriado = $diaFeriado['feriado'];
            $fechaComprobar = date("w", strtotime($i));
            if (($fechaComprobar == 0) || ($fechaComprobar == 6) || ($feriado > 0)) {
                $diasDiferencia++;
            }
        }

        $Fin=date("d-m-Y", strtotime($fechaFinPrevio . "+ ".$diasDiferencia." days"));

        // var_dump($fecha);
        // var_dump($sumaDias);
        // var_dump($diasDiferencia);
        // var_dump($fechaFinPrevio);
        //var_dump($Fin);

        return $Fin;
    }

    //Método para generar el documento de las invitaciones PDF
    public function metGenerarInvitacion($idInvi)
    {

        $invitacion=$this->atInvitacion->metMostrarInvitacion($idInvi);
        $proveedor=$this->atInvitacion->atActaInicioModelo->atRequerimientoModelo->atProveedorModelo->metBuscarProveedor($invitacion['fk_lgb022_num_proveedor']);
        $telefonos=$this->atInvitacion->atActaInicioModelo->atRequerimientoModelo->atProveedorModelo->metListarTelefonos($proveedor['fk_a003_num_persona_proveedor']);
        $requerimientos=$this->atInvitacion->metListarActaDetalle($invitacion['pk_num_acta_inicio']);
//        $usuario=$this->atInvitacion->nombreUsuario;
        $usuario=$this->atInvitacion->metMostrarEmpleadoInvitacion($invitacion['fk_a018_num_seguridad_usuario']);

        $telefono = "";
        $fax = "";
        foreach ($telefonos as $t){
            if(isset($t['ind_telefono'])){
                #1 es telefono normal
                if($t['fk_a006_num_miscelaneo_detalle_tipo_telefono']==1){
                    $telefono = $t['ind_telefono'];
                }elseif($t['fk_a006_num_miscelaneo_detalle_tipo_telefono']==2){
                    $fax = $t['ind_telefono'];
                }
            }
        }
        $fecha_inv=$this->metFormatoFecha($invitacion['fechaInvi'],1,'-');

        define('NRO_INVITACION',$invitacion['ind_num_invitacion']);
        define('COD_ACTA',$invitacion['ind_codigo_acta']);
        define('FECHA_INV',$fecha_inv);
        define('PROVEEDOR', $proveedor['ind_nombre1']." ".$proveedor['ind_apellido1']);
        define('RIF', $proveedor['ind_documento_fiscal']);
        define('DIRECCION',$proveedor['ind_direccion']);
        define('TELEFONO',$telefono);
        define('FAX',$fax);

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 10);
        $this->atFPDF->AddPage();

        $this->atFPDF->primeraPagina();


        $fecFinCotizacion=$this->metFechaFinCotizacion($invitacion['cod_detalle'],$fecha_inv);


        /* $this->atFPDF->SetXY(170, 17); $this->atFPDF->Cell(15, 5, utf8_decode('Invitación N°: '), 0, 0, 'R');
        // $this->atFPDF->Cell(60, 5, $invitacion['ind_num_invitacion'], 0, 1, 'L');
         $this->atFPDF->SetXY(170, 22); $this->atFPDF->Cell(15, 5, utf8_decode('# Acta de Inicio: '), 0, 0, 'R');
        // $this->atFPDF->Cell(60, 5, $invitacion['ind_codigo_acta'], 0, 1, 'L');
         $this->atFPDF->SetXY(170, 26); $this->atFPDF->Cell(15, 5, 'Fecha: ', 0, 0, 'R');
        // $this->atFPDF->Cell(60, 5, $this->metFormatoFecha($invitacion['fechaInvi'],1,'-'), 0, 1, 'L');

         $this->atFPDF->Ln(10);
         $this->atFPDF->SetXY(5, 30);
         $this->atFPDF->SetFillColor(250, 250, 250);
         $this->atFPDF->SetFont('Arial', '', 8);
         $this->atFPDF->Cell(45, 5, 'Proveedor: ', 0, 0, 'L', 1);
         $this->atFPDF->SetFont('Arial', 'B', 8);
        // $this->atFPDF->Cell(150, 6, utf8_decode($proveedor['ind_nombre1']." ".$proveedor['ind_apellido1']), 0, 1, 'L');
         $this->atFPDF->SetFont('Arial', '', 8);
         $this->atFPDF->Cell(35, 5, 'R.I.F: ', 0, 0, 'L', 1);
        // $this->atFPDF->Cell(50, 6, $proveedor['ind_documento_fiscal'], 0, 1, 'L');
         $this->atFPDF->Cell(35, 5, 'Domicilio: ', 0, 0, 'L', 1);
        // $this->atFPDF->MultiCell(150, 6, utf8_decode(
       //      $proveedor['ind_direccion']
         //), 0, 'L');
         $this->atFPDF->Cell(35, 5, 'Telefono Contacto: ', 0, 0, 'L', 1);
      //   $this->atFPDF->Cell(35, 6, $telefono , 0, 0, 'L');
         $this->atFPDF->Cell(15, 5, 'Fax: ', 0, 0, 'L', 1);
        // $this->atFPDF->Cell(50, 6, $fax, 0, 1, 'L');
         $this->atFPDF->Ln(5);
 */
        $this->atFPDF->SetFont('Arial', 'BU', 9);
        $this->atFPDF->Cell(200, 6, utf8_decode('INVITACIÓN'), 0, 1, 'C');
        $this->atFPDF->Cell(200, 3, utf8_decode('PLIEGO DE CONDICIONES:'), 0, 1, 'C');
        $this->atFPDF->Ln(10);

        $proce=substr($invitacion['ind_nombre_procedimiento'],3);

        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(5, 75);

       // $this->atFPDF->MultiCell(200, 6, utf8_decode('      Me dirijo a usted (es) en la oportunidad de informarle (s) que han sido seleccionado (s), para participar en el procedimiento de contratación bajo la modalidad consulta de precios, de acuerdo a lo establecido en el art. 96 del Decreto con Rango, Valor y Fuerza de la Ley de Contrataciones Públicas (LCP). Este procedimiento ha sido iniciado por el Área de Compras y Suministros adscrita a la Dirección de Administración de la Contraloría del estado Sucre, con la finalidad'), 0, 1,'J');

         $this->atFPDF->MultiCell(200, 5, utf8_decode('	        Me dirijo a usted (es) en la oportunidad de informarle (s) que han sido seleccionado (s), para participar en el procedimiento de contratación bajo la modalidad consulta de precios, de acuerdo a lo establecido en el art. 96 del Decreto con Rango, Valor y Fuerza de la Ley de Contrataciones Públicas (LCP). Este procedimiento ha sido iniciado por la División de Compras y Suministros adscrita a la Dirección de Administración de la Contraloría del estado Sucre, con la finalidad '.$invitacion['ind_nombre_procedimiento'].' .

	        La recepción de la manifestación de voluntad de participar y/o de ofertar en el presente procedimiento esta programada para la fecha '.$fecFinCotizacion.' en conformidad con el Art. 67 LCP numeral 4, la misma debe ser entregada ante la Dirección de Administración, División de Compras y Suministros de la Contraloría del estado Sucre, cotizando ofertas que tengan una validez de 10 días hábiles.
	
	            Dichas ofertas (cotización) debe emitirse a nombre de la Contraloría del estado Sucre Rif: G-20001224-2, en idioma castellano, montos expresados en moneda de curso legal (Bs.), e indicando número de cotización o presupuesto, condiciones de pago, plazo de entrega, fecha, validez de la oferta, RIF y nombre de su empresa, precio unitario por el renglón del servicio que ofrece suministrar, subtotal, y precio total de los servicios ofertados, incluyendo la discriminación del Impuesto al Valor Agregado (IVA). (si aplica).

	        En caso de recibir ofertas, superiores a 2500 U.T, el beneficiario de la adjudicación deberá cumplir con el  compromiso de responsabilidad social y su ejecución debe estar debidamente garantizada tal como lo establece el articulo 30 y 31 del Decreto con Rango Valor y Fuerza de Ley de Contrataciones Publica.
	        
	        El valor del PRESUPUESTO BASE para '.$proce.' es de Bs. '.number_format($invitacion['num_presupuesto_base'],2,',','.').' el cual tiene su basamento legal en el Art. 59 de la Ley de Contrataciones Públicas.
	
	        Art. 69 y 70 de la LCP. Podrá solicitar aclaratoria por escrito el día hábil siguiente al término del primer día de la fecha de este documento y la División de Compras y Suministros de la Contraloría del estado Sucre, aclarará por escrito la inquietud expresada.

	        Art. 131 numeral 1 y 2 de la LCP. Serán causas que darán origen a modificaciones del contrato las siguientes:

                1.- El incremento o reducción en la cantidad de la obra, bienes o servicios originalmente contratados.

                2.- Que surjan nuevas partidas o renglones a los contemplados en el contrato

	        Art. 101 del R.L.C.P. En las modalidades de Concurso Abierto, Concurso Cerrado y Consulta de Precios se adjudicará la contratación a la oferta que resulte con la primera opción, por el monto y cantidades de la oferta presentada, salvo que en el pliego de condiciones se indique la posibilidad de aumento o disminución de cantidades de bienes, servicio u obra, al momento de la adjudicación, siempre y cuando el aumento de estas se mantenga cuantitativamente dentro del rango que caracteriza la modalidad aplicada.
	   '), 0, 'J',false );
$this->atFPDF->ln(40);
        $this->atFPDF->MultiCell(200, 5, utf8_decode('   Art. 123 de LCP. Para asegurar el cumplimiento de todas las obligaciones que asume el contratista, con ocasión del contrato para la adquisición de bienes, prestación de servicios o ejecución de obras,  cuando se requiera, este deberá constituir una garantía a satisfacción del veinte por ciento (20%) del monto del contrato incluyendo tributos.
        
        EVALUACIÓN DE OFERTAS. (Art. 76, 94 y  95 de la LCP)

	    Fase I EVALUACIÓN LEGAL: Se examina y determina que los documentos  consignados, se ajusten a la presente solicitud, en caso contrario no pasara a la siguiente fase y será objeto de rechazo de la oferta.

	Fase II EVALUACIÓN TÉCNICA (CUALITATIVA): Se examina y determina que las especificaciones técnicas sean las requeridas, como una primera evaluación, la cual se realizará mediante el estudio de cada renglón ofertado con las descripciones técnicas exigidas, si cumple con lo requerido, pasa a la segunda evaluación la cual consiste en describir la variable aplicable entre los números de renglones debidamente ofertados en la Matriz de Puntaje Evaluado, esta última contará con una ponderación de cincuenta (50) puntos.

	Fase III EVALUACIÓN FINANCIERA (CUANTITATIVA): Se determinará por la evaluación de los ítem, posteriormente se aplican los beneficios del Decreto 4.998 (Valor Agregado Nacional) de ser el caso; en la Matriz de Puntaje Evaluado donde se realizá la evaluación del monto ofertado, esta se efectuará por cada renglón y precio unitario, el puntaje para este caso consistirá en cincuenta (50) puntos en total, se evaluará el tiempo de entrega establecido en el pliego de condiciones, el cual contará con una ponderación de diez o veinte (10 ó 20) puntos, la evaluación de las condiciones de pago será de cero o diez (0 ó 10) puntos, y el cumplimiento de los cincuenta (50) puntos.
	
	OTORGAMIENTO DE LA ADJUDICACIÓN

	 El contratante se reserva el derecho de adjudicar la totalidad o parte del objeto de la contratación entre las ofertas presentadas, de acuerdo a lo previsto en el Art. 109 de la LCP.

	El Órgano Contratante tomará en cuenta a la hora de otorgar la adjudicación la calidad del servicio o bien (es) ofertado (s), basando su criterio en el reconocimiento que tenga el servicio o bien en el mercado de consumo Venezolano. Art. 66 de la LCP. 

	Los oferentes que obtengan la segunda, tercera y subsiguientes opciones tienen, en este mismo orden, siempre que mantengan su oferta, el derecho a que les sea otorgada la ADJUDICACIÓN, en caso de que los oferentes adjudicatarios no mantengan su oferta, no suministren las garantías requeridas, se niegue a firmar el contrato o le sea anulada la ADJUDICACIÓN, por haber suministrado información falsa o por violación de disposiciones legales que rigen la materia de Contrataciones Publicas.  Art. 112 de la LCP.

	Las ofertas presentadas, serán sometidas a una evaluación cualitativa y cuantitativa por la División de Compras y Suministros, basándose en los criterios de calidad, durabilidad, mejor oferta y mecanismos de los distintos aspectos establecidos en las condiciones de participar (pliego de condiciones), la misma elabora un informe de recomendación, indicando la oferta que resulte con la primera opción, según los criterios y mecanismos previstos en el pliego de condiciones, así como la existencia de ofertas que merezcan la segunda o tercera opción. Art. 95 y 110 de la LCP y Art. 22 y 99 del Reglamento.

	En caso de ser otorgada la adjudicación y el proveedor solicite anticipo deberá presentar una fianza por el cien por ciento (100%) del monto otorgado como anticipo. Art. 122 de la LCP y Art.135 del Reglamento.

	La Contraloría del estado Sucre utilizará los métodos de Control Perceptivo que sean necesarios para verificar la correcta ejecución de la presente contratación a celebrarse entre el Órgano Contratante y la empresa adjudicataria, así como también, constatará que los servicios adquiridos se ajusten a las especificaciones técnicas exigidas en el presente pliego y sean de igual calidad que las muestras presentadas por el oferente adjudicatario.

	
	
	
	SUPUESTOS GENERADORES DE SANCIONES A LOS PARTICULARES:

	Art. 167. de la Ley de Contrataciones Públicas. "Sin perjuicio de la Responsabilidad Civil, Administrativa y Penal que corresponda, se consideran infracciones de los particulares al presente Decreto con Rango, Valor y Fuerza de la Ley, los siguientes supuestos:
    1. Cuando el contrato sea rescindido por incumplimiento o por cualquier otra causa imputable al contratista.
    2. Cuando por causas que le sean imputables al contratista, incumpla cualquier otra obligación de ley, o que hubiere asumido para el contratante, aun cuando ello no importe rescisión del contrato.
    3. En caso de suministro o presentación de información o documentación falsa, antes los contratantes o ante el Servicio Nacional de Contrataciones.
    4. Cuando retiren ofertas durante su vigencia.
    5. Cuando siendo beneficiarios de la adjudicación no suscriban el contrato dentro del plazo establecido.
    6. Cuando incurran en practicas de mala fe o empleen practicas fraudulentas en los tramites y procesos regulados por este Decreto con Rango, Valor y Fuerza de Ley.'), 0, 'J',false );
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(200, 6,utf8_decode( 'DICHO REQUERIMIENTO SE DETALLA ACONTINUACIÓN:'), 0, 1, 'C');
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);

        $this->atFPDF->SetWidths(array(15, 140, 20, 20));
        $this->atFPDF->SetAligns(array('C', 'L', 'C', 'R'));

        $this->atFPDF->Row(array('Item',
            'Descripcion',
            'Uni.',
            'Cant.'));

        $this->atFPDF->Ln(2);
        $this->atFPDF->SetFont('Arial', '',9);
        $this->atFPDF->SetDrawColor(255, 255, 255);

        foreach ($requerimientos as $req){

            $descripcion = $req['ind_descripcion'];

            $this->atFPDF->Ln(3);
            $this->atFPDF->Row(array($req['num_secuencia'],
                utf8_decode($descripcion),
                $req['unidad'],
                number_format($req['num_cantidad_pedida'],2, ',', '.')));

        }

        $this->atFPDF->Ln(10);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $ob=utf8_decode($invitacion['ind_observaciones']);

        $this->atFPDF->Cell(98, 5, utf8_decode('Observaciones: '), 0, 1, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->MultiCell(195, 5, $ob,0, 'J');

        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->Rect(70, 200, 75, 0.1, "D");
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->SetXY(10, 205); $this->atFPDF->MultiCell(195, 3, utf8_decode($usuario['nombre']), 0, 'C');
        $this->atFPDF->SetXY(10, 210); $this->atFPDF->MultiCell(195, 3, utf8_decode($usuario['ind_descripcion_cargo']), 0, 'C');

        $this->atFPDF->SetY(220);
        $this->atFPDF->SetDrawColor(0, 0, 0); $this->atFPDF->SetFillColor(0, 0, 0); $this->atFPDF->SetTextColor(0, 0, 0);

        $this->atFPDF->Rect(10, 220, 200, 0.1, "DF");

        $this->atFPDF->SetFillColor(245, 245, 245);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetXY(10, 225); $this->atFPDF->Cell(98, 5, utf8_decode('Condiciones de Entrega: '), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(10, 230 ); $this->atFPDF->MultiCell(98, 4, utf8_decode($invitacion['ind_condiciones']), 0, 'L');
        $y = $this->atFPDF->GetY();
        $this->atFPDF->SetXY(10, $y+5 ); $this->atFPDF->Cell(110, 4, utf8_decode('Direccion: '.APP_ORGANISMO_DIRECCION), 0, 0, 'L', 1);
        $this->atFPDF->SetXY(10, $y+10 ); $this->atFPDF->Cell(110, 4, utf8_decode('Correo Eléctronico: despacho@cgesucre.gob.ve, Zona Postal: 6101, Twitter: @CESucre.'), 0, 0, 'L', 1);

        #salida del pdf
        $this->atFPDF->Output('invitacion.pdf', 'I');

    }

    //Método para anular la invitacion
    public function metAnularInvitacion()
    {
        $idInvitacion = $this->metObtenerInt('idInvitacion');
        if($idInvitacion!=0){

            $id=$this->atInvitacion->metAnularInvitacion($idInvitacion);

            if(is_array($id)){
                $valido=array(
                    'status'=>"error",
                    'mensaje'=>'Disculpa. Pero no se puede anular la invitacion'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idInvitacion
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar las invitaciones de los proveedores
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                    invitacion.*,
                    CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS proveedor,
                    
                    (SELECT SUM(num_total) FROM lg_c003_cotizacion AS cotizacion,lg_b010_invitacion AS invitacion3 
                    WHERE 
                    invitacion3.ind_num_invitacion = invitacion.ind_num_invitacion
                    AND invitacion3.fec_anio = invitacion.fec_anio
                    AND cotizacion.fk_lgb010_num_invitacion = invitacion3.pk_num_invitacion
                    ) AS totalCotizado,
    
                    (SELECT COUNT(*) FROM lg_b010_invitacion AS invitacion2
                     WHERE invitacion.ind_num_invitacion = invitacion2.ind_num_invitacion
                     AND invitacion.fec_anio = invitacion2.fec_anio
                    ) AS nroLineas,
                    cotizacion2.pk_num_cotizacion,
                    (SELECT 
                      COUNT(*) 
                    FROM 
                      lg_b012_informe_recomendacion AS recomendacion,
                      lg_b011_evaluacion AS evaluacion,
                      lg_b009_acta_inicio AS inicio ,
                      lg_b023_acta_detalle AS detalle 
                      WHERE recomendacion.ind_estado = 'AP'
                      AND recomendacion.fk_lgb011_num_evaluacion = evaluacion.pk_num_evaluacion
                      AND evaluacion.fk_lgb009_num_acta_inicio = inicio.pk_num_acta_inicio
                      AND detalle.fk_lgb009_num_acta_inicio = inicio.pk_num_acta_inicio 
                      AND detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
                    ) AS estadoInforme
                FROM
                    lg_b010_invitacion AS invitacion
                INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
                LEFT JOIN lg_c003_cotizacion AS cotizacion2 ON cotizacion2.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (acta.ind_codigo_acta LIKE '%$busqueda[value]%' OR
                        acta.ind_nombre_procedimiento LIKE '%$busqueda[value]%' OR
                        acta.ind_modalidad_contratacion LIKE '%$busqueda[value]%' OR
                        acta.ind_estado LIKE '%$busqueda[value]%')
                        ";
        }
        $sql .= " GROUP BY invitacion.ind_num_invitacion ";
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio

        $campos = array(
            'pk_num_invitacion',
            'fec_invitacion',
            'ind_num_invitacion',
            'fk_lgb022_num_proveedor',
            'proveedor',
            'totalCotizado',
            'nroLineas',
            'ind_estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_invitacion';
        #construyo el listado de botones
        $camposExtra = array(
            'pk_num_cotizacion',
            'ind_num_invitacion',
            'fec_anio');

        if (in_array('LG-01-01-10-01-I',$rol)) {
            $campos['boton']['Imprimir'] = "
                <a href='".BASE_URL."modLG/compras/invitacionesCONTROL/generarInvitacionMET/$clavePrimaria' target='_blank'>
                    <button title='Imprimir Invitación'
                            class='accion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary-light'
                            titulo='Imprimir Invitación'
                            id='imprimir'>
                        <i class='md md-print'></i>
                    </button>
                </a>
                "
            ;
        } else {
            $campos['boton']['Imprimir'] = false;
        }

        if (in_array('LG-01-01-10-02-A',$rol)) {
            $campos['boton']['Anular'] = array("
                <button title='Anular Invitación' id='anular$clavePrimaria'
                        class='anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger'
                        titulo='Anular Invitación'
                        mensaje='Desea Anular esta Invitacion?'
                        idInvitacion='$clavePrimaria'>
                    <i class='md md-block'></i>
                </button>
                ",
                'if( $i["ind_estado"] == "PR" AND $i["pk_num_cotizacion"] == NULL) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Anular'] = false;
        }

        if (in_array('LG-01-01-10-03-C',$rol)) {
            $campos['boton']['Cotizar'] = array("
                <button title='Cotizar' data-toggle='modal' data-target='#formModal'
                        class='cotizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        titulo='Cotizar por Invitación a Proveedor'
                        numInvitacion='ind_num_invitacion'
                        anio='fec_anio'>
                    <i class='md md-attach-money'></i>
                </button>
                ",
                'if( $i["estadoInforme"] == 0 ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
                );
        } else {
            $campos['boton']['Cotizar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);
    }

}
