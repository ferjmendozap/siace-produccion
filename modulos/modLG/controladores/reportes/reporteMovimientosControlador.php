<?php
class reporteMovimientosControlador extends Controlador
{
    public $atReporte;
    public $atAlmacenModelo ;
    public $atItemsModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteMovimientos', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('headerMovimientos','modLG');
        $this->atFPDF = new pdf('p','mm','letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('almacen',$this->atReporte->atAlmacenModelo ->metListaralmacen());
        $this->atVista->metRenderizar('filtro');
    }

    public function metGenerarReporte($stock,$almacen,$buscar,$fdesde,$fhasta,$numItem)
    {
        $sql = "WHERE 1 ";
        $consultaStock = array(
            '1'=>'',
            '2'=>' AND stock.num_stock_actual != "0" ',
            '3'=>' AND stock.num_stock_actual = "0" ',
            'no'=>''
        );
        $sql .= $consultaStock[$stock];

        if($buscar!='no'){
            $sql .=" AND ( i.pk_num_item LIKE '%$buscar%' OR 
                            i.ind_codigo_interno LIKE '%$buscar%' OR 
                            i.ind_descripcion LIKE '%$buscar%' OR 
                            unidad.ind_descripcion LIKE '%$buscar%' OR 
                            stock.num_stock_actual LIKE '%$buscar%' OR 
                            stock.num_stock_inicial LIKE '%$buscar%'
                            ) "; }
        if($numItem!='no'){ $sql .=" AND i.pk_num_item='$numItem'"; }
        if($almacen!='no'){ $sql .=" AND almacen.pk_num_almacen='$almacen'"; }

        $fd='';
        if($fdesde!='no'){
            $fd = $fdesde;
        }
        $fh='';
        if($fhasta!='no'){
            $fh = $fhasta;
        }

        $elaboradoPor = $this->atReporte->metMostrarEmpleado(Session::metObtener('idEmpleado'));
        $revisadoPor = $this->atReporte->metMostrarEmpleado(35);//#35 pk_num_empleado Juan Hernandez
        $conformadoPor = $this->atReporte->metMostrarEmpleado(36);//#36 pk_num_empleado Geovanny Caraballo
        $aprobadoPor = $this->atReporte->metMostrarEmpleado(67);//#67 pk_num_empleado Contralor Andy Vazquez



        define('ELABORADO_POR',$elaboradoPor['nombre']);
        define('ELABORADO_CARGO',$elaboradoPor['ind_descripcion_cargo']);
        define('REVISADO_POR',$revisadoPor['nombre']);
        define('REVISADO_CARGO',$revisadoPor['ind_descripcion_cargo']);
        define('CONFORMADO_POR',$conformadoPor['nombre']);
        define('CONFORMADO_CARGO',$conformadoPor['ind_descripcion_cargo']);
        define('APROBADO_POR',$aprobadoPor['nombre']);
        define('APROBADO_CARGO',$aprobadoPor['ind_descripcion_cargo']);

        define('FDESDE',$fd);
        define('FHASTA',$fh);

        $sql .= " ORDER BY i.ind_descripcion ASC";
        $consulta = $this->atReporte->atItemsModelo->metListarItems(false,$sql);

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(1, 35);
        $this->atFPDF->AddPage();

        $this->atFPDF->SetFont('Arial', '', 6);

        foreach($consulta AS $titulo=>$valor){
            $sql2 = "";
            $sql3 = "";
            if($fdesde!='no'){
                $anio = substr($fdesde,0,4);
                $mes = substr($fdesde,5,2);
                $sql2 .=" AND transaccion.fec_anio>='$anio' AND transaccion.fec_mes>='$mes'";
            }
            if($fhasta!='no'){
                $anio = substr($fhasta,0,4);
                $mes = substr($fhasta,5,2);
                $sql2 .=" AND transaccion.fec_anio<='$anio' AND transaccion.fec_mes<='$mes'";
                $sql3 .=" AND transaccion.fec_anio<='$anio' AND transaccion.fec_mes<'$mes'";
            }
            if ($sql2==''){
                $sql2 = false;
            }
            if ($sql3==''){
                $sql3 = false;
            }

            $recibido = $this->atReporte->metBuscarRecibidoEnviado($valor['pk_num_item'],'I',$sql2);
            if(!$recibido['cantidad']){
                $recibido['cantidad'] = '0.00';
            }
            $enviado = $this->atReporte->metBuscarRecibidoEnviado($valor['pk_num_item'],'E',$sql2);
            if(!$enviado['cantidad']){
                $enviado['cantidad'] = '0.00';
            }

            $recibido2 = $this->atReporte->metBuscarRecibidoEnviado($valor['pk_num_item'],'I',$sql3);
            if(!$recibido2['cantidad']){
                $recibido2['cantidad'] = '0.00';
            }
            $enviado2 = $this->atReporte->metBuscarRecibidoEnviado($valor['pk_num_item'],'E',$sql3);
            if(!$enviado2['cantidad']){
                $enviado2['cantidad'] = '0.00';
            }

            $cantRec = $recibido['cantidad'];
            $cantEnv = $enviado['cantidad'];
            $cantRec2 = $recibido2['cantidad'];
            $cantEnv2 = $enviado2['cantidad'];

            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->Row(array(
                $valor['pk_num_item'],
                $valor['ind_codigo_interno'],
                utf8_decode($valor['ind_descripcion']),
                $valor['unidad'],
                number_format($cantRec2-$cantEnv2,6),
                number_format($cantRec,6),
                number_format($cantEnv,6),
                number_format(($cantRec+$cantRec2)-($cantEnv+$cantEnv2),6)
            ));
            $this->atFPDF->Ln(1);
        }

        #salida del pdf
        $this->atFPDF->Output();
    }

}


?>