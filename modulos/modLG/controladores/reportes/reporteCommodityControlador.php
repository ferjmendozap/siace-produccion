<?php
class reporteCommodityControlador extends Controlador
{
    public $atReporte;
    public $atCommodityModelo;
    public $atClasificacionModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteCommodity', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('headerCommodity','modLG');
        $this->atFPDF = new pdf('p','mm','letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'select2/select2.min',
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('commodity',$this->atReporte->atCommodityModelo->metListarCommodity());
        $this->atVista->metRenderizar('filtro');
    }

    public function metGenerarReporte($commodity)
    {
        $consulta = $this->atReporte->atCommodityModelo->metListarCommodity();
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(1, 20);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Arial', '', 6);

        foreach($consulta AS $titulo=>$valor){
            if($commodity=='no') {
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->Row(array(
                    $consulta[$titulo]['ind_descripcion'],
                    $consulta[$titulo]['unidad'],
                    $consulta[$titulo]['cod_partida'],
                    $consulta[$titulo]['cod_cuenta']
                ));
                $this->atFPDF->Ln(1);
            } elseif($commodity==$consulta[$titulo]['pk_num_commodity']){
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->Row(array(
                    $consulta[$titulo]['ind_descripcion'],
                    $consulta[$titulo]['unidad'],
                    $consulta[$titulo]['cod_partida'],
                    $consulta[$titulo]['cod_cuenta']
                ));
                $this->atFPDF->Ln(1);
            }
        }

        #salida del pdf
        $this->atFPDF->Output();
    }

}


?>