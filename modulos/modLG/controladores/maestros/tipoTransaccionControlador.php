<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class tipoTransaccionControlador extends Controlador
{
    private $atTipoTran;
    private $atTipoDocumentoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTipoTran = $this->metCargarModelo('tipoTransaccion', 'maestros');
    }

    //Método para listar los tipos de transaccion
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar los tipos de transaccion
    public function metCrearModificarTipoTran()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idTipoTran = $this->metObtenerInt('idTipoTran');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $Excceccion1 = array('vATipoTran','vCTipoTran','estadoTipoTran');
            $ind = $this->metValidarFormArrayDatos('formTT','int',$Excceccion1);
            $alphaNum = $this->metValidarFormArrayDatos('formTT','alphaNum');

            if ($alphaNum != null && $ind == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($ind, $alphaNum);
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['estadoTipoTran'])) {
                $validacion['estadoTipoTran'] = 0;
            }
            if (!isset($validacion['vCTipoTran'])) {
                $validacion['vCTipoTran'] = 0;
            }
            if (!isset($validacion['vATipoTran'])) {
                $validacion['vATipoTran'] = 0;
            }

            if($idTipoTran==0){
                $id=$this->atTipoTran->metCrearTipoTran(
                    $validacion['descTipoTran'],$validacion['tipoMov'],$validacion['tipoDocGen'],
                    $validacion['tipoDocTran'],$validacion['vCTipoTran'],$validacion['vATipoTran'],
                    $validacion['estadoTipoTran'],$validacion['codigo']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoTran->metModificarTipoTran(
                    $validacion['descTipoTran'],$validacion['tipoMov'],$validacion['tipoDocGen'],
                    $validacion['tipoDocTran'],$validacion['vCTipoTran'],$validacion['vATipoTran'],
                    $validacion['estadoTipoTran'],$validacion['codigo'],$idTipoTran);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }

            $validacion['idTipoTran']=$id;

            echo json_encode($validacion);
            exit;
        }

        $this->atVista->assign('formDBTT1',$this->atTipoTran->metListarTipoMov());
        $this->atVista->assign('formDBTT2',$this->atTipoTran->atTipoDocumentoModelo->metListarTipoDoc());
        if($idTipoTran!=0){
            $this->atVista->assign('formDBTT',$this->atTipoTran->metMostrarTipoTran($idTipoTran));
            $this->atVista->assign('idTipoTran',$idTipoTran);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar los tipos de transaccion
    public function metEliminarTipoTran()
    {
        $idTipoTran = $this->metObtenerInt('idTipoTran');
        if($idTipoTran!=0){
            $id=$this->atTipoTran->metEliminarTipoTran($idTipoTran);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idTipoTran
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar los tipos de transaccion
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM lg_b015_tipo_transaccion ";
        if ($busqueda['value']) {
            $sql .= " WHERE 
            pk_num_tipo_transaccion LIKE '%$busqueda[value]%' OR
            ind_cod_tipo_transaccion LIKE '%$busqueda[value]%' OR
            ind_descripcion LIKE '%$busqueda[value]%' 
            ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_tipo_transaccion','ind_cod_tipo_transaccion','ind_descripcion','num_flag_voucher_consumo','num_flag_voucher_ajuste','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_tipo_transaccion';
        #construyo el listado de botones
        $flags = array('num_flag_voucher_consumo','num_flag_voucher_ajuste');

        if (in_array('LG-01-07-03-03-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado el Tipo de Transacción Nro. '.$clavePrimaria.'"
                        idTipoTran="'.$clavePrimaria.'" titulo="Modificar Tipo de Transacción"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-03-03-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado el Tipo de Transacción Nro. '.$clavePrimaria.'"
                        idTipoTran="'.$clavePrimaria.'" titulo="Consultar Tipo de Transacción"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-03-03-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Tipo de Transacción Nro. '.$clavePrimaria.'"
                        idTipoTran="'.$clavePrimaria.'" titulo="Eliminar Tipo de Transacción"
                        mensaje="Estas seguro que desea eliminar el Tipo de Transacción!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,$flags);

    }


}