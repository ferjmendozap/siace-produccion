<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class aspectosCualitativosControlador extends Controlador
{
    private $atAspectosModelo;
    private $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atAspectosModelo = $this->metCargarModelo('aspectosCualitativos', 'maestros');
    }

    //Método para listar los aspectos cualitativos y cuantitativos
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar los aspectos
    public function metCrearModificarAspecto()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idAspecto = $this->metObtenerInt('idAspecto');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $Excceccion = array('num_estatus');
            $ind = $this->metValidarFormArrayDatos('form','int',$Excceccion);
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $ind == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($ind, $alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = 0;
            }

            $validacion['idAspecto']=$idAspecto;
            $parametro = $this->atAspectosModelo->metMostrarParametro('PMEC');
            $miscelaneo = $this->atAspectosModelo->atMiscelaneoModelo->metMostrarSelect('TAE');
            $cod_detalle = 0;
            foreach ($miscelaneo as $key=>$value) {
                if($value['pk_num_miscelaneo_detalle']==$validacion['fk_a006_num_miscelaneo_detalle']){
                    $cod_detalle = $value['cod_detalle'];
                }
            }
            $valorParametro = 0;
            if($cod_detalle=='01'){
                #cualitativos
                $valorParametro = $parametro['ind_valor_parametro'];
            } elseif($cod_detalle=='02'){
                #cuantitativos
                $valorParametro = 100-$parametro['ind_valor_parametro'];
            }
            $puntaje = $this->atAspectosModelo->metContarTotalPuntaje($idAspecto,$cod_detalle);

            if(($puntaje['maximo']+$validacion['num_puntaje_maximo'])>$valorParametro){
                $validacion['status'] = 'errorMax';
                $validacion['num_puntaje_maximo'] = 'error';
                $validacion['puntajeMaximo'] = $parametro['ind_valor_parametro'];
                echo json_encode($validacion);
                exit;
            }

            if($idAspecto==0){
                $id=$this->atAspectosModelo->metCrearAspecto($validacion);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atAspectosModelo->metModificarAspecto($validacion);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            echo json_encode($validacion);
            exit;
        }

        $this->atVista->assign('maximo','50');
        if($idAspecto!=0){
            $this->atVista->assign('formDB',$this->atAspectosModelo->metMostrarAspecto($idAspecto));
            $this->atVista->assign('idAspecto',$idAspecto);
            $this->atVista->assign('ver',$ver);
        }
        $this->atVista->assign('tipoAspecto',$this->atAspectosModelo->atMiscelaneoModelo->metMostrarSelect('TAE'));

        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar los aspectos
    public function metEliminarAspecto()
    {
        $idAspecto = $this->metObtenerInt('idAspecto');
        if($idAspecto!=0){
            $id=$this->atAspectosModelo->metEliminarAspecto($idAspecto);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Aspecto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idAspecto
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar los aspectos
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  *
                FROM lg_e007_aspecto_cualitativo ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        pk_num_aspecto LIKE '%$busqueda[value]%' OR
                        ind_cod_aspecto LIKE '%$busqueda[value]%' OR
                        ind_nombre LIKE '%$busqueda[value]%' OR
                        num_puntaje_maximo LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_aspecto','ind_cod_aspecto','ind_nombre','num_puntaje_maximo','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_aspecto';

        if (in_array('LG-01-07-05-02-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado el Aspecto Nro. '.$clavePrimaria.'"
                        idAspecto="'.$clavePrimaria.'" titulo="Modificar Aspecto"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-05-02-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado el Aspecto Nro. '.$clavePrimaria.'"
                        idAspecto="'.$clavePrimaria.'" titulo="Consultar Aspecto"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-05-02-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Aspecto Nro. '.$clavePrimaria.'"
                        idAspecto="'.$clavePrimaria.'" titulo="Eliminar Aspecto"
                        mensaje="Estas seguro que desea eliminar el Aspecto!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }


}
