<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de los Aspectos Cualitativos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Puntaje Máximo</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="5">
                                    {if in_array('LG-01-07-05-02-01-N',$_Parametros.perfil)}
                                        <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                                descipcion="el Usuario ha creado el aspecto Nro. " data-toggle="modal"
                                                data-target="#formModal" titulo="Registrar Nuevo Aspecto" id="nuevo">
                                            <i class="md md-create"></i> Nuevo Aspecto
                                    </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modLG/maestros/aspectosCualitativosCONTROL/crearModificarAspectoMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/maestros/aspectosCualitativosCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_aspecto" },
                    { "data": "ind_cod_aspecto" },
                    { "data": "ind_nombre" },
                    { "data": "num_puntaje_maximo"},
                    { "orderable": false,"data": "num_estatus"},
                    { "orderable": false,"data": "acciones"}
                ]
        );

        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAspecto: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAspecto: $(this).attr('idAspecto')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAspecto: $(this).attr('idAspecto'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idAspecto = $(this).attr('idAspecto');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/maestros/aspectosCualitativosCONTROL/eliminarAspectoMET';
                $.post(url, { idAspecto: idAspecto },function(dato){
                    if(dato['status']=='ok'){
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        swal("Eliminado!", "el Aspecto fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>