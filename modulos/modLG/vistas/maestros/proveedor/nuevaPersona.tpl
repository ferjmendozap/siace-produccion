<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<form action="{$_Parametros.url}modCV/maestros/personaCONTROL/{if isset($ver) }verMET{else}crearModificarMET{/if}{if isset($visita) && $visita!=false}/codvisita{/if}" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body ">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{if isset($visita)}{$visita}{/if}" name="visita" />

        {if isset($idPersona) }
            <input type="hidden" value="{$idPersona}" name="idPersona"/>
        {/if}
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <div class="col-sm-6" id="zonaFoto"></div>
                    <div class="col-sm-6" id="upload_results" style="background-color:#eee;">
                        {if isset($formDB.ind_foto)}
                            <img src="{$_Parametros.url}publico/imagenes/modCV/fotosVisitantes/{$formDB.ind_foto}.jpg">
                        {else}
                            No Tiene Foto Cargada
                        {/if}
                    </div>
                    <div class="col-sm-12">
                        <input type=button value="Tomar Foto" id="tomarFoto">
                        &nbsp;&nbsp;
                        <input type=button value="Limpiar" onClick="webcam.reset()">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group floating-label" id="ind_cedula_documentoError">
                        <input type="text" class="form-control" data-inputmask="'mask': '99999999'" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" {if isset($formDB.ind_cedula_documento)} readonly {/if} name="form[alphaNum][ind_cedula_documento]" id="ind_cedula_documento"{if isset($ver) }disabled{/if}>
                        <label for="ind_cedula_documento"><i class="md md-perm-identity"></i> Cedula </label>
                    </div>
                </div>
                <div class="col-sm-2">

                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estaus) and $formDB.num_estaus==1} checked{/if} value="1" id="cne" {if isset($ver) }disabled{/if}>
                            <span>Consultar con CNE</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_nombre1Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}" name="form[alphaNum][ind_nombre1]" id="ind_nombre12"{if isset($ver) }disabled{/if}>
                        <label for="ind_nombre1"><i class="fa fa-pencil"></i> Nombre </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_apellido1Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}" name="form[alphaNum][ind_apellido1]" id="ind_apellido12"{if isset($ver) }disabled{/if}>
                        <label for="ind_apellido1"><i class="fa fa-pencil"></i> Apellido </label>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="form-group floating-label" id="ind_direccionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}" name="form[alphaNum][ind_direccion]" id="ind_direccion"{if isset($ver) }disabled{/if}>
                        <label for="ind_direccion"><i class="md md-location-city"></i> Domicilio </label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="ind_emailError">
                        <input type="email" class="form-control" value="{if isset($formDB.ind_email)}{$formDB.ind_email}{/if}" name="form[formula][ind_email]" id="ind_email" {if isset($ver) }disabled{/if} >
                        <label for="ind_email"><i class="fa fa-at"></i> Email</label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="ind_telefonoError">
                        <input type="text" class="form-control" data-inputmask="'mask': '(9999)999-9999'" value="{if isset($formDB.ind_telefono)}{$formDB.ind_telefono}{/if}" name="form[alphaNum][ind_telefono]" id="ind_telefono"{if isset($ver) }disabled{/if}>
                        <label for="ind_telefono"><i class="md md-settings-cell"></i> Telefono</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus)} {if $formDB.num_estatus==1} checked {/if} {else} checked {/if} value="1" name="form[int][num_estatus]"{if isset($ver) }disabled{/if}>
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group floating-label" id="fec_ultima_modificacionError">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                            <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                        </div>
                    </div>
                </div>
            </div>
            <span class="clearfix"></span>
        </div>
        {if isset($ver)}

        {else}
            <div class="modal-footer">
                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
                    <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion2">
                    {if isset($idPersona) and $idPersona!=0}
                        <i class="fa fa-edit"></i>&nbsp;Modificar
                    {else}
                        <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                    {/if}
                </button>
            </div>
        {/if}
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();
        webcam.set_quality( 90 ); // JPEG quality (1 - 100)
        webcam.set_shutter_sound( true, '{$_Parametros.ruta_Complementos}JPEGCam/shutter.mp3' ); // play shutter click sound
        webcam.set_swf_url( '{$_Parametros.ruta_Complementos}JPEGCam/webcam.swf' ); // play shutter click sound
        $('#zonaFoto').html(webcam.get_html(320, 240));
        $("#formAjax").submit(function(){
            return false;
        });
        $(":input").inputmask();
        $('#modalAncho').css("width","60%");
        $('#tomarFoto').click(function () {
            if($('#ind_cedula_documento').val()!=''){
                webcam.set_api_url( '{$_Parametros.url}modCV/visita/visitaCONTROL/cargarFotoMET/'+$('#ind_cedula_documento').val());
                webcam.freeze()
                setTimeout(function (){
                    do_upload();
                }, 700);
            }else{
                swal("Error!", 'Disculpa antes de tomar la Foto Tiene que colocar la cedula', "error");
            }
        });

        $('#cne').on('change',function () {
            if($(this).attr('checked')=='checked'){

                var cedula = $('#ind_cedula_documento').val();
                cedula = cedula.replace('_','');
                console.log(cedula);
                $.post('{$_Parametros.url}modCV/maestros/personaCONTROL/consultaCneMET/', { idCedula: cedula }, function (dato) {

                    if(dato['status']=='errorIns'){
                        swal('Error!','Disculpa. Esta cédula de identidad no se encuentra inscrita en el Registro Electoral','error');
                    } else if(dato['status']=='errorCurl'){
                        app.metValidarError(dato,'Disculpa. la versión de PHP no soporta CURL, contacte al administrador');
                    } else if(dato['status']=='correcto'){
                        $('#ind_nombre12').val(dato['nombres']);
                        var cantidad = dato['apellidos'].length;

                        var apellidos = '';
                        for (var i=0; i<cantidad; i=i+1){
                            if(dato['apellidos'][i]!='	'){
                                apellidos += dato['apellidos'][i];
                            } else {
                                i=cantidad;
                            }
                        }

                        $('#ind_apellido12').val(apellidos);

                    }
                }, 'json');
            } else {
                $('#ind_nombre12').val('');
                $('#ind_apellido12').val('');
            }
        });



        $('#accion2').click(function () {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'modificar') {
                    swal("Registro Modificado!", 'Los datos de la Persona se guardaron satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');
                } else if (dato['status'] == 'nuevo') {
                    {if isset($visita) && $visita!=false}

                    {else}
                        swal("Registro Guardado!", 'Los datos de la Persona se guardaron satisfactoriamente.', "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                    {/if}
                }
            }, 'json');
        });
    });
</script>
<script language="JavaScript">
    webcam.set_hook( 'onComplete', 'my_completion_handler' );
    function do_upload() {
        // upload to server
        document.getElementById('upload_results').innerHTML = '<h1>Uploading...</h1>';
        webcam.upload();
    }
    function my_completion_handler(msg) {
        // extract URL out of PHP output
        if (msg.match(/(http\:\/\/\S+)/)) {
            var image_url = RegExp.$1;
            // show JPEG image in page
            $(document.getElementById('upload_results')).html('<img src="' + image_url + '">');
            // reset camera for another shot
            webcam.reset();
        } else { alert("PHP Error: " + msg); }
    }
</script>