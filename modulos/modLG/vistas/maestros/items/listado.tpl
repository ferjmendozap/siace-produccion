<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado del Items</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nro.</th>
                            <th>Código.</th>
                            <th>Descripción</th>
                            <th width="200px">Unidad de Compra</th>
                            <th width="100px">Estado</th>
                            <th width="100px">Acciones</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('LG-01-07-02-01-01-N',$_Parametros.perfil)}
                                    <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                            descipcion="el Usuario ha creado el item Nro. " data-toggle="modal"
                                            data-target="#formModal" titulo="Registrar Nuevo Item" id="nuevo"><i class="md md-create"></i> Nuevo
                                        Item
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modLG/maestros/itemsCONTROL/crearModificarItemsMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/maestros/itemsCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_item" },
                    { "data": "ind_codigo_interno" },
                    { "data": "ind_descripcion" },
                    { "data": "uni" },
                    { "orderable": false,"data": "num_estatus"},
                    { "orderable": false,"data": "acciones"}
                ]
        );
        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idItem: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idItem: $(this).attr('idItem')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idItem: $(this).attr('idItem'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idItem = $(this).attr('idItem');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/maestros/itemsCONTROL/eliminarItemsMET';
                $.post(url, { idItem: idItem },function(dato){
                    if(dato['status']=='ok'){
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        swal("Eliminado!", "el Item fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>