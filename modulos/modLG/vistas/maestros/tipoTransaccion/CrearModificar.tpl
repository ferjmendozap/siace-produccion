<form action="{$_Parametros.url}modLG/maestros/tipoTransaccionCONTROL/crearModificarTipoTranMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idTipoTran)}{$idTipoTran}{/if}" name="idTipoTran"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group floating-label col-lg-10" id="descTipoTranError">
                    <input type="text" class="form-control" name="formTT[alphaNum][descTipoTran]" value="{if isset($formDBTT.ind_descripcion)}{$formDBTT.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="desTipoTran">
                    <label for="desTipoTran"><i class="md md-border-color"></i>Descripcion</label>
                </div>
                <div class="form-group floating-label col-lg-2" id="codigoError">
                    <input type="text" class="form-control" name="formTT[alphaNum][codigo]" value="{if isset($formDBTT.ind_cod_tipo_transaccion)}{$formDBTT.ind_cod_tipo_transaccion}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="codigo" maxlength="3">
                    <label for="codigo"><i class="md md-border-color"></i>Código</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="tipoMovError">
                    <select class="form-control select2-list select2" id="tipoMov" name="formTT[int][tipoMov]" {if isset($ver) and $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=post1 from=$formDBTT1}
                            {if $post1.pk_num_miscelaneo_detalle==$formDBTT.fk_a006_num_miscelaneo_detalle_tipo_documento}
                                <option value={$post1.pk_num_miscelaneo_detalle} selected>{$post1.ind_nombre_detalle}</option>
                            {else}
                                <option value={$post1.pk_num_miscelaneo_detalle}>{$post1.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="tipoMov"><i class="md md-border-color"></i>Tipo de Movimiento</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="tipoDocGenError">
                    <select class="form-control select2-list select2" id="tipoDocGen" name="formTT[int][tipoDocGen]" {if isset($ver) and $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=post2 from=$formDBTT2}
                            {if $post2.pk_num_tipo_documento==$formDBTT.fk_lgb016_num_tipo_documento_generado}
                                <option value={$post2.pk_num_tipo_documento} selected>{$post2.ind_descripcion}</option>
                            {else}
                                <option value={$post2.pk_num_tipo_documento}>{$post2.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="tipoDocGen"><i class="md md-border-color"></i>Tipo Documento a Generar</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="tipoDocTranError">
                    <select class="form-control select2-list select2" id="tipoDocTran" name="formTT[int][tipoDocTran]" {if isset($ver) and $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=post2 from=$formDBTT2}
                            {if $post2.pk_num_tipo_documento==$formDBTT.fk_lgb016_num_tipo_documento_transaccion}
                                <option value={$post2.pk_num_tipo_documento} selected>{$post2.ind_descripcion}</option>
                            {else}
                                <option value={$post2.pk_num_tipo_documento}>{$post2.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="tipoDocTran">Tipo Documento Transaccion</label>
                </div>
                <div class="form-group floating-label col-lg-4">
                    <label class="checkbox-styled">
                        <input type="checkbox" name="formTT[int][vCTipoTran]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDBTT.num_flag_voucher_consumo) and $formDBTT.num_flag_voucher_consumo==1}{"checked"}{/if}>
                        <span>Voucher Consumo</span>
                    </label>
                </div>
                <div class="form-group floating-label col-lg-4">
                    <label class="checkbox-styled">
                        <input type="checkbox" name="formTT[int][vATipoTran]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDBTT.num_flag_voucher_ajuste) and $formDBTT.num_flag_voucher_ajuste==1}{"checked"}{/if}>
                        <span>Voucher Ajuste de Inventario</span>
                    </label>
                </div>
                <div class="form-group floating-label col-lg-4">
                    <label class="checkbox-styled">
                        <input type="checkbox" name="formTT[int][estadoTipoTran]" value="1" {if isset($ver) and $ver==1} disabled {/if} {if isset($formDBTT.num_estatus)} {if $formDBTT.num_estatus==1}checked{/if}{else}checked{/if}>
                        <span>Estatus</span>
                    </label>
                </div>
                <div class="form-group floating-label col-lg-6" id="usuTipoTranError">
                    <input type="text" class="form-control disabled" disabled value="{if isset($formDBTT.ind_usuario)}{$formDBTT.ind_usuario}{/if}">
                    <label for="usuTipoTran"><i class="md md-border-color"></i>Ultimo Usuario</label>
                </div>
                <div class="form-group floating-label col-lg-6" id="fecTipoTranError">
                    <input type="text" class="form-control disabled" disabled value="{if isset($formDBTT.fec_ultima_modificacion)}{$formDBTT.fec_ultima_modificacion}{/if}">
                    <label for="fecTipoTran"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                </div>
        </div>
        <!--end .col -->
    </div>
    <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idTipoTran)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "40%");
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Tipo de Transaccion fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Tipo de Transaccion fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>