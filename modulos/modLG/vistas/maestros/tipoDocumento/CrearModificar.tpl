<form action="{$_Parametros.url}modLG/maestros/tipoDocumentoCONTROL/crearModificarTipoDocMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idTipoDoc)}{$idTipoDoc}{else}0{/if}" name="idTipoDoc"/>
    <div class="modal-body">
        <div class="row">
            <div class="form-group col-lg-7">
                <label class="checkbox-styled">
                    <span>Estatus</span>
                    <input type="checkbox" name="formtipodoc[txt][estadoTipoDoc]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDBTipoDoc.num_estatus)} {if $formDBTipoDoc.num_estatus==1}checked{/if}{else}checked{/if}>
                </label>
            </div>
            <div class="form-group col-lg-7">
                <label class="checkbox-styled">
                    <span>Documento Fiscal</span>
                    <input type="checkbox" name="formtipodoc[int][fiscalTipoDoc]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDBTipoDoc.num_flag_documento_fiscal) and $formDBTipoDoc.num_flag_documento_fiscal==1}{"checked"}{/if}>
                </label>
            </div>
            <div class="form-group col-lg-8">
                <label class="checkbox-styled">
                    <span>Transaccion del Sistema</span>
                    <input type="checkbox" name="formtipodoc[int][tranTipoDoc]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDBTipoDoc.num_flag_transaccion_sistema) and $formDBTipoDoc.num_flag_transaccion_sistema==1}{"checked"}{/if}>
                </label>
            </div>
            <div class="form-group floating-label col-lg-6" id="codTipoDocError">
                <input type="text" class="form-control" name="formtipodoc[alphaNum][codTipoDoc]" value="{if isset($formDBTipoDoc.cod_tipo_documento)}{$formDBTipoDoc.cod_tipo_documento}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="desTipoDoc">
                <label for="codTipoDoc"><i class="md md-border-color"></i>Codigo</label>
            </div>
            <div class="form-group floating-label col-lg-6" id="descTipoDocError">
                <input type="text" class="form-control" name="formtipodoc[txt][descTipoDoc]" value="{if isset($formDBTipoDoc.ind_descripcion)}{$formDBTipoDoc.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="desTipoDoc">
                <label for="desTipoDoc"><i class="md md-border-color"></i>Descripcion</label>
            </div>
            <div class="form-group floating-label col-lg-12" id="usuAlmacenError">
                <input type="text" class="form-control disabled" name="formAlm[alphaNum][usuAlmacen]" id="usuAlmacen" disabled value="{if isset($formDBTipoDoc.ind_usuario)}{$formDBTipoDoc.ind_usuario}{/if}">
                <label for="usuAlmacen"><i class="md md-border-color"></i>Ultimo Usuario</label>
            </div>
            <div class="form-group floating-label col-lg-12" id="fecAlmacenError">
                <input type="text" class="form-control disabled" name="formAlm[alphaNum][fecAlmacen]" id="fecAlmacen" disabled value="{if isset($formDBTipoDoc.fec_ultima_modificacion)}{$formDBTipoDoc.fec_ultima_modificacion}{/if}">
                <label for="fecAlmacen"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idTipoDoc)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $('#modalAncho').css("width", "60%");
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function () {
            return false;
        });
        $('#modalAncho').css("width", "35%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Tipo de Docuemnto fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Tipo de Docuemnto fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

    });
</script>