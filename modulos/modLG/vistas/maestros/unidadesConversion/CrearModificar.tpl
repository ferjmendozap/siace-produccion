<form action="{$_Parametros.url}modLG/maestros/unidadesConversionCONTROL/crearModificarUnidadesCMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idUnidades)}{$idUnidades}{/if}" name="idUnidades"/>
    <div class="modal-body">
        <div class="row">
            <div-- class="col-lg-12">
                <div class="form-group floating-label col-lg-5" id="cantUnidadesError">
                    <input type="text" class="form-control" name="formU[int][cantUnidades]" value="{if isset($formBDU.num_cantidad)}{$formBDU.num_cantidad}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="cantUnidades">
                    <label for="cantUnidades"><i class="md md-border-color"></i>Cantidad</label>
                </div>
                <div class="form-group floating-label col-lg-5" id="unidadError">
                    <label for="unidad"><i class="md md-border-color"></i>Unidad</label>
                    <select class="form-control select2-list select2" id="tipoMedi" name="formU[int][unidad]" {if isset($ver) and $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=u from=$unidades}
                            {if isset($u.pk_num_unidad) and $u.pk_num_unidad==$formBDU.fk_lgb004_num_unidad}
                                <option value="{$u.pk_num_unidad}" selected>{$u.ind_descripcion}</option>
                            {else}
                                <option value="{$u.pk_num_unidad}">{$u.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
                <div class="form-group floating-label col-lg-2">
                    <label class="checkbox-styled">
                        <span>Estatus</span>
                        <input type="checkbox" name="formU[txt][estadoUnidadC]" value="1" {if isset($ver) and $ver==1} disabled {/if} {if isset($formBDU.num_estatus)} {if $formBDU.num_estatus==1}checked{/if}{else}checked{/if}>
                    </label>
                </div>
                <div class="form-group floating-label col-lg-6" id="usuUnidadesError">
                    <input type="text" class="form-control disabled" disabled value="{if isset($formBDU.ind_usuario)}{$formBDU.ind_usuario}{/if}">
                    <label for="usuUnidades"><i class="md md-border-color"></i>Ultimo Usuario</label>
                </div>
                <div class="form-group floating-label col-lg-6" id="fecUnidadesError">
                    <input type="text" class="form-control disabled" disabled value="{if isset($formBDU.fec_ultima_modificacion)}{$formBDU.fec_ultima_modificacion}{/if}">
                    <label for="fecUnidades"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                </div>
        </div>
        <!--end .col -->
    </div>
    <!--end .row -->
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idUnidades)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "60%");
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').hul($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').hul($dato);
            });
        });
        $('#modalAncho').css("width", "60%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Tipo de Transaccion fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Tipo de Transaccion fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>