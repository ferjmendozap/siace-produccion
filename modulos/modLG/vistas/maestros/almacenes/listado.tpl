<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de los Almacenes</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Almacén</th>
                                <th>Descripción</th>
                                <th>Dirección</th>
                                <th>Commoditie</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="6">
                                    {if in_array('LG-01-07-03-01-01-N',$_Parametros.perfil)}
                                        <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                                descipcion="el Usuario ha creado el almacen Nro. " data-toggle="modal"
                                                data-target="#formModal" titulo="Registrar Nuevo Almacén" id="nuevo"><i class="md md-create"></i> Nuevo
                                            Almacén
                                    </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modLG/maestros/almacenesCONTROL/crearModificarAlmacenMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/maestros/almacenesCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_almacen" },
                    { "data": "ind_descripcion" },
                    { "data": "ind_direccion"},
                    { "orderable": false,"data": "num_flag_commodity"},
                    { "orderable": false,"data": "num_estatus"},
                    { "orderable": false,"data": "acciones"}
                ]
        );
        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAlmacen: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAlmacen: $(this).attr('idAlmacen')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAlmacen: $(this).attr('idAlmacen'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idAlmacen = $(this).attr('idAlmacen');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/maestros/almacenesCONTROL/eliminarAlmacenMET';
                $.post(url, { idAlmacen: idAlmacen },function(dato){
                    if(dato['status']=='ok'){
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        swal("Eliminado!", "el Almacén fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>