<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Empleados</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nro Empleado</th>
                            <th>Nro Documento</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Dependencia</th>
                        </tr>
                        </thead>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson2',
            '{$_Parametros.url}modLG/maestros/almacenesCONTROL/jsonDataTablaPersonaMET',
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "pk_num_persona" },
                { "data": "pk_num_empleado" },
                { "data": "ind_cedula_documento" },
                { "data": "ind_nombre1" },
                { "data": "ind_apellido1" },
                { "data": "ind_dependencia" }
            ]
        );

        $('#dataTablaJson2').on('click', 'tbody tr', function () {

            var celdas = $(this).find('td');
            var c = 0;
            var idPersona = 0;
            var idEmpleado = 0;
            var cedula = '';
            var nombre = '';
            celdas.each(function(){
                c = c+1;
                if(c==1){ idPersona = $(this).html(); }
                if(c==2){ idEmpleado = $(this).html(); }
                if(c==3){ cedula = $(this).html(); }
                if(c==4){ nombre = $(this).html(); }
                if(c==5){ nombre +=' '+$(this).html(); }
            });

            {if $persona == 'persona'}
            $('#idPersona').val(idPersona);
            $('#idEmpleado').val(idEmpleado);
            $('#personaResp').val(nombre);
            $('#cedula').val(cedula);
            $('#cerrarModal2').click();
            {elseif $persona == 'persona1'}
            if($('#idEmpleado2').val()==idEmpleado || $('#idEmpleado3').val()==idEmpleado || $('#idEmpleado4').val()==idEmpleado || $('#idEmpleado5').val()==idEmpleado){
                swal("Error!", 'No puede seleccionar el mismo empleado para diferentes firmantes', "error");
            } else {
                $('#idEmpleado').val(idEmpleado);
                $('#personaResp').val(nombre);
                $('#cedula').val(cedula);
                $('#cerrarModal2').click();
            }
            {elseif ($persona == 'persona2')}
            if($('#idEmpleado').val()==idEmpleado || $('#idEmpleado3').val()==idEmpleado || $('#idEmpleado4').val()==idEmpleado || $('#idEmpleado5').val()==idEmpleado){
                swal("Error!", 'No puede seleccionar el mismo empleado para diferentes firmantes', "error");
            } else {
                $('#idEmpleado2').val(idEmpleado);
                $('#personaResp2').val(nombre);
                $('#cedula2').val(cedula);
                $('#cerrarModal2').click();
            }
            {elseif ($persona == 'persona3')}
            if($('#idEmpleado2').val()==idEmpleado || $('#idEmpleado').val()==idEmpleado || $('#idEmpleado4').val()==idEmpleado || $('#idEmpleado5').val()==idEmpleado){
                swal("Error!", 'No puede seleccionar el mismo empleado para diferentes firmantes', "error");
            } else {
                $('#idEmpleado3').val(idEmpleado);
                $('#personaResp3').val(nombre);
                $('#cedula3').val(cedula);
                $('#cerrarModal2').click();
            }
            {elseif ($persona == 'persona4')}
            if($('#idEmpleado2').val()==idEmpleado || $('#idEmpleado').val()==idEmpleado || $('#idEmpleado3').val()==idEmpleado || $('#idEmpleado5').val()==idEmpleado){
                swal("Error!", 'No puede seleccionar el mismo empleado para diferentes firmantes', "error");
            } else {
                $('#idEmpleado4').val(idEmpleado);
                $('#personaResp4').val(nombre);
                $('#cedula4').val(cedula);
                $('#cerrarModal2').click();
            }
            {elseif ($persona == 'persona9')}
            if($('#idEmpleado2').val()==idEmpleado || $('#idEmpleado').val()==idEmpleado || $('#idEmpleado3').val()==idEmpleado || $('#idEmpleado4').val()==idEmpleado){
                swal("Error!", 'No puede seleccionar el mismo empleado para diferentes firmantes', "error");
            } else {
                $('#idEmpleado5').val(idEmpleado);
                $('#personaResp5').val(nombre);
                $('#cedula5').val(cedula);
                $('#cerrarModal2').click();
            }
            {elseif ($persona == 'persona5')}
            $('#idPersona5').val(idPersona);
            $('#idEmpleado5').val(idEmpleado);
            $('#personaResp5').val(nombre);
            $('#cedula5').val(cedula);
            $('#cerrarModal2').click();
            {elseif ($persona == 'persona6')}
            $('#idPersona6').val(idPersona);
            $('#idEmpleado6').val(idEmpleado);
            $('#personaResp6').val(nombre);
            $('#cedula6').val(cedula);
            $('#cerrarModal2').click();
            {elseif ($persona == 'persona7')}
            $('#idPersona7').val(idPersona);
            $('#idEmpleado7').val(idEmpleado);
            $('#personaResp7').val(nombre);
            $('#cedula7').val(cedula);
            $('#cerrarModal2').click();
            {elseif ($persona == 'persona8')}
            $('#idPersona8').val(idPersona);
            $('#idEmpleado8').val(idEmpleado);
            $('#personaResp8').val(nombre);
            $('#cedula8').val(cedula);
            $('#cerrarModal2').click();
            {/if}
        });
    });
</script>