<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{$estado} Ordenes de Compra / Servicio</h2>
        <input type="hidden" id="estadoSimbolo" value="{$estadoSimbolo}">
    </div>
    <div class="card">
        <div class="card-body">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkDependencia">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="dependencia"
                               class="control-label"> Dependencia: </label>
                    </div>
                    <div class="col-sm-9">
                        <select id="dependenciaL" class="form-control select2-list select3" disabled>
                            <option value="0">Seleccione...</option>
                            {foreach item=dep from=$dependencia}
                                {if $formDBR.fk_a004_num_dependencia==$dep.pk_num_dependencia}
                                    <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                {elseif $predeterminado.fk_a004_num_dependencia==$dep.pk_num_dependencia AND !isset($formDBR.fk_a004_num_dependencia)}
                                    <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkClasificacion">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="clasificacionL1"
                               class="control-label" style="margin-top: 10px;"> Tipo de Orden: </label>
                    </div>
                    <div class="col-sm-6">
                        <select id="clasificacionL1" class="form-control select2-list select3" disabled>
                            <option value="0">Seleccione...</option>
                                <option value="OC">COMPRA</option>
                                <option value="OS">SERVICIO</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkProveedor" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <label for="proveedor"
                               class="control-label">Proveedor:</label>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group" id="idProveedorError">
                            <input type="hidden" name="idProveedor" id="fk_nmb005_num_interfaz_cxp" class="form-control">
                            <input type="text" disabled id="nombreProveedor" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group floating-label">
                            <button class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                    type="button" disabled
                                    data-toggle="modal" data-target="#formModal2"
                                    id="bProveedor"
                                    titulo="Buscar Proveedor"
                                    url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/proveedorMET/proveedor2/">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkEstado" checked {if $estadoSimbolo!=''} disabled{/if}>
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="estado"
                               class="control-label" style="margin-top: 10px;"> Estado:</label>
                    </div>
                    <div class="col-sm-6">
                        <select id="estadoFiltro" class="form-control select2-list select3" disabled>
                            <option value="0">Seleccione...</option>
                            {foreach item=e from=$listadoEstado}
                                {if $estadoSimbolo!=''}
                                    {if $estadoSimbolo==$e.cod_detalle}
                                        <option value="{$e.cod_detalle}" selected>{$e.ind_nombre_detalle}</option>
                                    {/if}
                                {else}
                                    <option value="{$e.cod_detalle}">{$e.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkBusqueda" >
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label for="busqueda"
                               class="control-label"> Buscar:</label>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control text-center"
                               id="busqueda"
                               disabled>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkFechaDoc" checked="checked">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> F.Preparación: </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="desde"
                               style="text-align: center"
                               value="{date('Y-m')}-01"
                               placeholder="Desde"
                               disabled="disabled"
                               readonly>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="hasta"
                               style="text-align: center"
                               value="{date('Y-m-d')}"
                               placeholder="Hasta"
                               disabled="disabled"
                               readonly>
                    </div>
                </div>
            </div>

            <div align="center">
                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary">
                    BUSCAR
                </button>
            </div>
        </div>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tipo Orden</th>
                            <th>Nro. Orden</th>
                            <th>Fecha Preparación</th>
                            <th>Proveedor</th>
                            <th>Monto</th>
                            <th>Monto Pagado</th>
                            <th>Monto Pendiente</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tfoot>

                        {if in_array('LG-01-01-12-01-01-N',$_Parametros.perfil) and $estado=='Listado de'}
                        <tr>
                            <td colspan="9">
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descripcion="el Usuario a creado la Orden Nro. "
                                        data-toggle="modal" data-target="#formModal"
                                        estado="Listado de"
                                        data-keyboard="false" data-backdrop="static"
                                        titulo="Registrar Nueva Orden" id="nuevo">
                                    <i class="md md-create"></i> Nueva Orden
                                </button>
                            </td>
                        </tr>
                        {/if}
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select3').select2({ allowClear: true });
        $('#desde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#hasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#bProveedor').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        if($('#checkEstado').attr('checked')=='checked' && $('#estadoSimbolo').val()==''){
            $('#estadoFiltro').attr('disabled', false);
        }else if($('#estadoSimbolo').val()==''){
            $('#estadoFiltro').attr('disabled', true);
            $('#estadoFiltro option[value=0]').attr("selected",true);
            $('#s2id_estadoFiltro .select2-chosen').html($('#estadoFiltro option:selected').text());
        }

        var app = new AppFunciones();
        var dependenciaL = 'false';
        var clasificacionL = 'false';
        var proveedorL = 'false';
        var estadoL = 'false';
        var buscarL = 'false';
        var desdeL = 'false';
        var hastaL = 'false';

        var url2 = "{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/jsonDataTablaMET/{$op}";
        if($('#checkDependencia').attr('checked')=='checked') { dependenciaL = $('#dependenciaL').val(); }
        if($('#checkClasificacion').attr('checked')=='checked') { clasificacionL = $('#clasificacionL1').val(); }
        if($('#checkProveedor').attr('checked')=='checked') { proveedorL = $('#fk_nmb005_num_interfaz_cxp').val(); }
        if($('#checkEstado').attr('checked')=='checked') { estadoL = $('#estadoFiltro').val(); }
        if($('#checkBusqueda').attr('checked')=='checked' && $('#busqueda').val().length>0) { buscarL = $('#busqueda').val(); }

        if($('#checkFechaDoc').attr('checked')=='checked'){
            desdeL= $('#desde').val();
            hastaL= $('#hasta').val();
        }

        var dt = app.dataTable(
                '#dataTablaJson',
            url2
            +'/'+dependenciaL
            +'/'+clasificacionL
            +'/'+proveedorL
            +'/'+buscarL
            +'/'+desdeL
            +'/'+hastaL
            +'/'+estadoL,
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_orden"},
                    { "data": "ind_tipo_orden_nombre" },
                    { "data": "ind_orden" },
                    { "data": "fec_creacion"},
                    { "data": "proveedor"},
                    { "data": "num_monto_total"},
                    { "data": "num_monto_pagado"},
                    { "data": "num_monto_pendiente"},
                    { "data": "ind_estado_nombre"},
                    { "orderable": false,"data": "acciones", width:150 }
                ]
        );
        
        var url='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/crearModificarOrdenCompraMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idOrden: 0, estado: $(this).attr('estado'), ver:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { ver: $(this).attr('ver'), idOrden: $(this).attr('idOrden'), clasificacion: $(this).attr('clasificacion'), idAdj: $(this).attr('idAdj'),  estado: $(this).attr('estado') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { ver: 1, idOrden: $(this).attr('idOrden'), estado: $(this).attr('estado') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.imprimir', function () {
            window.open('{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/imprimirMET/'+$(this).attr('idOrden'));
        });

        $('#dataTablaJson tbody').on( 'click', '.anular', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post('{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/anularMET', { idOrden: $(this).attr('idOrden'), estado: $(this).attr('estado') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });


        $('#checkClasificacion').on('change', function () {
            if($(this).attr('checked')=='checked') {
                $('#clasificacionL1').attr('disabled',false);
            } else {
                $('#clasificacionL1').attr('disabled',true);
                $('#clasificacionL1 option[value=0]').attr("selected",true);
                $('#s2id_clasificacionL1 .select2-chosen').html($('#clasificacionL1 option:selected').text());
            }
        });

        $('#checkDependencia').on('change', function () {
            if($(this).attr('checked')=='checked') {
                $('#dependenciaL').attr('disabled',false);
            } else {
                $('#dependenciaL').attr('disabled',true);
                $('#dependenciaL option[value={$predeterminado.fk_a004_num_dependencia}]').attr("selected",true);
                $('#s2id_dependenciaL .select2-chosen').html($('#dependenciaL option:selected').text());
            }
        });

        $('#checkProveedor').change(function () {
            if($('#checkProveedor').attr('checked')=="checked" ){
                $('#bProveedor').attr('disabled',false);
            } else {
                $('#bProveedor').attr('disabled','disabled');
                $('#fk_nmb005_num_interfaz_cxp').val('');
                $('#persona').val('');
            }
        });

        $('#checkEstado').click(function () {
            if($(this).attr('checked')=='checked' && $('#estadoSimbolo').val()==''){
                $('#estadoFiltro').attr('disabled', false);
            }else{
                $('#estadoFiltro').attr('disabled', true);
                $('#estadoFiltro option[value=0]').attr("selected",true);
                $('#s2id_estadoFiltro .select2-chosen').html($('#estadoFiltro option:selected').text());
            }
        });
        
        $('#checkBusqueda').click(function () {
            if($(this).attr('checked')=='checked'){
                $('#busqueda').attr('disabled', false);
            }else{
                $('#busqueda').attr('disabled', true);
                $('#busqueda').val("");
            }
        });

        function verificarFechas(cual) {
            if(cual=='checked'){
                $('#desde').attr('disabled', false);
                $('#hasta').attr('disabled', false);
            }else{
                $('#desde').attr('disabled', true);
                $('#hasta').attr('disabled', true);

                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                var yyyy = hoy.getFullYear();

                if(dd<10) { dd='0'+dd; }
                if(mm<10) { mm='0'+mm; }

                var fdesde = yyyy+'-'+mm+'-01';
                var fhasta = yyyy+'-'+mm+'-'+dd;
                $('#desde').val(fdesde);
                $('#hasta').val(fhasta);

            }
        }
        $('#checkFechaDoc').click(function () {
            verificarFechas($('#checkFechaDoc').attr('checked'));
        });

        verificarFechas($('#checkFechaDoc').attr('checked'));

        $('.buscar').click(function () {
            var dependenciaL = 'false';
            var clasificacionL = 'false';
            var proveedorL = 'false';
            var estadoL = 'false';
            var buscarL = 'false';
            var desdeL = 'false';
            var hastaL = 'false';

            var url = "{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/jsonDataTablaMET/{$op}";
            if($('#checkDependencia').attr('checked')=='checked') { dependenciaL = $('#dependenciaL').val(); }
            if($('#checkClasificacion').attr('checked')=='checked') { clasificacionL = $('#clasificacionL1').val(); }
            if($('#checkProveedor').attr('checked')=='checked') { proveedorL = $('#fk_nmb005_num_interfaz_cxp').val(); }
            if($('#checkEstado').attr('checked')=='checked') { estadoL = $('#estadoFiltro').val(); }
            if($('#checkBusqueda').attr('checked')=='checked' && $('#busqueda').val().length>0) { buscarL = $('#busqueda').val(); }

            if($('#checkFechaDoc').attr('checked')=='checked'){
                desdeL= $('#desde').val();
                hastaL= $('#hasta').val();
            }

            $('#dataTablaJson').DataTable().ajax.url(url
                +'/'+dependenciaL
                +'/'+clasificacionL
                +'/'+proveedorL
                +'/'+buscarL
                +'/'+desdeL
                +'/'+hastaL
                +'/'+estadoL
            ).load();
        });
    });
</script>