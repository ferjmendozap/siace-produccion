<form action="{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/cotizarMET" id="formAjax" method="post" class="formAjax form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" name="datos_ic" value="{$idActaDet}" id="idActaDet"/>
    <input type="hidden" name="form[txt][estadoActa]" value="{$acta.estadoActa}"/>
    <input type="hidden" id="formaPago" value="{$formaPago}"/>
    <input type="hidden" id="fecha" value="{date('Y-m-d')}"/>
    <div class="modal-body">
        <div class="row">
            <div class="panel-group floating-label" id="accordion1">
                <div class="card panel">
                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion1" data-target="#accordion1-1">
                        <header>Información General</header>
                        <div class="tools">
                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                    <div id="accordion1-1" class="collapse">
                        <div class="card-body">
                            <div class="form-group floating-label col-lg-3" id="codigoError">
                                <label for="codigo"><i class="md md-border-color"></i>Codigo</label>
                                <input disabled size="18" id="codigo" class="form-control" value="{if $acta.fk_lgb002_num_item!=null}{$acta.ind_codigo_interno}{else}{$acta.ind_cod_commodity}{/if}">
                            </div>
                            <div class="form-group floating-label col-lg-3" id="unidadError">
                                <label for="unidad"><i class="md md-border-color"></i>Unidad</label>
                                <input disabled size="18" id="unidad" class="form-control" value="{$acta.unidad}">
                            </div>
                            <div class="form-group floating-label col-lg-3" id="cantidadError">
                                <label for="cantidad"><i class="md md-border-color"></i>Cantidad</label>
                                <input readonly name="form[int][cPedida]" size="18" id="cant" class="form-control" value="{$acta.num_cantidad_pedida}">
                            </div>
                            <div class="form-group floating-label col-lg-3" id="ccontError">
                                <label for="ccont"><i class="md md-border-color"></i>Cuenta Contable</label>
                                <input disabled size="18" id="ccont" class="form-control" value="{$acta.fk_cbb004_num_plan_cuenta}">
                            </div>
                            <div class="form-group floating-label col-lg-12" id="descripcionError">
                                <label for="desc"><i class="md md-border-color"></i>Descripción</label>
                                <textarea id="descripcion" class="form-control" cols="50" rows="2" disabled>{$acta.ind_descripcion}</textarea>
                            </div>
                        </div><!--END card-body-->
                    </div><!--END accordion1-1-->
                </div><!--END card-panel-->
            </div><!--END panel accordion1-->
            <div class="panel-group floating-label" id="accordion2">
                <div class="card panel">
                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                        <header>Cotizaciones</header>
                        <div class="tools">
                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                    <div id="accordion2-1" class="collapse">
                        <div class="card-body">
                            <table class="table table-striped table-bordered"
                                   style="white-space:nowrap;overflow-x:scroll;
                                   overflow-y:scroll;width:100%;height:250px;display: inline-block;" id="tabla">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Proveedor<i style="color: #ffffff">________________________________________________</i></th>
                                    <th>Asignado</th>
                                    <th>Cantidad Compra<i style="color: #ffffff">___</i></th>
                                    <th>Precio Unitario<i style="color: #ffffff">___</i></th>
                                    <th>Exonerado IVA</th>
                                    <th>Precio con IVA<i style="color: #ffffff">____</i></th>
                                    <th>Descuento %<i style="color: #ffffff">_______</i></th>
                                    <th>Descuento Fijo<i style="color: #ffffff">____</i></th>
                                    <th>Total<i style="color: #ffffff">_____________</i></th>
                                    <th>Sugerido</th>
                                    <th>Validez Oferta<i style="color: #ffffff">____</i></th>
                                    <th>Días entrega<i style="color: #ffffff">______</i></th>
                                    <th>Fecha Invitación</th>
                                    <th>Fecha Entrega</th>
                                    <th>Fecha Recepción</th>
                                    <th>Unidad Recepción</th>
                                    <th>Unidad Compra<i style="color: #ffffff">______</i></th>
                                    <th>Forma Pago<i style="color: #ffffff">_________</i></th>
                                    <th>Nro Cotización</th>
                                    <th>Fecha Cotización</th>
                                    <th>Observación<i style="color: #ffffff">________________________________________________</i></th>
                                    <th>Recomendación<i style="color: #ffffff">______________________________________________</i></th>
                                    <th>Razón Asignación<i style="color: #ffffff">___________________________________________</i></th>
                                </tr>
                                </thead>
                                <tbody id="coti">
                                {$c=0}
                                {foreach item=i from=$invitaciones}
                                    {$c=$c+1}
                                    {if $i.pk_num_cotizacion!=null}
                                        {$idCotizacion = $i.pk_num_cotizacion}
                                        {$pUni = $i.num_precio_unitario}
                                        {$pIva = $i.num_precio_unitario_iva}
                                        {$dfijo = $i.num_descuento_fijo}
                                        {$dpor = $i.num_descuento_porcentaje}
                                        {$total = $i.num_total}
                                        {$fechaCotizacion = $i.fec_cotizacion_proveedor}
                                        {$idUnidad = $i.fk_lgb004_num_unidad}
                                        {$idFormaPago = $i.fk_a006_num_miscelaneo_detalle_forma_pago}
                                        {$numCotizacion = $i.ind_num_cotizacion}
                                        {$validez = $i.num_validez_oferta}
                                        {$dias = $i.num_dias_entrega}
                                        {$fechaEntrega = $i.fec_entrega}
                                        {$fechaRecepcion = $i.fec_recepcion}
                                        {$cantidadPedida = $i.num_cantidad}
                                    {else}
                                        {$idCotizacion = 0}
                                        {$pUni = 0}
                                        {$pIva = 0}
                                        {$dfijo = '0.00'}
                                        {$dpor = 0}
                                        {$total = 0}
                                        {$fechaCotizacion = ''}
                                        {$idUnidad = ''}
                                        {$numCotizacion = ''}
                                        {$validez = 0}
                                        {$dias = 0}
                                        {$fechaEntrega = date('Y-m-d')}
                                        {$fechaRecepcion = date('Y-m-d')}
                                        {$idFormaPago = 0}
                                        {$cantidadPedida = $acta.num_cantidad_pedida}
                                    {/if}

                                    {$observacion = ''}
                                    {$recomendacion = ''}
                                    {$razon = ''}
                                    {$exo = ''}
                                    {$sugerido = ''}
                                    {$asignado = ''}

                                    {if $i.ind_observacion!=null}
                                        {$observacion = $i.ind_observacion}
                                    {/if}
                                    {if $i.ind_recomendacion!=null}
                                        {$recomendacion = $i.ind_recomendacion}
                                    {/if}
                                    {if $i.ind_razon_elegido!=null}
                                        {$razon = $i.ind_razon_elegido}
                                    {/if}
                                    {if $i.num_flag_exonerado==1}
                                        {$exo = 'checked'}
                                    {/if}
                                    {if $i.num_flag_elegido_sistema==1}
                                        {$sugerido = 'checked'}
                                    {/if}
                                    {if $i.num_flag_asignado==1}
                                        {$asignado = 'checked'}
                                    {/if}

                                        <tr>
                                        <td style="vertical-align: middle;">
                                            <input type="hidden" name="form[int][idCotizacion{$c}]" value="{$idCotizacion}">
                                            <input type="hidden" name="form[int][idActaDet{$c}]" value="{$i.pk_num_acta_detalle}">
                                            <input type="hidden" name="form[int][invi{$c}]" value="{$i.pk_num_invitacion}">
                                            <input readonly type="hidden" class="form-control" name="form[int][cantidad{$c}]" id="totalcantidad" value="{$i.num_cantidad_pedida}">
                                            {$c}
                                            <td width="1000px"> <input width="100%" readonly type="text" class="form-control" value="{$i.nombre}"></td>
                                        <td style="vertical-align: top;">
                                            <div align="center" class="checkbox checkbox-styled">
                                                <label>
                                                    <input type="checkbox" class="asignado" {$asignado} name="form[int][asignado{$c}]" i="{$c}" id="asignado{$c}" value="1"><span></span>
                                                    </label>
                                                </div>
                                            </td>
                                        <td><input type="text" class="form-control" onblur="calcular()" onclick="validarCampo({$c},4)" id="{$c}cantidadPedida" name="form[int][cantidadPedida{$c}]" value="{$cantidadPedida}"></td>
                                        <td><input type="text" class="form-control" onblur="calcular()" onclick="validarCampo({$c},1)" id="{$c}pUni" name="form[int][pUni{$c}]" value="{$pUni}"></td>
                                        <td style="vertical-align: top;">
                                            <div align="center" class="checkbox checkbox-styled">
                                                <label>
                                                    <input type="checkbox" class="exo" {$exo} onclick="calcular()" name="form[int][exo{$c}]" id="{$c}exo" value="1"><span></span>
                                                    </label>
                                                </div>
                                            </td>
                                        <td><input readonly type="text" class="form-control" name="form[int][pIVA{$c}]" id="{$c}pIVA" value="{$pIva}"></td>
                                        <td><input type="text" class="form-control dpor" onblur="calcular()" onclick="validarCampo({$c},2)" name="form[int][dpor{$c}]" id="{$c}dpor" value="{$dpor}"></td>
                                        <td><input type="text" class="form-control dfijo" onblur="calcular()" onclick="validarCampo({$c},3)" name="form[int][dfijo{$c}]" id="{$c}dfijo" value="{$dfijo}"></td>
                                        <td><input readonly type="text" class="form-control total" name="form[int][total{$c}]" id="{$c}total" sec="{$c}" value="{$total}" i="{$c}"></td>
                                        <td style="vertical-align: top;">
                                            <div align="center" class="checkbox checkbox-styled">
                                                <label>
                                                    <input type="checkbox" class="sugerido" {$sugerido} name="form[int][sugerido{$c}]" id="sugerido{$c}" i="{$c}" value="1"><span></span>
                                                    </label>
                                                </div>
                                            </td>
                                        <td><input type="text" class="form-control" name="form[int][vofer{$c}]" value="{$validez}"></td>
                                        <td><input type="text" class="form-control" name="form[int][dias{$c}]" value="{$dias}"></td>
                                        <td><input readonly type="text" class="form-control" name="form[txt][finvi{$c}]" id="finvi" value="{$i.fec_invitacion}"></td>
                                        <td><input  type="text" class="form-control" id="fentrega{$c}" name="form[txt][fentrega{$c}]" id="fentrega" value="{$fechaEntrega}"></td>
                                        <td><input  type="text" class="form-control" id="frecepcion{$c}" name="form[txt][frecepcion{$c}]" id="frecepcion" value="{$fechaRecepcion}"></td>
                                        <td style="vertical-align:middle;">{$acta.unidad}</td>
                                        <td style="vertical-align:middle;">
                                            <select class="form-control select2 select2-list" name="form[int][uniCompra{$c}]">

                                                {foreach item=uni from=$unidades}
                                                    {if $idUnidad == $uni.pk_num_unidad}
                                                        <option value="{$uni.pk_num_unidad}" selected>{$uni.ind_descripcion}</option>
                                                    {else}
                                                        <option value="{$uni.pk_num_unidad}">{$uni.ind_descripcion}</option>
                                                    {/if}
                                                {/foreach}
                                                </select>
                                            </td>
                                        <td style="vertical-align:middle;">
                                            <select class="form-control select2 select2-list" name="form[int][formaPago{$c}]">

                                                {foreach item=fp from=$formaPago}
                                                    {if $idFormaPago == $fp.pk_num_miscelaneo_detalle}
                                                        <option value="{$fp.pk_num_miscelaneo_detalle}" selected>{$fp.ind_nombre_detalle}</option>
                                                    {else}
                                                        <option value="{$fp.pk_num_miscelaneo_detalle}">{$fp.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                                </select>
                                            </td>
                                        <td><input type="text" class="form-control" readonly value="{$numCotizacion}"></td>
                                        <td><input type="text" class="form-control" readonly value="{$fechaCotizacion}"></td>
                                        <td><input type="text" class="form-control" name="form[alphaNum][observacion{$c}]" value="{$observacion}"></td>
                                        <td><input type="text" class="form-control" name="form[alphaNum][recomendacion{$c}]" value="{$recomendacion}"></td>
                                        <td><input type="text" class="form-control" name="form[alphaNum][razon{$c}]" id="razon{$c}" value="{$razon}"></td>
                                        </tr>
                                {/foreach}

                                {if $c==0}
                                    <tr>
                                        <td colspan="8" style="text-align: center; ">
                                            No se ha(n) realizado invitacion(es) para este requerimiento
                                        </td>
                                    </tr>
                                {/if}
                                </tbody>
                                <input type="hidden" name="form[int][secuencia]" value="{$c}">
                            </table>
                        </div><!--END card-body-->

                    </div><!--END accordion2-1-->

                </div><!--END card-panel-->
            </div><!--END panel accordion2->
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-warning ink-reaction btn-raised" id="presupuesto"
                data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static">
            <span class="md md-attach-money"></span>  Disp. Presupuestaria
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="glyphicon glyphicon-floppy-disk"></span>  Guardar
            </button>
        {/if}
        <button type="button" style="display: none" id="observacion" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"></button>
    </div>
</form>
<script type="text/javascript">
    //funciones
    function buscarSugerido() {
        var menor = 9999999999999999999999999999999999999999;
        $('.total').each(function () {
            var valor = $(this).val();
            var i = $(this).attr('i');
            if(parseFloat(valor)<parseFloat(menor)){
                menor = valor;
                $('.sugerido').attr('checked',false);
                $('#sugerido'+i).attr('checked',true);
                $('.asignado').attr('checked',false);
                $('#asignado'+i).attr('checked',true);
            }
        });
    }

    function calcular() {
        $('.total').each(function( titulo, valor ){
            var pIVA = 0;


            var descuento = 0;
            var id=$(this).attr('sec');
            var cantidadPedida = $('#'+id + 'cantidadPedida').val();
            var cantidadSolicitada = $('#cant').val();
            var dpor = $('#'+id + 'dpor').val();
            var dfijo = $('#'+id + 'dfijo').val();
            var precio = $('#'+id + 'pUni').val();

            if($('#'+ id + 'exo').is(':checked')){
                pIVA = precio;
            }else{
                pIVA = parseFloat(precio) + parseFloat(precio*0.12);
            }

            if (dpor!='0' && dfijo!='0.00') {
                dpor='0';
                dfijo='0.00';
            } else if (dpor!='0' && dpor < 100) {
                descuento = parseFloat(precio/100)*dpor;
            } else if (dfijo!='0.00' && dfijo < precio) {
                descuento = dfijo;
            } else {
                dpor='0';
                dfijo='0.00';
            }
            var subtotal = pIVA - descuento;
            if(parseFloat(cantidadPedida)>parseFloat(cantidadSolicitada)){
                cantidadPedida = cantidadSolicitada;
                $('#'+id + 'cantidadPedida').val(cantidadPedida);
            }
            var total = (cantidadPedida * subtotal).toFixed(2);

            $('#'+id + 'pIVA').val(pIVA);
            $('#'+id + 'dpor').val(dpor);
            $('#'+id + 'dfijo').val(dfijo);
            $('#'+id + 'total').val(total);

        });
        buscarSugerido();
    }

    function validarCampo(nombre,cual) {

        if(cual==1){
            var id = '#'+nombre+'pUni';
        } else if(cual==2){
            id = '#'+nombre+'dpor';
            cual = 1;
        } else if(cual==3){
            id = '#'+nombre+'dfijo';
            cual = 2;
        } else if(cual==4){
            id = '#'+nombre+'cantidadPedida';
            cual = 2;
        }

        if ($(id).val()=='' && cual==1){
            var cantidad = 0;
        } else if ($(id).val()=='' && cual==2){
            cantidad = '0.00';
        } else if($(id).val()!='') {
            cantidad = '';
        }
        $(id).val(cantidad);
    }

    $('#modalAncho').css("width", "80%");

    $("#formAjax").submit(function(){
        return false;
    });

    $(document).ready(function () {

        var app = new  AppFunciones();

        $('#accion').click(function(){
            var post = 'si';
            var id = '';
            $('#coti .asignado').each(function () {
                var checked1 = $(this).attr('checked');
                var i = $(this).attr('i');
                var checked2 = $('#sugerido'+i).attr('checked');
                if(checked1!=checked2){
                    post = 'no';
                    id = 'razon'+i;
                }
            });

            if(post=='no' && id!='' && $('#'+id).val()==''){
                $('#observacion').click();
                $('#formModalLabel2').html('Observaciones');
                var url = '{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/observacionMET';
                $.post(url,{ id:id },function(dato){
                    $('#ContenidoModal2').html(dato);
                });
            } else {
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($("#formAjax").attr("action"), $( ".formAjax" ).serialize(),function(dato) {
                    if(dato['status']=='error') {
                        app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                    }else if(dato['status']=='error2'){
                        swal('Error','Disculpa. no se puede cotizar','error');
                    }else if(dato['status']=='errorEstado'){
                        app.metValidarError(dato,'Disculpa. No se puede Cotizar para un procedimiento Completado o Anulado');
                    }else if(dato['status']=='errorSQL'){
                        app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    }else if(dato['status']=='nuevo'){
                        swal("Registro Guardado!", 'La Cotización fue guardada satisfactoriamente.', "success");
                        $('#cerrarModal').click();
                        $('#ContenidoModal').html('');
                    }
                }, 'json');
            }
        });

        $('#presupuesto').click(function(){
            var id = $('#idActaDet').val();
            $('#modalAncho2').css("width", "80%");
            $('#formModalLabel2').html('Disponibilidad Presupuestaria');
            var url = '{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/disponibilidadMET';
            $.post(url,{ id:id },function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });

        //evita que se seleccionen los sugeridos
        $('#coti').on('click','.sugerido',function () {
            var checked = $(this).attr('checked');
            if(checked=='checked'){
                $(this).attr('checked',false);
            } else {
                $(this).attr('checked',true);
            }
        });

        //evita que se seleccionen varios asignados
        $('#coti').on('click','.asignado',function () {
            var checked = $(this).attr('checked');
            var id = $(this).attr('id');
            $('.asignado').attr('checked',false);
            $('#'+id).attr('checked',true);
        });
    });
</script>