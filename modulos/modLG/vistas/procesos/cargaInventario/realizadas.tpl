<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Lista de Cotejamientos Realizados</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nro. Proceso</th>
                            <th>Periodo</th>
                            <th>Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr class="realizados">
                                <input type="hidden"
                                       numProceso="{$i.num_proceso}"
                                       anio="{$i.fec_anio}">
                                <td>{$i.num_proceso}</td>
                                <td>{$i.fec_anio}</td>
                                <td>{$i.fec_ultima_modificacion}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('.realizados').click(function() {
            var input = $(this).find('input');

            window.open('{$_Parametros.url}modLG/procesos/cargaInventarioCONTROL/imprimirRealizadosMET/'+input.attr('anio')+'/'+input.attr('numProceso'));

            $('#cerrarModal2').click();
        });
    });
</script>