<section class="style-default-bright">
        {if $cual=='crear'}
            <div class="section-header">
                <h2 class="text-primary">Crear Control Perceptivo</h2>
            </div>
            <div class="section-body contain-lg">
                <div class="row">
                    <div class="col-lg-12 contain-lg">
                        <div class="table-responsive">
                            <table id="dataTablaJson" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>N° ORDEN</th>
                                    <th>PROVEEDOR</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        {else}
            <div class="section-header">
                <h2 class="text-primary">Listado de Controles Perceptivos</h2>
            </div>

            <div class="section-body contain-lg">
                <div class="row">
                    <div class="col-lg-12 contain-lg">
                        <div class="table-responsive">
                            <table id="dataTablaJson" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>N° CONTROL</th>
                                    <th>N° ORDEN</th>
                                    <th>PROVEEDOR</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        {if $cual=='crear'}
            var dt = app.dataTable(
                    '#dataTablaJson',
                    "{$_Parametros.url}modLG/procesos/controlPerceptivoCONTROL/jsonDataTablaMET/{$cual}",
                    "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                    [
                        { "data": "ind_orden" },
                        { "data": "proveedor"},
                        { "orderable": false,"data": "acciones"}
                    ]
            );
        {else}
            dt = app.dataTable(
                    '#dataTablaJson',
                    "{$_Parametros.url}modLG/procesos/controlPerceptivoCONTROL/jsonDataTablaMET/{$cual}",
                    "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                    [
                        { "data": "pk_num_control_perceptivo" },
                        { "data": "ind_orden" },
                        { "data": "proveedor"},
                        { "orderable": false,"data": "acciones"}
                    ]
            );
        {/if}

        var url = '{$_Parametros.url}modLG/procesos/controlPerceptivoCONTROL/crearModificarConPerMET';
        var url2 = '{$_Parametros.url}modLG/procesos/controlPerceptivoCONTROL/generarMET';

        $('#dataTablaJson tbody').on( 'click', '.hacer', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idConPer: $(this).attr('idConPer'),idOrden: $(this).attr('idOrden'), estado: $(this).attr('estado'), idAsisA: $(this).attr('idAsisA'), idAsisB: $(this).attr('idAsisB'), idAsisC: $(this).attr('idAsisC'), idAsisD: $(this).attr('idAsisD') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idConPer: $(this).attr('idConPer'),idOrden: $(this).attr('idOrden'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.generar', function () {
            $.post(url2, { idControl: $(this).attr('idControl')}, function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });
    });
</script>