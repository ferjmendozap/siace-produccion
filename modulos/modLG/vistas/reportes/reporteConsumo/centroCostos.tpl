<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Centros de Costo</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Código</th>
                            <th>Nombre</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$centroCosto}
                            <tr class="cc">
                                <input type="hidden"
                                       numCC="{$i.pk_num_centro_costo}"
                                       nombreCC="{$i.ind_descripcion_centro_costo}">
                                <td>{$i.pk_num_centro_costo}</td>
                                <td>{$i.cod_centro_costo}</td>
                                <td>{$i.ind_descripcion_centro_costo}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable3').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable3 tbody').on( 'click', '.cc', function () {
            var input = $(this).find('input');
            {if $cual!=2}
                $('#numCC').val(input.attr('numCC'));
                $('#nombreCC').val(input.attr('nombreCC'));
            {else}
            var i = $('#idCC').val();
                $('#numCC'+i).val(input.attr('numCC'));
                $('#nombreCC'+i).val(input.attr('nombreCC'));
            {/if}

            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>