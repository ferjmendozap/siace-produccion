<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Reporte de Movimientos de Almacén</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12">
                <!--div class="contain-lg col-lg-1" align="right">
                    <span>almacen:</span>
                </div-->
                <div class="col-lg-12 contain-lg">

                    <div class="col-lg-5 contain-lg">
                        <div class="col-lg-3 contain-lg">
                            <span>Buscar:</span>
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkBuscar" class="form-control">
                                <span></span>
                            </label>
                        </div>
                        <div class="col-lg-9 contain-lg">
                            <div class="form-group" id="buscarError">
                                <input type="text" disabled name="buscar" id="buscar" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5 contain-lg">
                        <div class="contain-lg col-lg-3" align="right">
                            <span>Almacén:</span>
                        </div>
                        <div class="col-lg-9 contain-lg">
                            <select id="almacen" class="form-control select2">
                                {foreach item=alm from=$almacen}
                                    <option value="{$alm.pk_num_almacen}">{$alm.ind_descripcion}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-2 contain-lg">
                        <div class="contain-lg col-lg-6">
                            <span>Stock:</span>
                        </div>
                        <div class="col-lg-6 contain-lg">
                            <select id="stock" class="form-control select2">
                                <option value="1">Todos</option>
                                <option value="2">Con Stock</option>
                                <option value="3">Sin Stock</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 contain-lg">
                    <div class="col-lg-6 contain-lg">
                        <div class="col-lg-2 contain-lg">
                            <span>Periodo:</span>
                        </div>
                        <div class="col-lg-5 contain-lg">
                            <div class="form-group" id="fdesdeError">
                                <input type="text" readonly name="fdesde" id="fdesde" value="{date('Y-m')}" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-5 contain-lg">
                            <div class="form-group" id="fhastaError">
                                <input type="text" readonly name="fhasta" id="fhasta" value="{date('Y-m')}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 contain-lg">

                        <div class="col-lg-3 contain-lg">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkI" class="form-control">
                                <span>Item:</span>
                            </label>
                        </div>

                        <div class="col-lg-8 contain-lg">
                            <div class="form-group" id="numCCError">
                                <input type="hidden" name="numItem" id="numItem" class="form-control">
                                <input type="text" disabled id="nombreItem" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-1 contain-lg">
                            <button id="item" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                    data-toggle="modal" data-target="#formModal2"
                                    data-keyboard="false" data-backdrop="static"
                                    titulo="Listado de Items"
                                    url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/item4/">
                                <i class="md md-search" style="color: #ffffff;"></i>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>



        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });
        $('.accionModal').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#accion').click(function () {
            var url2 = '{$_Parametros.url}modLG/reportes/reporteMovimientosCONTROL/generarReporteMET/';

            if($('#almacen').val().length>0){
                var almacen = $('#almacen').val();
            } else {
                almacen = 'no';
            }
            if($('#stock').val().length>0){
                var stock = $('#stock').val();
            } else {
                stock = 'no';
            }
            if($('#checkBuscar').attr('checked')=="checked" && $('#buscar').val().length>0){
                var buscar = $('#buscar').val();
            } else {
                buscar = 'no';
            }
            if($('#checkI').attr('checked')=="checked" && $('#numItem').val().length>0){
                var numItem = $('#numItem').val();
            } else {
                numItem = 'no';
            }

            var fdesde = $('#fdesde').val();
            var fhasta = $('#fhasta').val();
            if(fdesde>fhasta){
                fhasta = fdesde;
                $('#fhasta').val(fdesde);
            }

            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +stock+'/'+almacen+'/'+buscar+'/'+fdesde+'/'+fhasta+'/'+numItem+'/" width="100%" height="540px"></iframe>');
        });

        //buscar
        $('#checkBuscar').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#buscar').attr('disabled',false);
            } else {
                $('#buscar').attr('disabled','disabled');
                $('#buscar').val('');
            }
        });

        $('#fdesde').datepicker({ autoclose: true, minViewMode: 1, todayHighlight: true, format: "yyyy-mm", language:'es' });
        $('#fhasta').datepicker({ autoclose: true, minViewMode: 1, todayHighlight: true, format: "yyyy-mm", language:'es' });

        //item
        $('#checkI').change(function () {
            if($('#checkI').attr('checked')=="checked" ){
                $('#item').attr('disabled',false);
            } else {
                $('#item').attr('disabled','disabled');
                $('#numItem').val('');
                $('#nombreItem').val('');
            }
        });

    });
</script>