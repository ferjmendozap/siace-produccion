<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de los Periodos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Año</th>
                                <th>Mes</th>
                                <th>Transaccion</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="6">
                                    {if in_array('LG-01-04-01-01-N',$_Parametros.perfil)}
                                        <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                                descipcion="el Usuario ha creado el periodo Nro. " data-toggle="modal"
                                                data-target="#formModal" titulo="Registrar Nuevo Periodo" id="nuevo">
                                            <i class="md md-create"></i> Nuevo Periodo
                                    </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/admin/periodoCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_periodo" },
                    { "data": "fec_anio" },
                    { "data": "fec_mes"},
                    { "data": "num_flag_transaccion"},
                    { "data": "num_estatus"},
                    { "orderable": false,"data": "acciones"}
                ]
        );

        var url = '{$_Parametros.url}modLG/admin/periodoCONTROL/crearModificarPeriodoMET';
        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idPeriodo: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idPeriodo: $(this).attr('idPeriodo')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idPeriodo: $(this).attr('idPeriodo'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idPeriodo = $(this).attr('idPeriodo');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/admin/periodoCONTROL/eliminarPeriodoMET';
                $.post(url, { idPeriodo: idPeriodo },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idPeriodo'+dato['id'])).remove();
                        swal("Eliminado!", "el Periodo fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>