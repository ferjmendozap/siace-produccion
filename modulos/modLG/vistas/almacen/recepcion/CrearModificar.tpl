<form action="{$_Parametros.url}modLG/almacen/recepcionCONTROL/crearRecepcionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idOrden)}{$idOrden}{/if}" name="idOrden"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">ITEMS</span> </a></li>
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                        <div class="card panel">
                                            <div class="card-head style-primary">
                                                <header>Información General</header>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-lg-12">
                                                    <div class="form-group col-lg-6" id="cCostoError">
                                                        <select id="cCosto" name="form[int][cCosto]" class="form-control select2 select2-list">
                                                            <option value=""></option>
                                                            {foreach item=cc from=$centroCosto}
                                                                {if $formDB.fk_a023_num_centro_costo == $cc.pk_num_centro_costo}
                                                                    <option value="{$cc.pk_num_centro_costo}" selected>{$cc.ind_descripcion_centro_costo}</option>
                                                                    {else}
                                                                    <option value="{$cc.pk_num_centro_costo}">{$cc.ind_descripcion_centro_costo}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="ccosto">Centro de Costo</label>
                                                    </div>
                                                    <div class="form-group col-lg-6" id="almacenError">
                                                            <select id="almacen" name="form[int][almacen]" class="form-control select2 select2-list">
                                                                <option value=""></option>
                                                                {foreach item=alm from=$almacen}
                                                                    {if $alm.num_flag_commodity!=1}
                                                                        <option value="{$alm.pk_num_almacen}" selected>{$alm.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="almacen">Almacen</label>
                                                        </div>
                                                    <div class="form-group col-lg-6" id="tipoTranError">
                                                            <select id="tipoTran" name="form[int][tipoTran]" class="form-control select2 select2-list">
                                                                <option value=""></option>
                                                                {foreach item=tran from=$transaccion}
                                                                    {if $tran.ind_cod_tipo_transaccion=="ROC"}
                                                                        <option value="{$tran.pk_num_tipo_transaccion}" selected>{$tran.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="tipoTran">Tipo Transacción</label>
                                                    </div>
                                                    <div class="form-group col-lg-6" id="tipoDocError">

                                                        <select id="tipoDoc" name="form[int][tipoDoc]" class="form-control select2 select2-list">
                                                            <option value=""></option>
                                                            {foreach item=doc from=$documento}
                                                                {if $doc.ind_descripcion=='NOTA DE INGRESO'}
                                                                    <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                                {else}
                                                                    <option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="tipoDoc">Doc. Referencia</label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                            <label class="checkbox-inline checkbox-styled">
                                                                <input type="checkbox" disabled {if isset($formDB.num_flag_manual) and $formDB.num_flag_manual==1}checked{/if}>
                                                                <span>Valorización Manual</span>
                                                            </label>
                                                        </div>
                                                    <div class="col-lg-3">
                                                            <label class="checkbox-inline checkbox-styled">
                                                                <input type="checkbox" disabled {if isset($formDB.num_flag_pendiente) and $formDB.num_flag_pendiente==1}checked{/if} >
                                                                <span>Valorización Pendiente</span>
                                                            </label>
                                                        </div>
                                                    <div class="form-group col-lg-3 floating-label" id="docRefError">
                                                        <input type="text" readonly class="form-control" id="docRef" name="form[int][docRef]" value="{if isset($formDB.ind_orden)}{$formDB.ind_orden}{/if}">
                                                        <label for="docRef">Nro. Doc. Referencia</label>
                                                    </div>
                                                    <input type="hidden"  id="fdoc" class="form-control" name="form[txt][fdoc]" value="{date('Y-m-d')}">
                                                    <div class="form-group col-lg-3 floating-label" id="notaError">

                                                        <input type="text" name="form[alphaNum][nota]" id="nota" class="form-control">
                                                        <label for="nota">Nota de Entrega / Factura</label>
                                                    </div>
                                                    <div class="col-lg-12">
                                                            <div class="form-group" id="comenError">
                                                                <input type="text" name="form[alphaNum][comen]" id="comen" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}">
                                                                <label for="comen"><i class="md md-border-color"></i>Comentario</label>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                </div><!--end #tab1 -->
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head style-primary">
                                            <header>Items</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" id="contenidoTabla">
                                                    <thead>
                                                    <tr>
                                                        <th width="10px">#</th>
                                                        <th width="10px">Item</th>
                                                        <th width="250px">Descripcion</th>
                                                        <th width="10px">Stock Actual</th>
                                                        <th width="10px">Cantidad Pedida</th>
                                                        <th width="10px">Cantidad Recibida</th>
                                                        <th width="10px">Precio Unit.</th>
                                                        <th width="10px">Total</th>
                                                        <th width="10px">Seleccionar Recepción</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item">
                                                    {if isset($formDBI)}
                                                        {$cant=0}
                                                        {foreach item=i from=$formDBI}
                                                            {$cantiRecibida=0}
                                                            {$cantiPedida=$i.cantidad}
                                                            {foreach item=td from=$tranDet}
                                                                {if $td.fk_lgc009_num_orden_detalle==$i.pk_num_orden_detalle}
                                                                    {if $td.ind_cod_tipo_transaccion=='ROC'}
                                                                        {$cantiRecibida=$cantiRecibida+$td.num_cantidad_transaccion}
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                            {$canti=$cantiPedida-$cantiRecibida}
                                                            {if $i.num_flag_exonerado==1}
                                                                {$precio = $i.num_precio_unitario}
                                                            {else}
                                                                {$precio = $i.num_precio_unitario_iva}
                                                            {/if}
                                                            {$total = $canti*$precio}

                                                            {if $canti!=0}
                                                                {$cant=$cant+1}
                                                                <tr id="item{$i.num_secuencia}">
                                                                    <input type="hidden" name="form[int][idDetalleOrden][{$cant}]" value="{$i.pk_num_orden_detalle}">
                                                                    <input type="hidden" name="form[int][idStock][{$cant}]" value="{$i.pk_num_item_stock}">
                                                                    <input type="hidden" name="form[int][cantiPedida][{$cant}]" id="cantiPedida{$cant}" value="{$cantiPedida}">
                                                                    <input type="hidden" id="totalCalculado{$cant}" value="{$total}">
                                                                    <input type="hidden" class="form-control" name="form[int][numUni][{$cant}]" value="{$cant}">
                                                                    <td style="vertical-align: middle;">{$i.num_secuencia}</td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" name="form[int][numItem][{$cant}]" size="2" value="{$i.fk_lgb002_num_item}"></td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" size="2" value="{$i.ind_descripcion_item}"></td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" name="form[int][sAnt][{$cant}]" size="4" value="{$i.num_stock_actual}"></td>
                                                                    <td style="vertical-align: middle;">
                                                                        <input type="text" class="form-control cantidad" name="form[int][canti][{$cant}]" id="cantidad{$cant}" value="{$canti}" secuencia="{$cant}">
                                                                    </td>
                                                                    <td style="vertical-align: middle;">
                                                                        <input type="text" readonly class="form-control" name="form[int][cantiRecibida][{$cant}]" value="{$cantiRecibida}">
                                                                    </td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" name="form[int][precio][{$cant}]" id="precio{$cant}" size="4" value="{$precio}"></td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" name="form[int][total][{$cant}]" id="total{$cant}" size="2" value="{$total}"></td>
                                                                    <td style="vertical-align: middle;">
                                                                        <div class="checkbox checkbox-styled" align="center">
                                                                            <label class="checkbox-inline checkbox-styled">
                                                                                <input type="checkbox" name="form[int][opcion][{$cant}]" class="checkbox checkbox-styled" value="1" id="check{$cant}">
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            {/if}
                                                        {/foreach}
                                                        <input type="hidden" class="form-control" name="form[int][cant]" value="{$cant}">
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--end #tab2 -->
                                </div>
                                <!--end .tab-content -->
                                <ul class="pager wizard">
                                    <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col -->
        </div><!--end .col -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="fa fa-sign-in"></span> Recepcionar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "80%");
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {
        $('#modalAncho').css("width", "80%");
        var app = new  AppFunciones();
        $('#fdoc').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('.cantidad').change(function(){
            var secuencia = $(this).attr('secuencia');
            var cantidad = $('#cantidad'+secuencia).val();
            var cantiPedida = $('#cantiPedida'+secuencia).val();
            if(parseFloat(cantiPedida)<parseFloat(cantidad)){
                cantidad = parseFloat(cantiPedida);
                $('#cantidad'+secuencia).val(cantidad);
            }
            var precio = $('#precio'+secuencia).val();
            var urlCalculo='{$_Parametros.url}modLG/almacen/recepcionCONTROL/calcularMET';
            $.post(urlCalculo, { cantidad: cantidad, precio: precio, contador: secuencia },function(dato) {
                $('#total'+dato['contador']).val(dato['total']);
                if($('#check'+dato['contador']).attr('checked')==undefined){
                    $('#check'+dato['contador']).attr('checked','checked');
                }
            }, 'json');
        });

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metNuevoRegistroTablaJson('dataTablaJson','La Transaccion fue ejecutada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorOpcion'){
                    app.metValidarError(dato,'Disculpa. Debe seleccionar cual(es) detalle(s) desea efectuar la acción');
                }else if(dato['status']=='errorCant'){
                    app.metValidarError(dato,'Disculpa. Las cantidades de los productos no pueden ser cero');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='periodo'){
                    swal("Error!", "No hay un periodo activo para realizar transacciones.", "error");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            }, 'json');
        });
    });
</script>