<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de {$mensaje} de Activos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable5" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{$mensaje}</th>
                            <th>Código</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr class="opciones">
                                    <input type="hidden"
                                           pk_num_miscelaneo_detalle="{$i.pk_num_miscelaneo_detalle}"
                                           ind_nombre_detalle="{$i.ind_nombre_detalle}"
                                            >
                                <td>{$i.pk_num_miscelaneo_detalle}</td>
                                <td>{$i.ind_nombre_detalle}</td>
                                <td>{$i.cod_detalle}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#datatable5').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable5 tbody').on( 'click', '.opciones', function () {
            var input = $(this).find('input');
            {if $mensaje == 'Marcas'}
                $(document.getElementById('pk_num_miscelaneo_detalle1'+{$id})).val(input.attr('pk_num_miscelaneo_detalle'));
                $(document.getElementById('ind_nombre_detalle1'+{$id})).val(input.attr('ind_nombre_detalle'));
            {elseif $mensaje == 'Colores'}
                $(document.getElementById('pk_num_miscelaneo_detalle2'+{$id})).val(input.attr('pk_num_miscelaneo_detalle'));
                $(document.getElementById('ind_nombre_detalle2'+{$id})).val(input.attr('ind_nombre_detalle'));
            {/if}



            $('#cerrarModal2').click();
        });

    });
</script>