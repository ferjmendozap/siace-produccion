<form action="{$_Parametros.url}modLG/almacen/despachoCONTROL/crearDespachoMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idReq)}{$idReq}{/if}" name="idReq"/>
    <input type="hidden" value="{if isset($estado)}{$estado}{/if}" name="estado"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">ITEMS</span> </a></li>
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                        <div class="card panel">
                                            <div class="card-head style-primary">
                                                <header>Información General</header>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-12">
                                                        <div class="form-group col-lg-6" id="ccostoError">
                                                            <select id="ccosto" name="form[int][cCosto]" class="form-control select2 select2-list">
                                                                <option value=""></option>
                                                                {foreach item=cc from=$centroCosto}
                                                                    {if $cc.pk_num_centro_costo==$formDB.fk_a023_num_centro_costo}
                                                                        <option value="{$cc.pk_num_centro_costo}" selected>{$cc.ind_descripcion_centro_costo}</option>
                                                                    {else}
                                                                        <option value="{$cc.pk_num_centro_costo}">{$cc.ind_descripcion_centro_costo}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="ccosto">Centro de Costo</label>
                                                        </div>
                                                        <div class="form-group col-lg-6" id="almacenError">
                                                            <select id="almacen" name="form[int][almacen]" class="form-control select2 select2-list">
                                                                {foreach item=alm from=$almacen}
                                                                    {if $alm.num_flag_commodity!=1}
                                                                        <option value="{$alm.pk_num_almacen}" selected>{$alm.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="almacen">Almacen</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group col-lg-6" id="tipoTranError">
                                                            <select id="tipoTran" name="form[int][tipoTran]" class="form-control select2 select2-list">
                                                                {foreach item=tran from=$transaccion}
                                                                    {if $tran.ind_cod_tipo_transaccion=='REQ'}
                                                                        <!--30 equivale al despacho de requisiciones-->
                                                                        <option value="{$tran.pk_num_tipo_transaccion}" selected>{$tran.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="tipoTran">Tipo Transacción</label>
                                                        </div>
                                                        <div class="form-group col-lg-6" id="tipoDocError">
                                                            <select id="tipoDoc" name="form[int][tipoDoc]" class="form-control select2 select2-list">
                                                                <option value=""></option>
                                                                {foreach item=doc from=$documento}
                                                                    {if $doc.ind_descripcion=='NOTA DE SALIDA'}
                                                                        <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="tipoDoc">Doc. Referencia</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group col-lg-3">
                                                            <label class="checkbox-inline checkbox-styled">
                                                                <input type="checkbox" disabled {if isset($formDB.num_flag_manual) and $formDB.num_flag_manual==1}checked{/if}>
                                                                <span>Valorización Manual</span>
                                                            </label>
                                                        </div>
                                                        <div class="form-group col-lg-3">
                                                            <label class="checkbox-inline checkbox-styled">
                                                                <input type="checkbox" disabled {if isset($formDB.num_flag_pendiente) and $formDB.num_flag_pendiente==1}checked{/if} checked>
                                                                <span>Valorización Pendiente</span>
                                                            </label>
                                                        </div>
                                                        <div class="form-group col-lg-3" id="docRefError">
                                                            <input type="text" readonly name="form[formula][docRef]" value="{if isset($formDB.cod_requerimiento)}{$formDB.cod_requerimiento}{/if}" class="form-control">
                                                            <label for="docRef">Nro. Doc. Referencia</label>
                                                        </div>
                                                        <div class="form-group col-lg-3" id="fdocError">
                                                        <input type="text"  id="fdoc" class="form-control" name="form[txt][fdoc]" value="{date('Y-m-d')}">
                                                        <label for="fdoc">Fecha de Documento<i class="fa fa-calendar"></i></label>
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-12">

                                                        <div class="form-group col-lg-12" id="comenError">
                                                            <input type="text" name="form[alphaNum][comen]" id="comen" class="form-control" value="{if isset($formDB.ind_comentarios)}{$formDB.ind_comentarios}{elseif $estado!='despachar'} {/if}">
                                                            <label for="comen"><i class="md md-border-color"></i>Comentario</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div><!--end #tab1 -->
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head style-primary">
                                            <header>Items</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" id="contenidoTabla">
                                                    <thead>
                                                    <tr align="center">
                                                        <th width="30px">#</th>
                                                        <th width="200px">Descripcion</th>
                                                        <th width="100px">Stock Actual</th>
                                                        <th width="100px">Cant. Pedida</th>
                                                        <th width="100px">Unidad de Medida</th>

                                                        <th width="100px">Cant. por Despachar</th>
                                                        <th width="100px">Cant. Despachada</th>
                                                        <th width="30px">Aprobar</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item">
                                                    {$readonly=""}
                                                    {$disabled=""}
                                                    {$e='PE'}
                                                    {if $estado=='aprobar'}{$e='PR'}{$readonly="readonly"}{$disabled="disabled"}{/if}
                                                    {if isset($formDBI)}
                                                        {$cant=0}
                                                        {foreach item=i from=$formDBI}
                                                            {if $e==$i.ind_estado}
                                                                {$cant=$cant+1}
                                                                {if isset($i.cantidadDespachada)}
                                                                    {$cantidadDespachada = $i.cantidadDespachada}
                                                                {else}
                                                                    {$cantidadDespachada = "0.00"}
                                                                {/if}

                                                                <tr id="item{$i.num_secuencia}">
                                                                    <input type="hidden" name="form[int][numItem][{$cant}]" value="{$i.fk_lgb002_num_item}">
                                                                    <td style="vertical-align: middle;">{$cant}</td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" name="form[alphaNum][descripcion][{$cant}]" size="2" value="{$i.ind_descripcion_item}"></td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" name="form[int][sAnt][{$cant}]" size="4" value="{$i.num_stock_actual}"></td>
                                                                    <td style="vertical-align: middle;"><input type="text" class="form-control" readonly name="form[int][cantiPedida][{$cant}]" size="2" value="{$i.num_cantidad_pedida}"></td>
                                                                    <td style="vertical-align: middle;">
                                                                        <input type="text"  class="form-control cantidad" name="form[int][canti][{$cant}]"  id="cantidad{$cant}" size="6" value="{$i.num_cantidad_pedida-$cantidadDespachada}">
                                                                    </td>
                                                                    <td style="vertical-align: middle;"><input type="text" class="form-control" readonly name="form[alphaNum][Unidad][{$cant}]" size="2" value="{$i.unidad}"></td>
                                                                    <td style="vertical-align: middle;"><input type="text" class="form-control" readonly name="form[int][despachada][{$cant}]" id="despachada{$cant}" size="2" value="{$cantidadDespachada}"></td>
                                                                    <td style="vertical-align: middle;">
                                                                        <div class="checkbox checkbox-styled" align="center">
                                                                            <label class="checkbox-inline checkbox-styled">
                                                                                <input type="checkbox" name="form[int][aprobar][{$cant}]" class="checkbox checkbox-styled" value="1">
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <input type="hidden" name="form[int][numUni][{$cant}]" value="{$i.pk_num_unidad}">
                                                                    <input type="hidden" name="form[int][idStock][{$cant}]" value="{$i.pk_num_item_stock}">
                                                                    {if isset($i.pk_num_orden_detalle)}
                                                                        <input type="hidden" class="form-control" name="form[int][idDetalleOrden][{$cant}]" value="{$i.pk_num_orden_detalle}">
                                                                    {/if}
                                                                    <input type="hidden" class="form-control" name="form[int][idDetalleReq][{$cant}]" value="{$i.pk_num_requerimiento_detalle}">
                                                                </tr>
                                                            {/if}
                                                        {/foreach}
                                                        <input type="hidden" class="form-control" name="form[int][cant]" value="{$cant}">
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--end #tab2 -->
                                </div>
                                <!--end .tab-content -->
                                <ul class="pager wizard">
                                    <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col -->
        </div><!--end .col -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <!--button type="button" class="btn btn-warning ink-reaction btn-raised" id="redirigir">
                <span class="fa fa-money"></span> Redirigir a Compras
            </button-->
            {if $estado=="despachar"}
                {$clase = "fa fa-sign-out"}
            {else}
                {$clase = "icm icm-rating3"}
            {/if}

            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="{$clase}"></span>  {ucwords($estado)}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "80%");
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#fdoc').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('.cantidad').change(function(){
            var contador=0;
            $('.cantidad').each(function(){
                contador=contador+1;
                var cantidad = $('#cantidad'+contador).val();
                var precio = $('#precio'+contador).val();
                var urlCalculo='{$_Parametros.url}modLG/almacen/despachoCONTROL/calcularMET';
                $.post(urlCalculo, { cantidad: cantidad, precio: precio, contador: contador },function(dato) {
                    $('#total'+dato['contador']).val(dato['total']);
                }, 'json');
            });
        });

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    if(dato['estado']=='aprobar') {
                        var mensaje = 'Aprobada';
                    } else {
                        mensaje = 'Despachada';
                    }
                    app.metNuevoRegistroTablaJson('dataTablaJson','La Transaccion fue '+mensaje+' satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorCant'){
                    app.metValidarError(dato,'Disculpa. Las cantidades de los productos no pueden ser mayores a las pedidas');
                }else if(dato['status']=='errorCant2'){
                    app.metValidarError(dato,'Disculpa. Las cantidades de los productos no pueden ser cero (0) o Vacías.');
                }else if(dato['status']=='errorSeleccion'){
                    swal("Error!", "Disculpa. No ha seleccionado items para realizar la acción.", "error");
                }else if(dato['status']=='errorStock'){
                    swal({
                        title: 'Advertencia!',
                        text: 'No hay suficiente stock para realizar la transacción.\n¿Desea Redirigir a Compras para su reposición?',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Redirigir',
                        closeOnConfirm: false
                    }, function(){
                        var url='{$_Parametros.url}modLG/almacen/despachoCONTROL/redirigirMET';

                        $.post(url,  $( "#formAjax" ).serialize() ,function(dato2){
                            if(dato2['status']=='ok'){
                                $(document.getElementById('idTran'+dato2['idTran'])).html(dato2);
                                swal("Ejecutado!", "Se ha dirigido a Compras para su reposición satisfactoriamente.", "success");
                                $(document.getElementById('cerrarModal')).click();
                                $(document.getElementById('ContenidoModal')).html('');
                            }else if(dato2['status']=='errorSeleccion'){
                                swal("Error!", "Disculpa. No ha seleccionado items para realizar la acción.", "error");

                            }else{
                                swal("Error!", "No se puede redirigir" , "error");

                            }

                        },'json');
                    });
                }else if(dato['status']=='errorSQL'){
                    swal('Error!',"Disculpa. no se puede realizar la accion, llame al administrador","error");
                }else if(dato['status']=='periodo'){
                    swal("Error!", "No hay un periodo activo para realizar transacciones.", "error");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            }, 'json');
        });
    });
</script>