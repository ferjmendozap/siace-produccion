<section class="style-default-bright">
    <div class="section-header">
        <input type="hidden" id="estado" value="{$estado}">
        <h2 class="text-primary">{if $estado=='listado'}Listado de Requerimientos{elseif $estado!='generar'}{ucwords($estado)} Requerimientos{else} Listado de Requerimientos Aprobados - Generar Actas de Inicio{/if}</h2>
    </div>
    <input type="hidden" id="estadoListado" value="{$estado}">
    <input type="hidden" id="estadoSimbolo" value="{$estadoSimbolo}">
    <div class="card">
        <div class="card-body">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkDependencia">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="dependencia"
                               class="control-label"> Dependencia: </label>
                    </div>
                    <div class="col-sm-9">
                        <select id="dependenciaL" class="form-control select2-list select3" disabled>
                            <option value="0">Seleccione...</option>
                            {foreach item=dep from=$dependencia}
                                {if $formDBR.fk_a004_num_dependencia==$dep.pk_num_dependencia}
                                    <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                {elseif $predeterminado.fk_a004_num_dependencia==$dep.pk_num_dependencia AND !isset($formDBR.fk_a004_num_dependencia)}
                                    <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkClasificacion">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="clasificacionL1"
                               class="control-label" style="margin-top: 10px;"> Clasificacion: </label>
                    </div>
                    <div class="col-sm-6">
                        <select id="clasificacionL1" class="form-control select2-list select3" disabled>
                            <option value="0">Seleccione...</option>
                            {foreach item=clasificacion from=$clas}
                                <option value="{$clasificacion.pk_num_clasificacion}">{$clasificacion.ind_descripcion}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkCC" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <label for="centroCosto"
                               class="control-label">C. Costo:</label>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group" id="numCCError">
                            <input type="hidden" readonly name="numCC" id="numCC" class="form-control">
                            <input type="text" disabled id="nombreCC" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group floating-label">
                            <button
                                    type="button"
                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                    data-toggle="modal"
                                    data-target="#formModal2"
                                    id="bcc"
                                    titulo="Listado Centro de Costo"
                                    disabled
                                    url="{$_Parametros.url}modLG/reportes/reporteConsumoCONTROL/centroCostoMET/">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkEstado" checked {if $estadoSimbolo!=''} disabled{/if}>
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="estado"
                               class="control-label" style="margin-top: 10px;"> Estado:</label>
                    </div>
                    <div class="col-sm-6">
                        <select id="estadoFiltro" class="form-control select2-list select3" disabled>
                            {foreach item=e from=$listadoEstado}
                                {if $estadoSimbolo!=''}
                                    {if $estadoSimbolo==$e.cod_detalle}
                                        <option value="{$e.cod_detalle}" selected>{$e.ind_nombre_detalle}</option>
                                    {/if}
                                {else}
                                    <option value="{$e.cod_detalle}">{$e.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkBusqueda" >
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label for="busqueda"
                               class="control-label"> Buscar:</label>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control text-center"
                               id="busqueda"
                               disabled>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkFechaDoc" checked="checked">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> F.Documento: </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="desde"
                               style="text-align: center"
                               value="{date('Y-m')}-01"
                               placeholder="Desde"
                               disabled="disabled"
                               readonly>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="hasta"
                               style="text-align: center"
                               value="{date('Y-m-d')}"
                               placeholder="Hasta"
                               disabled="disabled"
                               readonly>
                    </div>
                </div>
            </div>

            <div align="center">
                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary">
                    BUSCAR
                </button>
            </div>
        </div>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nro.</th>
                            <th>Dependencia</th>
                            <th>Fecha Preparación</th>
                            <th>Fecha Aprobación</th>
                            <th>Clasificación</th>
                            <th>Comentarios</th>
                            <th>Estado</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="8">
                                {if in_array('LG-01-01-01-01-01-N',$_Parametros.perfil) AND $estado=='listado'}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descripcion="el Usuario a creado el requerimiento Nro. "
                                        data-toggle="modal" data-target="#formModal"
                                        estado="{$estado}"
                                        data-keyboard="false" data-backdrop="static"
                                        titulo="Registrar Nuevo Requerimiento" id="nuevo">
                                    <i class="md md-create"></i> Nuevo Requerimiento
                                </button>
                                {/if}
                                {if in_array('LG-01-01-02-01-01-N',$_Parametros.perfil) AND $estado=='generar'}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                            descripcion="el Usuario a creado el Acta Nro. " disabled
                                            data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static"
                                            titulo="Registrar Nueva Acta" id="acta">
                                        <i class="md md-create"></i> Nueva Acta
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                        <input type="hidden" id="cantidad" value="0">
                        <input type="hidden" id="clasificacionL2" value="">
                    </table>
                </div>
            </div>
        </div>
        <form id="req_acta"></form>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select3').select2({ allowClear: true });
        $('#desde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#hasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#bcc').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });
        var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/crearModificarRequerimientoMET';
        var app = new AppFunciones();

        if($('#checkEstado').attr('checked')=='checked' && $('#estadoSimbolo').val()==''){
            $('#estadoFiltro').attr('disabled', false);
        }else if($('#estadoSimbolo').val()==''){
            $('#estadoFiltro').attr('disabled', true);
            $('#estadoFiltro option[value=1]').attr("selected",true);
            $('#s2id_estadoFiltro .select2-chosen').html($('#estadoFiltro option:selected').text());
        }

        function verificarBoton(valor = false) {
            var cantidad = 0;

            $('.cambio').each(function () {
                cantidad = parseInt(cantidad) + parseInt(1);
            });

            $('#cantidad').val(cantidad);

            if(cantidad==0 || valor==2){
                $('#acta').attr('disabled',true);
                $('#req_acta').html('');
                $('#clasificacionL2').val('');
            } else {
                $('#acta').attr('disabled',false);
            }
        }

        var eventFired = function ( type ) {
            verificarBoton(2);
        }

        var estadoListado = $('#estadoListado').val();
        var dependenciaL = 'false';
        var clasificacionL = 'false';
        var centroCostoL = 'false';
        var estadoL = 'false';
        var buscarL = 'false';
        var desdeL = 'false';
        var hastaL = 'false';

        var url2 = "{$_Parametros.url}modLG/compras/requerimientoCONTROL/jsonDataTablaMET";
        if($('#checkDependencia').attr('checked')=='checked') { dependenciaL = $('#dependenciaL').val(); }
        if($('#checkClasificacion').attr('checked')=='checked') { clasificacionL = $('#clasificacionL1').val(); }
        if($('#checkCC').attr('checked')=='checked') { centroCostoL = $('#numCC').val(); }
        if($('#checkEstado').attr('checked')=='checked') { estadoL = $('#estadoFiltro').val(); }
        if($('#checkBusqueda').attr('checked')=='checked') { buscarL = $('#busqueda').val(); }

        if($('#checkFechaDoc').attr('checked')=='checked'){
            desdeL= $('#desde').val();
            hastaL= $('#hasta').val();
        }

        var dt = app.dataTable(
                '#dataTablaJson',
                    url2
                    +'/'+estadoListado
                    +'/'+dependenciaL
                    +'/'+clasificacionL
                    +'/'+centroCostoL
                    +'/'+buscarL
                    +'/'+desdeL
                    +'/'+hastaL
                    +'/'+estadoL,
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_requerimiento" },
                    { "data": "ind_dependencia" },
                    { "data": "fec_preparacion" },
                    { "data": "fec_aprobacion"},
                    { "data": "ind_descripcion"},
                    { "data": "ind_comentarios"},
                    { "data": "estatus"},
                    { "orderable": false,"data": "acciones", width:150 }
                ]
        )
        .on( 'order.dt',  function () { eventFired( 'Order' ); } )
        .on( 'search.dt', function () { eventFired( 'Search' ); } )
        .on( 'page.dt',   function () { eventFired( 'Page' ); } )
        .on( 'length.dt',   function () { eventFired( 'lengthMenu' ); } );


        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idReq: 0, estado: $(this).attr('estado'), revisar: 0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        var urlacta = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarActaMET';
        $('#acta').on( 'click', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(urlacta, $( "#req_acta" ).serialize() , function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idReq: $(this).attr('idReq'), estado: $(this).attr('estado'), revisar: $(this).attr('revisar')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idReq: $(this).attr('idReq'), estado: $(this).attr('estado'), revisar: 0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.anular', function () {
            var url = '{$_Parametros.url}modLG/compras/requerimientoCONTROL/anularMET';
            $('#modalAncho').css("width", "40%");
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idReq: $(this).attr('idReq') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.imprimir', function () {
            var url = '{$_Parametros.url}modLG/compras/requerimientoCONTROL/anularMET';
        });

        $("#dataTablaJson tbody").on('click', 'td', function() {

            var fila = $(this).parents('tr');
            var largoCelda = $(this).html().length;
            if(largoCelda<256){
                var style = fila.attr('style');
                var id = fila.attr('id');
                var celdas = fila.find('td');
                var c = 0;
                //requerimiento
                var valor = 0;
                //clasificacion
                var valor5 = 0;
                //estado
                var valor7 = 0;

                celdas.each(function(){
                    c = c+1;
                    if(c==1){ valor = $(this).html(); }
                    if(c==5){ valor5 = $(this).html(); }
                    if(c==7){ valor7 = $(this).html(); }
                });

                if(valor!='No Hay Registros Disponibles' && $('#estado').val()=='generar'){

                    var clasificacionL2 = $('#clasificacionL2').val();
                    var procede = 1;

                    if(valor7!='Aprobado') {
                        swal('Error!','No se pueden seleccionar requerimientos sin aprobar','error');
                        procede = 0;
                    } else if(clasificacionL2==''){
                        $('#clasificacion').val(valor5);
                    } else if(clasificacionL2!=valor5){
                        swal('Error!','No se pueden seleccionar requerimientos mixtos','error');
                        procede = 0;
                    }

                    if(procede==1){

                        if(style == null || style == ''){
                            $.post('{$_Parametros.url}modLG/compras/actaInicioCONTROL/buscarReqMET',{ valor: valor },function(dato){
                                if(dato['estadoActa']=='PR') {
                                    swal('Error!','No se pueden seleccionar requerimientos con Acta de Inicio Preparadas','error');
                                } else {
                                    fila.attr('style','background-color:LightGreen;');
                                    fila.attr('class','cambio');
                                    $('#req_acta').append('<input type="hidden" name="form[formula][req_acta][]" value="'+valor+'" id="req_acta'+valor+'">');
                                    fila.attr('valor',valor);
                                    verificarBoton();
                                }
                            }, 'json');

                        } else {
                            fila.attr('style','');
                            fila.attr('class','');
                            fila.attr('valor','');
                            $('#req_acta'+valor).remove();
                        }
                    }
                    verificarBoton();

                }
            }
        });

        function verificarFechas(cual) {
            if(cual=='checked'){
                $('#desde').attr('disabled', false);
                $('#hasta').attr('disabled', false);
            }else{
                $('#desde').attr('disabled', true);
                $('#hasta').attr('disabled', true);

                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                var yyyy = hoy.getFullYear();

                if(dd<10) { dd='0'+dd; }
                if(mm<10) { mm='0'+mm; }

                var fdesde = yyyy+'-'+mm+'-01';
                var fhasta = yyyy+'-'+mm+'-'+dd;
                $('#desde').val(fdesde);
                $('#hasta').val(fhasta);

            }
        }

        verificarFechas($('#checkFechaDoc').attr('checked'));

        $('#checkClasificacion').on('change', function () {
            if($(this).attr('checked')=='checked') {
                $('#clasificacionL1').attr('disabled',false);
            } else {
                $('#clasificacionL1').attr('disabled',true);
                $('#clasificacionL1 option[value=0]').attr("selected",true);
                $('#s2id_clasificacionL1 .select2-chosen').html($('#clasificacionL1 option:selected').text());
            }
        });

        $('#checkDependencia').on('change', function () {
            if($(this).attr('checked')=='checked') {
                $('#dependenciaL').attr('disabled',false);
            } else {
                $('#dependenciaL').attr('disabled',true);
                $('#dependenciaL option[value={$predeterminado.fk_a004_num_dependencia}]').attr("selected",true);
                $('#s2id_dependenciaL .select2-chosen').html($('#dependenciaL option:selected').text());
            }
        });

        $('#checkCC').change(function () {
            if($('#checkCC').attr('checked')=="checked" ){
                $('#bcc').attr('disabled',false);
            } else {
                $('#bcc').attr('disabled','disabled');
                $('#numCC').val('');
                $('#nombreCC').val('');
            }
        });

        $('#checkEstado').click(function () {
            if($(this).attr('checked')=='checked' && $('#estadoSimbolo').val()==''){
                $('#estadoFiltro').attr('disabled', false);
            }else{
                $('#estadoFiltro').attr('disabled', true);
                $('#estadoFiltro option[value=1]').attr("selected",true);
                $('#s2id_estadoFiltro .select2-chosen').html($('#estadoFiltro option:selected').text());
            }
        });

        $('#checkBusqueda').click(function () {
            if($(this).attr('checked')=='checked'){
                $('#busqueda').attr('disabled', false);
            }else{
                $('#busqueda').attr('disabled', true);
                $('#busqueda').val("");
            }
        });

        $('#checkFechaDoc').click(function () {
            verificarFechas($('#checkFechaDoc').attr('checked'));
        });


        $('.buscar').click(function () {
            var estadoListado = $('#estadoListado').val();
            var dependenciaL = 'false';
            var clasificacionL = 'false';
            var centroCostoL = 'false';
            var estadoL = 'false';
            var buscarL = 'false';
            var desdeL = 'false';
            var hastaL = 'false';

            var url = "{$_Parametros.url}modLG/compras/requerimientoCONTROL/jsonDataTablaMET";
            if($('#checkDependencia').attr('checked')=='checked') { dependenciaL = $('#dependenciaL').val(); }
            if($('#checkClasificacion').attr('checked')=='checked') { clasificacionL = $('#clasificacionL1').val(); }
            if($('#checkCC').attr('checked')=='checked') { centroCostoL = $('#numCC').val(); }
            if($('#checkEstado').attr('checked')=='checked') { estadoL = $('#estadoFiltro').val(); }
            if($('#checkBusqueda').attr('checked')=='checked' && $('#busqueda').val().length>0) { buscarL = $('#busqueda').val(); }

            if($('#checkFechaDoc').attr('checked')=='checked'){
                desdeL= $('#desde').val();
                hastaL= $('#hasta').val();
            }

            $('#dataTablaJson').DataTable().ajax.url(url
                +'/'+estadoListado
                +'/'+dependenciaL
                +'/'+clasificacionL
                +'/'+centroCostoL
                +'/'+buscarL
                +'/'+desdeL
                +'/'+hastaL
                +'/'+estadoL
            ).load();
        });
    });
</script>