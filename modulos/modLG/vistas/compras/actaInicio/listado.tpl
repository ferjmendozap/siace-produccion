<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de las Actas de Inicio de Adquisición</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>CÓDIGO ACTA</th>
                                <th>NOMBRE PROCEDIMIENTO</th>
                                <th>MODALIDAD DE CONTRATACION</th>
                                <th>ESTADO</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('.generar').click(function () {
            var idActa=$(this).attr('idActa');
            var urlGen = '{$_Parametros.url}modLG/reportes/reporteActaInicioCONTROL/generarMET/?idActa='+idActa;
            $.post(urlGen, { }, function () {
            });
        });
        var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarActaMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/compras/actaInicioCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_codigo_acta", width:150 },
                    { "data": "ind_nombre_procedimiento" },
                    { "data": "ind_modalidad_contratacion" },
                    { "data": "estado"},
                    { "orderable": false,"data": "acciones", width:180}
                ]
        );
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idReq: $(this).attr('idReq'), idActa: $(this).attr('idActa'), idAsisA: $(this).attr('idAsisA'), idAsisB: $(this).attr('idAsisB'), idAsisC: $(this).attr('idAsisC'), idAsisD: $(this).attr('idAsisD')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idActa: $(this).attr('idActa'), idAsisA: $(this).attr('idAsisA'), idAsisB: $(this).attr('idAsisB'), idAsisC: $(this).attr('idAsisC'), idAsisD: $(this).attr('idAsisD'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.evaluar', function () {
            $('#modalAncho').css("width", "80%");
            var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarEvaluacionMET';
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { ver:0, idActa: $(this).attr('idActa'), idReq: $(this).attr('idReq'), idAsisA: $(this).attr('idAsisA'), idAsisB: $(this).attr('idAsisB'), idAsisC: $(this).attr('idAsisC')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.anular', function () {
            var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/anularActaMET';
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idActa: $(this).attr('idActa'), estado : $(this).attr('estado') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.generar', function () {
            var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/generarActaMET';
            $.post(url, { idActa: $(this).attr('idActa') }, function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });
    });
</script>