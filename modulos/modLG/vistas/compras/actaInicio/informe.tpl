<form action="{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarInformeRecMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idEval)}{$idEval}{/if}" name="idEval"/>
    <input type="hidden" value="{if isset($idInfor)}{$idInfor}{/if}" name="idInfor"/>
    <input type="hidden" value="{if isset($opcion)}{$opcion}{/if}" name="opcion" id="opcion"/>
    <input type="hidden" value="{if isset($idProveedor)}{$idProveedor}{/if}" name="idProveedor"/>
    <div class="modal-body">
        <div class="row">
           {if $opcion!="crear" AND $opcion!="modificarInfo"}
               {$disabled="disabled"}
           {else}
                {$disabled=""}
           {/if}
            <div class="col-lg-12">
                <div class="col-lg-12">
                    {$cant=strlen($idActa)}
                    {$idVisual=$idActa}
                    {$cont=0}
                    {while $cont<$cant}
                        {if $cant<4}
                            {$idVisual="0$idVisual"}
                        {/if}
                        {$cont=$cont+1}
                    {/while}
                    <div class="form-group floating-label col-lg-12" id="asuntoError">
                        <textarea name="form[formula][asunto]" id="" {$disabled} class="form-control" cols="30" rows="10" >{if isset($informe.ind_asunto)}{$informe.ind_asunto}{else}{$objeto}{/if}</textarea>
                        <label for="asunto"><i class="md md-border-color"></i>Asunto</label>
                    </div>
                    <div class="form-group floating-label col-lg-12" id="objetoError">
                        <textarea name="form[formula][objeto]" id="" {$disabled} class="form-control" cols="30" rows="10" >{if isset($informe.ind_objeto_consulta)}{$informe.ind_objeto_consulta}{elseif isset($objeto)}{$objeto}{/if}</textarea>
                        <label for="objeto"><i class="md md-border-color"></i>Objeto de la Consulta de Precios</label>
                    </div>
                    <div class="form-group floating-label col-lg-12" id="concError">
                        <textarea name="form[formula][conc]" id="" {$disabled} class="form-control" cols="30" rows="10" >{if isset($informe.ind_conclusiones)}{$informe.ind_conclusiones}{elseif isset($conclusion)}{$conclusion}{/if}</textarea>
                        <label for="conc"><i class="md md-border-color"></i>Conclusiones</label>
                    </div>
                    <div class="form-group floating-label col-lg-12" id="recError">
                        <textarea name="form[formula][rec]" id="" {$disabled} class="form-control" cols="30" rows="10" >{if isset($informe.rec)}{$informe.rec}{elseif isset($recomendacion)}{$recomendacion}{/if}</textarea>
                        <label for="rec"><i class="md md-border-color"></i>Recomendaciones</label>
                    </div>
                    <div class="form-group floating-label col-lg-12" id="artNumError">
                        <input type="text" class="form-control" {$disabled} id="artNum" name="form[formula][artNum]" value="{if isset($informe.ind_articulo_numeral)}{$informe.ind_articulo_numeral}{/if}" >
                        <label for="artNum"><i class="md md-border-color"></i>Articulo y Numeral</label>
                    </div>
                    <div class="form-group floating-label col-lg-12" id="proveedorError">
                        {$c=0}
                        <!--125-->
                        {foreach item=p1 from=$proveedores1}
                            {$c=$c+1}
                            <div class="form-group floating-label col-lg-12" id="proveedor{$c}Error">
                                <select name="form[int][proveedor{$c}]" {$disabled} class="form-control select2-list select2">
                                    <option value=""></option>
                                        {if isset($proveedores2)}
                                            {$valor=0}
                                            {foreach item=p2 from=$proveedores2}
                                                {if $p2.pk_num_proveedor==$p1.pk_num_proveedor}
                                                    {$valor = $p2.pk_num_proveedor}
                                                {/if}
                                            {/foreach}
                                            {foreach item=p3 from=$proveedores1}
                                                {if $p3.pk_num_proveedor==$valor}
                                                    <option value="{$p3.pk_num_proveedor}" selected>{$p3.ind_nombre1}</option>
                                                {else}
                                                    <option value="{$p3.pk_num_proveedor}">{$p3.ind_nombre1}</option>
                                                {/if}
                                            {/foreach}
                                        {else}
                                            {foreach item=p3 from=$proveedores1}
                                                <option value="{$p3.pk_num_proveedor}">{$p3.ind_nombre1}</option>
                                            {/foreach}
                                        {/if}
                                </select>
                                <label for="adjudicacion"><i class="md md-border-color"></i>{if $c==1}*{/if}Proveedor Recomendado (Opci&oacute;n {$c}) </label>
                            </div>
                        {/foreach}
                        <input type="hidden" name="form[int][cantidadProvRec]" value="{$c}">
                    </div>

                    <div class="form-group floating-label col-lg-12" id="adjudicacionError">
                        <select name="form[int][adjudicacion]" {$disabled} class="form-control select2-list select2">
                            <option value=""></option>
                            {foreach item=adj from=$adjudicacion}
                                {if isset($informe.fk_a006_num_miscelaneo_detalle_tipo_adjudicacion) and $adj.pk_num_miscelaneo_detalle == $informe.fk_a006_num_miscelaneo_detalle_tipo_adjudicacion}
                                    <option value="{$adj.pk_num_miscelaneo_detalle}" selected>{$adj.ind_nombre_detalle}</option>
                                {else}
                                    <option value="{$adj.pk_num_miscelaneo_detalle}">{$adj.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="adjudicacion"><i class="md md-border-color"></i>Tipo de Adjudicación</label>
                    </div>


                    <div class="form-group floating-label col-sm-12" id="asisAError">
                        <label for="asisA"><i class="md md-people"></i>
                            Asistente A</label>
                        <input type="hidden" class="form-control" name="form[int][asisA]"
                               value="{if isset($idAsisA)}{$idAsisA}{/if}"
                               id="idEmpleado">
                        <input type="text" class="form-control disabled" id="personaResp" value="{if isset($AsisA.nombre)}{$AsisA.nombre}{/if}" disabled>
                        <input type="text" class="form-control disabled" id="cedula" value="{if isset($AsisA.ind_documento_fiscal)}{$AsisA.ind_documento_fiscal}{/if}" disabled>
                    </div>
                    <div class="form-group floating-label col-sm-12" id="asisBError">
                        <label for="asisB"><i class="md md-people"></i>
                            Asistente B</label>
                        <input type="hidden" class="form-control" name="form[int][asisB]"
                               value="{if isset($idAsisB)}{$idAsisB}{/if}"
                               id="idEmpleado">
                        <input type="text" class="form-control disabled" id="personaResp" value="{if isset($AsisB.nombre)}{$AsisB.nombre}{/if}" disabled>
                        <input type="text" class="form-control disabled" id="cedula" value="{if isset($AsisB.ind_documento_fiscal)}{$AsisB.ind_documento_fiscal}{/if}" disabled>
                    </div>
                    <div class="form-group floating-label col-sm-12" id="asisCError">
                        <label for="asisC"><i class="md md-people"></i>
                            Asistente C</label>
                        <input type="hidden" class="form-control" name="form[int][asisC]"
                               value="{if isset($idAsisC)}{$idAsisC}{/if}"
                               id="idEmpleado">
                        <input type="text" class="form-control disabled" id="personaResp" value="{if isset($AsisC.nombre)}{$AsisC.nombre}{/if}" disabled>
                        <input type="text" class="form-control disabled" id="cedula" value="{if isset($AsisC.ind_documento_fiscal)}{$AsisC.ind_documento_fiscal}{/if}" disabled>
                    </div>
                    <div class="form-group floating-label col-lg-12">
                        <textarea disabled class="form-control" cols="30" rows="10" >{if isset($informe.ind_razon_rechazo)}{$informe.ind_razon_rechazo}{/if}</textarea>
                        <label><i class="md md-border-color"></i>Razón de Rechazo</label>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if $ver==0}
                <button type="button" class="btn btn-primary ink-reaction btn-warning" id="verEval"
                        idEval="{$informe.fk_lgb011_num_evaluacion}"
                        titulo="Ver Evaluacion">
                    <span class="icm icm-eye"></span>  Evaluacion
                </button>
            {if $opcion=="aprobar"}
                <button type="button" class="btn btn-primary ink-reaction btn-danger" id="rechazo">
                    <span class="md md-keyboard-backspace"></span>  Reverzar
                </button>
            {/if}
            {if $opcion=="aprobar"}
                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="apro">
                    <span class="icm icm-rating3"></span> Aprobar
                </button>
            {/if}
            {if $opcion=="revisar"}
                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="apro">
                    <span class="icm icm-rating"></span> Revisar
                </button>
            {/if}
            {if $opcion=="modificarInfo"}
                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                    <span class="fa fa-edit"></span> Modificar
                </button>
            {/if}
            {if $opcion=="crear"}
                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                    <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                </button>
            {/if}
        {/if}

    </div>
</form>

<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {
        var app = new  AppFunciones();

        $('#rechazo').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $(document.getElementById('opcion')).val('rechazo');
            var url2 = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/actualizarInformeRecMET';
            $.post(url2,$( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Informe fue Modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else {
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Informe fue '+dato['status']+' satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        $('#verEval').click(function () {
            var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/generarEvaluacionOdtMET';
            $.post(url, { idEval: $(this).attr('idEval') }, function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });

        $('#apro').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post('{$_Parametros.url}modLG/compras/actaInicioCONTROL/actualizarInformeRecMET', $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Informe fue Modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else {
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Informe fue '+dato['status']+' satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    swal("Realizado!", 'El Informe fue modificado satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='nuevo'){
                    swal("Realizado!", 'El Informe fue guardado satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('crear'+dato['idEval'])).remove();
                }
            }, 'json');
        });
    });
</script>