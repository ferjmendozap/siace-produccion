<form action="{$_Parametros.url}modLG/compras/generarOrdenCONTROL/generarOrdenMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idOrden)}{$idOrden}{/if}" name="idOrden"/>
    <input type="hidden" value="{if isset($estado)}{$estado}{/if}" name="estado"/>
    <input type="hidden" value="{if isset($idAdj)}{$idAdj}{/if}" name="idAdj" id="idAdj"/>    <input type="hidden" value="{if isset($formBD.fk_a023_num_centro_costo)}{$formBD.fk_a023_num_centro_costo}{else}{$predeterminado.fk_a023_num_centro_costo}{/if}" id="cc"/>
    <input type="hidden" value="{$predeterminado.fk_a023_num_centro_costo}" id="cc"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab"><span class="step">1</span>  <br/><span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <br/> <span class="title">ITEMS/COMMODITIES</span> </a></li>
                                    <li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <br/> <span class="title">COTIZACIONES</span> </a></li>
                                    <li><a href="#tab5" data-toggle="tab"><span class="step">3</span> <br/> <span class="title">DISTRIBUCIÓN PRESUPUESTARIA/CONTABLES</span> </a></li>
                                    <!--li><a href="#tab5" data-toggle="tab"><span class="step">3</span> <br/> <span class="title">COTIZACIONES</span> </a></li>
                                    <li><a href="#tab5" data-toggle="tab"><span class="step">4</span> <br/> <span class="title">AVANCES</span> </a></li-->
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                <div class="col-lg-12">
                                    <div class="card-head card-head-xs style-primary" align="center">
                                        <header>Información General</header>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-12" id="dependenciaError">
                                            <select name="form[int][dependencia]" id="dependencia" class="form-control select2-list select2" readonly>
                                                {foreach item=dep from=$dependencia}
                                                    {if $formBD.fk_a004_num_dependencia==$dep.pk_num_dependencia}
                                                        <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="dependencia">Dependencia</label>
                                            <input type="hidden" value="1" name="form[int][cCosto]" id="cCosto">
                                        </div>
                                        <div class="form-group col-lg-12" id="centro_costoError">
                                            <label for="centro_costo">Centro de Costo</label>
                                            <select id="centro_costo" name="form[int][centro_costo]" class="form-control select2-list select2" {if $estado!='istado de'} readonly {/if} {$disabled}>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-lg-12" id="proveedorError">
                                            <div class="col-lg-8">
                                                <input type="hidden" name="form[int][proveedor]" id="idProveedor" class="form-control" value="{if isset($formBD.fk_lgb022_num_proveedor)}{$formBD.fk_lgb022_num_proveedor}{/if}">
                                                <input type="text" disabled id="persona" class="form-control" value="{if isset($formBD.ind_nombre1)}{$formBD.ind_nombre1} {$formBD.ind_apellido1}{/if}">
                                            </div>
                                            <div class="col-lg-3">
                                                <input type="text" disabled id="cedula" class="form-control" value="{if isset($formBD.provCed)}{$formBD.provCed}{/if}">
                                            </div>
                                            <label for="idProveedor" class="control-label">Proveedor Recomendado:</label>
                                        </div>
                                        <div class="form-group col-lg-12" id="tipo_servicioError">
                                            <label for="tipo_servicio">Tipo de Servicio</label>
                                            <select id="tipo_servicio" class="form-control select2-list select2" name="form[int][tipo_servicio]" readonly>
                                                <option value=""></option>
                                            </select>
                                            <input type="hidden" value="0" id="iva">
                                            <input type="hidden" value="0" id="retencion">
                                            <input type="hidden" name="form[int][partida]" value="{$partidaCuenta.pk_num_partida_presupuestaria}" id="partidaIva" nombre="{$partidaCuenta.ind_denominacion}" codigo="{$partidaCuenta.cod_partida}">
                                            <input type="hidden" name="form[int][cuenta]" value="{$partidaCuenta.fk_cbb004_num_plan_cuenta_onco}" id="cuentaIva" nombre="{$partidaCuenta.ind_descripcion}" codigo="{$partidaCuenta.cod_cuenta}">
                                        </div>
                                        <div class="form-group col-lg-6" id="formaPagoError">
                                            <select id="formaPago" name="form[int][formaPago]" class="form-control select2-list select2" readonly>
                                                {foreach item=fp from=$formaPago}
                                                    {if (isset($formBD.fk_a006_num_miscelaneos_forma_pago) and $formBD.fk_a006_num_miscelaneos_forma_pago == $fp.pk_num_miscelaneo_detalle) or
                                                    (isset($formBD.cod_tipo_pago) and $formBD.cod_tipo_pago == $fp.cod_detalle)}
                                                        <option value="{$fp.pk_num_miscelaneo_detalle}" selected>{$fp.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="formaPago">Forma de Pago</label>
                                        </div>
                                        <div class="form-group col-lg-6" id="tipoOrdenError">
                                            <select id="tipoOrden" name="form[txt][tipoOrden]" class="form-control select2-list select2" readonly>
                                                <option value="OC">Orden Compra</option>
                                                <option value="OS">Orden Servicio</option>
                                            </select>
                                            <label for="tipoOrden">Tipo de Orden</label>
                                        </div>
                                        <div class="form-group col-lg-6" id="clasificacionError">
                                            <input type="hidden" id="clasificacion_id" name="form[int][clasificacion]" value="{if isset($formBD.fk_lgb017_num_clasificacion)}{$formBD.fk_lgb017_num_clasificacion}{/if}">
                                            <select id="clasificacion" class="form-control select2-list select2" readonly>
                                                {foreach item=clasificacion from=$clas}
                                                    {if isset($formBD.fk_lgb017_num_clasificacion) and $formBD.fk_lgb017_num_clasificacion == $clasificacion.pk_num_clasificacion}
                                                        <option value="{$clasificacion.pk_num_clasificacion}" selected>{$clasificacion.ind_descripcion}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="clasificacion">Clasificación</label>
                                        </div>
                                        <div class="form-group col-lg-6" id="almacenError">
                                            <!--input type="hidden" id="almacen_id" name="form[int][almacen]" value="{if isset($formBD.fk_a006_num_miscelaneos_almacen)}{$formBD.fk_a006_num_miscelaneos_almacen}{/if}"-->
                                            <label for="almacen">Almacen</label>
                                            <select id="almacen" class="form-control select2-list select2" name="form[int][almacen]" readonly>
                                                <option value=""></option>
                                            </select>
                                        </div>

                                        <div class="col-lg-12 style-primary" style="text-align: center;">
                                            <header>Información Adicicional</header>
                                        </div>
                                        <div class="col-lg-12" id="aprobadoError">
                                            <div class="form-group">
                                                <input type="text" disabled id="persona6" class="form-control disabled" value="{if isset($empAprueba.ind_nombre1)}{$empAprueba.ind_nombre1} {$empAprueba.ind_apellido1}{/if}">
                                                <label for="aprobado"><i class="md md-people"></i>Aprobado por</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" id="revisadoError">
                                            <div class="form-group">
                                                <input type="text" disabled id="persona4" class="form-control disabled" value="{if isset($empRevisa.ind_nombre1)}{$empRevisa.ind_nombre1} {$empRevisa.ind_apellido1}{/if}">
                                                <label for="revisado"><i class="md md-people"></i>Revisado por</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" id="ingresadoError">
                                            <div class="form-group">
                                                <input type="text" disabled id="persona3" class="form-control disabled" value="{if isset($formBD.prepNom)}{$formBD.prepNom} {$formBD.prepApe}{/if}">
                                                <label for="ingresado"><i class="md md-people"></i>Ingresado por</label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="style-primary" style="text-align: center;">
                                            <header>Monto de la Compra</header>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group" id="mAfectoError">
                                                <div class="form-group">
                                                    <input type="text" readonly name="form[int][mAfecto]" id="mAfecto" value="{if isset($formBD.num_monto_afecto)}{$formBD.num_monto_afecto}{else}0{/if}" class="form-control">
                                                    <label for="mAfecto">Monto Afecto</label>
                                                </div>
                                            </div>
                                            <div class="form-group" id="mNoAfectoError">
                                                <div class="form-group">
                                                    <input type="text" readonly name="form[int][mNoAfecto]" id="mNoAfecto" value="{if isset($formBD.num_monto_no_afecto)}{$formBD.num_monto_no_afecto}{else}0{/if}" class="form-control">
                                                    <label for="mNoAfecto">Monto No Afecto</label>
                                                </div>
                                            </div>
                                            <div class="form-group" id="mBrutoError">
                                                <div class="form-group">
                                                    <input type="text" readonly name="form[int][mBruto]" id="mBruto" value="{if isset($formBD.num_monto_bruto)}{$formBD.num_monto_bruto}{else}0{/if}" class="form-control">
                                                    <label for="mBruto">Monto Bruto</label>
                                                </div>
                                            </div>
                                            <div class="form-group" id="impuestosError">
                                                <div class="form-group">
                                                    <input type="text" readonly name="form[int][impuestos]" id="impuestos" value="{if isset($formBD.num_monto_igv)}{$formBD.num_monto_igv}{else}0{/if}" class="form-control">
                                                    <label for="impuestos">Impuestos</label>
                                                </div>
                                            </div>
                                            <div class="form-group" id="mTotalError">
                                                <div class="form-group">
                                                    <input type="text" readonly name="form[int][mTotal]" id="mTotal" value="{if isset($formBD.num_monto_total)}{$formBD.num_monto_total}{else}0{/if}" class="form-control">
                                                    <label for="mTotal">Monto Total</label>
                                                </div>
                                            </div>
                                            <div class="form-group" id="mPendienteError">
                                                <div class="form-group">
                                                    <input type="text" readonly name="form[int][mPendiente]" id="mPendiente" value="{if isset($formBD.num_monto_pendiente)}{$formBD.num_monto_pendiente}{else}0{/if}" class="form-control">
                                                    <label for="mPendiente">Monto Pendiente</label>
                                                </div>
                                            </div>
                                            <div class="form-group" id="oCargosError">
                                                <div class="form-group">
                                                    <input type="text" readonly name="form[int][oCargos]" id="oCargos" value="{if isset($formBD.num_monto_otros)}{$formBD.num_monto_otros}{else}0{/if}" class="form-control">
                                                    <label for="oCargos">Otros Cargos</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card-head card-head-xs style-primary" align="center">
                                        <header>Organismo</header>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group" id="plazoError">
                                            <input type="text" maxlength="2" name="form[int][plazo]" id="plazo" class="form-control" {if (isset($ver) and $ver==1)} disabled {/if}{if isset($formBD.num_plazo_entrega)} value="{$formBD.num_plazo_entrega}"{/if}>
                                            <label for="plazo"><i class="md md-border-color"></i>Plazo de Entrega (días)</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group" id="entregarError">
                                            <input type="text" name="form[txt][entregar]" id="entregar" class="form-control" {if (isset($ver) and $ver==1)} disabled {/if} value="{if isset($formBD.ind_entregar_en)}{$formBD.ind_entregar_en}{/if}">
                                            <label for="entregar"><i class="md md-border-color"></i>Entregar En</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="direccionError">
                                            <input type="text" name="form[txt][direccion]" id="direccion" class="form-control" {if (isset($ver) and $ver==1)} disabled {/if} value="{if isset($formBD.ind_direccion_entrega)}{$formBD.ind_direccion_entrega}{/if}">
                                            <label for="direccion"><i class="md md-border-color"></i>Direccion Entrega</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card-head card-head-xs style-primary" align="center">
                                        <header>Observaciones</header>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-6">
                                            <div class="form-group" id="descripcionError">
                                                <textarea name="form[alphaNum][descripcion]" id="descripcion" class="form-control"></textarea>
                                                <label for="descripcion"><i class="md md-border-color"></i>Descripcion</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group" id="comentarioError">
                                                <textarea name="form[alphaNum][comentario]" id="comentario" class="form-control"></textarea>
                                                <label for="comentario"><i class="md md-border-color"></i>Comentario</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                                            <header>Items/Commodities</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="col-lg-6">
                                                </div>
                                                <div class="col-lg-6">
                                                </div>
                                                <table class="table table-striped no-margin" id="contenidoTabla">
                                                    <thead>
                                                    <tr>
                                                        <th width="10px">#</th>
                                                        <th width="10px">Código</th>
                                                        <th width="10px">Descripción</th>
                                                        <th width="10px">Cant. Pedida</th>
                                                        <th width="10px">P. Unit.</th>
                                                        <th width="10px">% Desc.</th>
                                                        <th width="10px">Desc. Fijo</th>
                                                        <th width="10px">Exon.</th>
                                                        <th width="10px">Monto P. Unit.</th>
                                                        <th width="10px">Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item">

                                                    {if isset($formBDD)}
                                                        {$cont=1}
                                                        {foreach item=det from=$formBDD}
                                                            {if $det.pk_num_item}
                                                                <input type="hidden" id="cuentaIC{$det.pk_num_item}" class="cuenta"
                                                                       value="{$det.cuentaItem}"
                                                                       codCuenta="{$det.codCuentaItem}"
                                                                       descCuenta="{$det.descCuentaItem}"
                                                                       cantidad="{number_format($det.num_cantidad,0)}"
                                                                       precio="{number_format($det.num_precio_unitario,6,'.','')}">
                                                                <input type="hidden" id="partidaIC{$det.pk_num_item}" class="partida"
                                                                       value="{$det.partidaItem}"
                                                                       codPartida="{$det.codPartidaItem}"
                                                                       descPartida="{$det.descPartidaItem}"
                                                                       cantidad="{number_format($det.num_cantidad,0)}"
                                                                       precio="{number_format($det.num_precio_unitario,6,'.','')}">
                                                                {$num=$det.pk_num_item}
                                                                {$desc=$det.ind_descripcion_item}
                                                                {$partida=$det.codPartidaItem}
                                                                {$idPartida=$det.partidaItem}
                                                                {$tipo="I"}
                                                                {$cuenta=$det.fk_cbb004_num_plan_cuenta_gasto_oncop}
                                                                {$unidad=$det.fk_lgb004_num_unidad_compra}
                                                            {else}
                                                                <input type="hidden" id="cuentaIC{$det.pk_num_commodity}" class="cuenta"
                                                                       value="{$det.cuentaComm}"
                                                                       codCuenta="{$det.codCuentaComm}"
                                                                       descCuenta="{$det.descCuentaComm}"
                                                                       cantidad="{number_format($det.num_cantidad,0)}"
                                                                       precio="{number_format($det.num_precio_unitario,6,'.','')}">
                                                                <input type="hidden" id="partidaIC{$det.pk_num_commodity}" class="partida"
                                                                       value="{$det.partidaComm}"
                                                                       codPartida="{$det.codPartidaComm}"
                                                                       descPartida="{$det.descPartidaComm}"
                                                                       cantidad="{number_format($det.num_cantidad,0)}"
                                                                       precio="{number_format($det.num_precio_unitario,6,'.','')}">
                                                                {$num=$det.pk_num_commodity}
                                                                {$desc=$det.ind_descripcion_commodity}
                                                                {$partida=$det.codPartidaComm}
                                                                {$idPartida=$det.partidaComm}
                                                                {$cuenta=$det.fk_cbb004_num_plan_cuenta}
                                                                {$unidad=$det.fk_lgb004_num_unidad}
                                                                {$tipo="C"}
                                                            {/if}
                                                            <tr class="ic" idIC="{$num}">
                                                                <td style="vertical-align: middle;">
                                                                    <input type="text" readonly class="form-control" name="form[int][sec]" value="{$cont}">
                                                                    <input type="hidden" name="form[txt][actaDet][{$cont}]" value="{$det.fk_lgb023_num_acta_detalle}">
                                                                    <input type="hidden" name="form[txt][tipoNum][{$cont}]" value="{$tipo}">
                                                                    <input type="hidden" name="form[int][unidad][{$cont}]" value="{$unidad}">
                                                                    <input type="hidden" name="form[int][cuentaDetalle][{$cont}]" value="{$cuenta}" id="cuenta{$cont}">
                                                                    <input type="hidden" name="form[int][partidaDetalle][{$cont}]" value="{$partida}" id="partida{$cont}">
                                                                    <input type="hidden" name="form[int][idPartidaDetalle][{$cont}]" value="{$idPartida}">
                                                                </td>
                                                                <td><input readonly type="text" class="form-control" value="{$num}" name="form[int][num][{$cont}]" size="4"></td>
                                                                <td><input readonly type="text" class="form-control" value="{$desc}" name="form[alphaNum][desc][{$cont}]" size="6"></td>
                                                                <td><input readonly type="text" class="form-control nuevoValor" value="{$det.num_cantidad}" name="form[int][cant][{$cont}]" id="cantidad{$num}" sec="{$cont}" size="6"></td>
                                                                <td><input readonly type="text" class="form-control nuevoValor" value="{$det.num_precio_unitario}" name="form[int][precio][{$cont}]" id="precio{$num}" sec="{$cont}" size="4"></td>
                                                                <td><input readonly type="text" class="form-control nuevoValor" value="{$det.num_descuento_porcentaje}" name="form[int][descPorc][{$cont}]" id="descPorc{$num}" sec="{$cont}"></td>
                                                                <td><input readonly type="text" class="form-control nuevoValor" value="{$det.num_descuento_fijo}" name="form[int][descFijo][{$cont}]" id="descFijo{$num}" sec="{$cont}"></td>
                                                                <td style="vertical-align: middle;">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label class="checkbox-inline checkbox-styled">
                                                                            <input type="checkbox" disabled class="form-control nuevoValor" id="exonerado{$cont}" {if $det.num_flag_exonerado==1} checked {/if} value="1" sec="{$cont}">
                                                                        </label>
                                                                    </div>
                                                                    <input type="hidden" name="form[int][exonerado][{$cont}]" value="{$det.num_flag_exonerado}">
                                                                </td>
                                                                <td><input type="text" class="form-control" readonly value="{if $det.num_flag_exonerado!=1}{$det.num_precio_unitario_iva}{else}{$det.num_precio_unitario}{/if}" name="form[int][montoPU][{$cont}]" id="montoPU{$num}"></td>
                                                                <td><input type="text" class="form-control total" readonly value="{$det.num_total}" name="form[int][total][{$cont}]" idItem="{if isset($det.pk_num_item)}{$det.pk_num_item}{else}{$det.pk_num_commodity}{/if}" id="total{$num}"></td>
                                                            </tr>
                                                            {$cont=$cont+1}
                                                        {/foreach}
                                                        <input type="hidden" value="{$cont}" name="form[int][cantidad]" class="cantidad" id="cantidad" size="2">
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane floating-label" id="tab3">
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                                            <header>Cotizaciones</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" style="white-space:nowrap;overflow-x:scroll;overflow-y:scroll;width:100%;height:250px;display: inline-block;" id="contenidoTabla2">
                                                    <thead>
                                                    <tr>
                                                        <th>Código</th>
                                                        <th>Razón Social<i style="color: #ffffff">________________________________________________</i></th>
                                                        <th>Cantidad<i style="color: #ffffff">___</i></th>
                                                        <th>Precio Unit.</th>
                                                        <th>Precio Unit./Imp.</th>
                                                        <th>Monto Total</th>
                                                        <th>Asig.</th>
                                                        <th>Dias Entrega</th>
                                                        <th>Fecha Entrega</th>
                                                        <th>Cotización #</th>
                                                        <th>Observación<i style="color: #ffffff">________________________________________________</i></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="cotizaciones">
                                                    {if isset($cotizaciones)}
                                                        {$cual = 0}
                                                        {foreach item=c from=$cotizaciones}

                                                            {$ic = $c.fk_lgb002_num_item+$c.fk_lgb003_num_commodity}

                                                            {if $cual != $ic}
                                                                {$cual = $ic}
                                                                <tr style="font-weight:bold;">
                                                                    <td style="text-align: center;">{$c.fk_lgb002_num_item}{$c.fk_lgb003_num_commodity}</td>
                                                                    <td colspan="10">{$c.ind_descripcion}{$cual}</td>
                                                                </tr>
                                                            {/if}
                                                            {$checkAsignado = ''}
                                                            {if $c.num_flag_asignado==1}
                                                                {$checkAsignado = 'checked'}
                                                            {/if}
                                                            <tr>
                                                                <td>{$c.pk_num_proveedor}</td>
                                                                <td>{$c.nombre}</td>
                                                                <td>{$c.num_cantidad}</td>
                                                                <td>{$c.num_precio_unitario}</td>
                                                                <td>{$c.num_precio_unitario_iva}</td>
                                                                <td>{$c.num_total}</td>
                                                                <td style="vertical-align: top;">
                                                                    <div align="center" class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" class="bloqueoCheck" {$checkAsignado}><span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>{$c.num_dias_entrega}</td>
                                                                <td>{$c.fec_entrega}</td>
                                                                <td>{$c.ind_num_cotizacion}</td>
                                                                <td>{$c.ind_observacion}</td>
                                                            </tr>

                                                        {/foreach}
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                                            <header>Requerimientos</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" style="white-space:nowrap;overflow-x:scroll;overflow-y:scroll;width:100%;height:250px;display: inline-block;" id="contenidoTabla3">
                                                    <thead>
                                                    <tr>
                                                        <th>Requerimiento</th>
                                                        <th>Req. Linea</th>
                                                        <th>Cantidad<i style="color: #ffffff">___</i></th>
                                                        <th>Fecha Pedida</th>
                                                        <th>Fecha Aprobación</th>
                                                        <th>Comentarios<i style="color: #ffffff">________________________________________________</i></th>
                                                        <th>Preparado Por<i style="color: #ffffff">________________________________________________</i></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="requerimientos">
                                                    {if isset($requerimientos)}
                                                        {foreach item=r from=$requerimientos}
                                                            <tr>
                                                                <th>{$r.cod_requerimiento}</th>
                                                                <th>{$r.num_secuencia}</th>
                                                                <th>{$r.num_cantidad_pedida}</th>
                                                                <th>{$r.fec_preparacion}</th>
                                                                <th>{$r.fec_aprobacion}</th>
                                                                <th>{$r.ind_comentarios}</th>
                                                                <th>{$r.nombre}</th>
                                                            </tr>

                                                        {/foreach}
                                                    {/if}

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane floating-label" id="tab5">
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" align="center">
                                            <header>Distribución Contable</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped table-hover" style="width: 100%;display:block;overflow: auto;" id="cuenta2">
                                                    <thead style="display: inline-block; width: 100%;">
                                                    <tr>
                                                        <th style="width:10%" class="codCuenta">Cuenta</th>
                                                        <th style="width:100%; text-align: center;">Descripción</th>
                                                        <th style="width:10%; text-align: center;"> Monto</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="contable" style="height: 100px;display: inline-block;width: 100%;overflow: auto;">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" align="center">
                                            <header>Distribución Presupuestaria</header><br>
                                            <div style="background-color:red; width:25px; height:20px;"></div> Sin disponibilidad presupuestaria
                                            <div style="background-color:green; width:25px; height:20px;"></div> Disponibilidad presupuestaria
                                            <div style="background-color:yellow; width:25px; height:20px;"></div> Disponibilidad presupuestaria (Tiene ordenes pendientes)
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped table-hover" style="width: 100%;display:block;overflow: auto;" id="partida">
                                                    <thead style="display: inline-block; width: 100%;">
                                                    <tr>
                                                        <th style="width:10px" class="codPartida">Partida</th>
                                                        <th style="width:100%; text-align: center;">Descripción</th>
                                                        <th style="width:10px; text-align: center;"> Monto</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="presupuesto" style="height: 100px;display: inline-block;width: 100%;overflow: auto;">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--end #tab5 -->
                                <ul class="pager wizard">
                                    <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                    <li class="previous"><a class="btn-raised" href="javascript:void(3);">Anterior</a></li>
                                    <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                    <li class="next"><a class="btn-raised" href="javascript:void(3);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col-lg-12 -->
        </div><!--end .row -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="glyphicon glyphicon-floppy-disk"></span>  Guardar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "80%");
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        var app = new  AppFunciones();

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                var arrayCheck = [''];
                var arrayMostrarOrden = [];
                if(dato['status']=='modificar'){
                    var mensaje = 'Modificada';
                    if(dato['estado']=="Revisar"){
                        mensaje = 'Revisada';
                    }else if(dato['estado']=="Conformar"){
                        mensaje = 'Conformada';
                    }else if(dato['estado']=="Aprobar"){
                        mensaje = 'Aprobada';
                    }
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Orden fue '+mensaje+' satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorPresupuesto'){
                    app.metValidarError(dato,'Disculpa. No existe disponibilidad para una de las partidas asociadas.');
                }else if(dato['status']=='errorPresupuesto2'){
                    app.metValidarError(dato,'Disculpa. La partida '+dato['cod_partida']+' no tiene suficiente disponibilidad');
                }else if(dato['status']=='errorIC'){
                    app.metValidarError(dato,'Disculpa. No se han seleccionado items/commodities');
                }else if(dato['status']=='nuevo'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Orden fue generada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        function calcularPartidas() {
            $('.porcentajeCuenta').val(0);
            $('.porcentajePartida').val(0);
            var mAfecto = 0;
            var mNoAfecto = 0;
            $('.ic').each(function(){
                var id = $(this).attr('idIC');
                var cantidad = $('#cantidad'+id).val();
                var precio = $('#precio'+id).val();
                var dPor = $('#descPorc'+id).val();
                var dFijo = $('#descFijo'+id).val();
                var descuento = 0;
                var monto = cantidad * precio;
//                var monto = $(this).val();
                var idCuenta = $('#cuentaIC'+id).val();
                var idPartida = $('#partidaIC'+id).val();
                var montoCuenta = $('#porcentajeCuenta'+idCuenta).val();
                var montoPartida = $('#porcentajePartida'+idPartida).val();
                var montoTotalCuenta = parseFloat(monto) + parseFloat(montoCuenta);
                var montoTotalPartida = parseFloat(monto) + parseFloat(montoPartida);
                $('#porcentajeCuenta'+idCuenta).val(montoTotalCuenta.toFixed(2));
                $('#porcentajePartida'+idPartida).val(montoTotalPartida.toFixed(2));

                var url2='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/verificarPartidaMET';

                $.post(url2, { idPartida: idPartida, monto: montoTotalPartida }, function (dato) {
                    if(dato['status']=='error'){
                        $('#idPartida'+idPartida+'Error').attr('style','background-color:red;');
                    } else if(dato['status']=='ok'){
                        $('#idPartida'+idPartida+'Error').attr('style','background-color:green;');
                    } else if(dato['status']=='ok2'){
                        $('#idPartida'+idPartida+'Error').attr('style','background-color:yellow;');
                    }
                },'json');

                if(dPor!=0 && dFijo==0){
                    descuento = (precio*dPor)/100;
                } else if(dPor==0 && dFijo!=0){
                    descuento = dFijo;
                }
                var totalMontoSinIva = cantidad * (precio - descuento);
                var exo = $('#exonerado'+id);
                if (exo.attr('checked')=="checked"){
                    mNoAfecto = parseFloat(mNoAfecto) + parseFloat(totalMontoSinIva);
                } else {
                    mAfecto = parseFloat(mAfecto) + parseFloat(totalMontoSinIva);
                }
            });

            var impuestos = mAfecto*0.12;
            var mBruto = parseFloat(mAfecto) + parseFloat(mNoAfecto);
            var mTotal = parseFloat(mBruto) + parseFloat(impuestos);

            $('#mAfecto').val(mAfecto.toFixed(2));
            $('#mNoAfecto').val(mNoAfecto.toFixed(2));
            $('#impuestos').val(impuestos.toFixed(2));
            $('#mBruto').val(mBruto.toFixed(2));
            $('#mTotal').val(mTotal.toFixed(2));
            $('#mPendiente').val(mTotal.toFixed(2));

            $('#idCuenta'+$('#cuentaIva').val()+'Error').remove();
            $('#idPartida'+$('#partidaIva').val()+'Error').remove();

            if(impuestos>0){
                var id = 1;
                $(document.getElementById('contable')).append(
                        '<tr id="idCuenta'+$('#cuentaIva').val()+'Error">' +
                        '<td style="width:20%">' +
                        '<input type="text" class="form-control" disabled value="'+$('#cuentaIva').attr('codigo')+'">'+
                        '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$('#cuentaIva').attr('nombre')+'"></td>'+
                        '<td style="width:10%"><input type="text" class="form-control" disabled value="'+impuestos.toFixed(2)+'"></td>'+
                        '</tr>');
                $(document.getElementById('presupuesto')).append(
                        '<tr id="idPartida'+$('#partidaIva').val()+'Error">' +
                        '<td style="width:20%">' +
                        '<input type="text" class="form-control" disabled value="'+$('#partidaIva').attr('codigo')+'">'+
                        '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$('#partidaIva').attr('nombre')+'"></td>'+
                        '<td style="width:10%"><input type="text" class="form-control" readonly name="form[int][montoPartida]['+$('#partidaIva').val()+']" value="'+impuestos.toFixed(2)+'"></td>'+
                        '</tr>');

                var url2='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/verificarPartidaMET';
                $.post(url2, { idPartida: $('#partidaIva').val(), monto: impuestos }, function (dato) {
                    if(dato['status']=='error'){
                        $('#idPartida'+$('#partidaIva').val()+'Error').attr('style','background-color:red;');
                    } else if(dato['status']=='ok'){
                        $('#idPartida'+$('#partidaIva').val()+'Error').attr('style','background-color:green;');
                    } else if(dato['status']=='ok2'){
                        $('#idPartida'+$('#partidaIva').val()+'Error').attr('style','background-color:yellow;');
                    }
                },'json');
            }
        }

        $('#dependencia').ready( function () {
            var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/buscarCentroCostoMET';
            $.post(url, { idDependencia: $('#dependencia').val() }, function (dato) {
                $('#centro_costoError > select > option').remove();
                $('#centro_costo').append('<option value=""></option>');

                for(var i=0; i<dato.length; i++){
                    if($('#cc').val()==dato[i]['pk_num_centro_costo'] ){
                        $('#centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'" selected>'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    } else {
                        $('#centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    }
                }
                $('#s2id_centro_costo .select2-chosen').html($('#centro_costo option:selected').text());
                $('#s2id_centro_costo a').removeClass('select2-default');

            },'json');

        });
/*
        $('#tipoOrden').on('change', function () {
            if($(this).val()=='OS'){
                $('#clasificacion option[value=6]').attr("selected",true);
                var id = 6;
            } else {
                $('#clasificacion option[value=1]').attr("selected",true);
                id = 1;
            }
            $('#s2id_clasificacion .select2-chosen').html($('#clasificacion option:selected').text());
            colocarAlmacen(id);
        });*/

        function colocarAlmacen(idClasificacion){

            if(idClasificacion==''){
                idClasificacion = 1;
            }
            $('#almacen > option').remove();
            var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/buscarClasificacionMET';
            $.post(url, { idClas: idClasificacion }, function (dato) {
                $('#almacen').append('<option value="'+dato['pk_num_almacen']+'" flag="'+dato['num_flag_commodity']+'" selected>'+dato['ind_descripcion']+'</option>');

                if($('#clasificacion option:selected').attr('flag')==1 && {$ver}!=1) {
                    $("#nuevoItem").attr('disabled', 'disabled');
                    $("#nuevoComm").removeAttr("disabled");
                } else if({$ver}!=1) {
                    $("#nuevoItem").removeAttr("disabled");
                    $("#nuevoComm").attr('disabled', 'disabled');
                }

                $('#s2id_almacen .select2-chosen').html($('#almacen option:selected').text());
                $('#s2id_almacen a').removeClass('select2-default');
            },'json');
        }

        $('#clasificacion').ready(function () {
            var id=$('#clasificacion').val();
            if($('#clasificacion option:selected').text()=='SERVICIOS'){
                $('#tipoOrden option[value=OS]').attr("selected",true);
                $('#tipoOrden option[value=OC]').remove();
            } else {
                $('#tipoOrden option[value=OS]').remove();
                $('#tipoOrden option[value=OC]').attr("selected",true);
            }
            $('#s2id_tipoOrden .select2-chosen').html($('#tipoOrden option:selected').text());
            colocarAlmacen(id);
        });

        $('#idProveedor').ready(function () {
            var idP=$('#idProveedor').val();

            var url='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/buscarServiciosMET';
            $('#tipo_servicio > option').remove();
            $.post(url, { id: idP}, function (dato) {
                var desde = 0;
                var hasta = dato.length;
                $(document.getElementById('tipo_servicio')).append(
                        '<option value=""></option>'
                );
                while(desde<hasta){
                    $(document.getElementById('tipo_servicio')).append(
                            '<option value="'+dato[desde]['pk_num_tipo_servico']+'">'+dato[desde]['ind_descripcion']+'</option>'
                    );
                    desde=desde+1;
                }
                $('#s2id_tipo_servicio .select2-chosen').html($('#tipo_servicio option:selected').text());
                $('#s2id_tipo_servicio a').removeClass('select2-default');
            },'json');
        });

        $('#tipo_servicio').on('change' ,function () {
            var iva = 0;
            var retencion = 0;
            var url2='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/buscarIvaRetencionMET';
            var id = $(this).val();
            $.post(url2, { id: id }, function (dato2) {
                if(dato2[0]['ind_signo']==1 && dato2[0]['ind_signo']!='') {
                    iva = dato2[0]['num_factor_porcentaje'];
                } else if(dato2[0]['ind_signo']==2 && dato2[0]['ind_signo']!='') {
                    retencion = dato2[0]['num_factor_porcentaje'];
                }
                if(dato2.length > 1 && dato2[1]['ind_signo']==1) {
                    iva = dato2[1]['num_factor_porcentaje'];
                } else if(dato2.length > 1 && dato2[1]['ind_signo']==2) {
                    retencion = dato2[1]['num_factor_porcentaje'];
                }
                $('#iva').val(iva);
                $('#retencion').val(retencion);
            },'json');

        });

        $('#item').ready(function () {

            var c=0;
            $('.cuenta').each(function ( titulo, valor ) {
                c=c+1;

                var id = $(this).val();
//                var porcentajeCuenta = $(this).attr('cantidad') * $(this).attr('precio');

                if($('#catidadCuentaIC'+id).length>0){
//                    var contadorCuenta = parseFloat($('#catidadCuentaIC'+id).val())+parseFloat(1);
//                    $('#catidadCuentaIC'+id).val(contadorCuenta);
                } else {
                    $(document.getElementById('contable')).append(
                            '<tr id="idCuenta'+id+'Error" class="idCuenta'+c+'Error">' +
                            '<td style="width:20%"><input type="hidden" class="form-control" id="catidadCuentaIC'+id+'" value="1">' +
                            '<input type="text" class="form-control codCuenta" idCuenta="'+id+'" id="codCuenta'+$(this).attr('codCuenta')+'" disabled value="'+$(this).attr('codCuenta')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$(this).attr('descCuenta')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control porcentajeCuenta" disabled value="0" id="porcentajeCuenta'+id+'"></td>'+
                            '</tr>');
                }
            });

            c=0;
            $('.partida').each(function ( titulo, valor ) {
                c=c+1;
                var id = $(this).val();
//                var porcentajePartida = $(this).attr('cantidad') * $(this).attr('precio');
//                $('#porcentajePartida'+id).val(porcentajePartida);
                if($('#catidadPartidaIC'+id).length>0){
//                    var contadorPartida = parseFloat($('#catidadPartidaIC'+id).val())+parseFloat(1);
//                    $('#catidadPartidaIC'+id).val(contadorPartida);
                } else {
                    $(document.getElementById('presupuesto')).append(
                            '<tr id="idPartida'+id+'Error" class="idPartida'+c+'Error">' +
                            '<td style="width:20%"><input type="hidden" class="form-control" id="catidadPartidaIC'+id+'" value="1">' +
                            '<input type="text" class="form-control codPartida" idPartida="'+id+'" id="codPartida'+$(this).attr('codPartida')+'" disabled value="'+$(this).attr('codPartida')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$(this).attr('descPartida')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control porcentajePartida" readonly name="form[int][montoPartida]['+id+']" value="0" id="porcentajePartida'+id+'"></td>'+
                            '</tr>');
                }

            });


        });

        $('#rootwizard1').ready(function () {
            calcularPartidas();
        });
    });


</script>