<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Generar Ordenes Pendientes</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Procedimiento</th>
                            <th>Proveedor</th>
                            <th>Razón Social</th>
                            <th>Monto</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {

        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/compras/generarOrdenCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_codigo_acta" },
                    { "data": "pk_num_proveedor" },
                    { "data": "nombre" },
                    { "data": "montoTotal"},
                    { "data": "adjudicacion"},
                    { "orderable": false,"data": "acciones", width:150}
                ]
        );

        var url='{$_Parametros.url}modLG/compras/generarOrdenCONTROL/generarOrdenMET';
        $('#dataTablaJson').on('click','.generar',function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { clasificacion: $(this).attr('clasificacion'), idAdj: $(this).attr('idAdj'), estado: $(this).attr('estado') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        var url2='{$_Parametros.url}modLG/compras/generarOrdenCONTROL/asignarOrdenMET';
        $('#dataTablaJson').on('click','.asignar',function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url2, { idAdj: $(this).attr('idAdj') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
    });
</script>