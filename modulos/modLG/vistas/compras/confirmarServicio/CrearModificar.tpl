<form action="{$_Parametros.url}modLG/compras/confirmarServicioCONTROL/crearConfirmacionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idOrden)}{$idOrden}{/if}" name="idOrden"/>
    <input type="hidden" value="{if isset($idDet)}{$idDet}{/if}" name="idDet"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group col-lg-4">
                    <input type="text" readonly class="form-control" id="numero" value="{if isset($formDB.ind_orden)}{$formDB.ind_orden}{/if}" name="form[int][numero]">

                    <label for="numero"><i class="md md-border-color"></i>Número: </label>
                </div>
                <div class="form-group col-lg-2" id="secuenciaError">
                    <input type="text" readonly class="form-control" id="secuencia" value="{if isset($secuencia)}{$secuencia}{/if}" name="form[int][secuencia]" size="2">
                </div>
                <div class="form-group col-lg-6" id="totalError">
                    <input type="text" class="form-control" readonly id="total" value="{if isset($formDBDet.num_cantidad)}{$formDBDet.num_cantidad}{/if}" name="form[txt][total]">
                    <label for="total"><i class="md md-border-color"></i>Total: </label>
                </div>
                <div class="form-group col-lg-6" id="esperadaError">
                    {if isset($formDB.fec_creacion)}
                        {$dia=substr($formDB.fec_creacion,8,2)}
                        {$mes=substr($formDB.fec_creacion,5,2)}
                        {$anio=substr($formDB.fec_creacion,0,4)}
                        {$fecha="$anio-$mes-$dia"}
                        {else}
                        {$fecha="0000-00-00"}
                    {/if}
                    <input type="text" class="form-control" value="{$fecha}" disabled>
                    <label for="esperada"><i class="md md-border-color"></i>Fecha Esperada: </label>
                </div>

                <div class="form-group col-lg-6" id="recibidaError">
                    <input type="text" readonly class="form-control" name="form[int][recibida]" id="recibida" value="{if isset($formDBDet.recibida)}{$formDBDet.recibida}{else}0{/if}">
                    <label for="recibida"><i class="md md-border-color"></i>Recibida: </label>
                </div>
                <div class="form-group col-lg-6" id="terminoError">
                    <input type="text" class="form-control" id="termino" value="{$fecha}" name="form[txt][termino]">
                    <label for="termino"><i class="md md-border-color"></i>Fecha Termino Real: </label>
                </div>

                <div class="form-group col-lg-6" id="recibirError">
                    <input type="text" class="form-control" id="recibir" name="form[int][recibir]" onfocus="$(this).val('')" value="{if isset($formDBDet.recibida) AND isset($formDBDet.num_cantidad)}{$formDBDet.num_cantidad-$formDBDet.recibida}{else}{$formDBDet.num_cantidad}{/if}">
                    <label for="recibir"><i class="md md-border-color"></i>Por Recibir: </label>
                </div>

                <div class="form-group col-lg-6" id="saldoError">
                </div>

                <div class="form-group col-lg-6" id="saldoError">
                    <input type="text" class="form-control" id="saldo" value="0" disabled>
                    <label for="saldo"><i class="md md-border-color"></i> Nuevo Saldo: </label>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="icm icm-stack-checkmark"></span>  Confirmar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "80%");
    $("#formAjax").submit(function(){
        return false;
    });
    $('#termino').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#modalAncho').css("width", "40%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. No se puede Conformar el Servicio');
                }else if(dato['status']=='nuevo'){
                    swal("Registro Guardado!", 'Se ha Confirmado el Servicio satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('idConfirmacionesdet'+dato['idDet'])).remove();
                    if(dato['quitarOrden']!=0){
                        $(document.getElementById('idConfirmaciones'+dato['quitarOrden'])).remove();
                    }
                }
            }, 'json');
        });

        $('#recibir').ready(function(){
            var recibir = $('#recibir').val();
            var recibida = $('#recibida').val();
            var saldo = parseFloat(recibida)+parseFloat(recibir);
            $('#saldo').val(saldo.toFixed(2));
        });

        $('#recibir').change(function(){
            var cantidad = $('#total').val();
            var recibir = $(this).val();
            var recibida = $('#recibida').val();
            var saldo = parseFloat(recibida)+parseFloat(recibir);
            if(saldo>cantidad){
                saldo=cantidad;
                recibir = parseFloat(saldo)-parseFloat(recibida);
            }
            $('#recibir').val(recibir.toFixed(2));
            $('#saldo').val(saldo.toFixed(2));
        });
    });
</script>