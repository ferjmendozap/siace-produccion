<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Informe de Adjudicación - Informe Recomendación</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="15%">CÓDIGO</th>
                            <th width="30%">ASUNTO</th>
                            <th width="30%">OBJETO</th>
                            <th>PROVEEDOR RECOMENDADO</th>
                            <th width="10%">Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/compras/adjudicacionCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_cod_evaluacion", width:150 },
                    { "data": "ind_asunto", width:250 },
                    { "data": "ind_objeto_consulta", width:250 },
                    { "data": "nombre", width:250 },
                    { "orderable": false,"data": "acciones", width:100}
                ]
        );
        var url = '{$_Parametros.url}modLG/compras/adjudicacionCONTROL/crearModificarAdjudicacionMET';
        $('#dataTablaJson tbody').on( 'click', '.accion', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { secuencia: $(this).attr('secuencia'), idInfor: $(this).attr('idInfor'), tipoAdj: $(this).attr('tipoAdj'), idProveedor: $(this).attr('idProveedor'), opcion:'crear' }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { secuencia: $(this).attr('secuencia'), idInfor: $(this).attr('idInfor'), tipoAdj: $(this).attr('tipoAdj'), idProveedor: $(this).attr('idProveedor'), idAdj: $(this).attr('idAdj'), opcion:'modificar'}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.imprimir', function () {
            var url = '{$_Parametros.url}modLG/compras/adjudicacionCONTROL/generarInformeAdjudicacionMET';
            $.post(url, { idInfor: $(this).attr('idInfor') }, function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });
    });
</script>