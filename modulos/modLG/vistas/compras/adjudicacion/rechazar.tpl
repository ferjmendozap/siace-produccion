<form action="{$_Parametros.url}modLG/compras/adjudicacionCONTROL/rechazarMET" id="formAjax2" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idInfor)}{$idInfor}{/if}" name="idInfor"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                <div class="col-lg-12">
                    <div class="form-group floating-label col-lg-12" id="motivoError">
                        <textarea id="motivo" class="form-control" cols="50" rows="2" name="motivo">{if isset($eval.ind_conclusion)}{$eval.ind_conclusion}{/if}</textarea>
                        <label for="motivo"><i class="md md-border-color"></i>
                            Motivo del Rechazo
                        </label>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-danger ink-reaction btn-raised" id="accion2">
                <span class="icm icm-blocked"></span>
            Rechazar
        </button>
    </div>
</form>

<script type="text/javascript">
    $("#formAjax2").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();

        $('#accion2').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax2").attr("action"), $( "#formAjax2" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Informe fue Anulado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');
                }
            }, 'json');
        });
    });
</script>