<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Gestión de Vacaciones. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios 
class vacacionesControlador extends Controlador
{
	private $atVacacionModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atVacacionModelo = $this->metCargarModelo('vacaciones', 'gestion');
	}

	public function metIndex()
    {
        
    }

	// Index del controlador
	public function metListarVacaciones($estado)
	{
        $complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select',
		);
		$js = array('materialSiace/core/demo/DemoTableDynamic',  'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$listadoOrganismo = $this->atVacacionModelo->metListarOrganismo(0, 1);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atVacacionModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
		$pkNumDependencia = $datosEmpleado['pk_num_dependencia'];
		$listadoEmpleados = $this->atVacacionModelo->metDependenciaEmpleado($pkNumDependencia);
		$this->atVista->assign('empleado', $listadoEmpleados);
		$listadoDependencia = $this->atVacacionModelo->metListarDependencia($datosEmpleado['pk_num_organismo'], 1, 0);
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		// Organismo donde trabaja el empleado
		$empleado = array(
			'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'ind_estado' => $estado
		);
		$this->atVista->assign('emp', $empleado);
		$empleado = $datosEmpleado['ind_nombre1'].' '.$datosEmpleado['ind_nombre2'].' '.$datosEmpleado['ind_apellido1'].' '.$datosEmpleado['ind_apellido2'];
		// Filtro Restringido
		$busqueda = array(
			'ind_descripcion_empresa' => $datosEmpleado['ind_descripcion_empresa'],
			'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
			'pk_num_dependencia' => $datosEmpleado['pk_num_dependencia'],
			'ind_dependencia' => $datosEmpleado['ind_dependencia'],
			'empleado' => $empleado,
			'pk_num_empleado' => $datosEmpleado['pk_num_empleado']
		);
		$this->atVista->assign('busqueda', $busqueda);
		$this->atVista->assign('estado', $estado);
		$this->atVista->assign('periodoVacacion', $this->atVacacionModelo->metListarVacacion($pkNumEmpleado, $estado));
		$this->atVista->metRenderizar('listado');
	}

	// Este método permite crear una nueva solicitud de vacaciones
	public function metNuevaSolicitud()
	{
        $valido = $this->metObtenerInt('valido');
		$usuario = Session::metObtener('idUsuario');
		if (isset($valido) && $valido == 1) {
			$pkNumEmpleado = $_POST['pk_num_empleado'];
			$fecha_salida = $_POST['fecha_salida'];
			$fechaTermino = $_POST['fechaTermino'];
			$fechaIncorporacion = $_POST['fechaIncorporacion'];
			$tipo_vacacion = $_POST['tipo_vacacion'];
			//$periodo_total = $_POST['periodoTotal'];
			$num_dias = $_POST['num_dias'];
			$ind_motivo = $_POST['ind_motivo'];
			$fecha_hora = date('Y-m-d H:i:s');
			$fechaSolicitud = date('Y-m-d');
			// Cargo del empleado
			$cargoEmpleado = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
			$cargo = $cargoEmpleado['fk_rhc063_num_puestos_cargo'];
			$pkNumDependencia = $cargoEmpleado['pk_num_dependencia'];
			// Guardar solicitud
			$pkNumSolicitudVacacion = $this->atVacacionModelo->metGuardarVacacionSolicitud($pkNumEmpleado, $usuario, $fecha_hora, $fecha_salida, $tipo_vacacion, $cargo, $fechaTermino, $fechaIncorporacion, $pkNumDependencia, $ind_motivo, $num_dias, $fechaSolicitud, 1, 0);
			// Guardar detalle de la solicitud
			$this->metActualizarDetalle($pkNumEmpleado, $pkNumSolicitudVacacion, $num_dias, $usuario, $fecha_hora, $fecha_salida);
            $verSolicitud = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);
			$datos = array(
				'pk_num_solicitud_vacacion' => $pkNumSolicitudVacacion,
                'fecha_solicitud' => $verSolicitud['fecha_solicitud'],
                'ind_nombre_detalle' => $verSolicitud['ind_nombre_detalle'],
                'nombreCompleto' => $verSolicitud['ind_nombre1'].' '.$verSolicitud['ind_nombre2'].' '.$verSolicitud['ind_apellido1'].' '.$verSolicitud['ind_apellido2']
			);
			echo json_encode($datos);
			exit;
		}
		$obtenerEmpleado = $this->atVacacionModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
		$numEstatus = $obtenerEmpleado['num_estatus'];

		if($numEstatus==0){
			$dias = array(
				'diasPendientes' => 0,
				'fechaTermino' => null,
				'fechaIncorporacion' => null,
				'estado' => 'inactivo',
				'numEstatus' => 0
			);

			$this->atVista->assign('dias', $dias);
		} else {
			$datosEmpleado = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
			$empleadoDato = array(
				'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
				'pk_num_dependencia' => $datosEmpleado['pk_num_dependencia'],
				'pk_num_empleado' => $pkNumEmpleado,
				'ind_nombre1' => $datosEmpleado['ind_nombre1'],
				'ind_nombre2' => $datosEmpleado['ind_nombre2'],
				'ind_apellido1' => $datosEmpleado['ind_apellido1'],
				'ind_apellido2' => $datosEmpleado['ind_apellido2'],
				'ind_descripcion_empresa' => $datosEmpleado['ind_descripcion_empresa'],
				'ind_dependencia' => $datosEmpleado['ind_dependencia'],
				'cedula' => $datosEmpleado['ind_cedula_documento']

			);

			$periodoVacacion = $this->metPeriodoVacacion($pkNumEmpleado, $usuario);

			$tipoVacacion = $this->atVacacionModelo->metMostrarSelect('TIPVAC');
			$this->atVista->assign('tipoVacacion', $tipoVacacion);
			$this->atVista->assign('emp', $empleadoDato);
			$this->atVista->assign('periodoVacacion', $periodoVacacion);
			// Verifico si el empleado cumple con el tiempo requerido para solicitar disfrute de vacaciones
			$ingreso = $this->metObtenerFechaIngreso($pkNumEmpleado);
			$fechaIngreso = $ingreso['fechaIngresoPrevio'];
			$fechaExplode = explode("-", $fechaIngreso);
			$anioIngreso = $fechaExplode[0];
			$fechaActual = date('Y-m-d');
			$explodeActual = explode("-", $fechaActual);
			$anioActual = $explodeActual[0];
			// Fin
			$totalPeriodo = count($periodoVacacion);

			if ($anioActual > $anioIngreso) {
				if ($fechaActual >= $fechaIngreso) {
					// Calculo las fechas del periodo
					if ($totalPeriodo > 0) {
						$fechasPeriodo = $this->metGenerarFechas($pkNumEmpleado);
						$a = 1;
						foreach ($fechasPeriodo as $fecha) {
							$fechaExplode = explode("/", $fecha);
							$fechaInicial[$a] = $fechaExplode['0'];
							$fechaFinal[$a] = $fechaExplode['1'];
							$a++;
						}
						// Calculo los dias pendientes del periodo
						$acumuladorDias = 0;
						foreach ($periodoVacacion as $periodo) {
							$pendiente = $periodo['num_pendientes'];
							if ($pendiente == 0) {
								$diasConsulta = 0;
							} else {
								$diasConsulta = $periodo['num_pendientes'];
							}
							$acumuladorDias = $acumuladorDias + $diasConsulta;
						}

						// Calculo las fechas de termino e incorporación correspondiente a los dias pendientes por disfrute de vacaciones

						$fechaSalida = date('d/m/Y');
						if ($acumuladorDias > 0) {
							$procesarPeriodo = $this->metProcesarPeriodos($fechaSalida, $acumuladorDias);
							$fechaTermino = $procesarPeriodo['fechaTermino'];
							$fechaIncorporacion = $procesarPeriodo['fechaIncorporacion'];

						} else {
							$fechaTermino = '';
							$fechaIncorporacion = '';
						}

						$dias = array(
							'diasPendientes' => $acumuladorDias,
							'fechaTermino' => $fechaTermino,
							'fechaIncorporacion' => $fechaIncorporacion,
							'estado' => 'activo',
							'numEstatus' => 1
						);


						$this->atVista->assign('fechaInicial', $fechaInicial);
						$this->atVista->assign('fechaFinal', $fechaFinal);
					} else {
						$dias = array(
							'diasPendientes' => 0,
							'fechaTermino' => null,
							'fechaIncorporacion' => null,
							'estado' => 'inactivo',
							'numEstatus' => 1
						);
					}
				}
			} else {

				$dias = array(
					'diasPendientes' => 0,
					'fechaTermino' => null,
					'fechaIncorporacion' => null,
					'estado' => 'inactivo',
					'numEstatus' => 1
				);
			}
			$this->atVista->assign('dias', $dias);
			$comprobarSolicitud = $this->metComprobarSolicitud($pkNumEmpleado);
			$datosComprobar = array('datoComprobar' => $comprobarSolicitud);
			$this->atVista->assign('comprobar', $datosComprobar);
			$listarEmpleado = $this->atVacacionModelo->metListadoEmpleado();
			$this->atVista->assign('listarEmpleado', $listarEmpleado);
			$listarDependencia = $this->atVacacionModelo->metListadoDependencia();
			$this->atVista->assign('listarDependencia', $listarDependencia);
			$listarOrganismo = $this->atVacacionModelo->metListarOrganismo(0, 1);
			$this->atVista->assign('listarOrganismo', $listarOrganismo);
            $this->atVista->assign('periodo', $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado,7));
		}
		$this->atVista->metRenderizar('nuevaSolicitud', 'modales');
	}

	#para actualizar peridos del funcionario (select)
    public function metObtenerPeriodosSelect()
    {
        #obtengo el valor pasado del formulario
        $empleado = $this->metObtenerInt('empleado');

        #conecto con el modelo y consulto los datos
        $datos = $this->atVacacionModelo->metListarPeriodo($empleado,8);

        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['num_pendientes'].">".$datos[$i]['num_anio_ant']."-".$datos[$i]['num_anio']." (".$datos[$i]['num_pendientes'].")</option>";
        }

        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;

    }

	// Método que permite obtener la fecha de ingreso del funcionario
	public function metObtenerFechaIngreso($pkNumEmpleado)
	{
		// Verifico el parámetro de la base de datos para aperturar el periodo, es decir, en caso de realizarse cuando el empleado cumpla el tiempo requerido o para ser aperturado un mes antes.
		$aperturaPeriodo = Session::metObtener('PERIODOVAC');
		$vacacionDerecho = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
		$fechaIngreso1 = $vacacionDerecho['fec_ingreso'];

		if($aperturaPeriodo==1){
			$fechaIngresoPrevio = $fechaIngreso1;
		} else {
			$nuevafecha = strtotime('-30 day', strtotime($fechaIngreso1));
			$fechaIngresoPrevio = date('Y-m-d', $nuevafecha);
		}
		$fechaExplode = explode("-", $fechaIngresoPrevio);
		$anioIngreso = $fechaExplode[0];
		$mesIngreso = $fechaExplode[1];
		$diaIngreso = $fechaExplode[2];
		$ingreso = array(
			'anioIngreso' => $anioIngreso,
			'mesIngreso' => $mesIngreso,
			'diaIngreso' => $diaIngreso,
			'fechaIngresoPrevio' => $fechaIngresoPrevio,
			'fechaIngreso' => $fechaIngreso1
		);

		return $ingreso;
	}

	// Actualizo los periodos del funcionario
	public function metPeriodoVacacion($pkNumEmpleado, $usuario)
	{
		// Fecha Actual
		$fecha_hora = date('Y-m-d H:i:s');
		$ingreso = $this->metObtenerFechaIngreso($pkNumEmpleado);
		$anioIngreso = $ingreso['anioIngreso'];
		$mesIngreso = $ingreso['mesIngreso'];
		$diaIngreso = $ingreso['diaIngreso'];
		$fechaIngreso = $ingreso['fechaIngreso'];
		$fechaExplode = explode("-", $fechaIngreso);
		$mes = $fechaExplode['1'];
		$fechaActual = date('Y-m-d');
		$anioActual = date('Y');
		$aperturaPeriodo = Session::metObtener('PERIODOVAC');
		$consultaDerecho = $this->metDiasDerechoConsultar($fechaIngreso, $pkNumEmpleado);
		$anioDerecho = $consultaDerecho['anioTotal'];
		$adelanto = Session::metObtener('ADELANTOVAC');

		if($aperturaPeriodo==1) {
			if ($anioDerecho > 0) {
				$anioCorriente = $anioIngreso;

				for ($i = $anioIngreso; $i <= $anioActual; $i++) {
					$fechaComparar = $anioCorriente . '-' . $mes . '-' . $diaIngreso;
                    $a = $this->metObtenerFecha($adelanto,$fechaComparar,'-');
					$fechaComparar = $a['fecha'];

					if ($anioActual > $anioCorriente) {
						$comprobarAnioPrevio = $this->metTiempoServicioVacacion($fechaComparar, $fechaActual);
						$conprobarAnio = $comprobarAnioPrevio['anio'];


						if ($conprobarAnio > 0) {
							// Consulto si el empleado tiene un periodo aperturado
							$anioFinal = $anioCorriente + 1;

							$periodoApertura = $this->atVacacionModelo->metPeriodo($pkNumEmpleado, $mesIngreso, $anioFinal, 2);

							$contador = count($periodoApertura);
                                if ($contador == 0) {
								$explode = explode("-", $fechaComparar);
								$anioExplode = $explode[0]+1;
								$mesExplode = $explode[1];
								$diaExplode = $explode[2];
								$fechaCompararCompleta = $anioExplode . '-' . $mesExplode . '-' . $diaExplode;

								$diasDerecho = $this->metDiasDerecho($fechaCompararCompleta, $pkNumEmpleado);
								$derecho = $diasDerecho['dias_derecho'];
								if ($derecho > 0) {
									$this->atVacacionModelo->metGuardarPeriodo($pkNumEmpleado, $usuario, $mesIngreso, $anioFinal, $fecha_hora, $derecho, 0, 0, 0, 1, $derecho);
								}

							}

						}

					}
					$anioCorriente++;
				}
			}
		} else {
			if ($anioDerecho == 0) {
				$ingreso = $this->metObtenerFechaIngreso($pkNumEmpleado);
				$fechaIngresoComparar = $ingreso['fechaIngresoPrevio'];

				$periodoApertura = $this->atVacacionModelo->metPeriodo($pkNumEmpleado, $mes, $anioActual, 2);
				$contador = count($periodoApertura);
				if ($contador == 0) {
					$diasDerecho = $this->metDiasDerechoConsultar($fechaIngresoComparar, $pkNumEmpleado);
					$derecho = $diasDerecho['dias_derecho'];
					if ($derecho > 0) {
						$this->atVacacionModelo->metGuardarPeriodo($pkNumEmpleado, $usuario, $mes, $anioActual, $fecha_hora, $derecho, 0, 0, 0, 1, $derecho);
					}
				}
			} else {
				$anioCorriente = $anioIngreso;
				for ($i = $anioIngreso; $i <= $anioActual; $i++) {
					$fechaComparar = $anioCorriente . '-' . $mes . '-' . $diaIngreso;
					if ($anioActual > $anioCorriente) {
						$comprobarAnioPrevio = $this->metTiempoServicioVacacion($fechaComparar, $fechaActual);
						$conprobarAnio = $comprobarAnioPrevio['anio'];
						if ($conprobarAnio > 0) {
							// Consulto si el empleado tiene un periodo aperturado
							$anioFinal = $anioCorriente + 1;
							$periodoApertura = $this->atVacacionModelo->metPeriodo($pkNumEmpleado, $mes, $anioFinal, 2);
							$contador = count($periodoApertura);
							if ($contador == 0) {
								$explode = explode("-", $fechaComparar);
								$anioExplode = $explode[0] + 1;
								$mesExplode = $explode[1];
								$diaExplode = $explode[2];
								$fechaCompararCompleta = $anioExplode . '-' . $mesExplode . '-' . $diaExplode;
								$diasDerecho = $this->metDiasDerecho($fechaCompararCompleta, $pkNumEmpleado);
								$derecho = $diasDerecho['dias_derecho'];
								if ($derecho > 0) {
									$this->atVacacionModelo->metGuardarPeriodo($pkNumEmpleado, $usuario, $mes, $anioFinal, $fecha_hora, $derecho, 0, 0, 0, 1, $derecho);
								}
							}
						}

					}
					$anioCorriente++;
				}
			}
		}
		$periodoVacacion = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 6);
		return $periodoVacacion;

	}

	// Actualizo los periodos de todos los funcionarios
	public function metActualizarPeriodoGeneral()
	{
		// Fecha Actual
		$fecha_hora = date('Y-m-d H:i:s');
		$usuario = Session::metObtener('idUsuario');
		// Consulto el listado de los empleados
		$listado = $this->atVacacionModelo->metListarEmpleado(0, 2);
		foreach($listado as $listado) {
			$pkNumEmpleado = $listado['pk_num_empleado'];
			// Consulto si el empleado tiene derecho a vacaciones
			$ingreso = $this->metObtenerFechaIngreso($pkNumEmpleado);
			$diaIngreso = $ingreso['diaIngreso'];
			$mesIngreso = $ingreso['mesIngreso'];
			$anioIngreso = $ingreso['anioIngreso'];
			$fechaIngreso = $ingreso['fechaIngreso'];
			$fechaExplode = explode("-", $fechaIngreso);
			$mes = $fechaExplode['1'];
			$fechaActual = date('Y-m-d');
			$anioActual = date('Y');
			$aperturaPeriodo = Session::metObtener('PERIODOVAC');
			$consultaDerecho = $this->metDiasDerechoConsultar($fechaIngreso, $pkNumEmpleado);
			$anioDerecho = $consultaDerecho['anioTotal'];
			if($aperturaPeriodo==1) {
				if ($anioDerecho > 0) {
					$anioCorriente = $anioIngreso;
					for ($i = $anioIngreso; $i <= $anioActual; $i++) {
						$fechaComparar = $anioCorriente . '-' . $mes . '-' . $diaIngreso;
						if ($anioActual > $anioCorriente) {
							$comprobarAnioPrevio = $this->metTiempoServicioVacacion($fechaComparar, $fechaActual);
							$conprobarAnio = $comprobarAnioPrevio['anio'];
							if ($conprobarAnio > 0) {
								// Consulto si el empleado tiene un periodo aperturado
								$anioFinal = $anioCorriente + 1;
								$periodoApertura = $this->atVacacionModelo->metPeriodo($pkNumEmpleado, $mesIngreso, $anioFinal, 1);
								$contador = count($periodoApertura);
								if ($contador == 0) {
									$explode = explode("-", $fechaComparar);
									$anioExplode = $explode[0] + 1;
									$mesExplode = $explode[1];
									$diaExplode = $explode[2];
									$fechaCompararCompleta = $anioExplode . '-' . $mesExplode . '-' . $diaExplode;
									$diasDerecho = $this->metDiasDerecho($fechaCompararCompleta, $pkNumEmpleado);
									$derecho = $diasDerecho['dias_derecho'];
									if ($derecho > 0) {
										$this->atVacacionModelo->metGuardarPeriodo($pkNumEmpleado, $usuario, $mesIngreso, $anioFinal, $fecha_hora, $derecho, 0, 0, 0, 1, $derecho);
									}
								}
							}
						}
						$anioCorriente++;
					}
				}
			} else {
				if ($anioDerecho == 0) {
					$ingreso = $this->metObtenerFechaIngreso($pkNumEmpleado);
					$fechaIngresoComparar = $ingreso['fechaIngresoPrevio'];

					$periodoApertura = $this->atVacacionModelo->metPeriodo($pkNumEmpleado, $mes, $anioActual, 2);
					$contador = count($periodoApertura);
					if ($contador == 0) {
						$diasDerecho = $this->metDiasDerechoConsultar($fechaIngresoComparar, $pkNumEmpleado);
						$derecho = $diasDerecho['dias_derecho'];
						if ($derecho > 0) {
							$this->atVacacionModelo->metGuardarPeriodo($pkNumEmpleado, $usuario, $mes, $anioActual, $fecha_hora, $derecho, 0, 0, 0, 1, $derecho);
						}
					}
				} else {
					$anioCorriente = $anioIngreso;
					for ($i = $anioIngreso; $i <= $anioActual; $i++) {
						$fechaComparar = $anioCorriente . '-' . $mes . '-' . $diaIngreso;
						if ($anioActual > $anioCorriente) {
							$comprobarAnioPrevio = $this->metTiempoServicioVacacion($fechaComparar, $fechaActual);
							$conprobarAnio = $comprobarAnioPrevio['anio'];
							if ($conprobarAnio > 0) {
								// Consulto si el empleado tiene un periodo aperturado
								$anioFinal = $anioCorriente + 1;
								$periodoApertura = $this->atVacacionModelo->metPeriodo($pkNumEmpleado, $mes, $anioFinal, 2);
								$contador = count($periodoApertura);
								if ($contador == 0) {
									$explode = explode("-", $fechaComparar);
									$anioExplode = $explode[0] + 1;
									$mesExplode = $explode[1];
									$diaExplode = $explode[2];
									$fechaCompararCompleta = $anioExplode . '-' . $mesExplode . '-' . $diaExplode;
									$diasDerecho = $this->metDiasDerecho($fechaCompararCompleta, $pkNumEmpleado);
									$derecho = $diasDerecho['dias_derecho'];
									if ($derecho > 0) {
										$this->atVacacionModelo->metGuardarPeriodo($pkNumEmpleado, $usuario, $mes, $anioFinal, $fecha_hora, $derecho, 0, 0, 0, 1, $derecho);
									}
								}
							}
						}
						$anioCorriente++;
					}
				}
			}
		}
	}

	// Verifico los dias de derecho a vacaciones que posee el empleado
	public function metDiasDerecho($fechaIngresoFin, $pkNumEmpleado)
	{
		// Se verifica el parámetro activo para el manejo de las vacaciones
		//$manejoVacaciones = Session::metObtener('MANVAC');
		$ingreso = $this->metObtenerFechaIngreso($pkNumEmpleado);
		$fechaIngresoInicio = $ingreso['fechaIngreso'];

		$tiempoServicio = $this->metTiempoServicioVacacion($fechaIngresoInicio, $fechaIngresoFin);

		$anioTotalPrevio = $tiempoServicio['anio'];

		// Consulto si el empleado tiene Antecedentes de servicio *********
		$antecedente = $this->atVacacionModelo->metConsultarAntecedente($pkNumEmpleado);
		$acumuladorAntecedente = 0;
        $acumuladoMeses = 0;

		foreach ($antecedente as $ant){
			$fechaIngreso = $ant['fec_ingreso'];
			$fechaEgreso = $ant['fec_egreso'];
			$fechaInicial = new DateTime($fechaIngreso);
			$fechaFinal = new DateTime($fechaEgreso);
			$interval = $fechaInicial->diff($fechaFinal);
			$servicio = $interval->format('%y');
            $acumuladoMeses = $acumuladoMeses+$interval->format('%m');
			$acumuladorAntecedente  = $acumuladorAntecedente + $servicio;
		}
        $acumuladoMeses = $acumuladoMeses/12;

		// Fin antecedente de servicio
		$anioTotal = $anioTotalPrevio + $acumuladorAntecedente+(int)$acumuladoMeses;


		// Consulto la tabla de vacaciones dependendiendo del año total del funcionario


		$consultarNomina = $this->atVacacionModelo->metConsultarNomina($pkNumEmpleado);
		$pkNumTipoNomina = $consultarNomina['fk_nmb001_num_tipo_nomina'];
		$consultarTabla = $this->atVacacionModelo->metConsultarTabla($anioTotal, $pkNumTipoNomina);

		if($consultarTabla['num_dias_disfrute']>0){
			$derecho = $consultarTabla['num_dias_disfrute'];
			$diasAdicionales = $consultarTabla['num_dias_adicionales'];
			$diasDerecho = $consultarTabla['num_total_disfrutar'];
		} else {
			$derecho = 0;
			$diasAdicionales = 0;
			$diasDerecho = 0;
		}
		/*
		if($manejoVacaciones=='quinquenio'){
			/* Evalúo el quinquenio
			 1 - 5 = 15
			 6 - 10 = 20
			11 - 15 = 25
			16 en adelante = 30


			if (($anioTotal >= 1) && ($anioTotal <= 5)) {
				$diasAdicionales = 0;
				$derecho = 15;
				$diasDerecho = $derecho + $diasAdicionales;
			}
			if (($anioTotal >= 6) && ($anioTotal <= 10)) {
				$diasAdicionales = 0;
				$derecho = 20;
				$diasDerecho = $derecho + $diasAdicionales;
			}
			if (($anioTotal >= 11) && ($anioTotal <= 15)) {
				$diasAdicionales = 0;
				$derecho = 25;
				$diasDerecho = $derecho + $diasAdicionales;
			}
			if ($anioTotal > 16) {
				$diasAdicionales = 0;
				$derecho = 30;
				$diasDerecho = $derecho + $diasAdicionales;
			}
		} else {

			if($anioTotal>1){
				$derecho = 15;
				$diasAdicionales = $anioTotal-1;
				$diasDerecho = $derecho + $diasAdicionales;
			} else {
				$diasAdicionales = 0;
				$derecho = 15;
				$diasDerecho = $derecho + $diasAdicionales;
			}
		}
			*/
		$dato = array(
			'dias_derecho' => $diasDerecho,
			'dia_adicional' => $diasAdicionales,
			'derecho' => $derecho
		);
		return $dato;
	}


	// Verifico los dias de derecho a vacaciones que posee el empleado
	public function metDiasDerechoConsultar($fechaIngreso, $pkNumEmpleado)
	{
		// Se verifica el parámetro activo para el manejo de las vacaciones
		//$manejoVacaciones = Session::metObtener('MANVAC');


		$tiempoServicio = $this->metTiempoServicio($fechaIngreso);
		$anioTotalPrevio = $tiempoServicio['anio'];
		// Consulto si el empleado tiene Antecedentes de servicio *********
		$antecedente = $this->atVacacionModelo->metConsultarAntecedente($pkNumEmpleado);
		$acumuladorAntecedente = 0;
        $acumuladoMeses = 0;
		foreach ($antecedente as $ant){
			$fechaIngreso = $ant['fec_ingreso'];
			$fechaEgreso = $ant['fec_egreso'];
			$fechaInicial = new DateTime($fechaIngreso);
			$fechaFinal = new DateTime($fechaEgreso);
			$interval = $fechaInicial->diff($fechaFinal);
			$servicio = $interval->format('%y');
            $acumuladoMeses = $acumuladoMeses+$interval->format('%m');
            $acumuladorAntecedente = $acumuladorAntecedente + $servicio;

		}
        $acumuladoMeses = $acumuladoMeses/12;
		// Fin antecedente de servicio
		$anioTotal = $anioTotalPrevio + $acumuladorAntecedente + (int)$acumuladoMeses;
		// Consulto la tabla de vacaciones dependendiendo del año total del funcionario
		$consultarNomina = $this->atVacacionModelo->metConsultarNomina($pkNumEmpleado);
		$pkNumTipoNomina = $consultarNomina['fk_nmb001_num_tipo_nomina'];
		$consultarTabla = $this->atVacacionModelo->metConsultarTabla($anioTotal, $pkNumTipoNomina);
		if($consultarTabla['num_dias_disfrute']>0){
			$derecho = $consultarTabla['num_dias_disfrute'];
			$diasAdicionales = $consultarTabla['num_dias_adicionales'];
			$diasDerecho = $consultarTabla['num_total_disfrutar'];
		} else {
			$derecho = 0;
			$diasAdicionales = 0;
			$diasDerecho = 0;
		}
		$dato = array(
			'dias_derecho' => $diasDerecho,
			'dia_adicional' => $diasAdicionales,
			'derecho' => $derecho,
			'anioTotal' => $anioTotal
		);
		return $dato;
	}

	// Método que genera las fecha de salida, termino e incorporacion de las vacaciones
	public function metGenerarFechas($pkNumEmpleado)
	{
		$periodoVacacion = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 2);
		// Recorro el array consultando los dias pendientes
		foreach ($periodoVacacion as $periodo) {
			$pendiente = $periodo['num_pendientes'];
			//  Si no tiene dias pendientes entonces se toman los dias por derecho
			if ($pendiente == 0) {
				$diasConsulta = $periodo['num_dias_derecho'];
			} else {
				$diasConsulta = $pendiente;
			}
			$dias[] = $diasConsulta;
		} // Fin foreach

		// Genero las fechas y las almaceno en un array
		$fechaI = date('Y-m-d');
		$diaFeriado = $this->metDiaFeriado($fechaI);
		while($diaFeriado>0){
			$nuevafecha = strtotime('+1 day', strtotime($fechaI));
			$fechaI = date('Y-m-d', $nuevafecha);
			$diaFeriado = $this->metDiaFeriado($fechaI);
		}
		$i = 0;
		foreach ($dias as $dias) {
			$total = $dias - 1;
			if ($i == 0) {
				$nuevafecha = strtotime('+' . $total . ' day', strtotime($fechaI));
				$fechaFinPrevio = date('Y-m-d', $nuevafecha);
			} else {
				$nuevafechaPrevia = strtotime('+1 day', strtotime($fechaI));
				$fechaI = date('Y-m-d', $nuevafechaPrevia);
				$diaFeriado = $this->metDiaFeriado($fechaI);
				while($diaFeriado>0){
					$nuevafecha = strtotime('+1 day', strtotime($fechaI));
					$fechaI = date('Y-m-d', $nuevafecha);
					$diaFeriado = $this->metDiaFeriado($fechaI);
				}
				$fechaFinPrevia = strtotime('+' . $total . ' day', strtotime($fechaI));
				$fechaFinPrevio = date('Y-m-d', $fechaFinPrevia);
			}
			// Actualizo las fechas
			$diasDiferencia = $this->metFeriado($fechaI, $fechaFinPrevio);
			$a = $diasDiferencia;
			while ($a > 0) {
				$nuevafechaInicio = strtotime('+1 day', strtotime($fechaFinPrevio));
				$fechaInicio = date('Y-m-d', $nuevafechaInicio);
				$nuevafecha = strtotime('+' . $a . ' day', strtotime($fechaFinPrevio));
				$fechaFinPrevio = date('Y-m-d', $nuevafecha);
				$diasFeriados = $this->metFeriado($fechaInicio, $fechaFinPrevio);
				$a = $diasFeriados;
			}
			//Fin actualizar
			$fechas[] = $fechaI . ' / ' . $fechaFinPrevio;
			$fechaI = $fechaFinPrevio;
			$i++;
		}
		return $fechas;
	}

	// Método que determina si los dias feriados y no laborables dentro de un rango de fecha dado
	public function metFeriado($fechaI, $fechaFinPrevio)
	{
		// Determino los dias no laborables (Sábado, Domingo y Feriados)
		$diasDiferencia = 0;
		for ($i = $fechaI; $i <= $fechaFinPrevio; $i = date("Y-m-d", strtotime($i . "+ 1 days"))) {
			$diaFeriado = $this->atVacacionModelo->metFeriado($i);
			$feriado = $diaFeriado['feriado'];
			$fechaComprobar = date("w", strtotime($i));
			if (($fechaComprobar == 0) || ($fechaComprobar == 6) || ($feriado > 0)) {
				$diasDiferencia++;
			}
		}
		return $diasDiferencia;
	}

	// Verifico si una fecha es un dia feriado
	public function metDiaFeriado($fechaI)
	{
		// Determino los dias no laborables (Sábado, Domingo y Feriados)
		$diasDiferencia = 0;
		for ($i = $fechaI; $i <= $fechaI; $i = date("Y-m-d", strtotime($i . "+ 1 days"))) {
			$diaFeriado = $this->atVacacionModelo->metFeriado($i);
			$feriado = $diaFeriado['feriado'];
			$fechaComprobar = date("w", strtotime($i));
			if (($fechaComprobar == 0) || ($fechaComprobar == 6) || ($feriado > 0)) {
				$diasDiferencia++;
			}
		}
		return $diasDiferencia;
	}


	// Método que permite estimar las fechas de termino e incorporación de las vacaciones (Al seleccionar la fecha)
	public function metCalcularFechas()
	{
		$acumuladorPeriodo = $_POST['dias'];
		$fecha_salida = $_POST['fecha_salida'];
		if ($acumuladorPeriodo > 0) {
			$procesarPeriodo = $this->metProcesarPeriodos($fecha_salida, $acumuladorPeriodo);
			echo json_encode($procesarPeriodo);

		} else {
			exit();
		}
	}

	//Método encargado del procesamiento y de generar las fechas
	public function metProcesarPeriodos($fecha_salida, $acumuladorPeriodo)
	{

		$fechaExplode = explode("/", $fecha_salida);
		$fechaSalida = $fechaExplode['2'] . '-' . $fechaExplode['1'] . '-' . $fechaExplode['0'];
		$diaFeriado = $this->metDiaFeriado($fechaSalida);
		while($diaFeriado>0){
			$nuevafecha = strtotime('+1 day', strtotime($fechaSalida));
			$fechaSalida = date('Y-m-d', $nuevafecha);
			$diaFeriado = $this->metDiaFeriado($fechaSalida);
		}

		$fechaI = $fechaSalida;
		$fechaInicial = $fechaSalida;
		// Fecha de Termino
		$total = $acumuladorPeriodo - 1;

		$nuevafecha = strtotime('+' . $total . ' day', strtotime($fechaI));
		$fechaFinPrevio = date('Y-m-d', $nuevafecha);
		// Actualizo las fechas
		$diasDiferencia = $this->metFeriado($fechaI, $fechaFinPrevio);
		$a = $diasDiferencia;
		while ($a > 0) {
			$nuevafechaInicio = strtotime('+1 day', strtotime($fechaFinPrevio));
			$fechaInicio = date('Y-m-d', $nuevafechaInicio);
			$nuevafecha = strtotime('+' . $a . ' day', strtotime($fechaFinPrevio));
			$fechaFinPrevio = date('Y-m-d', $nuevafecha);
			$diasFeriados = $this->metFeriado($fechaInicio, $fechaFinPrevio);
			$a = $diasFeriados + $diaFeriado;
		}

		// Fecha de incorporación
		$fechaInicioPrevio = $fechaFinPrevio;
		$nuevafechaInicio = strtotime('+1 day', strtotime($fechaInicioPrevio));
		$fechaInicio = date('Y-m-d', $nuevafechaInicio);

		$diasDiferencia = $this->metFeriado($fechaInicio, $fechaInicio);
		$a = $diasDiferencia;

		while ($a > 0) {
			$nuevafecha = strtotime('+' . $a . ' day', strtotime($fechaInicio));
			$fechaInicio = date('Y-m-d', $nuevafecha);
			$diasFeriados = $this->metFeriado($fechaInicio, $fechaInicio);
			$a = $diasFeriados;
		}
		//Fin actualizar
		// Formato de Fechas
		$fechaExplode = explode("-", $fechaFinPrevio);
		$fechaTermino = $fechaExplode['2'] . '/' . $fechaExplode['1'] . '/' . $fechaExplode['0'];

		$fechaExplode = explode("-", $fechaInicio);
		$fechaIncorporacion = $fechaExplode['2'] . '/' . $fechaExplode['1'] . '/' . $fechaExplode['0'];

		$fechas = array(
			'fechaInicial' => $fechaInicial,
			'fechaTermino' => $fechaTermino,
			'fechaIncorporacion' => $fechaIncorporacion,
			'periodo' => $acumuladorPeriodo
		);
		return $fechas;
	}

	// Método que permite actualizar los periodos de acuerdo a los periodos solicitados por el empleado
	public function metCargarPeriodos()
	{
		$numDias = $_POST['periodo'];
		$fecha_salida = $_POST['fecha_salida'];
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$metodo = $_POST['metodo'];
		$i=0;
		$acumuladorPeriodo = 0;
		if($metodo==1){
			// Sumo los dias de cada periodo obteniendo el total de dias solcitados
			foreach($numDias as $periodo){
				$acumuladorPeriodo = $acumuladorPeriodo + $periodo;
			}
		} else {
			$acumuladorPeriodo = $numDias;
		}
		$listarPeriodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 6);
		foreach ($listarPeriodo as $listar){
			$pkNumPeriodo = $listar['pk_num_periodo'];
			$diasPendientes = $listar['num_pendientes'];
			$diasDerecho = $listar['num_dias_derecho'];
			$diasUsados = $listar['num_total_utilizados'];
			$numAnio = $listar['num_anio'];
			$totalDias = $diasPendientes - $acumuladorPeriodo;
			if($totalDias<0){
				$dias = $totalDias * (-1);
				$numDias = $diasPendientes;
				$acumuladorPeriodo = $dias;
				if($i==0){
					$fecha = $this->metProcesarPeriodos($fecha_salida, $numDias);
				} else {
					$fecha = $this->metProcesarPeriodos($fecha_salida, $acumuladorPeriodo);
				}

				$fechaSalida = $fecha['fechaInicial'];
				$fechaExplode = explode("-", $fechaSalida);
				$salidaFecha = $fechaExplode['2'] . '/' . $fechaExplode['1'] . '/' . $fechaExplode['0'];
				$datos[$i] = array(
					'numero' => $i+1,
					'anio' => $numAnio,
					'anioAnterior' => $numAnio-1,
					'dias' => $numDias,
					'fecha_salida' =>  $salidaFecha,
					'fecha_termino' => $fecha['fechaTermino'],
					'dias_derecho' => $diasDerecho,
					'dias_usados' => $diasUsados,
					'dias_pendientes' => $diasPendientes,
					'pkNumPeriodo' => $pkNumPeriodo
				);
				$fechaExplode = explode("/", $fecha['fechaTermino']);
				$fechaTermino = $fechaExplode['2'] . '-' . $fechaExplode['1'] . '-' . $fechaExplode['0'];
				$nuevafechaFin = strtotime('+1 day', strtotime($fechaTermino));
				$fechaInicioNuevo = date('d/m/Y', $nuevafechaFin);
				$fecha_salida = $fechaInicioNuevo;

			} else {
				$numDias = $acumuladorPeriodo;
				$fecha = $this->metProcesarPeriodos($fecha_salida, $numDias);
				$fechaSalida = $fecha['fechaInicial'];
				$fechaExplode = explode("-", $fechaSalida);
				$salidaFecha = $fechaExplode['2'] . '/' . $fechaExplode['1'] . '/' . $fechaExplode['0'];
				$datos[$i] = array(
					'numero' => $i+1,
					'anio' => $numAnio,
					'anioAnterior' => $numAnio-1,
					'dias' => $numDias,
					'fecha_salida' =>  $salidaFecha,
					'fecha_termino' => $fecha['fechaTermino'],
					'dias_derecho' => $diasDerecho,
					'dias_usados' => $diasUsados,
					'dias_pendientes' => $diasPendientes,
					'pkNumPeriodo' => $pkNumPeriodo
				);
				break;
			}
			$i++;
		}
		echo json_encode($datos);
	}

	// Método que permite actualizar los periodos de acuerdo a los periodos solicitados por el empleado
	public function metObtenerPeriodos()
	{
		$fecha_salida = $_POST['fecha_salida'];
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$i=0;
		$acumuladorPeriodo = 0;
		$listarPeriodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 2);
		foreach ($listarPeriodo as $listar){
			$pkNumPeriodo = $listar['pk_num_periodo'];
			$diasPendientes = $listar['num_pendientes'];
			$diasDerecho = $listar['num_dias_derecho'];
			$diasUsados = $listar['num_total_utilizados'];
			$numAnio = $listar['num_anio'];
			// Calculo las fechas de salida y termino del periodo
			$fecha = $this->metProcesarPeriodos($fecha_salida, $diasPendientes);
			$fechaSalida = $fecha['fechaInicial'];
			$fechaExplode = explode("-", $fechaSalida);
			$salidaFecha = $fechaExplode['2'] . '/' . $fechaExplode['1'] . '/' . $fechaExplode['0'];
			$datos[$i] = array(
				'numero' => $i+1,
				'anio' => $numAnio,
				'anioAnterior' => $numAnio-1,
				'dias' => $diasPendientes,
				'fecha_salida' =>  $salidaFecha,
				'fecha_termino' => $fecha['fechaTermino'],
				'dias_derecho' => $diasDerecho,
				'dias_usados' => $diasUsados,
				'dias_pendientes' => $diasPendientes,
				'pkNumPeriodo' => $pkNumPeriodo
			);
			$fechaExplode = explode("/", $fecha['fechaTermino']);
			$fechaTermino = $fechaExplode['2'] . '-' . $fechaExplode['1'] . '-' . $fechaExplode['0'];
			$nuevafechaFin = strtotime('+1 day', strtotime($fechaTermino));
			$fechaInicioNuevo = date('d/m/Y', $nuevafechaFin);
			$fecha_salida = $fechaInicioNuevo;
			$i++;
		}
		echo json_encode($datos);
	}

	// Método encargado de consultar las solicitudes de vacaciones asociadas a un periodo en especifico
	public function metSolicitudesPeriodo()
	{
		$periodo = $_POST['pk_num_periodo'];
		$solicitudVacacion = $this->atVacacionModelo->metSolicitudVacacion($periodo);
		$this->atVista->assign('solicitudVacacion', $solicitudVacacion);
		$periodo = $this->atVacacionModelo->metConsultarPeriodo($periodo, 1, 0);
		$periodoVacacion = array(
			'num_anio' => $periodo['num_anio']
		);
		$this->atVista->assign('periodoVacacion', $periodoVacacion);
		$this->atVista->metRenderizar('solicitudesPeriodo', 'modales');
	}

	// Método que permite visualizar la solicitud de vacación
	public function metVerVacacion()
	{
		$pkNumSolicitudVacacion = $this->metObtenerInt('pk_num_solicitud_vacacion');
		$formulario = $this->metObtenerTexto('estado');
		$accion = $this->metObtenerTexto('accion');
		$verSolicitud = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);
		$pkNumEmpleado = $verSolicitud['fk_rhb001_num_empleado'];
		$empleado = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
		$estatus = $this->atVacacionModelo->metEstatus($pkNumSolicitudVacacion);
		$creado = $estatus['ind_nombre1'] . ' ' . $estatus['ind_apellido1'];
		$acceso = $this->atVacacionModelo->metAcceso($pkNumSolicitudVacacion);
		$ultimoUsuario = $acceso['ind_nombre1'] . ' ' . $acceso['ind_apellido1'];
		$estadoPrevio = $verSolicitud['ind_estado'];
		if($estadoPrevio=='PR'){
			$estado = 'PREPARADO';
		}
		if($estadoPrevio=='RE'){
			$estado = 'REVISADO';
		}
		if($estadoPrevio=='CO'){
			$estado = 'CONFORMADO';
		}
		if($estadoPrevio=='AP'){
			$estado = 'APROBADO';
		}
		if($estadoPrevio=='AN'){
			$estado = 'ANULADO';
		}
        $listarDetalle = $this->atVacacionModelo->metMostrarDetalleVacacion($pkNumSolicitudVacacion, 1);
		$solicitud = array(
			'pk_num_solicitud_vacacion' => $verSolicitud['pk_num_solicitud_vacacion'],
			'estado' => $estado,
			'ind_estado' => $estadoPrevio,
			'fecha_solicitud' => $verSolicitud['fecha_solicitud'],
			'ind_motivo' => $verSolicitud['ind_motivo'],
			'num_dias_solicitados' => $verSolicitud['num_dias_solicitados'],
			'fec_salida' => $verSolicitud['fec_salida'],
			'fec_termino' => $verSolicitud['fec_termino'],
			'fec_incorporacion' => $verSolicitud['fec_incorporacion'],
			'ind_nombre1' => $empleado['ind_nombre1'],
			'ind_nombre2' => $empleado['ind_nombre2'],
			'ind_apellido1' => $empleado['ind_apellido1'],
			'ind_apellido2' => $empleado['ind_apellido2'],
			'ind_descripcion_empresa' => $empleado['ind_descripcion_empresa'],
			'ind_dependencia' => $empleado['ind_dependencia'],
			'creado' => $creado,
			'tipo_vacacion' => $verSolicitud['ind_nombre_detalle'],
			'ultimoUsuario' => $ultimoUsuario,
			'ultimaModificacion' => $acceso['ultima_modificacion'],
			'cedula' => $empleado['ind_cedula_documento'],
            'formulario' => $formulario,
            'codDetalle' => $verSolicitud['cod_detalle']
		);
		$this->atVista->assign('listarDetalle', $listarDetalle);
		$this->atVista->assign('solicitud', $solicitud);
		$this->atVista->assign('accion', $accion);
		// Consultar vacaciones pendientes del empleado
		$this->atVista->metRenderizar('ver', 'modales');
	}

	public function metActualizarDetalle($pkNumEmpleado, $pkNumSolicitudVacacion, $num_dias, $usuario, $fecha_hora, $fecha_salida)
	{
		$consultarPeriodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 5);
		foreach ($consultarPeriodo as $periodo) {
			$diasPendientes = $periodo['num_pendientes'];
			$numPeriodo = $periodo['pk_num_periodo'];
			$num_dias_derecho = $periodo['num_dias_derecho'];
			$num_dias_usados = $periodo['num_total_utilizados'];
			$totalDias = $diasPendientes - $num_dias;
			if ($diasPendientes >= $num_dias) {
				$fechas = $this->metProcesarPeriodos($fecha_salida, $num_dias);
				$fec_inicial = $fechas['fechaInicial'];
				$fechaFinal = $fechas['fechaTermino'];
				$fechaIncorporacion = $fechas['fechaIncorporacion'];

				$explodeFinal = explode("/", $fechaFinal);
				$fec_final = $explodeFinal['2'] . '-' . $explodeFinal['1'] . '-' . $explodeFinal['0'];

				$explodeIncorporacion = explode("/", $fechaIncorporacion);
				$fec_incorporacion = $explodeIncorporacion['2'] . '-' . $explodeIncorporacion['1'] . '-' . $explodeIncorporacion['0'];

				$this->atVacacionModelo->metGuardarDetalleVacacion($pkNumSolicitudVacacion, $numPeriodo, $num_dias_derecho, $diasPendientes, $num_dias_usados, $num_dias, $usuario, $fecha_hora, $fec_inicial, $fec_final, $fec_incorporacion);
				break;
			} else {
				$num_dias = (-1) * $totalDias;
				// Se actualiza el periodo
				$fechas = $this->metProcesarPeriodos($fecha_salida,  $diasPendientes);
				$fec_inicial = $fechas['fechaInicial'];
				$fechaFinal = $fechas['fechaTermino'];
				$fechaIncorporacion = $fechas['fechaIncorporacion'];

				$explodeFinal = explode("/", $fechaFinal);
				$fec_final = $explodeFinal['2'] . '-' . $explodeFinal['1'] . '-' . $explodeFinal['0'];

				$explodeIncorporacion = explode("/", $fechaIncorporacion);
				$fec_incorporacion = $explodeIncorporacion['2'] . '-' . $explodeIncorporacion['1'] . '-' . $explodeIncorporacion['0'];

				$this->atVacacionModelo->metGuardarDetalleVacacion($pkNumSolicitudVacacion, $numPeriodo, $num_dias_derecho, $diasPendientes, $num_dias_usados, $diasPendientes, $usuario, $fecha_hora, $fec_inicial, $fec_final, $fec_incorporacion);

			}
			$final = $fec_final;
			$nuevafechaInicio = strtotime('+1 day', strtotime($final));
			$fecha_salida = date('d/m/Y', $nuevafechaInicio);

		}
	}

	//Método que permite editar una solicitud de vacación
	public function metEditarVacacion()
	{
		$pkNumSolicitudVacacion = $this->metObtenerInt('pk_num_solicitud_vacacion');
		$valido = $this->metObtenerInt('valido');
        $estadoLista = $this->metObtenerAlphaNumerico('estadoLista');
		if (isset($valido) && $valido == 1) {
			$pkNumEmpleado = $_POST['pk_num_empleado_editar'];
			$fechaSalida = $_POST['fecha_salida'];
			$explodeSalida = explode("/", $fechaSalida);
			$fecha_salida = $explodeSalida['2'] . '-' . $explodeSalida['1'] . '-' . $explodeSalida['0'];
			$fechaTermino = $_POST['fechaTermino'];
			$fechaIncorporacion = $_POST['fechaIncorporacion'];
			$num_dias = $_POST['num_dias'];
			$ind_motivo = $_POST['ind_motivo'];
			$fecha_hora = date('Y-m-d H:i:s');
			$usuario = Session::metObtener('idUsuario');

			if($estadoLista == 'preparado' || $estadoLista == 'revisar' || $estadoLista == 'conformar') {
                // Actualización de la solicitud
                $this->atVacacionModelo->metActualizarSolicitud($pkNumSolicitudVacacion, $estadoLista);

			}else {

                // Actualización de la solicitud
                $this->atVacacionModelo->metEditarVacacion($pkNumSolicitudVacacion, $num_dias, $fecha_salida, $fechaTermino, $fechaIncorporacion, $ind_motivo, $usuario, $fecha_hora);
                // Verifico si se cambiaron los dias solicitados o la fecha de salida
                $dias = $_POST['dias'];
                $salida = $_POST['salida'];
                if (($dias != $num_dias) || ($salida != $fecha_salida)) {
                    $this->atVacacionModelo->metEliminarDetalleVacacion($pkNumSolicitudVacacion);
                    // Actualización del detalle de la solicitud
                    $this->metActualizarDetalle($pkNumEmpleado, $pkNumSolicitudVacacion, $num_dias, $usuario, $fecha_hora, $fechaSalida);
                }
            }

			// Fin
			$verSolicitud = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);
			$pkNumEmpleado = $verSolicitud['fk_rhb001_num_empleado'];
			$empleado = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
			$funcionario = $empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2'];
			$solicitudFecha = $verSolicitud['fecha_solicitud'];
			$explode = explode("/", $solicitudFecha);
			$solicitud = $explode['2'] . '/' . $explode['1'] . '/' . $explode['0'];

			$estadoPrevio = $verSolicitud['ind_estado'];
			if($estadoPrevio=='PR'){
				$estado = 'PREPARADO';
			}
			if($estadoPrevio=='RE'){
				$estado = 'REVISADO';
			}
			if($estadoPrevio=='CO'){
				$estado = 'CONFORMADO';
			}
			if($estadoPrevio=='AP'){
				$estado = 'APROBADO';
			}
			if($estadoPrevio=='AN'){
				$estado = 'ANULADO';
			}
			// Fin
			$dato = array(
				'pk_num_solicitud_vacacion' => $pkNumSolicitudVacacion,
				'empleado' => $funcionario,
				'tipo_vacacion' => $verSolicitud['ind_nombre_detalle'],
				'fec_solicitud' => $solicitud,
				'ind_estado' => $estado,
                'Mensaje' => 'Solicitud de Vacaciones '.$estadoLista,
                'contenido' => 'La Solicitud de Vacaciones  fue '.$estadoLista.' Satisfactoriamente'
			);
			echo json_encode($dato);
			exit;
		}

		if (!empty($pkNumSolicitudVacacion)) {
			$verSolicitud = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);
			$pkNumEmpleado = $verSolicitud['fk_rhb001_num_empleado'];
			$empleado = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
			$estatus = $this->atVacacionModelo->metEstatus($pkNumSolicitudVacacion);
			$creado = $estatus['ind_nombre1'] . ' ' . $estatus['ind_apellido1'];
			$acceso = $this->atVacacionModelo->metAcceso($pkNumSolicitudVacacion);
			$ultimoUsuario = $acceso['ind_nombre1'] . ' ' . $acceso['ind_apellido1'];
			$estadoPrevio = $verSolicitud['ind_estado'];
			if($estadoPrevio=='PR'){
				$estado = 'PREPARADO';
			}
			if($estadoPrevio=='RE'){
				$estado = 'REVISADO';
			}
			if($estadoPrevio=='CO'){
				$estado = 'CONFORMADO';
			}
			if($estadoPrevio=='AP'){
				$estado = 'APROBADO';
			}
			if($estadoPrevio=='AN'){
				$estado = 'ANULADO';
			}
			$solicitud = array(
				'pk_num_solicitud_vacacion' => $verSolicitud['pk_num_solicitud_vacacion'],
				'pk_num_empleado' => $pkNumEmpleado,
				'ind_estado' => $estado,
				'fec_solicitud' => $verSolicitud['fecha_solicitud'],
				'ind_motivo' => $verSolicitud['ind_motivo'],
				'num_dias_solicitados' => $verSolicitud['num_dias_solicitados'],
				'fec_salida' => $verSolicitud['fec_salida'],
				'fec_termino' => $verSolicitud['fec_termino'],
				'fec_incorporacion' => $verSolicitud['fec_incorporacion'],
				'ind_nombre1' => $empleado['ind_nombre1'],
				'ind_nombre2' => $empleado['ind_nombre2'],
				'ind_apellido1' => $empleado['ind_apellido1'],
				'ind_apellido2' => $empleado['ind_apellido2'],
				'ind_descripcion_empresa' => $empleado['ind_descripcion_empresa'],
				'ind_dependencia' => $empleado['ind_dependencia'],
				'creado' => $creado,
				'tipo_vacacion' => $verSolicitud['ind_nombre_detalle'],
				'ultimoUsuario' => $ultimoUsuario,
				'ultimaModificacion' => $acceso['ultima_modificacion'],
				'cedula' => $empleado['ind_cedula_documento'],
                'codDetalle' => $verSolicitud['cod_detalle']
            );

			$consultarPeriodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 2);
			$this->atVista->assign('periodoVacacion', $consultarPeriodo);

			// Calculo los dias pendientes del periodo
			$acumuladorDias = 0;
			foreach ($consultarPeriodo as $periodo) {
				$pendiente = $periodo['num_pendientes'];
				if ($pendiente == 0) {
					$diasConsulta = 0;
					$diasConsulta = $periodo['num_pendientes'];
				}
				$acumuladorDias = $acumuladorDias + $diasConsulta;
			}
			$dias = array(
				'diasPendientes' => $acumuladorDias
			);
			$vacacionPeriodo = $this->atVacacionModelo->metPeriodoSolicitud($pkNumSolicitudVacacion);
			$this->atVista->assign('dias', $dias);
			$this->atVista->assign('estado', $estadoLista);
			$this->atVista->assign('solicitud', $solicitud);
			$this->atVista->assign('vacacionPeriodo', $vacacionPeriodo);
            $this->atVista->assign('periodo', $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado,7));
            $this->atVista->metRenderizar('editar', 'modales');
		}
	}

	public function metReporteVacacion()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$pkNumSolicitudVacacion = $_GET['pk_num_solicitud_vacacion'];
		$this->metObtenerLibreria('cabeceraVacacion','modRH');
		$pdf= new pdfReporte('P','mm','Letter');

		$pdf->AliasNbPages();
		$detalleVacacion = $this->atVacacionModelo->metMostrarDetalleVacacion($pkNumSolicitudVacacion, 1);
		foreach ($detalleVacacion as $detalle) {
			$pdf->AddPage();
			$pdf->SetAutoPageBreak(true,2);
			// Consulto los datos del empleado correspondiente a la solicitud
			$solicitudVacacion = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);
			$pkNumEmpleado = $solicitudVacacion['fk_rhb001_num_empleado'];
			$empleado = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);

			// Fin
			$pdf->Ln();$pdf->Ln();
			$pdf->SETXY(10,52);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(165, 10,'DISFRUTE DE VACACIONES',1, 0, 'C');
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(33, 5,'FECHA:',1, 0, 'C');
			$pdf->Ln();
			$pdf->SETX(175);
			$pdf->Cell(33, 5,$detalle['fecha_solicitud'],1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
			$pdf->Cell(198, 5,'FUNCIONARIO O TRABAJADOR',1, 0, 'C', 1);
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 10);
			// Cuento los caracteres de la multicelda del nombre del funcionario
			$nombreApellido = $empleado['ind_apellido1'].' '.$empleado['ind_nombre1'];
			$contar = strlen($nombreApellido);
			if($contar>76){
				$altura = 15;
			} else {
				$altura = 10;
			}
			//Fin
			// Separo las fechas de inicio y fin
			// Desde
			$explodeDesde = explode("-", $detalle['fec_inicial']);
			$diaDesde = $explodeDesde[2];
			$mesDesde = $explodeDesde[1];
			$anioDesde = $explodeDesde[0];
			// Hasta
			$explodeHasta = explode("-", $detalle['fec_final']);
			$diaHasta = $explodeHasta[2];
			$mesHasta = $explodeHasta[1];
			$anioHasta = $explodeHasta[0];
			// Fin
			$pdf->Cell(33, $altura,'EMPLEADO',1, 0, 'C');
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(115, 5,'APELLIDOS Y NOMBRES',1, 0, 'C');
			$pdf->SETX(158);
			$pdf->Cell(50, 5,utf8_decode('CÉDULA DE IDENTIDAD'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SETX(43);
			$pdf->SetFont('Arial', '', 10);
			$pdf->SetWidths(array(115, 50));
			$pdf->Row(array(
				utf8_decode($nombreApellido), $empleado['ind_cedula_documento']
			), 5);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(99, 5,'CARGO',1, 0, 'C');
			$pdf->Cell(99, 5,utf8_decode('DIRECCIÓN O DIVISIÓN'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 10);
			$pdf->SetWidths(array(99, 99));
			$pdf->Row(array(
				utf8_decode($empleado['ind_descripcion_cargo']), utf8_decode($empleado['ind_dependencia'])
			), 5);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(65, 5,'FECHA DE INGRESO',1, 0, 'C');
			$pdf->Cell(133, 5,utf8_decode('PERIODO VENCIDO'),1, 0, 'C');
			$pdf->SetFont('Arial', '', 10);
			$pdf->Ln();
			$pdf->Cell(65, 5, $empleado['fec_ingreso'],1, 0, 'C');
			$pdf->Cell(66.5, 5,$detalle['num_anio']-1,1, 0, 'C');
			$pdf->Cell(66.5, 5,$detalle['num_anio'],1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(198, 5,'DETALLES DEL DISFRUTE VACACIONAL',1, 0, 'C', 1);
			$pdf->Ln();
			$pdf->Cell(138, 5, utf8_decode('DÍAS A DISFRUTAR'), 1, 0, 'C');
			$pdf->Cell(60, 5, utf8_decode('REINCORPORARSE'), 1, 1, 'C');
			$pdf->Cell(57, 5, utf8_decode('DESDE:'), 1, 0, 'C');
			$pdf->Cell(57, 5, utf8_decode('HASTA:'), 1, 0, 'C');
			$pdf->Cell(24, 5, utf8_decode('TOTAL DIAS'), 1, 0, 'C');
			$pdf->Cell(19, 5, utf8_decode('DIA'), 1, 0, 'C');
			$pdf->Cell(19, 5, utf8_decode('MES'), 1, 0, 'C');
			$pdf->Cell(22, 5, utf8_decode('AÑO'), 1, 1, 'C');
			$pdf->Cell(19, 5, utf8_decode('DIA'), 1, 0, 'C');
			$pdf->Cell(19, 5, utf8_decode('MES'), 1, 0, 'C');
			$pdf->Cell(19, 5, utf8_decode('AÑO'), 1, 0, 'C');
			$pdf->Cell(19, 5, utf8_decode('DIA'), 1, 0, 'C');
			$pdf->Cell(19, 5, utf8_decode('MES'), 1, 0, 'C');
			$pdf->Cell(19, 5, utf8_decode('AÑO'), 1, 0, 'C');
			$pdf->SetFont('Arial', '', 10);
			$pdf->Cell(24, 10,$solicitudVacacion['num_dias_solicitados'], 1, 0, 'C');
			// Fecha de Incorporacion
			$explodeIncorporar = explode("-", $detalle['fec_incorporacion']);
			$diaIncorporar = $explodeIncorporar[2];
			$mesIncorporar = $explodeIncorporar[1];
			$anioIncorporar = $explodeIncorporar[0];
			// Fin
			$pdf->Cell(19, 10, $diaIncorporar, 1, 0, 'C');
			$pdf->Cell(19, 10, $mesIncorporar, 1, 0, 'C');
			$pdf->Cell(22, 10, $anioIncorporar, 1, 1, 'C');
			$pdf->SetY($pdf->GetY()-5);
			$pdf->Cell(19, 5,$diaDesde, 1, 0, 'C');
			$pdf->Cell(19, 5,$mesDesde, 1, 0, 'C');
			$pdf->Cell(19, 5,$anioDesde, 1, 1, 'C');
			$pdf->SetY($pdf->GetY()-5);
			$pdf->SETX(67);
			$pdf->Cell(19, 5,$diaHasta, 1, 0, 'C');
			$pdf->Cell(19, 5,$mesHasta, 1, 0, 'C');
			$pdf->Cell(19, 5,$anioHasta, 1, 1, 'C');
			$pdf->SetFont('Arial', 'B', 10);

			// Cuento los caracteres de las multiceldas del cargo y el de la dependencia
			$contarCargo = strlen($empleado['ind_descripcion_cargo']);
			$contarDependencia = strlen($empleado['ind_dependencia']);
			if(($contarCargo>45)||($contarDependencia>45)){
				$a = 5;
			} else {
				$a = 0;
			}
			//Fin
            $pdf->SetFillColor(200, 200, 200);
			$pdf->Cell(198, 5,'OBSERVACIONES',1, 0, 'L', 1);
			$pdf->Ln();


			$consultarDetalle = $this->atVacacionModelo->metVerDetalleVacacion($detalle['pk_num_vacacion_detalle']);
            $observacion = strtoupper($consultarDetalle['ind_observacion_conformacion']);
            $pdf->SetWidths(array(198));
            $pdf->SetAligns('L');
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->MultiCell(198,50,utf8_decode($observacion),1, 'J');

            $pdf->SetFont('Arial', 'B', 10);
            //$pdf->Ln();
            $pdf->SetFillColor(200, 200, 200);
            $pdf->Cell(198, 5,utf8_decode('DIAS CONSUMIDOS'),1, 0, 'L', 1);
            $pdf->Ln();
            $pdf->SetWidths(array(198));
            $num_dias_derecho = $detalle['num_dias_derecho'];
            // Calculo los dias disfrutados (Anterior y ACtual)
            $usados = $detalle['num_dias_usados'];
            // Calculo los dias pendientes
            $pendientes = $num_dias_derecho - $usados;
            $anioAnterior = $detalle['num_anio']-1;
            $pdf->Cell(108, 5,'TOTAL PERIODO VACACIONAL '.$anioAnterior.' - '.$detalle['num_anio'].':',1, 0, 'L');
            $pdf->Cell(90, 5, $num_dias_derecho.' '.utf8_decode('DÍAS HÁBILES'),1, 0, 'L');
            $pdf->Ln();
            $pdf->Cell(108, 5,utf8_decode('CANTIDAD DE DÍAS DISFRUTADOS (ANTERIOR Y ACTUAL):'),1, 0, 'L');
            $pdf->Cell(90, 5,$usados.' '.utf8_decode('DÍAS HÁBILES'),1, 0, 'L');
            $pdf->Ln();
            $pdf->Cell(108, 5,utf8_decode('CANTIDAD DE DÍAS PENDIENTES:'),1, 0, 'L');
            $pdf->Cell(90, 5,$pendientes.' '.utf8_decode('DÍAS HÁBILES'),1, 0, 'L');
            // Firmas
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Ln();
            $pdf->SetFillColor(200, 200, 200);
            $pdf->Cell(198, 5,utf8_decode('CONFORMACIÓN Y APROBACIÓN'),1, 0, 'C', 1);


            // Estatus aprobatorios
            $estatusPreparado = 'PR';
            $estatusRevisado = 'RE';
            $estatusConformado = 'CO';
            $estatusAprobado = 'AP';
            $preparados = $this->atVacacionModelo->metEstatusDetalle($pkNumSolicitudVacacion, $estatusPreparado);
            $revisado = $this->atVacacionModelo->metEstatusDetalle($pkNumSolicitudVacacion, $estatusRevisado);
            $conformado = $this->atVacacionModelo->metEstatusDetalle($pkNumSolicitudVacacion, $estatusConformado);
            $aprobado = $this->atVacacionModelo->metEstatusDetalle($pkNumSolicitudVacacion, $estatusAprobado);
            //Fin

            $preparado = $this->atVacacionModelo->metMostrarEmpleadoCargo($solicitudVacacion['fk_rhb001_num_empleado']);

            $revisadoPor = $this->atVacacionModelo->metMostrarEmpleadoCargo($solicitudVacacion['fk_rhb001_num_empleado_revisa']);

            $conformadoPor =  $this->atVacacionModelo->metMostrarEmpleadoCargo($solicitudVacacion['fk_rhb001_num_empleado_conforma']);

            $aprobadoPor = $this->atVacacionModelo->metMostrarEmpleadoCargo($solicitudVacacion['fk_rhb001_num_empleado_aprueba']);





            $pdf->Ln();
            $pdf->SetFont('Arial', 'B', 10);
            $valor = $pdf->GETY();
            $pdf->Cell(99, 5,'FUNCIONARIO O TRABAJADOR',1, 0, 'C');
            $pdf->Cell(99, 5,'REVISADO POR',1, 0, 'C');
            $pdf->Ln();
            //Funcionario
            $pdf->SetFont('Arial', '', 8);
            $y = $pdf->GetY();
            $pdf->MultiCell(99, 5, utf8_decode($preparado['ind_nombre1'].' '.$preparado['ind_apellido1']), 0, 'C');
            $pdf->SetXY(109,$y);
            $pdf->MultiCell(99, 5, utf8_decode($revisadoPor['ind_nombre1'].' '.$revisadoPor['ind_apellido1']), 0, 'C');
            $y = $pdf->GetY();
            $pdf->MultiCell(99, 3, utf8_decode($preparado['ind_descripcion_cargo']), 0, 'C');
            $pdf->SetXY(109,$y);
            $pdf->MultiCell(99, 3, utf8_decode($revisadoPor['ind_descripcion_cargo']), 0, 'C');
            $pdf->Ln(8);




            $pdf->Cell(69, 5,'Firma:',0, 0, 'L');
            $pdf->Cell(29, 5,'Fecha: '.$preparados['fecha_operacion'],0, 0, 'L');
            $pdf->Cell(70, 5,'Firma:',0, 0, 'L');
            $pdf->Cell(28, 5,'Fecha: '.$solicitudVacacion['fecha_revisado'],0, 0, 'L');
            $pdf->Ln();
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(99, 5,'CONFORMADO POR',1, 0, 'C');
            $pdf->Cell(99, 5,'APROBADO POR',1, 0, 'C');
            $pdf->Ln();
            $pdf->SetFont('Arial', '', 8);

            $y = $pdf->GetY();
            $pdf->MultiCell(99, 5, utf8_decode($conformadoPor['ind_nombre1'].' '. $conformadoPor['ind_apellido1']), 0, 'C');
            $pdf->SetXY(109,$y);
            $pdf->MultiCell(99, 5, utf8_decode($aprobadoPor['ind_nombre1'] .' '. ($aprobadoPor['ind_apellido1'])), 0, 'C');
            $y = $pdf->GetY();
            $pdf->MultiCell(99, 3, utf8_decode($conformadoPor['ind_descripcion_cargo']), 0, 'C');
            $pdf->SetXY(109,$y);
            $pdf->MultiCell(99, 3, utf8_decode($aprobadoPor['ind_descripcion_cargo']), 0, 'C');




            $pdf->Ln(8);

            $pdf->Cell(69, 5,'Firma:',0, 0, 'L');
            $pdf->Cell(29, 5,'Fecha: '.$solicitudVacacion['fecha_conformado'],0, 0, 'L');
            $pdf->Cell(70, 5,'Firma:',0, 0, 'L');
            $pdf->Cell(28, 5,'Fecha: '.$solicitudVacacion['fecha_aprobado'],0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 8);
           // Lineas
            $pdf->SETXY(10, $valor+5);
            $pdf->Cell(99, 21,'',1, 0);
            $pdf->SETXY(109, $valor+5);
            $pdf->Cell(99, 21,'',1, 0);
            $pdf->SETXY(10, $valor+26);
            $pdf->Cell(99, 26,'',1, 0);
            $pdf->SETXY(109, $valor+26);
            $pdf->Cell(99, 26,'',1, 0);
		}
		// Fin
		$pdf->Output();

	}

	// Método encargado de mostrar el formulario para la utilización e interrumpción de vacaciones
	public function metGestionVacacion()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		// Valor temporal que identifica al empleado (Se usa ya que aun no esta integrado)
		// recibo al empleado al cual se le gestionará la utilización
		//$pkNumEmpleado = $_POST['pk_num_empleado'];
		$pkNumEmpleado = 12;
		$empleadoDato = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
		$funcionario = $empleadoDato['ind_nombre1'].' '.$empleadoDato['ind_nombre2'].' '.$empleadoDato['ind_apellido1'].' '.$empleadoDato['ind_apellido2'];
		$fechaActual = date('d/m/Y');
		$periodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 3);
		$contador = count($periodo);
		// Calculo el tiempo de servicio
		$fechaIngreso = $empleadoDato['fec_ingreso'];
		$tiempoServicio = $this->metTiempoServicio($fechaIngreso);
		$anio = $tiempoServicio['anio'];
		$diasDerecho = $this->metDiasDerecho($fechaIngreso, $pkNumEmpleado);
		$derecho = $diasDerecho['derecho'];
		$adicional = $diasDerecho['dia_adicional'];
		// Antecedentes de Servicio
		$antecedente = $this->atVacacionModelo->metConsultarAntecedente($pkNumEmpleado);

		$acumuladorAntecedente = 0;
		foreach ($antecedente as $ant){
			$fechaIngresoAnt = $ant['fec_ingreso'];
			$fechaEgreso = $ant['fec_egreso'];
			$fechaInicial = new DateTime($fechaIngresoAnt);
			$fechaFinal = new DateTime($fechaEgreso);
			$interval = $fechaInicial->diff($fechaFinal);
			$servicio = $interval->format('%y');
			$acumuladorAntecedente  = $acumuladorAntecedente + $servicio;
		}


		//Fin
		$empleado = array(
			'fecha_ingreso' => $fechaIngreso,
			'fecha_actual' => $fechaActual,
			'codigo_empleado' => $empleadoDato['pk_num_empleado'],
			'funcionario' => $funcionario,
			'tiempo_servicio' => $anio,
			'derecho' => $derecho,
			'dias_adicionales' => $adicional,
			'contador' => $contador,
			'antecedente' => $acumuladorAntecedente
		);
		$this->atVista->assign('empleado', $empleado);
		$this->atVista->assign('periodo', $periodo);
		// Calculo los dias solicitados de cada periodo de acuerdo a la utilizacion
		$this->atVista->metRenderizar('utilizacion');
	}

	// Método que permite calcular el tiempo de servicio de un empleado
	public function metTiempoServicio($fechaIngreso)
	{

		$explodeInicio = explode('-', $fechaIngreso);
		$diaInicio = $explodeInicio[2];
		$mesInicio = $explodeInicio[1];
		$anioInicio = $explodeInicio[0];

		$fechaActual = date('Y-m-d');
		$explodeFin = explode('-', $fechaActual);
		$diaFin = $explodeFin[2];
		$mesFin = $explodeFin[1];
		$anioFin = $explodeFin[0];

		$fecha1=mktime(0,0,0,$mesInicio,$diaInicio,$anioInicio);
		$fecha2=mktime(0,0,0,$mesFin,$diaFin,$anioFin);
		$diferencia=$fecha2-$fecha1;
		$dias=$diferencia/(60*60*24);
		$totalDias = $dias/365;
		$anioExplode = explode('.', $totalDias);
		$anioTotal = $anioExplode[0];
		$datosServicio = array(
			'anio' => $anioTotal
		);
		return $datosServicio;
	}

	// Método que permite calcular el tiempo de servicio de un empleado
	public function metTiempoServicioVacacion($fechaIngresoInicio, $fechaIngresoFin)
	{
		//echo 'fechaIngresoInicio='.$fechaIngresoInicio.' FechaIngresoFin'.$fechaIngresoFin;

		$explodeInicio = explode('-', $fechaIngresoInicio);
		$diaInicio = $explodeInicio[2];
		$mesInicio = $explodeInicio[1];
		$anioInicio = $explodeInicio[0];

		$explodeFin = explode('-', $fechaIngresoFin);
		$diaFin = $explodeFin[2];
		$mesFin = $explodeFin[1];
		$anioFin = $explodeFin[0];

		$fecha1=mktime(0,0,0,$mesInicio,$diaInicio,$anioInicio);
		$fecha2=mktime(0,0,0,$mesFin,$diaFin,$anioFin);
		$diferencia=$fecha2-$fecha1;

		$dias=$diferencia/(60*60*24);
		$totalDias = round($dias)/365;
		$anioExplode = explode('.', $totalDias);
		$anioTotal = (int)$totalDias;

		$datosServicio = array(
			'anio' => $anioTotal
		);

		return $datosServicio;

	}


	// Método que permite actualizar el estatus de una solicitud de vacaciones
	public function metEstatus()
	{
		$pkNumSolicitudVacacion = $_POST['pk_num_solicitud_vacacion'];
		$valor = $_POST['valor'];
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atVacacionModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$fechaHora = date('Y-m-d H:i:s');
		$verCargo = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
		$cargo = $verCargo['fk_rhc063_num_puestos_cargo'];

		// Antes de Actualizar el estatus consulto el ultimo estado que posea esa solicitud
		$consultarEstatus = $this->atVacacionModelo->metConsultarEstatus($pkNumSolicitudVacacion);
		$valorAnterior = $consultarEstatus['ind_estado'];
		//$this->atVacacionModelo->metActualizarEstatus($pkNumSolicitudVacacion, $valor, $pkNumEmpleado, $fechaHora, $cargo, $valorAnterior);
		$consultarEstatus = $this->atVacacionModelo->metConsultarEstatus($pkNumSolicitudVacacion);
		$valorNuevo = $consultarEstatus['ind_estado'];

		// En caso de ser anulado se devuelven los valores del periodo a su estado anterior
		if(($valor=='AN')&&($valorAnterior=='AP')&&($valorNuevo=='CO')){
			$detalleVacacion = $this->atVacacionModelo->metMostrarDetalleVacacion($pkNumSolicitudVacacion, 1);
			foreach($detalleVacacion as $detalle){
				$pkNumPeriodo = $detalle['fk_rhc081_num_periodo_vacacion'];
				$diasDetalle = $detalle['num_dias_detalle'];
				$consultarPeriodo = $this->atVacacionModelo->metConsultarPeriodo($pkNumPeriodo, 1);
				$diasGozados = $consultarPeriodo['num_dias_gozados'] - $diasDetalle;
				$totalUtilizados = $consultarPeriodo['num_dias_gozados'] - $diasDetalle;
				$diasPendientes = $consultarPeriodo['num_dias_derecho'] - $totalUtilizados;
				if($diasPendientes>0){
					$estado = 1;
				} else {
					$estado = 0;
				}
				$this->atVacacionModelo->metActualizarPeriodo($pkNumPeriodo, $diasPendientes, $diasGozados, $estado);
			}
		}
		$verSolicitud = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);

		if($valor=='AP'){
			$tipoVacacion = $verSolicitud['fk_a006_tipo_vacacion'];
			$codDetalle = $verSolicitud['cod_detalle'];
			if($codDetalle==1){
                $verDetalle = $this->atVacacionModelo->metMostrarDetalleVacacion($pkNumSolicitudVacacion, 1);
                foreach($verDetalle as $detalle){
                    $numDiasDetalle = $detalle['num_dias_detalle'];
                    $fechaInicio = $detalle['fec_inicial'];
                    $fechaFin = $detalle['fec_final'];
                    $pkNumPeriodo = $detalle['fk_rhc081_num_periodo_vacacion'];
                    //Inserto la utilización de las vacaciones aprobadas
                    $this->atVacacionModelo->metUtilizacion($pkNumSolicitudVacacion, $tipoVacacion, $numDiasDetalle, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario, 1);
                    // Actualizo el periodo del empleado
                    $periodo = $this->atVacacionModelo->metConsultarPeriodo($pkNumPeriodo, 2);
                    // calculo los dias pendientes
                    $num_pendientes = $periodo['num_pendientes'];
                    if($num_pendientes==0){
                        $dias = $periodo['num_dias_derecho'];
                    } else {
                        $dias = $num_pendientes;
                    }
                    $nuevoPendiente = $dias - $numDiasDetalle;
                    // Actualizo los dias gozados del funcionario
                    $gozados = $periodo['num_dias_gozados'];
                    $diasGozados = $gozados + $numDiasDetalle;
                    // Verifico el consumo de los dias con el fin de establecer si el periodo seguirá activo o se inactivará
                    $diasDerecho = $periodo['num_dias_derecho'];
                    if($diasDerecho>$diasGozados){
                        $estado = 1;
                    } else {
                        $estado = 0;
                    }
                    $this->atVacacionModelo->metActualizarPeriodo($pkNumPeriodo, $nuevoPendiente, $diasGozados, $estado);
                }
            } else {
			    // Tomo la solicitud de la cual se desprende esa interrupcion
                $pkNumSolicitudVacacionCompleta = $verSolicitud['fk_rhb009_solicitud_vacacion'];
                $fechaInicio = $verSolicitud['fec_salida'];
                $numDiasSolicitados = $verSolicitud['num_dias_solicitados'];
                $verDetalle = $this->atVacacionModelo->metMostrarDetalleVacacion($pkNumSolicitudVacacionCompleta, 2);

                foreach($verDetalle as $detalle) {
                    $numDiasDetalle = $detalle['num_dias_detalle'];
                    $pkNumPeriodo = $detalle['fk_rhc081_num_periodo_vacacion'];
                    $periodo = $this->atVacacionModelo->metConsultarPeriodo($pkNumPeriodo, 2);
                    if($numDiasSolicitados>$numDiasDetalle){
                        $restar = $numDiasSolicitados - $numDiasDetalle;
                        // Calculo las fechas
                        $obtenerFecha = $this->metObtenerFecha($numDiasDetalle, $fechaInicio, '+');
                        $fechaFin = $obtenerFecha['fecha'];
                        $this->atVacacionModelo->metUtilizacion($pkNumSolicitudVacacion, $tipoVacacion, $numDiasDetalle, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario, 1);
                        // calculo los dias
                        // pendientes
                        $num_pendientes = $periodo['num_pendientes'];
                        $nuevoPendiente =  $num_pendientes + $numDiasDetalle;
                        // Actualizo los dias gozados del funcionario
                        $gozados = $periodo['num_dias_gozados'];
                        $diasGozados = $gozados - $numDiasDetalle;
                        $numDiasSolicitados = $restar;
                        // Verifico el consumo de los dias con el fin de establecer si el periodo seguirá activo o se inactivará
                        $diasDerecho = $periodo['num_dias_derecho'];
                        if($diasDerecho>$diasGozados){
                            $estado = 1;
                        } else {
                            $estado = 0;
                        }
                        $this->atVacacionModelo->metPeriodoUtilizacion($pkNumPeriodo, $diasGozados, $numDiasSolicitados, $nuevoPendiente, $estado, $fechaHora, $usuario);
                        $fechaInicio = $fechaFin;
                    } else {
                        // Calculo las fechas
                        $obtenerFecha = $this->metObtenerFecha($numDiasSolicitados, $fechaInicio, '+');
                        $fechaFin = $obtenerFecha['fecha'];
                        $this->atVacacionModelo->metUtilizacion($pkNumSolicitudVacacion, $tipoVacacion, $numDiasSolicitados, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario, 1);
                        // calculo los dias pendientes
                        $num_pendientes = $periodo['num_pendientes'];
                        $nuevoPendiente =  $num_pendientes + $numDiasSolicitados;

                        // Actualizo los dias gozados del funcionario
                        $gozados = $periodo['num_dias_gozados'];
                        $diasGozados = $gozados - $numDiasSolicitados;

                        // Verifico el consumo de los dias con el fin de establecer si el periodo seguirá activo o se inactivará
                        $diasDerecho = $periodo['num_dias_derecho'];
                        if($diasDerecho>$diasGozados){
                            $estado = 1;
                        } else {
                            $estado = 0;
                        }
                        $this->atVacacionModelo->metPeriodoUtilizacion($pkNumPeriodo, $diasGozados, $numDiasSolicitados, $nuevoPendiente, $estado, $fechaHora, $usuario);
                        break;
                    }
                }
            }
		}



		$estadoPrevio = $verSolicitud['ind_estado'];

        if($valor=='RE'){
            $estadoPrevio = 'PR';
        }
        if($valor=='CO'){
            $estadoPrevio = 'RE';
        }
        if($valor=='AP'){
            $estadoPrevio = 'CO';
        }

        $this->atVacacionModelo->metActualizarSolicitud($pkNumSolicitudVacacion,$estadoPrevio);

		$listadoVacacion = array(
			'pk_num_solicitud_vacacion' => $pkNumSolicitudVacacion,
			'ind_estado' => $valor,
			'estadoPrevio' => $estadoPrevio
		);

		echo json_encode($listadoVacacion);
		exit;
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atVacacionModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select name="pk_num_dependencia" id="pk_num_dependencia" id="s2id_single" class="select2-container form-control select2 dirty" onchange="cargarEmpleado(this.value)"><a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencias()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atVacacionModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select name="pk_num_dependencia" id="pk_num_dependencia" class="form-control dirty" onchange="cargarEmpleados(this.value)"><a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar los empleados de una dependencia particular
	public function metBuscarEmpleado()
	{
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarEmpleado=$this->atVacacionModelo->metListarEmpleado($pkNumDependencia, 1);
		$a = '<div class="form-group floating-label"><select name="pk_num_empleado" id="pk_num_empleado" id="s2id_single" class="select2-container form-control select2 dirty" onchange="obtenerDatos(this.value)"><a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">';
		foreach($listarEmpleado as $listarEmpleado){
			$a .='<option value="'.$listarEmpleado['pk_num_empleado'].'">'.$listarEmpleado['ind_nombre1'].' '.$listarEmpleado['ind_nombre2'].' '.$listarEmpleado['ind_apellido1'].' '.$listarEmpleado['ind_apellido2'].'</option>';
		}
		$a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label></div>';
		echo $a;
	}

	// Método que permite listar los empleados de una dependencia particular
	public function metBuscarEmpleados()
	{
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarEmpleado=$this->atVacacionModelo->metListarEmpleado($pkNumDependencia, 1);
		$a = '<div class="form-group floating-label"><select name="pk_num_empleado" id="pk_num_empleado"  class="form-control dirty">';
		foreach($listarEmpleado as $listarEmpleado){
			$a .='<option value="'.$listarEmpleado['pk_num_empleado'].'">'.$listarEmpleado['ind_nombre1'].' '.$listarEmpleado['ind_nombre2'].' '.$listarEmpleado['ind_apellido1'].' '.$listarEmpleado['ind_apellido2'].'</option>';
		}
		$a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label></div>';
		echo $a;
	}

	// Método que gestiona el filtro de busqueda
	public function metBuscarVacacion()
	{
		$pk_num_organismo = $_POST['pk_num_organismo'];
		$pk_num_dependencia = $_POST['pk_num_dependencia'];
		$pk_num_empleado = $_POST['pk_num_empleado'];
		$fechaInicio = $_POST['fecha_inicio'];
		$fechaFin = $_POST['fecha_fin'];
		$pk_num_solicitud_vacacion = $_POST['pk_num_solicitud_vacacion'];
		$ind_estado = $_POST['ind_estado'];
		$vacacionFuncionario = $this->atVacacionModelo->metBuscarVacacion($pk_num_organismo, $pk_num_dependencia, $pk_num_empleado, $fechaInicio, $fechaFin, $pk_num_solicitud_vacacion, $ind_estado);
		echo json_encode($vacacionFuncionario);
	}

	// Método que permite generar el reporte correspondiente al filtro de búsqueda de vacaciones
	public function metBuscarReporte()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$pk_num_organismo = $_GET['pk_num_organismo'];
		$pk_num_dependencia = $_GET['pk_num_dependencia'];
		$pk_num_empleado = $_GET['pk_num_empleado'];
		$fechaInicio = $_GET['fecha_inicio'];
		$fechaFin = $_GET['fecha_fin'];
		$pk_num_solicitud_vacacion = $_GET['pk_num_solicitud_vacacion'];
		$ind_estado = $_GET['ind_estado'];
		$this->metObtenerLibreria('cabeceraVacacion', 'modRH');
		$pdf = new pdfListado('L', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 2);
		$vacacionSolicitud = $this->atVacacionModelo->metBuscarVacacion($pk_num_organismo, $pk_num_dependencia, $pk_num_empleado, $fechaInicio, $fechaFin, $pk_num_solicitud_vacacion, $ind_estado);
		$pdf->SetWidths(array(25, 80, 36, 30, 30, 30, 30));
		foreach ($vacacionSolicitud as $vacacion){
			$pdf->Row(array(
				$vacacion['pk_num_solicitud_vacacion'],
				utf8_decode($vacacion['ind_nombre1'].''.$vacacion['ind_nombre2'].' '.$vacacion['ind_apellido1'].' '.$vacacion['ind_apellido2']),
				$vacacion['fecha_solicitud'],
				$vacacion['num_dias_solicitados'],
				$vacacion['fecha_salida'],
				$vacacion['fecha_termino'],
				$vacacion['fecha_incorporacion']
			), 5);
		}

		$pdf->Output();

	}

	//Método que permite actualizar el periodo de un funcionario en la utilización de las vacaciones
	public function metActualizarPeriodo()
	{
		$valor = $_POST['valor'];
		$pkNumEmpleado = $_POST['empleado'];

		switch($valor) {
			case 1:
				$usuario = Session::metObtener('idUsuario');
				$this->metPeriodoVacacion($pkNumEmpleado, $usuario);
				$listado = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 3);
				echo json_encode($listado);
				break;
			case 2:
				$this->metActualizarPeriodoGeneral();
				$listado = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 3);
				echo json_encode($listado);
				break;
		}

	}

	// Método que permite mostrar la utilización de vacaciones para un periodo en especifico
	public function metUtilizacion()
	{
		$pkNumPeriodo = $_POST['pk_num_periodo'];
		$listadoUtilizacion = $this->atVacacionModelo->metMostrarUtilizacion($pkNumPeriodo);
		echo json_encode($listadoUtilizacion);

	}

	// Método que permite insertar la utilizacion de las vacaciones
	public function metUtilizacionVacaciones()
	{
		$valido = $this->metObtenerInt('valido');
        $tipoFormulario = $this->metObtenerInt('tipoFormulario');
        $pkNumPeriodo = $this->metObtenerInt('pk_num_periodo');
		if (isset($valido) && $valido == 1) {
			// Recibo las variables
            $numDias = $_POST['num_dias'];
            $usuario = Session::metObtener('idUsuario');
            $fechaHora = date('Y-m-d H:i:s');
            if($tipoFormulario==1) {
                $tipoVacacion = $_POST['tipo_vacacion'];
                $fechaInicioPrevio = $_POST['fecha_salida'];
                $fechaFinPrevio = $_POST['fecha_termino'];
                $fechaExplodeInicio = explode("/", $fechaInicioPrevio);
                $fechaInicio = $fechaExplodeInicio['2'] . '-' . $fechaExplodeInicio['1'] . '-' . $fechaExplodeInicio['0'];
                $fechaExplodeFin = explode("/", $fechaFinPrevio);
                $fechaFin = $fechaExplodeFin['2'] . '-' . $fechaExplodeFin['1'] . '-' . $fechaExplodeFin['0'];
                // Fin
                $detalleVacacion = $this->atVacacionModelo->metBuscarTipoVacacion($tipoVacacion);
                $codigoDetalle = $detalleVacacion['cod_detalle'];
                $usuario = Session::metObtener('idUsuario');
                if($codigoDetalle==1){
                    $consultarPeriodo = $this->atVacacionModelo->metConsultarPeriodo($pkNumPeriodo, 1);
                    $numPendientes = $consultarPeriodo['num_pendientes'];
                    $numInterrumpidos = $consultarPeriodo['num_dias_interrumpidos'];
                    $totalPendientes = $numPendientes - $numDias;
                    $numDiasGozados = $consultarPeriodo['num_dias_gozados'] + $numDias;
                    if($totalPendientes>0){
                        $estado = 1;
                    } else {
                        $estado = 0;
                    }
                    // inserto la utilizacion
                    $this->atVacacionModelo->metUtilizacion(0, $tipoVacacion, $numDias, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario, $estado);
                    // Actualizo el periodo
                    $this->atVacacionModelo->metPeriodoUtilizacion($pkNumPeriodo, $numDiasGozados, $numInterrumpidos, $totalPendientes, $estado, $fechaHora, $usuario);
                } else { // Interrupcion
                        $consultarPeriodo = $this->atVacacionModelo->metConsultarPeriodo($pkNumPeriodo, 1);
                        $numPendientes = $consultarPeriodo['num_pendientes'];
                        $numInterrumpidos = $consultarPeriodo['num_dias_interrumpidos'] + $numDias;
                        $totalPendientes = $numPendientes + $numDias;
                        $numDiasGozados = $consultarPeriodo['num_dias_gozados'] - $numDias;
                        if($totalPendientes>0){
                            $estado = 1;
                        } else {
                            $estado = 0;
                        }
                        //Actualizo el periodo para restarle la interrupcion
                        $this->atVacacionModelo->metPeriodoUtilizacion($pkNumPeriodo, $numDiasGozados, $numInterrumpidos, $totalPendientes, $estado, $fechaHora, $usuario);
                        $this->atVacacionModelo->metUtilizacion(0, $tipoVacacion, $numDias, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario, $estado);
                }

                $listadoUtilizacion = $this->atVacacionModelo->metMostrarUtilizacion($pkNumPeriodo);
                echo json_encode($listadoUtilizacion);
            } else {
                    // En este caso se corren los dias de la solicitud al final
                    $pkNumSolicitudVacacion = $_POST['pk_num_solicitud_vacacion'];
                    $fechaSalidaPrevia = $_POST['fecha_salida'];
                    $explodeSalida = explode("/", $fechaSalidaPrevia);
                    $fechaSalida = $explodeSalida[2].'-'.$explodeSalida[1].'-'.$explodeSalida[0];
                    $ind_motivo = $_POST['ind_motivo'];
                    $tipoVacacion = $_POST['tipo_vacacion'];
                    $listarVac = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);

                    // consulto el nombre del tipo de vacacion
                    $nombreVacacionPrevia = $this->atVacacionModelo->metMostrarMiscelaneo($tipoVacacion);
                    // Obtengo los valores
                    $pkNumEmpleado = $listarVac['fk_rhb001_num_empleado'];
                    $cargo = $listarVac['fk_rhc063_num_puesto'];
                    $pkNumDependencia = $listarVac['fk_a004_num_dependencia'];
                    $fechaSolicitud = date('Y-m-d');
                    // Calculo la fecha Termino
                    $fechaTermino = $listarVac['fec_salida'];
                    $obtenerFecha = $this->metObtenerFecha($numDias, $fechaTermino, '+');
                    $fechaTerminoFinal = $obtenerFecha['fecha'];

                    // Calculo la fecha de incorporacion de las vacaciones
                    $obtenerFechaInc = $this->metObtenerFecha(1,  $fechaTerminoFinal, '+');
                    $fechaIncorporacionFinal = $obtenerFechaInc['fecha'];

                    // Guardo la solicitud de vacaciones
                    $numSolicitud = $this->atVacacionModelo->metGuardarVacacionSolicitud($pkNumEmpleado, $usuario, $fechaHora, $fechaSalida, $tipoVacacion, $cargo, $fechaTerminoFinal, $fechaIncorporacionFinal, $pkNumDependencia, $ind_motivo, $numDias, $fechaSolicitud, 2, $pkNumSolicitudVacacion);

                    // retorno los valores
                    $array = array(
                        'pk_num_solicitud_vacacion' => $numSolicitud,
                        'nombreCompleto' => $listarVac['ind_nombre1'].' '.$listarVac['ind_nombre2'].' '.$listarVac['ind_apellido1'].' '.$listarVac['ind_apellido2'],
                        'ind_nombre_detalle' => $nombreVacacionPrevia['ind_nombre_detalle'],
                        'fecha_solicitud' => $listarVac['fecha_solicitud']
                    );
                    echo json_encode($array);
                    exit;
            }

		}
		$pkNumEmpleado = $_POST['pkNumEmpleado'];
		$periodo = array('periodo' => $pkNumPeriodo, 'pk_num_empleado' => $pkNumEmpleado, 'tipoFormulario' => 1);
		$tipoVacacion = $this->atVacacionModelo->metMostrarSelect('TIPVAC');
		$this->atVista->assign('tipoVacacion', $tipoVacacion);
		$this->atVista->assign('periodo', $periodo);
		$this->atVista->metRenderizar('utilizacionRegistrar', 'modales');
	}

    public function metObtenerFecha($dias, $fecha, $operacion)
    {
        $nuevafechaPrevia = strtotime($operacion.''.$dias.' day', strtotime($fecha));
        $fechaI = date('Y-m-d', $nuevafechaPrevia);
        $diaFeriado = $this->metDiaFeriado($fechaI);

        while($diaFeriado>0){
            $nuevafecha = strtotime('+1 day', strtotime($fechaI));
            $fechaI = date('Y-m-d', $nuevafecha);
            $diaFeriado = $this->metDiaFeriado($fechaI);
        }
        $array = array(
          'fecha' => $fechaI
        );
        return $array;
        exit;
    }


    public function metInterrumpirVacaciones()
    {
        $pkNumSolicitudVacacion = $_POST['pk_num_solicitud_vacacion'];
        $numDias = $_POST['num_dias']-1;
        // Consulto la fecha de inicio y fin de la solicitud
        $consultarVacacion = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);
        $fechaSalida = $consultarVacacion['fec_salida'];
        $fechaIncorporacion = $consultarVacacion['fec_termino'];
        $periodo = array(
            'tipoFormulario' => 2,
            'pkNumSolicitudVacacion' => $pkNumSolicitudVacacion,
            'num_dias' => $numDias,
            'fechaIncorporacion' => $fechaIncorporacion,
            'fechaSalida' => $fechaSalida
        );
        $tipoVacacion = $this->atVacacionModelo->metMostrarSelect('TIPVAC');
        $this->atVista->assign('tipoVacacion', $tipoVacacion);
        $this->atVista->assign('periodo', $periodo);
        $this->atVista->metRenderizar('utilizacionRegistrar', 'modales');
    }


	public function metListarPeriodo()
	{
		$pkNumEmpleado = $_POST['pkNumEmpleado'];
		$periodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 3);
		echo json_encode($periodo);
	}

	// Método que permite eliminar una utilización
	public function metEliminarUtilizacion()
	{
		$pk_num_vacacion_utilizacion = $_POST['pk_num_vacacion_utilizacion'];
		$buscarPeriodo = $this->atVacacionModelo->metBuscarPeriodo($pk_num_vacacion_utilizacion);
		$pkNumPeriodo = $buscarPeriodo['fk_rhc081_num_periodo'];
		// Obtengo los datos para buscar el periodo
		$buscarUtilizacion = $this->atVacacionModelo->metVerUtilizacion($pk_num_vacacion_utilizacion);
		$codigoDetalle = $buscarUtilizacion['cod_detalle'];
		$numDias = $buscarUtilizacion['num_dias_utiles'];
		//Actualizo el periodo
		$periodo = $this->atVacacionModelo->metConsultarPeriodo($pkNumPeriodo, 1);

		if($codigoDetalle==1){
			$numDiasGozados = $periodo['num_dias_gozados']-$numDias;
			$diasInterrumpidos = $periodo['num_dias_interrumpidos'];
			$diasPendientes = $periodo['num_pendientes']+$numDias;
		}else{
			$numDiasGozados = $periodo['num_dias_gozados']+$numDias;
			$diasInterrumpidos = $periodo['num_dias_interrumpidos']-$numDias;
			$diasPendientes = $periodo['num_pendientes']-$numDias;
		}
		$usuario = Session::metObtener('idUsuario');
		$fechaHora = date('Y-m-d H:i:s');
		$resultado=$this->atVacacionModelo->metEliminarUtilizacion($pk_num_vacacion_utilizacion);
		if(is_array($resultado)){
			$periodo=array(
				'status'=>'error',
				'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
			);
			echo json_encode($periodo);
		}else{
			if($diasPendientes>0){
				$numFlagEstatus = 1;
			} else {
				$numFlagEstatus = 0;
			}
			$this->atVacacionModelo->metPeriodoUtilizacion($pkNumPeriodo, $numDiasGozados, $diasInterrumpidos, $diasPendientes, $numFlagEstatus, $fechaHora, $usuario);
			// Fin
			$periodo = $this->atVacacionModelo->metMostrarUtilizacion($pkNumPeriodo);
            echo json_encode($periodo);
		}
	}

	//Método que permite validar si los dias solicitados estan disponibles
	public function metComprobarDias()
	{
		$pkNumEmpleado = $_POST['pkNumEmpleado'];
		$diasPendientes = 0;
		$listarPeriodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 2);
		foreach($listarPeriodo as $periodo){
			$diasPendientes = $diasPendientes + $periodo['num_pendientes'];
		}
		$dato = array('num_dias' => $diasPendientes);
		echo json_encode($dato);
	}

	// Método que permite verificar si el usuario tiene ya una solicitud pendiente por vacaciones //mod
	public function metComprobarSolicitud($pkNumEmpleado)
	{
		$vacacionesComprobar = $this->atVacacionModelo->metComprobarVacacion($pkNumEmpleado);
		$ind_estado = $vacacionesComprobar['ind_estado'];
		if(($ind_estado=='PR')||($ind_estado=='CO')||($ind_estado=='RE')){
			$indEstado = 1;
		} else {
			$indEstado = 0;
		}
		return $indEstado;
	}

	// Obtener los datos iniciales del empleado
	public function metObtenerDatos()
	{
		$usuario = Session::metObtener('idUsuario');
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$datosEmpleado = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
		$periodoVacacion = $this->metPeriodoVacacion($pkNumEmpleado, $usuario);
		$comprobarSolicitud = $this->metComprobarSolicitud($pkNumEmpleado);
		// Verifico si el empleado cumple con el tiempo requerido para solicitar disfrute de vacaciones
		$ingreso = $this->metObtenerFechaIngreso($pkNumEmpleado);
		$fechaIngreso = $ingreso['fechaIngresoPrevio'];
		$fechaExplode = explode("-", $fechaIngreso);
		$anioIngreso = $fechaExplode[0];
		$fechaActual = date('Y-m-d');
		$explodeActual = explode("-", $fechaActual);
		$anioActual = $explodeActual[0];
		// Fin
		$totalPeriodo = count($periodoVacacion);
		if ($anioActual > $anioIngreso) {
			if ($fechaActual >= $fechaIngreso) {
				// Calculo las fechas del periodo
				if($totalPeriodo>0) {
					$fechasPeriodo = $this->metGenerarFechas($pkNumEmpleado);
					$a = 1;
					foreach ($fechasPeriodo as $fecha) {
						$fechaExplode = explode("/", $fecha);
						$fechaInicial[$a] = $fechaExplode['0'];
						$fechaFinal[$a] = $fechaExplode['1'];
						$a++;
					}
					// Calculo los dias pendientes del periodo
					$acumuladorDias = 0;
					foreach ($periodoVacacion as $periodo) {
						$pendiente = $periodo['num_pendientes'];
						if ($pendiente == 0) {
							$diasConsulta = 0;
						} else {
							$diasConsulta = $periodo['num_pendientes'];
						}
						$acumuladorDias = $acumuladorDias + $diasConsulta;
					}

					// Calculo las fechas de termino e incorporación correspondiente a los dias pendientes por disfrute de vacaciones

					$fechaSalida = date('d/m/Y');
					if ($acumuladorDias > 0) {
						$procesarPeriodo = $this->metProcesarPeriodos($fechaSalida, $acumuladorDias);
						$fechaTermino = $procesarPeriodo['fechaTermino'];
						$fechaIncorporacion = $procesarPeriodo['fechaIncorporacion'];

					} else {
						$fechaTermino = '';
						$fechaIncorporacion = '';
					}

					$dias = array(
						'diasPendientes' => $acumuladorDias,
						'fechaTermino' => $fechaTermino,
						'fechaIncorporacion' => $fechaIncorporacion,
						'estado' => 'activo',
						'cedula' => $datosEmpleado['ind_cedula_documento'],
						'id_dependencia' => $datosEmpleado['pk_num_dependencia'],
						'dependencia' => $datosEmpleado['ind_dependencia'],
						'datoComprobar' => $comprobarSolicitud
					);

					$this->atVista->assign('fechaInicial', $fechaInicial);
					$this->atVista->assign('fechaFinal', $fechaFinal);
				} else {
					$dias = array(
						'diasPendientes' => 0,
						'fechaTermino' => null,
						'fechaIncorporacion' => null,
						'estado' => 'inactivo',
						'cedula' => $datosEmpleado['ind_cedula_documento'],
						'id_dependencia' => $datosEmpleado['pk_num_dependencia'],
						'dependencia' => $datosEmpleado['ind_dependencia'],
						'datoComprobar' => $comprobarSolicitud
					);
				}

			}
		} else {

			$dias = array(
				'diasPendientes' => 0,
				'fechaTermino' => null,
				'fechaIncorporacion' => null,
				'estado' => 'inactivo',
				'cedula' => $datosEmpleado['ind_cedula_documento'],
				'id_dependencia' => $datosEmpleado['pk_num_dependencia'],
				'dependencia' => $datosEmpleado['ind_dependencia'],
				'datoComprobar' => $comprobarSolicitud
			);
		}
		echo json_encode($dias);
	}

	// Método que permite seleccionar el primer empleado del listado
	public function metObtenerEmpleado()
	{
		$pkNumDependencia = $_POST['pk_num_dependencia'];
		$primerEmpleado = $this->atVacacionModelo->metObtenerEmpleado($pkNumDependencia);
		$dato = array('pk_num_empleado' => $primerEmpleado['pk_num_empleado']);
		echo json_encode($dato);
	}

	// Método que permite editar un periodo
	public function metEditarPeriodo()
	{
		$pkNumEmpleado = $_POST['pkNumEmpleado'];
		$valido = $this->metObtenerInt('valido');
		$usuario = Session::metObtener('idUsuario');
		$fecha_hora = date('Y-m-d H:i:s');
		if (isset($valido) && $valido == 1) {
			$periodoVacacion = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 4);
			$total = count($periodoVacacion);
			for($i=1; $i<=$total; $i++){
				$numAnio = $_POST['num_anio'.$i];
				$diasDerecho = $_POST['dias_derecho'.$i];
				$diasGozados = $_POST['dias_gozados'.$i];
				$diasInterrumpidos = $_POST['dias_interrumpidos'.$i];
				$totalUtilizados = $_POST['total_utilizados'.$i];
				$totalPendientes = $_POST['total_pendientes'.$i];
				if($totalPendientes>0){
					$estado = 1;
				} else {
					$estado = 0;
				}
				$this->atVacacionModelo->metGuardarEditarPeriodo($pkNumEmpleado, $numAnio, $diasDerecho,  $diasGozados, $diasInterrumpidos, $totalUtilizados, $totalPendientes, $estado, $usuario, $fecha_hora);
			}
			$periodoVacacion = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 1);
			echo json_encode($periodoVacacion);
			exit;
		}
		$listadoPeriodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 4);
		$i=1;
		foreach($listadoPeriodo as $periodo){
			$numAnio[$i] = $periodo['num_anio'];
			$numMes[$i] = $periodo['num_mes'];
			$numDiasDerecho[$i] = $periodo['num_dias_derecho'];
			$numDiasGozados[$i] = $periodo['num_dias_gozados'];
			$numDiasInterrumpidos[$i] = $periodo['num_dias_interrumpidos'];
			$numTotalUtilizados[$i] = $periodo['num_total_utilizados'];
			$numPendientes[$i] = $periodo['num_pendientes'];
			$i++;
		}
		$total = count($listadoPeriodo);
		$totalPeriodo = array(
			'totalPeriodo' => $total,
			'pkNumEmpleado' => $pkNumEmpleado
		);
		$this->atVista->assign('numAnio', $numAnio);
		$this->atVista->assign('numMes', $numMes);
		$this->atVista->assign('numDiasDerecho', $numDiasDerecho);
		$this->atVista->assign('numDiasGozados', $numDiasGozados);
		$this->atVista->assign('numDiasInterrumpidos', $numDiasInterrumpidos);
		$this->atVista->assign('numTotalUtilizados', $numTotalUtilizados);
		$this->atVista->assign('numPendientes', $numPendientes);
		$this->atVista->assign('total', $totalPeriodo);
		$this->atVista->metRenderizar('periodoRegistro', 'modales');
	}

	// Método que permite editar un periodo
	public function metEditarUtilizacion()
	{
		$valido = $this->metObtenerInt('valido');
		$pkNumPeriodo = $_POST['pk_num_periodo'];
		$pk_num_vacacion_utilizacion = $_POST['pk_num_vacacion_utilizacion'];
		if (isset($valido) && $valido == 1) {
			// Recibo las variables
			$numDias = $_POST['num_dias'];
			$tipoVacacion = $_POST['tipo_vacacion_d'];
			$fechaInicioPrevio = $_POST['fecha_salida'];
			$fechaFinPrevio = $_POST['fecha_termino'];
			$fechaExplodeInicio = explode("/", $fechaInicioPrevio);
			$fechaInicio = $fechaExplodeInicio['2'] . '-' . $fechaExplodeInicio['1'] . '-' . $fechaExplodeInicio['0'];
			$fechaExplodeFin = explode("/", $fechaFinPrevio);
			$fechaFin = $fechaExplodeFin['2'] . '-' . $fechaExplodeFin['1'] . '-' . $fechaExplodeFin['0'];
			// Fin
			$detalleVacacion = $this->atVacacionModelo->metBuscarTipoVacacion($tipoVacacion);
			$codigoDetalle = $detalleVacacion['cod_detalle'];
			$usuario = Session::metObtener('idUsuario');
			$fechaHora = date('Y-m-d H:i:s');
			// GOCE
			$consultarPeriodo = $this->atVacacionModelo->metConsultarPeriodo($pkNumPeriodo, 1);
			if($codigoDetalle==1){
				#modifico utilizacion del empleado
				$resultado = $this->atVacacionModelo->metModificarUtilizacion($pk_num_vacacion_utilizacion, $tipoVacacion, $numDias, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario);
				if(is_array($resultado)){

				}else{
					#si es correcto consulto las utilizaciones del empleado y actualizo periodo
					$consulta = $this->atVacacionModelo->metMostrarUtilizacionDetalle($pkNumPeriodo,1);
					$numDiasGozados=0;
					#bucle para recorrer los registros de la consulta y totalizar los dias
					foreach($consulta as $reg){
						$numDiasGozados  = $numDiasGozados + $reg['num_dias_utiles'];
						$numPendientes   = $consultarPeriodo['num_dias_derecho'] - $numDiasGozados;
						$totalPendientes = $numPendientes;
						$numInterrumpidos = $consultarPeriodo['num_dias_interrumpidos'];
						if($totalPendientes>0){
							$estado = 1;
						} else {
							$estado = 0;
						}
					}
					#Actualizo el periodo
					$this->atVacacionModelo->metPeriodoUtilizacion($pkNumPeriodo, $numDiasGozados, $numInterrumpidos, $totalPendientes, $estado, $fechaHora, $usuario);
				}

			} else {
				$resultado = $this->atVacacionModelo->metModificarUtilizacion($pk_num_vacacion_utilizacion, $tipoVacacion, $numDias, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario);
				if(is_array($resultado)){
				}else{
					#si es correcto consulto las utilizaciones del empleado y actualizo periodo
					$consulta = $this->atVacacionModelo->metMostrarUtilizacionDetalle($pkNumPeriodo,2);
					$numDiasGozados=0;
					#bucle para recorrer los registros de la consulta y totalizar los dias
					foreach($consulta as $reg){
						$numDiasGozados  = $consultarPeriodo['num_dias_gozados'] - $numDias;
						$numPendientes   = $consultarPeriodo['num_pendientes'];
						$totalPendientes = $numPendientes + $numDias;
						$numInterrumpidos = $consultarPeriodo['num_dias_interrumpidos'] + $numDias;
						if($totalPendientes>0){
							$estado = 1;
						} else {
							$estado = 0;
						}
					}
					$this->atVacacionModelo->metPeriodoUtilizacion($pkNumPeriodo, $numDiasGozados, $numInterrumpidos, $totalPendientes, $estado, $fechaHora, $usuario);
				}
				/*$numPendientes = $consultarPeriodo['num_pendientes'];
				$numInterrumpidos = $consultarPeriodo['num_dias_interrumpidos'] + $numDias;
				$totalPendientes = $numPendientes + $numDias;
				$numDiasGozados = $consultarPeriodo['num_dias_gozados'] - $numDias;
				if($totalPendientes>0){
					$estado = 1;
				} else {
					$estado = 0;
				}*/
				//$this->atVacacionModelo->metModificarUtilizacion($pk_num_vacacion_utilizacion, $tipoVacacion, $numDias, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario, $estado);
				//Actualizo el periodo para restarle la interrupcion

			}

			$listadoUtilizacion = $this->atVacacionModelo->metMostrarUtilizacion($pkNumPeriodo);
			echo json_encode($listadoUtilizacion);
			// Actualizo la tabla del periodo
			exit;
		}

		if($pk_num_vacacion_utilizacion!=0){
			$datos = $this->atVacacionModelo->metMostrarUtilizacionEmpleado($pk_num_vacacion_utilizacion);
			$this->atVista->assign('formDB', $datos);
		}

		$pkNumEmpleado = $_POST['pkNumEmpleado'];
		$periodo = array('periodo' => $pkNumPeriodo, 'pk_num_empleado' => $pkNumEmpleado);
		$tipoVacacion = $this->atVacacionModelo->metMostrarSelect('TIPVAC');
		$this->atVista->assign('tipoVacacion', $tipoVacacion);
		$this->atVista->assign('periodo', $periodo);
		$this->atVista->assign('pk_num_vacacion_utilizacion', $pk_num_vacacion_utilizacion);
		$this->atVista->metRenderizar('utilizacionModificar', 'modales');
	}

	// Método que permite calcular el total de dias pendientes de un empleado
	public function metCalcularDiasPendientes()
	{
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$listadoPeriodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 1);
		$diasPendientes = 0;
		foreach($listadoPeriodo as $periodo){
			$diasPendientes = $diasPendientes + $periodo['num_pendientes'];
		}
		$datoPendiente = array('diasPendientes' => $diasPendientes);
		echo json_encode($datoPendiente);
	}

	// Metodo que permite agregar la observacion del detalle de la solicitud de vacacion
    public function metGuardarObservacionConformacion()
    {
        //Actualizo el estatus de la solicitud
        $pkNumSolicitudVacacion = $_POST['pk_num_solicitud_vacacion'];
        $estado = $_POST['estado'];
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atVacacionModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
      /*  $fechaHora = date('Y-m-d H:i:s');
        $verCargo = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
        $cargo = $verCargo['fk_rhc063_num_puestos_cargo'];
        $this->atVacacionModelo->metActualizarEstatus($pkNumSolicitudVacacion, 'CO', $pkNumEmpleado, $fechaHora, $cargo, $valorAnterior);*/

        // Antes de Actualizar el estatus consulto el ultimo estado que posea esa solicitud
        $consultarEstatus = $this->atVacacionModelo->metConsultarEstatus($pkNumSolicitudVacacion);
        $valorAnterior = $consultarEstatus['ind_estado'];

        if($estado=='RE'){
            $valorAnterior = 'PR';
        }
        if($estado=='CO'){
            $valorAnterior = 'RE';
        }
        if($estado=='AP'){
            $valorAnterior = 'CO';
        }
        $this->atVacacionModelo->metActualizarSolicitud($pkNumSolicitudVacacion,$valorAnterior);

        // Cargo las observaciones registradas
        $listarDetalle = $this->atVacacionModelo->metMostrarDetalleVacacion($pkNumSolicitudVacacion, 1);
        $i=0;
        foreach ($listarDetalle as $listar){
            $pkNumVacacionDetalle = $listar['pk_num_vacacion_detalle'];
            $indObservacion = $_POST['ind_observacion'.$i];
            $this->atVacacionModelo->metConformar($pkNumVacacionDetalle, $indObservacion);
            $i++;
        }

        $listadoVacacion = array(
            'pk_num_solicitud_vacacion' => $pkNumSolicitudVacacion,
            'ind_observacion' => $indObservacion
        );
        echo json_encode($listadoVacacion);
        exit;
    }

    // Metodo que permite actualizar las fechas de salida y termino de la interrupcion de vacaciones
    public function metCargarFechasInterrupcion()
    {
        $pkNumSolicitudVacacion = $this->metObtenerInt('pkNumSolicitudVacacion');
        $numDias = $this->metObtenerInt('num_dias');
        $listarVac = $this->atVacacionModelo->metVerVacacion($pkNumSolicitudVacacion);
        $fechaSalida = $listarVac['fec_salida'];
        // Calculo la fecha Termino
        $obtenerFecha = $this->metObtenerFecha($numDias, $fechaSalida, '+');
        $fechaTerminoFinal = $obtenerFecha['fecha'];
        // Doy formato a la fecha
        $explodeInicial = explode("-", $fechaSalida);
        $fechaSalidaVac = $explodeInicial[2].'/'.$explodeInicial[1].'/'.$explodeInicial[0];
        $explodeFinal = explode("-", $fechaTerminoFinal);
        $fechaFinal = $explodeFinal[2].'/'.$explodeFinal[1].'/'.$explodeFinal[0];

        $array = array(
          'fechaInicial' =>  $fechaSalidaVac,
          'fechaFinal' =>  $fechaFinal
        );
        echo json_encode($array);
        exit;
    }

}
