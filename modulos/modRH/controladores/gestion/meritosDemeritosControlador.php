<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class meritosDemeritosControlador extends Controlador
{
    private $atEmpleados;
    private $atMeritoDemerito;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados     = $this->metCargarModelo('empleados','gestion');
        $this->atMeritoDemerito= $this->metCargarModelo('meritosDemeritos','gestion');
     }

    #Metodo Index del controlador listado de carga familiar del empleado
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoEmpleados', $this->atEmpleados->metListarEmpleado('AP','listado'));
        $this->atVista->assign('n', 1);
        $this->atVista->metRenderizar('empleados');
    }

    public function metMeritosDemeritos()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO

        $complementosCss = array(
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idMeritoDemerito = $this->metObtenerInt('idMeritoDemerito');
        $idEmpleado       = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');


        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_sueldo_mensual')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idMeritoDemerito===0){//PARA INSERTAR REGISTRO

                $validacion['status']='creacion';
                if(!isset($validacion['num_flag_externo'])){
                    $validacion['num_flag_externo']=0;
                }
                if(!isset($validacion['fk_rhb001_num_empleado_responsable'])){
                    $validacion['fk_rhb001_num_empleado_responsable']=NULL;
                }
                if(!isset($validacion['ind_persona_externa'])){
                    $validacion['ind_persona_externa']=NULL;
                }
                #INSTANCIO EL METODO PARA REGISTRAR EL MERITO DEMERITO
                $id = $this->atMeritoDemerito->metRegistrarMeritosDemeritos($idEmpleado,
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_meritodemerito'],
                    $validacion['ind_tipo'],
                    $validacion['ind_documento'],
                    $validacion['fec_documento'],
                    $validacion['fk_rhb001_num_empleado_responsable'],
                    $validacion['ind_persona_externa'],
                    $validacion['num_flag_externo'],
                    $validacion['txt_observaciones']

                );
                $validacion['idMeritoDemerito'] = $id;

                if(is_array($id)){
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atMeritoDemerito->metMostrarMeritosDemeritos($id);
                    $validacion['tipo_merdem'] = $datos['tipo_merdem'];
                    $validacion['merdem'] = $datos['merdem'];
                    echo json_encode($validacion);
                    exit;
                }



            }else{//PARA MODIFICAR REGISTRO

                $validacion['status']='modificar';
                if(!isset($validacion['num_flag_externo'])){
                    $validacion['num_flag_externo']=0;
                }
                if(!isset($validacion['fk_rhb001_num_empleado_responsable'])){
                    $validacion['fk_rhb001_num_empleado_responsable']=NULL;
                }
                if(!isset($validacion['ind_persona_externa'])){
                    $validacion['ind_persona_externa']=NULL;
                }
                #INSTANCIO EL METODO PARA MODIFICAR EL MERITO DEMERITO
                $id = $this->atMeritoDemerito->metModificarMeritosDemeritos($idMeritoDemerito,
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_meritodemerito'],
                    $validacion['ind_tipo'],
                    $validacion['ind_documento'],
                    $validacion['fec_documento'],
                    $validacion['fk_rhb001_num_empleado_responsable'],
                    $validacion['ind_persona_externa'],
                    $validacion['num_flag_externo'],
                    $validacion['txt_observaciones']
                );

                $validacion['idMeritoDemerito'] = $idMeritoDemerito;

                if(is_array($id)){
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atMeritoDemerito->metMostrarMeritosDemeritos($idMeritoDemerito);
                    $validacion['tipo_merdem'] = $datos['tipo_merdem'];
                    $validacion['merdem'] = $datos['merdem'];
                    echo json_encode($validacion);
                    exit;
                }
            }
        }

        if($idMeritoDemerito!=0){
            $db=$this->atMeritoDemerito->metMostrarMeritosDemeritos($idMeritoDemerito);
            if(strcmp($db['ind_tipo'],'ME')==0){//listamos los meritos
                #conecto con el modelo y consulto los datos
                $datos = $this->atEmpleados->metMostrarSelect('MERITO');

            }else{//sino listamos lo demeritos
                #conecto con el modelo y consulto los datos
                $datos = $this->atEmpleados->metMostrarSelect('DEMERITO');
            }
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('MeritoDemerito',$datos);
        }

        $miscelaneoTipo   = $this->atEmpleados->metMostrarSelect('MERDEM');
        $this->atVista->assign('TipoMerDem',$miscelaneoTipo);

        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones');
        $this->atVista->assign('Empleado',$Empleados);

        $this->atVista->assign('idMeritoDemerito',$idMeritoDemerito);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('form', 'modales');
    }

    #permite eliminar merito y demeritos del empleado
    public function metEliminarMeritosDemeritos()
    {
        $idMeritoDemerito = $this->metObtenerInt('idMeritoDemerito');
        if($idMeritoDemerito!=0){
            $id=$this->atMeritoDemerito->metEliminarMeritosDemeritos($idMeritoDemerito);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idMeritoDemerito'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    #para actualizar el select meritos demeritos al seleccionar el tipo
    public function metSelectMeritosDemeritos()
    {
        #obtengo el valor pasado del formulario
        $tipo = $this->metObtenerTexto('tipo');

        #lo desconpogo para obtener el valor que necesito
        $t  = explode("-",$tipo);
        $id = $t[0];//id del miscelaneo
        $cd = $t[1];//cod_detalle

        if(strcmp($cd,'ME')==0){//listamos los meritos
            #conecto con el modelo y consulto los datos
            $datos = $this->atEmpleados->metMostrarSelect('MERITO');

        }else{//sino listamos lo demeritos
            #conecto con el modelo y consulto los datos
            $datos = $this->atEmpleados->metMostrarSelect('DEMERITO');
        }


        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="<option value=''>Seleccione...</option>";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_miscelaneo_detalle'].">".$datos[$i]['ind_nombre_detalle']."</option>";
        }

        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;

    }

}
