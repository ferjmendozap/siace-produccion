<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class experienciaLaboralControlador extends Controlador
{
    private $atEmpleados;
    private $atExpeLaboral;
    private $atMotivoCese;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados     = $this->metCargarModelo('empleados','gestion');
        $this->atExpeLaboral   = $this->metCargarModelo('experienciaLaboral','gestion');
        $this->atMotivoCese    = $this->metCargarModelo('motivoCese','maestros');
     }

    #Metodo Index del controlador listado de carga familiar del empleado
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoEmpleados', $this->atEmpleados->metListarEmpleado('AP','listado'));
        $this->atVista->assign('n', 1);
        $this->atVista->metRenderizar('empleados');
    }

    public function metExperienciaLaboral()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO

        $complementosCss = array(
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'inputmask/mask',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idExperienciaLaboral = $this->metObtenerInt('idExperienciaLaboral');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');


        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_sueldo_mensual')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idExperienciaLaboral===0){//PARA INSERTAR REGISTRO

                $validacion['status']='creacion';

                if(!isset($validacion['num_flag_antvacacion'])){
                    $validacion['num_flag_antvacacion']=0;
                }
                #INSTANCIO EL METODO PARA REGISTRAR LA CARGA DE FAMILIAR
                $id = $this->atExpeLaboral->metRegistrarExperienciaLaboral($idEmpleado,
                    $validacion['ind_cargo'],
                    $validacion['ind_empresa'],
                    $validacion['fk_a006_num_miscelaneo_detalle_areaexp'],
                    $validacion['fec_ingreso'],
                    $validacion['fec_egreso'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_empresa'],
                    $validacion['fk_rhc032_num_motivo_cese'],
                    $validacion['num_sueldo_mensual'],
                    $validacion['num_flag_antvacacion'],
                    $validacion['txt_funciones']

                );
                $validacion['idExperienciaLaboral'] = $id;

                    echo json_encode($validacion);
                    exit;



            }else{//PARA MODIFICAR REGISTRO

                $validacion['status']='modificar';

                if(!isset($validacion['num_flag_antvacacion'])){
                    $validacion['num_flag_antvacacion']=0;
                }
                #INSTANCIO EL METODO PARA MODIFICAR LA INFORMACION BANCARIA
                $id = $this->atExpeLaboral->metModificarExperienciaLaboral($idExperienciaLaboral,
                    $validacion['ind_cargo'],
                    $validacion['ind_empresa'],
                    $validacion['fk_a006_num_miscelaneo_detalle_areaexp'],
                    $validacion['fec_ingreso'],
                    $validacion['fec_egreso'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_empresa'],
                    $validacion['fk_rhc032_num_motivo_cese'],
                    $validacion['num_sueldo_mensual'],
                    $validacion['num_flag_antvacacion'],
                    $validacion['txt_funciones']
                );
                $validacion['idExperienciaLaboral'] = $idExperienciaLaboral;

                    echo json_encode($validacion);
                    exit;


            }

        }

        if($idExperienciaLaboral!=0){
            $db=$this->atExpeLaboral->metMostrarExperienciaLaboral($idExperienciaLaboral);
            $this->atVista->assign('formDB',$db);
        }

        $miscelaneoEntidad   = $this->atEmpleados->metMostrarSelect('TIPOENTE');
        $motivoCese = $this->atMotivoCese->metConsultarMotivoCese();
        $miscelaneoAreaExp = $this->atEmpleados->metMostrarSelect('AREAEXP');
        $this->atVista->assign('Entidad',$miscelaneoEntidad);
        $this->atVista->assign('MotivoCese',$motivoCese);
        $this->atVista->assign('AreaExp',$miscelaneoAreaExp);

        $this->atVista->assign('idExperienciaLaboral',$idExperienciaLaboral);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('form', 'modales');
    }


    public function metEliminarExperienciaLaboral()
    {
        $idExperienciaLaboral = $this->metObtenerInt('idExperienciaLaboral');
        if($idExperienciaLaboral!=0){
            $id=$this->atExpeLaboral->metEliminarExperienciaLaboral($idExperienciaLaboral);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idExperienciaLaboral'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

}
