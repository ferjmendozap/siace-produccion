<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class referenciasControlador extends Controlador
{
    private $atEmpleados;
    private $atReferencias;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados    = $this->metCargarModelo('empleados','gestion');
        $this->atReferencias  = $this->metCargarModelo('referencias','gestion');
     }

    #Metodo Index del controlador listado de carga familiar del empleado
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoEmpleados', $this->atEmpleados->metListarEmpleado('AP','listado'));
        $this->atVista->assign('n', 1);
        $this->atVista->metRenderizar('empleados');
    }

    public function metReferencias()
    {

        $idReferencia = $this->metObtenerInt('idReferencia');
        $idEmpleado   = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');


        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt]=$valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idReferencia===0){//PARA INSERTAR REGISTRO

                $validacion['status']='creacion';

                #INSTANCIO EL METODO PARA REGISTRAR LAS REFRENCIAS
                $id = $this->atReferencias->metRegistrarReferencias($idEmpleado,
                    $validacion['fk_a006_num_miscelaneo_detalle_tiporef'],
                    $validacion['ind_nombre'],
                    $validacion['ind_empresa'],
                    $validacion['ind_direccion'],
                    $validacion['ind_telefono'],
                    $validacion['ind_cargo']
                );
                $validacion['idReferencia'] = $id;

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atReferencias->metMostrarReferencias($id);
                    $validacion['tipo_referencia'] = $datos['tipo_referencia'];
                    echo json_encode($validacion);
                    exit;
                }



            }else{//PARA MODIFICAR REGISTRO

                $validacion['status']='modificar';

                #INSTANCIO EL METODO PARA MODIFICAR LAS REFERENCIAS
                $id = $this->atReferencias->metModificarReferencias($idReferencia,
                    $validacion['fk_a006_num_miscelaneo_detalle_tiporef'],
                    $validacion['ind_nombre'],
                    $validacion['ind_empresa'],
                    $validacion['ind_direccion'],
                    $validacion['ind_telefono'],
                    $validacion['ind_cargo']
                );

                $validacion['idReferencia'] = $idReferencia;

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atReferencias->metMostrarReferencias($idReferencia);
                    $validacion['tipo_referencia'] = $datos['tipo_referencia'];
                    echo json_encode($validacion);
                    exit;
                }
            }
        }

        if($idReferencia!=0){
            $db=$this->atReferencias->metMostrarReferencias($idReferencia);
            $this->atVista->assign('formDB',$db);
        }

        $miscelaneoTipoRef = $this->atEmpleados->metMostrarSelect('TIPOREF');
        $this->atVista->assign('TipoReferencia',$miscelaneoTipoRef);

        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->assign('idReferencia',$idReferencia);
        $this->atVista->metRenderizar('form', 'modales');
    }

    #permite eliminar merito y demeritos del empleado
    public function metEliminarReferencias()
    {
        $idRefrencia = $this->metObtenerInt('idReferencia');
        if($idRefrencia!=0){
            $id=$this->atReferencias->metEliminarReferencias($idRefrencia);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idReferencia'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }



}
