<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class patrimonioControlador extends Controlador
{
    private $atEmpleados;
    private $atPatrimonio;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados   = $this->metCargarModelo('empleados','gestion');
        $this->atPatrimonio  = $this->metCargarModelo('patrimonio','gestion');
     }

    #Metodo Index del controlador listado de carga familiar del empleado
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoEmpleados', $this->atEmpleados->metListarEmpleado('AP','listado'));
        $this->atVista->assign('n', 1);
        $this->atVista->metRenderizar('empleados');
    }

    #INMUEBLES
    public function metInmuebles()
    {

        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
        );
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idPatrimonioInmueble = $this->metObtenerInt('idPatrimonioInmueble');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_valor')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idPatrimonioInmueble===0){
                #NUEVO REGISTRO
                $validacion['status']='creacion';
                if(!isset($validacion['num_flag_hipotecado'])){
                    $validacion['num_flag_hipotecado']=0;
                }
                $datos = array(
                    $validacion['ind_descripcion'],
                    $validacion['ind_ubicacion'],
                    $validacion['ind_uso'],
                    $validacion['num_valor'],
                    $validacion['num_flag_hipotecado']
                );
                #INSTANCIO EL METODO PARA REGISTRAR EL PATRIMONIO
                $id = $this->atPatrimonio->metRegistrarPatrimonio($idEmpleado,'INMUEBLE',$datos);
                $validacion['idPatrimonioInmueble'] = $id;
                echo json_encode($validacion);
                exit;

            }else{
                #MODIFICAR REGISTRO
                $validacion['status']='modificar';
                if(!isset($validacion['num_flag_hipotecado'])){
                    $validacion['num_flag_hipotecado']=0;
                }
                $datos = array(
                    $validacion['ind_descripcion'],
                    $validacion['ind_ubicacion'],
                    $validacion['ind_uso'],
                    $validacion['num_valor'],
                    $validacion['num_flag_hipotecado']
                );
                #INSTANCIO EL METODO PARA MODIFICAR EL PATRIMONIO
                $id = $this->atPatrimonio->metModificarPatrimonio($idPatrimonioInmueble,'INMUEBLE',$datos);
                $validacion['idPatrimonioInmueble'] = $idPatrimonioInmueble;
                echo json_encode($validacion);
                exit;
            }

        }

        if($idPatrimonioInmueble!=0){
            $db=$this->atPatrimonio->metMostrarPatrimonio($idPatrimonioInmueble,'INMUEBLE');
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idPatrimonioInmueble',$idPatrimonioInmueble);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('formInmueble', 'modales');

    }

    #INVERSION
    public function metInversion()
    {
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
        );
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idPatrimonioInversion = $this->metObtenerInt('idPatrimonioInversion');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_valor')==0) || (strcmp($tituloInt,'num_valor_nominal')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idPatrimonioInversion===0){
                #NUEVO REGISTRO
                $validacion['status']='creacion';
                if(!isset($validacion['num_flag_garantia'])){
                    $validacion['num_flag_garantia']=0;
                }
                $datos = array(
                    $validacion['ind_titular'],
                    $validacion['ind_remitente'],
                    $validacion['ind_certificado'],
                    $validacion['num_cant'],
                    $validacion['num_valor_nominal'],
                    $validacion['num_valor'],
                    $validacion['num_flag_garantia']
                );
                #INSTANCIO EL METODO PARA REGISTRAR EL PATRIMONIO
                $id = $this->atPatrimonio->metRegistrarPatrimonio($idEmpleado,'INVERSION',$datos);
                $validacion['idPatrimonioInversion'] = $id;
                echo json_encode($validacion);
                exit;

            }else{
                #MODIFICAR REGISTRO
                $validacion['status']='modificar';
                if(!isset($validacion['num_flag_garantia'])){
                    $validacion['num_flag_garantia']=0;
                }
                $datos = array(
                    $validacion['ind_titular'],
                    $validacion['ind_remitente'],
                    $validacion['ind_certificado'],
                    $validacion['num_cant'],
                    $validacion['num_valor_nominal'],
                    $validacion['num_valor'],
                    $validacion['num_flag_garantia']
                );
                #INSTANCIO EL METODO PARA MODIFICAR EL PATRIMONIO
                $id = $this->atPatrimonio->metModificarPatrimonio($idPatrimonioInversion,'INVERSION',$datos);
                $validacion['idPatrimonioInversion'] = $idPatrimonioInversion;
                echo json_encode($validacion);
                exit;
            }

        }

        if($idPatrimonioInversion!=0){
            $db=$this->atPatrimonio->metMostrarPatrimonio($idPatrimonioInversion,'INVERSION');
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idPatrimonioInversion',$idPatrimonioInversion);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('formInversion', 'modales');
    }

    #VEHICULOS
    public function metVehiculos()
    {
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
        );
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idPatrimonioVehiculo = $this->metObtenerInt('idPatrimonioVehiculo');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_valor')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idPatrimonioVehiculo===0){
                #NUEVO REGISTRO
                $validacion['status']='creacion';

                $datos = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_marca'],
                    $validacion['ind_modelo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_color'],
                    $validacion['ind_placa'],
                    $validacion['fec_anio'],
                    $validacion['num_valor']
                );
                #INSTANCIO EL METODO PARA REGISTRAR EL PATRIMONIO
                $id = $this->atPatrimonio->metRegistrarPatrimonio($idEmpleado,'VEHICULO',$datos);
                $validacion['idPatrimonioVehiculo'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atPatrimonio->metMostrarPatrimonio($id,'VEHICULO');
                    $validacion['marca_vehiculo'] = $datos['marca_vehiculo'];
                    $validacion['color_vehiculo'] = $datos['color_vehiculo'];
                    echo json_encode($validacion);
                    exit;
                }

            }else{
                #MODIFICAR REGISTRO
                $validacion['status']='modificar';

                $datos = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_marca'],
                    $validacion['ind_modelo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_color'],
                    $validacion['ind_placa'],
                    $validacion['fec_anio'],
                    $validacion['num_valor']
                );
                #INSTANCIO EL METODO PARA MODIFICAR EL PATRIMONIO
                $id = $this->atPatrimonio->metModificarPatrimonio($idPatrimonioVehiculo,'VEHICULO',$datos);
                $validacion['idPatrimonioVehiculo'] = $idPatrimonioVehiculo;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atPatrimonio->metMostrarPatrimonio($id,'VEHICULO');
                    $validacion['marca_vehiculo'] = $datos['marca_vehiculo'];
                    $validacion['color_vehiculo'] = $datos['color_vehiculo'];
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idPatrimonioVehiculo!=0){
            $db=$this->atPatrimonio->metMostrarPatrimonio($idPatrimonioVehiculo,'VEHICULO');
            $this->atVista->assign('formDB',$db);
        }


        $miscelaneoMarcaVehiculo = $this->atEmpleados->metMostrarSelect('MARVEH');
        $miscelaneoColores = $this->atEmpleados->metMostrarSelect('COLOR');
        $this->atVista->assign('listadoMarcaVehiculo',$miscelaneoMarcaVehiculo);
        $this->atVista->assign('listadoColores',$miscelaneoColores);

        $this->atVista->assign('idPatrimonioVehiculo',$idPatrimonioVehiculo);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('formVehiculo', 'modales');
    }

    #CUENTAS
    public function metCuentas()
    {
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
        );
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idPatrimonioCuenta = $this->metObtenerInt('idPatrimonioCuenta');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_monto')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idPatrimonioCuenta===0){
                #NUEVO REGISTRO
                $validacion['status']='creacion';

                $datos = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_tipocta'],
                    $validacion['ind_banco'],
                    $validacion['ind_cuenta'],
                    $validacion['num_monto']
                );
                #INSTANCIO EL METODO PARA REGISTRAR EL PATRIMONIO
                $id = $this->atPatrimonio->metRegistrarPatrimonio($idEmpleado,'CUENTAS',$datos);
                $validacion['idPatrimonioCuenta'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atPatrimonio->metMostrarPatrimonio($id,'CUENTAS');
                    $validacion['tipocta'] = $datos['tipocta'];
                    echo json_encode($validacion);
                    exit;
                }

            }else{
                #MODIFICAR REGISTRO
                $validacion['status']='modificar';

                $datos = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_tipocta'],
                    $validacion['ind_banco'],
                    $validacion['ind_cuenta'],
                    $validacion['num_monto']
                );
                #INSTANCIO EL METODO PARA MODIFICAR EL PATRIMONIO
                $id = $this->atPatrimonio->metModificarPatrimonio($idPatrimonioCuenta,'CUENTAS',$datos);
                $validacion['idPatrimonioCuenta'] = $idPatrimonioCuenta;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atPatrimonio->metMostrarPatrimonio($id,'CUENTAS');
                    $validacion['tipocta'] = $datos['tipocta'];
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idPatrimonioCuenta!=0){
            $db=$this->atPatrimonio->metMostrarPatrimonio($idPatrimonioCuenta,'CUENTAS');
            $this->atVista->assign('formDB',$db);
        }


        $miscelaneoTipoCta = $this->atEmpleados->metMostrarSelect('TIPOCTA');
        $this->atVista->assign('listadoTipoCta',$miscelaneoTipoCta);

        $this->atVista->assign('idPatrimonioCuenta',$idPatrimonioCuenta);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('formCuentas', 'modales');
    }

    #OTROS
    public function metOtros()
    {
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
        );
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idPatrimonioOtros = $this->metObtenerInt('idPatrimonioOtros');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_valor_compra')==0) || (strcmp($tituloInt,'num_valor_actual')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idPatrimonioOtros===0){
                #NUEVO REGISTRO
                $validacion['status']='creacion';

                $datos = array(
                    $validacion['ind_descripcion'],
                    $validacion['num_valor_compra'],
                    $validacion['num_valor_actual']
                );
                #INSTANCIO EL METODO PARA REGISTRAR EL PATRIMONIO
                $id = $this->atPatrimonio->metRegistrarPatrimonio($idEmpleado,'OTROS',$datos);
                $validacion['idPatrimonioOtros'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;

                }else{
                    echo json_encode($validacion);
                    exit;
                }

            }else{
                #MODIFICAR REGISTRO
                $validacion['status']='modificar';

                $datos = array(
                    $validacion['ind_descripcion'],
                    $validacion['num_valor_compra'],
                    $validacion['num_valor_actual']
                );
                #INSTANCIO EL METODO PARA MODIFICAR EL PATRIMONIO
                $id = $this->atPatrimonio->metModificarPatrimonio($idPatrimonioOtros,'OTROS',$datos);
                $validacion['idPatrimonioOtros'] = $idPatrimonioOtros;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;

                }else{
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idPatrimonioOtros!=0){
            $db=$this->atPatrimonio->metMostrarPatrimonio($idPatrimonioOtros,'OTROS');
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idPatrimonioOtros',$idPatrimonioOtros);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('formOtros', 'modales');
    }


    public function metConsultarTotalesPatrimonios()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $montosPatrimonio = $this->atPatrimonio->metConsultarTotalesPatrimonios($idEmpleado);
        $INMUEBLES = number_format($montosPatrimonio[0],2,',','.');
        $INVERSION = number_format($montosPatrimonio[1],2,',','.');
        $VEHICULO  = number_format($montosPatrimonio[2],2,',','.');
        $CUENTAS   = number_format($montosPatrimonio[3],2,',','.');
        $OTROS     = number_format($montosPatrimonio[4],2,',','.');
        $TOTAL     = number_format($montosPatrimonio[5],2,',','.');
        $datos = array($INMUEBLES,$INVERSION,$VEHICULO,$CUENTAS,$OTROS,$TOTAL);
        echo json_encode($datos);
    }


    public function metEliminarPatrimonio()
    {

        $tipo = $this->metObtenerTexto('tipo');

        if(strcmp($tipo,'INMUEBLE')==0){

            $idPatrimonioInmueble = $this->metObtenerInt('idPatrimonioInmueble');

            if($idPatrimonioInmueble!=0){
                $id=$this->atPatrimonio->metEliminarPatrimonio($idPatrimonioInmueble,'INMUEBLE');
                if(is_array($id)){
                    $valido=array(
                        'status'=>'error',
                        'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                    );
                }else{
                    $valido=array(
                        'status'=>'OK',
                        'idPatrimonioInmueble'=>$id
                    );
                }
            }
            echo json_encode($valido);
            exit;

        }
        if(strcmp($tipo,'INVERSION')==0){

            $idPatrimonioInversion = $this->metObtenerInt('idPatrimonioInversion');

            if($idPatrimonioInversion!=0){
                $id=$this->atPatrimonio->metEliminarPatrimonio($idPatrimonioInversion,'INVERSION');
                if(is_array($id)){
                    $valido=array(
                        'status'=>'error',
                        'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                    );
                }else{
                    $valido=array(
                        'status'=>'OK',
                        'idPatrimonioInversion'=>$id
                    );
                }
            }
            echo json_encode($valido);
            exit;


        }
        if(strcmp($tipo,'VEHICULO')==0){

            $idPatrimonioVehiculo = $this->metObtenerInt('idPatrimonioVehiculo');

            if($idPatrimonioVehiculo!=0){
                $id=$this->atPatrimonio->metEliminarPatrimonio($idPatrimonioVehiculo,'VEHICULO');
                if(is_array($id)){
                    $valido=array(
                        'status'=>'error',
                        'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                    );
                }else{
                    $valido=array(
                        'status'=>'OK',
                        'idPatrimonioVehiculo'=>$id
                    );
                }
            }
            echo json_encode($valido);
            exit;

        }
        if(strcmp($tipo,'CUENTAS')==0){

            $idPatrimonioCuenta = $this->metObtenerInt('idPatrimonioCuenta');

            if($idPatrimonioCuenta!=0){
                $id=$this->atPatrimonio->metEliminarPatrimonio($idPatrimonioCuenta,'CUENTAS');
                if(is_array($id)){
                    $valido=array(
                        'status'=>'error',
                        'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                    );
                }else{
                    $valido=array(
                        'status'=>'OK',
                        'idPatrimonioCuenta'=>$id
                    );
                }
            }
            echo json_encode($valido);
            exit;

        }
        if(strcmp($tipo,'OTROS')==0){

            $idPatrimonioOtros = $this->metObtenerInt('idPatrimonioOtros');

            if($idPatrimonioOtros!=0){
                $id=$this->atPatrimonio->metEliminarPatrimonio($idPatrimonioOtros,'OTROS');
                if(is_array($id)){
                    $valido=array(
                        'status'=>'error',
                        'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                    );
                }else{
                    $valido=array(
                        'status'=>'OK',
                        'idPatrimonioOtros'=>$id
                    );
                }
            }
            echo json_encode($valido);
            exit;

        }


    }

}
