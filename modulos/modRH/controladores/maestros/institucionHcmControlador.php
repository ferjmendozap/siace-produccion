<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class institucionHcmControlador extends Controlador
{
    private $atinstitucionHcm;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atinstitucionHcm  = $this->metCargarModelo('institucionHcm','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Utiles', $this->atinstitucionHcm->metConsultarInstitucion());
        $this->atVista->metRenderizar('listado');


    }

    #registrar institucion
    public function metNuevaInstitucion()
    {

        $valido    = $this->metObtenerInt('valido');
        $Accion=array( "accion" => "Registrar",);
        if($valido==1){

          //  $this->metValidarToken();

            $formTxt      = $this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            if(!isset($validacion['flag_convenio_ces'])){
                $validacion['flag_convenio_ces']=0;
            }


            $id = $this-> atinstitucionHcm->metRegistrarInstitucion($validacion['ind_nombre_institucion'],$validacion['ind_ubicacion'],$validacion['ind_telefono1'],$validacion['ind_telefono2'],$validacion['fk_a006_clase_centro'],$validacion['fk_a006_tipo_centro'],$validacion['flag_convenio_ces']);

            if(is_array($id)){

                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;

            }else{
                $validacion['status'] = 'OK';
                echo json_encode($validacion);
                exit;
            }

        }else{
            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->assign('_ClasePost', $this->atinstitucionHcm->atMiscelaneoModelo->metMostrarSelect('CLASEINST'));
            $this->atVista->assign('_TipoPost',  $this->atinstitucionHcm->atMiscelaneoModelo->metMostrarSelect('TIPOINST'));
            $this->atVista->metRenderizar('form','modales');


        }

    }

    #modificar institucion
    public function metModificarInstitucion()
    {
        $valido    = $this->metObtenerInt('valido');
        $id    = $this->metObtenerInt('idPost');

        $Accion=array( "accion" => "Modificar",);

        if($valido==1){

            //  $this->metValidarToken();

            $formTxt      = $this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if(!isset($validacion['flag_convenio_ces'])){
                $validacion['flag_convenio_ces']=0;
            }


            $id = $this-> atinstitucionHcm->metModificarInstitucion($validacion['ind_nombre_institucion'],$validacion['ind_ubicacion'],$validacion['ind_telefono1'],$validacion['ind_telefono2'],$validacion['fk_a006_clase_centro'],$validacion['fk_a006_tipo_centro'],$validacion['flag_convenio_ces'],$validacion['id_institucion']);

            if(is_array($id)){

                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;

            }else{
                $validacion['status'] = 'OK';
                echo json_encode($validacion);
                exit;
            }

        }else{
            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->assign('formDB',  $this->atinstitucionHcm->metConsultarInstitucionId($id));
            $this->atVista->assign('_ClasePost', $this->atinstitucionHcm->atMiscelaneoModelo->metMostrarSelect('CLASEINST'));
            $this->atVista->assign('_TipoPost',  $this->atinstitucionHcm->atMiscelaneoModelo->metMostrarSelect('TIPOINST'));
            $this->atVista->metRenderizar('form','modales');


        }

    }


    #para eliminar utiles Escolares
    public function metEliminarinstitucionHcm()
    {
        $idInstitucion = $this->metObtenerInt('idInstitucion');

        $this->atinstitucionHcm ->metEliminarInstitucion( $idInstitucion);

        $array = array(
            'status'  => 'OK',
            'idInstitucion' => $idInstitucion
        );

        echo json_encode($array);
    }


}
