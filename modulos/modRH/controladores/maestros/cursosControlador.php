<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class cursosControlador extends Controlador
{
    private $atCursos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atCursos = $this->metCargarModelo('cursos','maestros');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('enc','');
        $this->atVista->assign('Cursos', $this->atCursos->metConsultarCursos());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoCurso()
    {
        $idCurso = $this->metObtenerInt('idCurso');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formInt      = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');


            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($formAlphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='';

                }
            }

            if($idCurso===0){

                $validacion['status']='creacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                $id = $this->atCursos->metRegistrarCurso(
                    $validacion['fk_a006_num_miscelaneo_detalle_areaformacion'],
                    $validacion['ind_nombre_curso'],
                    $validacion['num_estatus']
                );

                $validacion['idCurso'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                $this->atCursos->metModificarCurso($idCurso,
                    $validacion['fk_a006_num_miscelaneo_detalle_areaformacion'],
                    $validacion['ind_nombre_curso'],
                    $validacion['num_estatus']
                );

                $validacion['idCurso']=$idCurso;
                echo json_encode($validacion);
                exit;

            }

        }

        if($idCurso!=0){
            $db=$this->atCursos->metMostrarCursos($idCurso);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        #PARA LLENAR SELECT AREA
        $Area = $this->atCursos ->metMostrarSelect('AREA');
        $this->atVista->assign('Area',$Area);


        $this->atVista->assign('idCurso',$idCurso);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar un curso
    public function metEliminarCurso()
    {
        $idCurso = $this->metObtenerInt('idCurso');

        $this->atCursos ->metEliminarCurso($idCurso);

        $array = array(
            'status'  => 'OK',
            'idCurso' => $idCurso
        );

        echo json_encode($array);
    }

}
