<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class grupoOcupacionalControlador extends Controlador
{
    private $atGrupoOcupacional;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atGrupoOcupacional = $this->metCargarModelo('grupoOcupacional','maestros');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        //$this->atVista->assign('GrupoOcupacional', $this->atGrupoOcupacional->metConsultarGrupoOcupacional());
        $this->atVista->assign('idMaestro', $this->atGrupoOcupacional->metObtenerMaestro());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoGrupoOcupacional()
    {
        $idGrupoOcupacional = $this->metObtenerInt('idGrupoOcupacional');
        $valido             = $this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');

        if($valido==1){

            $this->metValidarToken();

            $formTxt = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            if($idGrupoOcupacional===0){


                $validacion['status']='creacion';
                $id = $this->atGrupoOcupacional->metRegistrarGrupoOcupacional(
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_grupo'],
                    $validacion['idMaestro']
                );

                $validacion['idGrupoOcupacional']=$id;
                echo json_encode($validacion);
                exit;


            }else{


                $validacion['status']='modificacion';

                $this->atGrupoOcupacional->metModificarGrupoOcupacional($idGrupoOcupacional,
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_grupo']
                );
                $validacion['idGrupoOcupacional']=$idGrupoOcupacional;
                echo json_encode($validacion);
                exit;


            }


        }

        if($idGrupoOcupacional!=0){
            $db = $this->atGrupoOcupacional->metMostrarGrupoOcupacional($idGrupoOcupacional);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idGrupoOcupacional',$idGrupoOcupacional);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->metRenderizar('form','modales');

    }

    public function metEliminarGrupoOcupacional()
    {
        $idGrupoOcupacional = $this->metObtenerInt('idGrupoOcupacional');
        $this->atGrupoOcupacional->metEliminarGrupoOcupacional($idGrupoOcupacional);
        $array = array(
            'status' => 'OK',
            'idGrupoOcupacional' => $idGrupoOcupacional
        );

        echo json_encode($array);
    }



    #PERMITE LISTAR GRUPOS OCUPACIONALES A TRAVES DEL DATATABLES (JSON)
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT
              a006.pk_num_miscelaneo_detalle,
              a006.ind_nombre_detalle,
              a006.cod_detalle,
              a006.num_estatus,
              a006.fk_a005_num_miscelaneo_maestro
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE a005.cod_maestro='GRUPOCUP'
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      a006.ind_nombre_detalle LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_miscelaneo_detalle','ind_nombre_detalle');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_miscelaneo_detalle';
        #construyo el listado de botones
        if (in_array('RH-01-04-01-11-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" title="Editar" idGrupoOcupacional="'.$clavePrimaria.'" descipcion="El Usuario a Modificado un Grupo Ocupacional" titulo="Modificar Grupo Ocupacional">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('RH-01-04-01-12-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGrupoOcupacional="'.$clavePrimaria.'"  boton="si, Eliminar"
                            descipcion="El usuario a eliminado un Grupo Ocupacional" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Grupo Ocupacional!!">
                            <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }

}
