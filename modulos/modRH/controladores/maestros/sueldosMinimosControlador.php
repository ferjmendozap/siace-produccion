<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class sueldosMinimosControlador extends Controlador
{
    private $atSueldoMinimo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSueldoMinimo  = $this->metCargarModelo('sueldosMinimos','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('SueldoMinimo', $this->atSueldoMinimo->metConsultarSueldoMinimo());
        $this->atVista->metRenderizar('listado');


    }

    public function metSueldoMinimo()
    {
        $idSueldoMinimo = $this->metObtenerInt('idSueldoMinimo');
        $valido         = $this->metObtenerInt('valido');

        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min'
        );

        $js = array ('materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents'
        );

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');


            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    if((strcmp($tituloInt,'num_monto_sueldo')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }else{
                    $validacion[$tituloInt]='';
                }
            }


            if($idSueldoMinimo===0){

                $validacion['status']='creacion';

                $id = $this->atSueldoMinimo->metRegistrarSueldoMinimo(
                    $validacion['ind_periodo_sueldo'],
                    $validacion['num_monto_sueldo']
                );

                $validacion['idSueldoMinimo'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                $this->atSueldoMinimo->metModificarSueldoMinimo($idSueldoMinimo,
                    $validacion['ind_periodo_sueldo'],
                    $validacion['num_monto_sueldo']
                );
                $validacion['idSueldoMinimo']=$idSueldoMinimo;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idSueldoMinimo!=0){
            $db=$this->atSueldoMinimo->metMostrarSueldoMinimo($idSueldoMinimo);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idSueldoMinimo',$idSueldoMinimo);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar sueldo minimo
    public function metEliminarSueldoMinimo()
    {
        $idSueldoMinimo = $this->metObtenerInt('idSueldoMinimo');

        $this->atSueldoMinimo->metEliminarSueldoMinimo($idSueldoMinimo);

        $array = array(
            'status'  => 'OK',
            'idSueldoMinimo' => $idSueldoMinimo
        );

        echo json_encode($array);
    }

}
