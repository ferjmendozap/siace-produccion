<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class diasFeriadosControlador extends Controlador
{
    private $atDiasFeriados;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atDiasFeriados  = $this->metCargarModelo('diasFeriados','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $enc = null;
        $this->atVista->assign('enc',$enc);

        $this->atVista->assign('Feriado', $this->atDiasFeriados->metConsultarFeriados());
        $this->atVista->metRenderizar('listado');


    }

    #registrar y modificar clima laboral (plantillas)
    public function metDiasFeriados()
    {

        $complementosCss = array(
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker'
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);

        $idFeriado = $this->metObtenerInt('idFeriado');
        $valido    = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt      = $this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($formAlphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='';

                }
            }

            if($idFeriado===0){

                $validacion['status']='creacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                if(!isset($validacion['num_flag_variable'])){
                    $validacion['num_flag_variable']=0;
                }

                $id = $this->atDiasFeriados->metRegistrarFeriado(
                    $validacion['fec_anio'],
                    $validacion['fec_dia'],
                    $validacion['ind_descripcion_feriado'],
                    $validacion['num_flag_variable'],
                    $validacion['num_estatus']
                );

                $validacion['idFeriado'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                if(!isset($validacion['num_flag_variable'])){
                    $validacion['num_flag_variable']=0;
                }

                $this->atDiasFeriados->metModificarFeriado($idFeriado,
                    $validacion['fec_anio'],
                    $validacion['fec_dia'],
                    $validacion['ind_descripcion_feriado'],
                    $validacion['num_flag_variable'],
                    $validacion['num_estatus']
                );

                $validacion['idFeriado']=$idFeriado;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idFeriado!=0){
            $db=$this->atDiasFeriados->metMostrarFeriado($idFeriado);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $anio = date("Y");
            $this->atVista->assign('anio',$anio);
            $this->atVista->assign('default', 1);
        }


        $this->atVista->assign('idFeriado',$idFeriado);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar plantillas
    public function metEliminarDiasFeriados()
    {
        $idFeriado = $this->metObtenerInt('idFeriado');

        $this->atDiasFeriados ->metEliminarFeriado($idFeriado);

        $array = array(
            'status'  => 'OK',
            'idFeriado' => $idFeriado
        );

        echo json_encode($array);
    }


}
