<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class medicosHcmControlador extends Controlador
{
    private $atmedicoHcm;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atmedicoHcm  = $this->metCargarModelo('medicosHcm','maestros');
        $this->atinstitucionHcm  = $this->metCargarModelo('institucionHcm','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Medicos', $this->atmedicoHcm ->metConsultarMedicos());
        $this->atVista->metRenderizar('listado');


    }

    #registrar institucion
    public function metRegistrarMedico()
    {

        $valido    = $this->metObtenerInt('valido');
        $Accion=array( "accion" => "Registrar",);

        if($valido==1){

          //  $this->metValidarToken();

            $formTxt      = $this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if(!isset($validacion['flag_convenio_ces'])){
                $validacion['flag_convenio_ces']=0;
            }
            if(!isset($validacion['flag_interno_ces'])){
                $validacion['flag_interno_ces']=0;
            }



           $id = $this-> atmedicoHcm->metRegistrarMedico($validacion['nombre'],$validacion['apellido'],$validacion['cedula'],$validacion['telefono'],$validacion['institucion'],$validacion['flag_convenio_ces'],$validacion['flag_interno_ces']);

            if(is_array($id)){

                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;

            }else{
                $validacion['status'] = 'OK';
                echo json_encode($validacion);
                exit;
            }

        }else{
            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->assign('_InstitucionesPost', $this->atinstitucionHcm->metConsultarInstitucion());
 
            $this->atVista->metRenderizar('form','modales');


        }

    }

    #MODIFICAR EL REGISTRO DE UN MÉDICO
    public function metModificarMedico()
    {
        $valido    = $this->metObtenerInt('valido');
        $id    = $this->metObtenerInt('idPost');

        $Accion=array( "accion" => "Modificar",);

        if($valido==1){

            //  $this->metValidarToken();

            $formTxt      = $this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            if(!isset($validacion['flag_convenio_ces'])){
                $validacion['flag_convenio_ces']=0;
            }
            if(!isset($validacion['flag_interno_ces'])){
                $validacion['flag_interno_ces']=0;
            }


            #RECIBIMOS EL PK PERSONA
            $pk_persona=$this->atmedicoHcm->metConsultarPkPersona($validacion['id_medico']);

            $id = $this-> atmedicoHcm->metModificarMedico($validacion['telefono'],$validacion['institucion'],$validacion['flag_convenio_ces'],$validacion['flag_interno_ces'],$validacion['nombre'],$validacion['apellido'],$validacion['cedula'],$pk_persona[0]['fk_a003_persona'],$validacion['id_medico']);

            if(is_array($id)){

                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;

            }else{
                $validacion['status'] = 'OK';
                echo json_encode($validacion);
                exit;
            }


        }else{
            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->assign('formDB',  $this->atmedicoHcm->metConsultarMedicoId($id));
            $this->atVista->assign('_InstitucionesPost', $this->atinstitucionHcm->metConsultarInstitucion());
            $this->atVista->assign('_ClasePost', $this->atmedicoHcm->atMiscelaneoModelo->metMostrarSelect('CLASEINST'));
            $this->atVista->assign('_TipoPost',  $this->atmedicoHcm->atMiscelaneoModelo->metMostrarSelect('TIPOINST'));
            $this->atVista->metRenderizar('form','modales');


        }

    }


    #para eliminar utiles Escolares
    public function metEliminarMedicoHcm()
    {
        $idMedico = $this->metObtenerInt('idMedico');

        $this->atmedicoHcm ->metEliminarMedico($idMedico);

        $array = array(
            'status'  => 'OK',
            'idMedico' => $idMedico
        );

        echo json_encode($array);
    }


}
