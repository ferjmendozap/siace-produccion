<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class tipoContratoControlador extends Controlador
{
    private $atTipoContrato;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTipoContrato  = $this->metCargarModelo('tipoContrato','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('TipoContrato', $this->atTipoContrato->metConsultarTipoContrato());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoTipoContrato()
    {
        $idTipoContrato = $this->metObtenerInt('idTipoContrato');
        $valido         = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if($idTipoContrato===0){

                $validacion['status']='creacion';

                if(!isset($validacion['num_flag_nomina'])){
                    $validacion['num_flag_nomina']=0;
                }
                if(!isset($validacion['num_flag_vencimiento'])){
                    $validacion['num_flag_vencimiento']=0;
                }
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                $id = $this->atTipoContrato->metRegistrarTipoContrato(
                    $validacion['ind_tipo_contrato'],
                    $validacion['num_flag_nomina'],
                    $validacion['num_flag_vencimiento'],
                    $validacion['num_estatus']
                );

                $validacion['idTipoContrato'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['num_flag_nomina'])){
                    $validacion['num_flag_nomina']=0;
                }
                if(!isset($validacion['num_flag_vencimiento'])){
                    $validacion['num_flag_vencimiento']=0;
                }

                $this->atTipoContrato->metModificarTipoContrato($idTipoContrato,
                    $validacion['ind_tipo_contrato'],
                    $validacion['num_flag_nomina'],
                    $validacion['num_flag_vencimiento'],
                    $validacion['num_estatus']
                );
                $validacion['idTipoContrato']=$idTipoContrato;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idTipoContrato!=0){
            $db=$this->atTipoContrato->metMostrarTipoContrato($idTipoContrato);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        $this->atVista->assign('idTipoContrato',$idTipoContrato);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar un tipo de contrato
    public function metEliminarTipoContrato()
    {
        $idTipoContrato = $this->metObtenerInt('idTipoContrato');

        $this->atTipoContrato ->metEliminarTipoContrato($idTipoContrato);

        $array = array(
            'status'  => 'OK',
            'idTipoContrato' => $idTipoContrato
        );

        echo json_encode($array);
    }

}
