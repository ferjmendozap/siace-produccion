<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class tipoTrabajadorControlador extends Controlador
{
    private $atTipoTrabajador;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTipoTrabajador  = $this->metCargarModelo('tipoTrabajador','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('TipoTrabajador', $this->atTipoTrabajador->metConsultarTipoTrabajador());
        $this->atVista->assign('idMaestro', $this->atTipoTrabajador->metObtenerMaestro());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoTipoTrabajador()
    {
        $idTipoTrabajador = $this->metObtenerInt('idTipoTrabajador');
        $valido           = $this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if($idTipoTrabajador===0){

                $validacion['status']='creacion';

                if(!isset($validacion['ind_estatus'])){
                    $validacion['ind_estatus']=0;
                }

               $id = $this->atTipoTrabajador->metRegistrarTipoTrabajador(
                   $validacion['cod_detalle'],
                   $validacion['ind_nombre_trabajador'],
                   $validacion['idMaestro']
                );

                $validacion['idTipoTrabajador'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['ind_estatus'])){
                    $validacion['ind_estatus']=0;
                }

                $this->atTipoTrabajador->metModificarTipoTrabajador($idTipoTrabajador,
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_trabajador']
                );
                $validacion['idTipoTrabajador']=$idTipoTrabajador;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idTipoTrabajador!=0){
            $db=$this->atTipoTrabajador->metMostrarTipoTrabajador($idTipoTrabajador);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idTipoTrabajador',$idTipoTrabajador);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar un tipo de trabajador
    public function metEliminarTipoTrabajador()
    {
        $idTipoTrabajador = $this->metObtenerInt('idTipoTrabajador');

        $this->atTipoTrabajador ->metEliminarTipoTrabajador($idTipoTrabajador);

        $array = array(
            'status'  => 'OK',
            'idTipoTrabajador' => $idTipoTrabajador
        );

        echo json_encode($array);
    }

}
