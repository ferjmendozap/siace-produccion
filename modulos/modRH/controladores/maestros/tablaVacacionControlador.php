<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Recursos Humanos.
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de los dias de disfrute de vacaciones
class tablaVacacionControlador extends Controlador
{
	private $atTablaModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atTablaModelo = $this->metCargarModelo('tablaVacacion', 'maestros');
	}

	// Método index del controlador
	public function metIndex(){
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef'
		);
		$complementosJs = array('select2/select2.min');
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('tablaVacacion', $this->atTablaModelo->metListarTabla());
		$this->atVista->metRenderizar('listado');
	}

	//Método que permite registrar una nueva tabla
	public function metNuevaTabla()
	{
		$valido = $this->metObtenerInt('valido');
		$usuario = Session::metObtener('idUsuario');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$tipoNomina = $this->metObtenerInt('tipo_nomina');
			$anios = $this->metObtenerInt('anios');
			$diasDisfrute = $this->metObtenerInt('dias_disfrute');
			$diasAdicionales = $this->metObtenerInt('dias_adicionales');
			$totalDisfrute = $diasDisfrute + $diasAdicionales;
			$fecha_hora = date('Y-m-d H:i:s');
			$pkNumTablaVacacion = $this->atTablaModelo->metGuardarTabla($tipoNomina, $anios, $diasDisfrute, $diasAdicionales, $totalDisfrute, $usuario, $fecha_hora);
			$consultaTabla = $this->atTablaModelo->metConsultarTabla($pkNumTablaVacacion);
			$nombreNomina = $consultaTabla['ind_nombre_nomina'];
			$tablaVacacion = array(
				'pk_num_tabla_vacacion' => $pkNumTablaVacacion,
				'tipoNomina' => $nombreNomina,
				'anios' => $anios,
				'diasDisfrute' => $diasDisfrute,
				'diasAdicionales' => $diasAdicionales,
				'totalDisfrute' => $totalDisfrute
			);
			echo json_encode($tablaVacacion);
			exit;
		}
		$form = array(
			'status' => 'nuevo',
			'pk_num_tabla_vacacion' => null,
			'tipoNomina' => null,
			'anios' => null,
			'diasDisfrute' => null,
			'diasAdicionales' => null,
			'totalDisfrute' => null
		);
		$tipoNomina = $this->atTablaModelo->metListarNomina();
		$this->atVista->assign('form', $form);
		$this->atVista->assign('tipoNomina', $tipoNomina);
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	// Método que permite calcular el tiempo total del disfrute vacacional
	public function metCalcularTiempo()
	{
		$diasDisfrute = $this->metObtenerInt('dias_disfrute');
		$diasAdicionales = $this->metObtenerInt('dias_adicionales');
		$total = $diasAdicionales + $diasDisfrute;
		$arrayTotal = array(
			'total' => $total
		);
		echo json_encode($arrayTotal);
	}

	// Método que permite visualizar un disfrute
	public function metVerTabla()
	{
		$pkNumTablaVacacion = $this->metObtenerInt('pk_num_tabla_vacacion');
		$consultarTabla = $this->atTablaModelo->metConsultarTabla($pkNumTablaVacacion);
		$this->atVista->assign('tabla', $consultarTabla);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	//Método que permite eliminar un disfrute
	public function metEliminarTabla()
	{
		$pkNumTablaVacacion = $this->metObtenerInt('pk_num_tabla_vacacion');
		$this->atTablaModelo->metEliminarTabla($pkNumTablaVacacion);
		$eliminar = array(
			'status' => 'OK',
			'pk_num_tabla_vacacion' => $pkNumTablaVacacion
		);
		echo json_encode($eliminar);
	}

	//Método que permite editar un disfrute
	public function metEditarTabla()
	{
		$pkNumTablaVacacion = $this->metObtenerInt('pk_num_tabla_vacacion');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$tipoNomina = $this->metObtenerInt('tipo_nomina');
			$anios = $this->metObtenerInt('anios');
			$diasDisfrute = $this->metObtenerInt('dias_disfrute');
			$diasAdicionales = $this->metObtenerInt('dias_adicionales');
			$totalDisfrute = $diasDisfrute + $diasAdicionales;
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atTablaModelo->metEditarTabla($tipoNomina, $anios, $diasDisfrute, $diasAdicionales, $totalDisfrute, $usuario, $fecha_hora, $pkNumTablaVacacion);
			$consultaTabla = $this->atTablaModelo->metConsultarTabla($pkNumTablaVacacion);
			$nombreNomina = $consultaTabla['ind_nombre_nomina'];
			$tablaVacacion = array(
				'status' => 'modificar',
				'pk_num_tabla_vacacion' => $pkNumTablaVacacion,
				'tipoNomina' => $nombreNomina,
				'anios' => $anios,
				'diasDisfrute' => $diasDisfrute,
				'diasAdicionales' => $diasAdicionales,
				'totalDisfrute' => $totalDisfrute
			);
			echo json_encode($tablaVacacion);
			exit;
		}
		if (!empty($pkNumTablaVacacion)) {
			$consultarTabla = $this->atTablaModelo->metConsultarTabla($pkNumTablaVacacion);
			$this->atVista->assign('tabla', $consultarTabla);
			$tipoNomina = $this->atTablaModelo->metListarNomina();
			$this->atVista->assign('tipoNomina', $tipoNomina);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}
}
?>
