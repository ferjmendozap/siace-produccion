<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class gradoEstudiosControlador extends Controlador
{
    private $atGradoEstudios;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atGradoEstudios  = $this->metCargarModelo('gradoEstudios','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('GradoEstudios', $this->atGradoEstudios->metConsultarGradoEstudios());
        $this->atVista->assign('idMaestro', $this->atGradoEstudios->metObtenerMaestro());
        $this->atVista->metRenderizar('listado');


    }

    #registrar y modificar grados de Estudios
    public function metGradoEstudios()
    {

        $idGradoEstudio = $this->metObtenerInt('idGradoEstudio');
        $valido         = $this->metObtenerInt('valido');
        $maestro        = $this->metObtenerInt('maestro');

        if($valido==1){

            $this->metValidarToken();

            $formTxt      = $this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }


            if($idGradoEstudio===0){

                $validacion['status']='creacion';

                $id = $this->atGradoEstudios->metRegistrarGradoEstudios(
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_grado'],
                    $validacion['idMaestro']
                );

                $validacion['idGradoEstudio'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                $this->atGradoEstudios->metModificarGradoEstudios($idGradoEstudio,
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_grado']
                );

                $validacion['idGradoEstudio']=$idGradoEstudio;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idGradoEstudio!=0){
            $db=$this->atGradoEstudios->metMostrarGradoEstudios($idGradoEstudio);
            $this->atVista->assign('formDB',$db);
        }


        $this->atVista->assign('idGradoEstudio',$idGradoEstudio);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar grados de estudios
    public function metEliminarGradoEstudios()
    {
        $idGradoEstudio = $this->metObtenerInt('idGradoEstudio');

        $this->atGradoEstudios ->metEliminarGradoEstudios($idGradoEstudio);

        $array = array(
            'status'  => 'OK',
            'idGradoEstudio' => $idGradoEstudio
        );

        echo json_encode($array);
    }


}
