<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class utilesEscolaresControlador extends Controlador
{
    private $atUtilesEscolares;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atUtilesEscolares  = $this->metCargarModelo('utilesEscolares','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Utiles', $this->atUtilesEscolares->metConsultarUtiles());
        $this->atVista->assign('idMaestro', $this->atUtilesEscolares->metObtenerMaestro());
        $this->atVista->metRenderizar('listado');


    }

    #registrar y modificar utiles Escolares
    public function metUtilesEscolares()
    {

        $idUtiles  = $this->metObtenerInt('idUtiles');
        $valido    = $this->metObtenerInt('valido');
        $maestro   = $this->metObtenerInt('maestro');

        if($valido==1){

            $this->metValidarToken();

            $formTxt      = $this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if($idUtiles===0){

                $validacion['status']='creacion';

                $id = $this->atUtilesEscolares->metRegistrarUtiles(
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_utiles'],
                    $validacion['idMaestro']
                );

                $validacion['idUtiles'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                $this->atUtilesEscolares->metModificarUtiles($idUtiles,
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_utiles']
                );

                $validacion['idUtiles']=$idUtiles;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idUtiles!=0){
            $db=$this->atUtilesEscolares->metMostrarUtiles($idUtiles);
            $this->atVista->assign('formDB',$db);
        }


        $this->atVista->assign('idUtiles',$idUtiles);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar utiles Escolares
    public function metEliminarUtilesEscolares()
    {
        $idUtiles = $this->metObtenerInt('idUtiles');

        $this->atUtilesEscolares ->metEliminarUtiles($idUtiles);

        $array = array(
            'status'  => 'OK',
            'idUtiles' => $idUtiles
        );

        echo json_encode($array);
    }


}
