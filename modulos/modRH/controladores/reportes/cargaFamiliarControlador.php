<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class cargaFamiliarControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('cargaFamiliar', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['fk_a004_num_dependencia'];
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo,
            'pk_num_dependencia' => $pkNumDependencia

		);
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $tipoTrabajador = $this->atReporteModelo->metMostrarSelect('TIPOTRAB');
		$sexo = $this->atReporteModelo->metMostrarSelect('SEXO');
        $estadoCivil = $this->atReporteModelo->metMostrarSelect('EDOCIVIL');
        $parentesco = $this->atReporteModelo->metMostrarSelect('PARENT');
        $listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $listadoEmpleado = $this->atReporteModelo->metListarEmpleado($pkNumDependencia, 1);
        $centroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, $pkNumDependencia);
        $tipoNomina = $this->atReporteModelo->metListarNomina();
	// Cargar a la vista
	$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
	$this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('sexo', $sexo);
        $this->atVista->assign('parentesco', $parentesco);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('edoCivil', $estadoCivil);
        $this->atVista->assign('listadoEmpleado', $listadoEmpleado);
        $this->atVista->assign('centroCosto',  $centroCosto);
        $this->atVista->assign('nomina', $tipoNomina);
		$this->atVista->metRenderizar('listadoCargaFamiliar');
	}

    // Método que permite listar las dependencias del organismo
    public function metBuscarDependencia()
    {
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $listarDependencia=$this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="" disabled selected>&nbsp;</option>';
        foreach($listarDependencia as $listarDependencia){
            $a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
        echo $a;
    }

    // Método que permite listar las dependencias del organismo
    public function metBuscarCentroCosto()
    {
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
        $listarCentroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, $pkNumDependencia);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="centro_costo" id="centro_costo"><option value="0" disabled selected>&nbsp;</option>';
        foreach($listarCentroCosto as $centroCosto){
            $a .='<option value="'.$centroCosto['pk_num_centro_costo'].'">'.$centroCosto['ind_descripcion_centro_costo'].'</option>';
        }
        $a .= '</select><label for="pk_num_centro_costo"><i class="glyphicon glyphicon-briefcase"></i> Centro Costo</label></div>';
        echo $a;
    }

    // Método que permite calcular la edad
    public function metCalcularEdad($fechaInicio, $fechaFin)
    {
        $fechaInicial = new DateTime($fechaInicio);
        $fechaFinal = new DateTime($fechaFin);
        $interval = $fechaInicial->diff($fechaFinal);
        $datosServicio = array(
            'anio' => $interval->format('%y'),
            'mes' => $interval->format('%m'),
            'dia' => $interval->format('%d')
        );
        return $datosServicio;
    }

    // Método que permite calcular la edad de una persona
    public function metCalcularFechaEdad($edadPrevia)
    {
        $fecha = date('Y-m-d');
        $nuevafecha = strtotime ( '-'.$edadPrevia.' year' , strtotime ( $fecha ) ) ;
        $nuevafechaTotal = date ( 'Y-m-d' , $nuevafecha );
        return $nuevafechaTotal;
    }

	// Funcion que genera el PDF correspondiente al reporte de empleados
	public function metGenerarReporteFamiliar()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        // Datos del empleado
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pk_num_dependencia = $_GET['pk_num_dependencia'];
        $centro_costo = $_GET['centro_costo'];
        $tipo_nomina = $_GET['tipo_nomina'];
        $edo_reg = $_GET['edo_reg'];
        $sit_trab = $_GET['sit_trab'];
        $sexo_empleado = $_GET['sexo_empleado'];
        // Datos de la carga familiar
        $edad1 = $_GET['edad1'];
        $edad2 = $_GET['edad2'];
        $parentesco = $_GET['parentesco'];
        $edoCivil = $_GET['edoCivil'];
        $sexoFamiliar = $_GET['sexo_familiar'];
        $discapacidad = $_GET['discapacidad'];
        $estudio = $_GET['estudio'];

        //  Verifico la fecha de edad
        if(($edad1>0)&&($edad2>0)) {
            $fechaInicio = $this->metCalcularFechaEdad($edad1);
            $fechaFin = $this->metCalcularFechaEdad($edad2);
        }
        if(($edad1>0)&&($edad2==0)){
            $fechaInicio = $this->metCalcularFechaEdad($edad1);
            $fechaFin = '';
        }
        if(($edad1==0)&&($edad2>0)){
            $fechaInicio = '';
            $fechaFin = $this->metCalcularFechaEdad($edad2);
        }
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfCargaFamiliar('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 2);
        $pdf->SetFont('Arial', 'B', 8);
        $listarEmpleado = $this->atReporteModelo->metConsultarEmpleados($pkNumOrganismo, $pk_num_dependencia, $centro_costo, $tipo_nomina, $edo_reg, $sit_trab, $sexo_empleado);
        if($pk_num_dependencia!=''){
            $dependencia = $this->atReporteModelo->metListarDependencia(0, 2, $pk_num_dependencia);
            $pdf->Dependencia($dependencia['ind_dependencia']);
        }
            foreach($listarEmpleado as $empleado){
                $empleadoConsultar = $empleado['pk_num_empleado'];
                $cargaFamiliar = $this->atReporteModelo->metListadoCargaFamiliar($empleadoConsultar, $fechaInicio, $fechaFin, $parentesco, $edoCivil, $sexoFamiliar, $discapacidad, $estudio);
                $contador = count($cargaFamiliar);
                if($contador>0) {
                    $pdf->Cell(21, 5, number_format($empleado['ind_cedula_documento'],0 , ' , ' ,  '.'), 0, 0, 'C');
                    $pdf->Cell(82, 5, utf8_decode($empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2']), 0, 0, 'L');
                    $pdf->Ln();
                    $pdf->Cell(21, 5,'', 0, 0, 'C');
                    $pdf->Cell(82, 5, $empleado['ind_descripcion_cargo'], 0, 0, 'L');
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 7);
                    foreach ($cargaFamiliar as $familiar) {
                        // Calculo la edad de la persona
                        $fechaActual = date('Y-m-d');
                        $edadNacimiento = $this->metCalcularEdad($familiar['fec_nacimiento'], $fechaActual);
                        $anio = $edadNacimiento['anio'];
                        $pdf->Cell(21, 5, number_format($familiar['ind_cedula_documento'],0 , ' , ' ,  '.'), 0, 0, 'C');
                        $pdf->Cell(82, 5, utf8_decode($familiar['ind_apellido1'] . ' ' . $familiar['ind_apellido2'] . ' ' . $familiar['ind_nombre1'] . ' ' . $familiar['ind_nombre2']), 0, 0, 'L');
                        $pdf->Cell(20, 5, $familiar['ind_nombre_detalle'], 0, 0, 'C');
                        $pdf->Cell(35, 5, $familiar['fecha_nacimiento'], 0, 0, 'C');
                        $pdf->Cell(10, 5, $anio, 0, 0, 'C');
                        $pdf->Cell(10, 5, '', 0, 0, 'C');
                        $pdf->Cell(10, 5, '', 0, 0, 'C');
                        $pdf->Cell(10, 5, '', 0, 0, 'C');
                        $pdf->Ln();
                    }
                }
                $pdf->SetFont('Arial', 'B', 8);
            }
		$pdf->Output();

	}
}
