<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class cumpleanioControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('cumpleanio', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$tipoTrabajador = $this->atReporteModelo->metMostrarSelect('TIPOTRAB');
		$tipoNomina = $this->atReporteModelo->metListarNomina();
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '', '', '', '', '');
        $centroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, 0);
		// Cargar a la vista
		$this->atVista->assign('tipoTrabajador', $tipoTrabajador);
		$this->atVista->assign('nomina', $tipoNomina);
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('datos', $listadoEmpleado);
        $this->atVista->assign('centroCosto', $centroCosto);
		$this->atVista->metRenderizar('listadoCumpleanio');
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarCentroCosto()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarCentroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, $pkNumDependencia);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="centro_costo" id="centro_costo"><option value="">&nbsp;</option>';
		foreach($listarCentroCosto as $centroCosto){
			$a .='<option value="'.$centroCosto['pk_num_centro_costo'].'">'.$centroCosto['ind_descripcion_centro_costo'].'</option>';
		}
		$a .= '</select><label for="pk_num_centro_costo"><i class="glyphicon glyphicon-briefcase"></i> Centro Costo</label></div>';
		echo $a;
	}

	public function metObtenerMes($mes)
	{
		switch($mes){
			case 1:
				$nombreMes = 'ENERO';
				break;
			case 2:
				$nombreMes = 'FEBRERO';
				break;
			case 3:
				$nombreMes = 'MARZO';
				break;
			case 4:
				$nombreMes = 'ABRIL';
				break;
			case 5:
				$nombreMes = 'MAYO';
				break;
			case 6:
				$nombreMes = 'JUNIO';
				break;
			case 7:
				$nombreMes = 'JULIO';
				break;
			case 8:
				$nombreMes = 'AGOSTO';
				break;
			case 9:
				$nombreMes = 'SEPTIEMBRE';
				break;
			case 10:
				$nombreMes = 'OCTUBRE';
				break;
			case 11:
				$nombreMes = 'NOVIEMBRE';
				break;
			case 12:
				$nombreMes = 'DICIEMBRE';
				break;
		}
		return $nombreMes;
	}

	// Método que permite calcular la edad de una persona
	public function metCalcularFechaEdad($edadPrevia)
	{
		$fecha = date('Y-m-d');
		$nuevafecha = strtotime ( '-'.$edadPrevia.' year' , strtotime ( $fecha ) ) ;
		$nuevafechaTotal = date ( 'Y-m-d' , $nuevafecha );
		return $nuevafechaTotal;
	}

	// Método que permite calcular la edad
	public function metCalcularEdad($fechaInicio, $fechaFin)
	{
		$fechaInicial = new DateTime($fechaInicio);
		$fechaFinal = new DateTime($fechaFin);
		$interval = $fechaInicial->diff($fechaFinal);
		$datosServicio = array(
			'anio' => $interval->format('%y'),
			'mes' => $interval->format('%m'),
			'dia' => $interval->format('%d')
		);
		return $datosServicio;
	}

	// Método que permite
	public function metGenerarReporteCumpleanio()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfReporteCumpleanio('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$centroCosto = $_GET['centro_costo'];
		$mes_nacimiento = $_GET['mes_nacimiento'];
		$anio1 = $_GET['anio1'];
		$anio2 = $_GET['anio2'];
		$edoReg = $_GET['edoReg'];
		$sitTrab = $_GET['sitTrab'];
		$tipoNomina = $_GET['tipo_nomina'];
		$tipoTrabajador = $_GET['tipo_trabajador'];
		if(($anio1!='')&&($anio2!='')){
			$obtenerFecha1 = $this->metCalcularFechaEdad($anio1);
			$obtenerFecha2 = $this->metCalcularFechaEdad($anio2);
		} else {
			$obtenerFecha1 = '';
			$obtenerFecha2 = '';
		}
		$pdf->SetWidths(array(20, 102, 40, 20, 15));
		$pdf->SetAligns(array('C', 'L', 'L', 'C','C'));
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
		if($mes_nacimiento>0){
			$valor = $mes_nacimiento;
			$mesNacimiento = $mes_nacimiento;
		} else {
			$valor = 12;
			$mesNacimiento = 1;
		}
		$fechaActual = date('Y-m-d');
		$j = 0;
		for($i=$mesNacimiento; $i<=$valor; $i++){
			if($i==$mes_nacimiento){
				$mostrarMes = $this->metObtenerMes($mesNacimiento);
			} else {
				$mostrarMes = $this->metObtenerMes($i);
			}
			$mesNacimientoNuevo = str_pad($i, 2, '0', STR_PAD_LEFT);
			$empleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $centroCosto, $mesNacimientoNuevo, $obtenerFecha1, $obtenerFecha2, $edoReg, $sitTrab, $tipoNomina, $tipoTrabajador);
			$contadorEmpleado = count($empleado);
			$contar = 0;
			foreach($empleado as $emp){
				if($contar==0){
					$pdf->SetFont('Arial', 'B', 8);
					$pdf->Cell(15, 5, $mostrarMes, 0, 0, 'L');
					$pdf->SetFont('Arial', '', 8);
					$pdf->Ln();
					}
					$calcularEdad = $this->metCalcularEdad($emp['fec_nacimiento'], $fechaActual);
					$j++;
					if ($j % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
					else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
					$pdf->Row(array(
						number_format($emp['ind_cedula_documento'],0 , ' , ' ,  '.'),
						utf8_decode($emp['ind_nombre1'].' '.$emp['ind_nombre2'].' '.$emp['ind_apellido1'].' '.$emp['ind_apellido2']),
						utf8_decode($emp['ind_nombre_nomina']),
						$emp['fecha_nacimiento'],
						$calcularEdad['anio']
					));
					$contar++;

				if($contar==$contadorEmpleado){
					$pdf->SetFont('Arial', 'B', 8);
					$pdf->Cell(80, 5, 'Nro. de Trabajadores: '.$contar, 0, 0, 'R');
					$pdf->Ln();
					$pdf->SetFont('Arial', '', 8);
				}
			}
		}
		$pdf->Output();
	}
}
