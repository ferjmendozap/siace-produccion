<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class bonoAlimentacionControlador extends Controlador
{
    private $atEmpleados;
    private $atBonoAlimentacion;
    private $atDependencias;
    private $atTipoNomina;
    private $atHorario;
    private $atOrganismo;
    private $atFunciones;
    private $atFeriados;
    private $atIdUsuario;
    private $atObligacion;
    private $atPartidas;
    private $atPresupuesto;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados    = $this->metCargarModelo('empleados','gestion');
        $this->atBonoAlimentacion  = $this->metCargarModelo('bonoAlimentacion','procesos');
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');
        $this->atTipoNomina   = $this->metCargarModelo('tipoNomina','maestros','modNM');
        $this->atHorario      = $this->metCargarModelo('horarioLaboral','maestros');
        $this->atOrganismo    = $this->metCargarModelo('organismo','maestros','modCV');
        $this->atFeriados     = $this->metCargarModelo('diasFeriados','maestros');
        $this->atFunciones = $this->metCargarModelo('funcionesRecursosHumanos','funcionesRecursosHumanos');
        $this->atObligacion   = $this->metCargarModelo('obligacion','','modCP');
        $this->atPartidas   = $this->metCargarModelo('partida','maestros','modPR');
        $this->atPresupuesto = $this->metCargarModelo('presupuesto','','modPR');
        $this->atIdUsuario    = Session::metObtener('idUsuario');

    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('proceso','periodos');

        $this->atVista->assign('listadoBonoAlimentacion', $this->atBonoAlimentacion->metListarBonoAlimentacion());
        $this->atVista->metRenderizar('listado');
    }


    public function metBonoAlimentacion()
    {
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/mask'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoTableDynamic','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idBonoAlimentacion = $this->metObtenerInt('idBonoAlimentacion');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_valor_mes')==0) || (strcmp($tituloInt,'num_valor_diario')==0) || (strcmp($tituloInt,'num_valor_semanal')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idBonoAlimentacion==0){#REGISTRAR


                $validacion['status']='creacion';

                $cad_empleados = $validacion['id_empleados'];

                if(!isset($validacion['num_total_feriados']) || empty($validacion['num_total_feriados'])){
                    $validacion['num_total_feriados']=0;
                }

                #array de datos
                $datos = array(
                    $validacion['fk_a001_num_organismo'],
                    $validacion['ind_descripcion'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['ind_periodo'],
                    $validacion['fec_inicio_periodo'],
                    $validacion['fec_fin_periodo'],
                    $validacion['num_dias_periodo'],
                    $validacion['num_dias_pago'],
                    $validacion['num_total_feriados'],
                    $validacion['num_valor_diario'],
                    $validacion['fec_horas_diarias'],
                    $validacion['fec_horas_semanales'],
                    $validacion['num_valor_semanal'],
                    $validacion['num_valor_mes'],
                    $validacion['fk_b002_num_partida_presupuestaria'],
                    $validacion['cod_partida'],
                    $validacion['ind_denominacion_partida'],
                    $validacion['fk_cpb002_tipo_documento'],
                    $validacion['ind_cod_presupuesto'],
                    $validacion['fk_prb004_num_presupuesto'],
                    $validacion['ind_descripcion_descuento'],
                    $validacion['num_monto_descuento'],
                    $validacion['num_monto_descuento_otros'],

                );


                $id = $this->atBonoAlimentacion->metRegistrarBonoAlimentacion($datos,$cad_empleados);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status'] = 'OK';
                    echo json_encode($validacion);
                    exit;
                }



            }else{#modificar

                $validacion['status']='modificar';

                $cad_empleados = $validacion['id_empleados'];

                if(!isset($validacion['num_total_feriados']) || empty($validacion['num_total_feriados'])){
                    $validacion['num_total_feriados']=0;
                }

                #array de datos
                $datos = array(
                    $validacion['fk_a001_num_organismo'],
                    $validacion['ind_descripcion'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['ind_periodo'],
                    $validacion['fec_inicio_periodo'],
                    $validacion['fec_fin_periodo'],
                    $validacion['num_dias_periodo'],
                    $validacion['num_dias_pago'],
                    $validacion['num_total_feriados'],
                    $validacion['num_valor_diario'],
                    $validacion['fec_horas_diarias'],
                    $validacion['fec_horas_semanales'],
                    $validacion['num_valor_semanal'],
                    $validacion['num_valor_mes'],
                    $validacion['fk_b002_num_partida_presupuestaria'],
                    $validacion['cod_partida'],
                    $validacion['ind_denominacion_partida'],
                    $validacion['fk_cpb002_tipo_documento'],
                    $validacion['ind_cod_presupuesto'],
                    $validacion['fk_prb004_num_presupuesto'],
                    $validacion['ind_descripcion_descuento'],
                    $validacion['num_monto_descuento'],
                    $validacion['num_monto_descuento_otros']
                );

                $id = $this->atBonoAlimentacion->metModificarBonoAlimentacion($idBonoAlimentacion,$datos,$cad_empleados);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status'] = 'OK_mod';
                    echo json_encode($validacion);
                    exit;
                }


            }


        }

        $this->atVista->assign('listadoDocumento', $this->atObligacion->metDocumentoListar());
        $periodo = date("Y-m");#periodo actual
        $fecha_ini = date("d-m-Y");#fecha inicio
        $fecha_fin = $this->metUltimoDiaMes(date("Y"),date("m"));
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNomina->metListarTipoNomina());
        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha_ini',$fecha_ini);
        $this->atVista->assign('fecha_fin',$fecha_fin);

        $this->atVista->assign('idBonoAlimentacion',$idBonoAlimentacion);
        $this->atVista->assign('proceso','Nuevo');
        //$this->atVista->assign('id_empleados',$id_empleados);
        $this->atVista->metRenderizar('form','modales');
    }

    #PERMITE LISTAR LAS PARTIDAS PRESUPUESTARI PARA PODER SELECCIONARIOS Y AGREGARLOS AL FORMULARIO
    public function metListarPartidas()
    {
        $partidas = $this->atPartidas->metListarPartida();
        // Envío a la vista
        $this->atVista->assign('lista',$partidas);
        $this->atVista->metRenderizar('partidas','modales');

    }

    #PERMITE LISTAR LOS PRESUPUESTOS PARA PODER SELECCIONARIOS Y AGREGARLOS AL FORMULARIO
    public function metListarPresupuesto()
    {
        $presupuesto = $this->atPresupuesto->metListarPresupuesto(false);
        // Envío a la vista
        $this->atVista->assign('lista',$presupuesto);
        $this->atVista->metRenderizar('presupuesto','modales');

    }

    #VISUALIZAR EMPLEADOS DEL PERIODO PARA REGISTRAR EVENTOS
    public function metRegistrarEventos($bonoAlimentacion)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        //datos encabezado
        $datos = $this->atBonoAlimentacion->metMostrarBonoAlimentacionEmpleadosDetalle($bonoAlimentacion,'encabezado','');
        $this->atVista->assign('datos',$datos);

        //datos empleados
        $datosEmpleados = $this->atBonoAlimentacion->metMostrarBonoAlimentacionEmpleadosDetalle($bonoAlimentacion,'completo','');
        $this->atVista->assign('datosEmpleados',$datosEmpleados);

        #asigno datos a la vista
        $this->atVista->assign('idBonoAlimentacion',$bonoAlimentacion);
        $this->atVista->assign('i',1);

        #renderizo la vista
        $this->atVista->metRenderizar('listadoEmpleadoPeriodo');

    }

    #VISUALIZAR DATOS DEL EMPLEADO PARA REGISTRAR LOS EVENTOS
    public function metRegistrarEventosEmpleado($bonoAlimentacion,$empleado)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        //datos encabezado del periodo
        $datos = $this->atBonoAlimentacion->metMostrarBonoAlimentacionEmpleadosDetalle($bonoAlimentacion,'encabezado','');
        $this->atVista->assign('datos',$datos);

        //datos todos los empleados del periodo
        $datosEventos = $this->atBonoAlimentacion->metListarEventosEmpleado($bonoAlimentacion,$empleado,'','TODOS');
        $this->atVista->assign('datosEventos',$datosEventos);

        //datos solo del empleado
        $datosSoloEmpleados = $this->atBonoAlimentacion->metMostrarBonoAlimentacionEmpleadosDetalle($bonoAlimentacion,'empleado',$empleado);
        $this->atVista->assign('datosSoloEmpleados',$datosSoloEmpleados);

        #asigno datos a la vista
        $this->atVista->assign('idBonoAlimentacion',$bonoAlimentacion);
        $this->atVista->assign('i',1);

        #renderizo la vista
        $this->atVista->metRenderizar('listadoEmpleadoEventos');

    }


    #LEVANTA INTERFAZ FORMULARIO PARA REGISTRAR EL EVENTO DEL EMPLEADO
    public function metEventosEmpleado()
    {

        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min'
        );
        $js = array ('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $idBonoAlimentacion = $this->metObtenerInt('idBonoAlimentacion');
        $idEvento = $this->metObtenerInt('idEvento');
        $organismo = $this->metObtenerInt('organismo');
        $periodo  = $this->metObtenerTexto('periodo');
        $valido   = $this->metObtenerInt('valido');

        if($valido==1){

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formForm  = $this->metObtenerFormulas('form','formulas');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt]=$valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }
            if(!empty($formForm)) {
                foreach ($formForm as $tituloTxt => $valorTxt) {
                    if (!empty($formForm[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idEvento===0){#insertar

                /*armo array de datos*/
                $datos = array(
                    $validacion['fec_fecha'],
                    $validacion['fec_hora_salida'],
                    $validacion['fec_hora_entrada'],
                    $validacion['fec_total_horas'],
                    $validacion['fk_a006_num_miscelaneo_motivo_ausencia'],
                    $validacion['fk_a006_num_miscelaneo_tipo_ausencia'],
                    $validacion['txt_observaciones'],
                    $organismo
                );

                $id = $this->atBonoAlimentacion->metRegistrarEventosEmpleado($idBonoAlimentacion,$idEmpleado,$datos);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atBonoAlimentacion->metListarEventosEmpleado($idBonoAlimentacion,$idEmpleado,$id,'ID');
                    $validacion['idEvento'] = $id;
                    $validacion['status'] = 'creacion';
                    $validacion['mot_ausencia'] = $datos['mot_ausencia'];
                    $validacion['tip_ausencia'] = $datos['tip_ausencia'];
                    echo json_encode($validacion);
                    exit;
                }


            }else{#modificar

                /*armo array de datos*/
                $datos = array(
                    $validacion['fec_fecha'],
                    $validacion['fec_hora_salida'],
                    $validacion['fec_hora_entrada'],
                    $validacion['fec_total_horas'],
                    $validacion['fk_a006_num_miscelaneo_motivo_ausencia'],
                    $validacion['fk_a006_num_miscelaneo_tipo_ausencia'],
                    $validacion['txt_observaciones'],
                    $organismo
                );

                $id = $this->atBonoAlimentacion->metModificarEventosEmpleado($idBonoAlimentacion,$idEmpleado,$datos,$idEvento);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atBonoAlimentacion->metListarEventosEmpleado($idBonoAlimentacion,$idEmpleado,$idEvento,'ID');
                    $validacion['status'] = 'modificar';
                    $validacion['idEvento'] = $id;
                    $validacion['mot_ausencia'] = $datos['mot_ausencia'];
                    $validacion['tip_ausencia'] = $datos['tip_ausencia'];
                    echo json_encode($validacion);
                    exit;
                }

            }


        }//fin $valido

        if($idEvento!=0){
            $db=$this->atBonoAlimentacion->metListarEventosEmpleado($idBonoAlimentacion,$idEmpleado,$idEvento,'ID');
            $this->atVista->assign('formDB',$db);
        }


        #desconpongo el periodo para separar anio y mes
        $p = explode("-",$periodo);
        $anio = $p[0];
        $mes  = $p[1];

        #obtengo el numero de dias del mes
        $dias_mes = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
        #armo el array de fechas
        $fecha = array();
        for($i = 1; $i <= $dias_mes; $i++) {

            $dia = date("N", strtotime("".$anio."-".$mes."-".$i.""));
            if($dia==6 || $dia==7){
                //no lo agrego al array por que sabado o domingo
            }else{
                $fecha[] = $i."-".$mes."-".$anio;
            }


        }

        $this->atPermisoModelo = $this->metCargarModelo('permiso','gestion');
        // Obtengo el horario del empleado
        $horarioLaboral = $this->atPermisoModelo->metConsultarHorario($idEmpleado);
        $h_entrada1 = $horarioLaboral['fec_entrada1'];
        $h_salida1  = $horarioLaboral['fec_salida1'];
        $h_entrada2  = $horarioLaboral['fec_entrada2'];
        $h_salida2  = $horarioLaboral['fec_salida2'];
        if($h_entrada1!='' && $h_salida1!='' && $h_entrada2!='' && $h_salida2!=''){
            $entrada = $h_entrada1;
            $salida  = $h_salida2;
        }else{
            $entrada = $h_entrada1;
            $salida  = $h_salida1;
        }
        $array = $this->metHorarioEstandar($entrada,$salida);
        $entrada = $array[0].":".$array[1]." ".$array[4];
        $salida  = $array[2].":".$array[3]." ".$array[5];
        $form = array(
            'horaE' => $entrada,
            'horaS' => $salida
        );

        //MISCELNAEOS MOTIVO DE AUSENCIA Y TIPO DE EVENTO
        $miscMotivoAusencia = $this->atBonoAlimentacion->metMostrarSelect('MOTAUS');
        $miscTipoEvento     = $this->atBonoAlimentacion->metMostrarSelect('TIPAUS');
        $this->atVista->assign('MotivoAusencia',$miscMotivoAusencia);
        $this->atVista->assign('TipoEvento',$miscTipoEvento);

        $this->atVista->assign('hora', $form);
        $this->atVista->assign('i',0);
        $this->atVista->assign('fecha',$fecha);
        $this->atVista->assign('idBonoAlimentacion',$idBonoAlimentacion);
        $this->atVista->assign('idEvento',$idEvento);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('organismo',$organismo);
        $this->atVista->metRenderizar('eventos', 'modales');

    }


    #PROCESAR EVENTOS DEL EMPLEADO
    public function metProcesarEventosEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $idBonoAlimentacion = $this->metObtenerInt('idBonoAlimentacion');
        $organismo = $this->metObtenerInt('organismo');
        $periodo  = $this->metObtenerTexto('periodo');

        $procesar = $this->atBonoAlimentacion->metProcesarEventosEmpleado($idBonoAlimentacion,$idEmpleado);

        echo json_encode($procesar);

    }


    #PERMITE CALCULAR LA DEFIRENCIA QUE EXISTE ENTRE 2 HORAS
    public function metCalcularDiferenciaHoras()
    {
        $hora_salida  = $this->metObtenerFormulas('hora_salida');
        $hora_entrada = $this->metObtenerFormulas('hora_entrada');
        $empleado = $this->metObtenerInt('empleado');
        $fecha = $this->metObtenerTexto('fecha');

        $diferencia = $this->atFunciones->metDiferenciaHorasEventos($empleado,$fecha,$hora_salida,$hora_entrada);

        echo json_encode($diferencia);
        exit;


    }

    #PERMITE VISUALIZAR EL DETALLE DE LOS EVENTOS EN EL PERIODO DEL EMPLEADO
    public function metVerDetalleEventos()
    {
        $empleado = $this->metObtenerInt('idEmpleado');
        $bono_alimentacion = $this->metObtenerInt('idBonoAlimentacion');
        $organismo = $this->metObtenerInt('organismo');
        $nombre = $this->metObtenerTexto('nombre');

        $datos_gen = $this->atBonoAlimentacion->metMostrarBonoAlimentacionEmpleadosDetalle($bono_alimentacion,'encabezado','');

        $detalle_eventos = $this->atBonoAlimentacion->metVerDetalleEventos($empleado,$bono_alimentacion,$organismo);

        $this->atVista->assign('nombre',$nombre);
        $this->atVista->assign('detalle',$detalle_eventos);
        $this->atVista->assign('generales',$datos_gen);
        $this->atVista->assign('i',1);
        $this->atVista->assign('id','num_dia');
        $this->atVista->assign('color','');

        $this->atVista->metRenderizar('detalleEventosPeriodo','modales');

    }

    #REGISTRAR EVENTOS DETALLE
    public function metRegistrarEventosDetalle($bonoAlimentacion)
    {
        //consulto los empleados beneficiados en este periodo para mostrarlos en el listado
        $datos = $this->atBonoAlimentacion->metMostrarBonoAlimentacionEmpleadosDetalle($bonoAlimentacion,'completo');

        echo json_encode($datos);
        exit;

    }

    public function metVer()
    {

        $bonoAlimentacion = $this->metObtenerInt('idBonoAlimentacion');

        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/mask'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoTableDynamic','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('idBonoAlimentacion',$bonoAlimentacion);
        $this->atVista->assign('proceso','Ver');

        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNomina->metListarTipoNomina());

        $datosPrincipales = $this->atBonoAlimentacion->metMostrarBonoAlimentacion($bonoAlimentacion);
        $this->atVista->assign('formDB',$datosPrincipales);



        $this->atVista->assign('idBonoAlimentacion',$bonoAlimentacion);
        //$this->atVista->assign('id_empleados',$id_empleados);
        $this->atVista->metRenderizar('form','modales');

    }

    public function metMod()
    {

        $bonoAlimentacion = $this->metObtenerInt('idBonoAlimentacion');

        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/mask'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoTableDynamic','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('idBonoAlimentacion',$bonoAlimentacion);
        $this->atVista->assign('proceso','Modificar');

        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNomina->metListarTipoNomina());

        $datosPrincipales = $this->atBonoAlimentacion->metMostrarBonoAlimentacion($bonoAlimentacion);
        $this->atVista->assign('formDB',$datosPrincipales);

        $this->atVista->assign('listadoDocumento', $this->atObligacion->metDocumentoListar());

        $this->atVista->assign('idBonoAlimentacion',$bonoAlimentacion);
        //$this->atVista->assign('id_empleados',$id_empleados);
        $this->atVista->metRenderizar('form','modales');

    }

    #permite listar todos los empleados en el bono alimentacion
    public function metMostrarBonoAlimentacionEmpleados()
    {

        $bonoAlimentacion = $this->metObtenerInt('bono_alimentacion');

        //consulto los empleados beneficiados en este periodo para mostrarlos en el listado
        $datosEmpleados = $this->atBonoAlimentacion->metMostrarBonoAlimentacionEmpleados($bonoAlimentacion);

        echo json_encode($datosEmpleados);
        exit;

    }

    #permite agregar empleados  a la lista de beneficados del bono de alimentacion
    public function metAgregarBeneficiarios()
    {

        $complementosCss = array(
            'select2/select201ef',

        );
        $complementoJs = array(
            'select2/select2.min',
        );
        $js = array('materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $organismo = $this->metObtenerInt('organismo');
        $tipo_nomina = $this->metObtenerInt('tipo_nomina');

        $listaEmpleados = $this->atEmpleados->metListarEmpleadoNomina($tipo_nomina);
        $this->atVista->assign('datosDependencias', $this->atDependencias->metConsultarDependencias());
        $this->atVista->assign('datosOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNomina->metListarTipoNomina());
        $this->atVista->assign('listasEmpleados', $listaEmpleados);
        $this->atVista->assign('tipo_nomina', $tipo_nomina);
        $this->atVista->metRenderizar('agregarEmpleados', 'modales');

    }


    #permite hacer la busqueda de empleados segun la dependencia, org, nomina
    public function metFiltrarEmpleados()
    {

        $organismo   = $this->metObtenerInt('organismo');
        $dependencia = $this->metObtenerInt('dependencia');
        $tipo_nomina = $this->metObtenerInt('tipo_nomina');

        if($dependencia == 0){
            $datos = $this->atBonoAlimentacion->metListarEmpleados('1',$organismo,'',$tipo_nomina);//solo por organismo
        }else{
            $datos = $this->atBonoAlimentacion->metListarEmpleados('2',$organismo, $dependencia,$tipo_nomina);//por dependencia
        }

        echo json_encode($datos);

    }


    #listado de periodos bono de alimentacion
    public function metListar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('proceso','eventos');

        $this->atVista->assign('listadoBonoAlimentacion', $this->atBonoAlimentacion->metListarBonoAlimentacion());
        $this->atVista->metRenderizar('listado');
    }

    #permite cerrar un periodo
    public function metCerrarPeriodo()
    {
        $bono_alimentacion = $this->metObtenerInt('idBonoAlimentacion');

        $id = $this->atBonoAlimentacion->metCerrarPeriodo($bono_alimentacion);

        if(is_array($id)){
            $validacion['status'] = 'error';
            $validacion['detalleERROR'] = $id;
            echo json_encode($validacion);
            exit;
        }else{
            $validacion['status'] = 'OK';
            echo json_encode($validacion);
            exit;
        }

    }


    #PERMITE ELIMINAR UN EVENTO
    public function metEliminarEventoEmpleado()
    {
        $idEvento = $this->metObtenerInt('idEvento');

        if($idEvento!=0){
            $id=$this->atBonoAlimentacion->metEliminarEventoEmpleado($idEvento);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idEvento'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    #permite visualizar ultimo dia del mes dado
    public function metUltimoDiaMes($anio,$mes)
    {
        $dia = date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));

        $fecha = $dia."-".$mes."-".$anio;

        return $fecha;
    }

    #permite obtener el total de dia del periodo, de pago, feriados y valor diario
    public function metCalcularDiasPeriodoBono()
    {
        $fecha_ini = $this->metObtenerTexto('fecha_ini');
        $fecha_fin = $this->metObtenerTexto('fecha_fin');

        $datos = array('fecha_ini'=>$fecha_ini,'fecha_fin'=>$fecha_fin);

        #obtener paramateros para calculos de horas diras y semanales
        $horas_diarias = Session::metObtener('HORADIR');
        $horas_semanales = Session::metObtener('HORADIR') * Session::metObtener('HORDIAS');
        $max_dias_pago = Session::metObtener('MAXDIASBONO');
        $modalidad = Session::metObtener('MODDIASBONO');
        $v_diario  = $this->atBonoAlimentacion->metObtenerUT(Session::metObtener('UTANIO'));
        $porc = Session::metObtener('UTPORC') / 100;

        if($modalidad==1){
            //INDICA QUE SE VA A TOMAR OBLIGATORIAMENTE LOS 30 DIAS DE PAGO QUE INDICA LA LEY PARAMETRO (MAXDIASBONO)
            $dias_periodo = $max_dias_pago;
            $aux = $porc * $v_diario['ind_valor'];
            $mes = $aux * $dias_periodo;
            $valor_dia = $mes / $dias_periodo;
        }else{
            //INDICA QUE SE VA A TOMAR COMO REFERENCIA DE CALCULO LA FECHA DE INICIO Y FECHA FINAL DEL PERIODO.
            $dias_periodo = $this->atBonoAlimentacion->metObtenerDiasFecha($fecha_ini,$fecha_fin)+1;
            $valor_dia = ( $v_diario['ind_valor'] * Session::metObtener('UTPORC')) / 100;
        }

        $feriados  = $this->atFeriados->metObtenerDiasFeriados($fecha_ini,$fecha_fin);

        $valor_semanal = $valor_dia * Session::metObtener('DIASEMBONO');

        if(strcmp(Session::metObtener('UTPAGOMES'),'SI')==0){
            //solo 30 dias
            $dias_pago = $dias_periodo;

        }else if(strcmp(Session::metObtener('UTPAGOMES'),'NO')==0){

            $dias_pago = $this->atFeriados->metObtenerDiasHabiles($fecha_ini,$fecha_fin);

        }

        $valor_mes = $valor_dia * $dias_pago;

        $data = array(
            'num_dias_pago' => $dias_pago,
            'num_total_feriados' => $feriados,
            'num_valor_diario' => number_format($valor_dia, 2, ',', '.'),
            'num_dias_periodo' => $dias_periodo,
            'num_valor_mes' => number_format($valor_mes, 2, ',', '.'),
            'fec_horas_diarias' => $horas_diarias,
            'fec_horas_semanales' => $horas_semanales,
            'num_valor_semanal' => number_format($valor_semanal, 2, ',', '.')
        );

        echo json_encode($data);
        exit;
    }


    #permite importar todos los empleados de acuerdo al tipo de nomina
    public function metImportarEmpleados()
    {
        $tipo_nomina = $this->metObtenerInt('tipo_nomina');
        $organismo   = $this->metObtenerInt('organismo');

        $empleados   = $this->atBonoAlimentacion->metImportarEmpleados($organismo,$tipo_nomina);

        echo json_encode($empleados);
        exit;

    }

    // Método que permite transformar las horas de horario militar a horario estándar
    public function metHorarioEstandar($horaSalida, $horaEntrada)
    {
        // Hora de salida
        $horaExplodeInicio = explode(":", $horaSalida);
        $horaInicio = $horaExplodeInicio[0];
        $minutoInicio = $horaExplodeInicio[1];

        // Hora de entrada
        $horaExplodeFin = explode(":", $horaEntrada);
        $horaFin = $horaExplodeFin[0];
        $minutoFin = $horaExplodeFin[1];

        // Convirtiendo a horario estándar
        // Hora Inicio
        if($horaInicio>12){
            $horaInicioPrevio = $horaInicio - 12;
            $hora_inicio = str_pad($horaInicioPrevio, 2, "0", STR_PAD_LEFT);
        }
        if($horaInicio<=12) {
            $hora_inicio = $horaInicio;
        }
        // Hora Fin
        if($horaFin>12){
            $horaFinPrevio = $horaFin - 12;
            $hora_fin = str_pad($horaFinPrevio, 2, "0", STR_PAD_LEFT);
        }
        if($horaFin<=12) {
            $hora_fin = $horaFin;
        }
        // Horario
        // Horario Inicio
        if($horaInicio>=12){
            $horarioInicio = 'PM';
        }
        if($horaInicio<12) {
            $horarioInicio = 'AM';
        }
        //Horario Fin
        if($horaFin>=12){
            $horariofin = 'PM';
        }
        if($horaFin<12) {
            $horariofin = 'AM';
        }
        // Los agrupo en un array
        $datoHorario = array($hora_inicio,$minutoInicio, $hora_fin, $minutoFin, $horarioInicio, $horariofin);
        return $datoHorario;
    }

    #INTERFAZ DE CUENTAS POR PAGAR (BONO ALIMENTACION)
    public function metInterfazCxp()
    {
        $js[] = 'modRH/modRHFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina(1));
        $this->atVista->metRenderizar('interfazcxp');
    }

    public function metPeriodosDisponibles()
    {
        $idNomina = $this->metObtenerInt('idNomina');
        $idOrganismo = $this->metObtenerInt('idOrganismo');
        $periodos = $this->atBonoAlimentacion->metListarPeriodosDisponibles($idOrganismo,$idNomina);
        echo json_encode($periodos);
        exit;
    }

    public function metProcesosDisponibles()
    {
        $Proceso = $this->metObtenerTexto('Periodo');
        $idNomina = $this->metObtenerInt('idNomina');
        $idOrganismo = $this->metObtenerInt('idOrganismo');
        $procesos = $this->atBonoAlimentacion->metListarProcesosDisponibles($idOrganismo,$Proceso,$idNomina);
        echo json_encode($procesos);
        exit;
    }

    public function metListadoEmpleadosPeriodoCalculo()
    {
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);

        $pk_num_tipo_nomina = $this->metObtenerInt('pk_num_tipo_nomina');
        $periodo = $this->metObtenerTexto('periodo');

        if ($pk_num_tipo_nomina && $periodo) {

            $this->atVista->assign('listado', $this->atBonoAlimentacion->metCalculoBonoAlimentacion($pk_num_tipo_nomina, $periodo));
            $this->atVista->metRenderizar('interfazcxpDetalle', 'modales');
        }
    }




}
