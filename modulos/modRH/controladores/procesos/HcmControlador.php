<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 | José Pereda          | dt.ait.programador2@cgesucre.gob.ve  | 04248040078
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once ROOT.'librerias'.DS.'numTotext.php';

class HcmControlador extends Controlador
{
    private $atBeneficioHcm;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.

        $this->atBeneficioHcm = $this->metCargarModelo('Hcm', 'procesos');
        $this->atinstitucionHcm = $this->metCargarModelo('institucionHcm', 'maestros');
        $this->atMedicosHcm = $this->metCargarModelo('medicosHcm', 'maestros');
        $this->atBeneficiosGlobalHcm = $this->metCargarModelo('beneficiosGlobalHcm', 'maestros');
        $this->atBeneficiosHcm = $this->metCargarModelo('beneficiosHcm', 'maestros');
    }

    #Metodo Index del controlador
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',

        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
        );

        $js = array(

            'materialSiace/core/demo/DemoFormComponents',
            'materialSiace/core/demo/DemoTableDynamic'

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('BeneficiosPost', $this->atBeneficioHcm->atbeneficiosHcmModelo->metConsultarBeneficiosHcm());
        $this->atVista->assign('Ayuda', $this->atBeneficioHcm->atbeneficiosGlobalHcmModelo->metConsultarBeneficioGlobal());
        $this->atVista->assign('EstadoHCM', $this->atBeneficioHcm->metListarEstadoHCM());
        $this->atVista->assign('_EmpleadoPost', $this->atBeneficioHcm->atbeneficiosHcmModelo->metListarEmpleados());
        $this->atVista->assign('Beneficio', $this->atBeneficioHcm->metListarSolicitudBeneficioMedico());
        $this->atVista->metRenderizar('listado');


    }


    public function metRevisarSolicitud()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Beneficio', $this->atBeneficioHcm->metListarSolicitudBeneficioEstatus(1));
        $this->atVista->metRenderizar('listadoRevisar');


    }

    public function metAprobarSolicitud()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Beneficio', $this->atBeneficioHcm->metListarSolicitudBeneficioEstatus(2));
        $this->atVista->metRenderizar('listadoAprobar');

    }

    public function metBitacora()
    {
        $idHcm = $this->metObtenerInt('idHcm');
        $this->atVista->assign('_Bitacora', $this->atBeneficioHcm->metListarBitacoraHCM($idHcm));
        $this->atVista->metRenderizar('ListarMovimientos', 'modales');
    }


    public function metNuevoBeneficio()
    {
        $facturas          = $this->metObtenerInt('facturas');
        $valido            = $this->metObtenerInt('valido');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
        );

        $Accion=array( "accion" => "registrar",);

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);

        if($valido==1) {



            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    if ((strcmp($tituloInt, 'num_limite') == 0)) {
                        $validacion[$tituloInt] = str_replace(",", ".", preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    } else {
                        $validacion[$tituloInt] = $valorInt;
                    }
                } else {
                    $validacion[$tituloInt] = '';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt])) {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = '';

                }
            }


            if (!empty($validacion['flag_planilla_solicitud'])) {
                $validacion['flag_planilla_solicitud'] = 1;
            } else {
                $validacion['flag_planilla_solicitud'] = 0;
            }
            if (!empty($validacion['flag_informe_medico'])) {
                $validacion['flag_informe_medico'] = 1;
            } else {
                $validacion['flag_informe_medico'] = 0;
            }

            if (!empty($validacion['flag_factura'])) {
                $validacion['flag_factura'] = 1;
            } else {
                $validacion['flag_factura'] = 0;
            }

            if (!empty($validacion['flag_recipe_medico'])) {
                $validacion['flag_recipe_medico'] = 1;
            } else {
                $validacion['flag_recipe_medico'] = 0;
            }

            if (!empty($validacion['flag_otro'])) {
                $validacion['flag_otro'] = 1;
            } else {
                $validacion['flag_otro'] = 0;
            }

            if (!isset($validacion['rama']) || !empty($validacion['rama'])) {
                $validacion['rama'] = 'NULL';
            }

            if (!isset($validacion['institucion']) || !empty($validacion['medico'])) {
                $validacion['rama'] = 'NULL';
            }

            if (!isset($validacion['medico']) || !empty($validacion['medico'])) {
                $validacion['rama'] = 'NULL';
            }



            if($validacion['num_empleado_beneficio']==''){

               $validacion['status'] = 'error2';
               $validacion['detalleERROR'] = " No ha seleccionado el empleado";
               echo json_encode($validacion);
               exit;


            }/*else if($validacion['rama']=='' || $validacion['institucion']=='' || $validacion['medico']==''){

               $validacion['rama']=NULL;
               $validacion['institucion']=NULL;
               $validacion['medico']=NULL;
               /*$validacion['status'] = 'error2';
               $validacion['detalleERROR'] = " Hay campos sin seleccionar en el paso 2 del formulario";
               echo json_encode($validacion);
               exit;

           }*/else{

               $validacion['total']=str_replace(",", ".", $validacion['total']);
               $idBeneficio = $this->atBeneficioHcm->metRegistrarBeneficioPersonalHcm($validacion['organismo'], $validacion['tipo_solicitud'], $validacion['num_empleado_beneficio'], $validacion['carga_familiar'], $validacion['num_ayuda_global'], $validacion['num_beneficio'], $validacion['rama'], $validacion['institucion'], $validacion['medico'], $validacion['flag_planilla_solicitud'], $validacion['flag_informe_medico'], $validacion['flag_factura'], $validacion['flag_recipe_medico'], $validacion['flag_otro'],  $validacion['total']);

           }



            if(is_array($idBeneficio)){

                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $idBeneficio;
                echo json_encode($validacion);
                exit;

            }else{

                $this->atBeneficioHcm->metRegistrarBitacoraHcm($idBeneficio , 1);

                    if ($idBeneficio != 0) {
                        for ($i = 1; $i <= $facturas; $i++) {

                            if (!empty($validacion["factura" . $i])){

                                $this->atBeneficioHcm->metRegistrarFacturasHcm($idBeneficio, $validacion["factura" . $i], $validacion["descripcion" . $i], $validacion["montofactura" . $i]);

                            }

                        }
                    }

                /*$respuesta =$this->atBeneficioHcm->metMostrarID($idBeneficio);
                $datos = [];

                $datos['pk_beneficio_medico']=$idBeneficio;
                $datos['tipo']=$respuesta[0]['ind_nombre_detalle'];
                $datos['funcionario']=$respuesta[0]['ind_nombre1']." ".$respuesta[0]['ind_apellido1'];
                $datos['beneficio']=$respuesta[0]['servicio'];
                $datos['monto']=$respuesta[0]['num_monto'];
                $datos['estado']=$respuesta[0]['estado'];*/
                $datos['status']='registrar';
                echo json_encode($datos);
                exit;
            }


        }

        $this->atVista->assign('_MedicosPost', $this->atMedicosHcm->metConsultarMedicos());
        $this->atVista->assign('_InstitutucionPost', $this->atinstitucionHcm->metConsultarInstitucion());
        $this->atVista->assign('_EmpleadoPost', $this->atBeneficioHcm->atbeneficiosHcmModelo->metListarEmpleados());
        $this->atVista->assign('_OrganismoPost', $this->atBeneficioHcm->atOrganismoModelo->metListarOrganismos());
        $this->atVista->assign('_TipoSolicitudPost', $this->atBeneficioHcm->atMiscelaneoModelo->metMostrarSelect('SOLHCM'));
        $this->atVista->assign('_RamasPost', $this->atBeneficioHcm->atMiscelaneoModelo->metMostrarSelect('RAMAS'));
        $this->atVista->assign('_AyudaPost', $this->atBeneficioHcm->atbeneficiosGlobalHcmModelo->metConsultarBeneficioGlobal());

        $this->atVista->assign('_BeneficiosPost', $this->atBeneficioHcm->atbeneficiosHcmModelo->metConsultarBeneficiosHcm());

        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('form','modales');


    }

    public function metModificarBeneficio()
    {
        $facturas = $this->metObtenerInt('facturas');
        $id = $this->metObtenerInt('idPost');
        $idHcm = $this->metObtenerInt('idHcm');
        $valido = $this->metObtenerInt('valido');
        $tipo = $this->metObtenerAlphaNumerico('tipo');
	 
		
	    if($valido==1){

 

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    if((strcmp($tituloInt,'num_limite')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            

            if (!empty($validacion['flag_planilla_solicitud'])) {
                $validacion['flag_planilla_solicitud'] = 1;
            } else {
                $validacion['flag_planilla_solicitud'] = 0;
            }
            if (!empty($validacion['flag_informe_medico'])) {
                $validacion['flag_informe_medico'] = 1;
            } else {
                $validacion['flag_informe_medico'] = 0;
            }

            if (!empty($validacion['flag_factura'])) {
                $validacion['flag_factura'] = 1;
            } else {
                $validacion['flag_factura'] = 0;
            }

            if (!empty($validacion['flag_recipe_medico'])) {
                $validacion['flag_recipe_medico'] = 1;
            } else {
                $validacion['flag_recipe_medico'] = 0;
            }

            if (!empty($validacion['flag_otro'])) {
                $validacion['flag_otro'] = 1;
            } else {
                $validacion['flag_otro'] = 0;
            }

            if (!isset($validacion['rama']) || !empty($validacion['rama'])) {
                $validacion['rama'] = 'NULL';
            }

            if (!isset($validacion['institucion']) || !empty($validacion['medico'])) {
                $validacion['rama'] = 'NULL';
            }

            if (!isset($validacion['medico']) || !empty($validacion['medico'])) {
                $validacion['rama'] = 'NULL';
            }


 
           if($validacion['num_empleado_beneficio']==''){

               $validacion['status'] = 'error2';
               $validacion['detalleERROR'] = " No ha seleccionado el empleado";
               echo json_encode($validacion);
               exit;


            }/*else if($validacion['rama']==''||$validacion['institucion']==''||$validacion['medico']==''){

                SE DESABILITO ESTA OPCION
               $validacion['status'] = 'error2';
               $validacion['detalleERROR'] = " Hay campos sin seleccionar en el paso 2 del formulario";
               echo json_encode($validacion);
               exit;

           }*/else{

               //$validacion['total']=str_replace(",", ".", $validacion['total']);
               $this->atBeneficioHcm->metdeleteFacturasId($idHcm);
			    
	
	
				if($validacion['accion']=="revisar"){
					$this->atBeneficioHcm->metBeneficioActualizar($idHcm,'2');
					$this->atBeneficioHcm->metRegistrarBitacoraHcm( $idHcm , 2);
                    #APLICAMOS LOS CAMBIOS REALIZADOS EN EL FORMULARIO
                    $this->atBeneficioHcm->metModificarBeneficioPersonalHcm($validacion['organismo'],$validacion['tipo_solicitud'],$validacion['num_empleado_beneficio'],$validacion['carga_familiar'] ,$validacion['num_ayuda_global'],$validacion['num_beneficio'],$validacion['rama'],$validacion['institucion'],$validacion['medico'] ,$validacion['flag_planilla_solicitud'],$validacion['flag_informe_medico'],$validacion['flag_factura'],$validacion['flag_recipe_medico'],$validacion['flag_otro'], $validacion['total'],$idHcm);

                }else if($validacion['accion']=="aprobar"){
					$this->atBeneficioHcm->metBeneficioActualizar($idHcm,'3');
					$this->atBeneficioHcm->metRegistrarBitacoraHcm( $idHcm , 3);
				}else if($validacion['accion']=="modificar"){
						  $this->atBeneficioHcm->metRegistrarBitacoraHcm( $idHcm , 5);
                    #APLICAMOS LOS CAMBIOS REALIZADOS EN EL FORMULARIO
                    $this->atBeneficioHcm->metModificarBeneficioPersonalHcm($validacion['organismo'],$validacion['tipo_solicitud'],$validacion['num_empleado_beneficio'],$validacion['carga_familiar'] ,$validacion['num_ayuda_global'],$validacion['num_beneficio'],$validacion['rama'],$validacion['institucion'],$validacion['medico'] ,$validacion['flag_planilla_solicitud'],$validacion['flag_informe_medico'],$validacion['flag_factura'],$validacion['flag_recipe_medico'],$validacion['flag_otro'], $validacion['total'],$idHcm);

                }else{

                    $this->atBeneficioHcm->metRegistrarBitacoraHcm($idHcm , 1);
                }
			   


                  
           


                    if ($idHcm != 0) {
				        $total=0;
                        for ($i = 1; $i <= $facturas; $i++) {

                            if (!empty($validacion["factura" . $i])){
 
                                $this->atBeneficioHcm->metRegistrarFacturasHcm($idHcm, $validacion["factura" . $i], $validacion["descripcion" . $i], ($this->metPrecio($validacion["montofactura" . $i]))/100);
								 
                				$total=$total+$this->metPrecio($validacion["montofactura".$i]);
                            }

                        }
                    }

                   

                /*$respuesta =$this->atBeneficioHcm->metMostrarPdf($idHcm);
                $datos = [];
                $datos['pk_beneficio_medico']=$idHcm;
                $datos['tipo']=$respuesta[0]['ind_nombre_detalle'];
                $datos['funcionario']=$respuesta[0]['ind_nombre1']." ".$respuesta[0]['ind_apellido1'];
                $datos['beneficio']=$respuesta[0]['servicio'];
                $datos['monto']=$respuesta[0]['num_monto'];
                $datos['estado']=$respuesta[0]['estado'];*/
                $datos['status']='modificar';
                echo json_encode($datos);
                exit;
           }

            

  
        }
		
		
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
        );
		
		

        $Accion=array( "accion" => $tipo,);

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
 
 

        $beneficio=$this->atBeneficioHcm->metListarSolicitudBeneficioMedicoId($id);

        $this->atVista->assign('_formDB', $beneficio);

        $this->atVista->assign('_FacturasDB', $this->atBeneficioHcm->metListarSolicitudBeneficioMedicoFacturaId($id));

        $this->atVista->assign('_CargaFamiliarDB', $this->atBeneficioHcm->atcargaFamiliarModelo->metListarCargaFamiliar( $beneficio[0]['fk_rhb001_empleado']));

        $this->atVista->assign('_MedicosPost', $this->atMedicosHcm->metConsultarMedicos());
        $this->atVista->assign('_InstitutucionPost', $this->atinstitucionHcm->metConsultarInstitucion());
        $this->atVista->assign('_EmpleadoPost', $this->atBeneficioHcm->atbeneficiosHcmModelo->metListarEmpleados());
        $this->atVista->assign('_OrganismoPost', $this->atBeneficioHcm->atOrganismoModelo->metListarOrganismos());
        $this->atVista->assign('_TipoSolicitudPost', $this->atBeneficioHcm->atMiscelaneoModelo->metMostrarSelect('SOLHCM'));
        $this->atVista->assign('_RamasPost', $this->atBeneficioHcm->atMiscelaneoModelo->metMostrarSelect('RAMAS'));
        $this->atVista->assign('_AyudaPost', $this->atBeneficioHcm->atbeneficiosGlobalHcmModelo->metConsultarBeneficioGlobal());

        $this->atVista->assign('_BeneficiosPost', $this->atBeneficioHcm->atbeneficiosHcmModelo->metConsultarBeneficiosHcm());

        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('form','modales');


    }

    public function metRevisarBeneficio()
    {
        $idHcm = $this->metObtenerInt('idHcm');
        $valido = $this->metObtenerInt('valido');

        if($valido==1){


            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    if((strcmp($tituloInt,'num_limite')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if (!empty($validacion['flag_planilla_solicitud'])) {
                $validacion['flag_planilla_solicitud'] = 1;
            } else {
                $validacion['flag_planilla_solicitud'] = 0;
            }
            if (!empty($validacion['flag_informe_medico'])) {
                $validacion['flag_informe_medico'] = 1;
            } else {
                $validacion['flag_informe_medico'] = 0;
            }

            if (!empty($validacion['flag_factura'])) {
                $validacion['flag_factura'] = 1;
            } else {
                $validacion['flag_factura'] = 0;
            }

            if (!empty($validacion['flag_recipe_medico'])) {
                $validacion['flag_recipe_medico'] = 1;
            } else {
                $validacion['flag_recipe_medico'] = 0;
            }

            if (!empty($validacion['flag_otro'])) {
                $validacion['flag_otro'] = 1;
            } else {
                $validacion['flag_otro'] = 0;
            }

            $resultado = $this->atBeneficioHcm->metBeneficioActualizar($idHcm,'2');
            if(is_array($resultado)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $resultado;
                echo json_encode($validacion);
                exit;
            }else{
                $bitacora = $this->atBeneficioHcm->metRegistrarBitacoraHcm($idHcm,2);
                if(is_array($bitacora)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $bitacora;
                    echo json_encode($validacion);
                    exit;
                }else{
                    #APLICAMOS LOS CAMBIOS REALIZADOS EN EL FORMULARIO
                    $this->atBeneficioHcm->metModificarBeneficioPersonalHcm($validacion['organismo'],$validacion['tipo_solicitud'],$validacion['num_empleado_beneficio'],$validacion['carga_familiar'] ,$validacion['num_ayuda_global'],$validacion['num_beneficio'],$validacion['rama'],$validacion['institucion'],$validacion['medico'] ,$validacion['flag_planilla_solicitud'],$validacion['flag_informe_medico'],$validacion['flag_factura'],$validacion['flag_recipe_medico'],$validacion['flag_otro'], $validacion['total'],$idHcm);
                    $validacion['status'] = 'Ok';
                    echo json_encode($validacion);
                    exit;
                }
            }
        }
    }


    public function metAprobarBeneficio()
    {
        $idHcm = $this->metObtenerInt('idHcm');
        $valido = $this->metObtenerInt('valido');

        if($valido==1){
            $resultado = $this->atBeneficioHcm->metBeneficioActualizar($idHcm,'3');
            if(is_array($resultado)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $resultado;
                echo json_encode($validacion);
                exit;
            }else{
                $bitacora = $this->atBeneficioHcm->metRegistrarBitacoraHcm($idHcm,3);
                if(is_array($bitacora)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $bitacora;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status'] = 'Ok';
                    echo json_encode($validacion);
                    exit;
                }
            }
        }
    }

    public function metDisponibleAsignacion()
    {
        $ayuda = $this->metObtenerInt('idBeneficioGlobal');
        $beneficio = $this->metObtenerInt('idAsignacion');
        $empleado  = $this->metObtenerInt('idEmpleado');
        $monto_asignado = $_POST['monto_asignado'];

        $this->atOperaciones = $this->metCargarModelo('beneficiosHcm','maestros');
        #RECOGEMOS EL CONSUMO DE LA PERSONA POR BENEFICIO ESPECIFICO
        $consulta=$this->atBeneficioHcm->metConsumidoPersonaBeneficio($empleado,$ayuda,$beneficio);

        $monto_asignado = $this->atBeneficioHcm->metMontoAsignadoDisponible($empleado,$ayuda,$beneficio);

        $monto = $consulta[0]['monto_sin_formato'];

        if($monto==Null){
            $monto='0.00';
        }else{
            $monto = $consulta[0]['monto_sin_formato'];
        }

        $disponible = $monto_asignado['num_monto_definitivo'] - $monto;
        $disponible = number_format($disponible,2,',','.');

        echo json_encode($disponible);
        exit;

    }


/*
    public function  metActualizarDisponibilidad()
    {

        $ayuda = $this->metObtenerInt('Ayuda');
        $beneficio = $this->metObtenerInt('Beneficio');
        $empleado = $this->metObtenerInt('Empleado');
        #RECOGEMOS EL DISPONIBLE TENIENDO EN CUENTA LA ASIGNACION GLOBLAL Y EL VALOR DEL BENEFICIO
        $consulta=$this->atBeneficioHcm->metActualizarDisponibilidad($ayuda,$beneficio);
        #RECOGEMOS EL CONSUMO TOTAL DE LA PERSONA
        $consulta2=$this->atBeneficioHcm->metConsumidoPersona($empleado,$ayuda);
        #RECOGEMOS EL CONSUMO DE LA PERSONA POR BENEFICIO ESPECIFICO
        $consulta3=$this->atBeneficioHcm->metConsumidoPersonaBeneficio($empleado,$ayuda,$beneficio);
        #VERIFICAMOS SI HAY UNA EXTENSION DEL BENEFICIO
        $HCMCONTROL = Session::metObtener('HCMCONTROL');
        if($HCMCONTROL==2){
            #sicnifica que esta activiado la funcionalidad para extender los beneficios
            $consultaExtension=$this->atBeneficioHcm->metBeneficioExtension($empleado,$ayuda);
            if (isset($consultaExtension)) {
                for ($i = 0; $i < count($consultaExtension); $i++) {
                    if($beneficio==$consultaExtension[$i]['fk_rhc068_ayuda_especifica']){
                        $consulta[0]['beneficio']= number_format($consultaExtension[$i]['monto_final'],2,',','.');
                        $alerta="     * Este beneficio ha sido extendido a ".number_format($consultaExtension[$i]['monto_final'],2,',','.');
                    }
                }
            }else{

            }
        }

        $disponibleTotal = $consulta[0]['disponible_sin_formato'];
        $consumidoEmpleado = $consulta2[0]['monto_sin_formato'];
        $disponibleBeneficio = $consulta[0]['beneficio']-$consulta3[0]['monto_sin_formato'];
        $consumidoBeneficio = $consulta3[0]['monto_sin_formato'];




        if (!isset($alerta)) {$alerta="";}


        if (isset($consulta[0]['disponible'])) {
            $consulta[0]['disponible']= $consulta[0]['disponible'];
            /*if (isset($consultaExtension[0]['monto_final'])) {
                //$Suma = $this->metSumar($consulta[0]['disponible'], $consultaExtension[0]['monto']);
                //$Suma = ($Suma/100);
                $consulta[0]['disponible']= number_format($consultaExtension[0]['monto_final'], 2, ',', '.');
            }*/

        /*}else{$consulta[0]['disponible']="0,00";}

        if (isset($consulta[0]['beneficio'])){
            $consulta[0]['beneficio']=$consulta[0]['beneficio'];
        }else{
            $consulta[0]['beneficio']='0.00';
        }

        if (!isset($consulta2[0]['monto'])){ $consulta2[0]['monto']="0,00";}

        if (!isset($consulta3[0]['monto'])){ $consulta3[0]['monto']="0,00";}

        #transformando
        $consulta[0]['beneficio'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $consulta[0]['beneficio']));
        $disponible = $consulta[0]['beneficio']-$consulta3[0]['monto_sin_formato'];

        $resultado="<thead>
        <tr>
        <th>Disponible </th>
        <th class='col-sm-5'>".$disponibleTotal."</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>Consumido Empleado</td>
        <td>".$consumidoEmpleado."</td>
        </tr>
        <tr>
        <td>Disponible por Beneficio  </td>
        <td>".$disponibleBeneficio."  ".$alerta."</td>
        </tr>
        <tr>
        <td>Consumido por Beneficio</td>
        <td>".$consumidoBeneficio."</td>
        </tr>
        ";
        echo json_encode($resultado);
        exit;
    }*/

    public function  metActualizarDisponibilidad()
    {

        $ayuda = $this->metObtenerInt('Ayuda');
        $beneficio = $this->metObtenerInt('Beneficio');
        $empleado = $this->metObtenerInt('Empleado');
        #RECOGEMOS EL DISPONIBLE TENIENDO EN CUENTA LA ASIGNACION GLOBLAL Y EL VALOR DEL BENEFICIO
        $consulta=$this->atBeneficioHcm->metActualizarDisponibilidad($ayuda,$beneficio);
        #RECOGEMOS EL CONSUMO TOTAL DE LA PERSONA
        $consulta2=$this->atBeneficioHcm->metConsumidoPersona($empleado,$ayuda);
        #RECOGEMOS EL CONSUMO DE LA PERSONA POR BENEFICIO ESPECIFICO
        $consulta3=$this->atBeneficioHcm->metConsumidoPersonaBeneficio($empleado,$ayuda,$beneficio);
        #VERIFICAMOS SI HAY UNA EXTENSION DEL BENEFICIO
        //$consultaExtension=$this->atBeneficioHcm->metBeneficioExtension($empleado,$ayuda);


        //$consultaExtension=$this->atBeneficioHcm->metBeneficioExtension($empleado,$ayuda);


        /*if (isset($consultaExtension)) {
            for ($i = 0; $i < count($consultaExtension); $i++) {
                if($beneficio==$consultaExtension[$i]['fk_rhc068_ayuda_especifica']){

                    $consulta[0]['beneficio']= $consultaExtension[$i]['monto'];
                    $alerta=" * Este beneficio ha sido extendido.";
                }

            }
        }*/
        if (!isset($alerta)) {$alerta="";}

        if (isset($consulta[0]['disponible'])) {

            $consulta[0]['disponible']= $consulta[0]['disponible'];
            /*if (isset($consultaExtension[0]['monto'])) {
                $Suma = $this->metSumar($consulta[0]['disponible'], $consultaExtension[0]['monto']);
                $Suma = ($Suma/100);
                $consulta[0]['disponible']= number_format($Suma, 2, ',', '.');
            }*/

        }else{$consulta[0]['disponible']="0,00";}

        if (isset($consulta[0]['beneficio'])){
            $consulta[0]['beneficio']=$consulta[0]['beneficio'];
        }else{
            $consulta[0]['beneficio']='0.00';
        }

        if (!isset($consulta2[0]['monto'])){ $consulta2[0]['monto']="0,00";}

        if (!isset($consulta3[0]['monto'])){ $consulta3[0]['monto']="0,00";}

        /*$resultado="<thead>
        <tr>
        <th>Disponible </th>
        <th class='col-sm-5'>".$consulta[0]['disponible']."</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>Consumido</td>
        <td>".$consulta2[0]['monto']."</td>
        </tr>
        <tr>
        <td>Disponible por Beneficio  </td>
        <td>".$consulta[0]['beneficio']."  ".$alerta."</td>
        </tr>
        <tr>
        <td>Consumido por Beneficio</td>
        <td>".$consulta3[0]['monto']."</td>
        </tr>
        ";
        echo json_encode($resultado);
        exit;*/

        $respuesta = array(
            'disponible'  => $consulta[0]['disponible'],
            'consumido'   => $consulta2[0]['monto'],
            'disponibleB' => $consulta[0]['beneficio']."  ".$alerta,
            'consumidoB'  => $consulta3[0]['monto']
        );

        echo json_encode($respuesta);

    }

    public function metNombreAyuda()
    {
        $ayuda = $this->metObtenerInt('Ayuda');
        $datos = $this->atBeneficiosGlobalHcm->metMostrarBeneficioGlobal($ayuda);
        echo json_encode($datos['ind_descripcion']);

    }

    public function metNombreBeneficio()
    {
        $beneficio = $this->metObtenerInt('Beneficio');
        $datos = $this->atBeneficiosHcm->metMostrarBeneficioHcm($beneficio);
        echo json_encode($datos['ind_descripcion_especifica']);

    }


    #para actualizar el select asignaciones segun ayuda global
    public function metListarAsignaciones()
    {
        #obtengo el valor pasado del formulario
        $ayuda = $this->metObtenerInt('Ayuda');

        #conecto con el modelo y consulto los datos
        $this->atBeneficiosHcm = $this->metCargarModelo('beneficiosHcm','maestros');
        $datos = $this->atBeneficiosHcm->metMostrarAsignaciones($ayuda);

        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="<option value=''>Seleccione Beneficio...</option>";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_ayuda_especifica'].">".$datos[$i]['ind_descripcion_especifica']." - ".number_format($datos[$i]['num_limite_esp'],2,',','.')."</option>";
        }
        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;

    }

    public function  metActualizarCargaFamiliar()
    {

       $Empleado = $this->metObtenerInt('Empleado');
        #RECOGEMOS EL DISPONIBLE TENIENDO EN CUENTA LA ASIGNACION GLOBLAL Y EL VALOR DEL BENEFICIO

        $consulta=$this->atBeneficioHcm->atcargaFamiliarModelo->metListarCargaFamiliar($Empleado);
        $resultado="<option value=''>Seleccione</option>";
        for($i=0;$i<sizeof($consulta);$i++){
            $resultado.= "<option value=".$consulta[$i]['pk_num_persona'].">".$consulta[$i]['nombre']."</option>";
        }



        echo json_encode($resultado);
        exit;

    }



    public function metSumar($cantidad1,$cantidad2)
    {
        $Buscar= array(",", "." );
        $Por= array("", "" );
        $cantidad1= str_replace($Buscar, $Por, $cantidad1);
        $cantidad2= str_replace($Buscar, $Por, $cantidad2);
        $resultado=$cantidad1+$cantidad2;
        return $resultado;
    }

 

    public function metPrecio($monto)
    {
        $Buscar= array(",", "." );
        $Por= array(".", "" );
        $resultado= str_replace($Buscar, $Por, $monto);

        return $resultado;

    }

    // Método que permite realizar la búsqueda de beneficio mediante el filtro mostrado en la vista de listado
    public function metBuscarBeneficio()
    {

        $Estado = $_POST['EstadoFiltro'];
        $Empleado = $_POST['EmpleadoFiltro'];
        $Beneficio = $_POST['BeneficioFiltro'];
        $Ayuda = $_POST['AyudaFiltro'];
        $permisoFuncionario = $this->atBeneficioHcm->metBuscarBeneficio($Estado,$Empleado,$Beneficio,$Ayuda);
        echo json_encode($permisoFuncionario);

    }



    #para eliminar beneficio global
    public function metAnularBeneficio()
    {
        $idBeneficio = $this->metObtenerInt('idBeneficio');
        $respuesta=$this->atBeneficioHcm->metBeneficioMedicoIdEstatus($idBeneficio);
        if($respuesta[0]['ind_estado']==1){

            $this->atBeneficioHcm->metAnularBeneficioPersonalHcm($idBeneficio,4);

            $this->atBeneficioHcm->metRegistrarBitacoraHcm( $idBeneficio , 4);

        }else if ($respuesta[0]['ind_estado']==2){

            $this->atBeneficioHcm->metAnularBeneficioPersonalHcm($idBeneficio,1);

            $this->atBeneficioHcm->metRegistrarBitacoraHcm( $idBeneficio , 1);


        }



        $array = array(
            'status'  => 'OK',
            'idBeneficio' => $idBeneficio
        );

        echo json_encode($array);
    }


    // Método que permite generar un reporte con los datos de la búsqueda realizada
    public function metGenerarEntradaHCM( $idBeneficio )
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');

        $this->metObtenerLibreria('cabeceraHCM', 'modRH');
        $pdf = new pdfGeneral('P', 'mm', 'Letter');

        $pdf->AliasNbPages();
        $pdf->AddPage();


        $respuesta=$this->atBeneficioHcm->metBeneficioMedicoIdEstatus($idBeneficio);
        $Resultado=$this->atBeneficioHcm->metMostrarPdf( $respuesta[0]['fk_rhb001_empleado'],$respuesta[0]['fec_creacion']);

        $Elaborado=$this->atBeneficioHcm->metObtenerFuncionarioBitacora($idBeneficio,1);
        $Revisado= $this->atBeneficioHcm->metObtenerFuncionarioBitacora ($idBeneficio,2);
        $Aprobado= $this->atBeneficioHcm->metObtenerFuncionarioBitacora ($idBeneficio,3);
        $total=0;
        $ids='';
        $numeros='';
        for($i=0;$i<sizeof($Resultado);$i++){
            $total=$total+$Resultado[$i]['num_monto'];

            if($i==0){

                    $numeros=str_pad($Resultado[$i]['pk_beneficio_medico'], 6, '0', STR_PAD_LEFT) ;
                    $ids=$Resultado[$i]['pk_beneficio_medico'];
            }else{

                if($ids!=$Resultado[$i]['pk_beneficio_medico']){

                    $numeros.=" - ".str_pad($Resultado[$i]['pk_beneficio_medico'], 6, '0', STR_PAD_LEFT) ;
                    $ids=$Resultado[$i]['pk_beneficio_medico'];
                }


            }

        }
        $V=new numTotext();
        $letras=strtoupper($V->ValorEnLetras($total,"Bs"));
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(0, 5,utf8_decode('Número(s):'.$numeros), 0, 0, 'R');
        $pdf->Ln(5);

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(0, 5,utf8_decode( strtoupper($Resultado[0]['ind_nombre_detalle'])), 0, 0, 'C');
        $pdf->Ln(5);
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->Cell(0, 5,utf8_decode( 'Monto (Bs): '.number_format($total, 2, ",", ".")), 0, 0, 'R');
        $pdf->Ln(5);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(0, 5,utf8_decode( $letras ), 0, 0, 'R');
        $pdf->Ln(15);
        $pdf->SetFont('Arial', '', 9);
        $pdf->MultiCell(0,5,utf8_decode("Por Concepto de Gastos Médicos en atención a los Beneficios socioeconomicos que otorga este Organo al Personal adscrito a este Ente Contralor  "),0,'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 5,utf8_decode( "Funcionario: ".strtoupper($Resultado[0]['ind_nombre1']." ".$Resultado[0]['ind_apellido1'] ) ), 0, 0, 'L');
        $pdf->Ln(5);
        if($Resultado[0]['beneficiario_nombre']!=""){
            $pdf->MultiCell(0,5,utf8_decode("Beneficiario: ".$Resultado[0]['beneficiario_nombre']." ".$Resultado[0]['beneficiario_apellido']),0,'L');
         }

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(229, 229, 229); $pdf->SetTextColor(0, 0, 0);

        $pdf->Ln(10);
        $pdf->SetWidths(array(50,40,50,20,20,20));
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Row(array(
            utf8_decode("Servicio"),
            utf8_decode("Especialidad"),
            utf8_decode("Institución"),
            "Factura",
            "Fecha",
            "Monto"
        ));
        $pdf->SetFont('Arial', '', 8);
        $i=0;
        while ($i<sizeof($Resultado)){

            $pdf->Row(array(
                utf8_decode($Resultado[$i]['servicio']),
                utf8_decode($Resultado[$i]['rama']),
                utf8_decode($Resultado[$i]['ind_nombre_institucion']),
                $Resultado[$i]['num_factura'],
                $Resultado[$i]['fec_creacion'],
                number_format($Resultado[$i]['num_monto'], 2, ",", ".")
            ));


        $i++;
        }


        $pdf->Ln(15);
        $pdf->Cell(67,16,'',1);
        $pdf->Cell(67,16,'',1);
        $pdf->Cell(67,16,'',1);

        $pdf->Ln(16);
        $pdf->Cell(67,4,'Elaborado por',0,0,'C');
        $pdf->Cell(67,4,'Revisado por',0,0,'C');
        $pdf->Cell(67,4,'Aprobado por',0,0,'C');
        $pdf->Ln(4);

        $pdf->Cell(67,4,$Elaborado[0]['ind_nombre1']." ".$Elaborado[0]['ind_apellido1'],0,0,'C');
        $pdf->Cell(67,4,$Revisado[0]['ind_nombre1']." ".$Revisado[0]['ind_apellido1'],0,0,'C');
        $pdf->Cell(67,4,$Aprobado[0]['ind_nombre1']." ".$Aprobado[0]['ind_apellido1'],0,0,'C');


        $pdf->Output();
    }




}

