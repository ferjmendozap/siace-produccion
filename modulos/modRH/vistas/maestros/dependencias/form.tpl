<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idDependencia}" name="idDependencia"/>


        <div class="col-xs-5">
            <div class="form-group">
                <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" required data-msg-required="Seleccione el Organismo">
                    <option value="">Seleccione...</option>
                    {foreach item=dat from=$Organismos}
                        {if isset($formDB.fk_a001_num_organismo)}
                            {if $dat.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                            {/if}
                        {else}
                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a001_num_organismo">Organismo</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}" name="form[txt][ind_dependencia]" id="ind_dependencia" required data-msg-required="Introduzca Descripción de la Dependencia" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_dependencia">Dependencia</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- *************************** ENTIDAD PADRE **************** --------->
        <div class="col-xs-12">
            <div class="form-group">
                <label for="ind_codpadre">Entidad Padre</label>
                <select id="ind_codpadre" name="form[int][ind_codpadre]" class="form-control select2-list select2 input-sm">
                    <option value="">Seleccione Dependencia</option>
                    {foreach item=dat from=$Dependencia}
                        {if isset($formDB.ind_codpadre)}
                            {if $dat.pk_num_dependencia==$formDB.ind_codpadre}
                                <option selected value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                            {else}
                                <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                            {/if}
                        {else}
                            <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                        {/if}
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_codinterno)}{$formDB.ind_codinterno}{/if}" name="form[txt][ind_codinterno]" id="ind_codinterno" maxlength="4" required data-msg-required="Introduzca Numero Interno" data-rule-number="true" data-msg-number="Introduzca un numero valido">
                <label for="ind_codinterno">Num. Interno</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nivel)}{$formDB.ind_nivel}{else}1{/if}" name="form[int][ind_nivel]" id="ind_nivel" required data-msg-required="Introduzca Nivel">
                <label for="ind_nivel">Nivel</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.num_piso)}{$formDB.num_piso}{/if}" name="form[int][num_piso]" id="num_piso">
                <label for="num_piso">Piso</label>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.txt_abreviacion)}{$formDB.txt_abreviacion}{/if}" name="form[txt][txt_abreviacion]" id="txt_abreviacion" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="txt_abreviacion">Abreviación</label>
            </div>
        </div>

        <!-- ***************************   EMPLEADO RESPONSABLE **************** --------->
        <div class="col-xs-12">
            <div class="form-group" id="fk_a003_num_persona_responsableError">
                <label for="fk_a003_num_persona_responsable">Empleado</label>
                <select id="fk_a003_num_persona_responsable" name="form[int][fk_a003_num_persona_responsable]" class="form-control select2-list select2 input-sm" required>
                    <option value="">Seleccione Empleado</option>
                    {foreach item=i from=$Empleado}
                        {if isset($formDB.fk_a003_num_persona_responsable)}
                            {if $i.fk_a003_num_persona==$formDB.fk_a003_num_persona_responsable}
                                <option selected value="{$i.fk_a003_num_persona}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                            {else}
                                <option value="{$i.fk_a003_num_persona}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                            {/if}
                        {else}
                            <option value="{$i.fk_a003_num_persona}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                        {/if}
                    {/foreach}
                </select>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_cargo)}{$formDB.ind_cargo}{/if}" name="form[txt][ind_cargo]" id="ind_cargo" disabled>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_responsable) and $formDB.num_flag_responsable==1} checked{/if} value="1" name="form[int][num_flag_responsable]">
                    <span>Responsable</span>
                </label>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_controlfiscal) and $formDB.num_flag_controlfiscal==1} checked{/if} value="1" name="form[int][num_flag_controlfiscal]">
                    <span>Ejerce Control Fiscal</span>
                </label>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_principal) and $formDB.num_flag_principal==1} checked{/if} value="1" name="form[int][num_flag_principal]">
                    <span>Principal</span>
                </label>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>



        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            if($("#fk_a003_num_persona_responsable").val()==''){
                $("#fk_a003_num_persona_responsableError").addClass('has-error has-feedback');
                $("#fk_a003_num_persona_responsableError").append('<span id="fk_a001_num_organismo-error" class="help-block">Seleccione el Empleado</span>');
                return false;
            }


            $.post('{$_Parametros.url}modRH/maestros/dependenciasCONTROL/DependenciasMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idDependencia'+dato['idDependencia'])).html('<td>'+dato['idDependencia']+'</td>' +
                            '<td>'+dato['ind_dependencia']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-09-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idDependencia="'+dato['idDependencia']+'"' +
                            'descipcion="El Usuario a Modificado una Dependencia" titulo="Modificar Dependencia">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-01-20-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDependencia="'+dato['idDependencia']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado una Dependencia" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Dependencia!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Registro se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){

                    if(dato['idDependencia']=='repDesc'){
                        $("#ind_dependencia").focus();
                        swal("Error!", "Verifique el nombre de la dependencia, ya se encuentra registrada.", "error");
                    }else if(dato['idDependencia']=='repCodInt'){
                        $("#ind_codinterno").focus();
                        swal("Error!", "El numero interno introducido, ya se encuentra registrada en otra dependencia.", "error");
                    }else {
                        $(document.getElementById('datatableDependencias')).append('<tr  id="idDependencia' + dato['idDependencia'] + '">' +
                                '<td>' + dato['idDependencia'] + '</td>' +
                                '<td>' + dato['ind_dependencia'] + '</td>' +
                                '<td  align="center">' +
                                '{if in_array('RH-01-04-09-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" title="Editar" idDependencia="' + dato['idDependencia'] + '"' +
                                'descipcion="El Usuario a Modificado una Dependencia" titulo="Modificar Dependencia">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('RH-01-04-09-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDependencia="' + dato['idDependencia'] + '"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado una Dependencia" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Dependencia!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>' +
                                '</tr>');
                        swal("Registro Guardado!", "Registro guardado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }
                }


            },'json');


        }
    });




    $().ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "60%" );

        //AL SELECIONAR LA DEPENDENCIA PADRE
        $("#ind_codpadre").change(function () {
            $("#ind_codpadre option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/maestros/dependenciasCONTROL/VerificarNivelMET", { dep: $('#ind_codpadre').val() }, function(data){
                    var json = data,
                    obj = JSON.parse(json);
                    $("#ind_nivel").val(obj);
                });
            });
        });

        //AL SELECCIONAR EMPLEADO
        $("#fk_a003_num_persona_responsable").change(function () {
            $("#fk_a003_num_persona_responsable option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/maestros/dependenciasCONTROL/ObtenerCargoEmpleadoMET", { empleado: $('#fk_a003_num_persona_responsable').val() }, function(data){
                    $("#ind_cargo").val(data['ind_descripcion_cargo']);
                },'json');
            });
        });


    });






</script>