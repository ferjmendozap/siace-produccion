
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Centros de Estudios</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Centro</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=centros from=$Centros}

                                    <tr id="idCentro{$centros.pk_num_institucion}">
                                        <td>{$centros.pk_num_institucion}</td>
                                        <td>{$centros.ind_nombre_institucion}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-02-15-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idCentro="{$centros.pk_num_institucion}"
                                                        descipcion="El Usuario a Modificado un Centro de Estudio" titulo="Modificar Centro de Estudio">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-02-16-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar" idCentro="{$centros.pk_num_institucion}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Centro de Estudio" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Centro de Estudio!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-02-14-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descripcion="el Usuario a creado un Nuevo Centro de Estudio" title="Nuevo Centro de Estudios" titulo="Nuevo Centro de Estudio" id="nuevo" >
                                            <i class="md md-create"></i> Nuevo Centro de Estudio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/centroEstudiosCONTROL/NuevoCentroEstudioMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idCentro: $(this).attr('idCentro')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idCentro=$(this).attr('idCentro');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/centroEstudiosCONTROL/EliminarCentroMET';
                $.post($url, { idCentro: idCentro},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idCentro'+$dato['idCentro'])).html('');
                        swal("Eliminado!", "El Centro de Estudio fue Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>