<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idNivelGradoInstruccion}" name="idNivelGradoInstruccion"/>


        <!-- ***************************GRADO DE INSTRUCCION*********************--------->
        <div class="col-xs-5">
            <div class="form-group floating-label">
                <select id="fk_a006_num_miscelaneo_detalle_gradoinst" name="form[int][fk_a006_num_miscelaneo_detalle_gradoinst]" class="form-control input-sm" required data-msg-required="Seleccione el Grado de Instrucción">
                    <option value="">&nbsp;</option>
                    {foreach item=grado from=$GradoInstruccion}
                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_gradoinst)}
                            {if $grado.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_gradoinst}
                                <option selected value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                            {/if}
                        {else}
                            <option value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_gradoinst">Grado de Instrucción</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************ID NIVEL GRADO INSTRUCCION*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_nivel_grado)}{$formDB.pk_num_nivel_grado}{/if}" name="form[int][pk_num_nivel_grado]" id="pk_num_nivel_grado" disabled>
                <label for="pk_num_nivel_grado">Nivel</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION NIVEL GRADO INSTRUCCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_nivel_grado)}{$formDB.ind_nombre_nivel_grado}{/if}" name="form[txt][ind_nombre_nivel_grado]" id="ind_nombre_nivel_grado" required data-msg-required="Introduzca descripcion del nivel del grado de Instrucción" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_nivel_grado">Descripción </label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************ESTATUS GRADO INSTRUCCION*********************--------->
        <div class="col-xs-6">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_utiles) and $formDB.num_flag_utiles==1} checked{/if} value="1" name="form[int][num_flag_utiles]">
                    <span>Aplica para beneficios de Utiles ?</span>
                </label>
            </div>
        </div>


        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //obtengo todos los datos del formulario
            var datos = $( "#formAjax" ).serialize();

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            //url del controlador encargado de ejecutar la operacion
            $.post('{$_Parametros.url}modRH/maestros/nivelGradoInstruccionCONTROL/NuevoNivelGradoInstruccionMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idNivelGradoInstruccion'+dato['idNivelGradoInstruccion'])).html('<td>'+dato['idNivelGradoInstruccion']+'</td>' +
                            '<td>'+dato['ind_nombre_nivel_grado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idNivelGradoInstruccion="'+dato['idNivelGradoInstruccion']+'"' +
                            'descipcion="El Usuario a Modificado un Nivel Grado de Instrucción" titulo="Modificar Nivel Grado de Instrucción">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idNivelGradoInstruccion="'+dato['idNivelGradoInstruccion']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Nivel Grado de Instrucción" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Nivel Grado de Instrucción!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Nivel Grado de Instrucción se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatableGradoInstruccion')).append('<tr  id="idNivelGradoInstruccion'+dato['idNivelGradoInstruccion']+'">' +
                            '<td>'+dato['idNivelGradoInstruccion']+'</td>' +
                            '<td>'+dato['ind_nombre_nivel_grado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idNivelGradoInstruccion="'+dato['idNivelGradoInstruccion']+'"' +
                            'descipcion="El Usuario a Modificado un Nivel Grado de Instrucción" titulo="Modificar Nivel Grado de Instrucción">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idNivelGradoInstruccion="'+dato['idNivelGradoInstruccion']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Nivel Grado de Instrucción" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Nivel Grado de Instrucción!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "EL Nivel Grado de Instrucción fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "60%" );


    });






</script>