<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idIdioma}" name="idIdioma"/>


        <!-- ***************************ID IDIOMA*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_idioma)}{$formDB.pk_num_idioma}{/if}" name="form[int][pk_num_idioma]" id="pk_num_idioma" disabled>
                <label for="pk_num_idioma">Idioma</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION LOCAL*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_idioma)}{$formDB.ind_nombre_idioma}{/if}" name="form[txt][ind_nombre_idioma]" id="ind_nombre_idioma" maxlength="100" required data-msg-required="Introduzca descripcion del idioma" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_idioma">Descripción Local</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION EXTRANJERA*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_ext)}{$formDB.ind_nombre_ext}{/if}" name="form[txt][ind_nombre_ext]" id="ind_nombre_ext" maxlength="100" required data-msg-required="Introduzca descripcion extranjera del idioma" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_ext">Descripción Extranjera</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************ESTATUS *********************--------->
        <div class="col-xs-6">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" id="ind_estatus" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>


        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/idiomasCONTROL/NuevoIdiomaMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idIdioma'+dato['idIdioma'])).html('<td>'+dato['idIdioma']+'</td>' +
                            '<td>'+dato['ind_nombre_idioma']+'</td>' +
                            '<td>'+dato['ind_nombre_ext']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-23-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idIdioma="'+dato['idIdioma']+'"' +
                            'descipcion="El Usuario a Modificado un Idioma" title="Editar" titulo="Modificar Idioma">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-24-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idIdioma="'+dato['idIdioma']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Idioma" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Idioma!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Idioma se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatable1')).append('<tr  id="idIdioma'+dato['idIdioma']+'">' +
                            '<td>'+dato['idIdioma']+'</td>' +
                            '<td>'+dato['ind_nombre_idioma']+'</td>' +
                            '<td>'+dato['ind_nombre_ext']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-23-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idIdioma="'+dato['idIdioma']+'"' +
                            'descipcion="El Usuario a Modificado un Idioma" title="Editar" titulo="Modificar Idioma">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-24-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idIdioma="'+dato['idIdioma']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Idioma" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Idioma!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "EL Idioma fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "60%" );


    });






</script>