
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro de Motivos de Cese</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="170">Motivo</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=motivo from=$MotivoCese}

                                    <tr id="idMotivoCese{$motivo.pk_num_motivo_cese}">
                                        <td>{$motivo.pk_num_motivo_cese}</td>
                                        <td>{$motivo.ind_nombre_cese}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-03-13-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idMotivoCese="{$motivo.pk_num_motivo_cese}"
                                                        descipcion="El Usuario a Modificado un Motivo de Cese" titulo="Modificar Motivo de Cese">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-03-14-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMotivoCese="{$motivo.pk_num_motivo_cese}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Motivo de Cese" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Motivo de Cese!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-03-12-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Motivo de Cese" title="Nuevo Motivo de Cese"  titulo="Nuevo Motivo de Cese" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Motivo de Cese &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/motivoCeseCONTROL/NuevoMotivoCeseMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMotivoCese: $(this).attr('idMotivoCese')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idMotivoCese=$(this).attr('idMotivoCese');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/motivoCeseCONTROL/EliminarMotivoCeseMET';
                $.post($url, { idMotivoCese: idMotivoCese},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idMotivoCese'+$dato['idMotivoCese'])).html('');
                        swal("Eliminado!", "Motivo de Cese Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>