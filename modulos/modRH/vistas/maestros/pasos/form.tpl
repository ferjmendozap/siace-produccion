<form action="" id="formAjax" class="form" role="form" method="post" AUTOCOMPLETE="off">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idPasos}" name="idPasos"/>
        <input type="hidden" value="{if isset($idMaestro)}{$idMaestro}{/if}" name="form[int][idMaestro]"/>



        <div class="col-xs-6">

            <div class="form-group floating-label" id="ind_nombre_cargoError">

                <input type="text" class="form-control input-sm" maxlength="4" value="{if isset($formDB.cod_detalle)}{$formDB.cod_detalle}{/if}" name="form[txt][cod_detalle]" id="cod_detalle" required data-msg-required="Introduzca Codigo Detalle" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="cod_detalle">Codigo Detalle</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label" id="ind_nombre_cargoError">

                <input type="text" class="form-control input-sm" maxlength="100" value="{if isset($formDB.ind_nombre_detalle)}{$formDB.ind_nombre_detalle}{/if}" name="form[txt][ind_nombre_detalle]" id="ind_nombre_detalle" required data-msg-required="Introduzca la Descripcion del Paso" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_detalle">Descripción Paso</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>


        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"> <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"> <span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            var app = new  AppFunciones();

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/pasosCONTROL/NuevoPasoMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    app.metActualizarRegistroTablaJson('datatablePasos','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='creacion'){
                    app.metNuevoRegistroTablaJson('datatablePasos','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }


            },'json');


        }
    });


    $().ready(function() {

       $("#formAjax").validate();

       $('#modalAncho').css( "width", "50%" );

    });
</script>