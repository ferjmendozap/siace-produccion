<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idTipoContrato}" name="idTipoContrato"/>



        <!-- ***************************ID TIPO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_tipo_contrato)}{$formDB.pk_num_tipo_contrato}{/if}" name="form[int][pk_num_tipo_contrato]" id="pk_num_tipo_contrato" disabled>
                <label for="pk_num_tipo_contrato">Tipo</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_tipo_contrato)}{$formDB.ind_tipo_contrato}{/if}" name="form[txt][ind_tipo_contrato]" id="ind_tipo_contrato" maxlength="100" required data-msg-required="Introduzca Descripción de Contrato" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_tipo_contrato">Descripción</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************ID TIPO*********************--------->
        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_nomina) and $formDB.num_flag_nomina==1} checked{/if} value="1" name="form[int][num_flag_nomina]">
                    <span>En Nomina?</span>
                </label>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_vencimiento) and $formDB.num_flag_vencimiento==1} checked{/if} value="1" name="form[int][num_flag_vencimiento]">
                    <span>Tiene Vencimiento</span>
                </label>
            </div>
        </div>


        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/tipoContratoCONTROL/NuevoTipoContratoMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idTipoContrato'+dato['idTipoContrato'])).html('<td>'+dato['idTipoContrato']+'</td>' +
                            '<td>'+dato['ind_tipo_contrato']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-03-09-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idTipoContrato="'+dato['idTipoContrato']+'"' +
                            'descipcion="El Usuario a Modificado un Tipo de Contrato" titulo="Modificar Tipo de Contrato">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-03-10-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTipoContrato="'+dato['idTipoContrato']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Tipo de Contrato" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Contrato!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Tipo de Contrato se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatable1')).append('<tr id="idTipoContrato'+dato['idTipoContrato']+'">' +
                            '<td>'+dato['idTipoContrato']+'</td>' +
                            '<td>'+dato['ind_tipo_contrato']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-03-09-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idTipoContrato="'+dato['idTipoContrato']+'"' +
                            'descipcion="El Usuario a Modificado un Tipo de Contrato" titulo="Modificar Tipo de Contrato">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-03-10-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTipoContrato="'+dato['idTipoContrato']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Tipo de Contrato" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Contrato!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "EL Tipo de Contrato fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );


    });






</script>