
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo de Contrato</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="170">Tipo</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=tipo from=$TipoContrato}

                                    <tr id="idTipoContrato{$tipo.pk_num_tipo_contrato}">
                                        <td>{$tipo.pk_num_tipo_contrato}</td>
                                        <td>{$tipo.ind_tipo_contrato}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-03-09-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idTipoContrato="{$tipo.pk_num_tipo_contrato}"
                                                        descipcion="El Usuario a Modificado un Tipo de Contrato" titulo="Modificar Tipo de Contrato">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-03-10-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTipoContrato="{$tipo.pk_num_tipo_contrato}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Tipo de Contrato" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Contrato!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-03-08-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Tipo de Contrato" title="Nuevo Tipo Contrato" titulo="Nuevo Tipo Contrato" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Tipo  Contrato &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/tipoContratoCONTROL/NuevoTipoContratoMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idTipoContrato: $(this).attr('idTipoContrato')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idTipoContrato=$(this).attr('idTipoContrato');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/tipoContratoCONTROL/EliminarTipoContratoMET';
                $.post($url, { idTipoContrato: idTipoContrato},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idTipoContrato'+$dato['idTipoContrato'])).html('');
                        swal("Eliminado!", "El Tipo de Contrato fue Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>