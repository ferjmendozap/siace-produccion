<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idTiposCargos}" name="idTiposCargos"/>


        <div class="col-xs-12">

            <div class="form-group floating-label" id="ind_nombre_cargoError">

                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_cargo)}{$formDB.ind_nombre_cargo}{/if}" name="form[txt][ind_nombre_cargo]" id="ind_nombre_cargo" required data-msg-required="Introduzca Nombre del Tipo de Cargo" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="cod_interno">Descripción</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label" id="txt_descripcionError">
                <textarea name="form[alphaNum][txt_descripcion]" id="txt_descripcion" class="form-control input-sm" rows="3" placeholder="" required data-msg-required="Introduzca la Descripción del Tipo de Cargo" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">{if isset($formDB.txt_descripcion)}{$formDB.txt_descripcion}{/if}</textarea>
                <label for="textarea2">Definición</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label">
                <textarea name="form[alphaNum][txt_funcion]" id="txt_funcion" class="form-control input-sm" rows="3" placeholder="" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">{if isset($formDB.txt_funcion)}{$formDB.txt_funcion}{/if}</textarea>
                <label for="textarea2">Función</label>
            </div>

        </div>

        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"> <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            var datos = $( "#formAjax" ).serialize();
            var app = new  AppFunciones();

            $.post('{$_Parametros.url}modRH/maestros/tiposCargosCONTROL/NuevoTipoCargoMET', datos ,function(dato){

                if(dato['status']=='modificacion'){
                    app.metActualizarRegistroTablaJson('datatableTiposCargos','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='creacion'){
                    app.metNuevoRegistroTablaJson('datatableTiposCargos','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }

            },'json');


        }
    });


    $().ready(function() {

       $("#formAjax").validate();

       $('#modalAncho').css( "width", "70%" );


    });
</script>