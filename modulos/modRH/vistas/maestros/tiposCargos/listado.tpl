
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipos de Cargos</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <table id="datatableTiposCargos" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">Cod</th>
                                    <th>Tipos de Cargos</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-01-02-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Nuevo Tipo Cargo" title="Registrar Nuevo Tipo Cargo"  titulo="Registrar Nuevo Tipo Cargo" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Tipo de Cargo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">


    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/tiposCargosCONTROL/NuevoTipoCargoMET';

        var app = new AppFunciones();
        var dt = app.dataTable(
                '#datatableTiposCargos',
                "{$_Parametros.url}modRH/maestros/tiposCargosCONTROL/JsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_cargo" },
                    { "data": "ind_nombre_cargo" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

        //capturo el vento del boton nuevo y abro la ventana para registrar el nuevo tipo cargo
        $('#nuevo').click(function(){

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //evento del boton modificar
        $('#datatableTiposCargos tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idTiposCargos: $(this).attr('idTiposCargos')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });

        //evento del boton eliminar
        $('#datatableTiposCargos tbody').on( 'click', '.eliminar', function () {

            var idTiposCargos=$(this).attr('idTiposCargos');

            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/tiposCargosCONTROL/EliminarTipoCargoMET';
                $.post($url, { idTiposCargos: idTiposCargos},function($dato){
                    if($dato['status']=='OK'){
                        app.metEliminarRegistroJson('datatableTiposCargos','El Tipo de Cargo fue eliminado con Exito.','cerrarModal','ContenidoModal');
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>