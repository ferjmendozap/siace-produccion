<form action="{$_Parametros.url}modRH/TiposCargosCONTROL/NuevoTipoCargoMET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idTiposCargos}" name="idTiposCargos"/>


        <div class="col-xs-12">

            <div class="form-group floating-label" id="ind_nombre_cargoError">
                <input type="text" class="form-control" value="" name="form[txt][ind_nombre_cargo]" id="ind_nombre_cargo">
                <label for="cod_interno">Descripción</label>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label" id="txt_descripcionError">
                <textarea name="form[alphaNum][txt_descripcion]" id="txt_descripcion" class="form-control" rows="3" placeholder="">

                </textarea>
                <label for="textarea2">Definición</label>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label">
                <textarea name="form[alphaNum][txt_funcion]" id="txt_funcion" class="form-control" rows="3" placeholder="">

                </textarea>
                <label for="textarea2">Función</label>
            </div>

        </div>

        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
    </div>

</form>

<script type="text/javascript">
   $(document).ready(function() {

        $("#formAjax").submit(function(){
            return false;
        });
        $('#iconoMenu').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post("{$_Parametros.url}menu/icono",{ idMenu:0 },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#modalAncho').css( "width", "75%" );

        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    for (var item in dato) {
                        if(dato[item]=='error'){
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                        }else{
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                            $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                            $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                        }
                    }
                }else if(dato['status']=='medificacion'){
                    $(document.getElementById('idMenu'+dato['idMenu'])).html('<td>'+dato['cod_interno']+'</td>' +
                            '<td>'+dato['cod_padre']+'</td>' +
                            '<td>por Recargar</td>' +
                            '<td>'+dato['ind_nombre']+'</td>' +
                            '<td>'+dato['num_nivel']+'</td>' +
                            '<td>'+dato['ind_ruta']+'</td>' +
                            '<td>'+dato['ind_rol']+'</td>' +
                            '<td><i class="'+dato['ind_icono']+'"></i></td>' +
                            '<td><i class="md md-check"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('AP-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idMenu="'+dato['idMenu']+'"' +
                            'descipcion="El Usuario a Modificado un post" titulo="Modificar Post">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('AP-01-01-03-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMenu="'+dato['idMenu']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un post" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el post!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Menu fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatable1')).append('<tr  id="idMenu'+dato['idMenu']+'">' +
                            '<td>'+dato['cod_interno']+'</td>' +
                            '<td>'+dato['cod_padre']+'</td>' +
                            '<td>por Recargar</td>' +
                            '<td>'+dato['ind_nombre']+'</td>' +
                            '<td>'+dato['num_nivel']+'</td>' +
                            '<td>'+dato['ind_ruta']+'</td>' +
                            '<td>'+dato['ind_rol']+'</td>' +
                            '<td><i class="'+dato['ind_icono']+'"></i></td>' +
                            '<td><i class="md md-check"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('AP-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idMenu="'+dato['idMenu']+'"' +
                            'descipcion="El Usuario a Modificado un post" titulo="Modificar Post">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('AP-01-01-03-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMenu="'+dato['idMenu']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un post" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el post!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "El Menu fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>