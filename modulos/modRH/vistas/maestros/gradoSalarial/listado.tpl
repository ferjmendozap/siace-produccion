
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Grado Salarial</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableGradoSalarial" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Cod</th>
                                    <th>Categoria</th>
                                    <th>Grado</th>
                                    <th>Descripción</th>
                                    <th>Sueldo Promedio</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <!--
                                <tbody>
                                {foreach item=grado from=$GradoSalarial}

                                    <tr id="idGradoSalarial{$grado.pk_num_grado}">
                                        <td>{$grado.pk_num_grado}</td>
                                        <td>{$grado.ind_nombre_detalle}</td>
                                        <td>{$grado.ind_grado}</td>
                                        <td>{$grado.ind_nombre_grado}</td>
                                        <td>

                                            {if $grado.flag_pasos == 1}
                                                <button class="verPasos btn btn-xs ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        title="Visualizar Pasos" titulo="Visualizar Pasos" idGradoSalarial="{$grado.pk_num_grado}" nombre="{$grado.ind_nombre_grado}" id="verPasos" >
                                                    <i class="md md-search"></i> Pasos
                                                </button>
                                            {else}
                                                {$grado.num_sueldo_promedio|number_format:2:",":"."}
                                            {/if}



                                        </td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-01-19-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idGradoSalarial="{$grado.pk_num_grado}"
                                                        descipcion="El Usuario a Modificado un Grado Salarial" titulo="Modificar Grado Salarial">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-01-20-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGradoSalarial="{$grado.pk_num_grado}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Grado Salarial" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Grado Salarial!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                -->
                                <tfoot>
                                    <th colspan="6">
                                        {if in_array('RH-01-04-01-10-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Grado Salarial" title="Nuevo Grado Salarial" titulo="Nuevo Grado Salarial" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Grado Salarial &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/gradoSalarialCONTROL/NuevoGradoSalarialMET';
        var $url_pasos='{$_Parametros.url}modRH/maestros/gradoSalarialCONTROL/VisualizarPasosMET';


        /*cargo el listado de registros*/
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#datatableGradoSalarial',
                "{$_Parametros.url}modRH/maestros/gradoSalarialCONTROL/JsonDataTablaGradoSalarialMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_grado" },
                    { "data": "ind_nombre_detalle" },
                    { "data": "ind_grado" },
                    { "data": "ind_nombre_grado" },
                    { "data": "num_sueldo_promedio" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );


        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableGradoSalarial tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idGradoSalarial: $(this).attr('idGradoSalarial')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });

        $('#datatableGradoSalarial tbody').on( 'click', '.verPasos', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_pasos,{ idGradoSalarial: $(this).attr('idGradoSalarial'), nombre:$(this).attr('nombre') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableGradoSalarial tbody').on( 'click', '.eliminar', function () {

            var idGradoSalarial=$(this).attr('idGradoSalarial');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/gradoSalarialCONTROL/EliminarGradoSalarialMET';
                $.post($url, { idGradoSalarial: idGradoSalarial},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idGradoSalarial'+$dato['idGradoSalarial'])).html('');
                        swal("Eliminado!", "El Grado Salarial fue Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

    });
</script>