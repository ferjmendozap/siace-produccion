<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idBeneficioGlobal}" name="idBeneficioGlobal"/>



        <!-- ***************************ID BENEFICIO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_ayuda_global)}{$formDB.pk_num_ayuda_global}{/if}" name="form[int][pk_num_ayuda_global]" id="pk_num_ayuda_global" readonly>
                <label for="pk_num_ayuda_global">Id Beneficio</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{$anio}{/if}" name="form[txt][fec_anio]" id="fec_anio" maxlength="4" required data-msg-required="Introduzca Año">
                <label for="fec_anio">Año</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[txt][ind_descripcion]" id="ind_descripcion" maxlength="200" required data-msg-required="Introduzca Descripción Beneficio Global" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_descripcion">Descripción Beneficio</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************MONTO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm bolivar-mask" value="{if isset($formDB.num_limite)}{$formDB.num_limite|number_format:2:",":"."}{/if}" name="form[int][num_limite]" id="num_limite" maxlength="20" required data-msg-required="Introduzca Limite del Beneficio" >
                <label for="num_limite">Limite Beneficio</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"> <span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            var app = new  AppFunciones();

            $.post('{$_Parametros.url}modRH/maestros/beneficiosGlobalHcmCONTROL/BeneficioGlobalHcmMET', datos ,function(dato){

                if(dato['status']=='error'){
                    swal("ERROR!", "No de pudo realizar la Opreación. Consulte con el Administrador del Sistema", "error");
                }
                if(dato['status']=='duplicado'){
                    swal("ERROR!", "No de pudo realizar la Opreación. NO SE PUEDE REGISTRAR DOS BENEFICIOS EN EL MISMO AÑO. Consulte con el Administrador del Sistema", "error");
                }
                else if(dato['status']=='modificacion'){
                    /*$(document.getElementById('idBeneficioGlobal'+dato['idBeneficioGlobal'])).html('<td>'+dato['idBeneficioGlobal']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['num_limite']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-07-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idBeneficioGlobal="'+dato['idBeneficioGlobal']+'"' +
                            'descipcion="El Usuario a Modificado un Beneficio Global" titulo="Modificar Beneficio Global">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-07-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficioGlobal="'+dato['idBeneficioGlobal']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Beneficio Global" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Beneficio Global!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Registro se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');*/
                    app.metActualizarRegistroTablaJson('datatableBeneficiosGlobal','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');



                }else if(dato['status']=='creacion'){
                    /*$(document.getElementById('datatable1')).append('<tr id="idBeneficioGlobal'+dato['idBeneficioGlobal']+'">' +
                            '<td>'+dato['idBeneficioGlobal']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['num_limite']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-07-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Modificar" idBeneficioGlobal="'+dato['idBeneficioGlobal']+'"' +
                            'descipcion="El Usuario a Modificado un Beneficio Global" titulo="Modificar Beneficio Global">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-07-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficioGlobal="'+dato['idBeneficioGlobal']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Beneficio Global" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Beneficio Global!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "EL Registro fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');*/
                    app.metNuevoRegistroTablaJson('datatableBeneficiosGlobal','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "65%" );


    });






</script>