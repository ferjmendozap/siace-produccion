
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Institución HCM </h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="150">Id</th>
                                    <th class="col-sm-9">Institución</th>
                                    <th class="col-sm-1">Modificar</th>
                                    <th class="col-sm-1">Eliminar</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=f from=$Utiles}



                                        <tr id="idInstitucion{$f.pk_num_institucion}">
                                            <td>{$f.pk_num_institucion}</td>
                                            <td>{$f.ind_nombre_institucion}</td>
                                            <td align="center" width="100">
                                                {if in_array('RH-01-04-07-12-M',$_Parametros.perfil)}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static" title="Editar" idInstitucion="{$f.pk_num_institucion}"
                                                            descipcion="El Usuario a Modificado los datos de una Institución" titulo="Modificar Institución">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                &nbsp;&nbsp;
                                            </td>
                                            <td>
                                                {if in_array('RH-01-04-07-13-E',$_Parametros.perfil)}
                                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInstitucion="{$f.pk_num_institucion}"  boton="si, Eliminar"
                                                            descipcion="El usuario a eliminado Utiles Escolares" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            </td>
                                        </tr>

                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-07-11-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="El Usuario a registrado una Institución" title="Nueva Institución"  titulo="Nueva Institución" id="nuevo" >
                                                <i class="md md-create"></i> Nueva Institución  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        $('#nuevo').click(function(){
            var $url='{$_Parametros.url}modRH/maestros/institucionHcmCONTROL/NuevaInstitucionMET';

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            var $url='{$_Parametros.url}modRH/maestros/institucionHcmCONTROL/ModificarInstitucionMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idInstitucion')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idInstitucion=$(this).attr('idInstitucion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/institucionHcmCONTROL/EliminarinstitucionHcmMET';
                $.post($url, { idInstitucion: idInstitucion},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idInstitucion'+$dato['idInstitucion'])).html('');
                        swal("Eliminado!", "Utiles Escolares Eliminado.", "success");
                        $('#cerrar').click();
                    }

                },'json');
            });
        });
    });
</script>