<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idSueldoMinimo}" name="idSueldoMinimo"/>


        <!-- ***************************ID SUELDO MINIMO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_sueldo_minimo)}{$formDB.pk_num_sueldo_minimo}{/if}" name="form[int][pk_num_sueldo_minimo]" id="pk_num_sueldo_minimo" disabled>
                <label for="pk_num_sueldo_minimo">Codigo</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <!-- ***************************PERIODO*********************--------->
        <div class="col-xs-6">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_periodo_sueldo)}{$formDB.ind_periodo_sueldo}{/if}" name="form[txt][ind_periodo_sueldo]" id="ind_periodo_sueldo" maxlength="7" required data-msg-required="Introduzca Perido" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_periodo_sueldo">Periodo</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio  (año-mes) (yyyy-mm)</span></p>
            </div>
        </div>

        <!-- ***************************MONTO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm bolivar-mask" value="{if isset($formDB.num_monto_sueldo)}{$formDB.num_monto_sueldo|number_format:2:",":"."}{/if}" name="form[int][num_monto_sueldo]" id="num_monto_sueldo" maxlength="20" required data-msg-required="Introduzca Monto Sueldo Minimo" >
                <label for="num_monto_sueldo">Monto Sueldo</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>



        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/maestros/sueldosMinimosCONTROL/SueldoMinimoMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idSueldoMinimo'+dato['idSueldoMinimo'])).html('<td>'+dato['idSueldoMinimo']+'</td>' +
                            '<td>' + dato['ind_periodo_sueldo'] + '</td>' +
                            '<td>' + dato['num_monto_sueldo'] + '</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-10-08-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSueldoMinimo="' + dato['idSueldoMinimo'] + '"' +
                            'descipcion="El Usuario a Modificado un Sueldo Minimo" titulo="Modificar Sueldo Minimo">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-09-09-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSueldoMinimo="' + dato['idSueldoMinimo'] + '"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Sueldo Minimo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Sueldo Minimo!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){

                       if(dato['idSueldoMinimo']=='rep'){

                            $("#ind_periodo_sueldo").focus();
                            swal("Error!", "Verifique el Periodo, ya existe un registro con el perido introducido.", "error");

                        }else {

                        $(document.getElementById('datatable1')).append('<tr id="idSueldoMinimo' + dato['idSueldoMinimo'] + '">' +
                                '<td>' + dato['idSueldoMinimo'] + '</td>' +
                                '<td>' + dato['ind_periodo_sueldo'] + '</td>' +
                                '<td>' + dato['num_monto_sueldo'] + '</td>' +
                                '<td  align="center">' +
                                '{if in_array('RH-01-04-10-08-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" idSueldoMinimo="' + dato['idSueldoMinimo'] + '"' +
                                'descipcion="El Usuario a Modificado un Sueldo Minimo" titulo="Modificar Sueldo Minimo">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('RH-01-04-09-09-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSueldoMinimo="' + dato['idSueldoMinimo'] + '"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado un Sueldo Minimo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Sueldo Minimo!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>' +
                                '</tr>');
                        swal("Registro Guardado!", "Registro guardado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                    }
                }

            },'json');

        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );


    });






</script>