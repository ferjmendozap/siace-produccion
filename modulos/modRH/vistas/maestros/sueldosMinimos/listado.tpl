
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro Tabla  de Sueldos Minimos</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-head">
                            
                            <div class="tools">
                                <div class="btn-group">
                                    {if in_array('RH-01-04-10-07-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descripcion="el Usuario a creado un Sueldo Minimo"  titulo="Nuevo Sueldo Minimo" id="nuevo" >
                                        Nuevo Sueldo Minimo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                    {/if}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="130">Id</th>
                                    <th>Periodo</th>
                                    <th>Monto</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=b from=$SueldoMinimo}

                                    <tr id="idSueldoMinimo{$b.pk_num_sueldo_minimo}">
                                        <td>{$b.pk_num_sueldo_minimo}</td>
                                        <td>{$b.ind_periodo_sueldo}</td>
                                        <td>{$b.num_monto_sueldo|number_format:2:",":"."}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-10-08-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idSueldoMinimo="{$b.pk_num_sueldo_minimo}"
                                                        descipcion="El Usuario a Modificado un Sueldo Minimo" titulo="Modificar Sueldo Minimo">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-10-09-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSueldoMinimo="{$b.pk_num_sueldo_minimo}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Sueldo Minimo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Sueldo Minimo!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/sueldosMinimosCONTROL/SueldoMinimoMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idSueldoMinimo: $(this).attr('idSueldoMinimo')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idSueldoMinimo=$(this).attr('idSueldoMinimo');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/sueldosMinimosCONTROL/EliminarSueldoMinimoMET';
                $.post($url, { idSueldoMinimo: idSueldoMinimo},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idSueldoMinimo'+$dato['idSueldoMinimo'])).html('');
                        swal("Eliminado!", "Sueldo Minimo Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>