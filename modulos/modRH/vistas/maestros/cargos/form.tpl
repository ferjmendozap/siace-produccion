<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idCargo}" name="idCargo"/>

        <!-- ***************************TIPO*********************--------->

        <div class="col-xs-6">
            <div class="form-group">
                <select id="fk_rhc006_num_cargo" name="form[int][fk_rhc006_num_cargo]" class="form-control input-sm" required data-msg-required="Seleccione el Tipo de Cargo">
                    <option value="">&nbsp;</option>
                    {foreach item=tipo from=$TipoCargo}
                        {if isset($formDB.fk_rhc006_num_cargo)}
                            {if $tipo.pk_num_cargo==$formDB.fk_rhc006_num_cargo}
                                <option selected value="{$tipo.pk_num_cargo}">{$tipo.ind_nombre_cargo}</option>
                            {else}
                                <option value="{$tipo.pk_num_cargo}">{$tipo.ind_nombre_cargo}</option>
                            {/if}
                        {else}
                            <option value="{$tipo.pk_num_cargo}">{$tipo.ind_nombre_cargo}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_rhc006_num_cargo">Tipo de Cargo</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- ***********************NIVEL*****************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <select id="fk_rhc009_num_nivel" name="form[int][fk_rhc009_num_nivel]" class="form-control input-sm" required data-msg-required="Seleccione el Nivel">
                    <option value="">&nbsp;</option>
                    {foreach item=nivel from=$Nivel}
                        {if isset($formDB.fk_rhc009_num_nivel)}
                            {if $nivel.pk_num_nivel==$formDB.fk_rhc009_num_nivel}
                                <option selected value="{$nivel.pk_num_nivel}">{$nivel.ind_nombre_nivel}</option>
                            {else}
                                <option value="{$nivel.pk_num_nivel}">{$nivel.ind_nombre_nivel}</option>
                            {/if}
                        {else}
                            <option value="{$nivel.pk_num_nivel}">{$nivel.ind_nombre_nivel}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_rhc009_num_nivel">Nivel</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************GRUPO OCUPACIONAL*****************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <select id="fk_a006_num_miscelaneo_detalle_grupoocupacional" name="form[int][fk_a006_num_miscelaneo_detalle_grupoocupacional]" class="form-control input-sm" required data-msg-required="Seleccione Grupo Ocupacional">
                    <option value="">&nbsp;</option>
                    {foreach item=grupo from=$GrupoOcupacional}
                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_grupoocupacional)}
                            {if $grupo.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_grupoocupacional}
                                <option selected value="{$grupo.pk_num_miscelaneo_detalle}">{$grupo.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$grupo.pk_num_miscelaneo_detalle}">{$grupo.ind_nombre_detalle}</option>
                            {/if}
                        {else}
                            <option value="{$grupo.pk_num_miscelaneo_detalle}">{$grupo.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_grupoocupacional ">Grupo Ocupacional</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>


        <!-- SERIE OCUPACIONAL--------->
        <div class="col-xs-6">
            <div class="form-group">
                <select id="fk_rhc010_num_serie" name="form[int][fk_rhc010_num_serie]" class="form-control input-sm" required data-msg-required="Seleccione la Serie Ocupacional">
                    <option value="">&nbsp;</option>
                    {foreach item=serie from=$Serie}
                        {if isset($formDB.fk_rhc010_num_serie)}
                            {if $serie.pk_num_serie==$formDB.fk_rhc010_num_serie}
                                <option selected value="{$serie.pk_num_serie}">{$serie.ind_nombre_serie}</option>
                            {else}
                                <option value="{$serie.pk_num_serie}">{$serie.ind_nombre_serie}</option>
                            {/if}
                        {else}
                            <option value="{$serie.pk_num_serie}">{$serie.ind_nombre_serie}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_rhc010_num_serie">Serie Ocupacional</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>


        <!-- DESCRIPCION--------->
        <div class="col-xs-8">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_descripcion_cargo)}{$formDB.ind_descripcion_cargo}{/if}" name="form[txt][ind_descripcion_cargo]" id="ind_descripcion_cargo " required data-msg-required="Introduzca Descripción" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())" autocomplete="off">
                <label for="ind_descripcion_cargo ">Descripción</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- DESCRIPCION GENERAL DEL CARGO--------->
        <div class="col-xs-12">
            <div class="form-group">
                <textarea name="form[alphaNum][txt_descripcion_gen]" id="txt_descripcion_gen" class="form-control input-sm" rows="3" placeholder="" required data-msg-required="Introduzca Descripción General" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">{if isset($formDB.txt_descripcion_gen)}{$formDB.txt_descripcion_gen}{/if}</textarea>
                <label for="txt_descripcion_gen">Descripción General</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>


        <!-- CATEGORIAL DEL CARGO--------->
        <div class="col-xs-6">
            <div class="form-group">
                <select id="fk_a006_num_miscelaneo_detalle_categoria" name="form[int][fk_a006_num_miscelaneo_detalle_categoria]" class="form-control input-sm" required data-msg-required="Seleccione la Categoria del Cargo">
                    <option value="">&nbsp;</option>
                    {foreach item=cat from=$Categoria}
                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_categoria)}
                            {if $cat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_categoria}
                                <option selected value="{$cat.pk_num_miscelaneo_detalle}">{$cat.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$cat.pk_num_miscelaneo_detalle}">{$cat.ind_nombre_detalle}</option>
                            {/if}
                        {else}
                            <option value="{$cat.pk_num_miscelaneo_detalle}">{$cat.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_categoria">Categoria del Cargo</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- GRADO DEL CARGO--------->
        <div class="col-xs-6">
            <div class="form-group">
                <select id="fk_rhc007_num_grado" name="form[int][fk_rhc007_num_grado]" class="form-control input-sm" required data-msg-required="Seleccione el Grado del cargo">
                    <option value="">&nbsp;</option>
                    {foreach item=gra from=$Grado}
                        {if isset($formDB.fk_rhc007_num_grado)}
                            {if $gra.pk_num_grado==$formDB.fk_rhc007_num_grado}
                                <option selected value="{$gra.pk_num_grado}">{$gra.ind_nombre_grado}</option>
                            {else}
                                <option value="{$gra.pk_num_grado}">{$gra.ind_nombre_grado}</option>
                            {/if}
                        {else}
                            <option value="{$gra.pk_num_grado}">{$gra.ind_nombre_grado}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_rhc007_num_grado">Grado del Cargo</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- ******************************* sueldos ***************************************************-->

        <div class="col-xs-3">
            <div class="form-group">
                <select id="fk_a006_num_miscelaneo_detalle_paso" name="form[int][fk_a006_num_miscelaneo_detalle_paso]" class="form-control input-sm" required data-msg-required="Seleccione el Paso">
                    {foreach item=i from=$Pasos}
                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_paso)}
                            {if $i.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_paso}
                                <option selected value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                            {/if}
                        {else}
                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_paso">Paso</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.num_sueldo_basico)}{$formDB.num_sueldo_basico|number_format:2:",":"."}{/if}" name="form[int][num_sueldo_basico]" id="num_sueldo_basico" readonly>
                <label for="num_sueldo_basico">Sueldo Básico</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            var app = new  AppFunciones();

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/cargosCONTROL/NuevoCargoMET', datos ,function(dato){

                if(dato['status']=='error'){

                    /*Error para usuarios normales del sistema*/
                    //swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                }else if(dato['status']=='modificacion'){
                    app.metActualizarRegistroTablaJson('datatableCargos','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='creacion'){
                    app.metNuevoRegistroTablaJson('datatableCargos','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }


            },'json');


        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();


        //AL SELECIONAR OPCION TIPO DE CARGO (SELECT)
        $("#fk_rhc006_num_cargo").change(function () {

            $("#fk_rhc006_num_cargo option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/cargosCONTROL/ActualizarNivelMET", { TipoCargo: $('#fk_rhc006_num_cargo').val() }, function(data){
                    var json = data,
                    obj = JSON.parse(json);
                    $("#fk_rhc009_num_nivel").html(obj);
                });

            });

        });


        //AL SELECIONAR OPCION GRUPO OCUPACIONAL (SELECT)
        $("#fk_a006_num_miscelaneo_detalle_grupoocupacional").change(function () {

           $("#fk_a006_num_miscelaneo_detalle_grupoocupacional option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/cargosCONTROL/ActualizarSerieMET", { GrupoOcupacional: $('#fk_a006_num_miscelaneo_detalle_grupoocupacional').val() }, function(data){
                    var json = data,
                    obj = JSON.parse(json);
                    $("#fk_rhc010_num_serie").html(obj);
                });

            });

        });


        //AL SELECIONAR CATEGORIA DEL CARGO
        $("#fk_a006_num_miscelaneo_detalle_categoria").change(function () {

            $("#fk_a006_num_miscelaneo_detalle_categoria option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/cargosCONTROL/ActualizarGradoMET", { Categoria: $('#fk_a006_num_miscelaneo_detalle_categoria').val() }, function(data){
                    var json = data,
                    obj = JSON.parse(json);
                    $("#fk_rhc007_num_grado").html(obj);
                });

            });

        });

        //PARA VER EL SUELDO BASICO SEGUN EL GRADO SELECCIONADO
        $("#fk_rhc007_num_grado").change(function () {

           $("#fk_rhc007_num_grado option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/cargosCONTROL/ActualizarSueldoMET", { Grado: $('#fk_rhc007_num_grado').val() }, function(data){

                    if(data['P']==0){
                        $("#num_sueldo_basico").val(data['sueldo']);
                        $('#fk_a006_num_miscelaneo_detalle_paso').html('');
                        $('#fk_a006_num_miscelaneo_detalle_paso').append('<option value="'+data['pasos'][0]['pk_num_miscelaneo_detalle']+'">'+data['pasos'][0]['ind_nombre_detalle']+'</option>');




                    }else{

                        $('#fk_a006_num_miscelaneo_detalle_paso').html('');
                        $('#fk_a006_num_miscelaneo_detalle_paso').append('<option value="">Seleccione Paso</option>');

                        for(var i=0;i<data['pasos'].length;i++){

                            $('#fk_a006_num_miscelaneo_detalle_paso').append('<option value="'+data['pasos'][i]['fk_a006_num_miscelaneo_detalle_paso']+'">'+data['pasos'][i]['ind_nombre_detalle']+'</option>');

                        }

                    }



                },'json');

            });

        });

        $("#fk_a006_num_miscelaneo_detalle_paso").change(function () {

            $("#fk_a006_num_miscelaneo_detalle_paso option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/cargosCONTROL/ActualizarSueldoPasosMET", { Grado: $('#fk_rhc007_num_grado').val(), Paso: $('#fk_a006_num_miscelaneo_detalle_paso').val() }, function(data){

                    $("#num_sueldo_basico").val(data);

                },'json');

            });

        });


        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );


    });






</script>