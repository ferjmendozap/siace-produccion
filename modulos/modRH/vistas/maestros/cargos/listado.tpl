
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Cargos</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableCargos" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Cod</th>
                                    <th>Grupo</th>
                                    <th>Serie</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <th colspan="5">
                                        {if in_array('RH-01-04-01-26-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Cargo" title="Nuevo Cargo" titulo="Nuevo cargo" id="nuevo" >
                                                    <i class="md md-create"></i> Nuevo Cargo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/cargosCONTROL/NuevoCargoMET';


        //INICIALIZAR LA DATATABLES
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#datatableCargos',
                "{$_Parametros.url}modRH/maestros/cargosCONTROL/JsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_puestos" },
                    { "data": "grupo_ocupacional" },
                    { "data": "ind_nombre_serie" },
                    { "data": "ind_descripcion_cargo" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableCargos tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idCargo: $(this).attr('idCargo')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatableCargos tbody').on( 'click', '.eliminar', function () {

            var idCargo=$(this).attr('idCargo');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/cargosCONTROL/EliminarCargoMET';
                $.post($url, { idCargo: idCargo},function($dato){
                    if($dato['status']=='OK'){
                        app.metEliminarRegistroJson('datatableCargos','El Cargo fue Eliminado.','cerrarModal','ContenidoModal');
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>