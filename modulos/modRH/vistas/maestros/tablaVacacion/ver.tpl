<div class="form floating-label">
	<div class="modal-body" >
		<div class="form-group floating-label">
			<select class="form-control dirty">
				<option selected>{$tabla.ind_nombre_nomina}</option>
			</select>
			<label for=""><i class="glyphicon glyphicon-home"></i> Tipo de Nómina</label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control"  name="anios" id="anios" value="{$tabla.num_anios}" disabled>
			<label for="anios"><i class="md md-create"></i> Años </label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control"  name="dias_disfrute" id="dias_disfrute" value="{$tabla.num_dias_disfrute}" disabled>
			<label for="dias_disfrute"><i class="fa fa-certificate"></i> Dias de Disfrute </label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control"  name="dias_adicionales" id="dias_adicionales" value="{$tabla.num_dias_adicionales}" disabled>
			<label for="dias_adicionales"><i class="md md-done-all"></i> Dias de Adicionales </label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control dirty" disabled  name="total_disfrute" id="total_disfrute" value="{$tabla.num_total_disfrutar}" disabled>
			<label for="total_disfrute"><i class="md md-add-alarm"></i> Total Disfrute </label>
		</div>
	</div>
<div align="right">
	<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
</div>