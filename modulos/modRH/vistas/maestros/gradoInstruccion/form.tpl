<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idGradoInstruccion}" name="idGradoInstruccion"/>
        <input type="hidden" value="{if isset($idMaestro)}{$idMaestro}{/if}" name="form[int][idMaestro]"/>

        <!-- ***************************ID GRADO INSTRUCCION*********************--------->

        <div class="col-xs-6">

            <div class="form-group floating-label" id="ind_nombre_cargoError">

                <input type="text" class="form-control input-sm" maxlength="4" value="{if isset($formDB.cod_detalle)}{$formDB.cod_detalle}{/if}" name="form[txt][cod_detalle]" id="cod_detalle" required data-msg-required="Introduzca Codigo Detalle" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="cod_detalle">Codigo Detalle</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <!-- ***************************DESCRIPCION GRADO INSTRUCCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_detalle)}{$formDB.ind_nombre_detalle}{/if}" name="form[txt][ind_nombre_instruccion]" id="ind_nombre_instruccion" required data-msg-required="Introduzca descripcion del grado de Instrucción" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_instruccion">Descripción</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"> <span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/gradoInstruccionCONTROL/NuevoGradoInstruccionMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idGradoInstruccion'+dato['idGradoInstruccion'])).html('<td>'+dato['idGradoInstruccion']+'</td>' +
                            '<td>'+dato['ind_nombre_instruccion']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idGradoInstruccion="'+dato['idGradoInstruccion']+'"' +
                            'descipcion="El Usuario a Modificado un Grado de Instrucción" titulo="Modificar Grado de Instrucción">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGradoInstruccion="'+dato['idGradoInstruccion']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Grado de Instrucción" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Grado de Instrucción!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Grado de Instrucción se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatable1')).append('<tr  id="idGradoInstruccion'+dato['idGradoInstruccion']+'">' +
                            '<td>'+dato['idGradoInstruccion']+'</td>' +
                            '<td>'+dato['ind_nombre_instruccion']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idGradoInstruccion="'+dato['idGradoInstruccion']+'"' +
                            'descipcion="El Usuario a Modificado un Grado de Instruccion" titulo="Modificar Grado de Instrucción">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGradoInstruccion="'+dato['idGradoInstruccion']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Grado de Instrucción" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Grado de Instrucción!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "EL Grado de Instrucción fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "60%" );


    });






</script>