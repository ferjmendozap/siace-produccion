<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idFeriado}" name="idFeriado"/>



        <!-- ***************************ID FERIADO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_feriado)}{$formDB.pk_num_feriado}{/if}" name="form[int][pk_num_feriado]" id="pk_num_feriado" disabled>
                <label for="pk_num_feriado">Feriado</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <!-- ***************************ANIO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{if isset($anio)}{$anio}{/if}{/if}" name="form[alphaNum][fec_anio]" id="fec_anio" maxlength="4" readonly>
                <label for="fec_anio">Año</label>
            </div>
        </div>

        <!-- ***************************DIA*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <div class="input-group date" id="fec_dia">
                    <div class="input-group-content">
                        <input type="text" class="form-control input-sm" value="{if isset($formDB.fec_dia)}{$formDB.fec_dia}{/if}" name="form[txt][fec_dia]" id="fec_dia" maxlength="5" required data-msg-required="Introduzca Día Feriado">
                        <label for="fec_dia">Dia Feriado</label>
                    </div>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <p class="help-block"><span class="text-xs" style="color: red">* (mm-dd) (mes-día)</span></p>
                </div>
            </div>
        </div>

        <!-- ***************************DESCRIPCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_descripcion_feriado)}{$formDB.ind_descripcion_feriado}{/if}" name="form[txt][ind_descripcion_feriado]" id="ind_descripcion_feriado" maxlength="100" required data-msg-required="Introduzca Descripción del Día Feriado" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_descripcion_feriado">Descripción Día Feriado</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************ESTATUS*********************--------->
        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_variable) and $formDB.num_flag_variable==1} checked{/if} value="1" name="form[int][num_flag_variable]">
                    <span>Variable todos los años?</span>
                </label>
            </div>
        </div>




        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>  Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">

    //inicializar el datepicker
    $("#fec_dia").datepicker({
        format:'mm-dd',
        language:'es',
        autoclose: true
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/diasFeriadosCONTROL/DiasFeriadosMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idFeriado'+dato['idFeriado'])).html('<td>'+dato['fec_dia']+'</td>' +
                            '<td>'+dato['ind_descripcion_feriado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-10-04-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idFeriado="'+dato['idFeriado']+'"' +
                            'descipcion="El Usuario a Modificado un Día Feriado" titulo="Modificar Día Feriado">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-10-05-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idFeriado="'+dato['idFeriado']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Día Feriado" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Día Feriado!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Día Feriado se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){
                    $(document.getElementById('tablaFeriados')).append('<tr id="idFeriado'+dato['idFeriado']+'">' +
                            '<td>'+dato['fec_dia']+'</td>' +
                            '<td>'+dato['ind_descripcion_feriado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-10-04-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idFeriado="'+dato['idFeriado']+'"' +
                            'descipcion="El Usuario a Modificado un Día Feriado" title="Editar" titulo="Modificar Día Feriado">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-10-05-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idFeriado="'+dato['idFeriado']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Día Feriado" title="Eliminar"  titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Día Feriado!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "EL Día Feriado fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();
        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );

    });






</script>