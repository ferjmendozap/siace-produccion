<form action="" id="formAjax" class="form" role="form" method="post" autocomplete="off">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idNivelesTiposCargos}" name="idNivelesTiposCargos"/>


        <div class="col-xs-5">

            <div class="form-group floating-label">

                <select id="fk_rhc006_num_cargo" name="form[int][fk_rhc006_num_cargo]" class="form-control" required data-msg-required="Introduzca Nombre del Tipo de Cargo">
                    <option value="">&nbsp;</option>
                    {foreach item=tipoCargo from=$tiposCargos}
                        {if isset($formDB.fk_rhc006_num_cargo)}
                            {if $tipoCargo.pk_num_cargo==$formDB.fk_rhc006_num_cargo}
                                <option selected value="{$tipoCargo.pk_num_cargo}">{$tipoCargo.ind_nombre_cargo}</option>
                            {else}
                                <option value="{$tipoCargo.pk_num_cargo}">{$tipoCargo.ind_nombre_cargo}</option>
                            {/if}
                        {else}
                            <option value="{$tipoCargo.pk_num_cargo}">{$tipoCargo.ind_nombre_cargo}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_rhc006_num_cargo">Tipo de Cargo</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>


            </div>

        </div>
        <div class="col-xs-12">

            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.num_nivel)}{$formDB.num_nivel}{/if}" name="form[int][num_nivel]" id="num_nivel" readonly>
                <label for="num_nivel">Nivel</label>

            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_nivel)}{$formDB.ind_nombre_nivel}{/if}" name="form[txt][ind_nombre_nivel]" id="ind_nombre_nivel" required data-msg-required="Introduzca Descripción Nivel del Tipo de Cargo" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_nivel">Descripción Nivel</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"> <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            var app = new  AppFunciones();

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/nivelesTiposCargosCONTROL/NuevoNivelesTiposCargosMET', datos ,function(dato){

                if(dato['status']=='modificacion'){
                    app.metActualizarRegistroTablaJson('datatableNivelTipoCargo','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='creacion'){
                    app.metNuevoRegistroTablaJson('datatableNivelTipoCargo','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }

            },'json');


        }
    });




    $().ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //dependiendo del tipo cargo seleccionado consulto en la base de datos cual es su ultimo nivel asigando
        //y los muestro en el formulario
        $("#fk_rhc006_num_cargo").change(function(){


            $.post('{$_Parametros.url}modRH/maestros/nivelesTiposCargosCONTROL/VerificarNivelMET', { cargo: $('#fk_rhc006_num_cargo').val() } ,function(dato){


                if(dato['ultimo']==null){
                    $("#num_nivel").val('0');
                    $("#num_nivel").focus();
                }else{
                    $("#num_nivel").val(dato['ultimo']);
                    $("#num_nivel").focus();
                }


            },'json');

        });


        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );


    });








</script>