
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Serie Ocupacional</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableSerieOcupacional" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Cod</th>
                                    <th>Grupo Ocupacional</th>
                                    <th>Serie Ocupacional</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-01-14-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado una Serie Ocupacional" title="Nueva Serie Ocupacional"  titulo="Nueva Serie Ocupacional" id="nuevo" >
                                                <i class="md md-create"></i> Nueva Serie Ocupacional &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">


    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/serieOcupacionalCONTROL/NuevaSerieOcupacionalMET';

        var app = new AppFunciones();

        //INICIALIZAR DATATABLES
        var table = $('#datatableSerieOcupacional').DataTable({
            "dom": 'lCfrtip',
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": '{$_Parametros.url}modRH/maestros/serieOcupacionalCONTROL/JsonDataTablaMET',
                "type": "POST",
                "data": function ( d ) {
                    //d.filtro = filtrado;
                }
            },
            "columnDefs": [
                { "visible" : false, "targets": 1 }
            ],
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "processing":     "Procesando...",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "loadingRecords": "Cargando...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "columns": [
                { "data": "pk_num_serie" },
                { "data": "ind_nombre_detalle" },
                { "data": "ind_nombre_serie" },
                { "orderable": false,"data": "acciones",'width':100 }
            ],
            "drawCallback" : function (settings) {
                var api = this.api();
                var rows = api.rows( { page:'current'} ).nodes();
                var last = null;

                api.column(1, { page:'current'} ).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"> Grupo Ocupacional: ' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#datatableSerieOcupacional tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });


        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableSerieOcupacional tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idSerieOcupacional: $(this).attr('idSerieOcupacional')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatableSerieOcupacional tbody').on( 'click', '.eliminar', function () {

            var idSerieOcupacional=$(this).attr('idSerieOcupacional');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/serieOcupacionalCONTROL/EliminarSerieOcupacionalMET';
                $.post($url, { idSerieOcupacional: idSerieOcupacional},function($dato){
                    if($dato['status']=='OK'){
                        app.metEliminarRegistroJson('datatableSerieOcupacional','La Serie Ocupacional eliminada con Exito.','cerrarModal','ContenidoModal');
                        $('#cerrar').click();
                    }
                },'json');
            });
        });



    });
</script>