<div class="col-md-12">
    <ul class="nav nav-tabs" data-toggle="tabs">
        <li class="active"><a href="#first1">Listado por Mes</a></li>
        <li><a href="#second1">Cuadro Demostrativo</a></li>
        <li><a href="#second2">Cuadro Jubilación</a></li>
    </ul>
    <div class="card-body tab-content">
        <div class="tab-pane active" id="first1">
           <iframe src="{$_Parametros.url}modRH/reportes/aniversarioCONTROL/GenerarReporteAniversarioMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&centro_costo={$datos.centro_costo}&mes_ingreso={$datos.mes_ingreso}&anio1={$datos.anio1}&anio2={$datos.anio2}&edo_reg={$datos.edoReg}&sitTrab={$datos.sitTrab}&tipo_nomina={$datos.tipoNomina}&tipoTrabajador={$datos.tipoTrabajador}" width="100%" height="950"></iframe>
        </div>
        <div class="tab-pane" id="second1">
            <iframe src="{$_Parametros.url}modRH/reportes/aniversarioCONTROL/GenerarReporteDemostrativoMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&centro_costo={$datos.centro_costo}&anio1={$datos.anio1}&anio2={$datos.anio2}&edo_reg={$datos.edoReg}&sitTrab={$datos.sitTrab}&tipo_nomina={$datos.tipoNomina}&tipoTrabajador={$datos.tipoTrabajador}" width="100%" height="950"></iframe>
        </div>
        <div class="tab-pane" id="second2">
            <iframe src="{$_Parametros.url}modRH/reportes/aniversarioCONTROL/GenerarReporteJubilacionMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&centro_costo={$datos.centro_costo}&anio1={$datos.anio1}&anio2={$datos.anio2}&edo_reg={$datos.edoReg}&sitTrab={$datos.sitTrab}&tipo_nomina={$datos.tipoNomina}&tipoTrabajador={$datos.tipoTrabajador}" width="100%" height="950"></iframe>
        </div>
    </div>
</div>
