<div class="form floating-label">
    <br/>
    <section class="style-default-bright">
        <h2 class="text-primary">&nbsp;Aniversario</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarCentroCosto(this.value)">
                            <option value="">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                    <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-home"></i> Dependencia</label>
                    </div>
                </div>
                <div id="centroCosto">
                    <div class="form-group floating-label">
                        <select id="centro_costo" name="centro_costo" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=centro from=$centroCosto}
                                <option value="{$centro.pk_num_centro_costo}">{$centro.ind_descripcion_centro_costo}</option>
                            {/foreach}
                        </select>
                        <label for="centro_costo"><i class="glyphicon glyphicon-briefcase"></i> Centro de Costo</label>
                    </div>
                </div>
                <div id="centroCosto">
                    <div class="form-group floating-label">
                        <select id="mes_ingreso" name="mes_ingreso" class="form-control dirty">
                                <option value="0"></option>
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Septiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
                        </select>
                        <label for="mes_ingreso"><i class="glyphicon glyphicon-calendar"></i> Mes de Ingreso</label>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8">
                    <table align="left">
                        <tr>
                            <td>Años:&nbsp;&nbsp;</td>
                            <td>
                                <div class="form-group">
                                    <input type="text" class="form-control dirty" name="anio1" id="anio1">
                                    <label class="control-label"> Desde</label>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <div class="form-group">
                                    <input type="text" class="form-control dirty" name="anio2" id="anio2">
                                    <label class="control-label">Hasta</label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="edo_reg" name="edo_reg" class="form-control dirty">
                        <option value="1">ACTIVO</option>
                        <option value="0">INACTIVO</option>
                    </select>
                    <label><i class="glyphicon glyphicon-briefcase"></i> Estado del Registro</label>
                </div>
                <div class="form-group floating-label">
                    <select id="sit_trab" name="sit_trab" class="form-control dirty">
                        <option value="1">ACTIVO</option>
                        <option value="0">INACTIVO</option>
                    </select>
                    <label><i class="glyphicon glyphicon-briefcase"></i> Situación de Trabajo</label>
                </div>
                <div class="form-group floating-label">
                    <select id="tipo_nomina" name="tipo_nomina" class="form-control dirty">
                        <option value="">&nbsp;</option>
                        {foreach item=nom from=$nomina}
                            <option value="{$nom.pk_num_tipo_nomina}">{$nom.ind_nombre_nomina}</option>
                        {/foreach}
                    </select>
                    <label for="tipo_nomina"><i class="glyphicon glyphicon-list"></i> Tipo de Nómina</label>
                </div>
                <div class="form-group floating-label">
                    <select id="tipo_trabajador" name="tipo_trabajador" class="form-control dirty">
                        <option value="">&nbsp;</option>
                        {foreach item=tipo from=$tipoTrabajador}
                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                        {/foreach}
                    </select>
                    <label><i class="glyphicon glyphicon-briefcase"></i> Tipo de Trabajador</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-sm-12" align="center">
            <button  class="btn btn-primary ink-reaction btn-raised" descripcion="El usuario ha visualizado el reporte de aniversario" data-toggle="modal" data-target="#formModal" titulo="Aniversario" id="aniversario"> Buscar</button>
        </div>
<br/><br/><br/>
    </div>
</section>
<script type="text/javascript">
    $(".fecha").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    var $url='{$_Parametros.url}modRH/reportes/aniversarioCONTROL/GenerarAniversarioMET';
    $('#aniversario').click(function(){
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        var pkNumOrganismo = $("#pk_num_organismo").val();
        var pk_num_dependencia = $("#pk_num_dependencia").val();
        var centro_costo = $("#centro_costo").val();
        var mes_ingreso = $("#mes_ingreso").val();
        var anio1 = $("#anio1").val();
        var anio2 = $("#anio2").val();
        var edo_reg = $("#edo_reg").val();
        var sit_trab = $("#sit_trab").val();
        var tipo_nomina = $("#tipo_nomina").val();
        var tipo_trabajador = $("#tipo_trabajador").val();
        $.post($url,{ pk_num_organismo: pkNumOrganismo, mes_ingreso: mes_ingreso, pk_num_dependencia: pk_num_dependencia, centro_costo: centro_costo, anio1: anio1, anio2: anio2, edo_reg: edo_reg, sit_trab: sit_trab, tipo_nomina: tipo_nomina, tipo_trabajador: tipo_trabajador },function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/reportes/aniversarioCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarCentroCosto(pk_num_dependencia) {
        var pk_num_organismo = $("#pk_num_organismo").val();
        $("#centroCosto").html("");
        $.post("{$_Parametros.url}modRH/reportes/aniversarioCONTROL/BuscarCentroCostoMET",{ pk_num_dependencia:""+pk_num_dependencia, pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#centroCosto").html(dato);
        });
    }
</script>