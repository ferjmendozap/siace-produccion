<div class="form floating-label">
    <br/>
    <section class="style-default-bright">
        <h2 class="text-primary">&nbsp;Listar Beneficio por Partidas</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                    <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-home"></i> Dependencia</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_ayuda_global" name="pk_num_ayuda_global" class="form-control dirty" onchange="cargarAyudaEspecifica(this.value)">
                        {foreach item=ayuda from=$ayudaMedica}
                            <option value="{$ayuda.pk_num_ayuda_global}">{$ayuda.ind_descripcion}</option>
                        {/foreach}
                    </select>
                    <label for="pk_num_ayuda_global"><i class="glyphicon glyphicon-heart"></i> Ayuda</label>
                </div>
                <div id="cargarAyuda">
                    <div class="form-group floating-label">
                        <select id="pk_num_ayuda_especifica" name="pk_num_ayuda_especifica" class="form-control dirty">
                            <option value=""></option>
                            {foreach item=listar from=$listarAyuda}
                                <option value="{$listar.pk_num_ayuda_especifica}">{$listar.ind_descripcion_especifica}</option>
                            {/foreach}
                        </select>
                        <label for="pk_num_ayuda_especifica"><i class="glyphicon glyphicon-list-alt"></i> Partidas</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-sm-12" align="center">
            <button type="submit" id="buscarPartidas" data-toggle="modal" data-target="#formModal" titulo="Listado de Beneficio por Partidas" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
        </div>
<br/><br/><br/>
    </div>
</section>
<script type="text/javascript">

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/reportes/resumenEventoCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarAyudaEspecifica(pk_num_ayuda_global) {
        $("#cargarAyuda").html("");
        $.post("{$_Parametros.url}modRH/reportes/hcmCONTROL/BuscarAyudaEspecificaMET",{ pk_num_ayuda_global:""+pk_num_ayuda_global }, function (dato) {
            $("#cargarAyuda").html(dato);
        });
    }

    $(document).ready(function() {
        $('#buscarPartidas').click(function () {
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var pk_num_ayuda_global = $("#pk_num_ayuda_global").val();
            var pk_num_ayuda_especifica = $("#pk_num_ayuda_especifica").val();
            var urlReporte = '{$_Parametros.url}modRH/reportes/hcmCONTROL/GenerarConsumoPartidasMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_dependencia='+pk_num_dependencia+'&pk_num_ayuda_especifica='+pk_num_ayuda_especifica+'&pk_num_ayuda_global='+pk_num_ayuda_global;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
        });
    });

</script>