<div class="form floating-label">
    <br/>
    <input type="hidden" name="pkNumEmpleado" id="pkNumEmpleado"/>
    <section class="style-default-bright">
        <h2 class="text-primary">&nbsp;Reporte de Carga Familiar</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                        <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                            {foreach item=org from=$listadoOrganismo}
                                {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                    <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {else}
                                    <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                    </div>
                    <div id="dependencia">
                        <div class="form-group floating-label">
                            <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarCentroCosto(this.value)">
                                <option value="">&nbsp;</option>
                                {foreach item=dep from=$listadoDependencia}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/foreach}
                            </select>
                            <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                        </div>
                    </div>
                    <div id="centroCosto">
                         <div class="form-group floating-label">
                            <select id="centro_costo" name="centro_costo" class="form-control dirty">
                                <option value="">&nbsp;</option>
                                {foreach item=centro from=$centroCosto}
                                    <option value="{$centro.pk_num_centro_costo}">{$centro.ind_descripcion_centro_costo}</option>
                                {/foreach}
                            </select>
                            <label for="centro_costo"><i class="glyphicon glyphicon-home"></i> Centro de Costo</label>
                        </div>
                    </div>
                     <div class="form-group floating-label">
                        <select id="tipo_nomina" name="tipo_nomina" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=nom from=$nomina}
                                <option value="{$nom.pk_num_tipo_nomina}">{$nom.ind_nombre_nomina}</option>
                            {/foreach}
                        </select>
                        <label for="tipo_nomina"><i class="glyphicon glyphicon-list-alt"></i> Tipo de Nómina</label>
                    </div>
                </div> 
                <div class="col-md-6 col-sm-6">
                    <div class="form-group floating-label">
                        <select id="edo_reg" name="edo_reg" class="form-control dirty">
                            <option value="1">ACTIVO</option>
                            <option value="0">INACTIVO</option>
                        </select>
                        <label><i class="glyphicon glyphicon-list-alt"></i> Estado del Registro</label>
                    </div>
                    <div class="form-group floating-label">
                        <select id="sit_trab" name="sit_trab" class="form-control dirty">
                            <option value="1">ACTIVO</option>
                            <option value="0">INACTIVO</option>
                        </select>
                        <label><i class="glyphicon glyphicon-briefcase"></i> Situación de Trabajo</label>
                    </div>
                     <div class="form-group floating-label">
                        <select id="sexo_empleado" name="sexo_empleado" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=sex from=$sexo}
                                <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                            {/foreach}
                        </select>
                        <label for="tipo_nomina"><i class="glyphicon glyphicon-th-large"></i> Sexo</label>
                    </div>
                    <div id="empleado">
                        <div class="form-group floating-label">
                            <select id="pk_num_empleado" name="pk_num_empleado" id="s2id_single" class="select2-container form-control select2 dirty">
                                <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                    <option value="">&nbsp;</option>
                                    {foreach item=empleado from=$listadoEmpleado}
                                        <option value="{$empleado.pk_num_empleado}">{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
                                    {/foreach}
                            </select>
                            <label for="pk_num_empleado"><i class="md md-person"></i>Buscar</label>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
         <div class="well clearfix">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-6 col-sm-6">
                	<div class="col-md-8 col-sm-8">                                                
                           <table align="left">
                                <tr>
                                    <td>Edad (Familiar)</td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" name="edad1" id="edad1">
                                            <label class="control-label">Desde</label>
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                     <td>
                                         <div class="form-group">
                                            <input type="text" class="form-control dirty" name="edad2" id="edad2">
                                            <label class="control-label">Hasta</label>
                                         </div>
                                    </td>
                                </tr>
                           </table>
                     </div>
                     <div class="col-md-8 col-sm-8"> 
                         <div class="form-group floating-label">
                            <select id="parentesco" name="parentesco" class="form-control dirty">
                                <option value="">&nbsp;</option>
                                {foreach item=parent from=$parentesco}
                                    <option value="{$parent.pk_num_miscelaneo_detalle}">{$parent.ind_nombre_detalle}</option>
                                {/foreach}
                            </select>
                            <label for="tipo_nomina"><i class="glyphicon glyphicon-briefcase"></i> Parentesco</label>
                        </div>
                        <div class="form-group floating-label">
                            <select id="edoCivil" name="edoCivil" class="form-control dirty">
                                <option value="">&nbsp;</option>
                                {foreach item=civil from=$edoCivil}
                                    <option value="{$civil.pk_num_miscelaneo_detalle}">{$civil.ind_nombre_detalle}</option>
                                {/foreach}
                            </select>
                            <label for="tipo_nomina"><i class="glyphicon glyphicon-th"></i> Edo. Civil (Familiar)</label>
                        </div>
                    </div>        
                </div> 
                <div class="col-md-6 col-sm-6">
                    <div class="form-group floating-label">
                        <select id="sexo_familiar" name="sexo_familiar" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=sex from=$sexo}
                                <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                            {/foreach}
                        </select>
                        <label for="sexo_empleado"><i class="glyphicon glyphicon-th-large"></i> Sexo (Familiar)</label>
                    </div> 
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="seguro" id="seguro" disabled>
                            <span>Afiliado al Seguro Médico</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="discapacidad" id="discapacidad">
                            <span>Familiar con Discapacidad o Especial </span>
                        </label>
                    </div>
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="estudiante" id="estudiante" value="1">
                            <span>Familiar Estudiante</span>
                        </label>
                    </div>
                </div>     
            </div>
        </div>
        <div class="col-sm-12 col-sm-12" align="center">
            <button type="submit" id="buscarTrabajador" data-toggle="modal" data-target="#formModal" titulo="Listado de Empleados" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
        </div>
        <br/>
     <br/>   
</div>
</section>
<script type="text/javascript">
    
    $(document).ready(function() {
        $('#buscarTrabajador').click(function () {
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia= $("#pk_num_dependencia").val();
            var centro_costo = $("#centro_costo").val();
            var tipo_nomina = $("#tipo_nomina").val();
            var edo_reg = $("#edo_reg").val();
            var sit_trab = $("#sit_trab").val();
            var sexo_empleado = $("#sexo_empleado").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var edad1 = $("#edad1").val();
            var edad2 = $("#edad2").val();
            var parentesco = $("#parentesco").val();
            var edoCivil = $("#edoCivil").val();
            var sexo_familiar = $("#sexo_familiar").val();
            var discapacidad = $("#discapacidad").is(':checked');
            var estudio = $("#estudiante").is(':checked');
            if(discapacidad)
                discapacidad = 1;
            else
                discapacidad = '';
            if(estudio)
                estudio = 1;
            else
                estudio = '';
            var urlReporte = '{$_Parametros.url}modRH/reportes/cargaFamiliarCONTROL/GenerarReporteFamiliarMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_dependencia='+pk_num_dependencia+'&centro_costo='+centro_costo+'&tipo_nomina='+tipo_nomina+'&edo_reg='+edo_reg+'&sit_trab='+sit_trab+'&sexo_empleado='+sexo_empleado+'&pk_num_empleado='+pk_num_empleado+'&edad1='+edad1+'&edad2='+edad2+'&parentesco='+parentesco+'&edoCivil='+edoCivil+'&sexo_familiar='+sexo_familiar+'&discapacidad='+discapacidad+'&estudio='+estudio;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
        });
    });

    var placeholder = "";

    $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
    $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult( repo ) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    function repoFormatSelection( repo ) {
        return repo.full_name;
    }

    $( "button[data-select2-open]" ).click( function() {
        $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
    });

    function cargarDependencia(pk_num_organismo) {
    $("#dependencia").html("");
    $.post("{$_Parametros.url}modRH/reportes/cargaFamiliarCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
    $("#dependencia").html(dato);
    });
    }

    function cargarCentroCosto(pk_num_dependencia) {
        var pk_num_organismo = $("#pk_num_organismo").val();
        $("#centroCosto").html("");
        $.post("{$_Parametros.url}modRH/reportes/cargaFamiliarCONTROL/BuscarCentroCostoMET",{ pk_num_dependencia:""+pk_num_dependencia, pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#centroCosto").html(dato);
        });
    }
</script>
