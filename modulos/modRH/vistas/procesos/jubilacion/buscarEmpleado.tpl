<input type="hidden" name="requisito" id="requisito"/>
<input type="hidden" name="anioTotal" id="anioTotal"/>
<input type="hidden" name="primaSueldo" id="primaSueldo"/>
<input type="hidden" name="totalSueldo" id="totalSueldo"/>
<div class="form floating-label">
	<section class="style-default-bright">
		<div class="well clearfix">
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<select id="listado_organismo" name="listado_organismo" class="form-control dirty" onchange="cargarDependenciaListado(this.value)">
						{foreach item=org from=$listadoOrganismo}
							{if $org.pk_num_organismo == $emp.pk_num_organismo}
								<option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
							{else}
								<option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
							{/if}
						{/foreach}
					</select>
					<label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
				</div>
				<div id="dependencia_listado">
					<div class="form-group floating-label">
						<select id="listado_dependencia" name="listado_dependencia" class="form-control" onchange="cargarCentroListado(this.value)">
							<option value="">&nbsp;</option>
							{foreach item=dep from=$listadoDependencia}
								{if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
									<option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
								{else}
									<option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
								{/if}
							{/foreach}
						</select>
						<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
					</div>
				</div>
				<div id="centro_listado">
					<div class="form-group floating-label">
						<select id="centro_costo" name="centro_costo" class="form-control dirty">
							<option value="">&nbsp;</option>
							{foreach item=centro from=$centroCosto}
								<option value="{$centro.pk_num_centro_costo}">{$centro.ind_descripcion_centro_costo}</option>
							{/foreach}
						</select>
						<label for="centro_costo"><i class="glyphicon glyphicon-home"></i> Centro de Costo</label>
					</div>
				</div>
				<div id="tipoNomina">
					<div class="form-group floating-label">
						<select id="tipo_nomina_buscar" name="tipo_nomina_buscar" class="form-control dirty">
							<option value="">&nbsp;</option>
							{foreach item=nom from=$nomina}
								<option value="{$nom.pk_num_tipo_nomina}">{$nom.ind_nombre_nomina}</option>
							{/foreach}
						</select>
						<label for="tipo_nomina"><i class="glyphicon glyphicon-briefcase"></i> Tipo de Nómina</label>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div id="tipoTrabajador">
					<div class="form-group floating-label">
						<select id="tipo_trabajador_buscar" name="tipo_trabajador_buscar" class="form-control dirty">
							<option value="">&nbsp;</option>
							{foreach item=tipo from=$tipoTrabajador}
								<option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
							{/foreach}
						</select>
						<label><i class="glyphicon glyphicon-briefcase"></i> Tipo de Trabajador</label>
					</div>
				</div>
				<div id="edoReg">
					<div class="form-group floating-label">
						<select id="edo_reg" name="edo_reg" class="form-control dirty">
							<option value="1">ACTIVO</option>
							<option value="0">INACTIVO</option>
						</select>
						<label><i class="glyphicon glyphicon-briefcase"></i> Estado del Registro</label>
					</div>
				</div>
				<div id="sitTrab">
					<div class="form-group floating-label">
						<select id="sit_trab" name="sit_trab" class="form-control dirty">
							<option value="1">ACTIVO</option>
							<option value="0">INACTIVO</option>
						</select>
						<label><i class="glyphicon glyphicon-briefcase"></i> Situación de Trabajo</label>
					</div>
				</div>
				<div class="form-group" style="width:100%">
					<div class="input-daterange input-group" id="demo-date-range">
						<div class="input-group-content">
							<input type="text" class="fecha form-control" name="fechaInicio" id="fecha_inicio">
							<label><i class="glyphicon glyphicon-calendar"></i> Fecha de Ingreso</label>
						</div>
						<span class="input-group-addon">a</span>
						<div class="input-group-content">
							<input type="text" class="fecha form-control" name="fechaFin" id="fecha_fin">
							<div class="form-control-line"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-sm-12" align="center">
			<button type="submit" onclick="buscarTrabajador()" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk" ></span> Buscar</button>
		</div>
</div>
<br/><br/>
<table id="datatable2" class="table table-striped table-hover">
	<thead>
	<tr align="center">
		<th><i class="md-person"></i> Nº</th>
		<th><i class="md-person"></i> Código</th>
		<th><i class="md-person"></i> Nombre Completo</th>
		<th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Documento</th>
		<th> Seleccionar</th>
	</tr>
	</thead>
	<tbody>
	{assign var="numero" value="1"}
	{foreach item=datos from=$datosTrabajador}
		<tr>
			<td>{$numero++}</td>
			<td>{$datos.pk_num_empleado}</td>
			<td>{$datos.ind_nombre1} {$datos.ind_nombre2} {$datos.ind_apellido1} {$datos.ind_apellido2} </td>
			<td>{$datos.ind_cedula_documento}</td>
			<td><button type="button" pk_num_empleado="{$datos.pk_num_empleado}" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button></td>
		</tr>
	{/foreach}
	</tbody>
</table>
</section>
<script type="text/javascript">
	// Inicializador de DataTables
	$(".fecha").datepicker({
		format: 'dd/mm/yyyy',
	});


	// Buscador
	function buscarTrabajador()
	{
		var pk_num_organismo = $("#listado_organismo").val();
		var pk_num_dependencia = $("#listado_dependencia").val();
		var centro_costo = $("#centro_costo").val();
		var tipo_nomina_buscar = $("#tipo_nomina_buscar").val();
		var tipo_trabajador_buscar = $("#tipo_trabajador_buscar").val();
		var estado_registro = $("#edo_reg").val();
		var situacion_trabajo = $("#sit_trab").val();
		var fecha_inicio = $("#fecha_inicio").val();
		var fecha_fin = $("#fecha_fin").val();
		var url_listar='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/BuscarTrabajadorMET';
		$.post(url_listar,{ pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, centro_costo: centro_costo, tipo_nomina_buscar: tipo_nomina_buscar, tipo_trabajador_buscar: tipo_trabajador_buscar, estado_registro: estado_registro, situacion_trabajo: situacion_trabajo, fecha_inicio: fecha_inicio, fecha_fin: fecha_fin},function(respuesta_post) {
			var tabla_listado = $('#datatable2').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					var botonCargar = '<button type="button" pk_num_empleado = "' + respuesta_post[i].pk_num_empleado + '" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button>';
					tabla_listado.row.add([
						i+1,
						respuesta_post[i].pk_num_empleado,
						respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
						respuesta_post[i].ind_cedula_documento,
						botonCargar
					]).draw()
							.nodes()
							.to$()
				}
			}

		},'json');
	}

	function calcular_edad(fecha)
	{
		//calculo la fecha de hoy
		hoy=new Date()
		//alert(hoy)
		//calculo la fecha que recibo
		//La descompongo en un array
		var array_fecha = fecha.split("/")
		//si el array no tiene tres partes, la fecha es incorrecta
		if (array_fecha.length!=3)
			return false
		//compruebo que los anio, mes, dia son correctos
		var anio
		anio = parseInt(array_fecha[2]);
		if (isNaN(anio))
			return false
		var mes
		mes = parseInt(array_fecha[1]);
		if (isNaN(mes))return false
		var dia
		dia = parseInt(array_fecha[0]);
		if (isNaN(dia))
			return false
		//resto los años de las dos fechas
		 var edad=hoy.getFullYear()- anio - 1; //-1 porque no se si ha cumplido años ya este año

		//si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido
		if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
			return edad
		if (hoy.getMonth() + 1 - mes > 0)
			return edad+1
		//entonces es que eran iguales. miro los dias
		//si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido
		if (hoy.getDate() - dia >= 0)
			return edad + 1
		return edad
	}

	var formatNumber = {
		separador: ".", // separador para los miles
		sepDecimal: ',', // separador para los decimales
		formatear:function (num){
			num +='';
			var splitStr = num.split('.');
			var splitLeft = splitStr[0];
			var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
			var regx = /(\d+)(\d{3})/;
			while (regx.test(splitLeft)) {
				splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
			}
			return this.simbol + splitLeft  +splitRight;
		},
		new:function(num, simbol){
			this.simbol = simbol ||'';
			return this.formatear(num);
		}
	}

	Number.prototype.format = function(number){
		var result = '';
		while( number.length > 3 ) {
			result = '.' + number.substr(number.length - 3) + result;
			number = number.substring(0, number.length - 3);
		}
		result = number + result;
		return result;
	};

	// Seleccion de empleados
	$('#datatable2 tbody').on( 'click', '.cargar', function (respuesta_post) {
		// **********************************************PASO 1*************************************************
		var $urlCargar='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/CargarEmpleadoMET';
		$.post($urlCargar,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(dato){
			var $nombre = dato['ind_nombre1']+' '+dato['ind_nombre2']+' '+dato['ind_apellido1']+' '+dato['ind_apellido2'];
			var edad = calcular_edad(dato['fecha_nacimiento']);
			var salarioPrevio = parseFloat(dato['num_sueldo_basico']);
			var salario = formatNumber.new(salarioPrevio);
			var cedula = Number.prototype.format(dato['ind_cedula_documento']);
			$("#num_organismo").val(dato['pk_num_organismo']);
			$("#num_dependencia").val(dato['pk_num_dependencia']);
			$("#cargo").val(dato['pk_num_puestos']);
			$("#nombreTrabajador").val($nombre);
			$("#pkNumEmpleado").val(dato['pk_num_empleado']);
			$("#num_documento").val(cedula);
			$("#sexo").val(dato['fk_a006_num_miscelaneo_detalle_sexo']);
			$("#nacimiento").val(dato['fecha_nacimiento']);
			$("#edad").val(edad);
			$("#fecha_ingreso").val(dato['fecha_ingreso']);
			$("#salario").val(dato['num_sueldo_basico']);
			$("#num_organismo_previo").val(dato['ind_descripcion_empresa']);
			$("#num_dependencia_previo").val(dato['ind_dependencia']);
			$("#cargo_previo").val(dato['ind_descripcion_cargo']);
			$("#nombreTrabajador_previo").val($nombre);
			$("#num_documento_previo").val(cedula);
			$("#sexo_previo").val(dato['ind_nombre_detalle']);
			$("#nacimiento_previo").val(dato['fecha_nacimiento']);
			$("#edad_previo").val(edad);
			$("#fecha_ingreso_previo").val(dato['fecha_ingreso']);
			$("#salario_previo").val(salario);
		},'json');
		var $urlServicio='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/CalcularServicioMET';
		$.post($urlServicio,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(datoServicio){
			var requisito = datoServicio['requisito'];
			if(requisito==1){
				$('#requisitoVer').html('<div class="alert alert-success"><strong><i class="glyphicon glyphicon-ok"></i></strong> El empleado cumple con los requisitos para optar a la Jubilación.</div>');
				$('#botonGuardar').html('<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;<button type="submit" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Procesar </button>');
			} else {
				$('#requisitoVer').html('<div class="alert alert-danger"><strong><i class="glyphicon glyphicon-alert"></i></strong> El Empleado NO cumple con los requisitos para optar a la Jubilación.</div>');
                $('#botonGuardar').html('<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>');
			}
		},'json');
		// **********************************************PASO 2*************************************************
		var $servicio='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/GenerarServicioMET';
		$.post($servicio,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(datoServicio){
			var tabla_listado = $('#datatable3').DataTable();
			tabla_listado.clear().draw();
			if(datoServicio != -1) {
				for(var i=0; i<datoServicio.length; i++) {
					tabla_listado.row.add([
						i+1,
						datoServicio[i].empresa,
						datoServicio[i].fechaIngreso,
						datoServicio[i].fechaEgreso,
						datoServicio[i].anio,
						datoServicio[i].mes,
						datoServicio[i].dia
					]).draw()
							.nodes()
							.to$()
				}
			}
		},'json');
		// Antecedentes totales
		var $totalServicio='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/CalcularServicioTotalMET';
		var tabla_listado = $('#datatable3').DataTable();
		tabla_listado.clear().draw();
		$.post($totalServicio,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(servicioTotal){
			var requisito = servicioTotal['anioTotal'];
			$(document.getElementById('datatable3')).append('<tr><td></td><td></td><td></td><td><b>Antecedentes:</b></td><td>'+servicioTotal['anio']+'</td><td>'+servicioTotal['mes']+'</td><td>'+servicioTotal['dia']+'</td></tr>');
			$(document.getElementById('datatable3')).append('<tr><td></td><td></td><td></td><td><b>En el Organismo:</b></td><td>'+servicioTotal['anioInst']+'</td><td>'+servicioTotal['mesInst']+'</td><td>'+servicioTotal['diaInst']+'</td></tr>');
			$(document.getElementById('datatable3')).append('<tr><td></td><td></td><td></td><td><b>Tiempo de Servicio:</b></td><td>'+servicioTotal['anioTotal']+'</td><td>'+servicioTotal['mesTotal']+'</td><td>'+servicioTotal['diaTotal']+'</td></tr>');
			$("#anioExperiencia").val(servicioTotal['anio']);
			$("#anioServicio").val(servicioTotal['anioInst']);
			$("#anioTotal").val(servicioTotal['anioTotal']);
		},'json');
		// Relación de sueldos. Sueldo Base
		var $urlSueldoBase='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/ConsultarRelacionBaseMET';
		$.post($urlSueldoBase,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(datoSueldo){
			var tablaSueldoBase = $('#datatable4').DataTable();
			tablaSueldoBase.clear().draw();
			if(datoSueldo != -1) {
				for(var i=0; i<datoSueldo.length; i++) {
					var numBase = parseFloat(datoSueldo[i].monto);
                    var numeroBase = numBase.toFixed(2);
                    var salario = formatNumber.new(numeroBase);
					tablaSueldoBase.row.add([
						i+1,
						datoSueldo[i].periodo,
                        salario
					]).draw()
							.nodes()
							.to$()
				}
			}
		},'json');
		// Relación de sueldos. Prima por Antecedente de Servicio
		var $urlSueldoPrima='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/ConsultarRelacionPrimaMET';
		$.post($urlSueldoPrima,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(datoPrima){
			var tablaSueldoPrima = $('#datatable5').DataTable();
			tablaSueldoPrima.clear().draw();
			if(datoPrima != -1) {
				for(var i=0; i<datoPrima.length; i++) {
					var numPrima = parseFloat(datoPrima[i].monto);
                    var numeroBase = numPrima.toFixed(2);
                    var salario = formatNumber.new(numeroBase);
					tablaSueldoPrima.row.add([
						i+1,
						datoPrima[i].periodo,
                        salario
					]).draw()
							.nodes()
							.to$()
				}
			}
		},'json');
		// Relación de sueldos. Prima por Antecedente de Servicio
		var $urlTotal='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/CalcularJubilacionMET';
		$.post($urlTotal,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(jubilacion){
			$("#sueldo").val(jubilacion['totalSueldoOriginal']);
			$("#prima").val(jubilacion['totalPrimaOriginal']);
			$("#total").val(jubilacion['totalOriginal']);
			$("#porcentaje").val(jubilacion['porcentajeOriginal']);
			$("#jubilacion").val(jubilacion['montoJubilacionOriginal']);
			$("#baseJubilacion").val(jubilacion['baseJubilacionOriginal']);
			$("#coeficiente").val(jubilacion['coeficienteOriginal']);
			$("#sueldo_previo").val(jubilacion['totalSueldo']);
			$("#prima_previo").val(jubilacion['totalPrima']);
			$("#total_previo").val(jubilacion['total']);
			$("#porcentaje_previo").val(jubilacion['porcentaje']);
			$("#jubilacion_previo").val(jubilacion['montoJubilacion']);
			$("#baseJubilacion_previo").val(jubilacion['baseJubilacion']);
		},'json');
		// Relación de sueldos. Prima por Antecedente de Servicio
		var $urlTotal='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/ComprobarEmpleadoMET';
		$.post($urlTotal,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(comprobar){
			var valor = comprobar['valor'];
			if(valor==1) {
				$('#botonGuardar').html('<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>');
				swal("No se puede procesar", "El empleado ya posee una jubilación registrada")
			} 
		},'json');
		$(document.getElementById('cerrarModal2')).click();
		$(document.getElementById('ContenidoModal2')).html('');
	});

	$(document).ready(function() {
		$('#datatable2').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );
	} );

	if ( $.fn.dataTable.isDataTable( '#datatable2' ) ) {
		table = $('#datatable2').DataTable();
	}
	else {
		table = $('#datatable2').DataTable( {
			paging: true
		} );
	}

	function cargarDependenciaListado(listado_organismo) {
		$("#dependencia_listado").html("");
		$.post("{$_Parametros.url}modRH/procesos/jubilacionCONTROL/BuscarDependenciaListadoMET",{ listado_organismo:""+listado_organismo }, function (dato) {
			$("#dependencia_listado").html(dato);
		});
	}

	function cargarCentroListado(listado_dependencia) {
		$("#centro_listado").html("");
		$.post("{$_Parametros.url}modRH/procesos/jubilacionCONTROL/BuscarCentroListadoMET",{ listado_dependencia:""+listado_dependencia }, function (dato) {
			$("#centro_listado").html(dato);
		});
	}
</script>