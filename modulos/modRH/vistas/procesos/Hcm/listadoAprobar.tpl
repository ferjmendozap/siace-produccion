
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Beneficios por Aprobar</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-head">
                            

                        </div>
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="col-sm-1" width="170">Nro </th>
                                    <th class="col-sm-1">Tipo</th>
                                    <th>Funcionario</th>
                                    <th>Beneficio</th>
                                    <th class="col-sm-1">Monto</th>
                                    <th class="col-sm-1"> Estado </th>
                                    <th class="col-sm-1"> Bitácora </th>
                                    {if in_array('RH-01-04-07-03-M',$_Parametros.perfil)}<th class="col-sm-1">  Aprobar </th> {/if}

                                    {if in_array('RH-01-04-07-03-M',$_Parametros.perfil)} <th class="col-sm-1"> Anular  </th> {/if}
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=b from=$Beneficio}

                                    <tr id="idBeneficioGlobal{$b.pk_beneficio_medico}">
                                        <td>{$b.pk_beneficio_medico}</td>
                                        <td>{$b.ind_nombre_detalle}</td>
                                        <td>{$b.ind_nombre1} {$b.ind_apellido1}</td>
                                        <td>{$b.ind_descripcion_especifica}</td>
                                        <td>{$b.num_monto|number_format:2:",":"."}</td>
                                        <td>{$b.estado_nombre}</td>




                                            {if in_array('RH-01-02-04-05-B',$_Parametros.perfil)}

                                            <td align="center" width="100">
                                                <button class="bitacora logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Bitácora de Movimientos" descipcion="El Usuario a Visualizado la Bitácora de Movimientos" idBitacora="{$b.pk_beneficio_medico}" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal">
                                                    <i class="md md-perm-identity" style="color: #ffffff;"></i>
                                                </button>

                                            </td>
                                            {/if}



                                            {if in_array('RH-01-04-07-03-M',$_Parametros.perfil)}
                                            <td align="center"  >
                                                <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idAprobar="{$b.pk_beneficio_medico}"
                                                        descipcion="Se ha visualizado un Beneficio Personal" title="Ver Beneficio HCM" titulo="Ver Beneficio Personal">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            </td>
                                            {/if}






                                            {if in_array('RH-01-04-07-03-M',$_Parametros.perfil)}
                                            <td align="center"  >
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="{$b.pk_beneficio_medico}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado Beneficio " title="Eliminar Beneficio HCM" titulo="Estas Seguro?" mensaje="Estas seguro que desea anular el Beneficio !!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            </td>
                                            {/if}



                                    </tr>
                                {/foreach}
                                </tbody>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {


        $('#datatable1 tbody').on( 'click', '.bitacora', function () {
            var $url='{$_Parametros.url}modRH/procesos/HcmCONTROL/BitacoraMET';

            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url,{ idHcm:$(this).attr('idBitacora')},function($dato){
                $('#ContenidoModal2').html($dato);
            });
            $('#modalAncho2').css( "width", "55%" );
        });


        $('#datatable1 tbody').on( 'click', '.aprobar', function () {
            var $url='{$_Parametros.url}modRH/procesos/HcmCONTROL/ModificarBeneficioMET';
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPost: $(this).attr('idAprobar'), tipo:"aprobar"},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idBeneficio=$(this).attr('idBeneficio');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/procesos/HcmCONTROL/AnularBeneficioMET';
                $.post($url, { idBeneficio: idBeneficio},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idBeneficio'+$dato['idBeneficio'])).html('');
                        swal("Anulado!", "Beneficio Anulado.", "success");

                    }
                },'json');

            });
        });
    });
</script>