<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Pensiones por Sobreviviente {$proceso}</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        {if $proceso=='Listado'}
                        <div class="card-head">

                                <div class="col-sm-12 text-right">

                                        <div class="btn-group">
                                            <a data-toggle="offcanvas" title="Filtro" class="btn  ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
                                        </div>



                                </div>
                        </div>
                        {else}
                        {/if}
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="sort-numeric col-sm-1">Nro.</th>
                                    <th class="sort-numeric col-sm-1">Empleado</th>
                                    <th class="sort-alpha col-sm-3">Nombre Completo</th>
                                    <th class="sort-alpha col-sm-3">Dependencia</th>
                                    <th class="col-sm-1">Fecha</th>
                                    <th class="col-sm-1">Estatus</th>
                                    <th  width="80">
                                        {if $proceso=='Listado'}
                                            Modificar
                                        {elseif $proceso=='Conformar'}
                                            Conformar
                                        {else}
                                            Aprobar
                                        {/if}

                                    </th>
                                    <th  width="40">Ver</th>
                                    <th  width="50">Anular</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$listadoPensionesSobreviviente}

                                    <tr id="idPensionSobreviviente{$dat.pk_num_proceso_pension}">
                                        <td>{$dat.pk_num_proceso_pension}</td>
                                        <td>{$dat.fk_rhb001_num_empleado}</td>
                                        <td>{$dat.nombre_completo}</td>
                                        <td>{$dat.ind_dependencia}</td>
                                        <td>{$dat.fec_fecha|date_format:"d-m-Y"}</td>
                                        <td>{if $dat.txt_estatus=='PR'}Preparado{elseif $dat.txt_estatus=='CO'}Conformado{elseif $dat.txt_estatus=='AP'}Aprobado{else}Anulado{/if}</td>
                                        <td class="text-center" width="80">
                                            {if $proceso=='Listado'}
                                                {if in_array('RH-01-02-01-01-01-02-M',$_Parametros.perfil)}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  idPensionSobreviviente="{$dat.pk_num_proceso_pension}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                            descipcion="Modificar Pensión Sobreviviente" title="Editar Pensión Sobreviviente" titulo="Modificar Pensión Sobreviente">
                                                        <i class="fa md-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {elseif $proceso=='Conformar'}
                                                {if in_array('RH-01-02-01-01-01-02-M',$_Parametros.perfil)}
                                                    <button class="conformar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idPensionSobreviviente="{$dat.pk_num_proceso_pension}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                            descipcion="Conformar Pensión Sobreviente" title="Conformar Pensión Sobreviviente" titulo="Conformar Pensión Sobreviente">
                                                        <i class="md md-done" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {else}
                                                {if in_array('RH-01-02-01-01-01-02-M',$_Parametros.perfil)}
                                                    <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idPensionSobreviviente="{$dat.pk_num_proceso_pension}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                            descipcion="Aprobar Pensión Sobreviente" title="Aprobar Pensión Sobreviviente" titulo="Aprobar Pensión Sobreviente">
                                                        <i class="md md-done" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {/if}

                                        </td>
                                        <td class="text-center" width="40">
                                            {if in_array('RH-01-02-01-01-01-03-C',$_Parametros.perfil)}
                                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idPensionSobreviviente="{$dat.pk_num_proceso_pension}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descipcion="Ver Datos Pensión Sobreviente" title="Ver datos" titulo="Ver Datos Pensión Sobreviente">
                                                    <i class="fa md-search" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                        <td class="text-center" width="50">
                                            {if in_array('RH-01-02-01-01-01-04-CE',$_Parametros.perfil)}
                                                <button class="anular btn ink-reaction btn-raised btn-xs btn-danger" idPensionSobreviviente="{$dat.pk_num_proceso_pension}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descipcion="Anular Pensión Sobreviente" title="Anular Pensión Sobreviviente" titulo="Anular Pensión Sobreviente">
                                                    <i class="fa md-block" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="9">
                                        {if $proceso=='Listado'}
                                            {if in_array('RH-01-02-01-01-01-01-N',$_Parametros.perfil)}
                                                <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                                        descripcion="Registrar Pensión Sobreviviente" titulo="Registrar Pensión Sobreviviente"  id="nuevo" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                                                    Nueva pension por Sobreviviente &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                                </button>
                                            {/if}
                                        {/if}
                                    </th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- filtro -->
<div class="offcanvas">

    <div id="offcanvas-filtro" class="offcanvas-pane width-10">

        <div class="offcanvas-head">
            <header>Filtro Listado</header>
            <div class="offcanvas-tools">
                <a class="cerrar btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form id="filtroListado" action="" class="form" role="form" method="post">
                <input type="hidden" name="filtro" value="1">

                <!--ORGANISMO-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="organismo_filtro" name="form[int][organismo_filtro]" class="form-control input-sm" required data-msg-required="Campo Requerido">
                                <option value="">Seleccione...</option>
                                {foreach item=dat from=$listadoOrganismos}
                                    {if isset($estadoEmpleado.pk_num_organismo)}
                                        {if $dat.pk_num_organismo==$estadoEmpleado.pk_num_organismo}
                                            <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {else}
                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {/if}
                                    {else}
                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="organismo_filtro">Organismo</label>
                        </div>
                    </div>
                </div>
                <!--DEPENDENCIA-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="dependencia_filtro" name="form[int][dependencia_filtro]" class="form-control input-sm" required data-msg-required="Campo Requerido">
                                <option value="">Seleccione...</option>
                                {foreach item=dat from=$listadoDependencias}
                                    {if isset($estadoEmpleado.pk_num_dependencia)}
                                        {if $dat.pk_num_dependencia==$estadoEmpleado.pk_num_dependencia}
                                            <option selected value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                        {else}
                                            <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                        {/if}
                                    {else}
                                        <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="dependencia_filtro">Dependencia</label>
                        </div>
                    </div>
                </div>
                <!--ESTADO DE REGISTTRO-->
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <select id="estatus" name="form[int][estatus]" class="form-control input-sm" required data-msg-required="Campo Requerido">
                                <option value="">Seleccione...</option>
                                {foreach key=key item=item from=$EstadoRegistro}
                                    {if isset($estadoEmpleado.num_estado_registro)}
                                        {if $key==$estadoEmpleado.num_estado_registro}
                                            <option selected value="{$key}">{$item}</option>
                                        {else}
                                            <option value="{$key}">{$item}</option>
                                        {/if}
                                    {else}
                                        <option value="{$key}">{$item}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="estatus">Estado. Registro</label>
                        </div>
                    </div>
                </div>
                <!--BOTON-->
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        var $url_registrar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/RegistrarMET';
        var $url_modificar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ModificarMET';
        var $url_ver='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/VerMET';
        var $url_conformar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConformarMET';
        var $url_aprobar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/AprobarMET';
        var $url_anular='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/AnularMET';

        //NUEVA PENSION
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_registrar,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //MODIFICAR PENSION
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_modificar,{ idPensionSobreviviente: $(this).attr('idPensionSobreviviente')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //VER PENSION
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_ver,{ idPensionSobreviviente: $(this).attr('idPensionSobreviviente')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //CONFORMAR PENSION
        $('#datatable1 tbody').on( 'click', '.conformar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_conformar,{ idPensionSobreviviente: $(this).attr('idPensionSobreviviente')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //APROBAR PENSION
        $('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_aprobar,{ idPensionSobreviviente: $(this).attr('idPensionSobreviviente')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //ANULAR PENSION
        $('#datatable1 tbody').on( 'click', '.anular', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_anular,{ idPensionSobreviviente: $(this).attr('idPensionSobreviviente')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $("#filtroListado").validate({
            submitHandler: function(form) {
                var organismo = $("#organismo_filtro").val();
                var dependencia = $("#dependencia_filtro").val();
                var estatus = $("#estatus").val();

                var url_listar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/BusquedaFiltroListadoMET';
                $.post(url_listar,{ organismo: organismo, dependencia: dependencia, estatus: estatus },function(respuesta_post) {
                    var tabla_listado = $('#datatable1').DataTable();
                    tabla_listado.clear().draw();
                    if(respuesta_post != -1) {
                        for(var i=0; i<respuesta_post.length; i++) {

                            if(respuesta_post[i].txt_estatus=='PR'){
                                var estatus ='Preparado';
                                var botonEditar = '<button type="button" idPensionSobreviviente = "' + respuesta_post[i].pk_num_proceso_pension + '" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"  descripcion="Modificar Pension Sobreviviente" title="Modificar Pension Sobreviviente"  titulo="Modificar Pension Sobreviviente"><i class="fa fa-edit"></i></button>';
                                var botonVer = '<button type="button" idPensionSobreviviente = "' + respuesta_post[i].pk_num_proceso_pension + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"  descripcion="Ver Pension Sobreviviente" title="Ver Pension Sobreviviente"  titulo="Ver Pension Sobreviviente"><i class="fa md-search"></i></button>';
                                var botonAnular = '<button type="button" idPensionSobreviviente = "' + respuesta_post[i].pk_num_proceso_pension + '" class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"  descripcion="Anular Pension Sobreviviente" title="Anular Pension por Invalidez"  titulo="Anular Pension Sobreviviente"><i class="fa md-block"></i></button>';
                            } else if(respuesta_post[i].txt_estatus=='CO') {
                                var estatus ='Conformado';
                                var botonEditar = '';
                                var botonVer = '<button type="button" idPensionSobreviviente = "' + respuesta_post[i].pk_num_proceso_pension + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"  descripcion="Ver Pension Sobreviviente" title="Ver Pensión Sobreviviente"  titulo="Ver Pension Sobreviviente"><i class="fa md-search"></i></button>';
                                var botonAnular = '<button type="button" idPensionSobreviviente = "' + respuesta_post[i].pk_num_proceso_pension + '" class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"  descripcion="Anular Pension Sobreviviente" title="Anular Pensión Sobreviviente"  titulo="Anular Pension Sobreviviente"><i class="fa md-block"></i></button>';

                            } else if(respuesta_post[i].txt_estatus=='AP') {
                                var estatus ='Aprobado';
                                var botonEditar = '';
                                var botonVer = '<button type="button" idPensionSobreviviente = "' + respuesta_post[i].pk_num_proceso_pension + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"  descripcion="Ver Pensión Sobreviviente" title="Ver Pension Sobreviviente"  titulo="Ver Pension Sobreviviente"><i class="fa md-search"></i></button>';
                                var botonAnular = '';

                            } else if(respuesta_post[i].txt_estatus=='AN') {
                                var estatus ='Anulado';
                                var botonEditar = '';
                                var botonVer = '<button type="button" idPensionSobreviviente = "' + respuesta_post[i].pk_num_proceso_pension + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"  descripcion="Ver Pensión Sobreviviente" title="Ver Pension Sobreviviente"  titulo="Ver Pension Sobreviviente"><i class="fa md-search"></i></button>';
                                var botonAnular = '';
                            }
                            tabla_listado.row.add([
                                respuesta_post[i].pk_num_proceso_pension,
                                respuesta_post[i].fk_rhb001_num_empleado,
                                respuesta_post[i].nombre_completo,
                                respuesta_post[i].ind_dependencia,
                                respuesta_post[i].fec_fecha,
                                estatus,
                                    botonEditar,
                                    botonVer,
                                    botonAnular
                            ]).draw()
                                    .nodes()
                                    .to$()
                        }
                        $('#filtroListado').each(function () { {*resetear los campos del formulario*}
                            this.reset();
                        });
                        $(".cerrar").click();
                    }

                },'json');
            }
        });


    });
</script>