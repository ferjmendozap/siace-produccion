<div class="form floating-label">
	<section class="style-default-bright">
		<div class="well clearfix">
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<select id="pk_num_organismo" name="pk_num_organismo" class="form-control input-sm dirty" onchange="cargarDependencia(this.value)">
						{foreach item=org from=$listadoOrganismo}
							{if $org.pk_num_organismo == $emp.pk_num_organismo}
								<option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
							{else}
								<option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
							{/if}
						{/foreach}
					</select>
					<label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
				</div>
				<div id="dependencia">
					<div class="form-group floating-label">
						<select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control input-sm">
							<option value="">&nbsp;</option>
							{foreach item=dep from=$listadoDependencia}
								{if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
									<option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
								{else}
									<option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
								{/if}
							{/foreach}
						</select>
						<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
					</div>
				</div>
				<div id="centroConsto">
					<div class="form-group floating-label">
						<select id="centro_costo" name="centro_costo" class="form-control input-sm dirty">
							<option value="">&nbsp;</option>
							{foreach item=centro from=$centroCosto}
								<option value="{$centro.pk_num_centro_costo}">{$centro.ind_descripcion_centro_costo}</option>
							{/foreach}
						</select>
						<label for="centro_costo"><i class="glyphicon glyphicon-home"></i> Centro de Costo</label>
					</div>
				</div>
				<div id="tipoNomina">
					<div class="form-group floating-label">
						<select id="tipo_nomina" name="tipo_nomina" class="form-control input-sm dirty">
							<option value="">&nbsp;</option>
							{foreach item=nom from=$nomina}
								<option value="{$nom.pk_num_tipo_nomina}">{$nom.ind_nombre_nomina}</option>
							{/foreach}
						</select>
						<label for="tipo_nomina"><i class="glyphicon glyphicon-briefcase"></i> Tipo de Nómina</label>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div id="tipoTrabajador">
					<div class="form-group floating-label">
						<select id="tipo_trabajador" name="tipo_trabajador" class="form-control input-sm dirty">
							<option value="">&nbsp;</option>
							{foreach item=tipo from=$tipoTrabajador}
								<option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
							{/foreach}
						</select>
						<label><i class="glyphicon glyphicon-briefcase"></i> Tipo de Trabajador</label>
					</div>
				</div>
				<div id="edoReg">
					<div class="form-group floating-label">
						<select id="edo_reg" name="edo_reg" class="form-control input-sm dirty">
							<option value="1">ACTIVO</option>
							<option value="0">INACTIVO</option>
						</select>
						<label><i class="glyphicon glyphicon-briefcase"></i> Estado del Registro</label>
					</div>
				</div>
				<div id="sitTrab">
					<div class="form-group floating-label">
						<select id="sit_trab" name="sit_trab" class="form-control input-sm dirty">
							<option value="1">ACTIVO</option>
							<option value="0">INACTIVO</option>
						</select>
						<label><i class="glyphicon glyphicon-briefcase"></i> Situación de Trabajo</label>
					</div>
				</div>
				<div class="form-group" style="width:100%">
					<div class="input-daterange input-group" id="demo-date-range">
						<div class="input-group-content">
							<input type="text" class="fecha form-control input-sm" name="fechaInicio" id="fecha_inicio">
							<label><i class="glyphicon glyphicon-calendar"></i> Fecha de Ingreso</label>
						</div>
						<span class="input-group-addon">a</span>
						<div class="input-group-content">
							<input type="text" class="fecha form-control input-sm" name="fechaFin" id="fecha_fin">
							<div class="form-control-line"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-sm-12" align="center">
			<button type="submit" onclick="buscarTrabajador()" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk" ></span> Buscar</button>
		</div>
</div>
<br/><br/>
<table id="datatable2" class="table table-striped table-hover">
	<thead>
	<tr align="center">
		<th><i class="md-person"></i> Nº</th>
		<th><i class="md-person"></i> Código</th>
		<th><i class="md-person"></i> Nombre Completo</th>
		<th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Documento</th>
		<th> Seleccionar</th>
	</tr>
	</thead>
	<tbody>
	{assign var="numero" value="1"}
	{foreach item=datos from=$datosTrabajador}
		<tr>
			<td>{$numero++}</td>
			<td>{$datos.pk_num_empleado}</td>
			<td>{$datos.ind_nombre1} {$datos.ind_nombre2} {$datos.ind_apellido1} {$datos.ind_apellido2} </td>
			<td>{$datos.ind_cedula_documento}</td>
			<td><button type="button" pk_num_empleado="{$datos.pk_num_empleado}" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button></td>
		</tr>
	{/foreach}
	</tbody>
</table>
</section>
<script type="text/javascript">
	// Inicializador de DataTables
	$(".fecha").datepicker({
		format: 'dd/mm/yyyy',
	});

	if ( $.fn.dataTable.isDataTable( '#datatable2' ) ) {
		table = $('#datatable2').DataTable();
	}
	else {
		table = $('#datatable2').DataTable( {
			paging: true
		} );
	}

	// Buscador
	function buscarTrabajador()
	{
		var pk_num_organismo = $("#pk_num_organismo").val();
		var pk_num_dependencia = $("#pk_num_dependencia").val();
		var centro_costo = $("#centro_costo").val();
		var tipo_nomina = $("#tipo_nomina").val();
		var tipo_trabajador = $("#tipo_trabajador").val();
		var estado_registro = $("#edo_reg").val();
		var situacion_trabajo = $("#sit_trab").val();
		var fecha_inicio = $("#fecha_inicio").val();
		var fecha_fin = $("#fecha_fin").val();

		var url_listar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/BuscarTrabajadorMET';
		$.post(url_listar,{ pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, centro_costo: centro_costo, tipo_nomina: tipo_nomina, tipo_trabajador: tipo_trabajador, estado_registro: estado_registro, situacion_trabajo: situacion_trabajo, fecha_inicio: fecha_inicio, fecha_fin: fecha_fin},function(respuesta_post) {
			var tabla_listado = $('#datatable2').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					var botonCargar = '<button type="button" pk_num_empleado = "' + respuesta_post[i].pk_num_empleado + '" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button>';
					tabla_listado.row.add([
						i+1,
						respuesta_post[i].pk_num_empleado,
						respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
						respuesta_post[i].ind_cedula_documento,
						botonCargar
					]).draw()
							.nodes()
							.to$()
				}
			}

		},'json');
	}

	function calcular_edad(fecha)
	{
		//calculo la fecha de hoy
		hoy=new Date()
		//alert(hoy)
		//calculo la fecha que recibo
		//La descompongo en un array
		var array_fecha = fecha.split("-")
		//si el array no tiene tres partes, la fecha es incorrecta
		if (array_fecha.length!=3)
			return false
		//compruebo que los anio, mes, dia son correctos
		var anio
		anio = parseInt(array_fecha[2]);
		if (isNaN(anio))
			return false
		var mes
		mes = parseInt(array_fecha[1]);
		if (isNaN(mes))return false
		var dia
		dia = parseInt(array_fecha[0]);
		if (isNaN(dia))
			return false
		//resto los años de las dos fechas
		 var edad=hoy.getFullYear()- anio - 1; //-1 porque no se si ha cumplido años ya este año

		//si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido
		if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
			return edad
		if (hoy.getMonth() + 1 - mes > 0)
			return edad+1
		//entonces es que eran iguales. miro los dias
		//si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido
		if (hoy.getDate() - dia >= 0)
			return edad + 1
		return edad
	}

	// Seleccion de empleados
	$('#datatable2 tbody').on( 'click', '.cargar', function (respuesta_post) {
		// **********************************************PASO 1*************************************************
		var $urlCargar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ObtenerDatosEmpleadoMET';
		$.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato){
			var $nombre = dato['nombreTrabajador'];
			var edad = calcular_edad(dato['fec_nacimiento']);
			$("#fk_rhb001_num_empleado").val(dato['id_empleado']);
			$("#ind_organismo").val(dato['organismo']);
			$("#fk_a001_num_organismo").val(dato['fk_a001_num_organismo']);
			$("#ind_dependencia").val(dato['ind_dependencia']);
			$("#fk_a004_num_dependencia").val(dato['fk_a004_num_dependencia']);
			$("#ind_cargo").val(dato['cargo']);
			$("#fk_rhc063_num_puestos").val(dato['fk_rhc063_num_puestos_cargo']);
			$("#nombreTrabajador").val($nombre);
			$("#pkNumEmpleado").val(dato['pk_num_empleado']);
			$("#cedula").val(dato['ind_cedula_documento']);
			$("#ind_sexo").val(dato['sexo']);
			$("#sexo").val(dato['fk_a006_num_miscelaneo_detalle_sexo']);
			$("#nacimiento").val(dato['fec_nacimiento']);
			$("#num_edad").val(edad);
			$("#fec_fecha_ingreso").val(dato['fec_ingreso']);
			$("#num_ultimo_sueldo").val(dato['num_sueldo_basico']);
			$("#num_anio_servicio").val(dato['anio_serv']);
			$("#fk_nmb001_num_tipo_nomina").val(dato['nomina']);
			$("#fk_a006_num_miscelaneo_detalle_tipotrab").val(dato['tipo_trabajador']);
			$("#fk_a006_num_miscelaneo_detalle_motivopension").val(dato['fk_a006_num_miscelaneo_detalle_motivopension']);
			$("#fk_a006_num_miscelaneo_detalle_tipopension").val(dato['fk_a006_num_miscelaneo_detalle_tipopension']);
			$("#ind_detalle_tipopension").val(dato['tipo_pension']);
			$("#ind_detalle_motivopension").val(dato['motivo_pension']);

			{*verifico si el empleado cumple con los requisitos para optar por la pension*}
			/*var $url_verificar = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/VerificarRequisitosMET';
			$.post($url_verificar, { idEmpleado: $('#fk_rhb001_num_empleado').val(), tipo:'REQUISITOS', fecha_nac: dato['fec_nacimiento'] , fecha_ing: dato['fec_ingreso'] }, function(resultado){

				$requisito = resultado['requisito'];
				if($requisito==1){
					$('#requisitoVer').html('<div class="alert alert-success"><strong><i class="glyphicon glyphicon-ok"></i></strong> El empleado cumple con los requisitos para optar a la Pensión.</div>');
					$('#botonGuardar').show();
					$("#num_monto_pension").removeAttr('disabled');
				} else {
					$('#botonGuardar').hide();
					$('#requisitoVer').html('<div class="alert alert-danger"><strong><i class="glyphicon glyphicon-alert"></i></strong> El Empleado NO cumple con los requisitos para optar a la Pensión.</div>');
				}


			},'json');*/

			// **********************************************PASO 2*************************************************
			var $antecedentes='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ObtenerAntecedentesMET';
			$.post($antecedentes,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val() },function(datoAntecedentes){
				var tabla_listado = $('#datatable3').DataTable();
				tabla_listado.clear().draw();
				if(datoAntecedentes != -1) {
					for(var i=0; i<datoAntecedentes.length; i++) {
						tabla_listado.row.add([
							i+1,
							datoAntecedentes[i].empresa,
							datoAntecedentes[i].fechaIngreso,
							datoAntecedentes[i].fechaEgreso,
							datoAntecedentes[i].anio,
							datoAntecedentes[i].mes,
							datoAntecedentes[i].dia
						]).draw()
								.nodes()
								.to$()
					}
				}
			},'json');

			// **********************************************PASO 3*************************************************
			// Antecedentes totales
			var $totalServicio='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/CalcularAntecedentesTotalMET';
			var tabla_listado = $('#datatable3').DataTable();
			tabla_listado.clear().draw();
			$.post($totalServicio,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val()},function(servicioTotal){

				$("#antAnio").html(servicioTotal['anio']);
				$("#antMes").html(servicioTotal['mes']);
				$("#antDia").html(servicioTotal['dia']);

				$("#orgAnio").html(servicioTotal['anioInst']);
				$("#orgMes").html(servicioTotal['mesInst']);
				$("#orgDia").html(servicioTotal['diaInst']);

				$("#tAnio").html(servicioTotal['anioTotal']);
				$("#tMes").html(servicioTotal['mesTotal']);
				$("#tDia").html(servicioTotal['diaTotal']);

			},'json');

			var $urlSueldoBase='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConsultarRelacionSueldosMET';
			$.post($urlSueldoBase,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val()},function(datoSueldo){
				var tablaSueldoBase = $('#datatable4').DataTable();
				tablaSueldoBase.clear().draw();
				var sumaBase = 0;
				if(datoSueldo != -1) {
					for(var i=0; i<datoSueldo.length; i++) {
						var numBase = parseFloat(datoSueldo[i].num_sueldo_base);
						sumaBase = sumaBase + numBase;
						tablaSueldoBase.row.add([
							i+1,
							datoSueldo[i].fec_anio+'-'+datoSueldo[i].fec_mes,
							numBase.toFixed(2)
						]).draw()
								.nodes()
								.to$()
					}
				}
				$("#sueldo").val(sumaBase);
			},'json');

			var hoy = new Date();
			var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();

			if($("#proceso").val()=='Nuevo'){
				$("#fk_nmb001_num_tipo_nomina").attr('disabled','disabled');
				$("#fk_a006_num_miscelaneo_detalle_tipotrab").attr('disabled','disabled');
				$("#ind_resolucion").attr('disabled','disabled');
				$("#num_monto_pension").attr('disabled','disabled');
				$("#fec_fecha_egreso").attr('disabled','disabled');
				$("#fk_rhc032_num_motivo_cese").attr('disabled','disabled');
				$("#txt_observacion_egreso").attr('disabled','disabled');
				$("#txt_observacion_pre").removeAttr('disabled');
				$("#fecha_preparado").val(fecha);
				$("#preparado_por").val('{$nombreUsuario}');
				$("#usuario_preparado_por").val({$idUsuario});
			}



		},'json');



		/*var $urlServicio='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/CalcularServicioMET';
		$.post($urlServicio,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(datoServicio){
			$requisito = datoServicio['requisito'];
			if($requisito==1){
				$('#requisitoVer').html('<div class="alert alert-success"><strong><i class="glyphicon glyphicon-ok"></i></strong> El empleado cumple con los requisitos para optar a la Jubilación.</div>');
				$('#botonGuardar').html('<button type="submit" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Procesar </button>');
			} else {
				$('#requisitoVer').html('<div class="alert alert-danger"><strong><i class="glyphicon glyphicon-alert"></i></strong> El Empleado NO cumple con los requisitos para optar a la Jubilación.</div>');
			}
		},'json');
		// **********************************************PASO 2*************************************************
		var $servicio='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/GenerarServicioMET';
		$.post($servicio,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(datoServicio){
			var tabla_listado = $('#datatable3').DataTable();
			tabla_listado.clear().draw();
			if(datoServicio != -1) {
				for(var i=0; i<datoServicio.length; i++) {
					tabla_listado.row.add([
						i+1,
						datoServicio[i].empresa,
						datoServicio[i].fechaIngreso,
						datoServicio[i].fechaEgreso,
						datoServicio[i].anio,
						datoServicio[i].mes,
						datoServicio[i].dia
					]).draw()
							.nodes()
							.to$()
				}
			}
		},'json');
		// Antecedentes totales
		var $totalServicio='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/CalcularServicioTotalMET';
		var tabla_listado = $('#datatable3').DataTable();
		tabla_listado.clear().draw();
		$.post($totalServicio,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function(servicioTotal){
			$(document.getElementById('datatable3')).append('<tr><td></td><td></td><td></td><td><b>Antecedente</b></td><td>'+servicioTotal['anio']+'</td><td>'+servicioTotal['mes']+'</td><td>'+servicioTotal['dia']+'</td></tr>');
		},'json');*/
		$(document.getElementById('cerrarModal2')).click();
		$(document.getElementById('ContenidoModal2')).html('');
	});
</script>