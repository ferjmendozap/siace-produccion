<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado Beneficios de Utiles Escolares</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 6%;">Orden</th>
                                    <th class="col-sm-2">Periodo</th>
                                    <th class="col-sm-3">Funcionario</th>
                                    <th style="width: 6%;">Monto</th>
                                    <th class="col-sm-2">Tipo</th>
                                    <th style="width: 5%;">Estado</th>
                                    <th style="width: 4%;">Info</th>
                                    <th style="width: 4%">Mod</th>
                                    <th style="width: 4%">Ver</th>
                                    <th style="width: 7%">Accion</th>
                                    <th style="width: 8%">Generar</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$listadoBeneficio}

                                    <tr id="idBeneficio{$dat.pk_num_utiles_beneficio}" style="font-size: 11px">
                                        <td>{$dat.num_orden}</td>
                                        <td>{$dat.ind_descripcion_beneficio}</td>
                                        <td>{$dat.nombre_empleado}</td>
                                        <td>{$dat.num_monto|number_format:2:",":"."}</td>
                                        <td>
                                            {$dat.ind_nombre_detalle}
                                        </td>
                                        <td>
                                            {if $dat.ind_estado=='PR'}
                                                Preparado
                                                {elseif $dat.ind_estado=='RV'}
                                                    Revisado
                                                {elseif $dat.ind_estado=='AP'}
                                                    Aprobado
                                                {elseif $dat.ind_estado=='PA'}
                                                    Por Asignar Utiles
                                                {elseif $dat.ind_estado=='AS'}
                                                    Utiles Asignados
                                                {else}
                                                    Anulado
                                            {/if}

                                        </td>
                                        <td>
                                            {if $dat.cod_detalle=='AD'}
                                            <button class="infoReg logsUsuario btn ink-reaction btn-raised btn-xs btn- btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idBeneficio="{$dat.pk_num_utiles_beneficio}"
                                                    descipcion="El Usuario a consultado operaciones" titulo="Información de Registro">
                                                <i class="md md-people" style="color: #ffffff;"></i>
                                            </button>
                                            {/if}
                                        </td>
                                        <td>
                                            {if in_array('RH-01-02-05-02-02-M',$_Parametros.perfil)}
                                                {if $dat.ind_estado=='PR' || $dat.ind_estado=='PA'}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  idBeneficio="{$dat.pk_num_utiles_beneficio}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                            descipcion="Modificar Beneficio Utiles" title="Editar Beneficio Utiles" titulo="Modificar Beneficio Utiles">
                                                        <i class="fa md-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {/if}
                                        </td>
                                        <td>
                                            {if in_array('RH-01-02-05-02-04-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idbeneficio="{$dat.pk_num_utiles_beneficio}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="Ver Datos Beneficio Utiles" title="Ver Datos Beneficio Utiles" titulo="Ver Datos Beneficio Utiles">
                                                <i class="fa md-search" style="color: #ffffff;"></i>
                                            </button>
                                            {/if}
                                        </td>
                                        <td>
                                            {if $dat.ind_estado=='PR' && $dat.cod_detalle=='AD'}
                                                {if in_array('RH-01-02-05-02-05-RV',$_Parametros.perfil)}
                                                <button class="revisar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idBeneficio="{$dat.pk_num_utiles_beneficio}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descripcion="Revisar Beneficio de Utiles" title="Revisar Beneficio Utiles" titulo="Revisar Beneficio de Utiles">
                                                    <i class="md md-done"></i> REVISAR
                                                </button>
                                                {/if}
                                            {elseif $dat.ind_estado=='RV' && $dat.cod_detalle=='AD'}
                                                {if in_array('RH-01-02-05-02-07-AP',$_Parametros.perfil)}
                                                <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idBeneficio="{$dat.pk_num_utiles_beneficio}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descipcion="Aprobar Beneficio de Utiles" title="Aprobar Benenficio Utiles" titulo="Aprobar Beneficio de Utiles">
                                                    <i class="md md-done-all"></i> APROBAR
                                                </button>
                                                {/if}
                                            {elseif $dat.ind_estado=='AP' && $dat.cod_detalle=='AD'}
                                                <button class="imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idBeneficio="{$dat.pk_num_utiles_beneficio}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descipcion="Imprimir Beneficio de Utiles" title="Imprimir"  titulo="Imprimir Beneficio de Utiles">
                                                    <i class="md md-print"></i> Imprimir
                                                </button>
                                            {else}

                                            {/if}
                                        </td>
                                        <td>
                                            {if in_array('RH-01-02-05-02-03-G',$_Parametros.perfil)}
                                                {if $dat.cod_detalle=='AF'}
                                                <button class="generar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idBeneficio="{$dat.pk_num_utiles_beneficio}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descripcion="Generar Orden" titulo="Orden Beneficio Utiles" id="GenerarOrden">
                                                    <i class="md md-print"></i> orden
                                                </button>
                                                {/if}
                                            {/if}
                                        </td>
                                    </tr>


                                {/foreach}

                                </tbody>

                                <tfoot>
                                    <th colspan="11">
                                        {if in_array('RH-01-02-05-01-N',$_Parametros.perfil)}
                                            <button class="nuevoReq logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descripcion="el Usuario va a registrar nuevos requisitos para utiles" titulo="Registrar Nuevo Requisitos"  id="nuevoReq"
                                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" >
                                                Ingresar Requisitos &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                            </button>
                                        {/if}
                                        {if in_array('RH-01-02-05-02-01-N',$_Parametros.perfil)}
                                            <button class="nuevoBen logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descripcion="el Usuario va a registrar un nuevo beneficio de utiles"  id="nuevoBen" titulo="Registrar Beneficio Utiles"
                                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" >
                                                Nuevo Beneficio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url_requisito='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevoRequisitoMET';
        var $url_beneficio='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevoBeneficioMET';
        var $url_ver      ='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/VerMET';
        var $url_modificar='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ModificarMET';
        var $url_revisar  ='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/RevisarMET';
        var $url_conformar='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ConformarMET';
        var $url_aprobar  ='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/AprobarMET';
        var $url_info_reg ='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/InformacionRegistroMET';

        //NUEVO REQUISITO
        $('#nuevoReq').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_requisito,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //NUEVO BENEFICIO
        $('#nuevoBen').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_beneficio,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //MODIFICAR BENEFICIO
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_modificar, { idBeneficio: $(this).attr('idBeneficio')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //VER BENEFICIO
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_ver, { idBeneficio: $(this).attr('idBeneficio')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //REVISAR BENEFICIO
        $('#datatable1 tbody').on( 'click', '.revisar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_revisar, { idBeneficio: $(this).attr('idBeneficio')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //CONFORMAR BENEFICIO
        $('#datatable1 tbody').on( 'click', '.conformar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_conformar, { idBeneficio: $(this).attr('idBeneficio')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //APROBAR BENEFICIO
        $('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_aprobar, { idBeneficio: $(this).attr('idBeneficio')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //INFORMACION DE REGISTRO
        $('#datatable1 tbody').on( 'click', '.infoReg', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_info_reg, { idBeneficio: $(this).attr('idBeneficio')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //GENERAR ORDEN DEL BENEFICIO
        $('#datatable1 tbody').on( 'click', '.generar', function () {
            var $url_reporte='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/OrdenBeneficioUtilesMET/'+$(this).attr('idBeneficio')+'';
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html('<iframe src="'+$url_reporte+'" width="100%" height="950"></iframe>');
        });

        //IMPRIMIR BENEFICIO (ASIGNACION DIRECTA)
        $('#datatable1 tbody').on( 'click', '.imprimir', function () {
            var $url_reporte='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ImprimirBeneficioUtilesMET/'+$(this).attr('idBeneficio')+'';
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html('<iframe src="'+$url_reporte+'" width="100%" height="950"></iframe>');
        });

        /*$('#GenerarOrden').click(function(){

        });*/


    });
</script>