<div class="modal-body">

        <div class="row">

                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >


                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idBeneficio}" name="idBeneficio"/>
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <div class="row contain-lg">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="num_orden" name="form[txt][num_orden]" value="{if isset($formDB.num_orden)}{$formDB.num_orden}{else}{$orden}{/if}" disabled>
                                <label for="num_orden">Nº Orden</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select id="fk_a006_num_miscelaneo_detalle_asignacion" name="form[int][fk_a006_num_miscelaneo_detalle_asignacion]" class="form-control input-sm" required>
                                    <option value="">Seleccione...</option>
                                    {foreach item=dat from=$tipoAsignacion}
                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_asignacion)}
                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_asignacion}
                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_a006_num_miscelaneo_detalle_asignacion">Tipo Asignación</label>
                            </div>
                            <input type="hidden" id="cod_detalle" value="{if isset($formDB.cod_detalle)}{$formDB.cod_detalle}{/if}">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select id="fk_rhc056_num_beneficio_utiles" name="form[int][fk_rhc056_num_beneficio_utiles]" class="form-control input-sm" required>
                                    <option value="">Seleccione...</option>
                                    {foreach item=dat from=$periodos}
                                        {if isset($formDB.fk_rhc056_num_beneficio_utiles)}
                                            {if $dat.pk_num_beneficio_utiles==$formDB.fk_rhc056_num_beneficio_utiles}
                                                <option selected value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                            {else}
                                                <option value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_rhc056_num_beneficio_utiles">Periodo</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="nombreTrabajador" value="{if isset($formDB.nombre_empleado)}{$formDB.nombre_empleado}{/if}" disabled>
                                        <input type="hidden" name="form[int][fk_rhb001_num_empleado]" id="fk_rhb001_num_empleado" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{/if}"  disabled>
                                        <label for="nombreTrabajador">Empleado</label>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado" disabled>Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="lg_b022_num_proveedor" name="form[int][lg_b022_num_proveedor]" class="form-control input-xs select2-list select2" disabled>
                                    <option value="">&nbsp;</option>
                                    {foreach item=dat from=$proveedores}
                                        {if isset($formDB.lg_b022_num_proveedor)}
                                            {if $dat.pk_num_proveedor == $formDB.lg_b022_num_proveedor}
                                                <option selected value="{$dat.pk_num_proveedor}">{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}</option>
                                            {else}
                                                <option value="{$dat.pk_num_proveedor}">{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_proveedor}">{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}</option>
                                        {/if}

                                    {/foreach}
                                </select>
                                <label for="lg_b022_num_proveedor">Proveedor</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-6">
                        <div class="form-group">
                            <select id="hijos" name="hijos" class="form-control input-sm">
                                <option value="">Seleccione...</option>
                            </select>
                            <label for="hijos">Hijos</label>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm bolivar-mask" id="monto" value="">
                                        <input type="hidden" class="form-control input-sm" id="monto_t" name="form[int][monto_t]" value="{if isset($formDB.num_monto)}{$formDB.num_monto|number_format:2:",":"."}{/if}">
                                        <label for="monto">Monto</label>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" id="AgregarBeneficiario">Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- estado preparado -->
                    {if $formDB.ind_estado=='PR'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_rev">Observaciones Revisado</label>
                                </div>
                            </div>
                        </div>
                    {/if}

                    <!-- estado revisado -->
                    {if $formDB.ind_estado=='RV'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_rev">Observaciones Revisado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apr" name="form[txt][txt_observacion_apr]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_apr">Observaciones Aprobado</label>
                                </div>
                            </div>
                        </div>
                    {/if}

                    <!-- estado aprobado -->
                    {if $formDB.ind_estado=='AP'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_rev">Observaciones Revisado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apr" name="form[txt][txt_observacion_apr]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_apr">Observaciones Aprobado</label>
                                </div>
                            </div>
                        </div>
                    {/if}

                    <!-- estado anulado -->
                    {if $formDB.ind_estado=='AN'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_anu" name="form[txt][txt_observacion_anu]" value="{if isset($formDBoperacionesAN[0].txt_observaciones)}{$formDBoperacionesAN[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_anu">Observaciones Anulado</label>
                                </div>
                            </div>
                        </div>
                    {/if}
                    <br>
                    <div class="row contain-lg">
                        <input type="hidden" name="form[txt][d_requisitos]" id="d_requisitos" value="" placeholder="requisitos">
                        <input type="hidden" name="form[txt][t_requisitos]" id="t_requisitos" value="" placeholder="tipo requisito">
                        <input type="hidden" name="form[txt][d_beneficiarios]" id="d_beneficiarios" value="" placeholder="beneficiarios">
                        <input type="hidden" name="form[txt][carga]" id="carga" value="{if isset($beneficiarios)}{$beneficiarios}{/if}" placeholder="beneficiarios">
                        <input type="hidden" name="form[txt][monto_a]" id="monto_a" value="{if isset($montos)}{$montos}{/if}" placeholder="monto asignado">
                        <table id="listaBeneficarios" class="table table-striped table-hover">
                            <thead>
                            <tr align="center">
                                <th class="col-sm-1"><i class="md-person"></i> Id</th>
                                <th class="col-sm-4"><i class="md-person"></i> Nombres y apellidos</th>
                                <th class="col-sm-2"><i class="md-person"></i> Monto</th>

                            </tr>
                            </thead>
                            <tbody>
                                {foreach item=dat from=$formDBbeneficiarios}
                                    <tr>
                                        <td>{$dat.fk_c015_num_carga_familiar}</td>
                                        <td>{$dat.nombre_hijo}</td>
                                        <td>{$dat.num_monto_asignado|number_format:2:",":"."}</td>

                                    </tr>

                                {/foreach}
                            </tbody>
                        </table>
                    </div>

                    <div class="row contain-lg">
                        <br>
                        <div align="right">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionBotonBeneficio">Guardar </button>
                            {if in_array('RH-01-02-05-02-08-AN',$_Parametros.perfil)}
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionAnularBeneficio">Anular </button>
                            {/if}
                        </div>
                        <br>
                    </div>
                </form>

        </div>

</div>




<style>
    .letra_pequena{
        font-size: 11px;
    }

</style>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();


            /*CUANDO ES REVISADO*/
            if($("#proceso").val()=='RevisarBeneficio'){
                var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/RevisarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",dato['detalleERROR'], "error");
                    }
                    if(dato['status']=='revisado'){


                        if(dato['cod_detalle']=='AD'){
                            var btn_aprobar = '<button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="'+dato['idBeneficio']+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Aprobar Beneficio de Utiles" titulo="Aprobar Beneficio de Utiles">APROBAR</button>';
                            var btn_orden = '';
                        }else{
                            var btn_aprobar = '';
                            var btn_orden = '<button class="generar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="'+dato['idBeneficio']+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Generar Orden" titulo="Generar Orden">ORDEN</button>';
                        }

                        var tabla = $('#datatable1').DataTable();

                        tabla.row('#idBeneficio'+dato['idBeneficio']+'').remove().draw();

                        var fila = tabla.row.add([
                            dato['num_orden'],
                            dato['ind_descripcion_beneficio'],
                            dato['nombre_empleado'],
                            dato['num_monto'],
                            dato['ind_nombre_detalle'],
                            'Revisado',
                            '<button class="infoReg logsUsuario btn ink-reaction btn-raised btn-xs btn- btn-danger" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idBeneficio="'+dato['idBeneficio']+'" descripcion="Consultar operaciones" titulo="Información de Registro"><i class="md md-people" style="color: #ffffff;"></i></button>',
                            '',
                            '{if in_array('RH-01-02-05-02-04-V',$_Parametros.perfil)}<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idbeneficio="'+dato['idBeneficio']+'" data-toggle="modal"  data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Ver Datos Beneficio Utiles" titulo="Ver Datos Beneficio Utiles"><i class="md md-search" style="color: #ffffff;"></i></button>{/if}',
                            '{if in_array('RH-01-02-05-02-05-RV',$_Parametros.perfil)}'+btn_aprobar+'{/if}',
                            '{if in_array('RH-01-02-05-02-03-G',$_Parametros.perfil)}'+btn_orden+'{/if}'

                        ]).draw()
                                .nodes()
                                .to$()
                                .addClass();

                        $(fila)
                                .attr('id','idBeneficio'+dato['idBeneficio']+'')
                                .css('font-size','11px')

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        });

                    }
                },'json');
            }



            /*CUANDO ES APROBADO*/
            if($("#proceso").val()=='AprobarBeneficio'){
                var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/AprobarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",dato['detalleERROR'], "error");
                    }
                    if(dato['status']=='aprobado'){


                        if(dato['cod_detalle']=='AD'){
                            var btn_imprimir = '<button class="imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="'+dato['idBeneficio']+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Imprimir Beneficio de Utiles" titulo="Imprimir Beneficio de Utiles"><i class="md md-print"></i> Imprimir</button>';
                            var btn_orden = '';
                        }else{
                            var btn_imprimir = '';
                            var btn_orden = '<button class="generar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="'+dato['idBeneficio']+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Generar Orden" titulo="Orden Beneficio Utiles"><i class="md md-print"></i> ORDEN</button>';
                        }

                        var tabla = $('#datatable1').DataTable();

                        tabla.row('#idBeneficio'+dato['idBeneficio']+'').remove().draw();

                        var fila = tabla.row.add([
                            dato['num_orden'],
                            dato['ind_descripcion_beneficio'],
                            dato['nombre_empleado'],
                            dato['num_monto'],
                            dato['ind_nombre_detalle'],
                            'Aprobado',
                            '<button class="infoReg logsUsuario btn ink-reaction btn-raised btn-xs btn- btn-danger" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idBeneficio="'+dato['idBeneficio']+'" descripcion="Consultar operaciones" titulo="Información de Registro">' +
                            '<i class="md md-people" style="color: #ffffff;"></i></button>',
                            '',
                            '{if in_array('RH-01-02-05-02-04-V',$_Parametros.perfil)}<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idBeneficio="'+dato['idBeneficio']+'"' +
                            'data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Ver Datos Beneficio Utiles" titulo="Ver Datos Beneficio Utiles">' +
                            '<i class="md md-search" style="color: #ffffff;"></i></button>{/if}',
                            '{if in_array('RH-01-02-05-02-05-RV',$_Parametros.perfil)}'+btn_imprimir+'{/if}',
                            '{if in_array('RH-01-02-05-02-03-G',$_Parametros.perfil)}'+btn_orden+'{/if}'

                        ]).draw()
                                .nodes()
                                .to$()
                                .addClass();

                        $(fila)
                                .attr('id','idBeneficio'+dato['idBeneficio']+'')
                                .css('font-size','11px')

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        });

                    }
                },'json');
            }


        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la ventana
        $('#modalAncho').css( "width", "65%" );

        if($("#proceso").val()=='RevisarBeneficio'){
            $(".form-control").attr("disabled","disabled");
            $("#buscarEmpleado").attr("disabled","disabled");
            $("#txt_observacion_rev").removeAttr('disabled');
            $("#accionBotonBeneficio").html('Revisar');
        }

        if($("#proceso").val()=='ConformarBeneficio'){
            $(".form-control").attr("disabled","disabled");
            $("#buscarEmpleado").attr("disabled","disabled");
            $("#txt_observacion_con").removeAttr('disabled');
            $("#accionBotonBeneficio").html('Conformar');
        }

        if($("#proceso").val()=='AprobarBeneficio'){
            $(".form-control").attr("disabled","disabled");
            $("#buscarEmpleado").attr("disabled","disabled");
            $("#txt_observacion_apr").removeAttr('disabled');
            $("#accionBotonBeneficio").html('Aprobar');
        }

        //consulto los hijos disponibles para el beneficio
        var $carga='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ConsultarCargaFamiliarHijosMET';
        $.post($carga,{ pk_num_empleado: $("#fk_rhb001_num_empleado").val() },function(datos){
            var json = datos,
                    obj = JSON.parse(json);
            $("#hijos").html(obj);
        });

        if( $("#cod_detalle").val() == 'AF' ){
            $("#lg_b022_num_proveedor").removeAttr('disabled');
        }else{
            $("#lg_b022_num_proveedor").attr('disabled','disabled');
        }


        if ( $.fn.dataTable.isDataTable( '#listaBeneficarios' ) ) {
            table = $('#listaBeneficarios').DataTable();
        }
        else {
            table = $('#listaBeneficarios').DataTable( {
                paging: false,
                searching: false
            } );
        }

        /*boton agregar beneficiario*/
        $("#AgregarBeneficiario").click(function(){

            var aux = $("#hijos").val();
            var monto = $("#monto").val();
            var monto_t = $("#monto_t").val();

            monto = monto.replace(/[^,\d]/g, '');
            monto = monto.replace("BS", "");
            monto_t = monto_t.replace(/[^,\d]/g, '');


            var hijos = aux.split("-");
            var hijos_id = hijos[0];
            var hijos_nom = hijos[1];

            if(aux==''){
                swal("Advertencia!","Debe Seleccionar Hijos.", "warning");
                return false;
            }
            if(monto==''){
                swal("Advertencia!","Debe Introducir Monto.", "warning");
                return false;
            }

            var info = '<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>';

            var carga_familiar = $("#carga").val();
            var monto_asignado = $("#monto_a").val();

            carga_familiar = carga_familiar + hijos_id +'#';
            monto_asignado = monto_asignado + monto +'#';


            var num_filas = $('#listaBeneficarios >tbody >tr').length;
            var filas = $('#listaBeneficarios >tbody >tr'); {*trae las filas del tbody*}
            var band = 0;
            for(var i=0; i<num_filas; i++){
                if(filas[i].cells[0].innerHTML == hijos_id  ){

                    swal({  {*Mensaje personalizado*}
                        title: 'Registro Repetido',
                        text: 'Ya está agregado',
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Aceptar',
                        closeOnConfirm: true
                    });

                    band = 1;
                    break;
                }
            }

            if(band==0){

                var tabla = $('#listaBeneficarios').DataTable();
                //tabla.clear().draw();

                var fila = tabla.row.add([
                    hijos_id,
                    hijos_nom,
                    monto,
                    info
                ]).draw()
                        .nodes()
                        .to$()
                        .addClass();

                $("#carga").val(carga_familiar);
                $("#monto_a").val(monto_asignado);
                $("#hijos").val('');
                $("#monto").val('0,00');
                var monto_t = parseFloat(monto_t) + parseFloat(monto);

            }

            //monto_t = monto_t + monto;

            $("#monto_t").val(monto_t);



        });

        var tabla = $('#listaBeneficarios').DataTable();


        /*ELIMINAR HIJO TEMPORAL*/

        {* ************************************************ *}
        $('#listaBeneficarios tbody').on( 'click', '.eliminar', function () {

            var obj = this;

            swal({
                title:'Estas Seguro?',
                text: 'Estas seguro que desea descartar al beneficiario(a)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function(){

                tabla.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
                swal("Descartado!", "Descartado de la lista de beneficiarios", "success");


                {*verifico la tablas donde estan los empleados para volver armar el arreglo de empleados*}
                var num_filas = $('#listaBeneficarios >tbody >tr').length;
                var filas = $('#listaBeneficarios >tbody >tr'); {*trae las filas del tbody*}


                var cad_id_beneficiarios = '';
                var monto_asignado = '';
                var monto = 0;
                $('#carga').val('');
                $('#monto_a').val('');
                $('#monto_t').val('0,00');

                for(var i=0; i<num_filas; i++) {

                    {*voy armando arreglo de beneficiarios*}

                    cad_id_beneficiarios = cad_id_beneficiarios + filas[i].cells[0].innerHTML + '#';

                    monto_asignado = monto_asignado + filas[i].cells[2].innerHTML + '#';

                    monto = parseFloat(monto) + parseFloat(filas[i].cells[2].innerHTML);

                }
                {*asigno arreglo de empleados*}
                $('#carga').val(cad_id_beneficiarios);
                $('#monto_t').val(monto);
                $('#monto_a').val(monto_asignado);


            });
        });
        {* **************************************************** *}


        /*ELIMINAR HIJO DE LA BASE DE DATOS*/

        {* ************************************************ *}
        $('#listaBeneficarios tbody').on( 'click', '.eliminarHijo', function () {

            var obj = this;

            var idCargaFamiliar = $(this).attr('idCargaFamiliar');
            var idBeneficio = $(this).attr('idBeneficio');

            swal({
                title:'Estas Seguro?',
                text: 'Estas seguro que desea descartar al beneficiario(a)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function(){

                var $url='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/EliminarBeneficarioMET';
                $.post($url, { idCargaFamiliar: idCargaFamiliar , idBeneficio: idBeneficio  },function($dato){
                    if($dato['status']=='OK'){
                        //$(document.getElementById('idCargaFamiliar'+$dato['idCargaFamiliar'])).html('');
                        tabla.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
                        swal("Descartado!", "Descartado de la lista de beneficiarios", "success");

                        {*verifico la tablas donde estan los empleados para volver armar el arreglo de empleados*}
                        var num_filas = $('#listaBeneficarios >tbody >tr').length;
                        var filas = $('#listaBeneficarios >tbody >tr'); {*trae las filas del tbody*}


                        var cad_id_beneficiarios = '';
                        var monto_asignado = '';
                        var monto = 0;
                        $('#carga').val('');
                        $('#monto_a').val('');
                        $('#monto_t').val('0,00');

                        for(var i=0; i<num_filas; i++) {

                            {*voy armando arreglo de beneficiarios*}

                            cad_id_beneficiarios = cad_id_beneficiarios + filas[i].cells[0].innerHTML + '#';

                            monto_asignado = monto_asignado + filas[i].cells[2].innerHTML + '#';

                            monto = monto + parseFloat(filas[i].cells[2].innerHTML);

                        }
                        {*asigno arreglo de empleados*}
                        $('#carga').val(cad_id_beneficiarios);
                        $('#monto_t').val(monto);
                        $('#monto_a').val(monto_asignado);


                        $('#cerrar').click();


                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');


            });
        });
        {* **************************************************** *}


        /*al selecionar tipo de asigancion*/
        $("#fk_a006_num_miscelaneo_detalle_asignacion").change(function(){

            var aux  = $(this).val();
            var tipo = aux.split("-");

            if(tipo[1]=='AF'){
                $("#lg_b022_num_proveedor").removeAttr('disabled');
            }else{
                $("#lg_b022_num_proveedor").attr('disabled','disabled');
            }

        });




        //al dar click en el boton requisitos del listado de hijos de la carga familiar
        var $url_req = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/IngresarRequisitosMET';
        $('#cargaFamiliarHijos tbody').on( 'click', '.requisitosBeneficiario', function () {
            $('#formModalLabel3').html('Registrar Requisitos: '+$(this).attr('tituloReq'));
            $('#carga').val($(this).attr('pk_num_carga'));

            $.post($url_req,{ idEmpleado: $(this).attr('idEmpleado'), idCargaFamiliar: $(this).attr('pk_num_carga') },function(dato){
                $('#ContenidoModal3').html(dato);

                var data_b = $("#d_requisitos").val();
                var data_r = $("#t_requisitos").val();

                var arr_b  = data_b.split("#");
                var arr_r  = data_r.split("#");

                for (i=0; i<=arr_b.length; i++){
                    if(arr_b[i] == $('#carga').val()){
                        $("#fk_a006_num_miscelaneo_detalle_requisito").val(arr_r[i]);
                        $("#num_flag_entregado").prop("checked", true);
                    }
                }
            });
        });




    });

</script>