<div class="modal-body">

        <div class="row">
            <!-- WIZARD CESE - REINGRESO-->
            <div id="rootwizard2" class="form-wizard form-wizard-horizontal">


                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                <!-- PESTAÑAS WIZAARD -->
                <div class="form-wizard-nav">
                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                    <ul class="nav nav-justified">
                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">Información General</span></a></li>
                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Cargar Justificativo</span></a></li>
                    </ul>
                </div>




                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idRequisito}" name="idRequisito"/>
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <!-- CONTENIDO -->
                    <div class="tab-content clearfix">

                        <!-- INFORMACIÓN GENERAL -->
                        <div class="tab-pane active" id="step1">

                            <br>
                            <div class="row contain-lg">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select id="fk_rhc056_num_beneficio_utiles" name="form[int][fk_rhc056_num_beneficio_utiles]" class="form-control input-sm" required>
                                            <option value="">Seleccione...</option>
                                            {foreach item=dat from=$periodos}
                                                {if isset($formDB.fk_rhc056_num_beneficio_utiles)}
                                                    {if $dat.pk_num_beneficio_utiles==$formDB.fk_rhc056_num_beneficio_utiles}
                                                        <option selected value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                                    {else}
                                                        <option value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <label for="fk_rhc056_num_beneficio_utiles">Periodo</label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="nombreTrabajador" value="{if isset($formDB.nombre_completo)}{$formDB.nombre_completo}{/if}" disabled>
                                                <input type="hidden" name="form[txt][fk_rhb001_num_empleado]" id="fk_rhb001_num_empleado" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{/if}"  disabled>
                                                <label for="nombreTrabajador">Empleado</label>
                                            </div>
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado">Buscar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row contain-lg">
                                <input type="hidden" name="form[txt][d_requisitos]" id="d_requisitos" value="" placeholder="requisitos">
                                <input type="hidden" name="form[txt][t_requisitos]" id="t_requisitos" value="" placeholder="tipo requisito">
                                <input type="hidden" name="form[txt][d_beneficiarios]" id="d_beneficiarios" value="" placeholder="beneficiarios">
                                <input type="hidden" name="form[txt][carga]" id="carga" value="" placeholder="beneficiarios">
                                <table id="cargaFamiliarHijos" class="table table-striped table-hover">
                                    <thead>
                                    <tr align="center">
                                        <th class="col-sm-1"><i class="md-person"></i> Cedula</th>
                                        <th class="col-sm-4"><i class="md-person"></i> Nombres y apellidos</th>
                                        <th class="col-sm-2"><i class="md-person"></i> Fecha Nacimiento</th>
                                        <th class="col-sm-2"><i class="md-person"></i> Parentesco</th>
                                        <th class="col-sm-2"><i class="glyphicon glyphicon-triangle-right"></i> Sexo</th>
                                        <th class="col-sm-1">Requisitos</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                        </div>


                        <div class="tab-pane" id="step2">

                            <br/><br/>
                            <input type="hidden" name="form[txt][aleatorio]" id="aleatorio" value="{$aleatorio}">
                            <div id="dropzone" class="dropzone" style="width: 50%; margin-left: 25% "></div>
                            <br>
                            <div class="row contain-lg">
                                <!-- botones para importar empleados, botones para insertar uno a uso y eliminar -->
                                <br>
                                <div align="right">
                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                    <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionBoton"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar </button>
                                </div>
                                <br>
                            </div>

                        </div>

                    </div>

                    <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                    <ul class="pager wizard">
                        <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                        <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                        <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>
                        <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                    </ul>

                </form>
            </div>
        </div>

</div>






<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();

            /*campos required*/
            var r1 = $("#d_requisitos").val();
            var r2 = $("#t_requisitos").val();
            if(r1=='' || r2==''){
                swal("Error","Por favor debe indicar el requisito entregado por cada beneficiario hijo", "error");
                $('#formAjax a[href="#step1"]').tab('show');
                return false;
            }

            if($("#proceso").val()=='Nuevo'){

                var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevoRequisitoMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        //swal("Error",dato['detalleERROR'], "error");
                    }
                    if(dato['status']=='registrado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        });
                    }
                },'json');

            }
            if($("#proceso").val()=='Modificar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ModificarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        //swal("Error",respuesta['detalleERROR'], "error");
                    }
                    if(dato['status']=='modificar'){

                        swal("Exito","Operación Realizada con Exito!!!.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                    }
                },'json');
            }
            if($("#proceso").val()=='Conformar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ConformarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='conformado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(location).attr('href','{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ListadoConformarMET');
                        });

                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');


                    }
                },'json');
            }
            if($("#proceso").val()=='Aprobar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/AprobarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='aprobado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(location).attr('href','{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ListadoAprobarMET');
                        });
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');


                    }
                },'json');
            }
            if($("#proceso").val()=='Anular'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/AnularMET';
                swal({
                    title: 'Anular',
                    text: 'Esta seguro que desea anular este registro? \n La operación sera irreversible!',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Si, Anular',
                    closeOnConfirm: false
                }, function(){
                    $.post(url, datos ,function(dato){
                        if(dato['status']=='anular'){

                            swal("Exito","Operación Realizada con Exito!!!.", "success");
                            $('#cerrar').click();
                        }
                    },'json');

                });
            }

        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la ventana
        $('#modalAncho').css( "width", "65%" );

        //Script para forzar el avance del wizard por el botón suigiente.
        $(".form-wizard-nav").on('click', function () {
            return false;
        });

        var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/ConsultarEmpleadoMET';
        $('#buscarEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'REQUISITOS_UTILES' },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        //al dar click en el boton requisitos del listado de hijos de la carga familiar
        var $url_req = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/IngresarRequisitosMET';
        $('#cargaFamiliarHijos tbody').on( 'click', '.requisitosBeneficiario', function () {
            $('#formModalLabel3').html('Registrar Requisitos: '+$(this).attr('tituloReq'));
            $('#carga').val($(this).attr('pk_num_carga'));

            $.post($url_req,{ idEmpleado: $(this).attr('idEmpleado'), idCargaFamiliar: $(this).attr('pk_num_carga') },function(dato){
                $('#ContenidoModal3').html(dato);

                var data_b = $("#d_requisitos").val();
                var data_r = $("#t_requisitos").val();

                var arr_b  = data_b.split("#");
                var arr_r  = data_r.split("#");

                for (i=0; i<=arr_b.length; i++){
                    if(arr_b[i] == $('#carga').val()){
                        $("#fk_a006_num_miscelaneo_detalle_requisito").val(arr_r[i]);
                        $("#num_flag_entregado").prop("checked", true);
                    }
                }
            });
        });

        if ( $.fn.dataTable.isDataTable( '#cargaFamiliarHijos' ) ) {
            table = $('#cargaFamiliarHijos').DataTable();
        }
        else {
            table = $('#cargaFamiliarHijos').DataTable( {
                paging: false,
                searching: false
            } );
        }

        Dropzone.autoDiscover = false;
        $("#dropzone").dropzone({
            url: "{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/CargarImagenMET",
            addRemoveLinks: true,
            dictCancelUpload: "Eliminar",
            maxFileSize: 1000,
            maxFile:2,
            params: { tipoCarga:'tmp', aleatorio: $("#aleatorio").val()  },
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF',
            dictMaxFilesExceeded: "You can not upload any more files.",
            autoProcessQueue: true,
            complete: function(file)
            {
                if(file.status == "success")
                {

                    //swal("Carga exitosa", "Archivo cargado satisfactoriamente", "success");
                }
            },
            error: function(file)
            {
                swal("Error", "Error subiendo el archivo", "error");
            },

            removedfile: function(file, serverFileName)
            {
                var name = file.name;
                $.ajax({
                    type: "POST",
                    url: "{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/CargarImagenMET",
                    data: { filename:""+name, delete:"true" , tipoCarga: 'tmp', aleatorio: $("#aleatorio").val() },
                    success: function(data)
                    {
                        var json = JSON.parse(data);
                        if(json.res == true)
                        {
                            var element;
                            (element = file.previewElement) != null ?
                                    element.parentNode.removeChild(file.previewElement) :
                                    false;
                            swal("Elemento eliminado", "Elemento eliminado Satisfactoriamente", "success");
                        }
                    }
                });
            }
        });

    });

</script>