<div class="modal-body">

        <div class="row">



                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >


                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idBeneficio}" name="idBeneficio"/>
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <div class="row contain-lg">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="num_orden" name="form[txt][num_orden]" value="{if isset($formDB.num_orden)}{$formDB.num_orden}{else}{$orden}{/if}" disabled>
                                <label for="num_orden">Nº Orden</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select id="fk_a006_num_miscelaneo_detalle_asignacion" name="form[int][fk_a006_num_miscelaneo_detalle_asignacion]" class="form-control input-sm" required>
                                    <option value="">Seleccione...</option>
                                    {foreach item=dat from=$tipoAsignacion}
                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_asignacion)}
                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_asignacion}
                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_a006_num_miscelaneo_detalle_asignacion">Tipo Asignación</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select id="fk_rhc056_num_beneficio_utiles" name="form[int][fk_rhc056_num_beneficio_utiles]" class="form-control input-sm" required>
                                    <option value="">Seleccione...</option>
                                    {foreach item=dat from=$periodos}
                                        {if isset($formDB.fk_rhc056_num_beneficio_utiles)}
                                            {if $dat.pk_num_beneficio_utiles==$formDB.fk_rhc056_num_beneficio_utiles}
                                                <option selected value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                            {else}
                                                <option value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_rhc056_num_beneficio_utiles">Periodo</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="nombreTrabajador" value="{if isset($formDB.nombre_empleado)}{$formDB.nombre_empleado}{/if}" disabled>
                                        <input type="hidden" name="form[int][fk_rhb001_num_empleado]" id="fk_rhb001_num_empleado" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{/if}"  disabled>
                                        <label for="nombreTrabajador">Empleado</label>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado" disabled>Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="lg_b022_num_proveedor" name="form[int][lg_b022_num_proveedor]" class="form-control input-xs select2-list select2" disabled>
                                    <option value="">&nbsp;</option>
                                    {foreach item=dat from=$proveedores}
                                        {if isset($formDB.lg_b022_num_proveedor)}
                                            {if $dat.pk_num_proveedor == $formDB.lg_b022_num_proveedor}
                                                <option selected value="{$dat.pk_num_proveedor}">{$dat.nomProveedor}</option>
                                            {else}
                                                <option value="{$dat.pk_num_proveedor}">{$dat.nomProveedor}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_proveedor}">{$dat.nomProveedor}</option>
                                        {/if}

                                    {/foreach}
                                </select>
                                <label for="lg_b022_num_proveedor">Proveedor</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-6">
                        <div class="form-group">
                            <select id="hijos" name="hijos" class="form-control input-sm">
                                <option value="">Seleccione...</option>
                            </select>
                            <label for="hijos">Hijos</label>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm bolivar-mask" id="monto" value="">
                                        <input type="hidden" class="form-control input-sm" id="monto_t" name="form[int][monto_t]" value="0,00">
                                        <label for="monto">Monto</label>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" id="AgregarBeneficiario" disabled>Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- estado preparado -->
                    {if $formDB.ind_estado=='PR'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                    {/if}

                    <!-- estado revisado -->
                    {if $formDB.ind_estado=='RV'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_rev">Observaciones Revisado</label>
                                </div>
                            </div>
                        </div>
                    {/if}

                    <!-- estado conformado -->
                    {if $formDB.ind_estado=='CO'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_rev">Observaciones Revisado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_con" name="form[txt][txt_observacion_con]" value="{if isset($formDBoperacionesCO[0].txt_observaciones)}{$formDBoperacionesCO[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_con">Observaciones Conformado</label>
                                </div>
                            </div>
                        </div>
                    {/if}

                    <!-- estado aprobado -->
                    {if $formDB.ind_estado=='AP'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_rev">Observaciones Revisado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_con" name="form[txt][txt_observacion_con]" value="{if isset($formDBoperacionesCO[0].txt_observaciones)}{$formDBoperacionesCO[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_con">Observaciones Conformado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apr" name="form[txt][txt_observacion_apr]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_apr">Observaciones Aprobado</label>
                                </div>
                            </div>
                        </div>
                    {/if}
                    <!-- estado anulado -->
                    {if $formDB.ind_estado=='AN'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_anu" name="form[txt][txt_observacion_anu]" value="{if isset($formDBoperacionesAN[0].txt_observaciones)}{$formDBoperacionesAN[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_anu">Observaciones Anulado</label>
                                </div>
                            </div>
                        </div>
                    {/if}
                    <br>
                    <div class="row contain-lg">
                        <input type="hidden" name="form[txt][d_requisitos]" id="d_requisitos" value="" placeholder="requisitos">
                        <input type="hidden" name="form[txt][t_requisitos]" id="t_requisitos" value="" placeholder="tipo requisito">
                        <input type="hidden" name="form[txt][d_beneficiarios]" id="d_beneficiarios" value="" placeholder="beneficiarios">
                        <input type="hidden" name="form[txt][carga]" id="carga" value="" placeholder="beneficiarios">
                        <input type="hidden" name="form[txt][monto_a]" id="monto_a" value="" placeholder="monto asignado">
                        <table id="listaBeneficarios" class="table table-striped table-hover">
                            <thead>
                            <tr align="center">
                                <th class="col-sm-1"><i class="md-person"></i> Id</th>
                                <th class="col-sm-4"><i class="md-person"></i> Nombres y apellidos</th>
                                <th class="col-sm-2"><i class="md-person"></i> Monto</th>
                            </tr>
                            </thead>
                            <tbody>

                                {foreach item=dat from=$formDBbeneficiarios}
                                        <tr>
                                            <td>{$dat.fk_c015_num_carga_familiar}</td>
                                            <td>{$dat.nombre_hijo}</td>
                                            <td>{$dat.num_monto_asignado|number_format:2:",":"."}</td>
                                        </tr>

                                {/foreach}

                            </tbody>
                        </table>
                    </div>

                    <div class="row contain-lg">
                        <br>
                        <div align="right">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>Cerrar</button>
                        </div>
                        <br>
                    </div>
                </form>

        </div>

</div>


<script type="text/javascript">

    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la ventana
        $('#modalAncho').css( "width", "65%" );



        if ( $.fn.dataTable.isDataTable( '#listaBeneficarios' ) ) {
            table = $('#listaBeneficarios').DataTable();
        }
        else {
            table = $('#listaBeneficarios').DataTable( {
                paging: false,
                searching: false
            } );
        }

     });

</script>