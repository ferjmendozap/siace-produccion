<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Control de Asistencias</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <table id="asistencias" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Empleado</th>
                                    <th>Nombre Completo</th>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Evento</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <th colspan="5">
                                    <button class="procesarAsistencia btn ink-reaction btn-raised btn-danger" boton="si, Procesar" title="Procesar Asistencias" titulo="Estas Seguro?" mensaje="Estas seguro que desea procesar las Asistencias!!">
                                        <i class="md md-settings" style="color: #ffffff;"></i> PROCESAR ASISTENCIAS
                                    </button>
                                </th>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {


        var app = new AppFunciones();
        var dt = app.dataTable(
                '#asistencias',
                "{$_Parametros.url}modRH/procesos/controlAsistenciaCONTROL/DataPorProcesarMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_cedula_empleado" },
                    { "data": "nombre_empleado" },
                    { "data": "fec_fecha" },
                    { "data": "fec_hora" },
                    { "data": "ind_evento" }
                ]
        );

        $('.procesarAsistencia').click(function(){

            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/procesos/controlAsistenciaCONTROL/ProcesarEventosMET';
                $.post($url, { } ,function($dato){
                    if($dato==1){
                        swal("Procesados!", "Operación Realizada Con Exito!", "success");
                        $("#_contenido").load('{$_Parametros.url}modRH/procesos/controlAsistenciaCONTROL/EventosPorProcesarMET/');
                    }
                },'json');
            });
        });
    });


</script>