<div class="modal-body">

        <div class="row">
            <!-- WIZARD CESE - REINGRESO-->
            <div id="rootwizard2" class="form-wizard form-wizard-horizontal">


                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                <!-- PESTAÑAS WIZAARD -->
                <div class="form-wizard-nav">
                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                    <ul class="nav nav-justified">
                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">Información General</span></a></li>
                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Detalle Cese-Reingreso</span></a></li>
                        <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Detalle Registro</span></a></li>
                    </ul>
                </div>




                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idCeseReingreso}" name="idCeseReingreso"/>
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <!-- CONTENIDO -->
                    <div class="tab-content clearfix">

                        <!-- INFORMACIÓN GENERAL -->
                        <div class="tab-pane active" id="step1">
                            <h4 class="text-primary text-center text-lg">Datos del Empleado</h4>

                            <!-- EMPLEADO - CEDULA -->
                            <div class="row contain-lg">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm dirty" id="nombreTrabajador" value="{if isset($formDB.nombre_completo)}{$formDB.nombre_completo}{/if}" disabled>
                                                <input type="hidden" name="form[txt][fk_rhb001_num_empleado]" id="fk_rhb001_num_empleado" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{/if}"  disabled>
                                                <label for="nombreTrabajador">Empleado</label>
                                            </div>
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado">Buscar</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="form-group" id="fk_rhb001_num_empleadoError">
                                        <label for="fk_rhb001_num_empleado">Empleado</label>
                                        <select id="fk_rhb001_num_empleado" name="form[int][fk_rhb001_num_empleado]" class="form-control select2-list select2 input-sm" required>
                                            <option value="">Seleccione Empleado</option>
                                            {foreach item=i from=$Empleado}
                                                {if isset($formDB.fk_rhb001_num_empleado)}
                                                    {if $i.pk_num_empleado==$formDB.fk_rhb001_num_empleado}
                                                        <option selected value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                                    {else}
                                                        <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>-->
                                </div>
                                <!--<div class="col-md-1">
                                    <div class="btn-toolbar">
                                        <div class="btn-group">
                                            <button
                                                    type="button"
                                                    class="btn btn-sm btn-danger ink-reaction btn-raised logsUsuarioModal"
                                                    data-toggle="offcanvas" onclick="location.href='#offcanvas-filtro'"><i class="fa fa-filter"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="cedula" name="form[txt][cedula]" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" disabled>
                                        <label for="cedula">Cedula</label>
                                    </div>
                                </div>

                            </div>

                            <!-- ORGANISMO - TIPO - FECHA NACIMIENTO-->
                            <div class="row contain-lg">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" required data-msg-required="Seleccione Organismo" disabled>
                                            <option value="">Seleccione...</option>
                                            {foreach item=dat from=$listadoOrganismos}
                                                {if isset($formDB.fk_a001_num_organismo)}
                                                    {if $dat.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                                        <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                    {else}
                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <label for="fk_a001_num_organismo">Organismo</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select id="fk_a006_num_miscelaneo_detalle_tipocr" name="form[int][fk_a006_num_miscelaneo_detalle_tipocr]" class="form-control input-sm" disabled>
                                            <option value="">Seleccione...</option>
                                            {foreach item=dat from=$TipoCR}
                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipocr)}
                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipocr}
                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {else}
                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <label for="fk_a006_num_miscelaneo_detalle_tipocr">Tipo</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="fecha_nacimiento" name="form[txt][fecha_nacimiento]" value="{if isset($formDB.fec_nacimiento)}{$formDB.fec_nacimiento|date_format:"d-m-Y"}{/if}" disabled>
                                        <label for="fecha_nacimiento">Fecha Nacimiento</label>
                                    </div>
                                </div>
                            </div>

                            <!-- DEPENDENCIAS - SEXO - EDAD -->
                            <div class="row contain-lg">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select id="fk_a004_num_dependencia" name="form[int][fk_a004_num_dependencia]" class="form-control input-sm">
                                            <option value="">Seleccione...</option>
                                            {foreach item=dat from=$listadoDependencias}
                                                {if isset($formDB.fk_a004_num_dependencia)}
                                                    {if $dat.pk_num_dependencia==$formDB.fk_a004_num_dependencia}
                                                        <option selected value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                    {else}
                                                        <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <label for="fk_a004_num_dependencia">Dependencia</label>
                                        <input type="hidden" id="ind_dependencia" name="form[txt][ind_dependencia]" value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}" >
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select id="sexo" name="form[int][sexo]" class="form-control input-sm" disabled>
                                            <option value="">Seleccione...</option>
                                            {foreach item=dat from=$Sexo}
                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_sexo)}
                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_sexo}
                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {else}
                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <label for="sexo">Sexo</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="num_edad" name="form[int][num_edad]" value="{if isset($formDB.num_edad)}{$formDB.num_edad}{/if}" disabled>
                                        <label for="num_edad">Edad</label>
                                    </div>
                                </div>
                            </div>

                            <!-- CARGO - FECHA INGRESO - AÑOS DE SERVICIO -->
                            <div class="row contain-lg">
                                <div class="col-md-6">
                                    <div class="form-group" id="fk_rhc063_num_puestosError">
                                        <label for="fk_rhc063_num_puestos">Cargo</label>
                                        <select id="fk_rhc063_num_puestos" name="form[int][fk_rhc063_num_puestos]" class="form-control select2-list select2 input-sm" >
                                            <option value="">Seleccione Empleado</option>
                                            {foreach item=i from=$listadoCargo}
                                                {if isset($formDB.fk_rhc063_num_puestos)}
                                                    {if $i.pk_num_puestos==$formDB.fk_rhc063_num_puestos}
                                                        <option selected value="{$i.pk_num_puestos}">{$i.ind_descripcion_cargo}</option>
                                                    {else}
                                                        <option value="{$i.pk_num_puestos}">{$i.ind_descripcion_cargo}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$i.pk_num_puestos}">{$i.ind_descripcion_cargo}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>
                                    <input type="hidden" class="form-control input-sm" id="ind_cargo" name="form[txt][ind_cargo]" value="{if isset($formDB.ind_cargo)}{$formDB.ind_cargo}{/if}" disabled>
                                    <!--<input type="hidden" id="fk_rhc063_num_puestos" name="form[int][fk_rhc063_num_puestos]" value="{if isset($formDB.fk_rhc063_num_puestos)}{$formDB.fk_rhc063_num_puestos}{/if}" >-->
                                    <!--<label for="ind_cargo">Cargo</label>-->
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="fec_fecha_ingreso" name="form[txt][fec_fecha_ingreso]" value="{if isset($formDB.fec_fecha_ingreso)}{$formDB.fec_fecha_ingreso|date_format:"d-m-Y"}{/if}" disabled>
                                        <label for="fec_fecha_ingreso">Fecha Ingreso</label>
                                        <p class="help-block"><span class="text-xs" style="color: red">dd-mm-yyyy</span></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="num_anio_servicio" name="form[int][num_anio_servicio]" value="{if isset($formDB.num_anio_servicio)}{$formDB.num_anio_servicio}{/if}" disabled>
                                        <label for="num_anio_servicio">Años de Servicio</label>
                                    </div>
                                </div>
                            </div>

                            <!-- SUELDO ACTUAL -->
                            <div class="row contain-lg">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="num_sueldo_actual" name="form[int][num_sueldo_actual]" value="{if isset($formDB.num_sueldo_actual)}{$formDB.num_sueldo_actual|number_format:2:",":"."}{/if}" disabled>
                                        <label for="num_sueldo_actual">Sueldo</label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- DETALLE CESE REINGRESO -->
                        <div class="tab-pane" id="step2">

                            <h4 class="text-primary text-center text-lg">Datos</h4>
                            <hr class="ruler-lg">

                                <div class="row contain-lg">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control input-sm">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoTipoNomina}
                                                    {if isset($formDB.fk_nmb001_num_tipo_nomina)}
                                                        {if $dat.pk_num_tipo_nomina==$formDB.fk_nmb001_num_tipo_nomina}
                                                            <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_nmb001_num_tipo_nomina">Tipo Nomina</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a006_num_miscelaneo_detalle_tipotrab" name="form[int][fk_a006_num_miscelaneo_detalle_tipotrab]" class="form-control input-sm">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoTipoTrabajador}
                                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipotrab)}
                                                        {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipotrab}
                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a006_num_miscelaneo_detalle_tipotrab">Tipo Trabajador</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row contain-lg">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="ind_resolucion" name="form[txt][ind_resolucion]" value="{if isset($formDB.ind_resolucion)}{$formDB.ind_resolucion}{/if}" style="text-transform: uppercase">
                                            <label for="ind_resolucion">Nro. Resolución</label>
                                        </div>
                                    </div>
                                </div>

                            <h4 class="text-primary text-center text-lg">Cese</h4>
                            <hr class="ruler-lg">

                                <div class="row contain-lg">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select id="num_situacion_trabajo" name="form[int][num_situacion_trabajo]" class="form-control input-sm" disabled>
                                                {foreach key=key item=item from=$EstadoRegistro}
                                                    {if isset($formDB.num_situacion_trabajo)}
                                                        {if $key==$formDB.num_situacion_trabajo}
                                                            <option selected value="{$key}">{$item}</option>
                                                        {else}
                                                            <option value="{$key}">{$item}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$key}">{$item}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="num_situacion_trabajo">Situacion de Trabajo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group date" id="fecha_egreso">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm" id="fec_fecha_egreso" name="form[txt][fec_fecha_egreso]" value="{if isset($formDB.fec_fecha_egreso)}{$formDB.fec_fecha_egreso|date_format:"d-m-Y"}{/if}">
                                                    <label>Fecha Egreso</label>
                                                </div>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select id="fk_rhc032_num_motivo_cese" name="form[int][fk_rhc032_num_motivo_cese]" class="form-control input-sm">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoMotivoCese}
                                                    {if isset($formDB.fk_rhc032_num_motivo_cese)}
                                                        {if $dat.pk_num_motivo_cese==$formDB.fk_rhc032_num_motivo_cese}
                                                            <option selected value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_rhc032_num_motivo_cese">Motivo Cese</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row contain-lg">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_egreso" name="form[txt][txt_observacion_egreso]" value="{if isset($formDB.txt_observacion_egreso)}{$formDB.txt_observacion_egreso}{/if}" style="text-transform: uppercase">
                                            <label for="txt_observacion_egreso">Explicación</label>
                                        </div>
                                    </div>
                                </div>


                            <!-- botones para importar empleados, botones para insertar uno a uso y eliminar -->
                            <!--<br>
                            <div class="row text-center">
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="importarEmpleados">Importar Empleados</button>
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="insertarEmpleado" descripcion="Agregar Empleado" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#formModal"
                                        titulo="Agregar Empleado">Insertar Empleado</button>
                                <button type="submit" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="guardarPeriodo"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="regresarListado2">Volver al Listado</button>
                            </div>
                            <br>-->

                        </div>

                        <div class="tab-pane" id="step3">
                                <h4 class="text-primary text-center text-lg">Información</h4>
                                <hr class="ruler-lg">

                                {if isset($formDB.txt_estatus)}

                                    {if $formDB.txt_estatus=='PR' || $formDB.txt_estatus=='CO' || $formDB.txt_estatus=='AP'}

                                        <!-- PREPARADO -->
                                        <div class="row contain-lg">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm"  id="preparado_por" name="form[txt][preparado_por]" value="{if isset($formDBoperacionesPR[0].nombre_registro)}{$formDBoperacionesPR[0].nombre_registro}{/if}" disabled>
                                                    <input type="hidden" id="usuario_preparado_por">
                                                    <label for="preparado_por">Preparado Por</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="fecha_preparado" name="form[txt][fecha_preparado]" value="{if isset($formDBoperacionesPR[0].fec_operacion)}{$formDBoperacionesPR[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                                    <label for="fecha_preparado">Fecha</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" maxlength="7" id="ind_periodo" name="form[txt][ind_periodo]" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" disabled>
                                                    <label for="ind_periodo">Periodo</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row contain-lg">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                                    <label for="txt_observacion_pre">Observaciones</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- CONFORMADO -->
                                        <div class="row contain-lg">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm"  id="conformado_por" name="form[txt][conformado_por]" value="{if isset($formDBoperacionesCO[0].nombre_registro)}{$formDBoperacionesCO[0].nombre_registro}{/if}" disabled>
                                                    <input type="hidden" id="usuario_conformado_por">
                                                    <label for="conformado_por">Conformado Por</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="fecha_conformado" name="form[txt][fecha_conformado]" value="{if isset($formDBoperacionesCO[0].fec_operacion)}{$formDBoperacionesCO[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                                    <label for="fecha_conformado">Fecha</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row contain-lg">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_con" name="form[txt][txt_observacion_con]" value="{if isset($formDBoperacionesCO[0].txt_observaciones)}{$formDBoperacionesCO[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                                    <label for="txt_observacion_con">Observaciones</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- APROBADO -->
                                        <div class="row contain-lg">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm"  id="aprobado_por" name="form[txt][aprobado_por]" value="{if isset($formDBoperacionesAP[0].nombre_registro)}{$formDBoperacionesAP[0].nombre_registro}{/if}" disabled>
                                                    <input type="hidden" id="usuario_aprobado_por">
                                                    <label for="aprobado_por">Aprobado Por</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="fecha_aprobado" name="form[txt][fecha_aprobado]" value="{if isset($formDBoperacionesAP[0].fec_operacion)}{$formDBoperacionesAP[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                                    <label for="fecha_aprobado">Fecha</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row contain-lg">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apro" name="form[txt][txt_observacion_apro]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                                    <label for="txt_observacion_apro">Observaciones</label>
                                                </div>
                                            </div>
                                        </div>

                                    {else}

                                        <!-- ANULADO -->
                                        <div class="row contain-lg">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm"  id="anulado_por" name="form[txt][anulado_por]" value="{if isset($formDBoperacionesAN[0].nombre_registro)}{$formDBoperacionesAN[0].nombre_registro}{/if}" disabled>
                                                    <input type="hidden" id="usuario_anulado_por">
                                                    <label for="anulado_por">Anulado Por</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="fecha_anulado" name="form[txt][fecha_anulado]" value="{if isset($formDBoperacionesAN[0].fec_operacion)}{$formDBoperacionesAN[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                                    <label for="fecha_anulado">Fecha</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row contain-lg">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_anulado" name="form[txt][txt_observacion_anulado]" value="{if isset($formDBoperacionesAN[0].txt_observaciones)}{$formDBoperacionesAN[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                                    <label for="txt_observacion_anulado">Observaciones</label>
                                                </div>
                                            </div>
                                        </div>

                                    {/if}

                                {else}
                                    <!-- PREPARADO -->
                                    <div class="row contain-lg">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm"  id="preparado_por" name="form[txt][preparado_por]" value="{if isset($formDBoperacionesPR[0].nombre_registro)}{$formDBoperacionesPR[0].nombre_registro}{/if}" disabled>
                                                <input type="hidden" id="usuario_preparado_por">
                                                <label for="preparado_por">Preparado Por</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="fecha_preparado" name="form[txt][fecha_preparado]" value="{if isset($formDBoperacionesPR[0].fec_operacion)}{$formDBoperacionesPR[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                                <label for="fecha_preparado">Fecha</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" maxlength="7" id="ind_periodo" name="form[txt][ind_periodo]" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" disabled>
                                                <label for="ind_periodo">Periodo</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row contain-lg">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                                <label for="txt_observacion_pre">Observaciones</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- CONFORMADO -->
                                    <div class="row contain-lg">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm"  id="conformado_por" name="form[txt][conformado_por]" value="{if isset($formDBoperacionesCO[0].nombre_registro)}{$formDBoperacionesCO[0].nombre_registro}{/if}" disabled>
                                                <input type="hidden" id="usuario_conformado_por">
                                                <label for="conformado_por">Conformado Por</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="fecha_conformado" name="form[txt][fecha_conformado]" value="{if isset($formDBoperacionesCO[0].fec_operacion)}{$formDBoperacionesCO[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                                <label for="fecha_conformado">Fecha</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row contain-lg">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_con" name="form[txt][txt_observacion_con]" value="{if isset($formDBoperacionesCO[0].txt_observaciones)}{$formDBoperacionesCO[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                                <label for="txt_observacion_con">Observaciones</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- APROBADO -->
                                    <div class="row contain-lg">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm"  id="aprobado_por" name="form[txt][aprobado_por]" value="{if isset($formDBoperacionesAP[0].nombre_registro)}{$formDBoperacionesAP[0].nombre_registro}{/if}" disabled>
                                                <input type="hidden" id="usuario_aprobado_por">
                                                <label for="aprobado_por">Aprobado Por</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="fecha_aprobado" name="form[txt][fecha_aprobado]" value="{if isset($formDBoperacionesAP[0].fec_operacion)}{$formDBoperacionesAP[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                                <label for="fecha_aprobado">Fecha</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row contain-lg">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apro" name="form[txt][txt_observacion_apro]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                                <label for="txt_observacion_apro">Observaciones</label>
                                            </div>
                                        </div>
                                    </div>
                                {/if}

                                {if $proceso=='Anular'}

                                    <!-- ANULADO -->
                                    <div class="row contain-lg">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm"  id="anulado_por" name="form[txt][anulado_por]" value="{if isset($formDBoperaciones[2].nombre_registro)}{$formDBoperaciones[2].nombre_registro}{/if}" disabled>
                                                <input type="hidden" id="usuario_anulado_por">
                                                <label for="anulado_por">Anulador Por</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="fecha_anulado" name="form[txt][fecha_anulado]" value="{if isset($formDBoperaciones[2].fec_operacion)}{$formDBoperaciones[2].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                                <label for="fecha_anulado">Fecha</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row contain-lg">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_anulado" name="form[txt][txt_observacion_anulado]" value="{if isset($formDBoperaciones[2].txt_observaciones)}{$formDBoperaciones[2].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                                <label for="txt_observacion_anulado">Observaciones</label>
                                            </div>
                                        </div>
                                    </div>

                                {/if}

                            <!-- fecha -->
                            <input type="hidden" id="fec_fecha" name="form[txt][fec_fecha]" value="{if isset($formDB.fec_fecha)}{$formDB.fec_fecha|date_format:"d-m-Y"}{else}{$fecha}{/if}">

                            <div class="row contain-lg">
                                <!-- botones para importar empleados, botones para insertar uno a uso y eliminar -->
                                <br>
                                <div align="right">
                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                    <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionC"><span class="glyphicon glyphicon-floppy-disk"></span> Procesar </button>
                                    <input type="hidden" id="motivo" name="form[txt][motivo]" value="{if isset($T)}{$T}{/if}">
                                </div>
                                <br>
                            </div>
                        </div>

                    </div>

                    <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                    <ul class="pager wizard">
                        <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                        <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                        <!--<li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>-->
                        <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                    </ul>

                </form>
            </div>
        </div>
</div>



<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha_egreso").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $("#fecha_fin").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();

            if($("#proceso").val()=='Nuevo'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/RegistrarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        //swal("Error",respuesta['detalleERROR'], "error");
                    }
                    if(dato['status']=='noEmpleado'){

                        $('#formAjax a[href="#step1"]').tab('show');
                        swal("Error!", "Debe Seleccionar un Empleado", "error");

                    }
                    if(dato['status']=='registrar'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $("#_contenido").load('{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL');
                        });

                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');


                    }
                },'json');
            }
            if($("#proceso").val()=='Modificar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ModificarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        //swal("Error",respuesta['detalleERROR'], "error");
                    }
                    if(dato['status']=='modificar'){

                        swal("Exito","Operación Realizada con Exito!!!.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                    }
                },'json');
            }
            if($("#proceso").val()=='Conformar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ConformarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='conformado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $("#_contenido").load('{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ListadoConformarMET');
                        });

                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');


                    }
                },'json');
            }
            if($("#proceso").val()=='Aprobar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/AprobarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='aprobado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $("#_contenido").load('{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ListadoAprobarMET');
                        });
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');


                    }
                },'json');
            }
            if($("#proceso").val()=='Anular'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/AnularMET';
                swal({
                    title: 'Anular',
                    text: 'Esta seguro que desea anular este registro? \n La operación sera irreversible!',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Si, Anular',
                    closeOnConfirm: false
                }, function(){
                    $.post(url, datos ,function(dato){
                        if(dato['status']=='anular'){

                            swal("Exito","Operación Realizada con Exito!!!.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                            $('#cerrar').click();
                        }
                    },'json');

                });
            }

        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la ventana
        $('#modalAncho').css( "width", "80%" );

        //Script para forzar el avance del wizard por el botón suigiente.
        $(".form-wizard-nav").on('click', function () {
            return false;
        });


        var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/ConsultarEmpleadoMET';
        $('#buscarEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'CESE_REINGRESO' },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });



        $("#botonFiltro").click(function() {

            var $url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/BusquedaFiltroMET';

            $('#formAjax').each(function () { {*resetear los campos del formulario*}
                this.reset();
            });
            $('#s2id_fk_rhc063_num_puestos .select2-chosen').html('Seleccione Empleado');

            $.post($url, { organismo: $('#organismo_filtro').val(), dependencia: $('#dependencia_filtro').val() , estatus_persona: $('#estatus_persona').val() , estatus_empleado: $('#estatus_empleado').val() }, function(datos){

                $('#fk_rhb001_num_empleado').html('');
                $('#fk_rhb001_num_empleado').append('<option value="">Seleccione Empleado</option>');
                $('#s2id_fk_rhb001_num_empleado .select2-chosen').html('Seleccione Empleado');

                for(var i=0;i<datos.length;i++){

                    $('#fk_rhb001_num_empleado').append('<option value="'+datos[i]['pk_num_empleado']+'">'+datos[i]['nombre_completo']+'</option>');

                }


            },'json');

        });


        var hoy = new Date();
        var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();

        if($("#proceso").val()=='Nuevo'){

        }

        if($("#proceso").val()=='Conformar'){
            $(".form-control").attr("disabled","disabled");
            $("#txt_observacion_con").removeAttr('disabled');
            $("#fecha_conformado").val(fecha);
            $("#conformado_por").val('{$nombreUsuario}');
            $("#usuario_conformado_por").val({$idUsuario});
            {*cambio nombre al boton*}
            $("#accionC").html('Conformar');
        }

        if($("#proceso").val()=='Aprobar'){
            $(".form-control").attr("disabled","disabled");
            $("#txt_observacion_apro").removeAttr('disabled');
            $("#fecha_aprobado").val(fecha);
            $("#aprobado_por").val('{$nombreUsuario}');
            $("#usuario_aprobado_por").val({$idUsuario});
            {*cambio nombre al boton*}
            $("#accionC").html('Aprobar');
        }

        if($("#proceso").val()=='Modificar'){

            var motivo = $("#motivo").val();

            {*cambio nombre al boton*}
            $("#accionC").html('Modificar');

            if(motivo=='C'){
                $("#txt_observacion_pre").removeAttr('disabled');
                $("#fk_rhb001_num_empleado").attr('disabled','disabled');
            }else{
                $("#txt_observacion_pre").removeAttr('disabled');
                $("#fk_a001_num_organismo").removeAttr('disabled');
                $("#fk_a004_num_dependencia").removeAttr('disabled');
                $("#fec_fecha_ingreso").removeAttr('disabled');
                $("#fk_rhb001_num_empleado").attr('disabled','disabled');
                $("#fec_fecha_egreso").attr('disabled','disabled');
                $("#fk_rhc032_num_motivo_cese").attr('disabled','disabled');
                $("#txt_observacion_egreso").attr('disabled','disabled');
            }


        }
        {*VER DATOS EN EL FORMULARIO*}
        if($("#proceso").val()=='Ver'){
            $(".form-control").attr("disabled","disabled");
            $("#accionC").hide();
        }
        {*ANULAR PROCESO*}
        if($("#proceso").val()=='Anular'){
            $(".form-control").attr("disabled","disabled");
            $("#accionC").html('Anular');
            $("#txt_observacion_anulado").removeAttr('disabled');
            $("#fecha_anulado").val(fecha);
            $("#anulado_por").val('{$nombreUsuario}');
            $("#usuario_anulado_por").val({$idUsuario});
        }


    });







</script>