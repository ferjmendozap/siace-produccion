<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Periodos Bono de Alimentación</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <input type="hidden" id="proceso" value="{$proceso}">
                        <div class="card-body">
                            <table id="datatableListadoBonos" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Nro.</th>
                                    <th>Periodo</th>
                                    <th>Nomina</th>
                                    <th>Descripción</th>
                                    <th>Organismo</th>
                                    <th>Estado</th>
                                    {if $proceso=='periodos'}
                                        <th  width="40">Mod</th>
                                        <th  width="40">Ver</th>
                                        <th  width="50">Cerrar</th>
                                    {else}
                                        <th  width="100">Reg. Eventos</th>
                                    {/if}
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$listadoBonoAlimentacion}

                                    <tr id="idBonoAlimentacion{$dat.pk_num_beneficio}">
                                        <td>{$dat.pk_num_beneficio}</td>
                                        <td>{$dat.ind_periodo}</td>
                                        <td>{$dat.ind_nombre_nomina}</td>
                                        <td style="text-transform: uppercase">{$dat.ind_descripcion}</td>
                                        <td>{$dat.ind_descripcion_empresa}</td>
                                        <td>{if $dat.num_estado==1}Abierto{else}Cerrado{/if}</td>
                                        {if $proceso=='periodos'}
                                            <td class="text-center" width="40">
                                                {if $dat.num_estado==1}
                                                    {if in_array('RH-01-02-01-01-01-02-M',$_Parametros.perfil)}
                                                        <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  idBonoAlimentacion="{$dat.pk_num_beneficio}" data-toggle="modal" data-target="#formModal"
                                                                data-keyboard="false" data-backdrop="static"
                                                                descipcion="El Usuario a Modificado Bono Alimentación" title="Editar Bono Alimentación" titulo="Modificar Bono Alimentación">
                                                            <i class="fa md-edit" style="color: #ffffff;"></i>
                                                        </button>
                                                    {/if}
                                                {/if}
                                            </td>
                                            <td class="text-center" width="40">
                                                {if in_array('RH-01-02-01-01-01-03-C',$_Parametros.perfil)}
                                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idBonoAlimentacion="{$dat.pk_num_beneficio}" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static"
                                                            descipcion="El usuario a visualizado datos del Empleado" title="Ver Datos" titulo="Ver Datos del Bono de Alimentacion">
                                                        <i class="fa md-search" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            </td>
                                            <td class="text-center" width="50">
                                                {if $dat.num_estado==1}
                                                    {if in_array('RH-01-02-01-01-01-04-CE',$_Parametros.perfil)}
                                                        <button class="cerrar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBonoAlimentacion="{$dat.pk_num_beneficio}"  boton="si, Cerrar"
                                                                descipcion="El usuario va cerrar un periodo" title="Cerrar Periodo" titulo="Cerrar Periodo?" mensaje="Estas seguro que desea Cerrar el Periodo!!">
                                                            <i class="fa md-block" style="color: #ffffff;"></i>
                                                        </button>
                                                    {/if}
                                                {/if}
                                            </td>
                                        {else}
                                            <td class="text-center" width="100">
                                                {if $dat.num_estado==1}
                                                    {if in_array('RH-01-02-01-02-01-N',$_Parametros.perfil)}
                                                        <button class="eventos logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idBonoAlimentacion="{$dat.pk_num_beneficio}"
                                                                descipcion="Registrar Eventos">
                                                            <i class="fa md-add-circle" style="color: #ffffff;"></i> Registrar
                                                        </button>
                                                    {/if}
                                                {/if}
                                            </td>
                                        {/if}

                                    </tr>
                                {/foreach}
                                </tbody>
                                {if $proceso=='periodos'}
                                <tfoot>
                                    <th colspan="9">
                                        {if in_array('RH-01-02-01-01-01-01-N',$_Parametros.perfil)}
                                            <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario va a registrar periodo bono alimentacion" titulo="Nuevo Periodo Bono Alimentacion"  id="nuevo" >
                                                Nuevo Periodo Bono Alimentacion &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>
                                {/if}

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/VerMET';
        var $url2='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ModMET';
        var $url_r='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/BonoAlimentacionMET';

        var table = $('#datatableListadoBonos').DataTable({
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        //NUEVO PERIODO BONO ALIMENTACION
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_r,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //MODIFICAR PERIODO BONO ALIMENTACION
        $('#datatableListadoBonos tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url2,{ idBonoAlimentacion: $(this).attr('idBonoAlimentacion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //VER PERIODO BONO ALIMENTACION
        $('#datatableListadoBonos tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idBonoAlimentacion: $(this).attr('idBonoAlimentacion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //CERRAR PERIODO BONO ALIMENTACION
        $('#datatableListadoBonos tbody').on( 'click', '.cerrar', function () {

            var idBonoAlimentacion=$(this).attr('idBonoAlimentacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                    var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CerrarPeriodoMET';
                    $.post($url, { idBonoAlimentacion: idBonoAlimentacion},function(dato){
                        if(dato['status']=='OK'){
                            swal({
                                title: 'Cerrado!',
                                text: 'El Periodo Bono Alimentacion fue Cerrado con Exito.',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonText: 'Aceptar',
                                closeOnConfirm: true
                            }, function(){

                                $("#_contenido").load('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL');

                            });
                        }
                    },'json');
            });
        });

        //REGISTRAR EVENTOS PERIODO BONO ALIMENTACION
        $('#datatableListadoBonos tbody').on( 'click', '.eventos', function () {
            $("#_contenido").load('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/RegistrarEventosMET/'+$(this).attr('idBonoAlimentacion'));
        });
    });
</script>