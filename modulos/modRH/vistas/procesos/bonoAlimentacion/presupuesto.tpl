<div class="col-lg-12 contain-lg">
    <div class="table-responsive">
        <table id="datatable2" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Cod. Presupuesto</th>
                <th>Ejer. Presupuestario</th>
                <th>Fecha Inicio</th>
                <th>Fecha Fin</th>
                <th>Monto</th>
            </tr>
            </thead>
            <tbody>
            {foreach item=i from=$lista}
                <tr id="" partida="">
                    <input type="hidden" value="{$i.pk_num_presupuesto}" class="presupuesto"
                     nombre="{$i.fec_anio}"
                     cod="{$i.ind_cod_presupuesto}">
                    <td>{$i.ind_cod_presupuesto}</td>
                    <td>{$i.fec_anio}</td>
                    <td>{$i.fec_inicio|date_format:" %d-%m-%Y"}</td>
                    <td>{$i.fec_fin|date_format:" %d-%m-%Y"}</td>
                    <td>{$i.num_monto_aprobado|number_format:2:",":"."}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');

            $("#ind_cod_presupuesto").val(input.attr('cod'));
            $("#fk_prb004_num_presupuesto").val(input.attr('value'));

            /***************************************************************************************/
            $(document.getElementById('cerrarModal2')).click();
            $(document.getElementById('ContenidoModal2')).html('');
            /***************************************************************************************/


        });
    });
</script>