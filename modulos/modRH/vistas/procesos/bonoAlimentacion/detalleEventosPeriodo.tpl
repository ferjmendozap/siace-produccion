
    <!-- NOMBRE DEL EMPLEADO -->
    <div class="row contain-lg">
        <div class="well clearfix">
            <div class="col-md-12 text-center">
                Empleado : {$nombre}
            </div>
        </div>
    </div>

    <br>

    <!-- DETALLE DE LOS EVENTOS -->
    <div class="row contain-lg">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        {for $i=1 to {$detalle.num_dias_periodo}}
                            <th>{$i}</th>
                        {/for}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {for $i=1 to {$detalle.num_dias_periodo}}

                            {if $detalle.{$id|cat:$i}=='X'}{$color='color:#090;'}

                            {elseif $detalle.{$id|cat:$i}=='D'}{$color='color:#900;'}

                            {else}
                                {$color=''}
                            {/if}
                            <td align="center" style="font-size:12px; padding:2px; font-weight:bold; {$color}" >{$detalle.{$id|cat:$i}}</td>
                        {/for}
                    </tr>
                </tbody>


            </table>
        </div>
    </div>

    <div class="row contain-lg">

        <div class="col-md-5">
            <div class="card">
                <div class="card-head card-head-xs style-primary">
                    <header>LEYENDA</header>
                </div><!--end .card-head -->
                <div class="card-body">
                    <div class="col-md-6">
                        <p style="color:#090; font-weight: bold">X : ASISTENCIA</p>
                    </div>
                    <div class="col-md-6">
                        <p style="color:#000; font-weight: bold">F : FERIADO</p>
                    </div>
                    <div class="col-md-6">
                        <p style="color:#900; font-weight: bold">D : DESCUENTO</p>
                    </div>
                    <div class="col-md-6">
                        <p style="color:#000; font-weight: bold">I : INACTIVO</p>
                    </div>
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div>

        <div class="col-md-7">
            <div class="card">
                <div class="card-head card-head-xs style-primary">
                    <header>RESUMEN</header>
                </div><!--end .card-head -->
                <div class="card-body">
                    <div class="row">
                        <form class="form" role="form">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="total" value="{$detalle.num_dias_periodo}" readonly>
                                    <label for="total">TOTAL</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="habiles" value="{math equation="{$detalle.num_dias_periodo} - ({$detalle.num_dias_inactivos}+{$detalle.num_dias_feriados})"} "  readonly>
                                    <label for="habiles">HABILES</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="trabajados" value="{math equation="{$detalle.num_dias_periodo} - ({$detalle.num_dias_inactivos}+{$detalle.num_dias_feriados}+{$detalle.num_dias_descuento})"} "  readonly>
                                    <label for="trabajados">TRABAJADOS</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="descuento" value="{$detalle.num_dias_descuento}" readonly>
                                    <label for="descuento">DESCUENTO</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="feriados" value="{$detalle.num_dias_feriados}" readonly>
                                    <label for="feriados">FERIADOS</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="inactivos" value="{$detalle.num_dias_inactivos}" readonly>
                                    <label for="inactivos">INACTIVOS</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div>

    </div>







<script type="text/javascript">



    $(document).ready(function() {


        $('#modalAncho').css( "width", "70%" );

    });




</script>