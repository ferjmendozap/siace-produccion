<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idIslr}" name="idIslr"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="4" id="fec_anio" name="form[txt][fec_anio]" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{/if}" required data-rule-number="true" data-msg-required="Indique Periodo" data-msg-number="Por favor Introduzca solo numeros (Indique Periodo)" autocomplete="off">
                                            <label for="fec_anio">Periodo</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">* Ejemplo: 2015</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group date" id="ind_desde">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm" maxlength="7" value="{if isset($formDB.ind_desde)}{$formDB.ind_desde}{/if}" name="form[txt][ind_desde]" id="ind_desde"  required data-msg-required="Indique Fecha Desde" autocomplete="off">
                                                    <label for="ind_desde">Desde</label>
                                                </div>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <p class="help-block"><span class="text-xs" style="color: red">* Ejemplo: (2015-03)</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group date" id="ind_hasta">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm " maxlength="7" id="ind_hasta" name="form[txt][ind_hasta]" value="{if isset($formDB.ind_hasta)}{$formDB.ind_hasta}{/if}" required data-msg-required="Indique Fecha Hasta" autocomplete="off">
                                                    <label for="ind_hasta">Hasta</label>
                                                </div>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <p class="help-block"><span class="text-xs" style="color: red">* Ejemplo: (2015-03)</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm porcentaje-mask" id="num_porcentaje"  name="form[int][num_porcentaje]" value="{if isset($formDB.num_porcentaje)}{$formDB.num_porcentaje}{/if}" required data-msg-required="Indique Porcentaje" autocomplete="off">
                                            <label for="num_porcentaje">Porcentaje</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">


    $("#ind_desde").datepicker({
        format:'yyyy-mm',
        language:'es',
        minViewMode: 1,
        autoclose: true
    });
    $("#ind_hasta").datepicker({
        format:'yyyy-mm',
        language:'es',
        minViewMode: 1,
        autoclose: true
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            var p_ini = $("#ind_desde").val();
            var p_fin = $("#ind_hasta").val();

            var aux1 = p_ini.split("-");
            var aux2 = p_fin.split("-");

            if(aux1[0] > aux2[0]){
                swal("Error!", "EL Periodo Final no puede ser menor que el Periodo Inicial.", "error");
                $("#ind_hasta").focus();
                return false;
            }else if(aux1[0] == aux2[0]){

                if(aux1[1] > aux2[1]){
                    swal("Error!", "EL Periodo Final no puede ser menor que el Periodo Inicial.", "error");
                    $("#ind_hasta").focus();
                    return false;
                }

            }

            $.post('{$_Parametros.url}modRH/gestion/islrCONTROL/IslrMET', datos ,function(dato){

                if(dato['status']=='modificar'){

                    $(document.getElementById('idIslr'+dato['idIslr'])).html('<td width="25%">'+dato['fec_anio']+'</td>' +
                            '<td width="20%">'+dato['ind_desde']+'</td>' +
                            '<td width="20%">'+dato['ind_hasta']+'</td>' +
                            '<td width="20%">'+dato['num_porcentaje']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idIslr="'+dato['idIslr']+'"' +
                            'descipcion="El Usuario a Modificado Impuesto Sobre la Renta" titulo="Modificar Impuesto Sobre la Renta">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idIslr="'+dato['idIslr']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Impuesto Sobre la Renta" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Impuesto Sobre la Renta!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){


                        $(document.getElementById('datatable2')).append('<tr id="idIslr'+dato['idIslr']+'" style="font-size: 11px">' +
                                '<td width="25%">'+dato['fec_anio']+'</td>' +
                                '<td width="20%">'+dato['ind_desde']+'</td>' +
                                '<td width="20%">'+dato['ind_hasta']+'</td>' +
                                '<td width="20%">'+dato['num_porcentaje']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idIslr="'+dato['idIslr']+'"' +
                                'descipcion="El Usuario a Modificado Impuesto Sobre la Renta" titulo="Modificar Impuesto Sobre la Renta">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idIslr="'+dato['idIslr']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Impuesto Sobre la Renta" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Impuesto Sobre la Renta!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');

                }

            },'json');


        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "60%" );


    });






</script>