<!-- FORMULARIOS CARRERAS -->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idInstruccionCarreras}" name="idInstruccionCarreras"/>
                    <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                    <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="fk_a006_num_miscelaneo_detalle_gradoinst" name="form[int][fk_a006_num_miscelaneo_detalle_gradoinst]" class="form-control input-sm" required data-msg-required="Seleccione Grado de Instrucción">
                                    <option value="">Seleccione...</option>
                                    {foreach item=dat from=$listadoGradoInstruccion}
                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_gradoinst)}
                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_gradoinst}
                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_a006_num_miscelaneo_detalle_gradoinst">Grado de Instruccion</label>
                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group date" id="fecha">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm" name="form[txt][fec_graduacion]" id="fec_graduacion" value="{if isset($formDB.fec_graduacion)}{$formDB.fec_graduacion|date_format:"d-m-Y"}{/if}" required>
                                        <label for="fec_graduacion">Fecha Graduación</label>
                                    </div>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                </div>
                            </div>
                        </div><!--end .col -->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="fk_rhc064_num_nivel_grado" name="form[int][fk_rhc064_num_nivel_grado]" class="form-control input-sm">
                                    <option value="">Seleccione...</option>
                                    {foreach item=dat from=$NivelGrado}
                                        {if isset($formDB.fk_rhc064_num_nivel_grado)}
                                            {if $dat.pk_num_nivel_grado==$formDB.fk_rhc064_num_nivel_grado}
                                                <option selected value="{$dat.pk_num_nivel_grado}">{$dat.ind_nombre_nivel_grado}</option>
                                            {else}
                                                <option value="{$dat.pk_num_nivel_grado}">{$dat.ind_nombre_nivel_grado}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_nivel_grado}">{$dat.ind_nombre_nivel_grado}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_rhc064_num_nivel_grado">Nivel de Instruccion</label>
                            </div>
                        </div><!--end .col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-daterange input-group" id="rango-fechas-instruccion">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" name="form[txt][fec_ingreso]" id="fec_ingreso" value="{if isset($formDB.fec_ingreso)}{$formDB.fec_ingreso|date_format:"d-m-Y"}{/if}" required />
                                    </div>
                                    <span class="input-group-addon">a</span>
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" name="form[txt][fec_egreso]" id="fec_egreso" value="{if isset($formDB.fec_egreso)}{$formDB.fec_egreso|date_format:"d-m-Y"}{/if}"  required/>
                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                                <label for="">Periodo</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="fk_a006_num_miscelaneo_detalle_area" name="form[int][fk_a006_num_miscelaneo_detalle_area]" class="form-control input-sm">
                                    <option value="">&nbsp;</option>
                                    {foreach item=area from=$Area}
                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_area)}
                                            {if $area.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_area}
                                                <option selected value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                            {else}
                                                <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                            {/if}
                                        {else}
                                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_a006_num_miscelaneo_detalle_area">Area</label>
                            </div>
                        </div><!--end .col -->

                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="fk_a006_num_miscelaneo_detalle_colegiatura" name="form[int][fk_a006_num_miscelaneo_detalle_colegiatura]" class="form-control input-sm">
                                    <option value="">&nbsp;</option>
                                    {foreach item=dat from=$Colegiatura}
                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_colegiatura)}
                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_colegiatura}
                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_a006_num_miscelaneo_detalle_colegiatura">Colegiatura</label>
                            </div>
                        </div><!--end .col -->
                    </div><!--end .row -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="fk_rhc057_num_profesion" name="form[int][fk_rhc057_num_profesion]" class="form-control input-sm">
                                    <option value="">&nbsp;</option>
                                    {foreach item=dat from=$Profesiones}
                                        {if isset($formDB.fk_rhc057_num_profesion)}
                                            {if $dat.pk_num_profesion==$formDB.fk_rhc057_num_profesion}
                                                <option selected value="{$dat.pk_num_profesion}">{$dat.ind_nombre_profesion}</option>
                                            {else}
                                                <option value="{$dat.pk_num_profesion}">{$dat.ind_nombre_profesion}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_profesion}">{$dat.ind_nombre_profesion}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_rhc057_num_profesion">Prefesión</label>
                            </div>
                        </div><!--end .col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" maxlength="30" id="ind_colegiatura"  name="form[txt][ind_colegiatura]" value="{if isset($formDB.ind_colegiatura)}{$formDB.ind_colegiatura}{/if}">
                                <label for="ind_colegiatura">Num. Colegiatura</label>
                            </div>
                        </div><!--end .col -->
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fk_rhc040_num_institucion">Centro de Estudio</label>
                                <select id="fk_rhc040_num_institucion" name="form[int][fk_rhc040_num_institucion]" class="form-control input-sm select2-list select2">
                                    <option value="">Seleccione Centro de Estudio</option>
                                    {foreach item=dat from=$Centros}
                                        {if isset($formDB.fk_rhc040_num_institucion)}
                                            {if $dat.pk_num_institucion==$formDB.fk_rhc040_num_institucion}
                                                <option selected value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                            {else}
                                                <option value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="form[txt][txt_observaciones]" id="txt_observaciones" class="form-control input-sm" rows="3" placeholder="" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">{if isset($formDB.txt_observaciones)}{$formDB.txt_observaciones}{/if}</textarea>
                                <label for="txt_observaciones">Observaciones</label>
                            </div>
                        </div><!--end .col -->
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div align="right">
                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>


<!-- </form> -->
<style type="text/css">
    .letra_pequena{
        font-size: 11px;
    }
</style>
<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $("#rango-fechas-instruccion").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            var tabla = $('#datatableCarreras').DataTable();


            $.post('{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/CarrerasMET', datos ,function(dato){

                if(dato['status']=='error'){

                    /*Error para usuarios normales del sistema*/
                    //swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                }else if(dato['status']=='modificar'){


                    //elimino el registro temporalmente que se esta modificamndo
                    tabla.row('#idInstruccionCarreras'+dato['idInstruccionCarreras']+'').remove().draw();


                    //agrego el registro temporar
                    //permite dar la sensacion de que la tabla se actualiza automaticamente
                    var fila = tabla.row.add([
                        dato['grado_instruccion'],
                        dato['ind_nombre_profesion'],
                        dato['ind_nombre_institucion'],
                        dato['fec_graduacion'],
                        '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                        'data-keyboard="false" data-backdrop="static" idInstruccionCarreras="'+dato['idInstruccionCarreras']+'"' +
                        'descipcion="El Usuario a Modificado Instruccion del Empleado" titulo="Modificar Carrera del Empleado">' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}',
                        '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInstruccionCarreras="'+dato['idInstruccionCarreras']+'"  boton="si, Eliminar"' +
                        'descipcion="El usuario a eliminado Instruccion del Empleado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Carrera del Empleado!!">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}'
                    ]).draw()
                            .nodes()
                            .to$()
                            .addClass('letra_pequena');

                    $(fila)
                            .attr('id','idInstruccionCarreras'+dato['idInstruccionCarreras']+'')
                            .attr('valor',dato['idInstruccionCarreras'])

                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){


                    //agrego el registro temporalmente
                    var fila = tabla.row.add([
                        dato['grado_instruccion'],
                        dato['ind_nombre_profesion'],
                        dato['ind_nombre_institucion'],
                        dato['fec_graduacion'],
                        '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                        'data-keyboard="false" data-backdrop="static" idInstruccionCarreras="'+dato['idInstruccionCarreras']+'"' +
                        'descipcion="El Usuario a Modificado Instruccion del Empleado" titulo="Modificar Carrera del Empleado">' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}',
                        '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInstruccionCarreras="'+dato['idInstruccionCarreras']+'"  boton="si, Eliminar"' +
                        'descipcion="El usuario a eliminado Instruccion del Empleado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Carrera del Empleado!!">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}'
                    ]).draw()
                            .nodes()
                            .to$()
                            .addClass('letra_pequena');

                    $(fila)
                            .attr('id','idInstruccionCarreras'+dato['idInstruccionCarreras']+'')
                            .attr('valor',dato['idInstruccionCarreras'])

                    swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');


                }

            },'json');


        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();
        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "60%" );

        $("#contenidoModal2").css({ 'border' : '2px solid #0aa89e' });
        $("#headerModal2").css({ 'background-color':'#0aa89e','color' : '#FFFFFF' });

        //AL SELECIONAR GRADO DE INSTRUCCION
        $("#fk_a006_num_miscelaneo_detalle_gradoinst").change(function () {
            $("#fk_a006_num_miscelaneo_detalle_gradoinst option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/ActualizarNivelMET", { GradoInstruccion: $('#fk_a006_num_miscelaneo_detalle_gradoinst').val() }, function(data){
                    var json = data,
                    obj = JSON.parse(json);
                    $("#fk_rhc064_num_nivel_grado").html(obj);
                });
            });
        });

        //AL SELECIONAR AREA
        $("#fk_a006_num_miscelaneo_detalle_area").change(function () {
            $("#fk_a006_num_miscelaneo_detalle_area option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/ActualizarProfesionMET", { Area: $('#fk_a006_num_miscelaneo_detalle_area').val() , GradoInstruccion: $('#fk_a006_num_miscelaneo_detalle_gradoinst').val() }, function(data){
                    var json = data,
                            obj = JSON.parse(json);
                    $("#fk_rhc057_num_profesion").html(obj);
                });
            });
        });


    });






</script>