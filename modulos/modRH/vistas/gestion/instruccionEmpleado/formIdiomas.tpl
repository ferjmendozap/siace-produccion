<!-- FORMULARIO IDIOMAS -->
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">

            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                <input type="hidden" value="1" name="valido" />
                <input type="hidden" value="{$idInstruccionIdiomas}" name="idInstruccionIdiomas"/>
                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select id="fk_rhc013_num_idioma" name="form[int][fk_rhc013_num_idioma]" class="form-control input-sm" required data-msg-required="Seleccione Idiomas">
                                <option value="">Seleccione...</option>
                                {foreach item=dat from=$idiomas}
                                    {if isset($formDB.fk_rhc013_num_idioma)}
                                        {if $dat.pk_num_idioma==$formDB.fk_rhc013_num_idioma}
                                            <option selected value="{$dat.pk_num_idioma}">{$dat.ind_nombre_idioma}</option>
                                        {else}
                                            <option value="{$dat.pk_num_idioma}">{$dat.ind_nombre_idioma}</option>
                                        {/if}
                                    {else}
                                        <option value="{$dat.pk_num_idioma}">{$dat.ind_nombre_idioma}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_rhc013_num_idioma">Idioma</label>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <select id="fk_a006_num_miscelaneo_detalle_lectura" name="form[int][fk_a006_num_miscelaneo_detalle_lectura]" class="form-control input-sm" required>
                                <option value="">&nbsp;</option>
                                {foreach item=area from=$nivel}
                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_lectura)}
                                        {if $area.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_lectura}
                                            <option selected value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {else}
                                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {/if}
                                    {else}
                                        <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle_lectura">Lectura</label>
                        </div>
                    </div><!--end .col -->
                    <div class="col-md-12">
                        <div class="form-group">
                            <select id="fk_a006_num_miscelaneo_detalle_oral" name="form[int][fk_a006_num_miscelaneo_detalle_oral]" class="form-control input-sm" required>
                                <option value="">&nbsp;</option>
                                {foreach item=area from=$nivel}
                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_oral)}
                                        {if $area.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_oral}
                                            <option selected value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {else}
                                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {/if}
                                    {else}
                                        <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle_oral">Oral</label>
                        </div>
                    </div><!--end .col -->
                    <div class="col-md-12">
                        <div class="form-group">
                            <select id="fk_a006_num_miscelaneo_detalle_escritura" name="form[int][fk_a006_num_miscelaneo_detalle_escritura]" class="form-control input-sm" required>
                                <option value="">&nbsp;</option>
                                {foreach item=area from=$nivel}
                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_escritura)}
                                        {if $area.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_escritura}
                                            <option selected value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {else}
                                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {/if}
                                    {else}
                                        <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle_escritura">Escritura</label>
                        </div>
                    </div><!--end .col -->
                    <div class="col-md-12">
                        <div class="form-group">
                            <select id="fk_a006_num_miscelaneo_detalle_general" name="form[int][fk_a006_num_miscelaneo_detalle_general]" class="form-control input-sm" required>
                                <option value="">&nbsp;</option>
                                {foreach item=area from=$nivel}
                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_general)}
                                        {if $area.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_general}
                                            <option selected value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {else}
                                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {/if}
                                    {else}
                                        <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle_general">General</label>
                        </div>
                    </div><!--end .col -->

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div align="right">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>


<!-- </form> -->
<style type="text/css">
    .letra_pequena{
        font-size: 11px;
    }
</style>
<script type="text/javascript">

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            var tabla = $('#datatableIdiomas').DataTable();


            $.post('{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/IdiomasMET', datos ,function(dato){

                if(dato['status']=='error'){

                    /*Error para usuarios normales del sistema*/
                    //swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                }else if(dato['status']=='modificar'){

                    //elimino el registro temporalmente que se esta modificamndo
                    tabla.row('#idInstruccionIdiomas'+dato['idInstruccionIdiomas']+'').remove().draw();


                    //agrego el registro temporar
                    //permite dar la sensacion de que la tabla se actualiza automaticamente
                    var fila = tabla.row.add([
                        dato['ind_nombre_idioma'],
                        dato['lectura'],
                        dato['oral'],
                        dato['escritura'],
                        dato['general'],
                        '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                        'data-keyboard="false" data-backdrop="static" idInstruccionIdiomas="'+dato['idInstruccionIdiomas']+'"' +
                        'descipcion="Modificar Idioma del Empleado" titulo="Modificar Idioma del Empleado">' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}',
                        '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInstruccionIdiomas="'+dato['idInstruccionIdiomas']+'"  boton="si, Eliminar"' +
                        'descipcion="Eliminar Idioma del Empleado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Idioma del Empleado!!">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}'
                    ]).draw()
                            .nodes()
                            .to$()
                            .addClass('letra_pequena');

                    $(fila)
                            .attr('id','idInstruccionIdiomas'+dato['idInstruccionIdiomas']+'')
                            .attr('valor',dato['idInstruccionIdiomas'])

                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){


                    //agrego el registro temporalmente
                    var fila = tabla.row.add([
                        dato['ind_nombre_idioma'],
                        dato['lectura'],
                        dato['oral'],
                        dato['escritura'],
                        dato['general'],
                        '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                        'data-keyboard="false" data-backdrop="static" idInstruccionIdiomas="'+dato['idInstruccionIdiomas']+'"' +
                        'descipcion="Modificar Idioma del Empleado" titulo="Modificar Idioma del Empleado">' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}',
                        '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInstruccionIdiomas="'+dato['idInstruccionIdiomas']+'"  boton="si, Eliminar"' +
                        'descipcion="Eliminar Idioma del Empleado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Idioma del Empleado!!">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}'
                    ]).draw()
                            .nodes()
                            .to$()
                            .addClass('letra_pequena');

                    $(fila)
                            .attr('id','idInstruccionIdiomas'+dato['idInstruccionIdiomas']+'')
                            .attr('valor',dato['idInstruccionIdiomas'])

                    swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');


                }

            },'json');


        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();
        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "35%" );

        $("#contenidoModal2").css({ 'border' : '2px solid #0aa89e' });
        $("#headerModal2").css({ 'background-color':'#0aa89e','color' : '#FFFFFF' });


    });






</script>