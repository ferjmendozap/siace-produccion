<!-- FORMULARIO OTROS ESTUDIOS -->
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">

            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                <input type="hidden" value="1" name="valido" />
                <input type="hidden" value="{$idInstruccionOtrosEstudios}" name="idInstruccionOtrosEstudios"/>
                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="fk_rhc048_num_curso">Curso</label>
                            <select id="fk_rhc048_num_curso" name="form[int][fk_rhc048_num_curso]" class="form-control input-sm select2-list select2" required>
                                <option value="">Seleccione Curso</option>
                                {foreach item=dat from=$Cursos}
                                    {if isset($formDB.fk_rhc048_num_curso)}
                                        {if $dat.pk_num_curso==$formDB.fk_rhc048_num_curso}
                                            <option selected value="{$dat.pk_num_curso}">{$dat.ind_nombre_curso}</option>
                                        {else}
                                            <option value="{$dat.pk_num_curso}">{$dat.ind_nombre_curso}</option>
                                        {/if}
                                    {else}
                                        <option value="{$dat.pk_num_curso}">{$dat.ind_nombre_curso}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="fk_rhc040_num_institucion">Centro de Estudio</label>
                            <select id="fk_rhc040_num_institucion" name="form[int][fk_rhc040_num_institucion]" class="form-control input-sm select2-list select2" required>
                                <option value="">Seleccione Centro de Estudio</option>
                                {foreach item=dat from=$Centros}
                                    {if isset($formDB.fk_rhc040_num_institucion)}
                                        {if $dat.pk_num_institucion==$formDB.fk_rhc040_num_institucion}
                                            <option selected value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                        {else}
                                            <option value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                        {/if}
                                    {else}
                                        <option value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="fk_a006_num_miscelaneo_detalle_tipocurso" name="form[int][fk_a006_num_miscelaneo_detalle_tipocurso]" class="form-control input-sm" required data-msg-required="Requerido">
                                <option value="">&nbsp;</option>
                                {foreach item=area from=$tipoCurso}
                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipocurso)}
                                        {if $area.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipocurso}
                                            <option selected value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {else}
                                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                        {/if}
                                    {else}
                                        <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle_tipocurso">Tipo Curso</label>
                        </div>
                    </div><!--end .col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-daterange input-group" id="rango-fechas-curso">
                                <div class="input-group-content">
                                    <input type="text" class="form-control input-sm" name="form[txt][fec_desde]" id="fec_desde" value="{if isset($formDB.fec_desde)}{$formDB.fec_desde|date_format:"d-m-Y"}{/if}" required data-msg-required="Requerido" />
                                </div>
                                <span class="input-group-addon">a</span>
                                <div class="input-group-content">
                                    <input type="text" class="form-control input-sm" name="form[txt][fec_hasta]" id="fec_hasta" value="{if isset($formDB.fec_hasta)}{$formDB.fec_hasta|date_format:"d-m-Y"}{/if}"  required data-msg-required="Requerido" />
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <label for="">Periodo</label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm"  id="num_horas"  name="form[int][num_horas]" value="{if isset($formDB.num_horas)}{$formDB.num_horas}{/if}" required data-msg-required="Requerido">
                            <label for="num_horas">Horas</label>
                        </div>
                    </div><!--end .col -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <select id="periodo_cul_anio" name="form[int][periodo_cul_anio]" class="form-control input-sm" required data-msg-required="Requerido">
                                <option value="">....</option>
                                {foreach key=key item=item from=$arrayAnio}
                                    {if isset($anio)}
                                        {if $key==$anio}
                                            <option selected value="{$key}">{$item}</option>
                                        {else}
                                            <option value="{$key}">{$item}</option>
                                        {/if}
                                    {else}
                                        <option value="{$key}">{$item}</option>
                                    {/if}
                                {/foreach}

                            </select>
                            <label for="periodo_cul_anio">Culminación Año</label>
                        </div>
                    </div><!--end .col -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <select id="periodo_cul_mes" name="form[int][periodo_cul_mes]" class="form-control input-sm" value="{if isset($mes)}{$mes}{/if}" required data-msg-required="Requerido">
                                <option value="">....</option>
                                {foreach key=key item=item from=$arrayMes}
                                    {if isset($mes)}
                                        {if $key==$mes}
                                            <option selected value="{$key}">{$item}</option>
                                        {else}
                                            <option value="{$key}">{$item}</option>
                                        {/if}
                                    {else}
                                        <option value="{$key}">{$item}</option>
                                    {/if}
                                {/foreach}

                            </select>
                            <label for="periodo_cul_mes">Culminación Mes</label>
                        </div>
                    </div><!--end .col -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea name="form[txt][txt_observaciones]" id="txt_observaciones" class="form-control input-sm" rows="3" placeholder="" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">{if isset($formDB.txt_observaciones)}{$formDB.txt_observaciones}{/if}</textarea>
                            <label for="txt_observaciones">Observaciones</label>
                        </div>
                    </div><!--end .col -->
                </div>
               <div class="row">
                   <div class="col-md-12">
                        <div align="right">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                        </div>
                   </div>
                </div>
            </form>


        </div>
    </div>
</div>


<!-- </form> -->
<style type="text/css">
    .letra_pequena{
        font-size: 11px;
    }
</style>
<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $("#rango-fechas-curso").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            var tabla = $('#datatableOtrosEstudios').DataTable();


            $.post('{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/OtrosEstudiosMET', datos ,function(dato){

                if(dato['status']=='error'){

                    /*Error para usuarios normales del sistema*/
                    //swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                }if(dato['status']=='errorCurso'){

                    /*Error para usuarios normales del sistema*/
                    swal("Información!", "Debe Seleccionar un Curso", "warning");


                }if(dato['status']=='errorInstitucion'){

                    /*Error para usuarios normales del sistema*/
                    swal("Información!", "Debe Selecciona una Institución", "warning");


                }else if(dato['status']=='modificar'){


                    //elimino el registro temporalmente que se esta modificamndo
                    tabla.row('#idInstruccionOtrosEstudios'+dato['idInstruccionOtrosEstudios']+'').remove().draw();


                    //agrego el registro temporar
                    //permite dar la sensacion de que la tabla se actualiza automaticamente
                    var fila = tabla.row.add([
                        dato['ind_nombre_curso'],
                        dato['ind_nombre_institucion'],
                        dato['num_horas'],
                        dato['fec_desde'],
                        dato['fec_hasta'],
                        '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                        'data-keyboard="false" data-backdrop="static" idInstruccionOtrosEstudios="'+dato['idInstruccionOtrosEstudios']+'"' +
                        'descipcion="Modificado Otros Estudios del Empleado" titulo="Modificar Otros Estudios del Empleado">' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}',
                        '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInstruccionOtrosEstudios="'+dato['idInstruccionOtrosEstudios']+'"  boton="si, Eliminar"' +
                        'descipcion="Eliminado Otros Estudios del Empleado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Otros Estudios del Empleado!!">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}'
                    ]).draw()
                            .nodes()
                            .to$()
                            .addClass('letra_pequena');

                    $(fila)
                            .attr('id','idInstruccionOtrosEstudios'+dato['idInstruccionOtrosEstudios']+'')
                            .attr('valor',dato['idInstruccionOtrosEstudios'])

                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){


                    //agrego el registro temporalmente
                    var fila = tabla.row.add([
                        dato['ind_nombre_curso'],
                        dato['ind_nombre_institucion'],
                        dato['num_horas'],
                        dato['fec_desde'],
                        dato['fec_hasta'],
                        '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                        'data-keyboard="false" data-backdrop="static" idInstruccionOtrosEstudios="'+dato['idInstruccionOtrosEstudios']+'"' +
                        'descipcion="Modificado Otros Estudios del Empleado" titulo="Modificar Otros Estudios del Empleado">' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}',
                        '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInstruccionOtrosEstudios="'+dato['idInstruccionOtrosEstudios']+'"  boton="si, Eliminar"' +
                        'descipcion="Eliminado Otros Estudios del Empleado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Otros Estudios del Empleado!!">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}'
                    ]).draw()
                            .nodes()
                            .to$()
                            .addClass('letra_pequena');

                    $(fila)
                            .attr('id','idInstruccionOtrosEstudios'+dato['idInstruccionOtrosEstudios']+'')
                            .attr('valor',dato['idInstruccionOtrosEstudios'])

                    swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');


                }

            },'json');


        }

    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();
        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "60%" );

        $("#contenidoModal2").css({ 'border' : '2px solid #0aa89e' });
        $("#headerModal2").css({ 'background-color':'#0aa89e','color' : '#FFFFFF' });


    });

</script>