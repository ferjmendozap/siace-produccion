<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                        <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                            <form enctype="multipart/form-data" id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <div class="form-wizard-nav">
                                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                                    <ul class="nav nav-justified">
                                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">DATOS GENERALES</span></a></li>
                                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">EDUCACIÓN Y OCUPACIÓN</span></a></li>
                                    </ul>
                                </div><!--end .form-wizard-nav -->

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idCargaFamiliar}" name="idCargaFamiliar"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>

                                <div class="tab-content clearfix">

                                    <!-- datos genereales-->
                                    <div class="tab-pane active" id="step1">

                                        <br/><br/>
                                        <div class="col-xs-10" style="margin-top: -10px">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="cedula" name="form[txt][ind_cedula_documento]" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" required data-msg-required="Indique Cedula de Identidad">
                                                        <label for="ind_cedula_documento">Introduzca Cedula de Identidad</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_sexo" name="form[int][fk_a006_num_miscelaneo_detalle_sexo]" class="form-control input-sm" required data-msg-required="Seleccione el Sexo">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=sex from=$Sexo}
                                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_sexo)}
                                                                    {if $sex.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_sexo}
                                                                        <option selected value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_sexo">Sexo</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_parentezco" name="form[int][fk_a006_num_miscelaneo_detalle_parentezco]" class="form-control input-sm" required data-msg-required="Seleccione Parentesco">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$Parentesco}
                                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_parentezco)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_parentezco}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_parentezco">Parentesco</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>


                                            </div><!--end .row -->

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="apellido1" name="form[txt][apellido1]" value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}" required data-msg-required="Indique Primer Apellido" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="apellido1">Primer Apellido</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="apellido2" name="form[txt][apellido2]" value="{if isset($formDB.ind_apellido2)}{$formDB.ind_apellido2}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="apellido2">Segundo Apellido</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="nombre1" name="form[txt][nombre1]" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}" required data-msg-required="Indique Primer Nombre" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="nombre1">Primer Nombre</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="nombre2" name="form[txt][nombre2]" value="{if isset($formDB.ind_nombre2)}{$formDB.ind_nombre2}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="nombre2">Segundo Nombre</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div><!--end .col -->
                                            </div><!--end .row -->


                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group date" id="fecha">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][fec_nacimiento]" id="fec_nacimiento" value="{if isset($formDB.fec_nacimiento)}{$formDB.fec_nacimiento|date_format:"d-m-Y"}{/if}" required data-msg-required="Indique Fecha de Nacimiento" readonly>
                                                                <label for="fec_nacimiento">Fecha Nacimiento</label>
                                                            </div>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" value="{if isset($formDBdireccion.pk_num_direccion_persona)}{$formDBdireccion.pk_num_direccion_persona}{/if}" name="form[int][pk_num_direccion_persona]"/>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_tipodir" name="form[int][fk_a006_num_miscelaneo_detalle_tipodir]" class="form-control input-sm">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$TipoDir}
                                                                {if isset($formDBdireccion.fk_a006_num_miscelaneo_detalle_tipodir)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBdireccion.fk_a006_num_miscelaneo_detalle_tipodir}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_tipodir">Tipo. Dir</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control input-sm" id="ind_direccion" name="form[txt][ind_direccion]" value="{if isset($formDBdireccion.ind_direccion)}{$formDBdireccion.ind_direccion}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="ind_direccion">Dirección</label>
                                                    </div>
                                                </div>
                                            </div><!--end .row -->
                                        </div><!--end .col -->

                                        <div class="col-xs-2" style="margin-top: -10px;">
                                            <div class="row" align="center">
                                                <div class="col-md-12">
                                                    <div id="fotoCargada">
                                                        <img class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{$_Parametros.ruta_Img}foto.png" alt="Cargar o Actualizar Foto del Empleado" title="Cargar o Actualizar Foto del Empleado" id="cargarImagen" style="cursor: pointer" />
                                                    </div>
                                                    <input type="file" name="Foto" id="Foto" style="display: none" />
                                                    <!--<small id="ampliarFoto" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" style="cursor: pointer">Ampliar Foto</small>-->
                                                </div><!--end .col -->
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_det_gruposangre" name="form[int][fk_a006_num_miscelaneo_det_gruposangre]" class="form-control input-sm">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$GrupoSanguineo}
                                                                {if isset($formDB.fk_a006_num_miscelaneo_det_gruposangre)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_det_gruposangre}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_det_gruposangre">Grupo Sanguineo</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_edocivil" name="form[int][fk_a006_num_miscelaneo_detalle_edocivil]" class="form-control input-sm" required data-msg-required="Seleccione Estado Civil">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$EdoCivil}
                                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_edocivil)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_edocivil}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_edocivil">Estado Civil</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="col-lg-12">
                                                        <div class="card">
                                                            <div class="card-body no-padding">
                                                                <div class="table-responsive no-margin">
                                                                    <table class="table table-striped no-margin" id="contenidoTabla">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="text-center">#</th>
                                                                            <th>Tipo Telefono</th>
                                                                            <th>Telefono</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        {if isset($formDBtelefono)}
                                                                            {foreach item=datos from=$formDBtelefono}
                                                                                <tr>
                                                                                    <input type="hidden" value="{$datos.pk_num_telefono}" name="form[int][pk_num_telefono][{$n}]">
                                                                                    <td class="text-right" style="vertical-align: middle;">
                                                                                        {$numero++}
                                                                                    </td>
                                                                                    <td style="vertical-align: middle;">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_miscelaneo_detalle_tipo_telefono{$n}" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono][{$n}]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$TipoTelf}
                                                                                                        {if isset($datos.fk_a006_num_miscelaneo_detalle_tipo_telefono)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_tipo_telefono}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td style="vertical-align: middle;">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="form-group floating-label" id="ind_telefono{$n}Error">
                                                                                                <input type="text" class="form-control input-sm" value="{$datos.ind_telefono}" name="form[txt][ind_telefono][{$n}]" id="ind_telefono{$n}">
                                                                                                <label for="ind_telefono{$n}"><i class="md md-insert-comment"></i> Telefono </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <input type="hidden" value="{$n++}">
                                                                                </tr>
                                                                            {/foreach}
                                                                        {/if}
                                                                        </tbody>
                                                                        <tfoot>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <div class="col-sm-12 text-center">
                                                                                    <button type="button" id="nuevoTelefono" class="btn btn-info ink-reaction btn-raised">
                                                                                        <i class="md md-add"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- fin datos generales -->

                                    <!-- nacimiento -->
                                    <div class="tab-pane" id="step2">
                                        <br><br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select id="fk_a006_num_miscelaneo_detalle_gradoinst" name="form[int][fk_a006_num_miscelaneo_detalle_gradoinst]" class="form-control input-sm">
                                                        <option value="">Seleccione</option>
                                                        {foreach item=grado from=$GradoInst}
                                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_gradoinst)}
                                                                {if $grado.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_gradoinst}
                                                                    <option selected value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                                                                {else}
                                                                    <option value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="fk_a006_num_miscelaneo_detalle_gradoinst">Grado de Instrucción</label>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select id="fk_a006_num_miscelaneo_detalle_tipoedu" name="form[int][fk_a006_num_miscelaneo_detalle_tipoedu]" class="form-control input-sm">
                                                        <option value="">Seleccione</option>
                                                        {foreach item=dat from=$TipoEdu}
                                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipoedu)}
                                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipoedu}
                                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="fk_a006_num_miscelaneo_detalle_tipoedu">Tipo de Educacion</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="fk_rhc040_num_institucion">Centro de Estudio</label>
                                                    <select id="fk_rhc040_num_institucion" name="form[int][fk_rhc040_num_institucion]" class="form-control input-sm select2-list select2">
                                                        <option value="">Seleccione Centro de Estudio</option>
                                                        {foreach item=dat from=$Centros}
                                                            {if isset($formDB.fk_rhc040_num_institucion)}
                                                                {if $dat.pk_num_institucion==$formDB.fk_rhc040_num_institucion}
                                                                    <option selected value="{$dat.pk_num_centro}">{$dat.ind_nombre_institucion}</option>
                                                                {else}
                                                                    <option value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </div
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="ind_empresa" name="form[txt][ind_empresa]" value="{if isset($formDB.ind_empresa)}{$formDB.ind_empresa}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                    <label for="ind_empresa">Empresa</label>
                                                </div>
                                            </div><!--end .col -->
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="txt_direccion_empresa" name="form[txt][txt_direccion_empresa]" value="{if isset($formDB.txt_direccion_empresa)}{$formDB.txt_direccion_empresa}{/if}"  onkeyup="$(this).val($(this).val().toUpperCase())">
                                                    <label for="txt_direccion_empresa">Dirección</label>
                                                </div>
                                            </div><!--end .col -->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm bolivar-mask" id="num_sueldo_mensual" name="form[int][num_sueldo_mensual]" value="{if isset($formDB.num_sueldo_mensual)}{$formDB.num_sueldo_mensual|number_format:2:",":"."}{/if}">
                                                    <label for="num_sueldo_mensual">Sueldo Mensual</label>
                                                </div>
                                            </div><!--end .col -->
                                        </div><!--end .row -->

                                        <div class="row">
                                            <br><br>
                                            <div align="center">
                                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                                <ul class="pager wizard">
                                    <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>
                                    <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                                </ul>

                            </form>
                        </div>

            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha").datepicker({
        format:'dd-mm-yyyy'
    });
    $("#fecha2").datepicker({
        format:'dd-mm-yyyy'
    });
    $("#fechaIng").datepicker({
        format:'dd-mm-yyyy'
    });

    var app = new AppFunciones();
    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/cargaFamiliarCONTROL/CargaFamiliarMET', datos ,function(dato){

                if(dato['status']=='modificar'){

                    $(document.getElementById('idCargaFamiliar'+dato['idCargaFamiliar'])).html('<td>'+dato['ind_cedula_documento']+'</td>' +
                            '<td width="40%">'+dato['nombre1']+' '+dato['nombre2']+' '+dato['apellido1']+' '+dato['apellido2']+'</td>' +
                            '<td width="10%">Recargar</td>' +
                            '<td width="15%">'+dato['fec_nacimiento']+'</td>' +
                            '<td width="10%">Recargar</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idCargaFamiliar="'+dato['idCargaFamiliar']+'"' +
                            'descipcion="El Usuario a Modificado Carga Familiar" titulo="Modificar Carga Familiar">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCargaFamiliar="'+dato['idCargaFamiliar']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Carga Familiar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Carga Familiar!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){

                    $(document.getElementById('datatable2')).append('<tr id="idCargaFamiliar'+dato['idCargaFamiliar']+'" style="font-size: 11px">' +
                            '<td width="15%">'+dato['ind_cedula_documento']+'</td>' +
                            '<td width="40%">'+dato['nombre1']+' '+dato['nombre2']+' '+dato['apellido1']+' '+dato['apellido2']+'</td>' +
                            '<td width="10%">Recargar</td>' +
                            '<td width="15%">'+dato['fec_nacimiento']+'</td>' +
                            '<td width="10%">Recargar</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCargaFamiliar="'+dato['idCargaFamiliar']+'"' +
                            'descipcion="El Usuario a Modificado Carga Familiar" titulo="Modificar Carga Familiar">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCargaFamiliar="'+dato['idCargaFamiliar']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Carga Familiar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Carga Familiar!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '</tr>');
                    swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');


                }else if(dato['status']=='revisar'){


                    $(document.getElementById('idEmpleado'+dato['idEmpleado'])).html('');
                    swal("Registro del Empleado Revisado!", "EL Empleado fue revisado Exitosamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }else if(dato['status']=='aprobar'){

                    $(document.getElementById('idEmpleado'+dato['idEmpleado'])).html('');
                    swal("Registro del Empleado Aprobado!", "EL Empleado fue aprobado Exitosamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "72%" );
        //Script para forzar el avance del wizard por el botón suigiente.
        /*$(".form-wizard-nav").on('click', function(){
            return false;
        });*/

        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });

        $("#nuevoTelefono").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;


            var tipoTelefono='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_tipo_telefono'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$TipoTelf}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    '</div>';
            var telefono='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" value="" name="form[txt][ind_telefono]['+nuevoTr+']" id="ind_telefono'+nuevoTr+'">'+
                    '<label for="ind_telefono'+nuevoTr+'"><i class="md md-insert-comment"></i> Telefono '+numero+'</label>'+
                    '</div>';
            idtabla.append('<tr>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+tipoTelefono+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+telefono+'</div></td>'+
                    '</tr>');
        });


    });






</script>