
<form id="formAjax" action="{$_Parametros.url}modRH/gestion/permisoCONTROL/CrearPermisoMET" class="form floating-label" novalidate="novalidate">
	<input type="hidden" name="valido" value="1" />
	<br/>
	<h2 class="text-primary">&nbsp;Registrar Permiso</h2>
	<hr class="ruler-xl">
	<div class="col-md-6 col-sm-6">
		{if in_array('RH-01-01-03-09-PG',$_Parametros.perfil)}
			<div class="form-group floating-label" id="empleado_borrar">
				<select id="pk_num_empleado" name="pk_num_empleado" id="s2id_single" class="select2-container form-control select2 dirty" onchange="borrarEmpleado(this.value)">
					<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
						<option value="">&nbsp;</option>
						{foreach item=emp from=$empleado}
							{if $emp.pk_num_empleado == $form.pk_num_empleado}
								<option selected value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
							{else}
								<option value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
							{/if}
						{/foreach}
				</select>
				<label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
			</div>
		{else}
			<div class="form-group floating-label">
				<select id="pk_num_empleado" name="pk_num_empleado" id="s2id_single" class="select2-container form-control select2 dirty">
					<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
						<option value="">&nbsp;</option>
						{foreach item=emp from=$empleado}
							{if $emp.pk_num_empleado == $form.pk_num_empleado}
								<option selected value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
							{/if}
						{/foreach}
				</select>
				<label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
			</div>
		{/if}
		<div class="form-group floating-label" id="empleado_aprueba">
			<select id="pk_num_empleado_aprueba" name="pk_num_empleado_aprueba" id="s2id_single" class="select2-container form-control select2 dirty">
				<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
					<option value="">&nbsp;</option>
					{foreach item=emp from=$empleado}
						{if $emp.pk_num_empleado==$form.responsable}
							<option selected value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
						{else}
							<option value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
						{/if}
					{/foreach}
			</select>
			<label for="pk_num_empleado_aprueba"><i class="glyphicon glyphicon-user"></i> Aprueba</label>
		</div>
		<div class="form-group floating-label">
			<select id="motivo_ausencia" name="motivo_ausencia" class="form-control dirty">
					<option value="">&nbsp;</option>
					{foreach item=mot from=$motivoAusencia}
						<option value="{$mot.pk_num_miscelaneo_detalle}">{$mot.ind_nombre_detalle}</option>
					{/foreach}
			</select>
			<label for="motivo_ausencia"><i class="glyphicon glyphicon-triangle-right"></i> Motivo de Ausencia</label>
		</div>
		<div class="form-group floating-label">
			<select id="tipo_ausencia" name="tipo_ausencia" class="form-control dirty">

					<option value="">&nbsp;</option>
					{foreach item=tip from=$tipoAusencia}
						<option value="{$tip.pk_num_miscelaneo_detalle}">{$tip.ind_nombre_detalle}</option>
					{/foreach}
			</select>
			<label for="tipo_ausencia"><i class="glyphicon glyphicon-triangle-right"></i> Tipo de Ausencia</label>
		</div>
		<div class="form-group">
			<input type="text" class="form-control dirty" value="{$form.periodoContable}" disabled>
			<label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Periodo Contable</label>
		</div>
		<div class="form-group">
			<input type="text" class="form-control dirty" value="{$form.fechaIngreso}" disabled>
			<label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Fecha de Ingreso</label>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="form-group" style="width:100%">
			<div class="input-daterange input-group" id="demo-date-range">
				<div class="input-group-content">
					<input type="text" class="form-control dirty" name="fecha_inicio" id="fechaInicio1" value="{$form.fechaPermiso}" onChange="calcularTiempo()">
					<label><i class="glyphicon glyphicon-calendar"></i> Fecha del Permiso</label>
				</div>
				<span class="input-group-addon">al</span>
				<div class="input-group-content">
					<input type="text" class="form-control dirty" name="fecha_fin" id="fechaFin1" value="{$form.fechaPermiso}" onChange="calcularTiempo()">
					<div class="form-control-line"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 col-sm-8">
					<div class="form-group">
						<input type="text" name="hora_inicio" id="hora_inicio" class="form-control dirty" value="{$form.horaPermisoE}" data-rule-minlength="5" maxlength="5" onChange="calcularTiempo()">
						<label class="control-label"><i class="glyphicon glyphicon-time"></i> Hora Inicial</label>
					</div>
				</div>
				<div class="col-md-2 col-sm-3">
					<div class="form-group floating-label">
						<select name="horario_inicio" class="form-control dirty" onChange="calcularTiempo()" id="horario_inicio">
							<option value="{$form.horarioE}">{$form.horarioE}</option>
							{if $form.horarioE=='PM'}
								<option value="AM">AM</option>
							{else}
								<option value="PM">PM</option>
							{/if}
						</select>
						<label for="horario_inicio"></label>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="form-group">
						<input type="text" name="hora_fin" id="hora_fin" class="form-control dirty" data-rule-minlength="5" maxlength="5" value="{$form.horaPermisoS}" onChange="calcularTiempo()" id="hora_final">
						<label class="control-label">Hora Final</label>
					</div>
				</div>
				<div class="col-md-2 col-sm-3">
					<div class="form-group floating-label">
						<select name="horario_fin" class="form-control" id="horario_fin" onChange="calcularTiempo()">
							<option value="{$form.horarioS}">{$form.horarioS}</option>
							{if $form.horarioS=='PM'}
								<option value="AM">AM</option>
							{else}
								<option value="PM">PM</option>
							{/if}
						</select>
						<label for="pk_num_empleado"></label>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<div class="well clearfix">
				<table width="100%">
					<tr>
						<td class="text-primary"><h4>Total</h4></td>
					</tr>
					<tr>
						<td>Dias: <input type="text" id="totalDias" disabled size="7"/></td>
						<td>Horas: <input type="text" id="totalHoras" disabled size="7"/></td>
						<td>Minutos: <input type="text" id="totalMinutos" disabled size="7"/></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<div class="form-group">
				<textarea class="form-control dirty" rows="2" name="ind_motivo" id="ind_motivo"></textarea>
				<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Descripción del Motivo</label>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-sm-8">
		<div class="col-md-4 col-sm-6">
			<div class="checkbox checkbox-styled"><label><input type="checkbox" value="1" name="num_remunerado" id="guardar" checked disabled><span>¿Remunerado?</span></div>
		</div>
		<div class="col-md-4 col-sm-6">
			<div class="checkbox checkbox-styled"><label><input type="checkbox" value="1" name="num_justificativo" id="guardar" disabled><span>¿Entregar Justificativo?</span></div>
		</div>
		<div class="col-md-4 col-sm-6">
			<div class="checkbox checkbox-styled"><label><input type="checkbox" value="1" name="num_exento" id="guardar" disabled><span>¿Exento?</span></div>
		</div>
	</div>
	<div  class="col-md-12 col-sm-12">
		<br/>
		<div align="center">
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
			<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"> Enviar Solicitud</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$("#hora_inicio").inputmask("99:99");
		$("#hora_fin").inputmask("99:99");
	});
	$("#fechaInicio1").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
	$("#fechaFin1").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});

	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}

	function repoFormatSelection( repo ) {
		return repo.full_name;
	}

	$( "button[data-select2-open]" ).click( function() {
		$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
	});

	function calcularTiempo() {
		var urlComprueba = "{$_Parametros.url}modRH/gestion/permisoCONTROL/CalcularTiempoPermisoMET";
		var fechaInicial = $("#fechaInicio1").val();
		var fechaFinal = $("#fechaFin1").val();
		var horaInicio = $("#hora_inicio").val();
		var horaFin = $("#hora_fin").val();
		var horarioInicio = $("#horario_inicio").val();
		var horarioFin = $("#horario_fin").val();
		var pkNumEmpleado = $("#pk_num_empleado").val();
		$.post(urlComprueba,{ fechaInicio:""+fechaInicial, fechaFin:""+fechaFinal, horaInicio:""+horaInicio, horarioInicio:""+horarioInicio, horaFin:""+horaFin, horarioFin:""+horarioFin, pkNumEmpleado:""+pkNumEmpleado },function(dato){
			$("#totalDias").val(dato['diasDiferencia']);
			$("#totalHoras").val(dato['horaDiferencia']);
			$("#totalMinutos").val(dato['minutoDiferencia']);
		},'json');
	}
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				pk_num_empleado:{
					required:true
				},
				pk_num_empleado_aprueba:{
					required: true
				},
				motivo_ausencia:{
					required: true
				},
				tipo_ausencia:{
					required: true
				},
				hora_inicio:{
					required: true
				},
				horario_inicio:{
					required: true
				},
				hora_fin:{
					required: true
				},
				horario_fin:{
					required: true
				}
			},
			messages:{
				pk_num_empleado:{
					required: "Seleccione el empleado"
				},
				pk_num_empleado_aprueba:{
					required: "Seleccione el empleado que aprueba el permiso"
				},
				motivo_ausencia:{
					required: "Seleccione el motivo de ausencia"
				},
				tipo_ausencia:{
					required: "Seleccione el tipo de ausencia"
				},
				fecha_inicio:{
					required: "La fecha de inicio es requerida",
					date: "Formato de fecha inválido",
					remote: "Rango de fecha incorrecto"
				},
				fecha_fin:{
					required: "La fecha final es requerida",
					date: "Formato de fecha inválido",
					remote: "Rango de fecha incorrecto"
				},
				hora_inicio:{
					required: " "
				},
				horario_inicio:{
					required: " "
				},
				hora_fin:{
					required: " "
				},
				horario_fin:{
					required: " "
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
				    var estado = dato['ind_estado'];
                    $(document.getElementById('datatable1')).append('<tr id="pk_num_permiso'+dato['pk_num_permiso']+'">' +
						'<td>'+dato['pk_num_permiso']+'</td>'+
                        '<td>'+dato['nombreCompleto']+'</td>'+
                        '<td>'+dato['motivo_ausencia']+'</td>'+
                        '<td>'+dato['tipo_ausencia']+'</td>'+
                        '<td>'+dato['fecha_registro']+'</td>'+
                        '<td>PREPARADO</td>'+
                        '<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver permiso" titulo="Ver permiso" id="ver" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="glyphicon glyphicon-search"></i></button></td>'+
						'<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descipcion="El usuario ha modificado un permiso" title="Modificar permiso"  titulo="Modificar permiso" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="fa fa-edit"></i></button></td>'+
						'<td align="center"><button class="permiso logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver Permiso" titulo="Ver Permiso" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="md md-attach-file"></i></button></td>'+
						'</tr>');
                    swal("Permiso Guardado", "Permiso guardado satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});

	function borrarEmpleado(valor)
	{
		$("#empleado_aprueba").html("");
		$.post("{$_Parametros.url}modRH/gestion/permisoCONTROL/ValidarEmpleadoMET",{ pkNumEmpleado:""+valor }, function (dato) {
			$("#empleado_aprueba").html(dato);
		});
	}
</script>