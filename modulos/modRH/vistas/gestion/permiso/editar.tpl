<form action="{$_Parametros.url}modRH/gestion/permisoCONTROL/EditarPermisoMET" name="formAjax" id="formAjax" method="post"  class="form floating-label" role="form">
	<div class="modal-body">
		<input type="hidden" value="1" name="valido" />
		<input type="hidden" value="{$permiso.pk_num_empleado}" name="idEmpleado" />
		{if isset($permiso.pk_num_permiso)}
			<input type="hidden" value="{$permiso.pk_num_permiso}" name="pk_num_permiso" id="pk_num_permiso" />
		{/if}
		<h2 class="text-primary">&nbsp;Datos del Permiso</h2>
		<hr class="ruler-xl">
		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<input type="text" class="form-control" value="{$permiso.pk_num_permiso}">
				<label class="control-label"><i class="glyphicon glyphicon-calendar"></i> N° del Permiso</label>
			</div>
			<div class="form-group floating-label">
				<select id="pk_num_empleado" name="pk_num_empleado" class="select2-container form-control select2">
					<option value="{$permiso.pk_num_empleado}">{$permiso.ind_nombre1} {$permiso.ind_nombre2} {$permiso.ind_apellido1} {$permiso.ind_apellido2}</option>
				</select>
				<label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
			</div>
			<div class="form-group floating-label">
				<select id="pk_num_empleado_aprueba" name="pk_num_empleado_aprueba" class="select2-container form-control select2">
                    {foreach item=emp from=$empleado}
                        {if $emp.pk_num_empleado==$permiso.pk_num_empleado_aprueba}
							<option selected value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
                        {else}
							<option value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
                        {/if}
                    {/foreach}
				</select>
				<label for="pk_num_empleado_aprueba"><i class="glyphicon glyphicon-user"></i> Aprueba</label>
			</div>
			<div class="form-group floating-label">
				<select id="motivo_ausencia" name="motivo_ausencia" id="s2id_single" class="select2-container form-control select2">
					<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
						<option value="">&nbsp;</option>
						{foreach item=mot from=$motivoAusencia}
							{if $mot.pk_num_miscelaneo_detalle == $permiso.motivo_ausencia}
								<option selected value="{$mot.pk_num_miscelaneo_detalle}">{$mot.ind_nombre_detalle}</option>
							{else}
								<option value="{$mot.pk_num_miscelaneo_detalle}">{$mot.ind_nombre_detalle}</option>
							{/if}
						{/foreach}
				</select>
				<label for="motivo_ausencia"><i class="glyphicon glyphicon-triangle-right"></i> Motivo de Ausencia</label>
			</div>
			<div class="form-group floating-label">
				<select id="tipo_ausencia" name="tipo_ausencia" class="form-control">
					<option value="">&nbsp;</option>
					{foreach item=mot from=$tipoAusencia}
						{if $mot.pk_num_miscelaneo_detalle == $permiso.tipo_ausencia}
							<option selected value="{$mot.pk_num_miscelaneo_detalle}">{$mot.ind_nombre_detalle}</option>
						{else}
							<option value="{$mot.pk_num_miscelaneo_detalle}">{$mot.ind_nombre_detalle}</option>
						{/if}
					{/foreach}
				</select>
				<label for="tipo_ausencia"><i class="glyphicon glyphicon-triangle-right"></i> Tipo de Ausencia</label>
			</div>
			<div class="form-group">
				<input type="text" class="form-control" value="{$permiso.ind_periodo_contable}" disabled>
				<label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Periodo Contable</label>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="form-group" style="width:100%">
				<div class="form-group">
					<input type="text" class="form-control" value="{$permiso.fecha_ingreso}" disabled>
					<label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Fecha de Ingreso</label>
				</div>
				<div class="input-daterange input-group" id="demo-date-range">
					<div class="input-group-content">
						<input type="text" class="form-control" name="fecha_inicio" id="fechaInicio1" value="{$permiso.fecha_salida}" onChange="calcularTiempo()">
						<label><i class="glyphicon glyphicon-calendar"></i> Fecha del Permiso</label>
					</div>
					<span class="input-group-addon">al</span>
					<div class="input-group-content">
						<input type="text" class="form-control" name="fecha_fin" id="fechaFin1" value="{$permiso.fecha_entrada}" onChange="calcularTiempo()">
						<div class="form-control-line"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4 col-sm-8">
						<div class="form-group">
							<input type="text" name="hora_inicio" id="hora_inicio" class="form-control" value="{$datoHora.horaInicio}:{$datoHora.minutoInicio}" data-rule-minlength="5" maxlength="5" onChange="calcularTiempo()">
							<label class="control-label"><i class="glyphicon glyphicon-time"></i> Hora Inicial</label>
						</div>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="form-group floating-label">
							<select name="horario_inicio" class="form-control" onChange="calcularTiempo()" id="horario_inicio">
								<option value="{$datoHora.horarioInicio}">{$datoHora.horarioInicio}</option>
								{if {$datoHora.horarioInicio}=='PM'}
									<option value="AM">AM</option>
								{else}
									<option value="PM">PM</option>
								{/if}
							</select>
							<label for="horario_inicio"></label>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="form-group">
							<input type="text" name="hora_fin" id="hora_fin" class="form-control" data-rule-minlength="5" maxlength="5" value="{$datoHora.horaFin}:{$datoHora.minutoFin}" onChange="calcularTiempo()" id="hora_final">
							<label class="control-label">Hora Final</label>
						</div>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="form-group floating-label">
							<select name="horario_fin" class="form-control" id="horario_fin" onChange="calcularTiempo()">
								<option value="{$datoHora.horarioFin}">{$datoHora.horarioFin}</option>
								{if {$datoHora.horarioFin}=='PM'}
									<option value="AM">AM</option>
								{else}
									<option value="PM">PM</option>
								{/if}
							</select>
							<label for="horario_fin"></label>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="well clearfix">
					<table width="100%">
						<tr>
							<td class="text-primary"><h4>Total</h4></td>
						</tr>
						<tr>
							<td>Dias: <input type="text" id="totalDias" value="{$permiso.totalDia}" disabled size="7"/></td>
							<td>Horas: <input type="text" id="totalHoras" value="{$permiso.totalHora}" disabled size="7"/></td>
							<td>Minutos: <input type="text" id="totalMinutos" value="{$permiso.totalMinuto}" disabled size="7"/></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="form-group">
					<textarea class="form-control" rows="2" name="ind_motivo" id="ind_motivo">{ucfirst($permiso.ind_motivo)}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Descripción del Motivo</label>
				</div>
			</div>
		</div>
		<div class="col-md-8 col-sm-8">
			<div class="col-md-4 col-sm-6">
				<div class="checkbox checkbox-styled"><label><input type="checkbox" name="num_remunerado" id="guardar" disabled {if ($permiso.num_remuneracion==1)}checked{/if}><span>¿Remunerado?</span></div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="checkbox checkbox-styled"><label><input type="checkbox" name="num_justificativo" id="guardar" disabled {if ($permiso.num_justificativo==1)}checked{/if}><span>¿Entregar Justificativo?</span></div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="checkbox checkbox-styled"><label><input type="checkbox" name="num_exento" id="guardar" disabled {if ($permiso.num_exento==1)}checked{/if}><span>¿Exento?</span></div>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<h3 class="text-primary">&nbsp;Acceso</h3>
			<hr class="ruler-xl">
			{foreach item=ac from=$acceso}
				<div class="col-md-4 col-sm-6">
					<div class="form-group floating-label">
						<input type="text" class="form-control" value="{$ac.ind_usuario}" size="45%" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-user"></i> Último usuario</label>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="form-group floating-label">
						<input type="text" class="form-control" value="{$ac.fecha_modificacion}" size="45%" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Última fecha de modificación</label>
					</div>
				</div>
			{/foreach}
		</div>
		<div  class="col-md-12 col-sm-12">
			<div align="right">
				<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
				<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$("#hora_inicio").inputmask("99:99");
		$("#hora_fin").inputmask("99:99");
	});
	$("#fechaInicio1").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
	$("#fechaFin1").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}

	function repoFormatSelection( repo ) {
		return repo.full_name;
	}

	$( "button[data-select2-open]" ).click( function() {
		$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
	});
	function calcularTiempo() {
		var urlComprueba = "{$_Parametros.url}modRH/gestion/permisoCONTROL/CalcularTiempoPermisoMET";
		var fechaInicial = $("#fechaInicio1").val();
		var fechaFinal = $("#fechaFin1").val();
		var horaInicio = $("#hora_inicio").val();
		var horaFin = $("#hora_fin").val();
		var horarioInicio = $("#horario_inicio").val();
		var horarioFin = $("#horario_fin").val();
		var pkNumEmpleado = document.formAjax.pk_num_empleado.value;

		$.post(urlComprueba,{ fechaInicio:""+fechaInicial, fechaFin:""+fechaFinal, horaInicio:""+horaInicio, horarioInicio:""+horarioInicio, horaFin:""+horaFin, horarioFin:""+horarioFin, pkNumEmpleado:""+pkNumEmpleado },function(dato){
			$("#totalDias").val(dato['diasDiferencia']);
			$("#totalHoras").val(dato['horaDiferencia']);
			$("#totalMinutos").val(dato['minutoDiferencia']);
		},'json');
	}
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				motivo_ausencia:{
					required: true
				},
				tipo_ausencia:{
					required: true
				},
				fecha_inicio:{
					required: true
				},
				fecha_fin:{
					required: true
				},
				hora_inicio:{
					required: true
				},
				horario_inicio:{
					required: true
				},
				hora_fin:{
					required: true
				},
				horario_fin:{
					required: true
				}
			},
			messages:{
				motivo_ausencia:{
					required: "Seleccione el motivo de ausencia"
				},
				tipo_ausencia:{
					required: "Seleccione el tipo de ausencia"
				},
				fecha_inicio:{
					required: "Indique la fecha de inicio del permiso"
				},
				fecha_fin:{
					required: "Indique la fecha final del permmiso"
				},
				hora_inicio:{
					required: " "
				},
				horario_inicio:{
					required: " "
				},
				hora_fin:{
					required: " "
				},
				horario_fin:{
					required: " "
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr("action"), $(form).serialize(), function (dato) {
					var tipoListado = dato['tipoListado'];
					var url = '{$_Parametros.url}modRH/gestion/permisoCONTROL/ReportePermisoMET/?pk_num_permiso='+dato['pk_num_permiso'];
					$('#pk_num_permiso'+dato['pk_num_permiso']).remove();
					if(dato['ind_estado']=='PR'){
						var estado = 'PREPARADO';
					}
					if(dato['ind_estado']=='RE'){
						var estado = 'REVISADO';
					}
					if(dato['ind_estado']=='AP'){
						var estado = 'APROBADO';
					}
					if(dato['ind_estado']=='VE'){
						var estado = 'VERIFICADO';
					}
					if(dato['ind_estado']=='AN'){
						var estado = 'ANULADO';
					}
					if(tipoListado==1){
						$(document.getElementById('datatable1')).append('<tr id="pk_num_permiso'+dato['pk_num_permiso']+'">' +
								'<td>'+dato['pk_num_permiso']+'</td><td>'+dato['nombreCompleto']+'</td><td>'+dato['motivo_ausencia']+'</td><td>'+dato['tipo_ausencia']+'</td><td>'+dato['fecha_ingreso']+'</td><td>'+estado+'</td>' +
								'{if in_array('RH-01-01-06-02-01-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un permiso" titulo="Visualizar Permiso" title="Visualizar Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{else}<td></td>{/if}' +
								'{if in_array('RH-01-01-06-02-02-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha modificado un permiso" titulo="Modificar Permiso" title="Modificar Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{else}<td></td>{/if}' +
								'{if in_array('RH-01-01-06-02-03-RE',$_Parametros.perfil)}<td align="center"><a href="'+ url +'" target="_blank"><button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un permiso" titulo="Reporte de Permiso" title="Reporte de Permiso"><i class="md md-attach-file" style="color: #ffffff;"></i></button></a></td>{else}<td></td>{/if}</tr>');
					} else {
						$(document.getElementById('datatable1')).append('<tr id="pk_num_permiso'+dato['pk_num_permiso']+'">' +
								'<td>'+dato['pk_num_permiso']+'</td><td>'+dato['nombreCompleto']+'</td><td>'+dato['motivo_ausencia']+'</td><td>'+dato['tipo_ausencia']+'</td><td>'+dato['fecha_ingreso']+'</td><td>'+estado+'</td>' +
								'{if in_array('RH-01-01-06-03-08-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un permiso" titulo="Visualizar Permiso" title="Visualizar Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{else}<td></td>{/if}' +
								'{if in_array('RH-01-01-06-03-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha modificado un permiso" titulo="Modificar Permiso" title="Modificar Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{else}<td></td>{/if}' +
								'{if in_array('RH-01-01-06-03-05-RE',$_Parametros.perfil)}<td align="center"><a href="'+ url +'" target="_blank"><button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un permiso" titulo="Reporte de Permiso" title="Reporte de Permiso"><i class="md md-attach-file" style="color: #ffffff;"></i></button></a></td>{else}<td></td>{/if}</tr>');
					}
					swal("Permiso Modificado", "Permiso modificado exitosamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				}, 'json');
			}
		});
	});
</script>