<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idNivelacion}" name="idNivelacion"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>

                                <h4 class="text-primary text-center">Condiciones Anteriores</h4>
                                <input type="hidden" value="{$dataAnterior.ind_estatus}" name="form[int][ind_estatus]"/>
                                <div class="well clearfix">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="organismo" name="form[int][organismo]" class="form-control input-sm" disabled>
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoOrganismos}
                                                    {if isset($dataAnterior.fk_a001_num_organismo)}
                                                        {if $dat.pk_num_organismo==$dataAnterior.fk_a001_num_organismo}
                                                            <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="organismo">Organismo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="cargo" name="form[int][cargo]" class="form-control input-sm" disabled>
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoCargos}
                                                    {if isset($dataAnterior.fk_rhc063_num_puestos_cargo)}
                                                        {if $dat.pk_num_puestos==$dataAnterior.fk_rhc063_num_puestos_cargo}
                                                            <option selected value="{$dat.pk_num_puestos}">{$dat.ind_descripcion_cargo}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_puestos}">{$dat.ind_descripcion_cargo}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_puestos}">{$dat.ind_descripcion_cargo}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="cargo">Cargo</label>
                                            <input type="hidden" name="form[int][cargoAnterior]" id="cargoAnterior" value="{$dataAnterior.fk_rhc063_num_puestos_cargo}" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="dependencia" name="form[int][dependencia]" class="form-control input-sm" disabled>
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoDependencias}
                                                    {if isset($dataAnterior.fk_a004_num_dependencia)}
                                                        {if $dat.pk_num_dependencia==$dataAnterior.fk_a004_num_dependencia}
                                                            <option selected value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="dependencia">Dependencia</label>
                                            <input type="hidden" name="form[int][dependenciaAnterior]" id="dependenciaAnterior" value="{$dataAnterior.fk_a004_num_dependencia}" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a006_num_miscelaneo_detalle_paso1" name="form[int][fk_a006_num_miscelaneo_detalle_paso]" class="form-control input-sm" required data-msg-required="Seleccione el Paso" disabled>
                                                {foreach item=i from=$Pasos}
                                                    {if isset($dataAnterior.fk_a006_num_miscelaneo_detalle_paso)}
                                                        {if $i.pk_num_miscelaneo_detalle==$dataAnterior.fk_a006_num_miscelaneo_detalle_paso}
                                                            <option selected value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                        {else}
                                                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a006_num_miscelaneo_detalle_paso">Paso</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="tipo_nomina" name="form[int][tipo_nomina]" class="form-control input-sm" disabled>
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoTipoNomina}
                                                    {if isset($dataAnterior.fk_nmb001_num_tipo_nomina)}
                                                        {if $dat.pk_num_tipo_nomina==$dataAnterior.fk_nmb001_num_tipo_nomina}
                                                            <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="tipo_nomina">Tipo Nomina</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="categoria_a" name="form[txt][categoria]" value="{if isset($categoria)}{$categoria}{/if}" disabled>
                                            <label for="categoria">Categoria</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="centro_costo" name="form[int][centro_costo]" class="form-control input-sm" disabled>
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoCentroCostos}
                                                    {if isset($dataAnterior.fk_a023_num_centro_costo)}
                                                        {if $dat.pk_num_centro_costo==$dataAnterior.fk_a023_num_centro_costo}
                                                            <option selected value="{$dat.pk_num_centro_costo}">{$dat.ind_descripcion_centro_costo}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_centro_costo}">{$dat.ind_descripcion_centro_costo}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_centro_costo}">{$dat.ind_descripcion_centro_costo}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="centro_costo">Centro de Costo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="sueldo_a" name="sueldo" value="{if isset($dataAnteriorSueldo.num_sueldo_minimo)}{$dataAnteriorSueldo.num_sueldo_minimo|number_format:2:",":"."}{/if}" disabled>
                                            <label for="sueldo">Sueldo</label>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <h4 class="text-primary text-center">Nuevas Condiciones</h4>

                                <div class="well clearfix">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoOrganismos}
                                                    {if isset($dataAnterior.fk_a001_num_organismo)}
                                                        {if $dat.pk_num_organismo==$dataAnterior.fk_a001_num_organismo}
                                                            <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a001_num_organismo">Organismo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_rhc063_num_puestos" name="form[int][fk_rhc063_num_puestos]" class="form-control input-sm select2-list select2">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoCargos}
                                                    {if isset($dataAnterior.fk_rhc063_num_puestos_cargo)}
                                                        {if $dat.pk_num_puestos==$dataAnterior.fk_rhc063_num_puestos_cargo}
                                                            <option selected value="{$dat.pk_num_puestos}">{$dat.ind_descripcion_cargo}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_puestos}">{$dat.ind_descripcion_cargo}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_puestos}">{$dat.ind_descripcion_cargo}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_rhc063_num_puestos">Cargo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a004_num_dependencia" name="form[int][fk_a004_num_dependencia]" class="form-control input-sm">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoDependencias}
                                                    {if isset($dataAnterior.fk_a004_num_dependencia)}
                                                        {if $dat.pk_num_dependencia==$dataAnterior.fk_a004_num_dependencia}
                                                            <option selected value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a004_num_dependencia">Dependencia</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a006_num_miscelaneo_detalle_paso" name="form[int][fk_a006_num_miscelaneo_detalle_paso]" class="form-control input-sm" required data-msg-required="Seleccione el Paso">
                                                {foreach item=i from=$Pasos}
                                                    {if isset($dataAnterior.fk_a006_num_miscelaneo_detalle_paso)}
                                                        {if $i.pk_num_miscelaneo_detalle==$dataAnterior.fk_a006_num_miscelaneo_detalle_paso}
                                                            <option selected value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                        {else}
                                                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a006_num_miscelaneo_detalle_paso">Paso</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control input-sm">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoTipoNomina}
                                                    {if isset($dataAnterior.fk_nmb001_num_tipo_nomina)}
                                                        {if $dat.pk_num_tipo_nomina==$dataAnterior.fk_nmb001_num_tipo_nomina}
                                                            <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_nmb001_num_tipo_nomina">Tipo Nomina</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="categoria" name="form[txt][categoria]" value="{if isset($categoria)}{$categoria}{/if}" disabled>
                                            <label for="categoria">Categoria</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a023_num_centro_costo" name="form[int][fk_a023_num_centro_costo]" class="form-control input-sm">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoCentroCostos}
                                                    {if isset($dataAnterior.fk_a023_num_centro_costo)}
                                                        {if $dat.pk_num_centro_costo==$dataAnterior.fk_a023_num_centro_costo}
                                                            <option selected value="{$dat.pk_num_centro_costo}">{$dat.ind_descripcion_centro_costo}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_centro_costo}">{$dat.ind_descripcion_centro_costo}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_centro_costo}">{$dat.ind_descripcion_centro_costo}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a023_num_centro_costo">Centro de Costo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="sueldo" name="sueldo" value="{if isset($dataAnteriorSueldo.num_sueldo_minimo)}{$dataAnteriorSueldo.num_sueldo_minimo|number_format:2:",":"."}{/if}" disabled>
                                            <input type="hidden" class="form-control input-sm" id="sueldoF" name="form[int][sueldo]" value="{if isset($dataAnterior.num_sueldo_minimo)}{$dataAnterior.num_sueldo_minimo|number_format:2:",":"."}{/if}">
                                            <label for="sueldo">Sueldo</label>
                                        </div>
                                    </div>
                                </div>
                                </div>


                                <h4 class="text-primary text-center">Sustento</h4>
                                <div class="well clearfix">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                                 <input type="text" class="form-control input-sm" name="form[txt][fec_fecha_registro]" id="fec_fecha_registro" required data-msg-required="Indique Fecha">
                                                 <label for="fec_fecha_registro">Fecha</label>
                                                 <p class="help-block"><span class="text-xs" style="color: red">* (dd-mm-yyyy)</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a006_num_miscelaneo_detalle_tipoaccion" name="form[txt][fk_a006_num_miscelaneo_detalle_tipoaccion]" class="form-control input-sm" required data-msg-required="Indique Tipo de Acción">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$TipoAccion}
                                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipoaccion)}
                                                        {if $dat.pk_num_miscelaneo_detalle==$v.fk_a006_num_miscelaneo_detalle_tipoaccion}
                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <input type="hidden" name="form[txt][accion]" id="accion">
                                            <label for="fk_a006_num_miscelaneo_detalle_tipoaccion">Tipo de Acción</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="ind_motivo" name="form[txt][ind_motivo]" value="" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_motivo">Motivo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="ind_documento" name="form[txt][ind_documento]" value="" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_documento">Documento</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="txt_observaciones" name="form[txt][txt_observaciones]" value="" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="txt_observaciones">Observaciones Generales</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fk_rhb001_num_empleado_responsable">Responsable</label>
                                            <select id="fk_rhb001_num_empleado_responsable" name="form[int][fk_rhb001_num_empleado_responsable]" class="form-control select2-list select2">
                                                <option value="">Seleccione...</option>
                                                {foreach item=i from=$Empleado}
                                                    {if isset($formDB.fk_rhb001_num_empleado_responsable)}
                                                        {if $i.pk_num_empleado==$formDB.fk_rhb001_num_empleado_responsable}
                                                            <option selected value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                                        {else}
                                                            <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //inicializar el datepicker
    //inicializar el datepicker
    $("#fec_fecha_registro").datepicker({
        format:'dd-mm-yyyy',
        language:'es',
        autoclose: true
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();

            $.post('{$_Parametros.url}modRH/gestion/nivelacionesCONTROL/NivelacionesMET', datos ,function(dato){

                if(dato['status']=='error'){

                    /*Error para usuarios normales del sistema*/
                    //swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                }else if(dato['status']=='creacion'){


                       $(document.getElementById('datatable2')).append('<tr style="font-size: 11px">' +
                                '<td width="5%">'+dato['fec_fecha_registro']+'</td>' +
                                '<td width="20%">'+dato['ind_tipo_accion']+'</td>' +
                                '<td width="20%">'+dato['ind_dependencia']+'</td>' +
                                '<td width="20%">'+dato['ind_cargo']+'</td>' +
                                '<td width="15%">'+dato['ind_categoria_cargo']+'</td>' +
                                '<td width="15%">'+dato['ind_tipo_nomina']+'</td>' +
                                '</tr>');
                        swal("Registro Guardado!", "Nivelación Realizada Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                }

            },'json');

        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        var rh = new  ModRhFunciones();

        var idDependencia = '{if isset($dataAnterior.fk_a004_num_dependencia)}{$dataAnterior.fk_a004_num_dependencia}{/if}';
        var idCentroCosto = '{if isset($dataAnterior.fk_a023_num_centro_costo)}{$dataAnterior.fk_a023_num_centro_costo}{/if}';

        rh.metJsonCentroCosto('{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonCentroCostoMET',idDependencia,idCentroCosto);

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "65%" );

        $("#contenidoModal2").css({ 'border' : '2px solid #0aa89e' });
        $("#headerModal2").css({ 'background-color':'#0aa89e','color' : '#FFFFFF' });


        //AL SELECIONAR CARGO (SELECT) PARA VISUALIZAR LA CATEGORIA DEL CARGO Y EL SUELDO
        $("#fk_rhc063_num_puestos").change(function () {
            $("#fk_rhc063_num_puestos option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/gestion/empleadosCONTROL/ObtenerSueldoBasicoMET", { idCargo: $('#fk_rhc063_num_puestos').val() }, function(data){
                    $("#categoria").val(data['ind_nombre_detalle']);
                    $("#sueldo").val(data['num_sueldo_basico']);
                    $("#sueldoF").val(data['num_sueldo_basico']);
                    $("#fk_a006_num_miscelaneo_detalle_paso").val(data['paso']);
                    if(data['cod_detalle']=='NA'){
                        $("#fk_a006_num_miscelaneo_detalle_paso").prop('disabled',true);
                    }else{
                        $("#fk_a006_num_miscelaneo_detalle_paso").prop('disabled',false);
                    }
                },'json');
            });
        });

        //al seleccionar el paso
        $("#fk_a006_num_miscelaneo_detalle_paso").change(function () {

            $("#fk_a006_num_miscelaneo_detalle_paso option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/cargosCONTROL/ActualizarSueldoCargoPasosMET", { Cargo: $('#fk_rhc063_num_puestos').val(), Paso: $('#fk_a006_num_miscelaneo_detalle_paso').val() }, function(data){

                    $("#sueldo").val(data);
                    $("#sueldoF").val(data);

                },'json');

            });

        });
        /*****************DESABILITADO
        $("#fk_rhc063_num_puestos").change(function () {
            $("#fk_rhc063_num_puestos option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/gestion/empleadosCONTROL/ObtenerSueldoBasicoMET", { idCargo: $('#fk_rhc063_num_puestos').val() }, function(data){
                    $("#categoria").val(data['ind_nombre_detalle']);
                    $("#sueldo").val(data['num_sueldo_basico']);
                },'json');
            });
        });
        *****************/

        //AL SELECIONAR TIPO DE ACCION
        $("#fk_a006_num_miscelaneo_detalle_tipoaccion").change(function () {
            $("#fk_a006_num_miscelaneo_detalle_tipoaccion option:selected").each(function () {
                var accion = $("#fk_a006_num_miscelaneo_detalle_tipoaccion").val();
                var detalle = accion.split("-");
                $("#accion").val(detalle[1]);
            });
        });




    });






</script>