{if $Empleado==0}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->


    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-12">
            Empleado: {$Empleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>
    <!-- LISTADO DE DOCUMENTOS -->
    <div class="card">
        <div class="card-body">
            <input type="hidden" name="idEmpleado" id="idEmpleado" value="{$Empleado}">
            <table id="datatable2" class="table table-striped no-margin table-condensed">
                <thead>
                <tr>
                    <th width="14%">Documento</th>
                    <th width="20%">Persona</th>
                    <th width="5%">Entregado</th>
                    <th width="8%">Fecha</th>
                    <th width="20%">Observaciones</th>
                    <th width="5%">Mod</th>
                    <th width="5%">Eli</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=dat from=$listadoDocumentos}
                    <tr id="idDocumento{$dat.pk_num_empleado_documento}" valor="{$dat.pk_num_empleado_documento}" style="font-size: 11px">
                        <td width="14%">{$dat.documento}</td>
                        <td width="20%">{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}</td>
                        <td width="5%"><i class="{if $dat.num_flag_entregado==1}md md-check{else}md md-not-interested{/if}"></i></td>
                        <td width="8%">{$dat.fec_entrega|date_format:"d-m-Y"}</td>
                        <td width="20%">{$dat.txt_observaciones}</td>
                        <td width="5%">
                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                    data-keyboard="false" data-backdrop="static" title="Editar Documentos" idDocumento="{$dat.pk_num_empleado_documento}"
                                    descipcion="El Usuario a Modificado Documento del Empleado" titulo="Modificar Documentos">
                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                            </button>
                        </td>
                        <td width="5%">
                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idDocumento="{$dat.pk_num_empleado_documento}"
                                    descipcion="El usuario a Eliminado Documento del Empleado" title="Eliminar Documentos" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Documento!!">
                                <i class="md md-delete" style="color: #ffffff;"></i>
                            </button>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                    <th colspan="7">
                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                descripcion="el Usuario a Registrado Documento del Empleado" title="Nuevo Documento"  titulo="Nuevo Documento" id="nuevoDocumento" >
                            <i class="md md-create"></i> Nuevo Documento &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                    </th>
                </tfoot>
            </table>
        </div>
    </div>


{/if}


<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "70%" );

        //inicializo el datatbales
        var tabla = $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        var $url='{$_Parametros.url}modRH/gestion/documentosCONTROL/DocumentosMET';



        //al dar click en nuevo documento
        $('#nuevoDocumento').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });

        //al dar click boton modificar del listado de documentos
        $('#datatable2 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idDocumento: $(this).attr('idDocumento'), idEmpleado: $("#idEmpleado").val()},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        //al dar click boton eliminar del listado de documentos
        $('#datatable2 tbody').on( 'click', '.eliminar', function () {

            var idDocumento = $(this).attr('idDocumento');

            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/documentosCONTROL/EliminarDocumentosMET';
                $.post($url, { idDocumento: idDocumento},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idDocumento'+$dato['idDocumento'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });


    });

</script>