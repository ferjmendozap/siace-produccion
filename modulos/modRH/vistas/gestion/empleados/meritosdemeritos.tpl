{if $Empleado==0}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->


    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-12">
            Empleado: {$Empleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <input type="hidden" name="idEmpleado" id="idEmpleado" value="{$Empleado}">
            <table id="datatable2" class="table table-striped no-margin table-condensed">
                <thead>
                <tr>
                    <th width="20%">Tipo</th>
                    <th width="20%">Clasificación</th>
                    <th width="10%">Fecha</th>
                    <th width="15%">Documento</th>
                    <th width="5%">Mod</th>
                    <th width="5%">Eli</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=dat from=$listadoMeritosDemeritos}
                    <tr id="idMeritoDemerito{$dat.pk_num_meritos_demeritos}" valor="{$dat.pk_num_meritos_demeritos}" style="font-size: 11px">
                        <td width="20%">{$dat.tipo_merdem}</td>
                        <td width="20%">{$dat.merdem}</td>
                        <td width="10%">{$dat.fec_documento|date_format:"d-m-Y"}</td>
                        <td width="15%">{$dat.ind_documento}</td>
                        <td width="5%">
                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                    data-keyboard="false" data-backdrop="static" title="Editar" idMeritoDemerito="{$dat.pk_num_meritos_demeritos}"
                                    descipcion="El Usuario a Modificado Meritos Demeritos" titulo="Modificar Meritos Demeritos">
                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                            </button>
                        </td>
                        <td width="5%">
                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idMeritoDemerito="{$dat.pk_num_meritos_demeritos}"
                                    descipcion="El usuario a eliminado Meritos Demeritos" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Merito Demerito!!">
                                <i class="md md-delete" style="color: #ffffff;"></i>
                            </button>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                    <th colspan="6">
                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                descripcion="el Usuario a creado Meritos y Demeritos" title="Nuevo Merito/Demerito" titulo="Nuevo Merito/Demerito" id="nuevoMeritoDemerito" >
                            <i class="md md-create"></i> Nuevo Merito/Demeritos &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                    </th>
                </tfoot>
            </table>
        </div>
    </div>
{/if}




<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "70%" );

        //inicializo el datatbales
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        var $url='{$_Parametros.url}modRH/gestion/meritosDemeritosCONTROL/MeritosDemeritosMET';

        //al dar click en nueva carga
        $('#nuevoMeritoDemerito').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });

        //al dar click boton modificar del listado de carga familiar
        $('#datatable2 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idMeritoDemerito: $(this).attr('idMeritoDemerito'), idEmpleado: $("#idEmpleado").val()},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        //al dar click boton eliminar del listado de carga familiar
        $('#datatable2 tbody').on( 'click', '.eliminar', function () {

            var idMeritoDemerito=$(this).attr('idMeritoDemerito');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/meritosDemeritosCONTROL/EliminarMeritosDemeritosMET';
                $.post($url, { idMeritoDemerito: idMeritoDemerito},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idMeritoDemerito'+$dato['idMeritoDemerito'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

    });

</script>