{if $Empleado==0}
<!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
<!-- no se ha seleccionado ningun registro se muestra advertencia -->



    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-1">
            Interno: {$Empleado}
        </div>
        <div class="col-sm-11">
            Empleado: {$CodEmpleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>




    <div class="card">

        <div class="card-body">

            <table id="datatableExt" class="table table-striped no-margin table-condensed">
                <thead>
                <tr>
                    <th>Año</th>
                    <th>Beneficio</th>
                    <th>Asignación</th>
                    <th>Monto Orig</th>
                    <th>Incremento</th>
                    <th>Asig. Disminuida</th>
                    <th>Monto Orig</th>
                    <th>Disminución</th>
                    <th>Final</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <!--
                <tbody>
                {foreach item=dat from=$listadoOperaciones}

                    <tr id="idOperacion{$dat.pk_num_extension}" valor="{$dat.pk_num_extension}" style="font-size: 11px">
                        <td>{$dat.fec_anio}</td>
                        <td>{$dat.ind_descripcion}</td>
                        <td>{$dat.descripcion_p}</td>
                        <td>{$dat.num_monto_predeterminado|number_format:2:",":"."}</td>
                        <td>{$dat.num_monto_final|number_format:2:",":"."}</td>
                        <td>{$dat.descripcion_d}</td>
                        <td>{$dat.num_monto_disminucion_predeterminado|number_format:2:",":"."}</td>
                        <td>{$dat.num_monto_disminucion|number_format:2:",":"."}</td>
                        <td>{$dat.num_monto_disminucion_final|number_format:2:",":"."}</td>
                        <td>
                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                    data-keyboard="false" data-backdrop="static" title="Editar Carga Familiar" idCargaFamiliar="{$dat.pk_num_carga}"
                                    descipcion="El Usuario a Modificado Carga Familiar" titulo="Modificar Carga Familiar">
                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                            </button>
                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idCargaFamiliar="{$dat.pk_num_carga}"
                                    descipcion="El usuario a eliminado Carga Familiar" title="Eliminar Carga Familiar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Carga Familiar!!">
                                <i class="md md-delete" style="color: #ffffff;"></i>
                            </button>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
                -->
                <tfoot>
                    <th colspan="10">
                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                title="HCM Registro de Extensión"  titulo="HCM Registro de Extensión" id="nuevaExt" >
                            <i class="md md-create"></i> Nueva Extensión &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                    </th>
                </tfoot>
            </table>

        </div>

    </div>

{/if}


<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "80%" );

        var app = new AppFunciones();
        var dt = app.dataTable(
                '#datatableExt',
                "{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonDataTablaExtensionesMET/{$Empleado}",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "fec_anio" },
                    { "data": "ind_descripcion" },
                    { "data": "descripcion_p" },
                    { "data": "num_monto_predeterminado" },
                    { "data": "num_monto_final" },
                    { "data": "descripcion_d" },
                    { "data": "num_monto_disminucion_predeterminado" },
                    { "data": "num_monto_operacion" },
                    { "data": "num_monto_disminucion_final" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

        var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/HcmControlMET';

        //al dar click en nueva carga
        $('#nuevaExt').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idEmpleado:{$Empleado} },function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });

        //al dar click boton modificar del listado de carga familiar
        $('#datatableExt tbody').on( 'click', '.modificarExt', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idExtension: $(this).attr('idExtension'), idEmpleado:{$Empleado} },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });


    });

</script>