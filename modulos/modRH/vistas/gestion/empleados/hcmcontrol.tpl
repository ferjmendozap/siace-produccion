<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{$idExtension}" name="idExtension" id="idExtension"/>

                                    <div class="row">

                                        <!-- izquierda -->
                                        <div class="col-md-5">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="fec_anio" maxlength="4" name="form[txt][fec_anio]" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{$anio}{/if}" data-rule-number="true" required data-msg-required="Introduzca Año" data-msg-number="Por favor Introduzca solo numeros">
                                                    <label for="fec_anio">Año</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select id="fk_rhb008_ayuda_global" name="form[int][fk_rhb008_ayuda_global]" class="form-control input-sm" required data-msg-required="Seleccione Beneficio">
                                                        <option value="">Seleccione...</option>
                                                        {foreach item=dat from=$listadoBeneficioGlobal}
                                                            {if isset($formDB.fk_rhb008_ayuda_global)}
                                                                {if $dat.pk_num_ayuda_global==$formDB.fk_rhb008_ayuda_global}
                                                                    <option selected value="{$dat.pk_num_ayuda_global}">{$dat.ind_descripcion}</option>
                                                                {else}
                                                                    <option value="{$dat.pk_num_ayuda_global}">{$dat.ind_descripcion}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$dat.pk_num_ayuda_global}">{$dat.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="fk_rhb008_ayuda_global">Beneficio</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select id="fk_rhc068_ayuda_especifica" name="form[int][fk_rhc068_ayuda_especifica]" class="form-control input-sm" required data-msg-required="Seleccione Asignacion">
                                                        <option value="">Seleccione...</option>
                                                        {foreach item=dat from=$listadoAsignaciones}
                                                            {if isset($formDB.fk_rhc068_ayuda_especifica)}
                                                                {if $dat.pk_num_ayuda_especifica==$formDB.fk_rhc068_ayuda_especifica}
                                                                    <option selected value="{$dat.pk_num_ayuda_especifica}">{$dat.ind_descripcion_especifica}</option>
                                                                {else}
                                                                    <option value="{$dat.pk_num_ayuda_especifica}">{$dat.ind_descripcion_especifica}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$dat.pk_num_ayuda_especifica}">{$dat.ind_descripcion_especifica}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="fk_rhc068_ayuda_especifica">Asignación</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="num_monto_predeterminado"  name="form[int][num_monto_predeterminado]" value="{if isset($formDB.num_monto_predeterminado)}{$formDB.num_monto_predeterminado|number_format:2:",":"."}{/if}" required data-msg-required="Monto Asignado">
                                                    <label for="num_monto_predeterminado">Monto Asignado</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- centro -->
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>

                                        <!-- derecha -->
                                        <div class="col-md-6">

                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary">
                                                        <header>Asignación  a Disminuir</header>
                                                    </div><!--end .card-head -->
                                                    <div class="card-body">

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <select id="fk_rhc068_ayuda_especifica_disminucion" name="form[int][fk_rhc068_ayuda_especifica_disminucion]" class="form-control input-sm" required data-msg-required="Seleccione Asignacion a Disminuir">
                                                                    <option value="">Seleccione...</option>
                                                                    {foreach item=dat from=$listadoAsignaciones}
                                                                        {if isset($formDB.fk_rhc068_ayuda_especifica_disminucion)}
                                                                            {if $dat.pk_num_ayuda_especifica==$formDB.fk_rhc068_ayuda_especifica_disminucion}
                                                                                <option selected value="{$dat.pk_num_ayuda_especifica}">{$dat.ind_descripcion_especifica}</option>
                                                                            {else}
                                                                                <option value="{$dat.pk_num_ayuda_especifica}">{$dat.ind_descripcion_especifica}</option>
                                                                            {/if}
                                                                        {else}
                                                                            <option value="{$dat.pk_num_ayuda_especifica}">{$dat.ind_descripcion_especifica}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="fk_rhc068_ayuda_especifica_disminucion">Seleccione Asignación a Disminuir</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control input-sm" id="num_monto_disminucion_predeterminado"  name="form[int][num_monto_disminucion_predeterminado]" value="{if isset($formDB.num_monto_disminucion_predeterminado)}{$formDB.num_monto_disminucion_predeterminado|number_format:2:",":"."}{/if}" required data-msg-required="Monto Asignado">
                                                                <label for="num_monto_disminucion_predeterminado">Monto Asignado</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control input-sm" id="num_monto_disponible"  name="form[int][num_monto_disponible]" value="{if isset($formDB.num_monto_disponible)}{$formDB.num_monto_disponible|number_format:2:",":"."}{/if}" required data-msg-required="Monto Disponible">
                                                                <label for="num_monto_disponible">Disponible</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control input-sm" id="num_monto_operacion"  name="form[int][num_monto_operacion]" value="{if isset($formDB.num_monto_operacion)}{$formDB.num_monto_operacion|number_format:2:",":"."}{/if}" required data-msg-required="Monto Disminución">
                                                                <label for="num_monto_operacion">Monto a Disminuir</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                            </div>
                                                        </div>

                                                    </div><!--end .card-body -->
                                                </div>
                                            </div>

                                        </div>





                                    </div><!--end .row -->

                                    <div class="row">
                                        <br><br>
                                        <div align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                                        </div>
                                    </div>


                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            var app = new  AppFunciones();

            $.post('{$_Parametros.url}modRH/gestion/empleadosCONTROL/HcmControlMET', datos ,function(dato){

                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('datatableExt','Operación Realizada Satisfactoriamente.','cerrarModal2','ContenidoModal2');
                }else if(dato['status']=='creacion'){
                    app.metNuevoRegistroTablaJson('datatableExt','Operación Realizada Satisfactoriamente.','cerrarModal2','ContenidoModal2');
                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "50%" );

        $("#num_monto_operacion").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });

        //AL SELECIONAR EL BENEFICIO (SELECT)
        $("#fk_rhb008_ayuda_global").change(function () {

            $("#fk_rhb008_ayuda_global option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/beneficiosHcmCONTROL/ListarAsignacionesExtensiblesMET", { idBeneficioGlobal: $('#fk_rhb008_ayuda_global').val() }, function(data){
                    var json = data,
                            obj = JSON.parse(json);
                    $("#fk_rhc068_ayuda_especifica").html(obj);
                });

            });

        });


        //AL SELECIONAR EL BENEFICIO (SELECT)
        $("#fk_rhc068_ayuda_especifica").change(function () {

            $("#fk_rhc068_ayuda_especifica option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/beneficiosHcmCONTROL/MontoAsignadoMET", { idAsignacion: $('#fk_rhc068_ayuda_especifica').val() }, function(data){
                    $("#num_monto_predeterminado").val(data);
                },'json');

            });

        });

        //AL SELECIONAR EL BENEFICIO (SELECT)
        $("#fk_rhc068_ayuda_especifica_disminucion").change(function () {

            $("#fk_rhc068_ayuda_especifica_disminucion option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/maestros/beneficiosHcmCONTROL/MontoAsignadoMET", { idAsignacion: $('#fk_rhc068_ayuda_especifica_disminucion').val() }, function(data){
                    $("#num_monto_disminucion_predeterminado").val(data);
                },'json');

                $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/DisponibleAsignacionMET", { idEmpleado:$('#idEmpleado').val(), idBeneficioGlobal: $('#fk_rhb008_ayuda_global').val(), idAsignacion: $('#fk_rhc068_ayuda_especifica_disminucion').val(), monto_asignado:$('#num_monto_predeterminado').val() }, function(data){
                    $("#num_monto_disponible").val(data);
                },'json');

            });

        });


    });






</script>