{if $Empleado==0}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->


    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-12">
            Empleado: {$Empleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>

    <!--PARA VISUALIZAR MONTOS DE LOS PATRIMONIOS-->
    <div class="row">
        <div class="col-sm-12">
            <form class="form">
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" id="inmuebleT" name="inmuebleT" value="{$montosPatrimonio[0]|number_format:2:",":"."}" disabled>
                        <label>Inmueble</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" id="inversionT" name="inversionT" value="{$montosPatrimonio[1]|number_format:2:",":"."}" disabled>
                        <label>Inversión</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" id="vehiculoT" name="vehiculoT" value="{$montosPatrimonio[2]|number_format:2:",":"."}" disabled>
                        <label>Vehiculo</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" id="cuentaT" name="cuentaT" value="{$montosPatrimonio[3]|number_format:2:",":"."}" disabled>
                        <label>Cuentas</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" id="otrosT" name="otrosT" value="{$montosPatrimonio[4]|number_format:2:",":"."}" disabled>
                        <label>Otros</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" id="total" name="total" value="{$montosPatrimonio[5]|number_format:2:",":"."}" disabled>
                        <label>Total</label>
                    </div>
                </div>
            </form>
        </div>
     </div>

    <!-- TABS DE PATRIMONIOS -->
    <div class="card">
        <div class="card-head">
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#inmuebles">INMUEBLES</a></li>
                <li><a href="#inversion">INVERSIÓN</a></li>
                <li><a href="#vehiculo">VEHÍCULOS</a></li>
                <li><a href="#cuentas">CUENTAS</a></li>
                <li><a href="#otros">OTROS</a></li>
            </ul>
        </div><!--end .card-head -->
        <div class="card-body tab-content">

            <!--INMUEBLES-->
            <div class="tab-pane active" id="inmuebles">


                <table id="datatableInmuebles" class="table table-striped no-margin table-condensed">
                    <thead>
                    <tr>
                        <th width="25%">Descripción</th>
                        <th width="25%">Ubicación</th>
                        <th width="25%">Uso</th>
                        <th width="15%">Valor</th>
                        <th width="5%">Mod</th>
                        <th width="5%">Eli</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$listadoInmueble}

                        <tr id="idPatrimonioInmueble{$dat.pk_num_patrimonio_inmueble}" valor="{$dat.pk_num_patrimonio_inmueble}" style="font-size: 11px">
                            <td width="25%">{$dat.ind_descripcion}</td>
                            <td width="25%">{$dat.ind_ubicacion}</td>
                            <td width="25%">{$dat.ind_uso}</td>
                            <td width="15%">{$dat.num_valor|number_format:2:",":"."}</td>
                            <td width="5%">
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static" title="Editar" idPatrimonioInmueble="{$dat.pk_num_patrimonio_inmueble}"
                                        descipcion="El Usuario a Modificado Patrimonio Inmueble" titulo="Modificar Registro de Patrimonio Inmueble">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            <td width="5%">
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idPatrimonioInmueble="{$dat.pk_num_patrimonio_inmueble}"
                                        descipcion="El usuario a eliminado Patrimonio Inmueble" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Inmueble!!">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <th colspan="6">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                    descripcion="el Usuario a creado un Patrimonio Inmueble" title="Nuevo Registro de Patrimonio"  titulo="Nuevo Registro de Patrimonio" id="nuevoPatrimonioInmueble" >
                                <i class="md md-create"></i> Nuevo Inmueble &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        </th>
                    </tfoot>
                </table>
            </div>

            <!--INVERSIÓN-->
            <div class="tab-pane" id="inversion">


                <table id="datatableInversion" class="table table-striped no-margin table-condensed">
                    <thead>
                    <tr>
                        <th width="25%">Titular</th>
                        <th width="25%">Remitente</th>
                        <th width="18%">Certificado</th>
                        <th width="12%">Valor Nominal</th>
                        <th width="10%">Valor</th>
                        <th width="5%">Mod</th>
                        <th width="5%">Eli</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$listadoInversion}

                        <tr id="idPatrimonioInversion{$dat.pk_num_patrimonio_inversion}" valor="{$dat.pk_num_patrimonio_inversion}" style="font-size: 11px">
                            <td width="25%">{$dat.ind_titular}</td>
                            <td width="25%">{$dat.ind_remitente}</td>
                            <td width="18%">{$dat.ind_certificado}</td>
                            <td width="12%">{$dat.num_valor_nominal|number_format:2:",":"."}</td>
                            <td width="10%">{$dat.num_valor|number_format:2:",":"."}</td>
                            <td width="5%">
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static" title="Editar" idPatrimonioInversion="{$dat.pk_num_patrimonio_inversion}"
                                        descipcion="El Usuario a Modificado Patromonio Inversion" titulo="Modificar Registro de Patrimonio Inversion">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            <td width="5%">
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idPatrimonioInversion="{$dat.pk_num_patrimonio_inversion}"
                                        descipcion="El usuario a eliminado Patromonio Inversion" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patromonio Inversion!!">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <th colspan="7">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                    descripcion="el Usuario a creado un Patrimonio Inversion" title="Nuevo Registro de Inversión"  titulo="Nuevo Registro de Inversión" id="nuevoPatrimonioInversion" >
                                <i class="md md-create"></i> Nueva Inversion &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        </th>
                    </tfoot>
                </table>
            </div>

            <!--VEHICULO-->
            <div class="tab-pane" id="vehiculo">

                <table id="datatableVehiculo" class="table table-striped no-margin table-condensed">
                    <thead>
                    <tr>
                        <th width="20%">Marca</th>
                        <th width="20%">Modelo</th>
                        <th width="10%">Año</th>
                        <th width="10%">Placa</th>
                        <th width="15%">Color</th>
                        <th width="15%">Valor</th>
                        <th width="5%">Mod</th>
                        <th width="5%">Eli</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$listadoVehiculo}

                        <tr id="idPatrimonioVehiculo{$dat.pk_num_patrimonio_vehiculo}" valor="{$dat.pk_num_patrimonio_vehiculo}" style="font-size: 11px">
                            <td width="20%">{$dat.marca_vehiculo}</td>
                            <td width="20%">{$dat.ind_modelo}</td>
                            <td width="10%">{$dat.fec_anio}</td>
                            <td width="10%">{$dat.ind_placa}</td>
                            <td width="15%">{$dat.color_vehiculo}</td>
                            <td width="15%">{$dat.num_valor|number_format:2:",":"."}</td>
                            <td width="5%">
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static" title="Editar" idPatrimonioVehiculo="{$dat.pk_num_patrimonio_vehiculo}"
                                        descipcion="El Usuario a Modificado Patrimonio Vehiculo" titulo="Modificar Registro Patrimonio Vehiculo">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            <td width="5%">
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idPatrimonioVehiculo="{$dat.pk_num_patrimonio_vehiculo}"
                                        descipcion="El usuario a eliminado Patrimonio Vehiculo" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Vehiculo!!">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <th colspan="8">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                    descripcion="el Usuario a creado un Patrimonio Vehiculo" title="Nuevo Registro de Vehiculo"  titulo="Nuevo Registro de Vehiculo" id="nuevoPatrimonioVehiculo" >
                                <i class="md md-create"></i> Nuevo Vehiculo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        </th>
                    </tfoot>
                </table>
            </div>

            <!--CUENTAS-->
            <div class="tab-pane" id="cuentas">

                <table id="datatableCuentas" class="table table-striped no-margin table-condensed">
                    <thead>
                    <tr>
                        <th width="45%">Institución</th>
                        <th width="15%">Tipo de Cuenta</th>
                        <th width="15%">Nro. Cuenta</th>
                        <th width="15%">Valor</th>
                        <th width="5%">Mod</th>
                        <th width="5%">Eli</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$listadoCuenta}

                        <tr id="idPatrimonioCuenta{$dat.pk_num_patrimonio_cuenta}" valor="{$dat.pk_num_patrimonio_cuenta}" style="font-size: 11px">
                            <td width="40%">{$dat.ind_banco}</td>
                            <td width="15%">{$dat.tipocta}</td>
                            <td width="20%">{$dat.ind_cuenta}</td>
                            <td width="15%">{$dat.num_monto|number_format:2:",":"."}</td>
                            <td width="5%">
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static" title="Editar" idPatrimonioCuenta="{$dat.pk_num_patrimonio_cuenta}"
                                        descipcion="El Usuario a Modificado Patrimonio Cuenta" titulo="Modificar Registro Patrimonio Cuenta">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            <td width="5%">
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idPatrimonioCuenta="{$dat.pk_num_patrimonio_cuenta}"
                                        descipcion="El usuario a eliminado Patrimonio Cuenta" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Cuenta!!">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <th colspan="6">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                    descripcion="el Usuario a creado un Patrimonio Cuenta" title="Nuevo Registro de Cuenta"  titulo="Nuevo Registro de Cuenta" id="nuevoPatrimonioCuenta" >
                                <i class="md md-create"></i> Nueva Cuenta &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        </th>
                    </tfoot>
                </table>
            </div>

            <!--OTROS-->
            <div class="tab-pane" id="otros">

                <table id="datatableOtros" class="table table-striped no-margin table-condensed">
                    <thead>
                    <tr>
                        <th width="35%">Descripción</th>
                        <th width="20%">Valor de Compra</th>
                        <th width="20%">Valor Actual</th>
                        <th width="5%">Mod</th>
                        <th width="5%">Eli</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$listadoOtros}

                        <tr id="idPatrimonioOtros{$dat.pk_num_patrimonio_otros}" valor="{$dat.pk_num_patrimonio_otros}" style="font-size: 11px">
                            <td width="35%">{$dat.ind_descripcion}</td>
                            <td width="20%">{$dat.num_valor_compra|number_format:2:",":"."}</td>
                            <td width="20%">{$dat.num_valor_actual|number_format:2:",":"."}</td>
                            <td width="5%">
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static" title="Editar" idPatrimonioOtros="{$dat.pk_num_patrimonio_otros}"
                                        descipcion="El Usuario a Modificado Patrimonio Otros" titulo="Modificar Registro Patrimonio Otros">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            <td width="5%">
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idPatrimonioOtros="{$dat.pk_num_patrimonio_otros}"
                                        descipcion="El usuario a Eliminado Patrimonio Otros" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Otros!!">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <th colspan="5">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                    descripcion="el Usuario a creado un Patrimonio Cuenta" title="Nuevo Registro de Patrimonio Otros"  titulo="Nuevo Registro de Patrimonio Otros" id="nuevoPatrimonioOtros" >
                                <i class="md md-create"></i> Nuevo Registro &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        </th>
                    </tfoot>
                </table>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->



{/if}


<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "70%" );


        //inicializo el datatbales
        $('#datatableInmuebles').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatableInversion').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatableVehiculo').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatableCuentas').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatableOtros').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        var $urlInmuebles='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/InmueblesMET';
        var $urlInversion='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/InversionMET';
        var $urlVehiculo ='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/VehiculosMET';
        var $urlCuentas  ='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/CuentasMET';
        var $urlOtros    ='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/OtrosMET';

        /*******************************PATRIMONIO INMUEBLE***********************************************************************/
        //al dar click en nuevo patrimonio
        $('#nuevoPatrimonioInmueble').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlInmuebles,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#datatableInmuebles tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlInmuebles,{ idPatrimonioInmueble: $(this).attr('idPatrimonioInmueble'), idEmpleado:{$Empleado} },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#datatableInmuebles tbody').on( 'click', '.eliminar', function () {

            var idPatrimonioInmueble = $(this).attr('idPatrimonioInmueble');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/EliminarPatrimonioMET';
                $.post($url, { idPatrimonioInmueble: idPatrimonioInmueble, tipo:'INMUEBLE' },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idPatrimonioInmueble'+$dato['idPatrimonioInmueble'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();

                        /*PARA ACTUALIZAR TOTALES*/
                        $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$Empleado} } ,function(dato){
                                $("#inmuebleT").val(dato[0]);
                                $("#inversionT").val(dato[1]);
                                $("#vehiculoT").val(dato[2]);
                                $("#cuentaT").val(dato[3]);
                                $("#otrosT").val(dato[4]);
                                $("#total").val(dato[5]);
                        },'json');

                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        /*******************************PATRIMONIO INMUEBLE***********************************************************************/

        /*******************************PATRIMONIO INVERSION***********************************************************************/
        $('#nuevoPatrimonioInversion').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlInversion,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#datatableInversion tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlInversion,{ idPatrimonioInversion: $(this).attr('idPatrimonioInversion'), idEmpleado:{$Empleado} },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#datatableInversion tbody').on( 'click', '.eliminar', function () {

            var idPatrimonioInversion = $(this).attr('idPatrimonioInversion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/EliminarPatrimonioMET';
                $.post($url, { idPatrimonioInversion: idPatrimonioInversion, tipo:'INVERSION' },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idPatrimonioInversion'+$dato['idPatrimonioInversion'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();

                        /*PARA ACTUALIZAR TOTALES*/
                        $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$Empleado} } ,function(dato){
                            $("#inmuebleT").val(dato[0]);
                            $("#inversionT").val(dato[1]);
                            $("#vehiculoT").val(dato[2]);
                            $("#cuentaT").val(dato[3]);
                            $("#otrosT").val(dato[4]);
                            $("#total").val(dato[5]);
                        },'json');

                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        /*******************************PATRIMONIO INVERSION***********************************************************************/

        /*******************************PATRIMONIO VEHICULO***********************************************************************/
        $('#nuevoPatrimonioVehiculo').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlVehiculo,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#datatableVehiculo tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlVehiculo,{ idPatrimonioVehiculo: $(this).attr('idPatrimonioVehiculo'), idEmpleado:{$Empleado} },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#datatableVehiculo tbody').on( 'click', '.eliminar', function () {

            var idPatrimonioVehiculo = $(this).attr('idPatrimonioVehiculo');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/EliminarPatrimonioMET';
                $.post($url, { idPatrimonioVehiculo: idPatrimonioVehiculo, tipo:'VEHICULO' },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idPatrimonioVehiculo'+$dato['idPatrimonioVehiculo'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();

                        /*PARA ACTUALIZAR TOTALES*/
                        $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$Empleado} } ,function(dato){
                            $("#inmuebleT").val(dato[0]);
                            $("#inversionT").val(dato[1]);
                            $("#vehiculoT").val(dato[2]);
                            $("#cuentaT").val(dato[3]);
                            $("#otrosT").val(dato[4]);
                            $("#total").val(dato[5]);
                        },'json');

                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        /*******************************PATRIMONIO VEHICULO***********************************************************************/

        /*******************************PATRIMONIO CUENTA***********************************************************************/
        $('#nuevoPatrimonioCuenta').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlCuentas,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#datatableCuentas tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlCuentas,{ idPatrimonioCuenta: $(this).attr('idPatrimonioCuenta'), idEmpleado:{$Empleado} },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#datatableCuentas tbody').on( 'click', '.eliminar', function () {

            var idPatrimonioCuenta = $(this).attr('idPatrimonioCuenta');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/EliminarPatrimonioMET';
                $.post($url, { idPatrimonioCuenta: idPatrimonioCuenta, tipo:'CUENTAS' },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idPatrimonioCuenta'+$dato['idPatrimonioCuenta'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();

                        /*PARA ACTUALIZAR TOTALES*/
                        $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$Empleado} } ,function(dato){
                            $("#inmuebleT").val(dato[0]);
                            $("#inversionT").val(dato[1]);
                            $("#vehiculoT").val(dato[2]);
                            $("#cuentaT").val(dato[3]);
                            $("#otrosT").val(dato[4]);
                            $("#total").val(dato[5]);
                        },'json');

                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        /*******************************PATRIMONIO CUENTA***********************************************************************/

        /*******************************PATRIMONIO OTROS***********************************************************************/
        $('#nuevoPatrimonioOtros').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlOtros,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#datatableOtros tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlOtros,{ idPatrimonioOtros: $(this).attr('idPatrimonioOtros'), idEmpleado:{$Empleado} },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#datatableOtros tbody').on( 'click', '.eliminar', function () {

            var idPatrimonioOtros = $(this).attr('idPatrimonioOtros');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/patrimonioCONTROL/EliminarPatrimonioMET';
                $.post($url, { idPatrimonioOtros: idPatrimonioOtros, tipo:'OTROS' },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idPatrimonioOtros'+$dato['idPatrimonioOtros'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();

                        /*PARA ACTUALIZAR TOTALES*/
                        $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$Empleado} } ,function(dato){
                            $("#inmuebleT").val(dato[0]);
                            $("#inversionT").val(dato[1]);
                            $("#vehiculoT").val(dato[2]);
                            $("#cuentaT").val(dato[3]);
                            $("#otrosT").val(dato[4]);
                            $("#total").val(dato[5]);
                        },'json');

                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        /*******************************PATRIMONIO OTROS***********************************************************************/

    });

</script>