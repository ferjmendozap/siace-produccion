<section class="style-default-light">

    <!------ TITULO ----------->
    <div class="section-header">
        <h2 class="text-primary">Gestionar Empleados</h2>
    </div>


    <!-------CONTENIDO--------->
    <!
    <div class="section-body">

        <!--------FILTRO DE BUSQUEDA---------------->
        <div class="card card-underline">
            <div class="card-head">
                <header>Filtro de Busqueda</header>
                <div class="tools">
                    <div class="btn-group">
                         <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                     </div>
                </div>
            </div><!--end .card-head -->
            <div class="card-body" style="display: block;">

                <div class="row">
                    <form class="form-horizontal" role="form">

                        <!----IZQUIERDA------->
                        <div class="col-xs-6">

                            <!------------------------------ORGANISMO---------------------------------------->
                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Organismo:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox" checked>
												</span>
                                        <div class="input-group-content">
                                            <select id="select13" name="select13" class="form-control input-sm">
                                                <option value="">Contraloria del Estado Sucre</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------------------------DEPENDENCIA--------------------------------------->
                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Dependencia:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox" checked>
												</span>
                                        <div class="input-group-content">
                                            <select id="select13" name="select13" class="form-control input-sm">
                                                <option value="">Dirección Tecnica</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------------------------CENTRO DE COSTO--------------------------------------->
                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Centro de Costo:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox">
												</span>
                                        <div class="input-group-content">
                                            <select id="select13" name="select13" class="form-control input-sm">
                                                <option value="">&nbsp;</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                                <option value="60">60</option>
                                                <option value="70">70</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------------------------TIPO DE NOMINA--------------------------------------->
                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Tipo de Nomina:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox">
												</span>
                                        <div class="input-group-content">
                                            <select id="select13" name="select13" class="form-control input-sm">
                                                <option value="">&nbsp;</option>
                                                <option value="30">Alto Nivel</option>
                                                <option value="40">Directores y Jefes</option>
                                                <option value="50">Empleados</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------------------------TIPO DE TRABAJADOR-------------------------------------->
                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Tipo de Trabajador:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox">
												</span>
                                        <div class="input-group-content">
                                            <select id="select13" name="select13" class="form-control input-sm">
                                                <option value="">&nbsp;</option>
                                                <option value="30">Activo</option>
                                                <option value="40">Contratado</option>
                                                <option value="50">Inactivo</option>
                                                <option value="60">Jubilado</option>
                                                <option value="70">Pensionado</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!------DERECHA------->
                        <div class="col-xs-6">

                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Edo. Registro:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox" checked>
												</span>
                                        <div class="input-group-content">
                                            <select id="select13" name="select13" class="form-control input-sm">
                                                <option value="">Activo</option>
                                                <option value="30">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Sit. Trabajo:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox" checked>
												</span>
                                        <div class="input-group-content">
                                            <select id="select13" name="select13" class="form-control input-sm">
                                                <option value="">Activo</option>
                                                <option value="30">Inactivo</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Fecha Ingreso:</label>
                                <div class="col-sm-9">
                                    <div class="input-daterange input-group" id="rango-fechas">
                                        <div class="input-group-content">
                                            <input type="text" class="form-control" name="start" />

                                        </div>
                                        <span class="input-group-addon">a</span>
                                        <div class="input-group-content">
                                            <input type="text" class="form-control" name="end" />
                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox">
												</span>
                                        <div class="input-group-content">
                                            <input type="text" class="form-control input-sm" id="groupcheckbox17">
                                        </div>
                                    </div>
                                -->
                            </div>

                            <div class="form-group">
                                <label for="groupcheckbox17" class="col-sm-3 control-label">Buscar:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox">
												</span>
                                        <div class="input-group-content">
                                            <input type="text" class="form-control input-sm" id="groupcheckbox17">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" align="right">
                                <button class="btn ink-reaction btn-raised btn-info" id="buscar">
                                    Buscar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-search"></i>
                                </button>
                            </div>


                        </div>

                    </form>
                </div>

            </div><!--end .card-body -->
        </div>-->



        <!-------- LISTADO Y BOTONES DE OPCIONES-------->
        <div class="row">

            <div class="col-xs-2">
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Vacaciones del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Patrimonio del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Permisos del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Instrucción del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Referencias del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Documentos del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Experiencia Laboral
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Meritos y Demeritos
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Información Bancaria
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Cargar Familiar
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Impuesto ISLR
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Control de Nivelaciones
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm" type="button">
                    Historial de Modificaciones
                </button>
            </div>

            <!-- para lista de empleados -->
            <div class="col-xs-10">
                <div class="card">

                    <div class="card-actionbar-row">
                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                descripcion="el Usuario a creado un Nuevo Empleado"  titulo="Nuevo Registro de Empleado" id="nuevo" >
                            Nuevo Empleado &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                        </button>
                    </div>

                    <div class="card-body">

                        <table class="table table-striped no-margin table-condensed">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Nombre Completo</th>
                                <th>Nro. Documento</th>
                                <th>Cargo</th>
                                <th width="100">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr style="font-size: 11px">
                                <td><span class="input-group-addon"><input type="checkbox" checked></span></td>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                                <td><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idUtiles="{$f.pk_num_utiles}"
                                            descipcion="El Usuario a Modificado Utiles Escolares" titulo="Modificar Utiles Escolares">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-search" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr style="font-size: 11px">
                                <td><span class="input-group-addon"><input type="checkbox" checked></span></td>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                                <td><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idUtiles="{$f.pk_num_utiles}"
                                            descipcion="El Usuario a Modificado Utiles Escolares" titulo="Modificar Utiles Escolares">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-search" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr style="font-size: 11px">
                                <td><span class="input-group-addon"><input type="checkbox" checked></span></td>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                                <td><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idUtiles="{$f.pk_num_utiles}"
                                            descipcion="El Usuario a Modificado Utiles Escolares" titulo="Modificar Utiles Escolares">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-search" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr style="font-size: 11px">
                                <td><span class="input-group-addon"><input type="checkbox" checked></span></td>
                                <td>Laurie</td>
                                <td>Hart</td>
                                <td>@lau</td>
                                <td><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idUtiles="{$f.pk_num_utiles}"
                                            descipcion="El Usuario a Modificado Utiles Escolares" titulo="Modificar Utiles Escolares">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-search" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr style="font-size: 11px">
                                <td><span class="input-group-addon"><input type="checkbox" checked></span></td>
                                <td>Kristin</td>
                                <td>Poole</td>
                                <td>@kri</td>
                                <td><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idUtiles="{$f.pk_num_utiles}"
                                            descipcion="El Usuario a Modificado Utiles Escolares" titulo="Modificar Utiles Escolares">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-search" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr style="font-size: 11px">
                                <td><span class="input-group-addon"><input type="checkbox" checked></span></td>
                                <td>Melvin</td>
                                <td>Vega</td>
                                <td>@vega</td>
                                <td><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idUtiles="{$f.pk_num_utiles}"
                                            descipcion="El Usuario a Modificado Utiles Escolares" titulo="Modificar Utiles Escolares">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idUtiles="{$f.pk_num_utiles}"  boton="si, Eliminar"
                                            descipcion="El usuario a eliminado Utiles Escolares" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                        <i class="md md-search" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>

    </div>


</section>

<script type="text/javascript">

    $(document).ready(function() {

        //$(".btn-collapse").click();

        //inicializar el datepicker
        $("#rango-fechas").datepicker({
            format:'dd-mm-yyyy'
        });

        /*URL EN EL CONTROLADOR*/
        var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/EmpleadoMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });



    });


</script>