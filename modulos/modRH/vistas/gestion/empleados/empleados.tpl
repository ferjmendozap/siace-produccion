<section class="style-default-light">

    <!------ TITULO ----------->
    <div class="section-header">
        <h2 class="text-primary">Gestionar Empleados</h2>
    </div>

    <!-------CONTENIDO--------->

    <div class="section-body">
        <!-------- LISTADO Y BOTONES DE OPCIONES-------->
        <div class="row">

            <div class="col-md-2 text-center">
                {if in_array('RH-01-01-01-05-P',$_Parametros.perfil)}
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="vacaciones" titulo="Vacaciones del Empleado" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Vacaciones del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="patrimonio" titulo="Patrimonio del Empleado" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Patrimonio del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="permisos" titulo="Permisos del Empleado" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Permisos del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="instruccion" titulo="Instrucción del Empleado" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Instrucción del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="referencias" titulo="Referencias del Empleado" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Referencias del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="documentos" titulo="Documentos del Empleado" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Documentos del Empleado
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="experiencia" titulo="Experiencia Laboral" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Experiencia Laboral
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="meritosdemeritos" titulo="Meritos y Demeritos" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Meritos y Demeritos
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="infobanco" titulo="Información Bancaria" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Información Bancaria
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="carga" titulo="Cargar Familiar" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Cargar Familiar
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="impuesto" titulo="Impuesto ISLR" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Impuesto ISLR
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="ctrnivelaciones" titulo="Control de Nivelaciones" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Control de Nivelaciones
                </button>
                <button class="btn btn-block ink-reaction btn-info btn-sm info" type="button" title="" id="historial" titulo="Historial de Modificaciones" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                    Historial de Modificaciones
                </button>
                {/if}

            </div>

            <!-- para lista de empleados -->
            {if in_array('RH-01-01-01-05-P',$_Parametros.perfil)}
            <div class="col-md-10">
            {else}
            <div class="col-md-12">
            {/if}

                <div class="card">
                    <!-- barra superior y boton de filtro -->
                    <div class="card-head">
                        <div class="col-sm-6">
                            <div id="informacion" class="text-danger"></div>
                        </div>
                        <div class="col-sm-6 text-right">
                            <div class="btn-group">
                                <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-info pull-right"  href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
                            </div>
                        </div>
                    </div>

                    <!--  listado de empleados -->
                    <div class="card-body">
                        <input type="hidden" id="idEmpleado" name="idEmpleado" value="0">
                        <input type="hidden" id="Nombres" name="Nombres">
                        <table id="datatable1" class="table table-striped no-margin table-condensed">
                            <thead>
                            <tr>
                                <th>Foto</th>
                                <th width="140">Nro. Documento</th>
                                <th>Nombre Completo</th>
                                <th>Cargo</th>
                                {if $hcmcontrol==2}
                                    <th width="40">Hcm</th>
                                {/if}
                                <th width="40">Info</th>
                                <th width="40">Mod</th>
                                <th width="40">Ver</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach item=dat from=$listadoEmpleados}

                                <tr id="idEmpleado{$dat.pk_num_empleado}" valor="{$dat.pk_num_empleado}" nombres="{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}" style="font-size: 11px" onclick="fnMClk(this)">

                                    <!--<td><span class="input-group-addon"><input type="checkbox" name="ckeck" id="check-{$n}" value="{$n}" class="ck"></span></td>-->
                                    {if isset($dat.ind_foto)}
                                    <td><img class="img-circle width-0 ampliar_foto" src="{$_Parametros.ruta_Img}modRH/fotos/{$dat.ind_foto}" alt="" style="cursor: pointer"title="Ampliar Foto" data-toggle="modal" data-target="#formModal"
                                             data-keyboard="false" data-backdrop="static" ind_foto="{$dat.ind_foto}" titulo="{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}"/></td>
                                    {else}
                                    <td><img class="img-circle width-0" src="{$_Parametros.ruta_Img}avatar.jpg" alt="" /></td>
                                    {/if}
                                    <td style="cursor: pointer" title="Click para Seleccionar Registro">{$dat.ind_cedula_documento}</td>
                                    <td style="cursor: pointer" title="Click para Seleccionar Registro">{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}</td>
                                    <td style="cursor: pointer" title="Click para Seleccionar Registro">{$dat.ind_descripcion_cargo}</td>
                                    {if $hcmcontrol==2}
                                    <td class="text-left" width="40">
                                        {if in_array('RH-01-01-01-10-HCM',$_Parametros.perfil)}
                                        <button class="hcmControl logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" title="Ayuda Médica HCM" idEmpleado="{$dat.pk_num_empleado}" cod_empleado="{$dat.cod_empleado}" nombre="{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}"
                                                titulo="Configuración Ayuda Médicas HCM">
                                            <i class="fa fa-medkit" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                    </td>
                                    {/if}
                                    <td class="text-left" width="40">
                                        <button class="infoReg logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" title="Información de Registro" idEmpleado="{$dat.pk_num_empleado}"
                                                descipcion="El Usuario a consultado operaciones del Empleado" titulo="Información de Registro de Empleado">
                                            <i class="md md-people" style="color: #ffffff;"></i>
                                        </button>
                                    </td>
                                    <td class="text-left" width="40">
                                        {if in_array('RH-01-01-01-03-M',$_Parametros.perfil)}
                                        <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" title="Editar Empleado" idEmpleado="{$dat.pk_num_empleado}"
                                                descipcion="El Usuario a Modificado Registro de Empleado" titulo="Modificar Registro del Empleado">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                    </td>
                                    <td class="text-left" width="40">
                                        {if in_array('RH-01-01-01-04-C',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" title="Ver Empleado" idEmpleado="{$dat.pk_num_empleado}"
                                                descipcion="El usuario a visualizado datos del Empleado" titulo="Ver Datos del Empleado">
                                            <i class="md md-search" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                    </td>
                                    <input type="hidden" value="{$n++}">
                                </tr>
                            {/foreach}
                            </tbody>
                            <tfoot>
                                <th colspan="7">
                                    {if in_array('RH-01-01-01-02-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descripcion="el Usuario a creado un Nuevo Empleado" title="Nuevo Empleado"  titulo="Nuevo Registro de Empleado" id="nuevo" >
                                            <i class="md md-create"></i>  Nuevo Empleado
                                        </button>
                                    {/if}
                                </th>
                            </tfoot>
                        </table>
                    </div>
                </div><!--  fin card -->
            </div><!-- fin columna -->
        </div><!-- fin fila -->

    </div>


</section>

<!-- filtro de busqueda del empleado -->
<div class="offcanvas">

    <div id="offcanvas-filtro" class="offcanvas-pane width-10">

        <div class="offcanvas-head">
            <header>Filtro</header>
            <div class="offcanvas-tools">
                <a class="cerrar btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form id="filtroEmpleadoListado" class="form" role="form" method="post">
                <input type="hidden" name="filtro" value="1">

                <!--ORGANISMO-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" required data-msg-required="Seleccione Organismo">
                                {foreach item=dat from=$listadoOrganismos}
                                    {if isset($formDB.fk_a001_num_organismo)}
                                        {if $dat.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                            <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {else}
                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {/if}
                                    {else}
                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Organismo</label>
                        </div>
                    </div>
                </div>
                <!--DEPENDENCIA-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control input-sm select2-list select2">
                                <option value="">&nbsp;</option>
                                {foreach item=dep from=$listadoDependencia}
                                    {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                        <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                    {else}
                                        <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Dependencia</label>
                        </div>
                    </div>
                </div>
                <!--CENTRO DE COSTO-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="centro_costo" name="centro_costo" class="form-control input-sm select2-list select2">
                                <option value="">&nbsp;</option>
                                {foreach item=centro from=$centroCosto}
                                    <option value="{$centro.pk_num_centro_costo}">{$centro.ind_descripcion_centro_costo}</option>
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Centro de Costo</label>
                        </div>
                    </div>
                </div>
                <!--TIPO DE NOMINA-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="tipo_nomina" name="tipo_nomina" class="form-control input-sm select2-list select2">
                                <option value="">&nbsp;</option>
                                {foreach item=nom from=$nomina}
                                    <option value="{$nom.pk_num_tipo_nomina}">{$nom.ind_nombre_nomina}</option>
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Tipo de Nomina</label>
                        </div>
                    </div>
                </div>
                <!--TIPO TRABAJADOR-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="tipo_trabajador" name="tipo_trabajador" class="form-control input-sm select2-list select2">
                                <option value="">&nbsp;</option>
                                {foreach item=tipo from=$tipoTrabajador}
                                    <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Tipo Trabajador</label>
                        </div>
                    </div>
                </div>
                <!--SITUACION  DE TRABAJO-->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="edo_reg" name="edo_reg" class="form-control input-sm">
                                <option value="1">ACTIVO</option>
                                <option value="0">INACTIVO</option>
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Edo. Registro</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="sit_trab" name="sit_trab" class="form-control input-sm">
                                <option value="1">ACTIVO</option>
                                <option value="0">INACTIVO</option>
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Sit. Trabajo</label>
                        </div>
                    </div>
                </div>
                <!--FECHA INGRESO-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-daterange input-group" id="rango-fechas">
                                <div class="input-group-content">
                                    <input type="text" class="form-control" name="fecha_inicio" id="fecha_inicio" />
                                </div>
                                <span class="input-group-addon">a</span>
                                <div class="input-group-content">
                                    <input type="text" class="form-control" name="fecha_fin" id="fecha_fin"/>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <label for="">Fecha de Ingreso</label>
                        </div>
                    </div>
                </div>
                <!--BUSCAR-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" name="buscar" id="busccar">
                            <label for="buscar">Buscar</label>
                        </div>
                    </div>
                </div>
                <!--BOTON-->
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltroEmpleado">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .letra_pequena{
        font-size: 11px;
    }

</style>
<script type="text/javascript">

    $(document).ready(function() {

        //inicializar el datepicker
        $("#rango-fechas").datepicker({
            todayHighlight: true,
            format:'dd-mm-yyyy',
            autoclose: true,
            language:'es'
        });

        /*URL EN EL CONTROLADOR*/
        var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/EmpleadoMET';
        var $urlInfo='{$_Parametros.url}modRH/gestion/empleadosCONTROL/InformacionEmpleadoMET';
        var $urlReg='{$_Parametros.url}modRH/gestion/empleadosCONTROL/InformacionRegistroMET';
        var $url_ampliar_foto = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/AmpliarFotoMET';
        var $urlhcm='{$_Parametros.url}modRH/gestion/empleadosCONTROL/OperacionHCMMET';

        //al dar click en nuevo empleado
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { operacion:'registrar'} ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        /*CONFIGURAR HCM*/
        $('#datatable1 tbody').on( 'click', '.hcmControl', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            var nombres = $(this).attr('nombre');
            var cod_empleado = $(this).attr('cod_empleado');
            $.post($urlhcm,{ idEmpleado: $(this).attr('idEmpleado'), nombres:nombres, cod_empleado: cod_empleado},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        /*VER INFORMACION DEL EMPLEADO*/
        $('#datatable1 tbody').on( 'click', '.infoReg', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($urlReg,{ idEmpleado: $(this).attr('idEmpleado')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        /*VER DATOS DEL EMPLEADO*/
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEmpleado: $(this).attr('idEmpleado'), operacion:'ver' },function($dato){
                $('#ContenidoModal').html($dato);
                //pongo los campos de formualrio en disabled y oculto los botones
                $("#accion").hide();
                $(".form-control").attr("disabled","disabled");
                $("#nuevaDireccion").hide();
                $("#nuevoTelefono").hide();
                $("#nuevoTelefono1").hide();
                $("#nuevoTelefono2").hide();
                $("#nuevaLic").hide();
            });
        });

        /*MODIFICAR DATOS DEL DEL EMPLEADO*/
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEmpleado: $(this).attr('idEmpleado'), operacion:'modificar'},function($dato){
                $('#ContenidoModal').html($dato);

            });
        });

        //chekboxes del listado del empleado
        $('input[type=checkbox]').click(function() {
                $(".ck:checkbox:checked").removeAttr("checked");
                $(this).attr('checked', 'checked');
        });

        //ACCION DE LOS BOTONES DE LA INFORMACION DEL EMPLEADO (BOTONES LADO IZQUIERDO)
        $('.info').click(function(){
            var modal = ($(this).attr('id'));
            var empleado = $("#idEmpleado").val();
            var nombres  = $("#Nombres").val();
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($urlInfo, { info:modal , empleado:empleado , nombres:nombres },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $( "#vacaciones" ).mouseover(function() {
            $("#informacion").html('Actualizar Vacaciones del Empleado');
        });
        $( "#patrimonio" ).mouseover(function() {
            $("#informacion").html('Actualizar Patrimonio del Empleado');
        });
        $( "#permisos" ).mouseover(function() {
            $("#informacion").html('Ver Permisos del Empleado');
        });
        $( "#instruccion" ).mouseover(function() {
            $("#informacion").html('Actualizar Instrucci&oacute;n Academica, Idiomas y Otros Estudios del Empleado');
        });
        $( "#referencias" ).mouseover(function() {
            $("#informacion").html('Actualizar Referencias Personales y Laborales del Empleado');
        });
        $( "#documentos" ).mouseover(function() {
            $("#informacion").html('Actualizar Documentos Entregados por el Empleado');
        });
        $( "#experiencia" ).mouseover(function() {
            $("#informacion").html('Actualizar Experiencia Laboral del Empleado');
        });
        $( "#meritosdemeritos" ).mouseover(function() {
            $("#informacion").html('Actualizar M&eacute;ritos y Dem&eacute;ritos del Empleado');
        });
        $( "#infobanco" ).mouseover(function() {
            $("#informacion").html('Actualizar Informaci&oacute;n Bancaria del Empleado');
        });
        $( "#carga" ).mouseover(function() {
            $("#informacion").html('Actualizar Carga Familiar del Empleado');
        });
        $( "#impuesto" ).mouseover(function() {
            $("#informacion").html('Actualizar Porcentajes del Impuesto S. la Renta del Empleado');
        });
        $( "#ctrnivelaciones" ).mouseover(function() {
            $("#informacion").html('Control e Historial de Nivelaciones del Empleado');
        });
        $( "#historial" ).mouseover(function() {
            $("#informacion").html('Ver Historial de Modificaciones del Empleado');
        });

        $( "#vacaciones" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#patrimonio" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#permisos" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#instruccion" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#referencias" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#documentos" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#experiencia" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#meritosdemeritos" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#infobanco" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#carga" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#impuesto" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#ctrnivelaciones" ).mouseout(function() {
            $("#informacion").html('');
        });
        $( "#historial" ).mouseout(function() {
            $("#informacion").html('');
        });


        $("#filtroEmpleadoListado").validate({
            submitHandler: function(form) {
                var pk_num_organismo = $("#fk_a001_num_organismo").val();
                var pk_num_dependencia = $("#pk_num_dependencia").val();
                var centro_costo = $("#centro_costo").val();
                var tipo_nomina = $("#tipo_nomina").val();
                var tipo_trabajador = $("#tipo_trabajador").val();
                var estado_registro = $("#edo_reg").val();
                var situacion_trabajo = $("#sit_trab").val();
                var fecha_inicio = $("#fecha_inicio").val();
                var fecha_fin = $("#fecha_fin").val();
                var buscar = $("#buscar").val();

                var url_listar='{$_Parametros.url}modRH/gestion/empleadosCONTROL/BuscarTrabajadorMET';
                $.post(url_listar,{ pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, centro_costo: centro_costo, tipo_nomina: tipo_nomina, tipo_trabajador: tipo_trabajador, estado_registro: estado_registro, situacion_trabajo: situacion_trabajo, fecha_inicio: fecha_inicio, fecha_fin: fecha_fin },function(respuesta_post) {
                    var tabla_listado = $('#datatable1').DataTable();
                    tabla_listado.clear().draw();
                    if(respuesta_post != -1) {
                        for(var i=0; i<respuesta_post.length; i++) {

                            if(respuesta_post[i].ind_foto==''){
                                var foto = '<img class="img-circle width-0" src="{$_Parametros.ruta_Img}avatar.jpg" />';

                            } else  {
                                var ind_foto = respuesta_post[i].ind_foto;
                                var nombre = respuesta_post[i].ind_nombre1+' '+respuesta_post[i].ind_nombre2+' '+respuesta_post[i].ind_apellido1+' '+respuesta_post[i].ind_apellido2;
                                var foto = '<img class="img-circle width-0" id="x" onclick="ampliar_foto(\''+ind_foto+'\',\''+nombre+'\')"  src="{$_Parametros.ruta_Img}modRH/fotos/'+respuesta_post[i].ind_foto+'" title="Ampliar Foto" style="cursor:pointer" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" ind_foto="'+respuesta_post[i].ind_foto+'" titulo="'+respuesta_post[i].ind_nombre1+' '+respuesta_post[i].ind_nombre2+' '+respuesta_post[i].ind_apellido1+' '+respuesta_post[i].ind_apellido2+'"/>';

                            }

                            var info = '<button class="infoReg btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idEmpleado="'+respuesta_post[i].pk_num_empleado+'" titulo="Información de Registro de Empleado"><i class="md md-people" style="color: #ffffff;"></i></button>';
                            var mod  = '<button class="modificar btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idEmpleado="'+respuesta_post[i].pk_num_empleado+'" titulo="Modificar Registro del Empleado"><i class="fa fa-edit" style="color: #ffffff;"></i></button>';
                            var ver  = '<button class="ver btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idEmpleado="'+respuesta_post[i].pk_num_empleado+'"  titulo="Ver Datos del Empleado"><i class="md md-search" style="color: #ffffff;"></i></button>';

                            var fila = tabla_listado.row.add([
                                foto,
                                respuesta_post[i].ind_cedula_documento,
                                respuesta_post[i].ind_nombre1+' '+respuesta_post[i].ind_nombre2+' '+respuesta_post[i].ind_apellido1+' '+respuesta_post[i].ind_apellido2,
                                respuesta_post[i].ind_descripcion_cargo,
                                info,
                                mod,
                                ver
                            ]).draw()
                                    .nodes()
                                    .to$()
                                    .addClass( 'letra_pequena' );
                            $(fila)
                                    .css('cursor','pointer')
                                    .attr('id','idEmpleado'+respuesta_post[i].pk_num_empleado+'')
                                    .attr('valor',respuesta_post[i].pk_num_empleado)
                                    .attr('nombres',nombre)
                                    .click(function(){
                                        var seleccionado=document.getElementsByTagName("tr");
                                        for (var i=0; i<seleccionado.length; i++) {
                                            if (seleccionado[i].getAttribute((document.all ? 'className' : 'class')) ==	'selected') {
                                                seleccionado[i].setAttribute((document.all ? 'className' : 'class'), "");
                                                break;
                                            }
                                        }
                                        var row=document.getElementById(this.id);	//	OBTENGO LA FILA DEL CLICK
                                        row.className="selected";	//	CAMBIO EL COLOR DE LA FILA
                                        var registro=$('#idEmpleado');
                                        registro.val($(this).attr('valor'));
                                        $("#Nombres").val($(this).attr('nombres'));
                                    });
                        }
                        $('#filtroEmpleadoListado').each(function () { {*resetear los campos del formulario*}
                            this.reset();
                        });
                        $(".cerrar").click();
                    }

                },'json');
            }
        });

        //al dar click para ampliar la foto del empleado
        $('.ampliar_foto').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_ampliar_foto, { foto:$(this).attr('ind_foto')} ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });



    });

    /*
     *FUNCION QUE CAMBIA DE COLOR LA FILA DE UNA TABLA AL HACER CLICK EL PUNTERO DEL MOUSE SOBRE ELLA
     */
    function fnMClk(src) {

        var seleccionado=document.getElementsByTagName("tr");
        for (var i=0; i<seleccionado.length; i++) {
            if (seleccionado[i].getAttribute((document.all ? 'className' : 'class')) ==	'selected') {
                seleccionado[i].setAttribute((document.all ? 'className' : 'class'), "");
                break;
            }
        }
        //
        var row=document.getElementById(src.id);	//	OBTENGO LA FILA DEL CLICK
        row.className="selected";	//	CAMBIO EL COLOR DE LA FILA

        var registro=$('#idEmpleado');
        registro.val($(src).attr('valor'));

        $("#Nombres").val($(src).attr('nombres'));
    }


    function ampliar_foto(foto,nombre)
    {
        var $url_ampliar_foto = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/AmpliarFotoMET';
        $('#formModalLabel').html(nombre);
        $.post($url_ampliar_foto, { foto: foto} ,function($dato){
            $('#ContenidoModal').html($dato);
        });
    }


</script>