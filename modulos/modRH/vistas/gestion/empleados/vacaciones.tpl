{if $Empleado==0}
<!-- no se ha seleccionado ningun registro se muestra advertencia -->
<div class="card">
    <div class="card-body">
        <div class="alert alert-danger" role="alert">
            <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
        </div>
    </div><!--end .card-body -->
</div><!--end .card -->
{else}
<!-- no se ha seleccionado ningun registro se muestra advertencia -->

    <input type="hidden" value="{$empleado.codigo_empleado}" id="funcionario" />
    <section class="style-default-bright">
        <div class="form floating-label">
            <div class="row">
                <div class="col-lg-12"  align="right">
                    <div class="btn-group">
                        <button type="button" class="btn ink-reaction btn-default-light">Actualizar Periodo</button>
                        <button type="button" class="btn ink-reaction btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" id="actualizarFuncionario">Funcionario</a></li>
                            <li><a href="#" id="actualizarTodos">Todos</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="fa fa-fw fa-times text-danger"></i> Cerrar</a></li>
                        </ul>
                    </div><!--end .btn-group -->
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Datos del Empleado</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4 col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control dirty" disabled value="{$empleado.codigo_empleado}">
                                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> Empleado</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control dirty" disabled value="{$empleado.funcionario}">
                                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> Nombre Completo</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control dirty" disabled value="{$empleado.tiempo_servicio}">
                                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> Tiempo de Servicio</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control dirty" disabled value="{$empleado.antecedente}">
                                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> Antecedente de Servicio</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control dirty" disabled value="{$empleado.derecho}">
                                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> Días por Derecho</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control dirty" disabled value="{$empleado.dias_adicionales}">
                                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> Días Adicionales</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Periodos</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12">
                                <div class="col-md-2 col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control dirty" disabled value="{$empleado.fecha_ingreso|date_format:"%d/%m/%Y"}">
                                        <label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Fecha de Ingreso</label>
                                    </div>
                                </div>
                                <div class="col-md-2 col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control dirty" disabled value="{$empleado.fecha_actual}">
                                        <label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Fecha Actual</label>
                                    </div>
                                </div>
                                <div class="col-md-2 col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control dirty" id="diasPendientes" disabled value="{$empleado.pendientes}">
                                        <label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Dias Pendientes</label>
                                    </div>
                                </div>

                                <div class="col-sd-2 col-sd-2" align="right">
                                    <button type="button" id="registroPeriodo" class="nuevo logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" descripcion="El usuario ha editado un periodo de vacaciones" titulo="Editar Periodo" title="Editar periodo de vacaciones"><i class="md md-add"></i> Editar Periodos</button>
                                </div>

                            </div>
                            <div class="col-md-12 col-sm-12" align="center">
                                <table id="datatable5" class="table table-striped table-hover">
                                    <thead>
                                    <tr align="center">
                                        <th>N°</th>
                                        <th width="150">Periodo</th>
                                        <th>Mes Prog.</th>
                                        <th>Derecho</th>
                                        <th>Días Gozados</th>
                                        <th>Interrumpidos</th>
                                        <th>Total Utilizados</th>
                                        <th>Vac. Pendientes</th>
                                        <th>Pagos</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {assign var="numero" value="1"}
                                    {foreach item=per from=$periodo}
                                        {$solicitados = $per.num_dias_gozados + $per.num_dias_interrumpidos}
                                        <tr id="pk_num_periodo{$per.pk_num_periodo}">
                                            <td>{$numero++}</td>
                                            {if $per.num_pendientes>0}
                                                <td style="font-weight:bold;"><button class="periodo" periodo="{$per.pk_num_periodo}">{$per.num_anio-1} - {$per.num_anio}</button></td>
                                            {else}
                                                <td><button class="periodo" periodo="{$per.pk_num_periodo}">{$per.num_anio-1} - {$per.num_anio}</button></td></td>
                                            {/if}
                                            <td>{$empleado.fecha_ingreso|date_format:"%m"}</td>
                                            <td>{$per.num_dias_derecho}</td>
                                            <td>{$per.num_dias_gozados}</td>
                                            <td>{$per.num_dias_interrumpidos}</td>
                                            <td>{$per.num_total_utilizados}</td>
                                            <td>{$per.num_pendientes}</td>
                                            <td align="center"></td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                </table>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 contain-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Utilización</h3>
                        </div>
                        <div class="panel-body">
                            <table id="datatable2" class="table table-striped table-hover">
                                <thead>
                                <tr align="center">
                                    <th>N°</th>
                                    <th>Utilización</th>
                                    <th>Días</th>
                                    <th>Inicio</th>
                                    <th>Fin</th>
                                    <th width="100">Acción</th>
                                </tr>
                                </thead>
                            </table>
                            <div id="utilizacion" align="center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

{/if}
<script type="text/javascript">
    $(document).ready(function() {

        $('#modalAncho').css( "width", "85%" );

        var $urlUtilizacion ='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/UtilizacionVacacionesMET';
        var $pkNumEmpleado = $("#funcionario").val();
        $('#utilizacion').on( 'click', '.nuevo', function () {
            $('#modalAncho2').css( "width", "45%" );
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html("");
            $.post($urlUtilizacion,{ pk_num_periodo: $(this).attr('pk_num_periodo'), pkNumEmpleado: $pkNumEmpleado},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        var $urlPeriodo ='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/EditarPeriodoMET';
        $('#registroPeriodo').click(function(){
            $('#modalAncho2').css( "width", "75%" );
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html("");
            $.post($urlPeriodo,{ pkNumEmpleado: $pkNumEmpleado},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        /**PARA MODIFICAR UTILIZACION GOCE O INTERRUPCION*/
        var $urlModUtilizacion ='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/EditarUtilizacionMET';
        $('#datatable2').on( 'click', '.modificarUtilizacion', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($urlModUtilizacion,{ pk_num_vacacion_utilizacion: $(this).attr('pk_num_vacacion_utilizacion'), pk_num_periodo: $(this).attr('pk_num_periodo'), pkNumEmpleado: $pkNumEmpleado   },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        var $urlPendientes ='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularDiasPendientesMET';

        var $urlFuncionario='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ActualizarPeriodoMET';
        $('#actualizarFuncionario').click(function(){
            var $pkNumEmpleado = $("#funcionario").val();
            $.post($urlFuncionario,{ valor: 1, empleado: $pkNumEmpleado },function(respuesta_post){
                var tabla_listado = $('#datatable5').DataTable();
                tabla_listado.clear().draw();
                if(respuesta_post != -1) {
                    var $i = respuesta_post.length;
                    for(var i=0; i<respuesta_post.length; i++) {
                        var anioAnterior = respuesta_post[i].num_anio-1;
                        if(respuesta_post[i].num_pendientes>0){
                           var periodo = '<button class="periodo"  periodo="'+respuesta_post[i].pk_num_periodo+'" style="font-weight:bold;">'+anioAnterior+ ' - ' + respuesta_post[i].num_anio+'</button>';
                        } else {
                            var periodo = '<button class="periodo" periodo="'+respuesta_post[i].pk_num_periodo+'">'+anioAnterior+ ' - ' + respuesta_post[i].num_anio+'</button>';
                        }
                        if(respuesta_post[i].num_mes<10){
                            var num_mes = '0'+respuesta_post[i].num_mes;
                        } else {
                            var num_mes = respuesta_post[i].num_mes;
                        }
                        var fila = tabla_listado.row.add([
                            $i,
                            periodo,
                            num_mes,
                            respuesta_post[i].num_dias_derecho,
                            respuesta_post[i].num_dias_gozados,
                            respuesta_post[i].num_dias_interrumpidos,
                            respuesta_post[i].num_total_utilizados,
                            respuesta_post[i].num_pendientes,
                            ''
                        ]).draw()
                        .nodes()
                        .to$()

                        tabla_listado.order([1,'desc']);

                        $i--;
                        $(fila).attr('id','pk_num_periodo'+respuesta_post[i].pk_num_periodo+'');
                    }
                }
            },'json');

            $.post($urlPendientes,{ pk_num_empleado: $pkNumEmpleado },function(dato){
                $('#diasPendientes').val(dato['diasPendientes']);
            },'json');
            swal('Actualización Exitosa', "El periodo fue actualizado exitosamente", "success");

        });

        $('#actualizarTodos').click(function(){
            var $pkNumEmpleado = $("#funcionario").val();
            $.post($urlFuncionario,{ valor: 2, empleado: $pkNumEmpleado },function(respuesta_post){
                var tabla_listado = $('#datatable5').DataTable();
                tabla_listado.clear().draw();
                if(respuesta_post != -1) {
                    var $i = respuesta_post.length;
                    for(var i=0; i<respuesta_post.length; i++) {
                        var anioAnterior = respuesta_post[i].num_anio-1;
                        if(respuesta_post[i].num_pendientes>0){
                            var periodo = '<button class="periodo" periodo="'+respuesta_post[i].pk_num_periodo+'" style="font-weight:bold;">'+anioAnterior+ ' - ' + respuesta_post[i].num_anio+'</button>';
                        } else {
                            var periodo = '<button class="periodo" periodo="'+respuesta_post[i].pk_num_periodo+'">'+anioAnterior+ ' - ' + respuesta_post[i].num_anio+'</button>';
                        }
                       var fila = tabla_listado.row.add([
                            $i,
                            periodo,
                            respuesta_post[i].num_mes,
                            respuesta_post[i].num_dias_derecho,
                            respuesta_post[i].num_dias_gozados,
                            respuesta_post[i].num_dias_interrumpidos,
                            respuesta_post[i].num_total_utilizados,
                            respuesta_post[i].num_pendientes,
                            ''
                        ]).draw()
                            .nodes()
                            .to$()
                        $i--;
                        $(fila).attr('id','pk_num_periodo'+respuesta_post[i].pk_num_periodo+'');
                    }
                }
            },'json');
            $.post($urlPendientes,{ pk_num_empleado: $pkNumEmpleado },function(dato){
                $('diasPendientes').val(dato['diasPendientes']);
            },'json');
            swal('Actualización Exitosa', "Los periodos fueron actualizados exitosamente", "success");

        });

        var $urlVer='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/UtilizacionMET';
        $('#datatable5 tbody').on( 'click', '.periodo', function () {
            var $verPeriodo = $(this).attr('periodo');
            $.post($urlVer,{ pk_num_periodo: $verPeriodo },function(respuesta_post){
                var tabla_listado = $('#datatable2').DataTable();
                var botonUtilizacion = '<button type="button" pk_num_periodo = "' + $verPeriodo + '" class="nuevo logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" descripcion="El usuario ha creado una utilizacion de vacaciones" titulo="Utilización de Vacaciones" title="Registrar utilización de vacaciones"><i class="md md-add"></i>  Insertar Nuevo Campo</button>';
                tabla_listado.clear().draw();
                var $a=1;
                if(respuesta_post != -1) {
                    for(var i=0; i<respuesta_post.length; i++) {
                        var codigoDetalle = respuesta_post[i].cod_detalle;

                        /*se desactivo temporalmente esta condicion ( es imprescindible porder eliminar o modifcar una utilización )*/
                        /*if(codigoDetalle==1){
                            botonEliminar = '';
                        } else {
                            var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado una utilizacion" titulo="¿Estás Seguro?" title="Eliminar Utilización" mensaje="¿Estás seguro de eliminar la utilizacion?" boton="si, Eliminar" pk_num_vacacion_utilizacion="'+respuesta_post[i].pk_num_vacacion_utilizacion+'"><i class="md md-delete" style="color: #ffffff;"></i></button>';
                        }*/

                        var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descripcion="El usuario ha eliminado una utilizacion" titulo="¿Estás Seguro?" title="Eliminar Utilización" mensaje="¿Estás seguro de eliminar la utilizacion?" boton="si, Eliminar" pk_num_vacacion_utilizacion="'+respuesta_post[i].pk_num_vacacion_utilizacion+'"><i class="md md-delete" style="color: #ffffff;"></i></button>';
                        var botonModificar = '<button class="modificarUtilizacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" title="Editar" descripcion="El Usuario a Modificado una utilización" titulo="Modificar Utilización" pk_num_periodo="'+$verPeriodo+'" pk_num_vacacion_utilizacion="'+respuesta_post[i].pk_num_vacacion_utilizacion+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button>';

                        tabla_listado.row.add([
                            $a,
                            respuesta_post[i].ind_nombre_detalle,
                            respuesta_post[i].num_dias_utiles,
                            respuesta_post[i].fecha_inicio,
                            respuesta_post[i].fecha_fin,
                            botonEliminar+" "+botonModificar
                        ]).draw()
                                .nodes()
                                .to$()
                        $a++;
                    }

                    $("#utilizacion").html(botonUtilizacion);

                }
            },'json');
        });

        $('#datatable2').on( 'click', '.eliminar', function () {
            var pk_num_vacacion_utilizacion=$(this).attr('pk_num_vacacion_utilizacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $urlEliminar='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/EliminarUtilizacionMET';
                $.post($urlEliminar, { pk_num_vacacion_utilizacion: pk_num_vacacion_utilizacion},function(respuesta_post3){

                        var tabla_listado3 = $('#datatable2').DataTable();
                        tabla_listado3.clear().draw();
                        var $a=1;
                        if(respuesta_post3 != -1) {
                            for(var i=0; i<respuesta_post3.length; i++) {
                                var codigoDetalle = respuesta_post3[i].cod_detalle;


                                var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descripcion="El usuario ha eliminado una utilizacion" titulo="¿Estás Seguro?" title="Eliminar Utilización" mensaje="¿Estás seguro de eliminar la utilizacion?" boton="si, Eliminar" pk_num_vacacion_utilizacion="'+respuesta_post3[i].pk_num_vacacion_utilizacion+'"><i class="md md-delete" style="color: #ffffff;"></i></button>';
                                var botonModificar = '<button class="modificarUtilizacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" title="Editar" descripcion="El Usuario a Modificado una utilización" titulo="Modificar Utilización" pk_num_vacacion_utilizacion="'+respuesta_post3[i].pk_num_vacacion_utilizacion+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button>';

                                tabla_listado3.row.add([
                                    $a,
                                    respuesta_post3[i].ind_nombre_detalle,
                                    respuesta_post3[i].num_dias_utiles,
                                    respuesta_post3[i].fecha_inicio,
                                    respuesta_post3[i].fecha_fin,
                                    botonEliminar+" "+botonModificar
                                ]).draw()
                                        .nodes()
                                        .to$()

                                $a++;
                            }
                        }

                    },'json');
                var $pkNumEmpleado = $("#funcionario").val();
                $.post($urlPendientes,{ pk_num_empleado: $pkNumEmpleado },function(dato){
                    $('#diasPendientes').val(dato['diasPendientes']);
                },'json');
                var $url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ListarPeriodoMET';
                $.post($url,{ pkNumEmpleado: $pkNumEmpleado },function(dato){
                    var tabla = $('#datatable5').DataTable();
                    tabla.clear().draw();
                    if(dato != -1) {
                        var $i = dato.length;
                        for(var i=0; i<dato.length; i++) {
                            var anioAnterior = dato[i].num_anio-1;
                            if(dato[i].num_pendientes>0){
                                var periodo = '<button class="periodo" periodo="'+dato[i].pk_num_periodo+'" style="font-weight:bold;">'+anioAnterior+ ' - ' + dato[i].num_anio+'</button>';
                            } else {
                                var periodo = '<button class="periodo" periodo="'+dato[i].pk_num_periodo+'">'+anioAnterior+ ' - ' + dato[i].num_anio+'</button>';
                            }
                            if(dato[i].num_mes<10){
                                var num_mes = '0'+dato[i].num_mes;
                            } else {
                                var num_mes = dato[i].num_mes;
                            }
                            var fila = tabla.row.add([
                                $i,
                                periodo,
                                num_mes,
                                dato[i].num_dias_derecho,
                                dato[i].num_dias_gozados,
                                dato[i].num_dias_interrumpidos,
                                dato[i].num_total_utilizados,
                                dato[i].num_pendientes,
                                ''
                            ]).draw()
                                    .nodes()
                                    .to$()

                            tabla.order([1,'desc']);

                            $i--;
                            $(fila).attr('id','pk_num_periodo'+dato[i].pk_num_periodo+'');
                        }
                    }
                },'json');
                swal("Eliminado!", "La utilizacion fue eliminada exitosamente", "success");
                $('#cerrar').click();
            });
        });


    });
</script>