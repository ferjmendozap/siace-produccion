<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idPatrimonioVehiculo}" name="idPatrimonioVehiculo"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a006_num_miscelaneo_detalle_marca" name="form[int][fk_a006_num_miscelaneo_detalle_marca]" class="form-control input-sm" required data-msg-required="Seleccione Marca Vehiculo">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoMarcaVehiculo}
                                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_marca)}
                                                        {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_marca}
                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a006_num_miscelaneo_detalle_marca">Marca Vehiculo</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="100" id="ind_modelo" name="form[txt][ind_modelo]" value="{if isset($formDB.ind_modelo)}{$formDB.ind_modelo}{/if}" required data-msg-required="Indique Modelo" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_modelo">Modelo</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="fec_anio" maxlength="4"  name="form[txt][fec_anio]" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{/if}" required data-msg-required="Indique Año" data-rule-number="true" data-msg-number="Por favor Introduzca Año Valido">
                                            <label for="fec_anio">Año</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="fk_a006_num_miscelaneo_detalle_color" name="form[int][fk_a006_num_miscelaneo_detalle_color]" class="form-control input-sm" required data-msg-required="Indique Color">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoColores}
                                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_color)}
                                                        {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_color}
                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a006_num_miscelaneo_detalle_color">Color</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="ind_placa" name="form[txt][ind_placa]" value="{if isset($formDB.ind_placa)}{$formDB.ind_placa}{/if}" required data-msg-required="Indique Placa del Vehiculo" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_placa">Placa</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm bolivar-mask" id="num_valor" name="form[int][num_valor]" value="{if isset($formDB.num_valor)}{$formDB.num_valor|number_format:2:",":"."}{/if}">
                                            <label for="num_valor">Valor</label>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">



    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/VehiculosMET', datos ,function(dato){

                if(dato['status']=='error'){

                    swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");

                }else if(dato['status']=='modificar'){

                    $(document.getElementById('idPatrimonioVehiculo'+dato['idPatrimonioVehiculo'])).html('<td width="20%">'+dato['marca_vehiculo']+'</td>' +
                            '<td width="20%">'+dato['ind_modelo']+'</td>' +
                            '<td width="10%">'+dato['fec_anio']+'</td>' +
                            '<td width="10%">'+dato['ind_placa']+'</td>' +
                            '<td width="15%">'+dato['color_vehiculo']+'</td>' +
                            '<td width="15%">'+dato['num_valor']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idPatrimonioVehiculo="'+dato['idPatrimonioVehiculo']+'"' +
                            'descipcion="El Usuario a Modificado Patrimonio Vehiculo" titulo="Modificar Registro Patrimonio Vehiculo">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioVehiculo="'+dato['idPatrimonioVehiculo']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Patrimonio Vehiculo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Vehiculo!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');
                    /*ACTUALIZO TOTALES*/
                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                        $("#inmuebleT").val(dato[0]);
                        $("#inversionT").val(dato[1]);
                        $("#vehiculoT").val(dato[2]);
                        $("#cuentaT").val(dato[3]);
                        $("#otrosT").val(dato[4]);
                        $("#total").val(dato[5]);
                    },'json');

                }else if(dato['status']=='creacion'){

                        $(document.getElementById('datatableVehiculo')).append('<tr id="idPatrimonioVehiculo'+dato['idPatrimonioVehiculo']+'" style="font-size: 11px">' +
                                '<td width="20%">'+dato['marca_vehiculo']+'</td>' +
                                '<td width="20%">'+dato['ind_modelo']+'</td>' +
                                '<td width="10%">'+dato['fec_anio']+'</td>' +
                                '<td width="10%">'+dato['ind_placa']+'</td>' +
                                '<td width="15%">'+dato['color_vehiculo']+'</td>' +
                                '<td width="15%">'+dato['num_valor']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idPatrimonioVehiculo="'+dato['idPatrimonioVehiculo']+'"' +
                                'descipcion="El Usuario a Modificado Patrimonio Vehiculo" titulo="Modificar Registro Patrimonio Vehiculo">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioVehiculo="'+dato['idPatrimonioVehiculo']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Patrimonio Vehiculo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Vehiculo!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                    /*ACTUALIZO TOTALES*/
                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                        $("#inmuebleT").val(dato[0]);
                        $("#inversionT").val(dato[1]);
                        $("#vehiculoT").val(dato[2]);
                        $("#cuentaT").val(dato[3]);
                        $("#otrosT").val(dato[4]);
                        $("#total").val(dato[5]);
                    },'json');
                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "55%" );


    });






</script>