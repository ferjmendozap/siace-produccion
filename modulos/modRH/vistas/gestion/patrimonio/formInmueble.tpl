<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idPatrimonioInmueble}" name="idPatrimonioInmueble"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>



                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="255" id="ind_descripcion" name="form[txt][ind_descripcion]" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" required data-msg-required="Indique Descripción del Inmueble" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_descripcion">Descripción</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="255" id="ind_ubicacion" name="form[txt][ind_ubicacion]" value="{if isset($formDB.ind_ubicacion)}{$formDB.ind_ubicacion}{/if}" required data-msg-required="Indique Ubicación del Inmueble" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_ubicacion">Ubicación</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="200" id="ind_uso" name="form[txt][ind_uso]" value="{if isset($formDB.ind_uso)}{$formDB.ind_uso}{/if}" required data-msg-required="Indique Uso del Inmueble" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_uso">Uso</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm bolivar-mask" id="num_valor" name="form[int][num_valor]" value="{if isset($formDB.num_valor)}{$formDB.num_valor|number_format:2:",":"."}{/if}">
                                            <label for="num_valor">Valor</label>
                                        </div>
                                    </div><!--end .col -->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" {if isset($formDB.num_flag_hipotecado) and $formDB.num_flag_hipotecado==1} checked{/if} value="1" name="form[int][num_flag_hipotecado]">
                                                <span>Hipotecado?</span>
                                            </label>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->

                                <div class="row">
                                    <br><br>
                                    <div align="right">
                                        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                    </div>
                                </div>


                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">



    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/InmueblesMET', datos ,function(dato){

                if(dato['status']=='modificar'){

                    $(document.getElementById('idPatrimonioInmueble'+dato['idPatrimonioInmueble'])).html('<td width="25%">'+dato['ind_descripcion']+'</td>' +
                            '<td width="25%">'+dato['ind_ubicacion']+'</td>' +
                            '<td width="25%">'+dato['ind_uso']+'</td>' +
                            '<td width="15%">'+dato['num_valor']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idPatrimonioInmueble="'+dato['idPatrimonioInmueble']+'"' +
                            'descripcion="El Usuario a Modificado Patrimonio Inmueble" titulo="Modificar Registro de Patrimonio Inmueble">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioInmueble="'+dato['idPatrimonioInmueble']+'"  boton="si, Eliminar"' +
                            'descripcion="El usuario a Eliminado Patrimonio Inmueble" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Inmueblel!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                            $("#inmuebleT").val(dato[0]);
                            $("#inversionT").val(dato[1]);
                            $("#vehiculoT").val(dato[2]);
                            $("#cuentaT").val(dato[3]);
                            $("#otrosT").val(dato[4]);
                            $("#total").val(dato[5]);
                    },'json');

                }else if(dato['status']=='creacion'){

                        $(document.getElementById('datatableInmuebles')).append('<tr id="idPatrimonioInmueble'+dato['idPatrimonioInmueble']+'" style="font-size: 11px">' +
                                '<td width="25%">'+dato['ind_descripcion']+'</td>' +
                                '<td width="25%">'+dato['ind_ubicacion']+'</td>' +
                                '<td width="25%">'+dato['ind_uso']+'</td>' +
                                '<td width="15%">'+dato['num_valor']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idPatrimonioInmueble="'+dato['idPatrimonioInmueble']+'"' +
                                'descripcion="El Usuario a Modificado Patrimonio Inmueble" titulo="Modificar Registro de Patrimonio Inmueble">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioInmueble="'+dato['idPatrimonioInmueble']+'"  boton="si, Eliminar"' +
                                'descripcion="El usuario a Eliminado Patrimonio Inmueble" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Inmueblel!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');

                        $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                                $("#inmuebleT").val(dato[0]);
                                $("#inversionT").val(dato[1]);
                                $("#vehiculoT").val(dato[2]);
                                $("#cuentaT").val(dato[3]);
                                $("#otrosT").val(dato[4]);
                                $("#total").val(dato[5]);
                        },'json');

                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "55%" );



    });






</script>