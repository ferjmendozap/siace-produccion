<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Lista de Capacitaciones</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-head">
                               <div class="tools">

                                   <div class="btn-group">
                                       <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
                                   </div>
                                </div>
                        </div>


                        <div class="card-body">
                            <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 6%">Cap.</th>
                                    <th class="col-sm-1">Tipo</th>
                                    <th class="col-sm-3">Curso</th>
                                    <th class="col-sm-3">Centro</th>
                                    <th class="col-sm-1">Inicio</th>
                                    <th class="col-sm-1">Estado</th>
                                    <th style="width: 4%">Mod</th>
                                    <th style="width: 4%">Ver</th>
                                    {if $tipo=='PE'}
                                        <th style="width: 4%">Accion</th>
                                    {/if}
                                    {if $tipo=='AP' || $tipo=='TODOS'}
                                        <th style="width: 4%">Accion</th>
                                    {/if}
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$listadoCapacitacion}


                                    <tr id="idCapacitacion{$dat.pk_num_capacitacion}" style="font-size: 11px">
                                        <td>{$dat.pk_num_capacitacion}</td>
                                        <td>{$dat.nombre_tipo_capacitacion}</td>
                                        <td>{$dat.ind_nombre_curso}</td>
                                        <td>{$dat.ind_nombre_institucion}</td>
                                        <td>{$dat.fec_fecha_inicio|date_format:"d-m-Y"}</td>
                                        <td>
                                            {if $dat.estado_capacitacion=='PE'}
                                                PENDIENTE
                                                {elseif $dat.estado_capacitacion=='AP'}
                                                    APROBADO
                                                {elseif $dat.estado_capacitacion=='IN'}
                                                    INICIADO
                                                {elseif $dat.estado_capacitacion=='TE'}
                                                    TERMINADO
                                                {else}
                                                    ANULADO
                                            {/if}
                                        </td>
                                        <td>
                                            {if in_array('RH-01-01-04-06-M',$_Parametros.perfil)}
                                                {if $dat.estado_capacitacion=='PE'}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  idCapacitacion="{$dat.pk_num_capacitacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                            descripcion="Modificar Capacitación" title="Editar Capacitación" titulo="Modificar Capacitación">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {/if}
                                        </td>
                                        <td>
                                            {if in_array('RH-01-01-04-07-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idCapacitacion="{$dat.pk_num_capacitacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="Ver Datos Capacitación" title="Ver Datos Capacitación" titulo="Ver Datos Capacitación">
                                                <i class="fa md-search" style="color: #ffffff;"></i>
                                            </button>
                                            {/if}
                                        </td>
                                        {if $dat.estado_capacitacion=='PE'}
                                            <td>
                                                {if in_array('RH-01-01-04-09-AP',$_Parametros.perfil)}
                                                <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idCapacitacion="{$dat.pk_num_capacitacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descipcion="Aprobar Capacitación" title="Aprobar Capacitación" titulo="Aprobar Capacitación">
                                                    APROBAR
                                                </button>
                                                {/if}
                                            </td>
                                        {elseif $dat.estado_capacitacion=='AP'}
                                            <td>
                                                {if in_array('RH-01-01-04-10-IN',$_Parametros.perfil)}
                                                    <button class="iniciar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idCapacitacion="{$dat.pk_num_capacitacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                            descipcion="Iniciar Capacitación" title="Iniciar Capacitación" titulo="Iniciar Capacitación">
                                                        INICIAR
                                                    </button>
                                                {/if}
                                            </td>
                                        {elseif $dat.estado_capacitacion=='IN'}
                                            <td>
                                                {if in_array('RH-01-01-04-11-TE',$_Parametros.perfil)}
                                                    <button class="terminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idCapacitacion="{$dat.pk_num_capacitacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                            descipcion="Terminar Capacitación" title="Terminar Capacitación" titulo="Terminar Capacitación">
                                                        TERMINAR
                                                    </button>
                                                {/if}
                                            </td>
                                        {else}
                                            <td></td>
                                        {/if}

                                    </tr>


                                {/foreach}

                                </tbody>
                                <tfoot>
                                    <th colspan="9">
                                        {if in_array('RH-01-01-04-05-N',$_Parametros.perfil)}
                                            <button class="nuevaCap logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descripcion="Nueva Capacitacion" titulo="Registrar Nueva Capacitación"  id="nuevaCap"
                                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                                                <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nueva Capacitación
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- filtro de busqueda de las capacitaciones -->
<!-- filtro de busqueda del empleado -->
<div class="offcanvas">

    <div id="offcanvas-filtro" class="offcanvas-pane width-10">

        <div class="offcanvas-head">
            <header>Filtro de Busqueda</header>
            <div class="offcanvas-tools">
                <a class="cerrar btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form id="filtroCapacitaciones" class="form" role="form" method="post">
                <input type="hidden" name="filtro" value="1">

                <!--ORGANISMO-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" required data-msg-required="Seleccione Organismo">
                                {foreach item=dat from=$listadoOrganismos}
                                    {if isset($formDB.fk_a001_num_organismo)}
                                        {if $dat.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                            <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {else}
                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {/if}
                                    {else}
                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Organismo</label>
                        </div>
                    </div>
                </div>
                <!--CURSOS-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="pk_num_curso" name="pk_num_curso" class="form-control input-sm select2-list select2">
                                <option value="">&nbsp;</option>
                                {foreach item=i from=$listadoCursos}

                                    <option value="{$i.pk_num_curso}">{$i.ind_nombre_curso}</option>

                                {/foreach}
                            </select>
                            <label for="pk_num_curso">Cursos</label>
                        </div>
                    </div>
                </div>
                <!-- CENTROS DE ESTUDIOS -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="pk_num_institucion" name="pk_num_institucion" class="form-control input-sm select2-list select2">
                                <option value="">&nbsp;</option>
                                {foreach item=i from=$listadoCentrosEstudios}

                                    <option value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion}</option>

                                {/foreach}
                            </select>
                            <label for="pk_num_institucion">Centro de Estudio</label>
                        </div>
                    </div>
                </div>
                <!--TIPO DE CAPACITACION-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="tipo_cap" name="tipo_cap" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {foreach item=i from=$miscelaneoTipoCap}
                                    <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                {/foreach}
                            </select>
                            <label for="tipo_cap">Tipo de Capacitación</label>
                        </div>
                    </div>
                </div>
                <!--MODALIDAD-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="modalidad" name="modalidad" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {foreach item=i from=$miscelaneoModalidad}
                                    <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                {/foreach}
                            </select>
                            <label for="modalidad">Modalidad</label>
                        </div>
                    </div>
                </div>
                <!--ESTADO-->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="Estado" name="Estado" class="form-control input-sm">
                                {foreach key=key item=item from=$EstadoRegistro}

                                    {if $key==$EstadoPorDefecto}
                                        <option selected value="{$key}">{$item}</option>
                                    {else}
                                        <option value="{$key}">{$item}</option>
                                    {/if}

                                {/foreach}
                            </select>
                            <label for="Estado">Estado</label>
                        </div>
                    </div>
                </div>
                <!--FECHA INGRESO-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-daterange input-group" id="rango-fechas">
                                <div class="input-group-content">
                                    <input type="text" class="form-control" name="fecha_inicio" id="fecha_inicio" />
                                </div>
                                <span class="input-group-addon">a</span>
                                <div class="input-group-content">
                                    <input type="text" class="form-control" name="fecha_fin" id="fecha_fin"/>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <label for="">Fecha de Ingreso</label>
                        </div>
                    </div>
                </div>
                <!--BOTON-->
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltroCapacitaciones">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function() {

        var $url_registrar='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/RegistrarCapacitacionMET';
        var $url_Aprobar ='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/AprobarCapacitacionFormMET';
        var $url_Iniciar ='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/IniciarCapacitacionFormMET';
        var $url_Terminar='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/TerminarCapacitacionFormMET';
        var $url_Ver='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/VerCapacitacionFormMET';
        var $url_Modificar ='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/ModificarCapacitacionFormMET';


        //NUEVA CAPACITACION
        $('#nuevaCap').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_registrar, { operacion: 'REGISTRAR' } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //VER
        $('#datatable1 tbody').on( 'click', '.ver', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_Ver, { idCapacitacion: $(this).attr('idCapacitacion'), tipo:'VE' } ,function($dato){
                $('#ContenidoModal').html($dato);
            });

        });

        //MODIFICAR
        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_Modificar, { idCapacitacion: $(this).attr('idCapacitacion') } ,function($dato){
                $('#ContenidoModal').html($dato);
            });

        });

        //APROBAR
        $('#datatable1 tbody').on( 'click', '.aprobar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_Aprobar, { idCapacitacion: $(this).attr('idCapacitacion') } ,function($dato){
                $('#ContenidoModal').html($dato);
            });

        });

        //INICIAR
        $('#datatable1 tbody').on( 'click', '.iniciar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_Iniciar, { idCapacitacion: $(this).attr('idCapacitacion'), tipo:'IN' } ,function($dato){
                $('#ContenidoModal').html($dato);
            });

        });

        //TERMINAR
        $('#datatable1 tbody').on( 'click', '.terminar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_Terminar, { idCapacitacion: $(this).attr('idCapacitacion'), tipo:'TE' } ,function($dato){
                $('#ContenidoModal').html($dato);
            });

        });


        $("#filtroCapacitaciones").validate({
            submitHandler: function(form) {
                var pk_num_organismo = $("#fk_a001_num_organismo").val();
                var curso = $("#pk_num_curso").val();
                var institucion = $("#pk_num_institucion").val();
                var tipo_cap = $("#tipo_cap").val();
                var modalidad = $("#modalidad").val();
                var estado_registro = $("#Estado").val();
                var fecha_inicio = $("#fecha_inicio").val();
                var fecha_fin = $("#fecha_fin").val();

                var url_listar='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/BuscarCapacitacionesMET';
                $.post(url_listar,{ pk_num_organismo: pk_num_organismo, curso: curso, institucion: institucion, tipo_cap: tipo_cap, modalidad: modalidad, estado_registro: estado_registro, fecha_inicio: fecha_inicio, fecha_fin: fecha_fin },function(respuesta_post) {
                    var tabla_listado = $('#datatable1').DataTable();
                    tabla_listado.clear().draw();
                    if(respuesta_post != -1) {
                        for(var i=0; i<respuesta_post.length; i++) {


                            var aux = respuesta_post[i].fec_fecha_inicio;

                            var fecha = aux.split("-");

                            var ver  = '<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idCapacitacion="'+respuesta_post[i].pk_num_capacitacion+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Ver Datos Capacitación" titulo="Ver Datos Capacitación"><i class="fa md-search" style="color: #ffffff;"></i></button>';

                            var estado='';
                            var mod = '';
                            var accion = '';

                            if(respuesta_post[i].estado_capacitacion=='PE'){
                                estado='PENDIENTE';
                                mod = '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  idCapacitacion="'+respuesta_post[i].pk_num_capacitacion+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Modificar Capacitación" titulo="Modificar Capacitación"><i class="fa md-edit" style="color: #ffffff;"></i></button>';
                                accion= '<button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idCapacitacion="'+respuesta_post[i].pk_num_capacitacion+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Aprobar Capacitación" titulo="Aprobar Capacitación">APROBAR</button>';
                            }
                            if(respuesta_post[i].estado_capacitacion=='AP'){
                                estado='APROBADO';
                                mod ='';
                                accion= '<button class="iniciar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idCapacitacion="'+respuesta_post[i].pk_num_capacitacion+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descipcion="Iniciar Capacitación" titulo="Iniciar Capacitación">INICIAR</button>';
                            }
                            if(respuesta_post[i].estado_capacitacion=='IN'){
                                estado='INICIADO';
                                mod ='';
                                accion='<button class="terminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idCapacitacion="'+respuesta_post[i].pk_num_capacitacion+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descipcion="Terminar Capacitación" titulo="Terminar Capacitación">TERMINAR</button>';
                            }
                            if(respuesta_post[i].estado_capacitacion=='TE'){
                                estado='TERMINADO';
                                mod ='';
                                accion ='';

                            }

                            var fila = tabla_listado.row.add([
                                respuesta_post[i].pk_num_capacitacion,
                                respuesta_post[i].nombre_tipo_capacitacion,
                                respuesta_post[i].ind_nombre_curso,
                                respuesta_post[i].ind_nombre_institucion,
                                fecha[2]+"-"+fecha[1]+"-"+fecha[0],
                                estado,
                                mod,
                                ver,
                                accion
                            ]).draw()
                                    .nodes()
                                    .to$()
                                    .addClass( 'letra_pequena' );
                            $(fila)
                                    .css('font-size','11px')
                                    .attr('id','idCapacitacion'+respuesta_post[i].pk_num_capacitacion+'')

                        }
                        $('#filtroCapacitaciones').each(function () { {*resetear los campos del formulario*}
                            this.reset();
                        });
                        $(".cerrar").click();
                    }

                },'json');
            }
        });





    });
</script>