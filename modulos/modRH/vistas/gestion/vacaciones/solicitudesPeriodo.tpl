<div class="form floating-label">
	<div class="modal-body" >
		<h4 class="text-primary">&nbsp;Periodo {$periodoVacacion.num_anio-1} - {$periodoVacacion.num_anio}</h4>
		<table id="datatable3" class="table table-striped table-hover">
			<thead>
			<tr align="center">
				<th><i class="glyphicon glyphicon-th"></i> N° de solicitud</th>
				<th><i class="md md-brightness-low"></i> Dias Solicitados</th>
				<th><i class="glyphicon glyphicon-calendar"></i> Fecha de Salida</th>
				<th><i class="glyphicon glyphicon-calendar"></i> Fecha de Término</th>
				<th><i class="glyphicon glyphicon-calendar"></i> Fecha de Incorporación</th>
			</tr>
			</thead>
			<tbody>
			{foreach item=solicitud from=$solicitudVacacion}
				<tr align="center">
					<td>{$solicitud.pk_num_solicitud_vacacion}</td>
					<td>{$solicitud.num_dias_solicitados}</td>
					<td>{$solicitud.fec_salida|date_format:"%d/%m/%Y"}</td>
					<td>{$solicitud.fec_termino|date_format:"%d/%m/%Y"}</td>
					<td>{$solicitud.fec_incorporacion|date_format:"%d/%m/%Y"}</td>
				</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">

	$(document).ready(function() {
		$('#datatable3').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );
	} );

	if ( $.fn.dataTable.isDataTable( '#datatable3' ) ) {
		table = $('#datatable3').DataTable();
	}
	else {
		table = $('#datatable3').DataTable( {
			paging: true
		} );
	}


</script>