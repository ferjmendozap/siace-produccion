<form action="{$_Parametros.url}modRH/gestion/vacacionesCONTROL/EditarVacacionMET" name="formAjax" id="formAjax" method="post"  class="form" role="form">
	<input type="hidden" value="1" name="valido" />
	<input type="hidden" id="num_pendiente" value="{$dias.diasPendientes}"/>
	<input type="hidden" id="fechaTermino" name="fechaTermino" value="{$solicitud.fec_termino|date_format:"%d/%m/%Y"}"/>
	<input type="hidden" id="fechaIncorporacion" name="fechaIncorporacion" value="{$solicitud.fec_incorporacion|date_format:"%d/%m/%Y"}"/>
	<input type="hidden" value="{$solicitud.pk_num_solicitud_vacacion}" name="pk_num_solicitud_vacacion" />
	<input type="hidden" value="{$solicitud.num_dias_solicitados}" name="dias"/>
	<input type="hidden" value="{$solicitud.fec_salida}" name="salida" />
	<input type="hidden" value="{$estado}" name="estadoLista" />

	<div class="col-lg-12">
        {if $solicitud.codDetalle==1}
			<div class="card-head">
				<ul class="nav nav-tabs pull-right" data-toggle="tabs">
					<li class="active"><a href="#first2">Información General</a></li>
					<li><a href="#second2">Periodos Disponibles</a></li>
				</ul>
			</div>
        {/if}
		<div class="card-body tab-content">
			<div class="tab-pane active" id="first2">
				<h3 class="text-primary">Datos del Empleado</h3>
				<div class="col-md-6 col-sm-6">
					<div class="form-group ">
						<select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty">
							<option selected>{$solicitud.ind_descripcion_empresa}</option>
						</select>
						<label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="form-group ">
						<select name="pk_num_empleado_editar" id="pk_num_empleado_editar" class="form-control">
							<option selected value="{$solicitud.pk_num_empleado}">{$solicitud.ind_nombre1} {$solicitud.ind_nombre2} {$solicitud.ind_apellido1} {$solicitud.ind_apellido2}</option>
						</select>
						<label for="pk_num_empleado_editar"><i class="glyphicon glyphicon-user"></i> Empleado</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="form-group ">
						<select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty">
							<option selected>{$solicitud.ind_dependencia}</option>
						</select>
						<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h4>Total de dias pendientes: {$dias.diasPendientes}</h4>
						</div>
					</div>
				</div>
				<h3 class="text-primary">Información de la Solicitud</h3>
				<div class="col-md-12 col-sm-12">
					<div class="col-md-6 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled value="{$solicitud.pk_num_solicitud_vacacion}">
							<label class="control-label"><i class="glyphicon glyphicon-th"></i> N° de Solicitud</label>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" disabled value="{$solicitud.cedula}">
							<label class="control-label"><i class="glyphicon glyphicon-th"></i> Documento</label>
						</div>
						<div class="form-group ">
							<select id="tipo_vacacion" name="tipo_vacacion" class="form-control">
								<option selected>{$solicitud.tipo_vacacion}</option>
							</select>
							<label for="tipo_vacacion"><i class="glyphicon glyphicon-triangle-right"></i> Tipo</label>
						</div>
						<div class="form-group">
							<textarea class="form-control" rows="4" name="ind_motivo" id="ind_motivo">{$solicitud.ind_motivo}</textarea>
							<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Justificación</label>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled>
							<label class="control-label">N° de Otorgamiento</label>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" disabled value="{$solicitud.ind_estado}">
							<label class="control-label">Estado</label>
						</div>
						<div class="form-group">
							<input type="text" disabled class="form-control" value="{$solicitud.fec_solicitud|date_format:"%Y-%m"}">
							<label class="control-label">Periodo</label>
						</div>
						<div class="form-group">
							<input type="text" disabled class="form-control" value="{$solicitud.fec_solicitud|date_format:"%d/%m/%Y"}">
							<label class="control-label">Fecha</label>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" disabled value="{$solicitud.creado}">
							<label class="control-label">Creado por</label>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12">

					<div class="well clearfix">
							<div class="col-md-2 col-sm-2">
								<div class="form-group ">
									<select id="periodo_vacacion" name="periodo_vacacion" class="form-control dirty">
                                        {foreach item=vp from=$periodo}
                                            {if $vp.num_pendientes>0}
												<option value="{$vp.num_pendientes}">{$vp.num_anio-1}-{$vp.num_anio} ({$vp.num_pendientes})</option>
                                            {/if}
                                        {/foreach}
									</select>
									<label for="periodo_vacacion"><i class="glyphicon glyphicon-triangle-right"></i> Detalle</label>
								</div>
							</div>

							<div class="col-md-1 col-sm-1">
								<div class="form-group">
									<input type="text" name="num_dias" id="num_dias" class="form-control dirty" onchange="calcularFechas(this.value)" value="{$solicitud.num_dias_solicitados}">
									<label class="control-label">Nro Dias</label>
								</div>
							</div>

						<div class="col-md-3 col-sm-3">
							<div class="form-group control-width-normal">
								<div class="input-group date" id="demo-date">
									<div class="input-group-content">
										<input type="text" class="form-control" id="fecha_salida" name="fecha_salida" value="{$solicitud.fec_salida|date_format:"%d/%m/%Y"}" onchange="calcular(this.value)">
										<label>Fecha de Salida</label>
									</div>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>

						<div class="col-md-3 col-sm-3">
							<div class="form-group control-width-normal">
								<div class="input-group date" id="demo-date">
									<div class="input-group-content">
										<input type="text" class="form-control dirty" id="fecha_termino" name="fecha_termino" disabled value="{$solicitud.fec_termino|date_format:"%d/%m/%Y"}">
										<label>Fecha de Término</label>
									</div>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="form-group control-width-normal">
								<div class="input-group date" id="demo-date">
									<div class="input-group-content">
										<input type="text" class="form-control dirty" id="fecha_incorporacion"  name="fecha_incorporacion" disabled value="{$solicitud.fec_incorporacion|date_format:"%d/%m/%Y"}">
										<label>Fecha de Incorporación</label>
									</div>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div  class="col-md-12 col-sm-12">
					<div align="right">
						<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar </button>&nbsp;&nbsp;


						<button type="submit" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar Cambios</button>

					</div>
				</div>
			</div>
			<div class="tab-pane" id="second2">
				<table id="datatable2" class="table table-striped table-hover">
					<thead>
					<tr align="center">
						<th><i class="glyphicon glyphicon-th"></i> N°</th>
						<th><i class="md-person"></i> Periodo</th>
						<th><i class="md md-brightness-low"></i> Dias</th>
						<th><i class="glyphicon glyphicon-calendar"></i> Fecha Inicial</th>
						<th><i class="glyphicon glyphicon-calendar"></i> Fecha Final</th>
						<th><i class="md md-brightness-low"></i> Dias por Derecho</th>
						<th><i class="md md-brightness-low"></i> Dias Usados</th>
						<th><i class="md md-brightness-low"></i> Dias Pendientes</th>
					</tr>
					</thead>
					<tbody>
					{assign var=numero value=1}
					{assign var=acumuladorPendiente value=0}
					{assign var=num_dias value=0}
					{foreach item=periodo from=$vacacionPeriodo}
						<tr id="pk_num_periodo{$periodo.pk_num_periodo}">
							<td align="center">{$numero++}</td>
							<td>{$periodo.num_anio-1} - {$periodo.num_anio}</td>
							<td align="center">{$periodo.num_dias_detalle}</td>
							<td>{$periodo.fec_inicial}</td>
							<td>{$periodo.fec_final}</td>
							<td align="center">{$periodo.num_dias_derecho}</td>
							<td align="center">{$periodo.num_total_utilizados}</td>
							<td align="center">{$periodo.num_pendientes}</td>
						</tr>
						{$acumuladorPendiente = $acumuladorPendiente + $periodo.num_pendientes}
						{$num_dias = $num_dias + $periodo.num_dias_detalle}
					{/foreach}
					<tr>
						<td></td>
						<td></td>
						<td align="center"><b>{$num_dias}</b></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td align="center"><b>{$acumuladorPendiente}</b></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#fecha_salida").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
    $("#periodo_vacacion").on('change',function () {
        $('#num_dias').val($(this).val());
        calcularFechas($(this).val());
    });

	$('#periodoTotal').multiSelect({ selectableOptgroup: true });

	function procesarPeriodos() {
		var periodo = $("#periodoTotal").val();
		var fecha_salida = $("#fecha_salida").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ConsultarPeriodosMET';
		$.post(url,{ periodo: periodo, fecha_salida: fecha_salida },function(dato){
			$("#num_dias").val(dato['periodo']);
			$("#fecha_termino").val(dato['fechaTermino']);
			$("#fecha_incorporacion").val(dato['fechaIncorporacion']);
			$("#fechaTermino").val(dato['fechaTermino']);
			$("#fechaIncorporacion").val(dato['fechaIncorporacion']);
		},'json');
		// Actualizar Periodo
		var urlPeriodoSolicitud = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CargarPeriodosMET';
		var pk_num_empleado = $("#pk_num_empleado_editar").val();
		$.post(urlPeriodoSolicitud,{ periodo: periodo, fecha_salida: fecha_salida, metodo: 1, pk_num_empleado: pk_num_empleado },function(respuesta_post){
			var tabla_listado = $('#datatable2').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					tabla_listado.row.add([
						respuesta_post[i].numero,
						respuesta_post[i].anioAnterior+ '-' + respuesta_post[i].anio,
						respuesta_post[i].dias,
						respuesta_post[i].fecha_salida,
						respuesta_post[i].fecha_termino,
						respuesta_post[i].dias_derecho,
						respuesta_post[i].dias_usados,
						respuesta_post[i].dias_pendientes
					]).draw()
							.nodes()
							.to$()
				}
			}
		},'json');
	}

	function calcularFechas(dias)
	{
		var fecha_salida = $("#fecha_salida").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularFechasMET';
		$.post(url,{ dias: dias, fecha_salida: fecha_salida },function(dato){
			$("#num_dias").val(dato['periodo']);
			$("#fecha_termino").val(dato['fechaTermino']);
			$("#fecha_incorporacion").val(dato['fechaIncorporacion']);
			$("#fechaTermino").val(dato['fechaTermino']);
			$("#fechaIncorporacion").val(dato['fechaIncorporacion']);
		},'json');
		var pk_num_empleado = $("#pk_num_empleado_editar").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ComprobarDiasMET';
		//$.post(url,{ pkNumEmpleado: pk_num_empleado},function(dato){
        if(parseInt($("#periodo_vacacion").val()) < parseInt(dias)){
            $("#num_dias").val($("#periodo_vacacion").val());
            calcularFechas($("#periodo_vacacion").val());
            swal("No dispone de la cantidad de dias solicitados");
        }
		/*
			if(dato.num_dias<dias){
				swal("No dispone de la cantidad de dias solicitados");
				$("#botonGuardar").html("");
			} else {
				$("#botonGuardar").html('<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>&nbsp;&nbsp;<button type="submit" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar Cambios</button>');
			}*/
		//},'json');
		// Actualizar Periodo
		var urlPeriodoSolicitud = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CargarPeriodosMET';
		var pk_num_empleado = $("#pk_num_empleado_editar").val();
		$.post(urlPeriodoSolicitud,{ periodo: dias, fecha_salida: fecha_salida, metodo: 2, pk_num_empleado: pk_num_empleado },function(respuesta_post){
			var tabla_listado = $('#datatable2').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					tabla_listado.row.add([
						respuesta_post[i].numero,
						respuesta_post[i].anioAnterior+ '-' + respuesta_post[i].anio,
						respuesta_post[i].dias,
						respuesta_post[i].fecha_salida,
						respuesta_post[i].fecha_termino,
						respuesta_post[i].dias_derecho,
						respuesta_post[i].dias_usados,
						respuesta_post[i].dias_pendientes
					]).draw()
							.nodes()
							.to$()
				}
			}
		},'json');
	}


	function calcular(fecha_salida)
	{
		var dias = $("#num_dias").val();
		if(dias>0){
			var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularFechasMET';
			$.post(url,{ dias: dias, fecha_salida: fecha_salida },function(dato){
				$("#num_dias").val(dato['periodo']);
				$("#fecha_termino").val(dato['fechaTermino']);
				$("#fecha_incorporacion").val(dato['fechaIncorporacion']);
				$("#fechaTermino").val(dato['fechaTermino']);
				$("#fechaIncorporacion").val(dato['fechaIncorporacion']);
			},'json');
			// Actualizar Periodo
			var urlPeriodoSolicitud = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CargarPeriodosMET';
			var pk_num_empleado = $("#pk_num_empleado_editar").val();
			var dias = 0;
			var pendientes = 0;
			$.post(urlPeriodoSolicitud,{ periodo: dias, fecha_salida: fecha_salida, metodo: 2, pk_num_empleado: pk_num_empleado },function(respuesta_post){
				var tabla_listado = $('#datatable2').DataTable();
				tabla_listado.clear().draw();
				if(respuesta_post != -1) {
					for(var i=0; i<respuesta_post.length; i++) {
						dias = dias + respuesta_post[i].dias;
						pendientes = pendientes + respuesta_post[i].dias_pendientes;
						tabla_listado.row.add([
							respuesta_post[i].numero,
							respuesta_post[i].anioAnterior+ '-' + respuesta_post[i].anio,
							respuesta_post[i].dias,
							respuesta_post[i].fecha_salida,
							respuesta_post[i].fecha_termino,
							respuesta_post[i].dias_derecho,
							respuesta_post[i].dias_usados,
							respuesta_post[i].dias_pendientes
						]).draw()
								.nodes()
								.to$()

					}
				}
			},'json');
		}
	}

	$('#datatable1 tbody').on( 'click', '.vacacion', function () {
		$('#modalAncho').css( "width", "85%" );
		$('#formModalLabel').html($(this).attr('titulo'));
		$('#ContenidoModal').html("");
		var pk_num_solicitud_vacacion = $(this).attr('pk_num_solicitud_vacacion');
		var urlReporte = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ReporteVacacionMET/?pk_num_solicitud_vacacion='+pk_num_solicitud_vacacion;
		$('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
	});

	$(document).ready(function () {
		//validation rules
//        $('#num_dias').val($("#periodo_vacacion").val());
		$("#formAjax").validate({
			rules:{
				fecha_salida:{
					required:true
				},
				num_dias:{
					required:true
				}
			},
			messages:{
				fecha_salida:{
					required:true
				},
				num_dias:{
					required:true
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr("action"), $(form).serialize(), function (dato) {
					$('#pk_num_solicitud_vacacion'+dato['pk_num_solicitud_vacacion']).remove();
					$(document.getElementById('datatable1')).append('<tr id="pk_num_solicitud_vacacion'+dato['pk_num_solicitud_vacacion']+'">' +
					'<td>'+dato['pk_num_solicitud_vacacion']+'</td><td>'+dato['empleado']+'</td><td>'+dato['tipo_vacacion']+'</td><td>'+dato['fec_solicitud']+'</td><td>'+dato['ind_estado']+'</td>' +
					'<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un permiso" title="Ver solicitud de vacaciones" titulo="Ver solicitud de vacaciones" data-toggle="modal" data-target="#formModal" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="glyphicon glyphicon-eye" style="color: #ffffff;"></i></button></td>' +
					'<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha modificado un permiso" titulo="Modificar Permiso" title="Modificar Permiso" data-toggle="modal" data-target="#formModal" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>' +
					'<td align="center"><button class="vacacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver Solicitud de Vacación" titulo="Ver Solicitud de Vacación" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="md md-attach-file"></i></button></td></tr>');

                    swal(dato['Mensaje'] + '!!', dato['contenido'], "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				}, 'json');
			}
		});
	});
</script>
