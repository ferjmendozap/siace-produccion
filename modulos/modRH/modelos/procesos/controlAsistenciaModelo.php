<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-02-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/


class controlAsistenciaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #permite transferir los eventos del archivo Excel Ods a la base de datos para procesarlos
    public function metTransferirEventos($eventos)
    {

        $this->_db->beginTransaction();
        #inserto eventos
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c104_transferencia_eventos
             SET
                fec_fecha=:fec_fecha,
                fec_hora=:fec_hora,
                ind_evento=:ind_evento,
                ind_cedula_empleado=:ind_cedula_empleado
            "
        );

        foreach($eventos as $registros){

            $fecha = explode("/",$registros['fecha']);

            $registro->execute(array(
                'fec_fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                'fec_hora' => $registros['hora'],
                'ind_evento' => $registros['evento'],
                'ind_cedula_empleado' => $registros['cedula']
            ));

        }
        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return 1;
        }


    }

    #permite leer los datos transferidos y almacenarlos relacionados a cada empleado
    public function metControlAsistencia()
    {

        $this->_db->beginTransaction();

        #consulto empleados
        $con =  $this->_db->query("
            SELECT
                me.pk_num_empleado,
                mp.pk_num_persona,
                mp.ind_cedula_documento,
                CONCAT_WS(' ',mp.ind_nombre1,mp.ind_nombre2,mp.ind_apellido1,mp.ind_apellido2) AS nombre_empleado,
                d.pk_num_dependencia,
                d.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo
            FROM
                rh_b001_empleado AS me
            INNER JOIN a003_persona AS mp ON mp.pk_num_persona = me.fk_a003_num_persona
            INNER JOIN rh_c076_empleado_organizacion AS mo ON me.pk_num_empleado = mo.fk_rhb001_num_empleado
            INNER JOIN a004_dependencia AS d ON d.pk_num_dependencia = mo.fk_a004_num_dependencia
            INNER JOIN rh_c005_empleado_laboral ON me.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
            INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetchAll();


        #ciclo de empleados para verificar y procesar los eventos transferidos
        foreach($resultado as $registros){

            $empleado = $registros['pk_num_empleado'];
            $cedula   = $registros['ind_cedula_documento'];

            $consulta = $this->_db->query("
                select * from rh_c104_transferencia_eventos WHERE ind_cedula_empleado = '$registros[ind_cedula_documento]'
            ");
            $consulta->setFetchMode(PDO::FETCH_ASSOC);
            $resultado2 = $consulta->fetchAll();


            $hora='';

            $registro = $this->_db->prepare(
                "INSERT INTO
                    rh_c105_controlasistencia
                 SET
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                    ind_cedula_empleado=:ind_cedula_empleado,
                    fec_fecha=:fec_fecha,
                    fec_hora=:fec_hora,
                    fec_fecha_format=:fec_fecha_format,
                    fec_hora_format=:fec_hora_format,
                    ind_evento=:ind_evento,
                    ind_estado=:ind_estado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                "
                );

            foreach($resultado2 as $registros2){


                    $auxf = explode("-",$registros2['fec_fecha']);
                    $auxh = $registros2['fec_hora'];

                    $fec_fecha = $auxf[2]."-".$auxf[1]."-".$auxf[0];
                    $fec_hora  = strftime('%I:%M:%S %P',strtotime($auxh));
                    $fec_fecha_format = $registros2['fec_fecha'];
                    $fec_hora_format  = $registros2['fec_hora'];
                    $evento = $registros2['ind_evento'];
                    $estado = 'S';

                    $registro->execute(array(
                        'fk_rhb001_num_empleado' => $registros['pk_num_empleado'],
                        'ind_cedula_empleado' => $registros['ind_cedula_documento'],
                        'fec_fecha' => $fec_fecha,
                        'fec_hora' => $fec_hora,
                        'fec_fecha_format' => $fec_fecha_format,
                        'fec_hora_format' => $fec_hora_format,
                        'ind_evento' => $evento,
                        'ind_estado' => $estado
                    ));



            }//fin foreach

            $error = $registro->errorInfo();


        }//fin foreach

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return 1;
        }

    }

    #permite listar los eventos pendientes por procesar
    public function metEventosPorProcesar($sql)
    {
        $con =  $this->_db->query($sql);

        $con->setFetchMode(PDO::FETCH_ASSOC);

        return $con->fetchAll();
    }


    #procesar los eventos
    public function metProcesarEventos()
    {

        $this->_db->beginTransaction();

        $procesar = $this->_db->prepare(
            "UPDATE
                    rh_c105_controlasistencia
                 SET
                    ind_estado=:ind_estado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE ind_estado='S'
            "
        );

        $procesar->execute(array(
            'ind_estado' => 'P'
        ));

        $error = $procesar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();

            $vaciar = $this->_db->prepare(
                "TRUNCATE TABLE rh_c104_transferencia_eventos"
            );
            $vaciar->execute();

            return 1;
        }

    }



}//fin clase
