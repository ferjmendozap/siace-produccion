<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-02-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/


class pensionesInvalidezModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE OBTENER LISTADO DE LAS PENSIONES POR INVALIDEZ
    public function metListarPensionesInvalidez($estatus)
    {

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_pension_invalidez
                WHERE txt_estatus='$estatus' and num_flag_estatus=1 and ind_detalle_tipopension='INV'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    // Método que permite  listar a los empleados de acuerdo al buscador de trabajador al momento de registrar una nueva pension
    public function metBuscarTrabajador($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTabajo, $fechaInicio, $fecha_fin)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and e.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        $buscarTrabajador = $this->_db->query(
            "select c.pk_num_empleado, d.ind_cedula_documento, d.ind_nombre1, d.ind_nombre2, d.ind_apellido1, d.ind_apellido2  from a001_organismo as a, a004_dependencia as b, rh_b001_empleado as c, a003_persona as d, rh_c076_empleado_organizacion as e where c.fk_a003_num_persona=d.pk_num_persona and c.pk_num_empleado=e.fk_rhb001_num_empleado and e.fk_a004_num_dependencia=b.pk_num_dependencia and b.fk_a001_num_organismo=a.pk_num_organismo"
        );
        $buscarTrabajador->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTrabajador->fetchAll();
    }

    // Método que permite listar los centros de costos
    public function metListarCentroCosto()
    {
        $centroCosto =  $this->_db->query(
            "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }


    public function metMostrarPernsionesInvalidez($idPensionInvalidez)
    {
        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_pension_invalidez
                WHERE pk_num_proceso_pension = '$idPensionInvalidez' and num_flag_estatus=1

             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    public function metListarPensionesInvalidezFiltro($organismo,$dependencia,$estatus)
    {


        $filtro = "txt_estatus='$estatus' AND
                   num_flag_estatus=1 AND
                   fk_a001_num_organismo='$organismo' AND
                   fk_a004_num_dependencia='$dependencia'
        ";


        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_pension_invalidez
                WHERE $filtro

             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,$estatus)
    {
        $con = $this->_db->query("
            SELECT
            rh_c086_operaciones_pension.pk_num_operaciones_pension,
            rh_b003_pension.pk_num_proceso_pension,
            rh_c086_operaciones_pension.ind_estado as txt_estatus,
            rh_c086_operaciones_pension.num_flag_estatus,
            rh_c086_operaciones_pension.fec_operacion,
            rh_c086_operaciones_pension.txt_observaciones,
            rh_c086_operaciones_pension.fk_a018_num_seguridad_usuario,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) as nombre_registro
            FROM
            rh_b003_pension
            INNER JOIN rh_c034_invalidez ON rh_b003_pension.pk_num_proceso_pension = rh_c034_invalidez.fk_rhb003_num_proceso_pension
            INNER JOIN rh_b001_empleado ON rh_b003_pension.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN rh_c086_operaciones_pension ON rh_b003_pension.pk_num_proceso_pension = rh_c086_operaciones_pension.fk_rhb003_num_pension
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = rh_c086_operaciones_pension.fk_a018_num_seguridad_usuario
            WHERE rh_b003_pension.pk_num_proceso_pension='$idPensionInvalidez' AND rh_c086_operaciones_pension.ind_estado='$estatus'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #permite obtener los datos del empleado
    public function metObtenerDatosEmpleado($idEmpleado)
    {

        $con =  $this->_db->query("

                SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                rh_c076_empleado_organizacion.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa AS organismo,
                a003_persona.fk_a006_num_miscelaneo_detalle_sexo,
                sexo.ind_nombre_detalle AS sexo,
                a003_persona.fec_nacimiento,
                rh_c076_empleado_organizacion.fk_a004_num_dependencia,
                a004_dependencia.ind_dependencia AS dependencia,
                rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo,
                rh_c063_puestos.ind_descripcion_cargo AS cargo,
                rh_c063_puestos.num_sueldo_basico,
                rh_c005_empleado_laboral.fec_ingreso,
                rh_b001_empleado.num_estatus AS estatus_empleado,
                a003_persona.num_estatus AS estatus_persona,
                rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN a006_miscelaneo_detalle AS sexo ON a003_persona.fk_a006_num_miscelaneo_detalle_sexo = sexo.pk_num_miscelaneo_detalle
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                WHERE rh_b001_empleado.pk_num_empleado='$idEmpleado'

        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }


    #PERMITE REGISTRAR LA SOLICITUD DE PENSION INVALIDEZ
    public function metRegistrarPensionInvalidez($datos)
    {
        $this->_db->beginTransaction();

        /*datos para tabla rh_b003_pension*/
        $empleado = $datos[0];#id_empleado
        $id_organismo  = $datos[1];#id_organismo
        $organismo  = $datos[2];#organismo
        $id_dependencia = $datos[3];#id_dependencia
        $dependencia = $datos[4];#dependencia
        $id_cargo = $datos[5];#id_cargo
        $cargo    = $datos[6];#cargo
        $ultimo_sueldo  = $datos[7];#sueldo actual
        $anio_serv  = $datos[8];#años de servicio
        $edad    = $datos[9];#fecha
        $fecha  = explode("-",$datos[10]);#fecha
        $fecha_ing = explode("-",$datos[11]);#fecha_ingreso
        $id_tipo_pension = $datos[12];#id_tipo_pension
        $tipo_pension = $datos[13];#tipo_pension
        $id_motivo_pension = $datos[14];#id_motivo_pension
        $motivo_pension = $datos[15];#motivo_pension
        /*datos para tabla rh_b003_pension*/

        /*datos para tabla rh_c034_invalidez*/
        $num_monto_pension  = $datos[16];#monto_pension
        $periodo = $datos[17];#periodo
        $tipo_nom = $datos[18];
        $tipo_trab= $datos[19];
        $sit_trab  = $datos[20];#situacion de trabajo
        $mot_cese  = $datos[21];#motivo cese

        $fecha_egr = explode("-",$datos[22]);#fecha de egreso

        if(empty($datos[22])){
            $fecha_egr='0000-00-00';
        }else{
            $fecha_egr = explode("-",$datos[22]);#fecha de egreso
        }
        $obs_egre  = strtoupper($datos[23]);#observacion egreso
        $resolucion  = strtoupper($datos[24]);#resolucion
        $observacion_preparado = strtoupper($datos[25]);#observacion preparado
        /*datos para tabla rh_c034_invalidez*/


        #inserto nuevo registro en la bitacora
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_b003_pension
             SET
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fk_a001_num_organismo=:fk_a001_num_organismo,
                ind_organismo=:ind_organismo,
                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                ind_dependencia=:ind_dependencia,
                fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                ind_cargo=:ind_cargo,
                num_ultimo_sueldo=:num_ultimo_sueldo,
                num_anio_servicio=:num_anio_servicio,
                num_edad=:num_edad,
                fec_fecha=:fec_fecha,
                fec_fecha_ingreso=:fec_fecha_ingreso,
                fk_a006_num_miscelaneo_detalle_tipopension=:fk_a006_num_miscelaneo_detalle_tipopension,
                ind_detalle_tipopension=:ind_detalle_tipopension,
                fk_a006_num_miscelaneo_detalle_motivopension=:fk_a006_num_miscelaneo_detalle_motivopension,
                ind_detalle_motivopension=:ind_detalle_motivopension,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            "
        );

        $registro->execute(array(
            'fk_rhb001_num_empleado' => $empleado,
            'fk_a001_num_organismo' => $id_organismo,
            'ind_organismo' => $organismo,
            'fk_a004_num_dependencia' => $id_dependencia,
            'ind_dependencia' => $dependencia,
            'fk_rhc063_num_puestos' => $id_cargo,
            'ind_cargo' => $cargo,
            'num_ultimo_sueldo' => $ultimo_sueldo,
            'num_anio_servicio' => $anio_serv,
            'num_edad' => $edad,
            'fec_fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
            'fec_fecha_ingreso' => $fecha_ing[2]."-".$fecha_ing[1]."-".$fecha_ing[0],
            'fk_a006_num_miscelaneo_detalle_tipopension' => $id_tipo_pension,
            'ind_detalle_tipopension' => $tipo_pension,
            'fk_a006_num_miscelaneo_detalle_motivopension' => $id_motivo_pension,
            'ind_detalle_motivopension' => $motivo_pension
        ));

        $id_pension = $this->_db->lastInsertId();

        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            /*registro en la tbala rh_c034_invalidez*/
            $registro2 = $this->_db->prepare(
                "INSERT INTO
                rh_c034_invalidez
             SET
                fk_rhb003_num_proceso_pension='$id_pension',
                num_monto_pension=:num_monto_pension,
                ind_periodo=:ind_periodo,
                fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                fk_a006_num_miscelaneo_detalle_tipotrab=:fk_a006_num_miscelaneo_detalle_tipotrab,
                num_situacion_trabajo=:num_situacion_trabajo,
                fk_rhc032_num_motivo_cese=:fk_rhc032_num_motivo_cese,
                fec_fecha_egreso=:fec_fecha_egreso,
                txt_observacion_egreso=:txt_observacion_egreso,
                ind_resolucion=:ind_resolucion,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            "
            );
            $registro2->execute(array(
                'num_monto_pension' => $num_monto_pension,
                'ind_periodo' => $periodo,
                'fk_nmb001_num_tipo_nomina' => $tipo_nom,
                'fk_a006_num_miscelaneo_detalle_tipotrab' => $tipo_trab,
                'num_situacion_trabajo' => $sit_trab,
                'fk_rhc032_num_motivo_cese' => $mot_cese,
                'fec_fecha_egreso' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                'txt_observacion_egreso' => $obs_egre,
                'ind_resolucion' => $resolucion
            ));
            $error2 = $registro2->errorInfo();

            if(!empty($error2[1]) && !empty($error2[2])){
                $this->_db->rollBack();
                return $error2;
            }else{

                $ESTATUS = 'PR';/*preparado*/
                $FLAG = 1;

                /*ejecuto procedimiento alamcenado para guardar las operaciones*/
                $con =  $this->_db->prepare("
                    CALL w_rh_operacionesProcesoPension(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
                $con->execute(array(
                    ':estatus' => $ESTATUS,
                    ':id' => $id_pension,
                    ':empleado' => $empleado,
                    ':flag' => $FLAG,
                    ':fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                    ':observaciones' => $observacion_preparado,
                    ':usuario' => $this->atIdUsuario
                ));

                $this->_db->commit();
                return $id_pension;

            }

        }

    }


    #PERMITE MODIFICAR PENSIONES INVALIDEZ
    public function metModificarPensionesInvalidez($idPensionInvalidez,$datos)
    {
        $this->_db->beginTransaction();

        /*organizo los datos enviados*/
        /*solo se ṕuede modifcar si esta en preparación*/
        $num_monto_pension  = $datos[16];#monto_pension
        $observacion_preparado = strtoupper($datos[25]);#observacion preparado

        $actualizar = $this->_db->prepare(
            "UPDATE
                    rh_c034_invalidez
                 SET
                    num_monto_pension=:num_monto_pension,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                  WHERE fk_rhb003_num_proceso_pension = '$idPensionInvalidez'
                "
        );
        $actualizar->execute(array(
            ':num_monto_pension' => $num_monto_pension
        ));
        $error = $actualizar->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $con = $this->_db->prepare(
                "UPDATE
                        rh_c086_operaciones_pension
                     SET
                        txt_observaciones='$observacion_preparado'
                     WHERE fk_rhb003_num_pension='$idPensionInvalidez' and ind_estado='PR'
                     "
            );
            $con->execute();
            $error1 = $con->errorInfo();
            if(!empty($error1[1]) && !empty($error1[2])){
                $this->_db->rollBack();
                return $error1;
            }else{
                $this->_db->commit();
                return $idPensionInvalidez;
            }
        }
    }//fin modificar


    public function metAnularPensionesInvalidez($idPensionInvalidez,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c086_operaciones_pension
               where fk_rhb003_num_pension='$idPensionInvalidez'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");

        if(strcmp($resultado['ind_estado'],'PR')==0){
            $nuevoEstatus = 'AN';
        }
        elseif(strcmp($resultado['ind_estado'],'CO')==0){
            $nuevoEstatus = 'PR';
        }
        elseif(strcmp($resultado['ind_estado'],'AP')==0){
            $nuevoEstatus = 'CO';
        }


        /*ejecuto procedimiento alamcenado para guardar las operaciones*/
        $con =  $this->_db->prepare("
                    CALL w_rh_operacionesProcesoPension(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
        $con->execute(array(
            ':estatus' => $nuevoEstatus,
            ':id' => $idPensionInvalidez,
            ':empleado' => $resultado['fk_rhb0001_num_empleado'],
            ':flag' => 1,
            ':fecha' => $fecha,
            ':observaciones' => $observaciones,
            ':usuario' => $this->atIdUsuario
        ));
        $this->_db->commit();
        return $idPensionInvalidez;

    }


    public function metConformarPensionesInvalidez($idPensionInvalidez,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c086_operaciones_pension
               where fk_rhb003_num_pension='$idPensionInvalidez'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");

        /*ejecuto procedimiento alamcenado para guardar las operaciones*/
        $con =  $this->_db->prepare("
                    CALL w_rh_operacionesProcesoPension(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
        $con->execute(array(
            ':estatus' => 'CO',
            ':id' => $idPensionInvalidez,
            ':empleado' => $resultado['fk_rhb0001_num_empleado'],
            ':flag' => 1,
            ':fecha' => $fecha,
            ':observaciones' => strtoupper($observaciones),
            ':usuario' => $this->atIdUsuario
        ));
        $this->_db->commit();
        return $idPensionInvalidez;


    }


    public function metAprobarPensionesInvalidez($idPensionInvalidez,$observaciones,$datos)
    {

        $this->_db->beginTransaction();

        /*organizo los datos enviados*/
        $empleado  = $datos[0];#id_empleado,
        $tipo_nom  = $datos[1];#tipo_nomina
        $tipo_trab = $datos[2];#tipo_trabajador
        $sit_trab  = $datos[3];#situacion de trabajo
        $mot_cese  = $datos[4];#motivo cese
        $fecha_egr = explode("-",$datos[5]);#fecha de egreso
        $obs_egre  = strtoupper($datos[6]);#observacion egreso
        $resolucion  = strtoupper($datos[7]);#resolucion
        $ultimo_sueldo  = $datos[8];#ultimo_sueldo
        $monto_pension  = $datos[9];#monto_pension
        $organismo = $datos[10];
        $dependencia = $datos[11];
        $cargo = $datos[12];



        /*****************************ACTUALIZO ESTATUS APAROBADO***************************************************/
        $fecha = date("Y-m-d");

        $con = $this->_db->query("
               select * from rh_c086_operaciones_pension
               where 	fk_rhb003_num_pension='$idPensionInvalidez'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $empleado = $resultado['fk_rhb0001_num_empleado'];



            /*****************************ACTUALIZO ESTATUS APAROBADO***************************************************/


        $c0 = $this->_db->prepare("
            UPDATE rh_c034_invalidez
            SET
            fk_nmb001_num_tipo_nomina='$tipo_nom',
            fk_a006_num_miscelaneo_detalle_tipotrab='$tipo_trab',
            num_situacion_trabajo='$sit_trab',
            fk_rhc032_num_motivo_cese='$mot_cese',
            fec_fecha_egreso=:fec_fecha_egreso,
            txt_observacion_egreso='$obs_egre',
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE fk_rhb003_num_proceso_pension='$idPensionInvalidez'
        ");
        $c0->execute(array(
            'fec_fecha_egreso' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0]
        ));
        $c0Error = $c0->errorInfo();
        if(!empty($c0Error[1]) && !empty($c0Error[2])){
            $this->_db->rollBack();
            return $c0Error."invalidez";
        }else{

            /*actualizo el empleado*/
            $c1 = $this->_db->prepare("
            UPDATE rh_b001_empleado
            SET
            num_estatus=0,
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE pk_num_empleado='$empleado'
        ");
            $c1->execute();
            $c1Error = $c1->errorInfo();
            if(!empty($c1Error[1]) && !empty($c1Error[2])){
                $this->_db->rollBack();
                return $c1Error."empleado";
            }else{

                /*----------------------------------------------------------------------------*/
                $c2 = $this->_db->prepare("
            UPDATE rh_c076_empleado_organizacion
            SET
              fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
              fk_a006_num_miscelaneo_detalle_tipotrabajador=:fk_a006_num_miscelaneo_detalle_tipotrabajador,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE fk_rhb001_num_empleado='$empleado'
            ");
                $c2->execute(array(
                    ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                    ':fk_a006_num_miscelaneo_detalle_tipotrabajador' => $tipo_trab
                ));
                $c2Error = $c2->errorInfo();
                if(!empty($c2Error[1]) && !empty($c2Error[2])){
                    $this->_db->rollBack();
                    return $c2Error."organizacion";
                }else{
                    /*----------------------------------------------------------------------------*/
                    $c3 = $this->_db->prepare("
                UPDATE rh_c005_empleado_laboral
                SET
                  fec_egreso=:fec_egreso,
                  ind_resolucion_egreso=:ind_resolucion_egreso,
                  num_sueldo_ant=:num_sueldo_ant,
                  num_sueldo=:num_sueldo,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE fk_rhb001_num_empleado='$empleado'
                ");
                    $c3->execute(array(
                        ':fec_egreso' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                        ':ind_resolucion_egreso' => $resolucion,
                        ':num_sueldo_ant' => $ultimo_sueldo,
                        ':num_sueldo' => $monto_pension
                    ));
                    $c3Error = $c3->errorInfo();
                    if(!empty($c3Error[1]) && !empty($c3Error[2])){
                        $this->_db->rollBack();
                        return $c3Error."laboral";
                    }else{

                        /*actualizo e inserto nivelaciones*/
                        $con = $this->_db->query("SELECT ADDDATE('".$fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0]."', ".intval(-1).") AS FechaResultado");
                        $con->setFetchMode(PDO::FETCH_ASSOC);
                        $dato = $con->fetch();
                        $FechaHasta= $dato['FechaResultado'];

                        /*actualizo la ultima nivelacion*/
                        $c4 = $this->_db->prepare("
                    UPDATE rh_c059_empleado_nivelacion
                    SET
                      fec_fecha_hasta=:fec_fecha_hasta,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    WHERE fk_rhb001_num_empleado='$empleado' AND fec_fecha_hasta='0000-00-00'
                    ");
                        $c4->execute(array(
                            ':fec_fecha_hasta' => $FechaHasta
                        ));
                        $c4Error = $c4->errorInfo();
                        if(!empty($c4Error[1]) && !empty($c4Error[2])){
                            $this->_db->rollBack();
                            return $c4Error."ultima-nivelacion";
                        }else{

                            /*insero nueva nivelacion*/
                            $cc = $this->_db->query("select fk_a023_num_centro_costo from rh_c076_empleado_organizacion where fk_rhb001_num_empleado='$empleado'");
                            $cc->setFetchMode(PDO::FETCH_ASSOC);
                            $centroCosto = $cc->fetch();

                            $c5 = $this->_db->prepare(
                                "INSERT INTO
                                rh_c059_empleado_nivelacion
                             SET
                                fk_rhb001_num_empleado='$empleado',
                                fec_fecha_registro=:fec_fecha_registro,
                                fk_a001_num_organismo=:fk_a001_num_organismo,
                                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                                fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                                fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                                ind_documento=:ind_documento,
                                num_estatus=:num_estatus,
                                fec_fecha_hasta=:fec_fecha_hasta,
                                fec_ultima_modificacion=NOW(),
                                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                              ");
                            #execute — Ejecuta una sentencia preparada
                            $c5->execute(array(
                                ':fec_fecha_registro' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                                ':fk_a001_num_organismo'  => $organismo,
                                ':fk_a004_num_dependencia'  => $dependencia,
                                ':fk_a023_num_centro_costo'  => $centroCosto['fk_a023_num_centro_costo'],
                                ':fk_rhc063_num_puestos'  => $cargo,
                                ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                                ':ind_documento' => $resolucion,
                                ':num_estatus' => $sit_trab,
                                ':fec_fecha_hasta' => '0000-00-00'
                            ));
                            $c5Error = $c5->errorInfo();
                            $id_nivelacion = $this->_db->lastInsertId();
                            if(!empty($c5Error[1]) && !empty($c5Error[2])){
                                $this->_db->rollBack();
                                return $c5Error."nivelacion";
                            }else{

                                /*inserto nivelacion historial*/
                                $c6 = $this->_db->prepare(
                                    "INSERT INTO
                                    rh_c060_empleado_nivelacion_historial
                                    (fk_rhc059_num_empleado_nivelacion,
                                     fec_fecha_registro,
                                     ind_organismo,
                                     ind_dependencia,
                                     ind_cargo,
                                     num_nivel_salarial,
                                     ind_categoria_cargo,
                                     ind_tipo_nomina,
                                     ind_centro_costo,
                                     ind_tipo_accion,
                                     num_estatus,
                                     fec_ultima_modificacion,
                                     fk_a018_num_seguridad_usuario)
                                SELECT
                                    rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion,
                                    rh_c059_empleado_nivelacion.fec_fecha_registro,
                                    organismo.ind_descripcion_empresa AS ind_organismo,
                                    dependencia.ind_dependencia AS ind_dependencia,
                                    cargo.ind_descripcion_cargo AS ind_cargo,
                                    cargo.num_sueldo_basico AS num_nivel_salarial,
                                    categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                    nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                    centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                    tipo_accion.ind_nombre_detalle AS ind_tipo_accion,
                                    rh_c059_empleado_nivelacion.num_estatus,
                                    NOW() as fec_ultima_modificacion,
                                    '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                FROM
                                    rh_c059_empleado_nivelacion
                                INNER JOIN a001_organismo AS organismo ON organismo.pk_num_organismo = rh_c059_empleado_nivelacion.fk_a001_num_organismo
                                INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = rh_c059_empleado_nivelacion.fk_a004_num_dependencia
                                INNER JOIN a023_centro_costo AS centro_costo ON centro_costo.pk_num_centro_costo = rh_c059_empleado_nivelacion.fk_a023_num_centro_costo
                                INNER JOIN rh_c063_puestos AS cargo ON cargo.pk_num_puestos = rh_c059_empleado_nivelacion.fk_rhc063_num_puestos
                                INNER JOIN nm_b001_tipo_nomina AS nomina ON nomina.pk_num_tipo_nomina = rh_c059_empleado_nivelacion.fk_nmb001_num_tipo_nomina
                                LEFT JOIN a006_miscelaneo_detalle AS tipo_accion ON tipo_accion.pk_num_miscelaneo_detalle = rh_c059_empleado_nivelacion.fk_a006_num_miscelaneo_detalle_tipoaccion
                                INNER JOIN a006_miscelaneo_detalle AS categoria ON cargo.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                WHERE rh_c059_empleado_nivelacion.fk_rhb001_num_empleado='$empleado'
                                AND rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion='$id_nivelacion'
                                  ");

                                $c6->execute();
                                $c6Error = $c6->errorInfo();
                                if(!empty($c6Error[1]) && !empty($c6Error[2])){
                                    $this->_db->rollBack();
                                    return $c6Error."nivelacion-historial";
                                }else{

                                    /*inserto historial empleado*/
                                    $c7 = $this->_db->prepare(
                                        "INSERT INTO
                                        rh_c061_empleado_historial
                                        (fk_rhb001_num_empleado,
                                         ind_periodo,
                                         fec_ingreso,
                                         ind_organismo,
                                         ind_dependencia,
                                         ind_centro_costo,
                                         ind_cargo,
                                         num_nivel_salarial,
                                         ind_categoria_cargo,
                                         ind_tipo_nomina,
                                         ind_tipo_pago,
                                         num_estatus,
                                         ind_motivo_cese,
                                         fec_egreso,
                                         txt_obs_cese,
                                         ind_tipo_trabajador,
                                         fec_ultima_modificacion,
                                         fk_a018_num_seguridad_usuario)
                                    SELECT
                                        empleado.pk_num_empleado,
                                        NOW() as ind_periodo,
                                        rh_c005_empleado_laboral.fec_ingreso,
                                        a001_organismo.ind_descripcion_empresa AS ind_organismo,
                                        a004_dependencia.ind_dependencia,
                                        a023_centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                        rh_c063_puestos.ind_descripcion_cargo AS ind_cargo,
                                        rh_c063_puestos.num_sueldo_basico AS ind_nivel_salarial,
                                        categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                        nm_b001_tipo_nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                        tipo_pago.ind_nombre_detalle AS ind_tipo_pago,
                                        empleado.num_estatus,
                                        motivo_cese.ind_nombre_cese AS ind_motivo_cese,
                                        pension_invalidez.fec_fecha_egreso AS fec_egreso,
                                        pension_invalidez.txt_observacion_egreso AS txt_obs_cese,
                                        tipo_trabajador.ind_nombre_detalle AS ind_tipo_trabajador,
                                        NOW() as fec_ultima_modificacion,
                                        '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                    FROM
                                        rh_b001_empleado AS empleado
                                        INNER JOIN rh_c076_empleado_organizacion ON empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                                        INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                        INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                                        INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                                        INNER JOIN a023_centro_costo ON rh_c076_empleado_organizacion.fk_a023_num_centro_costo = a023_centro_costo.pk_num_centro_costo
                                        INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                                        INNER JOIN a006_miscelaneo_detalle AS categoria ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                        INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                                        INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                                        INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                                        INNER JOIN rh_b003_pension ON rh_b003_pension.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                        INNER JOIN rh_c034_invalidez AS pension_invalidez ON rh_b003_pension.pk_num_proceso_pension = pension_invalidez.fk_rhb003_num_proceso_pension
                                        INNER JOIN rh_c032_motivo_cese AS motivo_cese ON motivo_cese.pk_num_motivo_cese = pension_invalidez.fk_rhc032_num_motivo_cese
                                    WHERE
                                        empleado.pk_num_empleado='$empleado'

                                ");
                                    $c7->execute();
                                    $c7Error = $c7->errorInfo();
                                    if(!empty($c7Error[1]) && !empty($c7Error[2])){
                                        $this->_db->rollBack();
                                        return $c7Error."-historial";
                                    }else{

                                        /*ejecuto procedimiento alamcenado para guardar las operaciones*/
                                        $con =  $this->_db->prepare("
                                                    CALL w_rh_operacionesProcesoPension(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                                        ");
                                        $con->execute(array(
                                            ':estatus' => 'AP',
                                            ':id' => $idPensionInvalidez,
                                            ':empleado' => $empleado,
                                            ':flag' => 1,
                                            ':fecha' => $fecha,
                                            ':observaciones' => strtoupper($observaciones),
                                            ':usuario' => $this->atIdUsuario
                                        ));
                                        $errorCall = $con->errorInfo();
                                        if(!empty($errorCall[1]) && !empty($errorCall[2])){
                                            $this->_db->rollBack();
                                            return $errorCall;
                                        }else {
                                            $this->_db->commit();
                                            return $idPensionInvalidez;
                                        }

                                    }

                                }

                            }

                        }

                    }

                }

            }

        }

    }/*FIN METODO*/



}//fin clase
