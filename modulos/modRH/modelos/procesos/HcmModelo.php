<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de permisos por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modRH' . DS . 'modelos' . DS . 'gestion' . DS . 'cargaFamiliarModelo.php';
require_once RUTA_Modulo . 'modRH' . DS . 'modelos' . DS . 'gestion' . DS . 'empleadosModelo.php';
require_once RUTA_Modulo . 'modRH' . DS . 'modelos' . DS . 'maestros' . DS . 'beneficiosGlobalHcmModelo.php';
require_once RUTA_Modulo . 'modRH' . DS . 'modelos' . DS . 'maestros' . DS . 'beneficiosHcmModelo.php';

class HcmModelo extends miscelaneoModelo
{

    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atcargaFamiliarModelo = new cargaFamiliarModelo();
        $this->atbeneficiosHcmModelo = new beneficiosHcmModelo();
        $this->atbeneficiosGlobalHcmModelo = new beneficiosGlobalHcmModelo();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atOrganismoModelo  = new empleadosModelo();
        $this->atIdUsuario = Session::metObtener("idUsuario");

    }

    // Método que muestra el listado de permisos solicitados por el funcionario
    public function metListarSolicitudBeneficioMedico()
    {
        $listarSolicitud =  $this->_db->query(
        "select
         rh_c046_beneficio_medico.num_monto,
         rh_c046_beneficio_medico.pk_beneficio_medico,
         rh_c046_beneficio_medico.ind_estado as num_estado,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.fk_a003_persona,
         rh_c046_beneficio_medico.fk_a006_tipo_solicitud,
         a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
         rh_b001_empleado.fk_a003_num_persona,
         a006_miscelaneo_detalle.ind_nombre_detalle,
         estado_miscelaneo.pk_num_miscelaneo_maestro,
         estado_detalle.ind_nombre_detalle as estado_nombre,
         a003_persona.ind_nombre1,
         rh_c068_ayuda_especifica.ind_descripcion_especifica,
         a003_persona.ind_apellido1

        from rh_c046_beneficio_medico
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='SOLHCM')
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_tipo_solicitud=a006_miscelaneo_detalle.cod_detalle)

        left join a005_miscelaneo_maestro as estado_miscelaneo on (estado_miscelaneo.cod_maestro='ESTADOHCM')
        left join a006_miscelaneo_detalle as estado_detalle  on (estado_detalle.fk_a005_num_miscelaneo_maestro=estado_miscelaneo.pk_num_miscelaneo_maestro AND   estado_detalle.cod_detalle =rh_c046_beneficio_medico.ind_estado      )

        left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=rh_c046_beneficio_medico.fk_rhb001_empleado)
        left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
        left join rh_c068_ayuda_especifica on (rh_c068_ayuda_especifica.pk_num_ayuda_especifica=rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica)
        GROUP BY rh_c046_beneficio_medico.pk_beneficio_medico
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }


    public function metListarSolicitudBeneficioEstatus($estatus)
    {
        $listarSolicitud =  $this->_db->query(
            "select
         rh_c046_beneficio_medico.num_monto,

         rh_c046_beneficio_medico.pk_beneficio_medico,
          rh_c046_beneficio_medico.ind_estado as num_estado,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.fk_a003_persona,
         rh_c046_beneficio_medico.fk_a006_tipo_solicitud,
         a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
         rh_b001_empleado.fk_a003_num_persona,
         a006_miscelaneo_detalle.ind_nombre_detalle,
         a003_persona.ind_nombre1,
          estado_miscelaneo.pk_num_miscelaneo_maestro,
         estado_detalle.ind_nombre_detalle as estado_nombre,
         rh_c068_ayuda_especifica.ind_descripcion_especifica,
         a003_persona.ind_apellido1

        from rh_c046_beneficio_medico
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='SOLHCM')
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_tipo_solicitud=a006_miscelaneo_detalle.cod_detalle)


          left join a005_miscelaneo_maestro as estado_miscelaneo on (estado_miscelaneo.cod_maestro='ESTADOHCM')
        left join a006_miscelaneo_detalle as estado_detalle  on (estado_detalle.fk_a005_num_miscelaneo_maestro=estado_miscelaneo.pk_num_miscelaneo_maestro AND   estado_detalle.cod_detalle =rh_c046_beneficio_medico.ind_estado      )



        left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=rh_c046_beneficio_medico.fk_rhb001_empleado)
        left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
        left join rh_c068_ayuda_especifica on (rh_c068_ayuda_especifica.pk_num_ayuda_especifica=rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica)
        WHERE ind_estado='".$estatus."'
        GROUP BY rh_c046_beneficio_medico.pk_beneficio_medico
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }


    // Método que muestra los datos a mostrar en pdf
    public function metMostrarPdf($empleado,$fecha)
    {


        $fecha= explode(" ",$fecha);

        $listarSolicitud =  $this->_db->query(
            " select  
         rh_c058_factura_hcm.num_monto,
         date_format(rh_c046_beneficio_medico.fec_creacion,'%d-%m-%Y') as fec_creacion,
 
         rh_c046_beneficio_medico.fk_a001_organismo,
         rh_c046_beneficio_medico.fk_rhb008_ayuda_global,
         rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.pk_beneficio_medico,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.fk_a003_persona,
         rh_c046_beneficio_medico.fk_a006_tipo_solicitud,
         rh_c046_beneficio_medico.fk_a006_rama,
         rh_c046_beneficio_medico.fk_rhc040_institucion,
         rh_c046_beneficio_medico.fk_rhc047_medico_hcm,
         rh_c046_beneficio_medico.flag_planilla_solicitud ,
         rh_c046_beneficio_medico.flag_informe_medico ,
         rh_c046_beneficio_medico.flag_factura ,
         rh_c046_beneficio_medico.flag_recipe_medico ,
         rh_c015_carga_familiar.fk_a003_num_persona,
         rh_c046_beneficio_medico.flag_otro ,
         rh_c042_institucion_hcm.ind_nombre_institucion,
         rh_b001_empleado.fk_a003_num_persona,
         a003_persona.ind_nombre1,
         cargaFamiliar.ind_nombre1  as beneficiario_nombre,
         cargaFamiliar.ind_apellido1 as beneficiario_apellido,
         a006_miscelaneo_detalle.ind_nombre_detalle,
         misc_detalles.ind_nombre_detalle as rama,
         rh_c058_factura_hcm.pk_num_factura_hcm,
         rh_c058_factura_hcm.num_factura,
         rh_c068_ayuda_especifica.ind_descripcion_especifica as servicio,
         a003_persona.ind_apellido1,
         estado_detalle.ind_nombre_detalle as estado 

         from rh_c046_beneficio_medico

         left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='SOLHCM')
         left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_tipo_solicitud=a006_miscelaneo_detalle.cod_detalle)
         
         left join a005_miscelaneo_maestro as estado_maestro on (estado_maestro.cod_maestro='ESTADOHCM')
         left join a006_miscelaneo_detalle as estado_detalle on (estado_detalle.fk_a005_num_miscelaneo_maestro=estado_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_tipo_solicitud=estado_detalle.cod_detalle)
         
         inner join a005_miscelaneo_maestro as misc_maestro on  (misc_maestro.cod_maestro='RAMAS' )
         inner join a006_miscelaneo_detalle as misc_detalles on  (misc_detalles.fk_a005_num_miscelaneo_maestro=misc_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_rama=misc_detalles.cod_detalle  )

         left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=rh_c046_beneficio_medico.fk_rhb001_empleado)
         left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
         left join rh_c015_carga_familiar on (rh_c015_carga_familiar.fk_rhb001_num_empleado=rh_c046_beneficio_medico.fk_rhb001_empleado)
         left join a003_persona as cargaFamiliar on (cargaFamiliar.pk_num_persona=rh_c015_carga_familiar.fk_a003_num_persona)
         left join rh_c068_ayuda_especifica on (rh_c068_ayuda_especifica.pk_num_ayuda_especifica=rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica)
         left join rh_c042_institucion_hcm on (rh_c042_institucion_hcm.pk_num_institucion=rh_c046_beneficio_medico.fk_rhc040_institucion) 
         left join rh_c058_factura_hcm on (rh_c058_factura_hcm.fk_rhc046_beneficio_medico=rh_c046_beneficio_medico.pk_beneficio_medico)
         WHERE rh_c046_beneficio_medico.fk_rhb001_empleado='$empleado' AND  rh_c046_beneficio_medico.fec_creacion like '%$fecha[0]%'

         group by pk_num_factura_hcm
     
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }


    // Método que muestra los datos a mostrar en pdf
    public function metMostrarID($id)
    {


        $listarSolicitud =  $this->_db->query(
            " select  
         rh_c046_beneficio_medico.num_monto,
         date_format(rh_c046_beneficio_medico.fec_creacion,'%d-%m-%Y') as fec_creacion,
 
         rh_c046_beneficio_medico.fk_a001_organismo,
         rh_c046_beneficio_medico.fk_rhb008_ayuda_global,
         rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.pk_beneficio_medico,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.fk_a003_persona,
         rh_c046_beneficio_medico.fk_a006_tipo_solicitud,
         rh_c046_beneficio_medico.fk_a006_rama,
         rh_c046_beneficio_medico.fk_rhc040_institucion,
         rh_c046_beneficio_medico.fk_rhc047_medico_hcm,
         rh_c046_beneficio_medico.flag_planilla_solicitud ,
         rh_c046_beneficio_medico.flag_informe_medico ,
         rh_c046_beneficio_medico.flag_factura ,
         rh_c046_beneficio_medico.flag_recipe_medico ,
         rh_c015_carga_familiar.fk_a003_num_persona,
         rh_c046_beneficio_medico.flag_otro ,
         rh_c042_institucion_hcm.ind_nombre_institucion,
         rh_b001_empleado.fk_a003_num_persona,
         a003_persona.ind_nombre1,
         cargaFamiliar.ind_nombre1  as beneficiario_nombre,
         cargaFamiliar.ind_apellido1 as beneficiario_apellido,
         a006_miscelaneo_detalle.ind_nombre_detalle,
         misc_detalles.ind_nombre_detalle as rama,
         rh_c058_factura_hcm.pk_num_factura_hcm,
         rh_c058_factura_hcm.num_factura,
         rh_c068_ayuda_especifica.ind_descripcion_especifica as servicio,
         a003_persona.ind_apellido1,
         estado_detalle.ind_nombre_detalle as estado 

         from rh_c046_beneficio_medico

         left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='SOLHCM')
         left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_tipo_solicitud=a006_miscelaneo_detalle.cod_detalle)
         
         left join a005_miscelaneo_maestro as estado_maestro on (estado_maestro.cod_maestro='ESTADOHCM')
         left join a006_miscelaneo_detalle as estado_detalle on (estado_detalle.fk_a005_num_miscelaneo_maestro=estado_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_tipo_solicitud=estado_detalle.cod_detalle)
         
         inner join a005_miscelaneo_maestro as misc_maestro on  (misc_maestro.cod_maestro='RAMAS' )
         inner join a006_miscelaneo_detalle as misc_detalles on  (misc_detalles.fk_a005_num_miscelaneo_maestro=misc_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_rama=misc_detalles.cod_detalle  )

         left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=rh_c046_beneficio_medico.fk_rhb001_empleado)
         left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
         left join rh_c015_carga_familiar on (rh_c015_carga_familiar.fk_rhb001_num_empleado=rh_c046_beneficio_medico.fk_rhb001_empleado)
         left join a003_persona as cargaFamiliar on (cargaFamiliar.pk_num_persona=rh_c015_carga_familiar.fk_a003_num_persona)
         left join rh_c068_ayuda_especifica on (rh_c068_ayuda_especifica.pk_num_ayuda_especifica=rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica)
         left join rh_c042_institucion_hcm on (rh_c042_institucion_hcm.pk_num_institucion=rh_c046_beneficio_medico.fk_rhc040_institucion) 
         left join rh_c058_factura_hcm on (rh_c058_factura_hcm.fk_rhc046_beneficio_medico=rh_c046_beneficio_medico.pk_beneficio_medico)
         WHERE rh_c046_beneficio_medico.pk_beneficio_medico='$id' 

         group by pk_num_factura_hcm
     
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }

    // Método que muestra el listado de permisos solicitados por el funcionario por id
    public function metListarSolicitudBeneficioMedicoId($id)
    {
        $listarSolicitud =  $this->_db->query(
        "select
         rh_c046_beneficio_medico.ind_estado,
         rh_c046_beneficio_medico.num_monto,
         rh_c046_beneficio_medico.fk_a001_organismo,
         rh_c046_beneficio_medico.fk_rhb008_ayuda_global,
         rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.pk_beneficio_medico,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.fk_a003_persona,
         rh_c046_beneficio_medico.fk_a006_tipo_solicitud,
         rh_c046_beneficio_medico.fk_a006_rama,
         rh_c046_beneficio_medico.fk_rhc040_institucion,
         rh_c046_beneficio_medico.fk_rhc047_medico_hcm,
         rh_c046_beneficio_medico.flag_planilla_solicitud ,
         rh_c046_beneficio_medico.flag_informe_medico ,
         rh_c046_beneficio_medico.flag_factura ,
         rh_c046_beneficio_medico.flag_recipe_medico ,
         rh_c046_beneficio_medico.flag_otro ,
         rh_b001_empleado.fk_a003_num_persona,
         a003_persona.ind_nombre1,
         rh_c068_ayuda_especifica.ind_descripcion_especifica,
         a003_persona.ind_apellido1

         from rh_c046_beneficio_medico


         left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=rh_c046_beneficio_medico.fk_rhb001_empleado)
         left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
         left join rh_c068_ayuda_especifica on (rh_c068_ayuda_especifica.pk_num_ayuda_especifica=rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica)

         WHERE rh_c046_beneficio_medico.pk_beneficio_medico='".$id."'
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }


    public function metBeneficioMedicoIdEstatus($id)
    {
        $listarSolicitud =  $this->_db->query(
         "select
         rh_c046_beneficio_medico.ind_estado,
         rh_c046_beneficio_medico.fec_creacion,
         rh_c046_beneficio_medico.fk_rhb001_empleado
        

         from rh_c046_beneficio_medico


         WHERE rh_c046_beneficio_medico.pk_beneficio_medico='".$id."'
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }

    // Método que muestra el listado de permisos solicitados por el funcionario por id
    public function metListarSolicitudBeneficioMedicoFacturaId($id)
    {
        $listarSolicitud =  $this->_db->query(
            "select
             rh_c058_factura_hcm.pk_num_factura_hcm,
             rh_c058_factura_hcm.num_factura,
             rh_c058_factura_hcm.ind_descripcion,
             rh_c058_factura_hcm.num_monto

             from rh_c058_factura_hcm
             WHERE rh_c058_factura_hcm.fk_rhc046_beneficio_medico='".$id."'
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }

    // Método para calcular lo consumido por una persona en un periodo determinado
    public function metConsumidoPersona($idEmpleado,$ayuda)
    {
        $listarSolicitud =  $this->_db->query(
        "SELECT FORMAT(SUM(num_monto),2) AS monto, SUM(num_monto) AS monto_sin_formato
         from rh_c046_beneficio_medico

         WHERE rh_c046_beneficio_medico.fk_rhb008_ayuda_global='".$ayuda."' AND rh_c046_beneficio_medico.fk_rhb001_empleado='".$idEmpleado."' AND rh_c046_beneficio_medico.ind_estado='3'
        "
        );
        //var_dump($listarSolicitud);
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }
    // Método para calcular lo consumido por una persona en un periodo determinado , especificando el beneficio
    public function  metConsumidoPersonaBeneficio($idEmpleado,$ayuda,$beneficio)
    {
        $listarSolicitud =  $this->_db->query(
            "SELECT FORMAT(SUM(num_monto),2) AS monto, SUM(num_monto) AS monto_sin_formato
         from rh_c046_beneficio_medico

         WHERE rh_c046_beneficio_medico.fk_rhb008_ayuda_global='".$ayuda."' AND rh_c046_beneficio_medico.fk_rhb001_empleado='".$idEmpleado."' AND rh_c046_beneficio_medico.ind_estado='3' AND rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica='".$beneficio."'
        "
        );
       $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }

    // Método para calcular lo consumido por una persona en un periodo determinado , especificando el beneficio
    public function  metConsumidoPersonaBeneficioExtension($idEmpleado,$ayuda,$beneficio)
    {
        $listarSolicitud =  $this->_db->query(
            "SELECT FORMAT(SUM(num_monto),2) AS monto, SUM(num_monto) AS monto_sin_formato
         from rh_c052_ayuda_especifica_extension

         WHERE rh_c052_ayuda_especifica_extension.fk_rhb008_num_ayuda_global='".$ayuda."' AND rh_c052_ayuda_especifica_extension.fk_rhb001_num_empleado='".$idEmpleado."'  AND rh_c052_ayuda_especifica_extension.fk_rhc068_num_ayuda_especifica='".$beneficio."'
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }

    public function metMontoAsignadoDisponible($empleado,$ayuda,$idAsignacion)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT num_monto_definitivo FROM rh_c052_ayuda_especifica_extension WHERE fk_rhb001_num_empleado='$empleado' and fk_rhb008_num_ayuda_global='$ayuda' and fk_rhc068_num_ayuda_especifica='$idAsignacion'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }
    #MÉTODO PARA CONSULTAR  SI HAY UNA EXTENSION DEL BENEFICIO AL EMPLEADO
    public function metBeneficioExtension($idEmpleado,$ayuda )
    {
        $listarSolicitud =  $this->_db->query(
        "SELECT
            rh_c044_extension_beneficio.fk_rhc068_ayuda_especifica,
            rh_c068_ayuda_especifica.ind_descripcion_especifica,
            rh_c044_extension_beneficio.num_monto_predeterminado,
            rh_c044_extension_beneficio.num_monto_disminucion,
            SUM(num_monto_predeterminado + num_monto_disminucion) AS monto_final,
            FORMAT(num_monto_disminucion,2) AS monto
        FROM
            rh_c044_extension_beneficio
        INNER JOIN rh_c068_ayuda_especifica ON rh_c044_extension_beneficio.fk_rhc068_ayuda_especifica = rh_c068_ayuda_especifica.pk_num_ayuda_especifica
        WHERE rh_c044_extension_beneficio.fk_rhb008_ayuda_global='$ayuda'  AND rh_c044_extension_beneficio.fk_rhb001_empleado='$idEmpleado'

       ");

        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }

    public function metBuscarBeneficio ($Estado,$Empleado,$Beneficio,$Ayuda) {
        $filtro="";
        if ($Estado!='') {
            $filtro .= "and rh_c046_beneficio_medico.ind_estado='".$Estado."' ";
        }
        if ($Empleado!='') {
            $filtro .= "and rh_c046_beneficio_medico.fk_rhb001_empleado='".$Empleado."' ";
        }

        if ($Beneficio!='') {
            $filtro .= "and rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica='".$Beneficio."' ";
        }
        if ($Ayuda!='') {
            $filtro .= "and rh_c046_beneficio_medico.fk_rhb008_ayuda_global='".$Ayuda."' ";
        }
        $listarSolicitud =  $this->_db->query(

        " select
         FORMAT(rh_c046_beneficio_medico.num_monto,2) as num_monto,
         rh_c046_beneficio_medico.pk_beneficio_medico,
         rh_c046_beneficio_medico.ind_estado as num_estado,
         rh_c046_beneficio_medico.fk_rhb001_empleado,
         rh_c046_beneficio_medico.fk_a003_persona,
         rh_c046_beneficio_medico.fk_a006_tipo_solicitud,
         a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
         estado_miscelaneo.pk_num_miscelaneo_maestro,
         rh_b001_empleado.fk_a003_num_persona,
         a006_miscelaneo_detalle.ind_nombre_detalle,
         estado_detalle.ind_nombre_detalle as estado_nombre,
         a003_persona.ind_nombre1,
         rh_c068_ayuda_especifica.ind_descripcion_especifica,
         a003_persona.ind_apellido1


        from rh_c046_beneficio_medico
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='SOLHCM')
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND rh_c046_beneficio_medico.fk_a006_tipo_solicitud=a006_miscelaneo_detalle.cod_detalle )

        left join a005_miscelaneo_maestro as estado_miscelaneo on (estado_miscelaneo.cod_maestro='ESTADOHCM')
        left join a006_miscelaneo_detalle as estado_detalle  on (estado_detalle.fk_a005_num_miscelaneo_maestro=estado_miscelaneo.pk_num_miscelaneo_maestro AND   estado_detalle.cod_detalle =rh_c046_beneficio_medico.ind_estado      )

        left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=rh_c046_beneficio_medico.fk_rhb001_empleado)
        left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
        left join rh_c068_ayuda_especifica on (rh_c068_ayuda_especifica.pk_num_ayuda_especifica=rh_c046_beneficio_medico.fk_rhc068_ayuda_especifica)
        WHERE 1 $filtro
        GROUP BY rh_c046_beneficio_medico.pk_beneficio_medico
       "
    );

    $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
    return $listarSolicitud->fetchAll();
    }

    public function metRegistrarFacturasHcm($idBeneficio, $factura,$descripcion, $monto){
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
 
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                 rh_c058_factura_hcm
             SET

                fk_rhc046_beneficio_medico=:fk_rhc046_beneficio_medico,
                num_monto=:num_monto,
                num_factura=:num_factura,
                ind_descripcion=:ind_descripcion

             ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(

            ':fk_rhc046_beneficio_medico' => $idBeneficio,
            ':num_monto' => $monto,
            ':num_factura'=>  $factura,
            ':ind_descripcion' => $descripcion,


        ));

            $this->_db->commit();
    }

    // Método que muestra el listado de permisos solicitados por el funcionario por id
    public function metListarBitacoraHCM($id)
    {
        $listarSolicitud =  $this->_db->query(
        "select rh_c054_hcm_bitacora.*,
                a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                a018_seguridad_usuario.fk_rhb001_num_empleado,
                rh_b001_empleado.fk_a003_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_apellido1

             from rh_c054_hcm_bitacora

             inner join a005_miscelaneo_maestro on  (a005_miscelaneo_maestro.cod_maestro='ESTADOHCM' )
             inner join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND rh_c054_hcm_bitacora.fk_a006_miscelaneo=a006_miscelaneo_detalle.cod_detalle  )

             inner join a018_seguridad_usuario on (a018_seguridad_usuario.pk_num_seguridad_usuario=rh_c054_hcm_bitacora.fk_a018_num_seguridad_usuario)
             inner join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=a018_seguridad_usuario.fk_rhb001_num_empleado)
             inner join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)

             WHERE rh_c054_hcm_bitacora.fk_rhc046_beneficio_medico='".$id."'
             
             order by pk_num_hcm_bitacora
        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }

    public function metRegistrarBitacoraHcm($idBeneficio, $estado){
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                 rh_c054_hcm_bitacora
             SET
                  fk_a006_miscelaneo=:fk_a006_miscelaneo,
                  fk_rhc046_beneficio_medico=:fk_rhc046_beneficio_medico,
                  fec_ultima_modificacion=:fec_ultima_modificacion,
                  fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario

             ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(

            ':fk_a006_miscelaneo' => $estado,
            ':fk_rhc046_beneficio_medico' => $idBeneficio,
            ':fec_ultima_modificacion'=> date('Y-m-d H:i:s'),
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario

        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idBeneficio;
        }



    }


    public function metActualizarDisponibilidad($ayuda,$beneficio)
    {
        $listarSolicitud =  $this->_db->query(
       "SELECT
        rh_b008_ayuda_global.pk_num_ayuda_global,
        FORMAT(rh_b008_ayuda_global.num_limite ,2) as disponible,
        rh_b008_ayuda_global.num_limite as disponible_sin_formato,
        FORMAT(rh_c068_ayuda_especifica.num_limite_esp ,2) as beneficio,
        rh_c068_ayuda_especifica.num_limite_esp as beneficio_sin_formato
        FROM  rh_b008_ayuda_global
        left join  rh_c068_ayuda_especifica on (   pk_num_ayuda_especifica='$beneficio')
        WHERE pk_num_ayuda_global='".$ayuda."'"
        );
        //var_dump($listarSolicitud);
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }


    #PERMITE REGISTRAR BENEFICIO HCM
    public function metRegistrarBeneficioPersonalHcm($organismo,$tipo,$empleado,$persona,$global,$especifica,$rama,$institucion,$medico ,$f_planilla,$f_informe,$f_factura,$f_recipe,$f_otro,$monto)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=0;"
        );
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare("INSERT INTO rh_c046_beneficio_medico 
        values (null,
         :fk_a001_organismo,
         :fk_a006_tipo_solicitud,
         :fk_rhb001_empleado,
         :fk_a003_persona,
         :fk_rhb008_ayuda_global, 
         :fk_rhc068_ayuda_especifica, 
         :fk_a006_rama, 
         :fk_rhc040_institucion, 
         :fk_rhc047_medico_hcm, 
         NOW(),
         :flag_planilla_solicitud, 
         :flag_informe_medico, 
         :flag_factura, 
         :flag_recipe_medico, 
         :flag_otro, 
         :ind_estado, 
         :num_monto,
         NOW(),
         :fk_a018_num_seguridad_usuario
         )");


        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a001_organismo' => $organismo,
            ':fk_a006_tipo_solicitud' => $tipo,
            ':fk_rhb001_empleado'=>  $empleado,
            ':fk_a003_persona' => $persona,
            ':fk_rhb008_ayuda_global' => $global,
            ':fk_rhc068_ayuda_especifica' => $especifica,
            ':fk_a006_rama' => $rama,
            ':fk_rhc040_institucion' => $institucion,
            ':fk_rhc047_medico_hcm' => $medico,
            ':flag_planilla_solicitud' =>$f_planilla,
            ':flag_informe_medico' =>$f_informe,
            ':flag_factura' => $f_factura,
            ':flag_recipe_medico' => $f_recipe,
            ':flag_otro' => $f_otro,
            ':ind_estado' =>  1,
            ':num_monto' => $monto,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
        ));

        $idRegistro= $this->_db->lastInsertId();

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=1;"
        );
    }




    #PERMITE REGISTRAR BENEFICIO HCM
    public function metModificarBeneficioPersonalHcm($organismo,$tipo,$empleado,$persona,$global,$especifica,$rama,$institucion,$medico ,$f_planilla,$f_informe,$f_factura,$f_recipe,$f_otro,$monto,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "UPDATE
                rh_c046_beneficio_medico
             SET

                fk_a001_organismo=:fk_a001_organismo,
                fk_a006_tipo_solicitud=:fk_a006_tipo_solicitud,
                fk_rhb001_empleado=:fk_rhb001_empleado,
                fk_a003_persona=:fk_a003_persona,
                fk_rhb008_ayuda_global=:fk_rhb008_ayuda_global,
                fk_rhc068_ayuda_especifica=:fk_rhc068_ayuda_especifica,
                fk_a006_rama=:fk_a006_rama,
                fk_rhc040_institucion=:fk_rhc040_institucion,
                fk_rhc047_medico_hcm=:fk_rhc047_medico_hcm,
                flag_planilla_solicitud=:flag_planilla_solicitud,
                flag_informe_medico=:flag_informe_medico,
                flag_factura=:flag_factura,
                flag_recipe_medico=:flag_recipe_medico,
                flag_otro=:flag_otro,
                num_monto=:num_monto,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'

                WHERE pk_beneficio_medico='".$id."'

             ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(

            ':fk_a001_organismo' => $organismo,
            ':fk_a006_tipo_solicitud' => $tipo,
            ':fk_rhb001_empleado'=>  $empleado,
            ':fk_a003_persona' => $persona,
            ':fk_rhb008_ayuda_global' => $global,
            ':fk_rhc068_ayuda_especifica' => $especifica,
            ':fk_a006_rama' => $rama,
            ':fk_rhc040_institucion' => $institucion,
            ':fk_rhc047_medico_hcm' => $medico,
            ':flag_planilla_solicitud' =>$f_planilla,
            ':flag_informe_medico' =>$f_informe,
            ':flag_factura' => $f_factura,
            ':flag_recipe_medico' => $f_recipe,
            ':flag_otro' => $f_otro,
            ':num_monto' => $monto

        ));



        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    #PERMITE CAMBIAR ESTATUS
    public function metAnularBeneficioPersonalHcm($id,$valor)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "UPDATE
                rh_c046_beneficio_medico
             SET

                ind_estado=:num_estado,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario

                WHERE pk_beneficio_medico='".$id."'

             ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(

            ':num_estado' => $valor,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario ,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),

        ));

            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
      }


    public function metBeneficioActualizar ($id,$estatus){

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();


        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "UPDATE
                rh_c046_beneficio_medico
             SET

                ind_estado=:num_estado,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'

                WHERE pk_beneficio_medico='$id'

             ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':num_estado' => $estatus
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $id;
        }



    }

    // Método que muestra el listado de permisos solicitados por el funcionario por id
    public function metListarEstadoHCM()
    {
        $listarSolicitud =  $this->_db->query(
            "   SELECT

                a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
                a006_miscelaneo_detalle.cod_detalle,
                a006_miscelaneo_detalle.ind_nombre_detalle

                from a005_miscelaneo_maestro


                inner join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro  )


                WHERE   a005_miscelaneo_maestro.cod_maestro='ESTADOHCM'

        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();
    }

    public function metObtenerFuncionarioBitacora($idBeneficio,$estado)
    {


        $listarSolicitud =  $this->_db->query(
           "
             SELECT   rh_c054_hcm_bitacora.fk_a006_miscelaneo, rh_c054_hcm_bitacora.fk_a018_num_seguridad_usuario
            ,a018_seguridad_usuario.fk_rhb001_num_empleado,rh_b001_empleado.fk_a003_num_persona,
            a003_persona.ind_nombre1,a003_persona.ind_apellido1
            
            FROM rh_c054_hcm_bitacora
            
            
            left join a018_seguridad_usuario on (a018_seguridad_usuario.pk_num_seguridad_usuario=rh_c054_hcm_bitacora.fk_a018_num_seguridad_usuario
            
            )
            
            left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=a018_seguridad_usuario.fk_rhb001_num_empleado)
            left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
            
            WHERE  rh_c054_hcm_bitacora.fk_rhc046_beneficio_medico=$idBeneficio AND rh_c054_hcm_bitacora.fk_a006_miscelaneo=$estado

        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();



    }


    public function metdeleteFacturasId($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `rh_c058_factura_hcm` " .
            "where fk_rhc046_beneficio_medico= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }



    public function metListarOperacionesHcm($empleado)
    {
        $datos = $this->_db->query("select * from vl_rh_hcm_extension where pk_num_empleado='$empleado'");
        $datos->setFetchMode(PDO::FETCH_ASSOC);
        return $datos->fetchAll();
    }

    public function metMostrarExtension($idExtension)
    {
        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c044_extension_beneficio
                WHERE pk_num_extension='$idExtension'
               ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metRegistrarExtension($datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #descompongo el array de datos
        $anio = $datos[0];
        $ayuda_especifica1 = $datos[1];
        $ayuda_global = $datos[2];
        $empleado = $datos[3];
        $ma_ayuda_especifica1 = $datos[4];
        $ayuda_especifica2 = $datos[5];
        $ma_ayuda_especifica2 = $datos[6];
        $md_ayuda_especifica2 = $datos[7];
        $mdi_ayuda_especifica2 = $datos[8];
        $monto_f = $ma_ayuda_especifica1 + $mdi_ayuda_especifica2;
        $monto_d = $ma_ayuda_especifica2 - $mdi_ayuda_especifica2;


        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c044_extension_beneficio
             SET
                fec_anio=:fec_anio,
                fk_rhc068_ayuda_especifica=:fk_rhc068_ayuda_especifica,
                fk_rhb008_ayuda_global=:fk_rhb008_ayuda_global,
                fk_rhb001_empleado=:fk_rhb001_empleado,
                num_monto_predeterminado=:num_monto_predeterminado,
                fk_rhc068_ayuda_especifica_disminucion=:fk_rhc068_ayuda_especifica_disminucion,
                num_monto_disminucion_predeterminado=:num_monto_disminucion_predeterminado,
                num_monto_disponible=:num_monto_disponible,
                num_monto_operacion=:num_monto_operacion,
                num_monto_final=:num_monto_final,
                num_monto_disminucion_final=:num_monto_disminucion_final,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fec_anio' => $anio,
            ':fk_rhc068_ayuda_especifica' => $ayuda_especifica1,
            ':fk_rhb008_ayuda_global' => $ayuda_global,
            ':fk_rhb001_empleado' => $empleado,
            ':num_monto_predeterminado' => $ma_ayuda_especifica1,
            ':fk_rhc068_ayuda_especifica_disminucion' => $ayuda_especifica2,
            ':num_monto_disminucion_predeterminado' => $ma_ayuda_especifica2 ,
            ':num_monto_disponible' => $md_ayuda_especifica2,
            ':num_monto_operacion' => $mdi_ayuda_especifica2,
            ':num_monto_final' => $monto_f,
            ':num_monto_disminucion_final' => $monto_d
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            #REGISTRO Y ACTUALIZO tabla distributivas de asignaciones con la extension del empleado
            #consulto las ayudas especificas de acuerdo al beneficio global
            #consulto para verificar si el empleado ya tiene extension
            $verificar = $this->_db->query("select * from rh_c052_ayuda_especifica_extension where fk_rhb001_num_empleado='$empleado' and fk_rhb008_num_ayuda_global='$ayuda_global'");
            $verificar->setFetchMode(PDO::FETCH_ASSOC);
            $contador = count($verificar->fetchAll());

            if($contador==0){//no tiene registros


                $consulta = $this->_db->query("select * from rh_c068_ayuda_especifica where fk_rhb008_num_ayuda_global='$ayuda_global'");
                $consulta->setFetchMode(PDO::FETCH_ASSOC);
                $lista = $consulta->fetchAll();

                $inserto = $this->_db->prepare(
                    "INSERT INTO
                        rh_c052_ayuda_especifica_extension
                     SET
                        fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                        fk_rhb008_num_ayuda_global=:fk_rhb008_num_ayuda_global,
                        fk_rhc068_num_ayuda_especifica=:fk_rhc068_num_ayuda_especifica,
                        num_limite_especifica=:num_limite_especifica,
                        ind_tipo=:ind_tipo,
                        num_monto_operacion=:num_monto_operacion,
                        num_monto_definitivo=:num_monto_definitivo,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                      ");

                foreach($lista  as $reg){

                    if($reg['pk_num_ayuda_especifica']==$ayuda_especifica1){
                        $tipo='I';
                        $md = $reg['num_limite_esp'] + $mdi_ayuda_especifica2;
                        $mo = $mdi_ayuda_especifica2;
                    }
                    else if($reg['pk_num_ayuda_especifica']==$ayuda_especifica2){
                        $tipo='D';
                        $md = $reg['num_limite_esp'] - $mdi_ayuda_especifica2;
                        $mo = $mdi_ayuda_especifica2;
                    }else{
                        $tipo='N';
                        $md = $reg['num_limite_esp'];
                        $mo = '0.000000';
                    }
                    $inserto->execute(array(
                        ':fk_rhb001_num_empleado' => $empleado,
                        ':fk_rhb008_num_ayuda_global' => $ayuda_global,
                        ':fk_rhc068_num_ayuda_especifica' => $reg['pk_num_ayuda_especifica'],
                        ':num_limite_especifica' => $reg['num_limite_esp'],
                        ':ind_tipo' => $tipo,
                        ':num_monto_operacion' => $mo,
                        ':num_monto_definitivo' => $md
                    ));

                }


            }else{//actualizo los beneficios


                $consulta = $this->_db->query("select * from rh_c052_ayuda_especifica_extension where fk_rhb008_num_ayuda_global='$ayuda_global' and fk_rhb001_num_empleado='$empleado'");
                $consulta->setFetchMode(PDO::FETCH_ASSOC);
                $lista = $consulta->fetchAll();

                print_r($lista);

                /*$inserto = $this->_db->prepare(
                    "UPDATE
                        rh_c052_ayuda_especifica_extension
                     SET
                        ind_tipo=:ind_tipo,
                        num_monto_operacion=:num_monto_operacion,
                        num_monto_definitivo=:num_monto_definitivo,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                      WHERE fk_rhb001_num_empleado='$empleado' and  fk_rhb008_num_ayuda_global='$ayuda_global'
                      ");

                foreach($lista  as $reg){

                    if($reg['fk_rhc068_num_ayuda_especifica']==$ayuda_especifica1){
                        $tipo='I';
                        $md = $reg['num_monto_definitivo'] + $mdi_ayuda_especifica2;
                        $mo = $mdi_ayuda_especifica2;
                    }
                    else if($reg['fk_rhc068_num_ayuda_especifica']==$ayuda_especifica2){
                        $tipo='D';
                        $md = $reg['num_monto_definitivo'] - $mdi_ayuda_especifica2;
                        $mo = $mdi_ayuda_especifica2;
                    }else{
                        $tipo=$reg['ind_tipo'];
                        $md = $reg['num_monto_definitivo'];
                        $mo = $reg['num_monto_operacion'];
                    }
                    $inserto->execute(array(
                        ':ind_tipo' => $tipo,
                        ':num_monto_operacion' => $mo,
                        ':num_monto_definitivo' => $md
                    ));

                }*/


            }//contador



            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }


    }


    public function metModificarExtension($idExtension,$datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #descompongo el array de datos
        $anio = $datos[0];
        $ayuda_especifica1 = $datos[1];
        $ayuda_global = $datos[2];
        $empleado = $datos[3];
        $ma_ayuda_especifica1 = $datos[4];
        $ayuda_especifica2 = $datos[5];
        $ma_ayuda_especifica2 = $datos[6];
        $md_ayuda_especifica2 = $datos[7];
        $mdi_ayuda_especifica2 = $datos[8];
        $monto_f = $ma_ayuda_especifica1 + $mdi_ayuda_especifica2;
        $monto_d = $ma_ayuda_especifica2 - $mdi_ayuda_especifica2;

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $UpRegistro=$this->_db->prepare(
            "UPDATE
               rh_c044_extension_beneficio
             SET
                fec_anio=:fec_anio,
                fk_rhc068_ayuda_especifica=:fk_rhc068_ayuda_especifica,
                fk_rhb008_ayuda_global=:fk_rhb008_ayuda_global,
                fk_rhb001_empleado=:fk_rhb001_empleado,
                num_monto_predeterminado=:num_monto_predeterminado,
                fk_rhc068_ayuda_especifica_disminucion=:fk_rhc068_ayuda_especifica_disminucion,
                num_monto_disminucion_predeterminado=:num_monto_disminucion_predeterminado,
                num_monto_disponible=:num_monto_disponible,
                num_monto_operacion=:num_monto_operacion,
                num_monto_final=:num_monto_final,
                num_monto_disminucion_final=:num_monto_disminucion_final,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE
               pk_num_extension='$idExtension'
            ");

        #execute — Ejecuta una sentencia preparada
        $UpRegistro->execute(array(
            ':fec_anio' => $anio,
            ':fk_rhc068_ayuda_especifica' => $ayuda_especifica1,
            ':fk_rhb008_ayuda_global' => $ayuda_global,
            ':fk_rhb001_empleado' => $empleado,
            ':num_monto_predeterminado' => $ma_ayuda_especifica1,
            ':fk_rhc068_ayuda_especifica_disminucion' => $ayuda_especifica2,
            ':num_monto_disminucion_predeterminado' => $ma_ayuda_especifica2,
            ':num_monto_disponible' => $md_ayuda_especifica2,
            ':num_monto_operacion' => $mdi_ayuda_especifica2,
            ':num_monto_final' => $monto_f,
            ':num_monto_disminucion_final' => $monto_d
        ));

        $error = $UpRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idExtension;
        }
    }

}// fin de la clase
?>
