<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class nivelGradoInstruccionModelo extends Modelo
{

    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metConsultarNivelGradoInstruccion()
    {

        $con = $this->_db->query("
            SELECT
            rh_c064_nivel_grado_instruccion.pk_num_nivel_grado,
            rh_c064_nivel_grado_instruccion.fk_a006_num_miscelaneo_detalle_gradoinst,
            rh_c064_nivel_grado_instruccion.ind_nombre_nivel_grado,
            rh_c064_nivel_grado_instruccion.num_estatus,
            rh_c064_nivel_grado_instruccion.num_flag_utiles,
            a006_miscelaneo_detalle.ind_nombre_detalle AS grado_instruccion
            FROM
            rh_c064_nivel_grado_instruccion
            INNER JOIN a006_miscelaneo_detalle ON rh_c064_nivel_grado_instruccion.fk_a006_num_miscelaneo_detalle_gradoinst = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarNivelGradoInstruccion($idNivelGradoInstruccion)
    {

        $con = $this->_db->query("
            SELECT
            rh_c064_nivel_grado_instruccion.pk_num_nivel_grado,
            rh_c064_nivel_grado_instruccion.fk_a006_num_miscelaneo_detalle_gradoinst,
            rh_c064_nivel_grado_instruccion.ind_nombre_nivel_grado,
            rh_c064_nivel_grado_instruccion.num_estatus,
            rh_c064_nivel_grado_instruccion.num_flag_utiles,
            a006_miscelaneo_detalle.ind_nombre_detalle AS grado_instruccion
            FROM
            rh_c064_nivel_grado_instruccion
            INNER JOIN a006_miscelaneo_detalle ON rh_c064_nivel_grado_instruccion.fk_a006_num_miscelaneo_detalle_gradoinst = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            WHERE  rh_c064_nivel_grado_instruccion.pk_num_nivel_grado='$idNivelGradoInstruccion'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #MUESTRA LOS REGISTROS SEGUN UN GRADO DE INSTRUCCION ESPECIFICO
    public function metMostrarNivelesGradoInst($GradoInst)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT * FROM rh_c064_nivel_grado_instruccion WHERE fk_a006_num_miscelaneo_detalle_gradoinst='$GradoInst'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE REGISTRAR UN NIVEL GRADO DE INSTRUCCIÓN EN LA BD
    public function metRegistrarNivelGradoInstruccion($grado,$desc,$est,$flag_utiles)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c064_nivel_grado_instruccion
                (fk_a006_num_miscelaneo_detalle_gradoinst,ind_nombre_nivel_grado,num_estatus,num_flag_utiles,fk_a018_num_seguridad_usuario,fec_ultima_modificacion)
                VALUES
                (:fk_a006_num_miscelaneo_detalle_gradoinst,:ind_nombre_nivel_grado,:num_estatus,:num_flag_utiles,'$this->atIdUsuario',NOW())
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_gradoinst' => $grado,
            ':ind_nombre_nivel_grado'   => $desc,
            ':num_estatus'              => $est,
            ':num_flag_utiles'          => $flag_utiles
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR UN NIVEL GRADO DE INSTRUCCION EN LA BD
    public function metModificarNivelGradoInstruccion($idNivelGradoInstruccion,$grado,$desc,$est,$flag_utiles)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              rh_c064_nivel_grado_instruccion
            SET
              fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
              ind_nombre_nivel_grado=:ind_nombre_nivel_grado,
              num_estatus=:num_estatus,
              num_flag_utiles=:num_flag_utiles,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE
            pk_num_nivel_grado='$idNivelGradoInstruccion'
            "
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_gradoinst' => $grado,
            ':ind_nombre_nivel_grado'   => $desc,
            ':num_estatus'              => $est,
            ':num_flag_utiles'          => $flag_utiles
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE ELIMINAR UN NIEVL GRADO DE INSTRUCCION DE LA BD
    public function metEliminarNivelGradoInstruccion($idNivelGradoInstruccion)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c064_nivel_grado_instruccion where pk_num_nivel_grado ='$idNivelGradoInstruccion'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }



}
