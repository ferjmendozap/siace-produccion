<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de los dias de disfrute de vacaciones
class tablaVacacionModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    // Método que permite listar la tabla de vacaciones
    public function metListarTabla()
    {
        $tabla =  $this->_db->query(
            "select a.pk_num_tabla_vacacion, a.num_anios, a.num_dias_disfrute, a.num_dias_adicionales, a.num_total_disfrutar, b.ind_nombre_nomina from rh_c024_tabla_vacacion as a, nm_b001_tipo_nomina as b where a.fk_nmb001_num_nomina=b.pk_num_tipo_nomina order by a.pk_num_tabla_vacacion"
        );
        $tabla->setFetchMode(PDO::FETCH_ASSOC);
        return $tabla->fetchAll();
    }

    // Método que permite consultar la tabla de vacaciones
    public function metConsultarTabla($pkNumTablaVacacion)
    {
        $tablaConsultar =  $this->_db->query(
            "select a.pk_num_tabla_vacacion, a.num_anios, a.num_dias_disfrute, a.num_dias_adicionales, a.num_total_disfrutar, b.pk_num_tipo_nomina, b.ind_nombre_nomina from rh_c024_tabla_vacacion as a, nm_b001_tipo_nomina as b where a.fk_nmb001_num_nomina=b.pk_num_tipo_nomina and a.pk_num_tabla_vacacion=$pkNumTablaVacacion"
        );
        $tablaConsultar->setFetchMode(PDO::FETCH_ASSOC);
        return $tablaConsultar->fetch();
    }

    // Método que permite guardar la tabla de vacaciones
    public function metGuardarTabla($tipoNomina, $anios, $diasDisfrute, $diasAdicionales, $totalDisfrute, $usuario, $fecha_hora)
    {
        $this->_db->beginTransaction();
        $NuevaTabla = $this->_db->prepare(
            "insert into rh_c024_tabla_vacacion values (null, :fk_nmb001_num_nomina, :num_anios, :num_dias_disfrute, :num_dias_adicionales, :num_total_disfrutar, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $NuevaTabla->execute(array(
            ':fk_nmb001_num_nomina' => $tipoNomina,
            ':num_anios' => $anios,
            ':num_dias_disfrute' => $diasDisfrute,
            ':num_dias_adicionales' => $diasAdicionales,
            ':num_total_disfrutar' => $totalDisfrute,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fec_ultima_modificacion' => $fecha_hora
        ));
        $pkNumTablaVacacion = $this->_db->lastInsertId();
        $this->_db->commit();
        return  $pkNumTablaVacacion;
    }

    // Método que permite eliminar un disfrute
    public function metEliminarTabla($pkNumTablaVacacion)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from rh_c024_tabla_vacacion where pk_num_tabla_vacacion = $pkNumTablaVacacion"
        );
        $this->_db->commit();
    }

    public function metEditarTabla($tipoNomina, $anios, $diasDisfrute, $diasAdicionales, $totalDisfrute, $usuario, $fecha_hora, $pkNumTablaVacacion)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update rh_c024_tabla_vacacion set fk_nmb001_num_nomina=$tipoNomina, num_anios=$anios, num_dias_disfrute=$diasDisfrute, num_dias_adicionales=$diasAdicionales, num_total_disfrutar=$totalDisfrute, fk_a018_num_seguridad_usuario=$usuario, fec_ultima_modificacion='$fecha_hora'  where pk_num_tabla_vacacion=$pkNumTablaVacacion"
        );
        $this->_db->commit();
    }
}// fin de la clase
?>
