<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class dependenciasModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE MOSTRAR TODOS LAS DEPENDECIAS
    public function metConsultarDependencias($estatus = true)
    {
        if($estatus){
            $filtro = 'WHERE a004_dependencia.num_estatus=1';
        }else{
            $filtro = '';
        }

        $con = $this->_db->query("
            SELECT
            a004_dependencia.pk_num_dependencia,
            a004_dependencia.ind_dependencia,
            a004_dependencia.ind_codinterno,
            a004_dependencia.ind_codpadre,
            a004_dependencia.ind_nivel,
            a004_dependencia.num_flag_controlfiscal,
            a004_dependencia.num_flag_principal,
            a004_dependencia.num_piso,
            a004_dependencia.num_estatus,
            a004_dependencia.fk_a001_num_organismo,
            a004_dependencia.fk_a003_num_persona_responsable,
            a004_dependencia.num_flag_responsable,
            a004_dependencia.txt_abreviacion,
            a003_persona.ind_nombre1,
            a003_persona.ind_nombre2,
            a003_persona.ind_apellido1,
            a003_persona.ind_apellido2,
            a001_organismo.ind_descripcion_empresa
            FROM
            a004_dependencia
            LEFT JOIN a003_persona ON a003_persona.pk_num_persona = a004_dependencia.fk_a003_num_persona_responsable
            LEFT JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = a004_dependencia.fk_a001_num_organismo
            $filtro
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #para listar select organismos
    public function metMostrarOrganismosSelect()
    {
        $con = $this->_db->query("
            SELECT
            pk_num_organismo,
            ind_descripcion_empresa
            FROM
            a001_organismo
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE LISTAR LOS EMPLEADOS
    public function metListarEmpleados()
    {

        $con = $this->_db->query("
              SELECT
                rh_b001_empleado.pk_num_empleado,
                rh_b001_empleado.fk_a003_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona and a003_persona.num_estatus=1 and rh_b001_empleado.num_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();

    }

    #PERMITE BUSCAR UNA DEPENDENCIA PARA SER MOSTRADO EN EL FORMULARIO
    public function metMostrarDependencias($idDependencia)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
            a004_dependencia.pk_num_dependencia,
            a004_dependencia.ind_dependencia,
            a004_dependencia.ind_codinterno,
            a004_dependencia.ind_codpadre,
            a004_dependencia.ind_nivel,
            a004_dependencia.num_flag_controlfiscal,
            a004_dependencia.num_flag_principal,
            a004_dependencia.num_piso,
            a004_dependencia.num_estatus,
            a004_dependencia.fk_a001_num_organismo,
            a004_dependencia.fk_a003_num_persona_responsable,
            a004_dependencia.num_flag_responsable,
            a004_dependencia.txt_abreviacion,
            a003_persona.ind_nombre1,
            a003_persona.ind_nombre2,
            a003_persona.ind_apellido1,
            a003_persona.ind_apellido2,
            a001_organismo.ind_descripcion_empresa
            FROM
            a004_dependencia
            LEFT JOIN a003_persona ON a003_persona.pk_num_persona = a004_dependencia.fk_a003_num_persona_responsable
            LEFT JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = a004_dependencia.fk_a001_num_organismo
            WHERE a004_dependencia.pk_num_dependencia='$idDependencia'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #METODO PARA REGISTRAR LA DEPENDENCIA
    public function metRegistrarDependencias($organismo,$dependencia,$codpadre,$codinterno,$nivel,$piso,$abreviacion,$empleado,$flag_responsable,$flag_controlfiscal,$flag_principal,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #para verificar si existe una dependencia con el mismo nombre
        $con = $this->_db->query("select * from a004_dependencia where ind_dependencia='$dependencia'");
        if($con->fetchColumn() > 0){#hay filas con la misma nombre de la dependencia

            $x = 'repDesc';//repetida dependencia
            return $x;

        }else{

            $con1 = $this->_db->query("select * from a004_dependencia where ind_codinterno='$codinterno'");
            if($con1->fetchColumn() > 0) {#hay filas con el mismo codigo interno
                $x = 'repCodInt';//repetido codigo interno
                return $x;

            }else{

                #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
                $NuevoRegistro = $this->_db->prepare(
                    "INSERT INTO
                        a004_dependencia
                     SET
                        ind_dependencia=:ind_dependencia,
                        ind_codinterno=:ind_codinterno,
                        ind_codpadre=:ind_codpadre,
                        ind_nivel=:ind_nivel,
                        num_flag_controlfiscal=:num_flag_controlfiscal,
                        num_flag_principal=:num_flag_principal,
                        num_piso=:num_piso,
                        num_estatus=:ind_estatus,
                        fk_a001_num_organismo=:fk_a001_num_organismo,
                        fk_a003_num_persona_responsable=:fk_a003_num_persona_responsable,
                        num_flag_responsable=:num_flag_responsable,
                        txt_abreviacion=:txt_abreviacion,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                      ");

                        #execute — Ejecuta una sentencia preparada
                        $NuevoRegistro->execute(array(
                            ':ind_dependencia' => $dependencia,
                            ':ind_codinterno' => $codinterno,
                            ':ind_codpadre' => $codpadre,
                            ':ind_nivel' => $nivel,
                            ':num_flag_controlfiscal' => $flag_controlfiscal,
                            ':num_flag_principal' => $flag_principal,
                            ':num_piso' => $piso,
                            ':ind_estatus' => $estatus,
                            ':fk_a001_num_organismo' => $organismo,
                            ':fk_a003_num_persona_responsable' => $empleado,
                            ':num_flag_responsable' => $flag_responsable,
                            ':txt_abreviacion' => $abreviacion
                        ));

                        $error = $NuevoRegistro->errorInfo();

            }

        }




        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR UN REGISTRO EN LA BASE DE DATOS (DEPENDENCIAS)
    public function metModificarDependencias($idDependencia,$organismo,$dependencia,$codpadre,$codinterno,$nivel,$piso,$abreviacion,$empleado,$flag_responsable,$flag_controlfiscal,$flag_principal,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $ModificarRegistro=$this->_db->prepare(
            "UPDATE
              a004_dependencia
            SET
                ind_dependencia=:ind_dependencia,
                ind_codinterno=:ind_codinterno,
                ind_codpadre=:ind_codpadre,
                ind_nivel=:ind_nivel,
                num_flag_controlfiscal=:num_flag_controlfiscal,
                num_flag_principal=:num_flag_principal,
                num_piso=:num_piso,
                num_estatus=:ind_estatus,
                fk_a001_num_organismo=:fk_a001_num_organismo,
                fk_a003_num_persona_responsable=:fk_a003_num_persona_responsable,
                num_flag_responsable=:num_flag_responsable,
                txt_abreviacion=:txt_abreviacion,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
            pk_num_dependencia='$idDependencia'");


        #execute — Ejecuta una sentencia preparada
        $ModificarRegistro->execute(array(
            ':ind_dependencia' => $dependencia,
            ':ind_codinterno' => $codinterno,
            ':ind_codpadre' => $codpadre,
            ':ind_nivel' => $nivel,
            ':num_flag_controlfiscal' => $flag_controlfiscal,
            ':num_flag_principal' => $flag_principal,
            ':num_piso' => $piso,
            ':ind_estatus' => $estatus,
            ':fk_a001_num_organismo' => $organismo,
            ':fk_a003_num_persona_responsable' => $empleado,
            ':num_flag_responsable' => $flag_responsable,
            ':txt_abreviacion' => $abreviacion
        ));

        $error = $ModificarRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idDependencia;
        }
    }

    #PERMITE ELIMINAR UNA DEPENDENCIA DE LA BD
    public function metEliminarDependencias($idDependencia)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from a004_dependencia where pk_num_dependencia ='$idDependencia'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metVerificarNivelDependencia($dependencia)
    {


        $con = $this->_db->query("
              SELECT
                *
                FROM
                a004_dependencia
                where ind_codpadre='$dependencia'
        ");

        return $con->fetchAll();


    }


    // Método que permite listar lo empleados asociados a una dependencia
    public function metDependenciaEmpleado($pkNumDependencia)
    {
        $empleado =  $this->_db->query(
            "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b, rh_c076_empleado_organizacion as c where a.fk_a003_num_persona=b.pk_num_persona and c.fk_a004_num_dependencia=$pkNumDependencia and c.fk_rhb001_num_empleado=a.pk_num_empleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método que permite listar lo empleados asociados a una dependencia
    public function metEmpleadoDependencia($idEmpleado)
    {
        $empleado =  $this->_db->query(
            "SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                a004_dependencia.ind_dependencia
            FROM
                rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
            INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
            WHERE rh_b001_empleado.pk_num_empleado='$idEmpleado'
            "
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependenciaOrganismo($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }


    public function metResponsableDependencia($codInterno)
    {
        $responsable =  $this->_db->query(
            "SELECT
                a004_dependencia.pk_num_dependencia,
                a004_dependencia.ind_codinterno,
                a004_dependencia.ind_dependencia,
                a003_persona.ind_cedula_documento,
                CONCAT_WS(
                    ' ',
                    a003_persona.ind_nombre1,
                    a003_persona.ind_nombre2,
                    a003_persona.ind_apellido1,
                    a003_persona.ind_apellido2
                ) AS responsable
            FROM
                a004_dependencia
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = a004_dependencia.fk_a003_num_persona_responsable
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            WHERE
                a004_dependencia.ind_codinterno = '$codInterno'
            "
        );
        $responsable->setFetchMode(PDO::FETCH_ASSOC);
        return $responsable->fetch();
    }


    public function metNumeroInteroDependencia($dependencia)
    {
        $responsable =  $this->_db->query(
            "SELECT
                ind_codinterno
            FROM
                a004_dependencia
            WHERE
                pk_num_dependencia = '$dependencia'
            "
        );
        $responsable->setFetchMode(PDO::FETCH_ASSOC);
        return $responsable->fetch();
    }


}//fin clase
