<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class horarioLaboralModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarHorarioLaboral()
    {

        $con = $this->_db->query("
                SELECT
                 *
                FROM
                rh_b007_horario_laboral
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR UN HORARIO LABORAL
    public function metMostrarHorarioGeneral($idHorario)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_b007_horario_laboral
                WHERE  pk_num_horario='$idHorario'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE MOSTRAR O CONSULTAR UN HORARIO LABORAL en DETALLE
    public function metMostrarHorarioDetalle($idHorario)
    {

        $con = $this->_db->query("
                SELECT
                pk_num_horario_detalle,
                fk_rhb007_num_horario,
                num_dia,
                num_flag_laborable,
                fec_entrada1,
                fec_salida1,
                fec_entrada2,
                fec_salida2,
                fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion
                FROM
                rh_c067_horario_detalle
                WHERE  fk_rhb007_num_horario='$idHorario'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE ARMAR LOS DIAS DE LA SEMANA PARA EL FORMUALRIO
    public function metDiasSemana()
    {
        $con = $this->_db->query("
                (SELECT '1' AS num_dia, 'LUNES' AS dia_nombre, '1' AS FlagLaborable) UNION
				(SELECT '2' AS num_dia, 'MARTES' AS dia_nombre, '1' AS FlagLaborable) UNION
				(SELECT '3' AS num_dia, 'MIERCOLES' AS dia_nombre, '1' AS FlagLaborable) UNION
				(SELECT '4' AS num_dia, 'JUEVES' AS dia_nombre, '1' AS FlagLaborable) UNION
				(SELECT '5' AS num_dia, 'VIERNES' AS dia_nombre, '1' AS FlagLaborable) UNION
				(SELECT '6' AS num_dia, 'SABADO' AS dia_nombre, '0' AS FlagLaborable) UNION
				(SELECT '7' AS num_dia, 'DOMINGO' AS dia_nombre, '0' AS FlagLaborable)
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE REGISTRAR HORARIO
    public function metRegistrarHorariolaboral($horario,$flag_corrido,$resolucion,$fec_resolucion,$dia=false,$laborable=false,$e1=false,$s1=false,$e2=false,$s2=false,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        $fecha = explode("-",$fec_resolucion);
        #para verificar si existe un horario con la misma descripcion o nombre
        $con = $this->_db->query("select * from rh_b007_horario_laboral where ind_descripcion='$horario'");
        if($con->fetchColumn() > 0){#hay filas con la misma descripcion del horario

            $x = 'rep';
            return $x;

        }
        else{

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                rh_b007_horario_laboral
                SET
                ind_descripcion=:ind_descripcion,
                num_flag_corrido=:num_flag_corrido,
                ind_resolucion=:ind_resolucion,
                fec_resolucion=:fec_resolucion,
                num_estatus=:ind_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':ind_descripcion'  => $horario,
                ':num_flag_corrido' => $flag_corrido,
                ':ind_resolucion'   => $resolucion,
                ':fec_resolucion'   => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                ':ind_estatus'      => $estatus
            ));

            $idHorario = $this->_db->lastInsertId();

            if($dia) {

                $InsertoDetalleHorario = $this->_db->prepare(
                    "INSERT INTO rh_c067_horario_detalle
                     SET
                     fk_rhb007_num_horario=:fk_rhb007_num_horario,
                     num_dia=:num_dia,
                     num_flag_laborable=:num_flag_laborable,
                     fec_entrada1=:fec_entrada1,
                     fec_salida1=:fec_salida1,
                     fec_entrada2=:fec_entrada2,
                     fec_salida2=:fec_salida2,
                     fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                     fec_ultima_modificacion=NOW()
                    "
                );

                for ($i = 0; $i < count($dia); $i++) {

                    if(!isset($laborable[$i])){$laborable[$i]=0;}
                    if($e1){
                        if(isset($e1[$i])){
                            if(!empty($e1[$i])){
                                $entrada1 = $this->formatoHora24($e1[$i]);
                            }else{
                                $entrada1 = '00:00:00';
                            }
                        }else{
                            $entrada1 = '00:00:00';
                        }
                    }
                    if($s1){
                        if(isset($s1[$i])){
                            if(!empty($s1[$i])){
                                $salida1 = $this->formatoHora24($s1[$i]);
                            }else{
                                $salida1 = '00:00:00';
                            }

                        }else{
                            $salida1 = '00:00:00';
                        }
                    }
                    if($e2){
                        if(isset($e2[$i])){
                            if(!empty($e2[$i])){
                                $entrada2 = $this->formatoHora24($e2[$i]);
                            }else{
                                $entrada2 = '00:00:00';
                            }

                        }else{
                            $entrada2 = '00:00:00';
                        }
                    }
                    if($s2){
                        if(isset($s2[$i])){
                            if(!empty($s2[$i])) {
                                $salida2 = $this->formatoHora24($s2[$i]);
                            }else{
                                $salida2 = '00:00:00';
                            }

                        }else{
                            $salida2 = '00:00:00';
                        }
                    }

                    $InsertoDetalleHorario->execute(array(
                        ':fk_rhb007_num_horario' => $idHorario,
                        ':num_dia'  => $dia[$i],
                        ':num_flag_laborable'  => $laborable[$i],
                        ':fec_entrada1'  => $entrada1,
                        ':fec_salida1'   => $salida1,
                        ':fec_entrada2'  => $entrada2,
                        ':fec_salida2'   => $salida2
                    ));
                }
            }

        }

        $error1 = $NuevoRegistro->errorInfo();

        if($dia){
            $error2 = $InsertoDetalleHorario->errorInfo();
        }else{
            $error2 =array(1=>null,2=>null);
        }

        if(!empty($error1[1]) && !empty($error1[2])  && !empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            return $error1;
        }else{
            //$idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idHorario;
        }
    }

    #PERMITE MODIFICAR HORARIO
    public function metModificarHorarioLaboral($idHorario,$horario,$flag_corrido,$resolucion,$fec_resolucion,$dia=false,$laborable=false,$e1=false,$s1=false,$e2=false,$s2=false,$detalle=false,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        $fecha = explode("-",$fec_resolucion);
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_b007_horario_laboral
             SET
                ind_descripcion=:ind_descripcion,
                num_flag_corrido=:num_flag_corrido,
                ind_resolucion=:ind_resolucion,
                fec_resolucion=:fec_resolucion,
                num_estatus=:ind_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_horario='$idHorario'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_descripcion'  => $horario,
            ':num_flag_corrido' => $flag_corrido,
            ':ind_resolucion'   => $resolucion,
            ':fec_resolucion'   => $fecha[2]."-".$fecha[1]."-".$fecha[0],
            ':ind_estatus'      => $estatus
        ));

        $error1 = $NuevoRegistro->errorInfo();

        if(empty($error1[1]) && empty($error1[2])){

                /*$eliminarDetalleHorario=$this->_db->prepare(
                    "DELETE FROM rh_c067_horario_detalle WHERE  fk_rhb007_num_horario=:id_horario"
                );
                $eliminarDetalleHorario->execute(array(':id_horario'=>$idHorario));*/


                if($detalle) {

                    $InsertoDetalleHorario = $this->_db->prepare(
                            "UPDATE
                              rh_c067_horario_detalle
                             SET
                             fec_entrada1=:fec_entrada1,
                             fec_salida1=:fec_salida1,
                             fec_entrada2=:fec_entrada2,
                             fec_salida2=:fec_salida2,
                             fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                             fec_ultima_modificacion=NOW()
                             WHERE
                             pk_num_horario_detalle=:pk_num_horario_detalle
                            "
                    );

                    for ($i = 0; $i < count($detalle); $i++) {

                        if(!isset($laborable[$i])){$laborable[$i]=0;}
                        if($e1){
                            if(isset($e1[$i])){
                                if(!empty($e1[$i])){
                                    $entrada1 = $this->formatoHora24($e1[$i]);
                                }else{
                                    $entrada1 = '00:00:00';
                                }
                            }else{
                                $entrada1 = '00:00:00';
                            }
                        }
                        if($s1){
                            if(isset($s1[$i])){
                                if(!empty($s1[$i])){
                                    $salida1 = $this->formatoHora24($s1[$i]);
                                }else{
                                    $salida1 = '00:00:00';
                                }

                            }else{
                                $salida1 = '00:00:00';
                            }
                        }
                        if($e2){
                            if(isset($e2[$i])){
                                if(!empty($e2[$i])){
                                    $entrada2 = $this->formatoHora24($e2[$i]);
                                }else{
                                    $entrada2 = '00:00:00';
                                }

                            }else{
                                $entrada2 = '00:00:00';
                            }
                        }
                        if($s2){
                            if(isset($s2[$i])){
                                if(!empty($s2[$i])) {
                                    $salida2 = $this->formatoHora24($s2[$i]);
                                }else{
                                    $salida2 = '00:00:00';
                                }

                            }else{
                                $salida2 = '00:00:00';
                            }
                        }

                        $InsertoDetalleHorario->execute(array(
                            ':fec_entrada1'  => $entrada1,
                            ':fec_salida1'   => $salida1,
                            ':fec_entrada2'  => $entrada2,
                            ':fec_salida2'   => $salida2,
                            ':pk_num_horario_detalle' => $detalle[$i]
                        ));

                    }
                }

            //$error2 = $eliminarDetalleHorario->errorInfo();

            if($dia){
                $error2 = $InsertoDetalleHorario->errorInfo();
            }else{
                $error2 =array(1=>null,2=>null);
            }

        }


        if(!empty($error1[1]) && !empty($error1[2]) && !empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            return $error1;
        }else{
            $this->_db->commit();
            return $idHorario;
        }

    }

    #PERMITE ELIMINAR HORARIO LABORAL
    public function metEliminarHorarioLaboral($idHorario)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        $con = $this->_db->query("
                SELECT
                count(*) as num
                FROM
                rh_c005_empleado_laboral
                WHERE  fk_rhb007_num_horario='$idHorario'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado =  $con->fetch();

        if($resultado['num']>0){

            return 0;

        }else{
            #ejecuto la consulta a la base de datos
            $this->_db->query(
                "
            delete from rh_c067_horario_detalle where fk_rhb007_num_horario='$idHorario';
            delete from rh_b007_horario_laboral where pk_num_horario='$idHorario';
            "
            );

            #commit — Consigna una transacción
            $this->_db->commit();
            return 1;
        }



    }

    #permite ajustar la hora a formato 24
    public function formatoHora24($hora)
    {

        return date("H:i",strtotime($hora));

    }

    #permite ajustar la hora a formato 12 horas
    function formatoHora12($hora, $seg=false) {
        list($h, $m, $s) = explode(":", $hora);
        $time = "";
        if ($seg) {
            if ($h >= "01" && $h < 12) $time = "$h:$m:$s am";
            if ($h == 12) $time = "$h:$m:$s pm";
            elseif ($h == "00") $time = "12:$m:$s am";
            elseif ($h > 12) {
                $hh = $h - 12;
                if ($hh < 10) $hh = "0$hh";
                $time = "$hh:$m:$s pm";
            }
        } else {
            if ($h >= "01" && $h < 12) $time = "$h:$m am";
            if ($h == 12) $time = "$h:$m pm";
            elseif ($h == "00") $time = "12:$m am";
            elseif ($h > 12) {
                $hh = $h - 12;
                if ($hh < 10) $hh = "0$hh";
                $time = "$hh:$m pm";
            }
        }
        return $time;
    }


}
