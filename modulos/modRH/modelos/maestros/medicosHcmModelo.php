<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
class medicosHcmModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarMedicos()
    {

        $con = $this->_db->query("
                SELECT
                rh_c043_medico_hcm.pk_num_medico_hcm,
                rh_c043_medico_hcm.fk_a003_persona,
                rh_c043_medico_hcm.fk_rhc042_institucion,
                a003_persona.ind_nombre1,
                a003_persona.ind_apellido1
                FROM
                rh_c043_medico_hcm
                left join a003_persona on (a003_persona.pk_num_persona=rh_c043_medico_hcm.fk_a003_persona)
                left join rh_c042_institucion_hcm on (rh_c042_institucion_hcm.pk_num_institucion=rh_c043_medico_hcm.fk_rhc042_institucion)
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE CONSULTAR DE UN ID ESPECIFICA
    public function metConsultarMedicoId($id)
    {

        $con = $this->_db->query("
                SELECT  rh_c043_medico_hcm.*,
                a003_persona.ind_nombre1,
                a003_persona.ind_cedula_documento,
                a003_persona.ind_apellido1
                FROM
                rh_c043_medico_hcm
                left join a003_persona on (a003_persona.pk_num_persona=rh_c043_medico_hcm.fk_a003_persona)
                left join rh_c042_institucion_hcm on (rh_c042_institucion_hcm.pk_num_institucion=rh_c043_medico_hcm.fk_rhc042_institucion)
                WHERE pk_num_medico_hcm='".$id."'
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE CONSULTAR DE UN ID ESPECIFICA Y TRAER SOLO EL PK
    public function metConsultarPkPersona($id)
    {

        $con = $this->_db->query("
                SELECT  rh_c043_medico_hcm.fk_a003_persona

                FROM
                rh_c043_medico_hcm


                WHERE pk_num_medico_hcm='".$id."'
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }
    #PERMITE REGISTRAR UN MÉDICO
    public function metRegistrarMedico($nombre,$apellido,$cedula,$telefono,$institucion,$convenio,$interno)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                a003_persona
                (ind_cedula_documento,ind_documento_fiscal,ind_nombre1,ind_apellido1,ind_tipo_persona,num_estatus,fec_ultima_modificacion,fk_a018_num_seguridad_usuario)
                VALUES
                (:ind_cedula_documento,:ind_documento_fiscal,:ind_nombre1,:ind_apellido1,:ind_tipo_persona,:ind_estatus_persona,NOW(),'$this->atIdUsuario')
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_cedula_documento' => $cedula,
            ':ind_documento_fiscal' => $cedula,
            ':ind_nombre1' => $nombre,
            ':ind_apellido1' => $apellido,
            ':ind_tipo_persona' => 'N',
            ':ind_estatus_persona' => '1',


         ));


        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();

            return $error;

        }else{

            $idRegistro= $this->_db->lastInsertId();

            $SegundoRegistro = $this->_db->prepare(
              "INSERT INTO
                rh_c043_medico_hcm
                (fk_a003_persona, flag_convenio_ces,flag_interno_ces,fk_rhc042_institucion,ind_telefono,fec_ultima_modificacion,fk_a018_num_seguridad_usuario)
                VALUES
                (:fk_a003_persona,:flag_convenio_ces,:flag_interno_ces,:fk_rhc042_institucion,:ind_telefono,:fec_ultima_modificacion,:fk_a018_num_seguridad_usuario)
              ");

            #execute — Ejecuta una sentencia preparada
            $SegundoRegistro->execute(array(
                ':fk_a003_persona' => $idRegistro,
                ':flag_convenio_ces' => $convenio,
                ':flag_interno_ces' => $interno,
                ':fk_rhc042_institucion' => $institucion,
                ':ind_telefono'=> $telefono,
                ':fec_ultima_modificacion' => date('Y-m-d H:i:s')  ,
                ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
            ));

            $error = $SegundoRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;

            }else {
                $idRegistro = $this->_db->lastInsertId();
                $this->_db->commit();
                return $idRegistro;
            }

        }


    }

    #PERMITE MODIFICAR UNA INSTITUCIÓN
    public function metModificarMedico($telefono,$institucion,$convenio,$interno,$nombre,$apellido,$cedula,$persona,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare("UPDATE
             rh_c043_medico_hcm
             SET
             flag_convenio_ces=:flag_convenio_ces,
             flag_interno_ces=:flag_interno_ces,
             fk_rhc042_institucion=:fk_rhc042_institucion,
             ind_telefono=:ind_telefono,
             fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion=:fec_ultima_modificacion
             WHERE pk_num_medico_hcm='".$id."'
             ");


        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(

            ':flag_convenio_ces' => $convenio,
            ':flag_interno_ces' => $interno,
            ':fk_rhc042_institucion' =>$institucion,
            ':ind_telefono' => $telefono,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s')  ,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,

        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;

        }else {

            $SegundoRegistro = $this->_db->prepare("UPDATE
             a003_persona
             SET

             ind_cedula_documento=:ind_cedula_documento,
             ind_documento_fiscal=:ind_documento_fiscal,
             ind_nombre1=:ind_nombre1,
             ind_apellido1=:ind_apellido1



             WHERE pk_num_persona='".$persona."'
             ");


            #execute — Ejecuta una sentencia preparada
            $SegundoRegistro->execute(array(

                ':ind_cedula_documento' => $cedula,
                ':ind_documento_fiscal' => $cedula,
                ':ind_nombre1' =>$nombre,
                ':ind_apellido1' => $apellido,


            ));

            $error2 = $SegundoRegistro->errorInfo();

            if(!empty($error2[1]) && !empty($error2[2])){
                $this->_db->rollBack();
                return $error2;

            }else {

                $this->_db->commit();
                return $id;

            }

        }





    }

    #PERMITE ELIMINAR UNA INSTITUCION DE LA BD
    public function metEliminarMedico($id)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c043_medico_hcm where pk_num_medico_hcm ='$id'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
