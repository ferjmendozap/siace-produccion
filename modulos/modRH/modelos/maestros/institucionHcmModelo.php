<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
class institucionHcmModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarInstitucion()
    {

        $con = $this->_db->query("
                SELECT
                 *
                FROM
                rh_c042_institucion_hcm
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE CONSULTAR DE UN ID ESPECIFICA
    public function metConsultarInstitucionId($id)
    {

        $con = $this->_db->query("
                SELECT
                rh_c042_institucion_hcm.pk_num_institucion,
                rh_c042_institucion_hcm.ind_nombre_institucion,
                rh_c042_institucion_hcm.ind_ubicacion,
                rh_c042_institucion_hcm.ind_telefono1,
                rh_c042_institucion_hcm.ind_telefono2,
                rh_c042_institucion_hcm.fk_a006_clase_centro,
                rh_c042_institucion_hcm.fk_a006_tipo_centro,
                rh_c042_institucion_hcm.flag_convenio_ces

                FROM
                rh_c042_institucion_hcm
                WHERE pk_num_institucion='".$id."'
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE REGISTRAR UNA INSTITUCIÓN
    public function metRegistrarInstitucion($nombre,$ubicacion,$telefono1,$telefono2,$clase,$tipo,$convenio)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c042_institucion_hcm
                (ind_nombre_institucion,ind_ubicacion,ind_telefono1,ind_telefono2,fk_a006_clase_centro,fk_a006_tipo_centro,flag_convenio_ces,fec_ultima_modificacion,fk_a018_num_seguridad_usuario)
                VALUES
                (:ind_nombre_institucion,:ind_ubicacion,:ind_telefono1,:ind_telefono2,:fk_a006_clase_centro,:fk_a006_tipo_centro,:flag_convenio_ces,:fec_ultima_modificacion,:fk_a018_num_seguridad_usuario)
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_nombre_institucion' => $nombre,
            ':ind_ubicacion' => $ubicacion,
            ':ind_telefono1' => $telefono1,
            ':ind_telefono2' => $telefono2,
            ':fk_a006_clase_centro' => $clase,
            ':fk_a006_tipo_centro' => $tipo,
            ':flag_convenio_ces' => $convenio,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s')  ,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,

        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;

        }else {

            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;

        }


    }

    #PERMITE MODIFICAR UNA INSTITUCIÓN
    public function metModificarInstitucion($nombre,$ubicacion,$telefono1,$telefono2,$clase,$tipo,$convenio,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare("UPDATE
             rh_c042_institucion_hcm
             SET
             ind_nombre_institucion=:ind_nombre_institucion,
             ind_ubicacion=:ind_ubicacion,
             ind_telefono1=:ind_telefono1,
             ind_telefono2=:ind_telefono2,
             fk_a006_clase_centro=:fk_a006_clase_centro,
             fk_a006_tipo_centro=:fk_a006_tipo_centro,
             flag_convenio_ces=:flag_convenio_ces,

             fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion=:fec_ultima_modificacion

             WHERE pk_num_institucion='".$id."'
             ");


        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_nombre_institucion' => $nombre,
            ':ind_ubicacion' => $ubicacion,
            ':ind_telefono1' => $telefono1,
            ':ind_telefono2' => $telefono2,
            ':fk_a006_clase_centro' => $clase,
            ':fk_a006_tipo_centro' => $tipo,
            ':flag_convenio_ces' => $convenio,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s')  ,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,

        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;

        }else {

            $this->_db->commit();
            return $id;

        }

    }

    #PERMITE ELIMINAR UNA INSTITUCION DE LA BD
    public function metEliminarInstitucion($id)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c042_institucion_hcm where pk_num_institucion ='$id'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
