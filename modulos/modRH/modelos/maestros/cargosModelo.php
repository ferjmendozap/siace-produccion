<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class cargosModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metConsultarCargos()
    {

        $con = $this->_db->query("
            SELECT
            rh_c063_puestos.pk_num_puestos,
            rh_c063_puestos.fk_rhc006_num_cargo,
            rh_c063_puestos.fk_rhc009_num_nivel,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_grupoocupacional,
            rh_c063_puestos.fk_rhc010_num_serie,
            rh_c063_puestos.ind_descripcion_cargo,
            rh_c063_puestos.txt_descripcion_gen,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria,
            rh_c063_puestos.fk_rhc007_num_grado,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_paso,
            rh_c063_puestos.num_sueldo_basico,
            rh_c063_puestos.num_estatus,
            rh_c006_tipo_cargo.ind_nombre_cargo,
            rh_c009_nivel.ind_nombre_nivel,
            rh_c010_serie.ind_nombre_serie,
            misc1.ind_nombre_detalle AS grupo_ocupacional,
            rh_c007_grado_salarial.ind_nombre_grado,
            misc2.ind_nombre_detalle AS categoria
            FROM
            rh_c063_puestos
            INNER JOIN rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
            INNER JOIN rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
            INNER JOIN a006_miscelaneo_detalle as misc1 ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_grupoocupacional = misc1.pk_num_miscelaneo_detalle
            INNER JOIN rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
            INNER JOIN rh_c010_serie ON rh_c063_puestos.fk_rhc010_num_serie = rh_c010_serie.pk_num_serie
            INNER JOIN a006_miscelaneo_detalle as misc2 ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = misc2.pk_num_miscelaneo_detalle
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarCargo($idCargo)
    {

        $con = $this->_db->query("
              SELECT
            rh_c063_puestos.pk_num_puestos,
            rh_c063_puestos.fk_rhc006_num_cargo,
            rh_c063_puestos.fk_rhc009_num_nivel,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_grupoocupacional,
            rh_c063_puestos.fk_rhc010_num_serie,
            rh_c063_puestos.ind_descripcion_cargo,
            rh_c063_puestos.txt_descripcion_gen,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria,
            rh_c063_puestos.fk_rhc007_num_grado,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_paso,
            rh_c063_puestos.num_sueldo_basico,
            rh_c063_puestos.num_estatus,
            rh_c006_tipo_cargo.ind_nombre_cargo,
            rh_c009_nivel.ind_nombre_nivel,
            rh_c010_serie.ind_nombre_serie,
            misc1.ind_nombre_detalle AS grupo_ocupacional,
            rh_c007_grado_salarial.ind_nombre_grado,
            misc2.ind_nombre_detalle AS categoria
            FROM
            rh_c063_puestos
            INNER JOIN rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
            INNER JOIN rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
            INNER JOIN a006_miscelaneo_detalle as misc1 ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_grupoocupacional = misc1.pk_num_miscelaneo_detalle
            INNER JOIN rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
            INNER JOIN rh_c010_serie ON rh_c063_puestos.fk_rhc010_num_serie = rh_c010_serie.pk_num_serie
            INNER JOIN a006_miscelaneo_detalle as misc2 ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = misc2.pk_num_miscelaneo_detalle
            WHERE  rh_c063_puestos.pk_num_puestos='$idCargo'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE REGISTRAR UN CARGO O PUESTO
    public function metRegistrarCargo($tipo,$nivel,$grupo,$serie,$desc,$descg,$categoria,$grado,$paso,$sueldo,$est)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c063_puestos
                (fk_rhc006_num_cargo,
                 fk_rhc007_num_grado,
                 fk_a006_num_miscelaneo_detalle_paso,
                 fk_a006_num_miscelaneo_detalle_grupoocupacional,
                 fk_rhc009_num_nivel,
                 fk_rhc010_num_serie,
                 fk_a006_num_miscelaneo_detalle_categoria,
                 ind_descripcion_cargo,
                 txt_descripcion_gen,
                 num_sueldo_basico,
                 num_estatus,
                 fk_a018_num_seguridad_usuario,
                 fec_ultima_modificacion)
                VALUES
                (:fk_rhc006_num_cargo,:fk_rhc007_num_grado,:fk_a006_num_miscelaneo_detalle_paso,:fk_a006_num_miscelaneo_detalle_grupoocupacional,:fk_rhc009_num_nivel,:fk_rhc010_num_serie,:fk_a006_num_miscelaneo_detalle_categoria,:ind_descripcion_cargo,:txt_descripcion_gen,:num_sueldo_basico,:num_estatus,'$this->atIdUsuario',NOW())
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_rhc006_num_cargo' => $tipo,
            ':fk_rhc009_num_nivel' => $nivel,
            ':fk_a006_num_miscelaneo_detalle_grupoocupacional' => $grupo,
            ':fk_rhc010_num_serie' => $serie,
            ':ind_descripcion_cargo' => $desc,
            ':txt_descripcion_gen' => $descg,
            ':fk_a006_num_miscelaneo_detalle_categoria' => $categoria,
            ':fk_rhc007_num_grado' => $grado,
            ':fk_a006_num_miscelaneo_detalle_paso' => $paso,
            ':num_sueldo_basico' => $sueldo,
            ':num_estatus' => $est
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    public function metModificarCargo($idCargo,$tipo,$nivel,$grupo,$serie,$desc,$descg,$categoria,$grado,$paso,$sueldo,$est)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              rh_c063_puestos
            SET
              fk_rhc006_num_cargo=:fk_rhc006_num_cargo,
              fk_rhc009_num_nivel=:fk_rhc009_num_nivel,
              fk_a006_num_miscelaneo_detalle_grupoocupacional=:fk_a006_num_miscelaneo_detalle_grupoocupacional,
              fk_rhc010_num_serie=:fk_rhc010_num_serie,
              ind_descripcion_cargo=:ind_descripcion_cargo,
              txt_descripcion_gen=:txt_descripcion_gen,
              fk_a006_num_miscelaneo_detalle_categoria=:fk_a006_num_miscelaneo_detalle_categoria,
              fk_rhc007_num_grado=:fk_rhc007_num_grado,
              fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
              num_sueldo_basico=:num_sueldo_basico,
              num_estatus=:num_estatus,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE
            pk_num_puestos='$idCargo'
            "
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_rhc006_num_cargo' => $tipo,
            ':fk_rhc009_num_nivel' => $nivel,
            ':fk_a006_num_miscelaneo_detalle_grupoocupacional' => $grupo,
            ':fk_rhc010_num_serie' => $serie,
            ':ind_descripcion_cargo' => $desc,
            ':txt_descripcion_gen' => $descg,
            ':fk_a006_num_miscelaneo_detalle_categoria' => $categoria,
            ':fk_rhc007_num_grado' => $grado,
            ':fk_a006_num_miscelaneo_detalle_paso' => $paso,
            ':num_sueldo_basico' => $sueldo,
            ':num_estatus' => $est
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCargo;
        }
    }


    public function metEliminarCargo($idCargo)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c063_puestos where pk_num_puestos ='$idCargo'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


    public function metObtenerCargoEmpleado($idPersona)
    {
        $con = $this->_db->query("
            SELECT
            rh_b001_empleado.pk_num_empleado,
            rh_b001_empleado.fk_a003_num_persona,
            rh_c063_puestos.pk_num_puestos,
            rh_c063_puestos.ind_descripcion_cargo
            FROM
            rh_b001_empleado
            INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
            WHERE rh_b001_empleado.fk_a003_num_persona='$idPersona'
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    public function metJsonCargos($grupo,$serie)
    {
        $cargo = $this->_db->query(
            "SELECT
              *
            FROM
              rh_c063_puestos
            WHERE
              fk_a006_num_miscelaneo_detalle_grupoocupacional='$grupo' and fk_rhc010_num_serie='$serie'
            ");
        $cargo->setFetchMode(PDO::FETCH_ASSOC);
        return $cargo->fetchAll();
    }


}
