<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class historialModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR HISTORIAL DEL EMPLEADO
    public function metListarHistorial($empleado)
    {
        $con = $this->_db->query("
                SELECT
                rh_c061_empleado_historial.pk_num_empleado_historial,
                rh_c061_empleado_historial.fk_rhb001_num_empleado,
                persona_empleado.pk_num_persona,
                CONCAT(persona_empleado.ind_nombre1,' ',persona_empleado.ind_nombre2,' ',persona_empleado.ind_apellido1,' ',persona_empleado.ind_apellido2) AS nombre_empleado,
                rh_c061_empleado_historial.ind_periodo,
                date_format(rh_c061_empleado_historial.fec_ingreso,'%d-%m-%Y') as fec_ingreso,
                rh_c061_empleado_historial.ind_organismo,
                rh_c061_empleado_historial.ind_dependencia,
                rh_c061_empleado_historial.ind_centro_costo,
                rh_c061_empleado_historial.ind_cargo,
                rh_c061_empleado_historial.num_nivel_salarial,
                rh_c061_empleado_historial.ind_categoria_cargo,
                rh_c061_empleado_historial.ind_tipo_nomina,
                rh_c061_empleado_historial.ind_tipo_pago,
                rh_c061_empleado_historial.num_estatus,
                rh_c061_empleado_historial.ind_motivo_cese,
                date_format(rh_c061_empleado_historial.fec_egreso,'%d-%m-%Y') as fec_egreso,
                rh_c061_empleado_historial.txt_obs_cese,
                rh_c061_empleado_historial.ind_tipo_trabajador,
                rh_c061_empleado_historial.fec_ultima_modificacion,
                rh_c061_empleado_historial.fk_a018_num_seguridad_usuario
                FROM
                rh_c061_empleado_historial
                INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c061_empleado_historial.fk_rhb001_num_empleado
                INNER JOIN a003_persona AS persona_empleado ON rh_b001_empleado.fk_a003_num_persona = persona_empleado.pk_num_persona
                WHERE fk_rhb001_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }




}//fin clase
