<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class meritosDemeritosModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR MERITOS Y DEMERITOS DEL EMPLEADO
    public function metListarMeritosDemeritos($empleado)
    {
        $con = $this->_db->query("
                SELECT
                rh_c008_meritos_demeritos.pk_num_meritos_demeritos,
                rh_c008_meritos_demeritos.fk_rhb001_num_empleado,
                tipo_merdem.ind_nombre_detalle AS tipo_merdem,
                merdem.ind_nombre_detalle AS merdem,
                rh_c008_meritos_demeritos.ind_documento,
                rh_c008_meritos_demeritos.fec_documento,
                rh_c008_meritos_demeritos.fk_a006_num_miscelaneo_detalle_meritodemerito,
                rh_c008_meritos_demeritos.fk_a006_num_miscelaneo_detalle_tipo,
                rh_c008_meritos_demeritos.fk_rhb001_num_empleado_responsable,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c008_meritos_demeritos.ind_persona_externa,
                rh_c008_meritos_demeritos.num_flag_externo,
                rh_c008_meritos_demeritos.txt_observaciones,
                rh_c008_meritos_demeritos.fec_ultima_modificacion,
                rh_c008_meritos_demeritos.fk_a018_num_seguridad_usuario
                FROM
                rh_c008_meritos_demeritos
                INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = rh_c008_meritos_demeritos.fk_rhb001_num_empleado
                LEFT JOIN rh_b001_empleado AS responsable ON responsable.pk_num_empleado = rh_c008_meritos_demeritos.fk_rhb001_num_empleado_responsable
                INNER JOIN a006_miscelaneo_detalle AS merdem ON merdem.pk_num_miscelaneo_detalle = rh_c008_meritos_demeritos.fk_a006_num_miscelaneo_detalle_meritodemerito
                INNER JOIN a006_miscelaneo_detalle AS tipo_merdem ON tipo_merdem.pk_num_miscelaneo_detalle = rh_c008_meritos_demeritos.fk_a006_num_miscelaneo_detalle_tipo
                LEFT JOIN a003_persona ON a003_persona.pk_num_persona = responsable.fk_a003_num_persona
                WHERE fk_rhb001_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE MOSTRAR LOS MERITOS Y DEMERITOS EN EL FORMULARIO
    public function metMostrarMeritosDemeritos($idMeritosDemeritos)
    {

        $con = $this->_db->query("
                SELECT
                rh_c008_meritos_demeritos.pk_num_meritos_demeritos,
                rh_c008_meritos_demeritos.fk_rhb001_num_empleado,
                tipo_merdem.ind_nombre_detalle AS tipo_merdem,
                merdem.ind_nombre_detalle AS merdem,
                rh_c008_meritos_demeritos.ind_tipo,
                rh_c008_meritos_demeritos.ind_documento,
                rh_c008_meritos_demeritos.fec_documento,
                rh_c008_meritos_demeritos.fk_a006_num_miscelaneo_detalle_meritodemerito,
                rh_c008_meritos_demeritos.fk_a006_num_miscelaneo_detalle_tipo,
                rh_c008_meritos_demeritos.fk_rhb001_num_empleado_responsable,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c008_meritos_demeritos.ind_persona_externa,
                rh_c008_meritos_demeritos.num_flag_externo,
                rh_c008_meritos_demeritos.txt_observaciones,
                rh_c008_meritos_demeritos.fec_ultima_modificacion,
                rh_c008_meritos_demeritos.fk_a018_num_seguridad_usuario
                FROM
                rh_c008_meritos_demeritos
                INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = rh_c008_meritos_demeritos.fk_rhb001_num_empleado
                LEFT JOIN rh_b001_empleado AS responsable ON responsable.pk_num_empleado = rh_c008_meritos_demeritos.fk_rhb001_num_empleado_responsable
                INNER JOIN a006_miscelaneo_detalle AS merdem ON merdem.pk_num_miscelaneo_detalle = rh_c008_meritos_demeritos.fk_a006_num_miscelaneo_detalle_meritodemerito
                INNER JOIN a006_miscelaneo_detalle AS tipo_merdem ON tipo_merdem.pk_num_miscelaneo_detalle = rh_c008_meritos_demeritos.fk_a006_num_miscelaneo_detalle_tipo
                LEFT JOIN a003_persona ON a003_persona.pk_num_persona = responsable.fk_a003_num_persona
                WHERE pk_num_meritos_demeritos='$idMeritosDemeritos'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #PERMITE INSERTAR MERITOS Y DEMERITOS DEL EMPLEADO
    public function metRegistrarMeritosDemeritos($empleado,$tipo,$merdem,$ind_tipo,$doc,$fecha,$responsable,$p_ext,$flag_externo,$observaciones)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

            $fc = explode("-",$fecha);

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c008_meritos_demeritos
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_a006_num_miscelaneo_detalle_meritodemerito=:fk_a006_num_miscelaneo_detalle_meritodemerito,
                    ind_tipo=:ind_tipo,
                    ind_documento=:ind_documento,
                    fec_documento=:fec_documento,
                    fk_a006_num_miscelaneo_detalle_tipo=:fk_a006_num_miscelaneo_detalle_tipo,
                    fk_rhb001_num_empleado_responsable=:fk_rhb001_num_empleado_responsable,
                    ind_persona_externa=:ind_persona_externa,
                    num_flag_externo=:num_flag_externo,
                    txt_observaciones=:txt_observaciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_meritodemerito'  => $merdem,
                ':ind_tipo'  => $ind_tipo,
                ':ind_documento'  => $doc,
                ':fec_documento' => $fc[2]."-".$fc[1]."-".$fc[0],
                ':fk_a006_num_miscelaneo_detalle_tipo' => $tipo,
                ':fk_rhb001_num_empleado_responsable' => $responsable,
                ':ind_persona_externa' => $p_ext,
                ':num_flag_externo' => $flag_externo,
                ':txt_observaciones' => $observaciones
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                $this->_db->commit();
                return $idRegistro;

            }


    }

    #PERMITE MODIFICAR MERITOS Y DEMERITOS DEL EMPLEADO
    public function metModificarMeritosDemeritos($idMeritosDemeritos,$tipo,$merdem,$ind_tipo,$doc,$fecha,$responsable,$p_ext,$flag_externo,$observaciones)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        $fc = explode("-",$fecha);

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                rh_c008_meritos_demeritos
             SET
                    fk_a006_num_miscelaneo_detalle_meritodemerito=:fk_a006_num_miscelaneo_detalle_meritodemerito,
                    ind_tipo=:ind_tipo,
                    ind_documento=:ind_documento,
                    fec_documento=:fec_documento,
                    fk_a006_num_miscelaneo_detalle_tipo=:fk_a006_num_miscelaneo_detalle_tipo,
                    fk_rhb001_num_empleado_responsable=:fk_rhb001_num_empleado_responsable,
                    ind_persona_externa=:ind_persona_externa,
                    num_flag_externo=:num_flag_externo,
                    txt_observaciones=:txt_observaciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_meritos_demeritos='$idMeritosDemeritos'
              ");

        #execute — Ejecuta una sentencia preparada
        $ActualizarRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_meritodemerito'  => $merdem,
            ':ind_tipo'  => $ind_tipo,
            ':ind_documento'  => $doc,
            ':fec_documento' => $fc[2]."-".$fc[1]."-".$fc[0],
            ':fk_a006_num_miscelaneo_detalle_tipo' => $tipo,
            ':fk_rhb001_num_empleado_responsable' => $responsable,
            ':ind_persona_externa' => $p_ext,
            ':num_flag_externo' => $flag_externo,
            ':txt_observaciones' => $observaciones
        ));

        $error= $ActualizarRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $error;

        }else{
            $this->_db->commit();
            return $idMeritosDemeritos;
        }

    }

    #PERMITE ELIMINAR MERITOS Y DEMERITOS DEL EMPLEADO
    public function metEliminarMeritosDemeritos($idMeritosDemeritos)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c008_meritos_demeritos WHERE pk_num_meritos_demeritos=:pk_num_meritos_demeritos
            ");
        $eliminar->execute(array(
            'pk_num_meritos_demeritos'=>$idMeritosDemeritos
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idMeritosDemeritos;
        }

    }




}//fin clase
