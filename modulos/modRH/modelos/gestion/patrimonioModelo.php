<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class patrimonioModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR PATRIMONIOS DEL EMPLEADO
    public function metListarPatrimonio($empleado,$tipo)
    {

        //VERIFICO EL TIPO DE PATROMINOY LISTO SEGUN EL TIPO
        if(strcmp($tipo,'INMUEBLE')==0){
            $inmueble = $this->_db->query("
                SELECT
                rh_c020_patrimonio_inmueble.pk_num_patrimonio_inmueble,
                a003_persona.pk_num_persona,
                rh_c020_patrimonio_inmueble.fk_rhb001_num_empleado,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c020_patrimonio_inmueble.ind_descripcion,
                rh_c020_patrimonio_inmueble.ind_ubicacion,
                rh_c020_patrimonio_inmueble.ind_uso,
                rh_c020_patrimonio_inmueble.num_valor,
                rh_c020_patrimonio_inmueble.num_flag_hipotecado,
                rh_c020_patrimonio_inmueble.fec_ultima_modificacion,
                rh_c020_patrimonio_inmueble.fk_a018_num_seguridad_usuario
                FROM
                rh_c020_patrimonio_inmueble
                INNER JOIN rh_b001_empleado ON rh_c020_patrimonio_inmueble.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $inmueble->setFetchMode(PDO::FETCH_ASSOC);
            return $inmueble->fetchAll();
        }
        if(strcmp($tipo,'INVERSION')==0){
            $inversion = $this->_db->query("
                SELECT
                rh_c023_patrimonio_inversion.pk_num_patrimonio_inversion,
                rh_c023_patrimonio_inversion.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c023_patrimonio_inversion.ind_titular,
                rh_c023_patrimonio_inversion.ind_remitente,
                rh_c023_patrimonio_inversion.ind_certificado,
                rh_c023_patrimonio_inversion.num_cant,
                rh_c023_patrimonio_inversion.num_valor_nominal,
                rh_c023_patrimonio_inversion.num_valor,
                rh_c023_patrimonio_inversion.num_flag_garantia,
                rh_c023_patrimonio_inversion.fec_ultima_modificacion
                FROM
                rh_c023_patrimonio_inversion
                INNER JOIN rh_b001_empleado ON rh_c023_patrimonio_inversion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $inversion->setFetchMode(PDO::FETCH_ASSOC);
            return $inversion->fetchAll();
        }
        if(strcmp($tipo,'VEHICULO')==0){
            $vehiculo = $this->_db->query("
                SELECT
                rh_c021_patrimonio_vehiculo.pk_num_patrimonio_vehiculo,
                rh_c021_patrimonio_vehiculo.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c021_patrimonio_vehiculo.fk_a006_num_miscelaneo_detalle_marca,
                marca_vehiculo.ind_nombre_detalle AS marca_vehiculo,
                rh_c021_patrimonio_vehiculo.ind_modelo,
                rh_c021_patrimonio_vehiculo.fk_a006_num_miscelaneo_detalle_color,
                color_vehiculo.ind_nombre_detalle AS color_vehiculo,
                rh_c021_patrimonio_vehiculo.ind_placa,
                rh_c021_patrimonio_vehiculo.fec_anio,
                rh_c021_patrimonio_vehiculo.num_valor,
                rh_c021_patrimonio_vehiculo.fec_ultima_modificacion,
                rh_c021_patrimonio_vehiculo.fk_a018_num_seguridad_usuario
                FROM
                rh_c021_patrimonio_vehiculo
                INNER JOIN rh_b001_empleado ON rh_c021_patrimonio_vehiculo.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS marca_vehiculo ON marca_vehiculo.pk_num_miscelaneo_detalle = rh_c021_patrimonio_vehiculo.fk_a006_num_miscelaneo_detalle_marca
                INNER JOIN a006_miscelaneo_detalle AS color_vehiculo ON color_vehiculo.pk_num_miscelaneo_detalle = rh_c021_patrimonio_vehiculo.fk_a006_num_miscelaneo_detalle_color
                WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $vehiculo->setFetchMode(PDO::FETCH_ASSOC);
            return $vehiculo->fetchAll();
        }
        if(strcmp($tipo,'CUENTAS')==0){
            $cuentas = $this->_db->query("
                SELECT
                rh_c025_patrimonio_cuenta.pk_num_patrimonio_cuenta,
                rh_c025_patrimonio_cuenta.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c025_patrimonio_cuenta.fk_a006_num_miscelaneo_detalle_tipocta,
                a006_miscelaneo_detalle.ind_nombre_detalle AS tipocta,
                rh_c025_patrimonio_cuenta.ind_banco,
                rh_c025_patrimonio_cuenta.ind_cuenta,
                rh_c025_patrimonio_cuenta.num_monto,
                rh_c025_patrimonio_cuenta.fec_ultima_modificacion,
                rh_c025_patrimonio_cuenta.fk_a018_num_seguridad_usuario
                FROM
                rh_c025_patrimonio_cuenta
                INNER JOIN rh_b001_empleado ON rh_c025_patrimonio_cuenta.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle ON rh_c025_patrimonio_cuenta.fk_a006_num_miscelaneo_detalle_tipocta = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $cuentas->setFetchMode(PDO::FETCH_ASSOC);
            return $cuentas->fetchAll();
        }
        if(strcmp($tipo,'OTROS')==0){
            $otros = $this->_db->query("
                SELECT
                rh_c026_patrimonio_otros.pk_num_patrimonio_otros,
                rh_c026_patrimonio_otros.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c026_patrimonio_otros.ind_descripcion,
                rh_c026_patrimonio_otros.num_valor_compra,
                rh_c026_patrimonio_otros.num_valor_actual,
                rh_c026_patrimonio_otros.fec_ultima_modificacion,
                rh_c026_patrimonio_otros.fk_a018_num_seguridad_usuario
                FROM
                rh_c026_patrimonio_otros
                INNER JOIN rh_b001_empleado ON rh_c026_patrimonio_otros.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $otros->setFetchMode(PDO::FETCH_ASSOC);
            return $otros->fetchAll();
        }

    }

    public function metConsultarTotalesPatrimonios($empleado)
    {

        $inmueble = $this->_db->query("
              SELECT SUM(num_valor) AS Total
         		FROM rh_c020_patrimonio_inmueble
         		WHERE fk_rhb001_num_empleado = '$empleado'
     			GROUP BY fk_rhb001_num_empleado
        ");
        $inmueble->setFetchMode(PDO::FETCH_ASSOC);
        $inm = $inmueble->fetch();

        $inversion = $this->_db->query("
                SELECT SUM(num_valor) AS Total
                    FROM rh_c023_patrimonio_inversion
                    WHERE fk_rhb001_num_empleado = '$empleado'
                    GROUP BY fk_rhb001_num_empleado
        ");
        $inversion->setFetchMode(PDO::FETCH_ASSOC);
        $in = $inversion->fetch();

        $vehiculo = $this->_db->query("
                SELECT SUM(num_valor) AS Total
                    FROM rh_c021_patrimonio_vehiculo
                    WHERE fk_rhb001_num_empleado = '$empleado'
                    GROUP BY fk_rhb001_num_empleado
        ");
        $vehiculo->setFetchMode(PDO::FETCH_ASSOC);
        $ve = $vehiculo->fetch();

        $cuenta = $this->_db->query("
                SELECT SUM(num_monto) AS Total
                    FROM rh_c025_patrimonio_cuenta
                    WHERE fk_rhb001_num_empleado = '$empleado'
                    GROUP BY fk_rhb001_num_empleado
        ");
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        $cu = $cuenta->fetch();

        $otros = $this->_db->query("
               SELECT SUM(num_valor_actual) AS Total
  			   FROM rh_c026_patrimonio_otros
               WHERE fk_rhb001_num_empleado = '$empleado'
               GROUP BY fk_rhb001_num_empleado
        ");
        $otros->setFetchMode(PDO::FETCH_ASSOC);
        $ot = $otros->fetch();


        $TOTAL = $inm['Total'] + $in['Total'] + $ve['Total'] + $cu['Total'] + $ot['Total'];

        return array($inm['Total'],$in['Total'],$ve['Total'],$cu['Total'],$ot['Total'],$TOTAL);


    }


    #PERMITE MOSTRAR EL PATRIMONIO DEL EMPLEADO EN EL FORMULARIO
    public function metMostrarPatrimonio($idPatrimonio,$tipo)
    {

        if(strcmp($tipo,'INMUEBLE')==0){
            $inmueble = $this->_db->query("
                SELECT
                rh_c020_patrimonio_inmueble.pk_num_patrimonio_inmueble,
                a003_persona.pk_num_persona,
                rh_c020_patrimonio_inmueble.fk_rhb001_num_empleado,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c020_patrimonio_inmueble.ind_descripcion,
                rh_c020_patrimonio_inmueble.ind_ubicacion,
                rh_c020_patrimonio_inmueble.ind_uso,
                rh_c020_patrimonio_inmueble.num_valor,
                rh_c020_patrimonio_inmueble.num_flag_hipotecado,
                rh_c020_patrimonio_inmueble.fec_ultima_modificacion,
                rh_c020_patrimonio_inmueble.fk_a018_num_seguridad_usuario
                FROM
                rh_c020_patrimonio_inmueble
                INNER JOIN rh_b001_empleado ON rh_c020_patrimonio_inmueble.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE pk_num_patrimonio_inmueble='$idPatrimonio'
            ");
            $inmueble->setFetchMode(PDO::FETCH_ASSOC);
            return $inmueble->fetch();
        }

        if(strcmp($tipo,'INVERSION')==0){
            $inversion = $this->_db->query("
                SELECT
                rh_c023_patrimonio_inversion.pk_num_patrimonio_inversion,
                rh_c023_patrimonio_inversion.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c023_patrimonio_inversion.ind_titular,
                rh_c023_patrimonio_inversion.ind_remitente,
                rh_c023_patrimonio_inversion.ind_certificado,
                rh_c023_patrimonio_inversion.num_cant,
                rh_c023_patrimonio_inversion.num_valor_nominal,
                rh_c023_patrimonio_inversion.num_valor,
                rh_c023_patrimonio_inversion.num_flag_garantia,
                rh_c023_patrimonio_inversion.fec_ultima_modificacion,
                rh_c023_patrimonio_inversion.fk_a018_num_seguridad_usuario
                FROM
                rh_c023_patrimonio_inversion
                INNER JOIN rh_b001_empleado ON rh_c023_patrimonio_inversion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE pk_num_patrimonio_inversion='$idPatrimonio'
            ");
            $inversion->setFetchMode(PDO::FETCH_ASSOC);
            return $inversion->fetch();
        }

        if(strcmp($tipo,'VEHICULO')==0){

            $vehiculo = $this->_db->query("
                SELECT
                rh_c021_patrimonio_vehiculo.pk_num_patrimonio_vehiculo,
                rh_c021_patrimonio_vehiculo.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c021_patrimonio_vehiculo.fk_a006_num_miscelaneo_detalle_marca,
                marca_vehiculo.ind_nombre_detalle AS marca_vehiculo,
                rh_c021_patrimonio_vehiculo.ind_modelo,
                rh_c021_patrimonio_vehiculo.fk_a006_num_miscelaneo_detalle_color,
                color_vehiculo.ind_nombre_detalle AS color_vehiculo,
                rh_c021_patrimonio_vehiculo.ind_placa,
                rh_c021_patrimonio_vehiculo.fec_anio,
                rh_c021_patrimonio_vehiculo.num_valor,
                rh_c021_patrimonio_vehiculo.fec_ultima_modificacion,
                rh_c021_patrimonio_vehiculo.fk_a018_num_seguridad_usuario
                FROM
                rh_c021_patrimonio_vehiculo
                INNER JOIN rh_b001_empleado ON rh_c021_patrimonio_vehiculo.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS marca_vehiculo ON marca_vehiculo.pk_num_miscelaneo_detalle = rh_c021_patrimonio_vehiculo.fk_a006_num_miscelaneo_detalle_marca
                INNER JOIN a006_miscelaneo_detalle AS color_vehiculo ON color_vehiculo.pk_num_miscelaneo_detalle = rh_c021_patrimonio_vehiculo.fk_a006_num_miscelaneo_detalle_color
                WHERE pk_num_patrimonio_vehiculo='$idPatrimonio'
            ");
            $vehiculo->setFetchMode(PDO::FETCH_ASSOC);
            return $vehiculo->fetch();

        }
        if(strcmp($tipo,'CUENTAS')==0){

            $cuentas = $this->_db->query("
                SELECT
                rh_c025_patrimonio_cuenta.pk_num_patrimonio_cuenta,
                rh_c025_patrimonio_cuenta.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c025_patrimonio_cuenta.fk_a006_num_miscelaneo_detalle_tipocta,
                a006_miscelaneo_detalle.ind_nombre_detalle AS tipocta,
                rh_c025_patrimonio_cuenta.ind_banco,
                rh_c025_patrimonio_cuenta.ind_cuenta,
                rh_c025_patrimonio_cuenta.num_monto,
                rh_c025_patrimonio_cuenta.fec_ultima_modificacion,
                rh_c025_patrimonio_cuenta.fk_a018_num_seguridad_usuario
                FROM
                rh_c025_patrimonio_cuenta
                INNER JOIN rh_b001_empleado ON rh_c025_patrimonio_cuenta.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle ON rh_c025_patrimonio_cuenta.fk_a006_num_miscelaneo_detalle_tipocta = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                WHERE pk_num_patrimonio_cuenta='$idPatrimonio'
            ");
            $cuentas->setFetchMode(PDO::FETCH_ASSOC);
            return $cuentas->fetch();

        }
        if(strcmp($tipo,'OTROS')==0){

            $otros = $this->_db->query("
                SELECT
                rh_c026_patrimonio_otros.pk_num_patrimonio_otros,
                rh_c026_patrimonio_otros.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c026_patrimonio_otros.ind_descripcion,
                rh_c026_patrimonio_otros.num_valor_compra,
                rh_c026_patrimonio_otros.num_valor_actual,
                rh_c026_patrimonio_otros.fec_ultima_modificacion,
                rh_c026_patrimonio_otros.fk_a018_num_seguridad_usuario
                FROM
                rh_c026_patrimonio_otros
                INNER JOIN rh_b001_empleado ON rh_c026_patrimonio_otros.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE pk_num_patrimonio_otros='$idPatrimonio'
            ");
            $otros->setFetchMode(PDO::FETCH_ASSOC);
            return $otros->fetch();

        }

    }

    #PERMITE REGISTRAR PATRIMONIOS DEL EMPLEADO
    public function metRegistrarPatrimonio($empleado,$tipo,$datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        if(strcmp($tipo,'INMUEBLE')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c020_patrimonio_inmueble
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    ind_descripcion=:ind_descripcion,
                    ind_ubicacion=:ind_ubicacion,
                    ind_uso=:ind_uso,
                    num_valor=:num_valor,
                    num_flag_hipotecado=:num_flag_hipotecado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':ind_descripcion'  => $datos[0],
                ':ind_ubicacion'  => $datos[1],
                ':ind_uso'  => $datos[2],
                ':num_valor' => $datos[3],
                ':num_flag_hipotecado' => $datos[4]
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }

        }
        if(strcmp($tipo,'INVERSION')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c023_patrimonio_inversion
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    ind_titular=:ind_titular,
                    ind_remitente=:ind_remitente,
                    ind_certificado=:ind_certificado,
                    num_cant=:num_cant,
                    num_valor_nominal=:num_valor_nominal,
                    num_valor=:num_valor,
                    num_flag_garantia=:num_flag_garantia,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':ind_titular'  => $datos[0],
                ':ind_remitente'  => $datos[1],
                ':ind_certificado'  => $datos[2],
                ':num_cant'  => $datos[3],
                ':num_valor_nominal'  => $datos[4],
                ':num_valor' => $datos[5],
                ':num_flag_garantia' => $datos[6]
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }

        }
        if(strcmp($tipo,'VEHICULO')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c021_patrimonio_vehiculo
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_a006_num_miscelaneo_detalle_marca=:fk_a006_num_miscelaneo_detalle_marca,
                    ind_modelo=:ind_modelo,
                    fk_a006_num_miscelaneo_detalle_color=:fk_a006_num_miscelaneo_detalle_color,
                    ind_placa=:ind_placa,
                    fec_anio=:fec_anio,
                    num_valor=:num_valor,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_marca'  => $datos[0],
                ':ind_modelo'  => $datos[1],
                ':fk_a006_num_miscelaneo_detalle_color'  => $datos[2],
                ':ind_placa'  => $datos[3],
                ':fec_anio'  => $datos[4],
                ':num_valor' => $datos[5]
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }

        }
        if(strcmp($tipo,'CUENTAS')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c025_patrimonio_cuenta
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_a006_num_miscelaneo_detalle_tipocta=:fk_a006_num_miscelaneo_detalle_tipocta,
                    ind_banco=:ind_banco,
                    ind_cuenta=:ind_cuenta,
                    num_monto=:num_monto,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_tipocta'  => $datos[0],
                ':ind_banco'  => $datos[1],
                ':ind_cuenta'  => $datos[2],
                ':num_monto'  => $datos[3]
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }

        }
        if(strcmp($tipo,'OTROS')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c026_patrimonio_otros
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    ind_descripcion=:ind_descripcion,
                    num_valor_compra=:num_valor_compra,
                    num_valor_actual=:num_valor_actual,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':ind_descripcion'  => $datos[0],
                ':num_valor_compra'  => $datos[1],
                ':num_valor_actual'  => $datos[2]
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }

        }

    }

    #PERMITE MODIFICAR EL PATRIMONIO DEL EMPLEADO
    public function metModificarPatrimonio($idPatrimonio,$tipo,$datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        if(strcmp($tipo,'INMUEBLE')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $ActualizarRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c020_patrimonio_inmueble
                 SET
                    ind_descripcion=:ind_descripcion,
                    ind_ubicacion=:ind_ubicacion,
                    ind_uso=:ind_uso,
                    num_valor=:num_valor,
                    num_flag_hipotecado=:num_flag_hipotecado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_patrimonio_inmueble='$idPatrimonio'
              ");
            #execute — Ejecuta una sentencia preparada
            $ActualizarRegistro->execute(array(
                ':ind_descripcion'  => $datos[0],
                ':ind_ubicacion'  => $datos[1],
                ':ind_uso'  => $datos[2],
                ':num_valor' => $datos[3],
                ':num_flag_hipotecado' => $datos[4]
            ));

            $error= $ActualizarRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }

        if(strcmp($tipo,'INVERSION')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $ActualizarRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c023_patrimonio_inversion
                 SET
                    ind_titular=:ind_titular,
                    ind_remitente=:ind_remitente,
                    ind_certificado=:ind_certificado,
                    num_cant=:num_cant,
                    num_valor_nominal=:num_valor_nominal,
                    num_valor=:num_valor,
                    num_flag_garantia=:num_flag_garantia,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_patrimonio_inversion='$idPatrimonio'
              ");
            #execute — Ejecuta una sentencia preparada
            $ActualizarRegistro->execute(array(
                ':ind_titular'  => $datos[0],
                ':ind_remitente'  => $datos[1],
                ':ind_certificado'  => $datos[2],
                ':num_cant'  => $datos[3],
                ':num_valor_nominal'  => $datos[4],
                ':num_valor' => $datos[5],
                ':num_flag_garantia' => $datos[6]
            ));

            $error= $ActualizarRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }
        if(strcmp($tipo,'VEHICULO')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $ActualizarRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c021_patrimonio_vehiculo
                 SET
                    fk_a006_num_miscelaneo_detalle_marca=:fk_a006_num_miscelaneo_detalle_marca,
                    ind_modelo=:ind_modelo,
                    fk_a006_num_miscelaneo_detalle_color=:fk_a006_num_miscelaneo_detalle_color,
                    ind_placa=:ind_placa,
                    fec_anio=:fec_anio,
                    num_valor=:num_valor,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_patrimonio_vehiculo='$idPatrimonio'
              ");
            #execute — Ejecuta una sentencia preparada
            $ActualizarRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_marca'  => $datos[0],
                ':ind_modelo'  => $datos[1],
                ':fk_a006_num_miscelaneo_detalle_color'  => $datos[2],
                ':ind_placa'  => $datos[3],
                ':fec_anio'  => $datos[4],
                ':num_valor' => $datos[5]
            ));

            $error= $ActualizarRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }
        if(strcmp($tipo,'CUENTAS')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $ActualizarRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c025_patrimonio_cuenta
                 SET
                    fk_a006_num_miscelaneo_detalle_tipocta=:fk_a006_num_miscelaneo_detalle_tipocta,
                    ind_banco=:ind_banco,
                    ind_cuenta=:ind_cuenta,
                    num_monto=:num_monto,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_patrimonio_cuenta='$idPatrimonio'
              ");
            #execute — Ejecuta una sentencia preparada
            $ActualizarRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_tipocta'  => $datos[0],
                ':ind_banco'  => $datos[1],
                ':ind_cuenta'  => $datos[2],
                ':num_monto'  => $datos[3]
            ));

            $error= $ActualizarRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }
        if(strcmp($tipo,'OTROS')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $ActualizarRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c026_patrimonio_otros
                 SET
                    ind_descripcion=:ind_descripcion,
                    num_valor_compra=:num_valor_compra,
                    num_valor_actual=:num_valor_actual,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_patrimonio_otros='$idPatrimonio'
              ");
            #execute — Ejecuta una sentencia preparada
            $ActualizarRegistro->execute(array(
                ':ind_descripcion'  => $datos[0],
                ':num_valor_compra'  => $datos[1],
                ':num_valor_actual'  => $datos[2]
            ));

            $error= $ActualizarRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }

    }

    #PERMITE ELIMINAR MERITOS Y DEMERITOS DEL EMPLEADO
    public function metEliminarPatrimonio($idPatrimonio,$tipo)
    {

        $this->_db->beginTransaction();

        if(strcmp($tipo,'INMUEBLE')==0){

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c020_patrimonio_inmueble WHERE pk_num_patrimonio_inmueble=:pk_num_patrimonio_inmueble
            ");
            $eliminar->execute(array(
                'pk_num_patrimonio_inmueble'=>$idPatrimonio
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }
        if(strcmp($tipo,'INVERSION')==0){

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c023_patrimonio_inversion WHERE pk_num_patrimonio_inversion=:pk_num_patrimonio_inversion
            ");
            $eliminar->execute(array(
                'pk_num_patrimonio_inversion'=>$idPatrimonio
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }
        if(strcmp($tipo,'VEHICULO')==0){

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c021_patrimonio_vehiculo WHERE pk_num_patrimonio_vehiculo=:pk_num_patrimonio_vehiculo
            ");
            $eliminar->execute(array(
                'pk_num_patrimonio_vehiculo'=>$idPatrimonio
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }
        if(strcmp($tipo,'CUENTAS')==0){

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c025_patrimonio_cuenta WHERE pk_num_patrimonio_cuenta=:pk_num_patrimonio_cuenta
            ");
            $eliminar->execute(array(
                'pk_num_patrimonio_cuenta'=>$idPatrimonio
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }
        if(strcmp($tipo,'OTROS')==0){

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c026_patrimonio_otros WHERE pk_num_patrimonio_otros=:pk_num_patrimonio_otros
            ");
            $eliminar->execute(array(
                'pk_num_patrimonio_otros'=>$idPatrimonio
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPatrimonio;
            }

        }


    }




}//fin clase
