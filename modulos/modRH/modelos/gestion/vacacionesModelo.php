<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Gestión de Vacaciones. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php'; 
class vacacionesModelo extends miscelaneoModelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado, b.num_estatus from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener la dependencia, fecha de ingreso y el organismo al que pertenece el empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado =  $this->_db->query(
            "SELECT
                          a.pk_num_empleado,
                          c.pk_num_organismo,
                          c.ind_descripcion_empresa,
                          d.pk_num_dependencia,
                          d.ind_dependencia,
                          e.ind_cedula_documento,
                          e.ind_nombre1,
                          e.ind_nombre2,
                          e.ind_apellido1,
                          e.ind_apellido2,
                          e.ind_cedula_documento,
                          f.fec_ingreso,
                          f.fk_rhc063_num_puestos_cargo,
                          g.ind_descripcion_cargo
                        FROM
                          rh_b001_empleado AS a,
                          rh_c076_empleado_organizacion AS b,
                          a001_organismo AS c,
                          a004_dependencia AS d,
                          a003_persona AS e,
                          rh_c005_empleado_laboral AS f,
                          rh_c063_puestos AS g,
                          rh_c059_empleado_nivelacion AS h
                        WHERE
                          a.pk_num_empleado = '$pkNumEmpleado' 
                          AND a.pk_num_empleado = b.fk_rhb001_num_empleado 
                          AND b.fk_a001_num_organismo = c.pk_num_organismo 
                          AND h.fk_a004_num_dependencia = d.pk_num_dependencia
                          AND a.fk_a003_num_persona = e.pk_num_persona 
                          AND f.fk_rhb001_num_empleado = a.pk_num_empleado 
                          AND f.fk_rhc063_num_puestos_cargo = g.pk_num_puestos
                          AND a.pk_num_empleado = h.fk_rhb001_num_empleado
                          ORDER BY h.pk_num_empleado_nivelacion DESC
                          LIMIT 1
                          "
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    // Método que permite listar los periodos de un empleado
    public function metListarPeriodo($pkNumEmpleado, $metodo)
    {
        switch($metodo){
            case 1:
                $periodo =  $this->_db->query(
                    "select a.pk_num_periodo, a.fk_rhb001_num_empleado, a.num_anio, a.num_mes, a.num_dias_derecho, a.num_dias_gozados, a.	num_dias_interrumpidos, a.num_total_utilizados, a.num_pendientes, a.num_estatus, (select SUM(num_dias_utiles) as dias_utiles from rh_c082_vacacion_utilizacion where fk_rhc081_num_periodo=a.pk_num_periodo) as total_dias from rh_c081_periodo_vacacion as a where fk_rhb001_num_empleado=$pkNumEmpleado order by a.num_anio desc"
                );
                break;
            case 2:
                $periodo =  $this->_db->query(
                    "select  a.pk_num_periodo, a.fk_rhb001_num_empleado, a.num_anio, a.num_mes, a.num_dias_derecho, a.num_dias_gozados, a.	num_dias_interrumpidos, a.num_total_utilizados, a.num_pendientes, a.num_estatus, (select SUM(num_dias_utiles) as dias_utiles from rh_c082_vacacion_utilizacion where fk_rhc081_num_periodo=a.pk_num_periodo) as total_dias from rh_c081_periodo_vacacion as a where fk_rhb001_num_empleado=$pkNumEmpleado and num_estatus=1 order by a.num_anio desc"
                );
                break;
            case 3:
                $periodo =  $this->_db->query(
                    "select  a.pk_num_periodo, a.fk_rhb001_num_empleado, a.num_anio, a.num_mes, a.num_dias_derecho, a.num_dias_gozados, a.	num_dias_interrumpidos, a.num_total_utilizados, a.num_pendientes, a.num_estatus, (select SUM(num_dias_utiles) as dias_utiles from rh_c082_vacacion_utilizacion where fk_rhc081_num_periodo=a.pk_num_periodo) as total_dias from rh_c081_periodo_vacacion as a where fk_rhb001_num_empleado=$pkNumEmpleado order by a.num_anio desc"
                );
                break;

            case 4:
                $periodo =  $this->_db->query(
                    "select a.pk_num_periodo, a.fk_rhb001_num_empleado, a.num_anio, a.num_mes, a.num_dias_derecho, a.num_dias_gozados, a.	num_dias_interrumpidos, a.num_total_utilizados, a.num_pendientes, a.num_estatus, (select SUM(num_dias_utiles) as dias_utiles from rh_c082_vacacion_utilizacion where fk_rhc081_num_periodo=a.pk_num_periodo) as total_dias from rh_c081_periodo_vacacion as a where fk_rhb001_num_empleado=$pkNumEmpleado order by a.num_anio desc"
                );
                break;

            case 5:
                $periodo =  $this->_db->query(
                    "select  a.pk_num_periodo, a.fk_rhb001_num_empleado, a.num_anio, a.num_mes, a.num_dias_derecho, a.num_dias_gozados, a.	num_dias_interrumpidos, a.num_total_utilizados, a.num_pendientes, a.num_estatus, (select SUM(num_dias_utiles) as dias_utiles from rh_c082_vacacion_utilizacion where fk_rhc081_num_periodo=a.pk_num_periodo) as total_dias from rh_c081_periodo_vacacion as a where fk_rhb001_num_empleado=$pkNumEmpleado order by a.num_anio asc"
                );
                break;
            case 6:
                $periodo =  $this->_db->query(
                    "select  a.pk_num_periodo, a.fk_rhb001_num_empleado, a.num_anio, a.num_mes, a.num_dias_derecho, a.num_dias_gozados, a.	num_dias_interrumpidos, a.num_total_utilizados, a.num_pendientes, a.num_estatus, (select SUM(num_dias_utiles) as dias_utiles from rh_c082_vacacion_utilizacion where fk_rhc081_num_periodo=a.pk_num_periodo) as total_dias from rh_c081_periodo_vacacion as a where fk_rhb001_num_empleado=$pkNumEmpleado and num_estatus=1 order by a.num_anio asc"
                );
                break;
            case 7:
                $periodo =  $this->_db->query(
                    "SELECT
                              a.pk_num_periodo,
                              a.fk_rhb001_num_empleado,
                              a.num_anio,
                              a.num_mes,
                              a.num_dias_derecho,
                              a.num_dias_gozados,
                              a.num_dias_interrumpidos,
                              a.num_total_utilizados,
                              a.num_pendientes,
                              a.num_estatus,
                              (
                              SELECT
                                SUM(u.num_dias_utiles) AS dias_utiles
                              FROM
                                rh_c082_vacacion_utilizacion AS u,
                                a006_miscelaneo_detalle AS d
                              WHERE
                                u.fk_rhc081_num_periodo = a.pk_num_periodo AND
                                d.pk_num_miscelaneo_detalle = u.fk_a006_tipo_vacacion AND
                                d.cod_detalle = 1
                            ) AS total_dias_goce,
                              (
                              SELECT
                                SUM(u.num_dias_utiles) AS dias_utiles
                              FROM
                                rh_c082_vacacion_utilizacion AS u,
                                a006_miscelaneo_detalle AS d
                              WHERE
                                u.fk_rhc081_num_periodo = a.pk_num_periodo AND
                                d.pk_num_miscelaneo_detalle = u.fk_a006_tipo_vacacion AND
                                d.cod_detalle = 2
                            ) AS total_dias_interrupcion
                            FROM
                              rh_c081_periodo_vacacion AS a
                            WHERE
                              fk_rhb001_num_empleado = $pkNumEmpleado
                            ORDER BY
                              a.num_anio DESC"
                );
                break;
                case 8:
                $periodo =  $this->_db->query(
                    "SELECT
                              a.pk_num_periodo,
                              a.fk_rhb001_num_empleado,
                              a.num_anio,
                              a.num_anio-1 as num_anio_ant,
                              a.num_mes,
                              a.num_dias_derecho,
                              a.num_dias_gozados,
                              a.num_dias_interrumpidos,
                              a.num_total_utilizados,
                              a.num_pendientes,
                              a.num_estatus,
                              (
                              SELECT
                                SUM(u.num_dias_utiles) AS dias_utiles
                              FROM
                                rh_c082_vacacion_utilizacion AS u,
                                a006_miscelaneo_detalle AS d
                              WHERE
                                u.fk_rhc081_num_periodo = a.pk_num_periodo AND
                                d.pk_num_miscelaneo_detalle = u.fk_a006_tipo_vacacion AND
                                d.cod_detalle = 1
                            ) AS total_dias_goce,
                              (
                              SELECT
                                SUM(u.num_dias_utiles) AS dias_utiles
                              FROM
                                rh_c082_vacacion_utilizacion AS u,
                                a006_miscelaneo_detalle AS d
                              WHERE
                                u.fk_rhc081_num_periodo = a.pk_num_periodo AND
                                d.pk_num_miscelaneo_detalle = u.fk_a006_tipo_vacacion AND
                                d.cod_detalle = 2
                            ) AS total_dias_interrupcion
                            FROM
                              rh_c081_periodo_vacacion AS a
                            WHERE
                              fk_rhb001_num_empleado = $pkNumEmpleado and num_pendientes <>0
                            ORDER BY
                              a.num_anio DESC"
                );
                break;

        }
        $periodo->setFetchMode(PDO::FETCH_ASSOC);
        return $periodo->fetchAll();
    }

    // Método que permite consultar la existencia de un periodo vacacional
    public function metPeriodo($pkNumEmpleado, $mesActual, $anioActual, $metodo)
    {
        $periodo =  $this->_db->query(
                "select * from rh_c081_periodo_vacacion where num_anio=$anioActual and num_mes=$mesActual and fk_rhb001_num_empleado=$pkNumEmpleado"
            );
        $periodo->setFetchMode(PDO::FETCH_ASSOC);

        if($metodo==1){
            return $periodo->fetch();
        } else {
            return $periodo->fetchAll();
        }
    }

    // Método que permite guardar los periodos vacacionales de un funcionario
    public function metGuardarPeriodo($pkNumEmpleado, $usuario, $mesActual, $anioActual, $fecha_hora, $diasDerecho, $num_dias_gozados, $num_dias_interrumpidos, $num_total_utilizados, $num_estatus, $diasPendiente)
    {
        $this->_db->beginTransaction();
        $nuevoPeriodo = $this->_db->prepare(
            "insert into 
                      rh_c081_periodo_vacacion 
                      values 
                      ( NULL,
                      :fk_rhb001_num_empleado, 
                      :num_anio, 
                      :num_mes, 
                      :num_dias_derecho, 
                      :num_dias_gozados, 
                      :num_dias_interrumpidos, 
                      :num_total_utilizados, 
                      :num_pendientes, 
                      :num_estatus, 
                      :fk_a018_num_seguridad_usuario, 
                      :fec_ultima_modificacion)"
        );
        $nuevoPeriodo->execute(array(
            ':fk_rhb001_num_empleado' => $pkNumEmpleado,
            ':num_anio' => $anioActual,
            ':num_mes' => $mesActual,
            ':num_dias_derecho' => $diasDerecho,
            ':num_dias_gozados' => $num_dias_gozados,
            ':num_dias_gozados' => $num_dias_gozados,
            ':num_dias_interrumpidos' => $num_dias_interrumpidos,
            ':num_total_utilizados' => $num_total_utilizados,
            ':num_pendientes' => $diasPendiente,
            ':num_estatus' => $num_estatus,
            ':fk_a018_num_seguridad_usuario'=> $usuario,
            ':fec_ultima_modificacion' => $fecha_hora
        ));

        $this->_db->commit();
        return $nuevoPeriodo;

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $nuevoPeriodo->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    //Método que permite obtener los dias feriados dentro de un rango de fechas
    public function metFeriado($fechaComparar)
    {
        //Formato para la base de datoss
        $fechaExplode = explode("-", $fechaComparar);
        $anio = $fechaExplode[0];
        $mesDia = $fechaExplode[1].'-'.$fechaExplode[2];
        $diaFeriado = $this->_db->query(
            "select count(pk_num_feriado) as feriado from rh_c069_feriados where num_estatus=1 and (fec_anio='$anio' OR fec_anio = '') and fec_dia='$mesDia'"
        );
        $diaFeriado->setFetchMode(PDO::FETCH_ASSOC);
        return $diaFeriado->fetch();
    }

    // Método que permite guardar la solicitud de vacaciones
    public function metGuardarVacacionSolicitud($pkNumEmpleado, $usuario, $fecha_hora, $fechaSalida, $tipo_vacacion, $cargo, $fechaTermino, $fechaIncorporacion, $pkNumDependencia, $ind_motivo, $num_dias, $fechaSolicitud, $valor, $pkNumSolicitudVacacion)
    {
        // Formato de fechas
        if($valor==1){
            // Fecha de salida
            $fechaExplodeSalida = explode("/", $fechaSalida);
            $fechaSalida = $fechaExplodeSalida[2].'-'.$fechaExplodeSalida[1].'-'.$fechaExplodeSalida[0];

            // Fecha de Termino
            $fechaExplodeTermino = explode("/", $fechaTermino);
            $fechaTermino = $fechaExplodeTermino[2].'-'.$fechaExplodeTermino[1].'-'.$fechaExplodeTermino[0];

            // Fecha de Incorporación
            $fechaExplodeIncorporacion = explode("/", $fechaIncorporacion);
            $fechaIncorporacion = $fechaExplodeIncorporacion[2].'-'.$fechaExplodeIncorporacion[1].'-'.$fechaExplodeIncorporacion[0];
        }
        $this->_db->beginTransaction();
        $guardarSolicitud = $this->_db->prepare(
            "insert into rh_b009_vacacion_solicitud values (null, :fk_rhb001_num_empleado, :fk_a004_num_dependencia, :fk_rhc063_num_puesto, :fk_a006_tipo_vacacion, :num_dias_solicitados, :fec_solicitud, :fec_salida, :fec_termino, :fec_incorporacion, :ind_motivo, :num_otorgamiento, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion, :fk_rhb009_solicitud_vacacion)"
        );
        $guardarSolicitud->execute(array(
            ':fk_rhb001_num_empleado' => $pkNumEmpleado,
            ':fk_a004_num_dependencia' => $pkNumDependencia,
            ':fk_rhc063_num_puesto' => $cargo,
            ':fk_a006_tipo_vacacion' => $tipo_vacacion,
            ':num_dias_solicitados' => $num_dias,
            ':fec_solicitud' => $fechaSolicitud,
            ':fec_salida' => $fechaSalida,
            ':fec_termino' => $fechaTermino,
            ':fec_incorporacion' => $fechaIncorporacion,
            ':ind_motivo' => $ind_motivo,
            ':num_otorgamiento' => 0,
            ':fk_a018_num_seguridad_usuario'=> $usuario,
            ':fec_ultima_modificacion' => $fecha_hora,
            ':fk_rhb009_solicitud_vacacion' => $pkNumSolicitudVacacion
        ));
        $pkNumSolicitudVacacion = $this->_db->lastInsertId();
        $nuevaOperacion = $this->_db->prepare(
            "insert into rh_c080_operacion_vacacion values (null, :ind_estado, :fk_rhb009_num_solicitud_vacacion, :fk_rhb001_num_empleado, :fk_rhc063_num_puesto, :num_estatus, :fec_operacion,null,null,null,null,null,null)"
        );
        $nuevaOperacion->execute(array(
            ':ind_estado' => 'PR',
            ':fk_rhb009_num_solicitud_vacacion' =>  $pkNumSolicitudVacacion,
            ':fk_rhb001_num_empleado' => $usuario,
            ':fk_rhc063_num_puesto' => $cargo,
            ':num_estatus' => 1,
            ':fec_operacion' => $fecha_hora
        ));
        $this->_db->commit();
        return $pkNumSolicitudVacacion;
    }

    // Método que permite consultar las solicitudes de vacaciones asociadas a un periodo
    public function metSolicitudVacacion($periodo)
    {
        $solicitud = $this->_db->query(
            "select a.pk_num_solicitud_vacacion, a.num_dias_solicitados, c.num_anio, a.fec_salida, a.fec_termino, a.fec_incorporacion from rh_b009_vacacion_solicitud as a, rh_c079_vacacion_detalle as b, rh_c081_periodo_vacacion as c where b.fk_rhc081_num_periodo_vacacion=$periodo and a.pk_num_solicitud_vacacion=b.fk_rhb009_num_solicitud_vacacion and b.fk_rhc081_num_periodo_vacacion=c.pk_num_periodo"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }

    // Método que permite consultar un periodo determinado
    public function metConsultarPeriodo($periodo, $metodo)
    {
        if($metodo==1){
            $solicitud = $this->_db->query(
                "select num_anio, num_pendientes, num_dias_derecho, num_dias_gozados, num_dias_interrumpidos from rh_c081_periodo_vacacion where pk_num_periodo=$periodo"
            );
        } else {
            $solicitud = $this->_db->query(
                "select num_anio, num_pendientes, num_dias_derecho, num_dias_gozados, num_total_utilizados, num_dias_interrumpidos from rh_c081_periodo_vacacion where pk_num_periodo=$periodo and num_estatus=1"
            );
        }

        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetch();
    }

    public function metGuardarDetalleVacacion($pkNumSolicitudVacacion, $numPeriodo, $num_dias_derecho, $diasPendientes, $num_dias_usados, $num_dias, $usuario, $fecha_hora, $fec_inicial, $fec_final, $fec_incorporacion)
    {
        $this->_db->beginTransaction();
        $guardarSolicitud = $this->_db->prepare(
            "insert into rh_c079_vacacion_detalle values (null, :fk_rhb009_num_solicitud_vacacion, :fk_rhc081_num_periodo_vacacion, :num_dias_derecho, :num_dias_pendientes, :num_dias_usados, :num_dias_detalle, :fec_inicial, :fec_final, :fec_incorporacion, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion, null)"
        );
        $guardarSolicitud->execute(array(
            ':fk_rhb009_num_solicitud_vacacion' => $pkNumSolicitudVacacion,
            ':fk_rhc081_num_periodo_vacacion' => $numPeriodo,
            ':num_dias_derecho' => $num_dias_derecho,
            ':num_dias_pendientes' => $diasPendientes,
            ':num_dias_usados' => $num_dias_usados,
            ':num_dias_detalle'=> $num_dias,
            ':fec_inicial' => $fec_inicial,
            ':fec_final' => $fec_final,
            ':fec_incorporacion' => $fec_incorporacion,
            ':fk_a018_num_seguridad_usuario'=> $usuario,
            ':fec_ultima_modificacion' => $fecha_hora
        ));
        $this->_db->commit();
    }

    // Método que permite consultar las solicitudes de vacaciones asociadas a un empleado
    public function metListarVacacion($pkNumEmpleado, $estado)
    {
        if($estado=='AD'){
           $filtro = "";
        }
        if($estado=='LI'){
            $filtro = " and b.pk_num_empleado=$pkNumEmpleado";
        }
        if($estado=='PR'){
            $filtro = " and f.ind_estado='$estado'";
        }
        if($estado=='RE'){
            $filtro = " and f.ind_estado='$estado'";
        }
        if($estado=='CO'){
            $filtro = " and f.ind_estado='$estado'";
        }

        $listado = $this->_db->query(
            "SELECT
                          a.pk_num_solicitud_vacacion,
                          a.fec_solicitud,
                          c.ind_nombre1,
                          c.ind_nombre2,
                          c.ind_apellido1,
                          c.ind_apellido2,
                          e.ind_nombre_detalle,
                          e.cod_detalle,
                          f.ind_estado
                        FROM
                          rh_b009_vacacion_solicitud AS a,
                          rh_b001_empleado AS b,
                          a003_persona AS c,
                          a005_miscelaneo_maestro AS d,
                          a006_miscelaneo_detalle AS e,
                          rh_c080_operacion_vacacion AS f
                        WHERE
                          a.fk_rhb001_num_empleado = b.pk_num_empleado AND 
                          b.fk_a003_num_persona = c.pk_num_persona AND 
                          d.cod_maestro = 'TIPVAC' AND 
                          d.pk_num_miscelaneo_maestro = e.fk_a005_num_miscelaneo_maestro AND 
                          e.pk_num_miscelaneo_detalle = a.fk_a006_tipo_vacacion AND 
                          a.pk_num_solicitud_vacacion = f.fk_rhb009_num_solicitud_vacacion AND 
                          f.num_estatus = 1 $filtro
                        ORDER BY
                          a.pk_num_solicitud_vacacion ASC"
        );
        $listado->setFetchMode(PDO::FETCH_ASSOC);
        return $listado->fetchAll();
    }

    //Método utilizado para visualizar una solicitud de vacacion
    public function metVerVacacion($pkNumSolicitudVacacion)
    {
        $solicitud = $this->_db->query(
            "SELECT
                        a.pk_num_solicitud_vacacion,
                        a.fk_rhb009_solicitud_vacacion,
                        a.num_otorgamiento,
                        b.ind_estado,
                        DATE_FORMAT(a.fec_solicitud,'%d/%m/%Y') AS fecha_solicitud,
                        a.ind_motivo,
                        a.fk_rhc063_num_puesto,
                        a.fk_a004_num_dependencia,
                        a.fec_salida,
                        a.fec_termino,
                        a.fec_incorporacion,
                        a.num_dias_solicitados,
                        a.fk_rhb001_num_empleado,
                        a.fk_a006_tipo_vacacion,
                        d.ind_nombre_detalle,
                        d.cod_detalle,
                        e.ind_nombre1,
                        e.ind_nombre2,
                        e.ind_apellido1,
                        e.ind_apellido2,
                        b.fk_rhb001_num_empleado_revisa,
                        b.fk_rhb001_num_empleado_conforma,
                        b.fk_rhb001_num_empleado_aprueba,
                        DATE_FORMAT(b.fec_revisado,'%d/%m/%Y') AS fecha_revisado,
                        DATE_FORMAT(b.fec_conformado,'%d/%m/%Y') AS fecha_conformado,
                        DATE_FORMAT(b.fec_aprobado,'%d/%m/%Y') AS fecha_aprobado
                      FROM
                        rh_b009_vacacion_solicitud AS a,
                        rh_c080_operacion_vacacion AS b,
                        a005_miscelaneo_maestro AS c,
                        a006_miscelaneo_detalle AS d,
                        vl_rh_persona_empleado AS e
                      WHERE
                        a.pk_num_solicitud_vacacion = b.fk_rhb009_num_solicitud_vacacion AND
                        a.pk_num_solicitud_vacacion = $pkNumSolicitudVacacion AND 
                        b.num_estatus = 1 AND c.cod_maestro = 'TIPVAC' AND
                        c.pk_num_miscelaneo_maestro = d.fk_a005_num_miscelaneo_maestro AND
                        d.pk_num_miscelaneo_detalle = a.fk_a006_tipo_vacacion AND a.fk_rhb001_num_empleado = e.pk_num_empleado
                       "
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetch();
    }


/*
    //Método que permite seleccionar un estatus en especifico
    public function metEstatus($pkNumSolicitudVacacion)
    {
        $estatus = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1, d.ind_descripcion_cargo from rh_c080_operacion_vacacion as a, rh_b001_empleado as b, a003_persona as c, rh_c063_puestos as d where a.fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and a.fk_rhc063_num_puesto=d.pk_num_puestos"
        );
        $estatus->setFetchMode(PDO::FETCH_ASSOC);
        return $estatus->fetch();
    }
*/
    //Método que permite seleccionar un estatus en especifico
    public function metEstatus($pkNumSolicitudVacacion)
    {
        $estatus = $this->_db->query("
            SELECT
              a003_persona.ind_nombre1,
              a003_persona.ind_apellido1,
              rh_c063_puestos.ind_descripcion_cargo
            FROM
              rh_c080_operacion_vacacion
            INNER JOIN
              a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = rh_c080_operacion_vacacion.fk_rhb001_num_empleado
            INNER JOIN
              rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = a018_seguridad_usuario.fk_rhb001_num_empleado
            INNER JOIN
              a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN
              rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c080_operacion_vacacion.fk_rhc063_num_puesto
            WHERE
              rh_c080_operacion_vacacion.fk_rhb009_num_solicitud_vacacion = '$pkNumSolicitudVacacion'
  ");
        $estatus->setFetchMode(PDO::FETCH_ASSOC);
        return $estatus->fetch();
    }



    //Método que permite obtener el ultimo acceso de un documento
    public function metAcceso($pkNumSolicitudVacacion)
    {
        $acceso = $this->_db->query(
            "select date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%m:%S') as ultima_modificacion, d.ind_nombre1, d.ind_apellido1 from rh_b009_vacacion_solicitud as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.pk_num_solicitud_vacacion=$pkNumSolicitudVacacion and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona"
        );
        $acceso->setFetchMode(PDO::FETCH_ASSOC);
        return $acceso->fetch();
    }

    // Método que permite editar una solicitud de vacaciones
    public function metEditarVacacion($pkNumSolicitudVacacion, $num_dias, $fecha_salida, $fecha_termino, $fecha_incorporacion, $ind_motivo, $usuario, $fecha_hora)
    {

        $fechaExplodeTermino = explode("/", $fecha_termino);
        $fechaTermino = $fechaExplodeTermino[2].'-'.$fechaExplodeTermino[1].'-'.$fechaExplodeTermino[0];

        // Fecha de Incorporación
        $fechaExplodeIncorporacion = explode("/", $fecha_incorporacion);
        $fechaIncorporacion = $fechaExplodeIncorporacion[2].'-'.$fechaExplodeIncorporacion[1].'-'.$fechaExplodeIncorporacion[0];

        $this->_db->beginTransaction();
        $this->_db->query(
            "update rh_b009_vacacion_solicitud set num_dias_solicitados=$num_dias, fec_salida='$fecha_salida', fec_termino='$fechaTermino', fec_incorporacion='$fechaIncorporacion', ind_motivo='$ind_motivo', fk_a018_num_seguridad_usuario=$usuario, fec_ultima_modificacion='$fecha_hora' where pk_num_solicitud_vacacion=$pkNumSolicitudVacacion"
        );
        $this->_db->commit();
    }

    // Método que permite eliminar el detalle de una solicitud de vacaciones
    public function metEliminarDetalleVacacion($pkNumSolicitudVacacion)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from rh_c079_vacacion_detalle where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion"
        );
        $this->_db->commit();
    }

    // Método que permite generar el estado de los periodos vacacionales solicitados por los funcionarios al momento en que fue realizada la solicitud
    public function metPeriodoSolicitud($pkNumSolicitudVacacion)
    {
        $periodoVacacion = $this->_db->query(
            "select a.num_dias_derecho, b.num_pendientes, a.num_dias_detalle, a.fec_inicial, a.fec_final, b.num_anio, b.num_total_utilizados from rh_c079_vacacion_detalle as a, rh_c081_periodo_vacacion as b where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and a.fk_rhc081_num_periodo_vacacion=b.pk_num_periodo"
        );
        $periodoVacacion->setFetchMode(PDO::FETCH_ASSOC);
        return $periodoVacacion->fetchAll();
    }

    public function metActualizarEstatus($pkNumSolicitudVacacion, $valor, $pkNumEmpleado, $fechaHora, $cargo, $valorAnterior)
    {

        $this->_db->beginTransaction();
        if($valor=='AN'){
            if($valorAnterior=='AP'){
              //  $this->_db->query(
              //      "delete from rh_c080_operacion_vacacion where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and ind_estado='AP'"
              //  );
               // $this->_db->query(
             //       "delete from rh_c082_vacacion_utilizacion where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion"
              //  );
                $this->_db->query(
                    "update rh_c080_operacion_vacacion set num_estatus=1 where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and ind_estado='CO'"
                );
            }
            if($valorAnterior=='CO'){
              //  $this->_db->query(
               //     "delete from rh_c080_operacion_vacacion where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and ind_estado='CO'"
              //  );
                $this->_db->query(
                    "update rh_c080_operacion_vacacion set num_estatus=1 where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and ind_estado='RE'" );
            }
            if($valorAnterior=='RE'){
            //    $this->_db->query(
            //        "delete from rh_c080_operacion_vacacion where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and ind_estado='RE'"
             //   );
                $this->_db->query(
                    "update rh_c080_operacion_vacacion set num_estatus=1 where fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and ind_estado='PR'" );
            }
            if($valorAnterior=='PR'){

                $this->_db->query(
                    "update rh_c080_operacion_vacacion 
                                set num_estatus = 0
                                where 
                                fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion"
                );

                $cambiarEstatus=$this->_db->prepare(
                    "insert into 
                              rh_c080_operacion_vacacion 
                                values (
                                :ind_estado, 
                                :fk_rhb009_num_solicitud_vacacion, 
                                :fk_rhb001_num_empleado, 
                                :fk_rhc063_num_puesto, 
                                :num_estatus, 
                                :fec_operacion
                                )"
                );
                $cambiarEstatus->execute(array(
                    ':ind_estado' => $valor,
                    ':fk_rhb009_num_solicitud_vacacion'=> $pkNumSolicitudVacacion,
                    ':fk_rhb001_num_empleado' => $pkNumEmpleado,
                    ':fk_rhc063_num_puesto' => $cargo,
                    ':fec_operacion' => $fechaHora
                ));
            }

        } else {
            $this->_db->query(
                "update 
                            rh_c080_operacion_vacacion 
                          set 
                            num_estatus = 1 
                          where 
                            fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion"
            );
            $cambiarEstatus=$this->_db->prepare(
                "insert into 
                      rh_c080_operacion_vacacion 
                      values 
                      
                      :ind_estado, 
                      :fk_rhb009_num_solicitud_vacacion, 
                      :fk_rhb001_num_empleado, 
                      :fk_rhc063_num_puesto, 
                      :num_estatus, 
                      :fec_operacion"
            );
            $cambiarEstatus->execute(array(
                ':ind_estado' => $valor,
                ':fk_rhb009_num_solicitud_vacacion'=> $pkNumSolicitudVacacion,
                ':fk_rhb001_num_empleado' => $pkNumEmpleado,
                ':fk_rhc063_num_puesto' => $cargo,
                ':num_estatus' => 1,
                ':fec_operacion' => $fechaHora
            ));
        }
        $this->_db->commit();
    }

    // Método que permite agregar las observaciones de la conformacion a la solicitud de vacaciones
    public function metConformar($pkNumSolicitudDetalle, $ind_observacion)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update
                        rh_c079_vacacion_detalle 
                      set 
                        ind_observacion_conformacion='$ind_observacion' 
                      where
                        pk_num_vacacion_detalle=$pkNumSolicitudDetalle"
        );
        $this->_db->commit();
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite listar empleados
    public function metListarEmpleado($pkNumDependencia, $metodo)
    {
        if($metodo==1){
            $empleado =  $this->_db->query(
                "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b, rh_c076_empleado_organizacion as c where a.fk_a003_num_persona=b.pk_num_persona and c.fk_a004_num_dependencia=$pkNumDependencia and c.fk_rhb001_num_empleado=a.pk_num_empleado and a.num_estatus=1"
            );
        } else {
            $empleado =  $this->_db->query(
                "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b where a.fk_a003_num_persona=b.pk_num_persona and a.num_estatus=1"
            );
        }
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo WHERE ind_tipo_organismo='I'"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar lo empleados asociados a una dependencia
    public function metDependenciaEmpleado($pkNumDependencia)
    {
        $empleado =  $this->_db->query(
            "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b, rh_c076_empleado_organizacion as c where a.fk_a003_num_persona=b.pk_num_persona and c.fk_a004_num_dependencia=$pkNumDependencia and c.fk_rhb001_num_empleado=a.pk_num_empleado and a.num_estatus=1"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método encargado de consultar los datos procedentes del filtro
    public function metBuscarVacacion($pk_num_organismo, $pk_num_dependencia, $pk_num_empleado, $fechaInicio, $fechaFin, $pk_num_solicitud_vacacion, $ind_estado)
    {

        if ($pk_num_organismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pk_num_organismo";
        }
        if ($pk_num_dependencia!='') {
            $filtro .= " and b.fk_a004_num_dependencia=$pk_num_dependencia";
        }
        if ($pk_num_empleado!= '') {
            $filtro .= " and b.pk_num_empleado=$pk_num_empleado";
        }
        if ($pk_num_solicitud_vacacion!= '') {
            $filtro .= " and a.pk_num_solicitud_vacacion=$pk_num_solicitud_vacacion";
        }
        if (($fechaInicio!='')&&($fechaFin!='')) {
            $fechaExplodeInicio = explode("/", $fechaInicio);
            $fecha_inicio = $fechaExplodeInicio[2].'-'.$fechaExplodeInicio[1].'-'.$fechaExplodeInicio[0];
            // Fecha Fin
            $fechaExplodeFin = explode("/", $fechaFin);
            $fecha_fin = $fechaExplodeFin[2].'-'.$fechaExplodeFin[1].'-'.$fechaExplodeFin[0];
            $filtro .= " and a.fec_solicitud between '$fecha_inicio' and '$fecha_fin'";
        }
        if ($ind_estado!='') {
            $filtro .= " and e.ind_estado='$ind_estado'";
        }

        $buscarVacacion = $this->_db->query(
            "SELECT
                          a.pk_num_solicitud_vacacion,
                          a.num_dias_solicitados,
                          DATE_FORMAT(a.fec_salida,'%d/%m/%Y') AS fecha_salida,
                          DATE_FORMAT(a.fec_termino,'%d/%m/%Y') AS fecha_termino,
                          DATE_FORMAT(a.fec_incorporacion,'%d/%m/%Y') AS fecha_incorporacion,
                          DATE_FORMAT(a.fec_solicitud,'%d/%m/%Y') AS fecha_solicitud,
                          b.ind_nombre1,
                          b.ind_nombre2,
                          b.ind_apellido1,
                          b.ind_apellido2,
                          d.ind_nombre_detalle,
                          e.ind_estado
                        FROM
                          rh_b009_vacacion_solicitud AS a,
                          vl_rh_persona_empleado_datos_laborales AS b,
                          a005_miscelaneo_maestro AS c,
                          a006_miscelaneo_detalle AS d,
                          rh_c080_operacion_vacacion AS e
                        WHERE
                          b.pk_num_empleado = a.fk_rhb001_num_empleado AND 
                          c.cod_maestro = 'TIPVAC' AND 
                          c.pk_num_miscelaneo_maestro = d.fk_a005_num_miscelaneo_maestro AND 
                          d.pk_num_miscelaneo_detalle = a.fk_a006_tipo_vacacion AND 
                          a.pk_num_solicitud_vacacion = e.fk_rhb009_num_solicitud_vacacion AND 
                          e.num_estatus = 1 
                        ORDER BY
                          a.pk_num_solicitud_vacacion ASC"
        );
        $buscarVacacion->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarVacacion->fetchAll();
    }

    // Método que permite manejar el detalle de la solicitud de vacaciones 
    public function metMostrarDetalleVacacion($pkNumSolicitudVacacion, $valor)
    {
        $detalle =  $this->_db->query(
            "SELECT
                          DATE_FORMAT(a.fec_solicitud,'%d/%m/%Y') AS fecha_solicitud,
                          b.pk_num_vacacion_detalle,
                          b.fk_rhc081_num_periodo_vacacion,
                          b.fec_inicial,
                          b.fec_final,
                          b.fec_incorporacion,
                          b.num_dias_detalle,
                          b.ind_observacion_conformacion,
                          b.num_dias_usados,
                          b.num_dias_derecho,
                          b.num_dias_pendientes,
                          c.num_anio
                        FROM
                          rh_b009_vacacion_solicitud AS a,
                          rh_c079_vacacion_detalle AS b,
                          rh_c081_periodo_vacacion AS c
                        WHERE
                          a.pk_num_solicitud_vacacion = $pkNumSolicitudVacacion AND 
                          a.pk_num_solicitud_vacacion = b.fk_rhb009_num_solicitud_vacacion AND 
                          b.fk_rhc081_num_periodo_vacacion = c.pk_num_periodo
                        ORDER BY
                         c.num_anio DESC LIMIT 0,1"
        );

        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetchAll();
    }

    // Método que permite obtener un estatus aprobatorio definido
    public function metEstatusDetalle($pkNumSolicitudVacacion, $estatus)
    {
        $estatus = $this->_db->query(
            "SELECT
                          DATE_FORMAT(rh_c080_operacion_vacacion.fec_operacion,'%d/%m/%Y') AS fecha_operacion,
                          a003_persona.ind_nombre1,
                          a003_persona.ind_apellido1,
                          rh_c063_puestos.ind_descripcion_cargo
                        FROM
                          rh_c080_operacion_vacacion
                        INNER JOIN
                          a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = rh_c080_operacion_vacacion.fk_rhb001_num_empleado
                        INNER JOIN
                          rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = a018_seguridad_usuario.fk_rhb001_num_empleado
                        INNER JOIN
                          a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN
                          rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c080_operacion_vacacion.fk_rhc063_num_puesto
                        WHERE
                          rh_c080_operacion_vacacion.fk_rhb009_num_solicitud_vacacion = $pkNumSolicitudVacacion
     ");
        $estatus->setFetchMode(PDO::FETCH_ASSOC);
        return $estatus->fetch();
    }


    // Método que gestiona la utilización de las vacaciones
    public function metUtilizacion($pkNumSolicitudVacacion, $tipoVacacion, $numDiasDetalle, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario, $flag)
    {
        $this->_db->beginTransaction();
        $utilizacion=$this->_db->prepare(
            "insert into rh_c082_vacacion_utilizacion values (null, :num_dias_utiles, :fec_inicio, :fec_fin, :fk_a006_tipo_vacacion, :fk_rhb009_num_solicitud_vacacion, :fk_rhc081_num_periodo, :num_flag_solicitud, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $utilizacion->execute(array(
            ':num_dias_utiles' => $numDiasDetalle,
            ':fec_inicio' => $fechaInicio,
            ':fec_fin' => $fechaFin,
            ':fk_a006_tipo_vacacion' => $tipoVacacion,
            ':fk_rhb009_num_solicitud_vacacion' => $pkNumSolicitudVacacion,
            ':fk_rhc081_num_periodo' => $pkNumPeriodo,
            ':num_flag_solicitud' => $flag,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fec_ultima_modificacion' => $fechaHora
        ));
        $this->_db->commit();
    }

    public function metModificarUtilizacion($pk_num_vacacion_utilizacion, $tipoVacacion, $numDiasDetalle, $fechaInicio, $fechaFin, $pkNumPeriodo, $fechaHora, $usuario)
    {
        $this->_db->beginTransaction();
        $utilizacion=$this->_db->prepare(
            "update rh_c082_vacacion_utilizacion set num_dias_utiles=:num_dias_utiles, fec_inicio=:fec_inicio, fec_fin=:fec_fin,fk_a006_tipo_vacacion=:fk_a006_tipo_vacacion, fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, fec_ultima_modificacion=:fec_ultima_modificacion WHERE pk_num_vacacion_utilizacion=$pk_num_vacacion_utilizacion"
        );
        $utilizacion->execute(array(
            ':num_dias_utiles' => $numDiasDetalle,
            ':fec_inicio' => $fechaInicio,
            ':fec_fin' => $fechaFin,
            ':fk_a006_tipo_vacacion' => $tipoVacacion,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fec_ultima_modificacion' => $fechaHora
        ));

        $error = $utilizacion->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $pk_num_vacacion_utilizacion;
        }

    }

    // Método utilizado para actualizar los periodos vacacionales de los empleados al aplicar la utilización de vacaciones
    public function metActualizarPeriodo($pkNumPeriodo, $nuevoPendiente, $diasGozados, $estado)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update rh_c081_periodo_vacacion 
                        set num_dias_gozados=$diasGozados, 
                        num_total_utilizados=$diasGozados, 
                        num_pendientes=$nuevoPendiente, 
                        num_estatus=$estado 
                        where 
                        pk_num_periodo=$pkNumPeriodo"
        );

        $this->_db->query(
            "update rh_c079_vacacion_detalle 
                        set
                        num_dias_usados=$diasGozados, 
                        num_dias_pendientes=$nuevoPendiente, 
                        num_dias_detalle=$nuevoPendiente
                        where 
                        fk_rhc081_num_periodo_vacacion=$pkNumPeriodo"
        );
        $this->_db->commit();
    }

    // Método que permite obtener la utilizacion de vacaciones de un funcionario en un periodo especifico
    public function metMostrarUtilizacion($pkNumPeriodo)
    {
        $utilizacion = $this->_db->query(
            "select a.pk_num_vacacion_utilizacion, a.num_dias_utiles, date_format(a.fec_inicio, '%d/%m/%Y') as fecha_inicio, date_format(a.fec_fin, '%d/%m/%Y') as fecha_fin, c.cod_detalle, c.ind_nombre_detalle from rh_c082_vacacion_utilizacion as a, a005_miscelaneo_maestro as b, a006_miscelaneo_detalle as c where a.fk_rhc081_num_periodo=$pkNumPeriodo and b.cod_maestro='TIPVAC' and b.pk_num_miscelaneo_maestro=c.fk_a005_num_miscelaneo_maestro and c.pk_num_miscelaneo_detalle=a.fk_a006_tipo_vacacion order by a.pk_num_vacacion_utilizacion asc"
        );
        $utilizacion->setFetchMode(PDO::FETCH_ASSOC);
        return $utilizacion->fetchAll();
    }

    public function metMostrarUtilizacionDetalle($pkNumPeriodo,$tipo)
    {
        $utilizacion = $this->_db->query(
            "select a.pk_num_vacacion_utilizacion, a.num_dias_utiles, date_format(a.fec_inicio, '%d/%m/%Y') as fecha_inicio, date_format(a.fec_fin, '%d/%m/%Y') as fecha_fin, c.cod_detalle, c.ind_nombre_detalle from rh_c082_vacacion_utilizacion as a, a005_miscelaneo_maestro as b, a006_miscelaneo_detalle as c where a.fk_rhc081_num_periodo=$pkNumPeriodo and b.cod_maestro='TIPVAC' AND c.cod_detalle=$tipo and b.pk_num_miscelaneo_maestro=c.fk_a005_num_miscelaneo_maestro and c.pk_num_miscelaneo_detalle=a.fk_a006_tipo_vacacion order by a.pk_num_vacacion_utilizacion asc"
        );
        $utilizacion->setFetchMode(PDO::FETCH_ASSOC);
        return $utilizacion->fetchAll();
    }

    // Método que permite obtener la utilizacion de vacaciones de un funcionario en un periodo especifico
    public function metMostrarUtilizacionEmpleado($pk_num_vacacion_utilizacion)
    {
        $utilizacion = $this->_db->query(
            "select a.pk_num_vacacion_utilizacion, a.num_dias_utiles, date_format(a.fec_inicio, '%d/%m/%Y') as fecha_inicio, date_format(a.fec_fin, '%d/%m/%Y') as fecha_fin, c.cod_detalle, c.ind_nombre_detalle, a.fk_a006_tipo_vacacion from rh_c082_vacacion_utilizacion as a, a005_miscelaneo_maestro as b, a006_miscelaneo_detalle as c where a.pk_num_vacacion_utilizacion=$pk_num_vacacion_utilizacion and b.cod_maestro='TIPVAC' and b.pk_num_miscelaneo_maestro=c.fk_a005_num_miscelaneo_maestro and c.pk_num_miscelaneo_detalle=a.fk_a006_tipo_vacacion order by a.pk_num_vacacion_utilizacion asc"
        );
        $utilizacion->setFetchMode(PDO::FETCH_ASSOC);
        return $utilizacion->fetch();
    }

    // Método que permite eliminar una utilización de vacaciones
    public function metEliminarUtilizacion($pk_num_vacacion_utilizacion)
    {
        $this->_db->beginTransaction();

        $eliminar=$this->_db->prepare("
                delete from rh_c082_vacacion_utilizacion where pk_num_vacacion_utilizacion = '$pk_num_vacacion_utilizacion'
            ");
        $eliminar->execute();

        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $pk_num_vacacion_utilizacion;
        }


    }

    // Método que permite obtener la ultima solicitud de vacaciones (Goce)
    public function metBuscarSolicitud($pkNumPeriodo)
    {
        // Consulto la ultima solicitud de vacaciones de Goce de ese periodo
        $buscarSolicitud = $this->_db->query(
            "select a.fk_rhb009_num_solicitud_vacacion, a.num_dias_utiles, a.fec_inicio, a.fec_fin from rh_c082_vacacion_utilizacion as a where a.fk_rhc081_num_periodo=$pkNumPeriodo and num_flag_solicitud=1 order by a.pk_num_vacacion_utilizacion desc"
        );
        $buscarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarSolicitud->fetch();
    }

    // Método que permite ubicar el periodo de una utilizacion
    public function metBuscarPeriodo($pk_num_vacacion_utilizacion)
    {
        $buscarPeriodo = $this->_db->query(
            "select fk_rhc081_num_periodo, 	fk_a006_tipo_vacacion from rh_c082_vacacion_utilizacion where pk_num_vacacion_utilizacion=$pk_num_vacacion_utilizacion"
        );
        $buscarPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarPeriodo->fetch();
    }

    // Método que permite obtener las operaciones de utilización realizadas sobre una solicitud
    public function metBuscarUtilizacion($numSolicitud, $flagSolicitud, $orden)
    {
        if($orden==1){
            $ordenar = ' order by a.pk_num_vacacion_utilizacion asc';
        } else {
            $ordenar = ' order by a.pk_num_vacacion_utilizacion desc';
        }
        $buscarUtilizacion = $this->_db->query(
            "select a.num_dias_utiles, c.ind_nombre_detalle, a.fk_rhb009_num_solicitud_vacacion from rh_c082_vacacion_utilizacion as a, a005_miscelaneo_maestro as b, a006_miscelaneo_detalle as c where a.fk_rhb009_num_solicitud_vacacion=$numSolicitud and a.num_flag_solicitud=$flagSolicitud and b.cod_maestro='TIPVAC' and b.pk_num_miscelaneo_maestro=c.fk_a005_num_miscelaneo_maestro and c.pk_num_miscelaneo_detalle=a.fk_a006_tipo_vacacion $ordenar"
        );
        $buscarUtilizacion->setFetchMode(PDO::FETCH_ASSOC);
        if($orden==1){
            return $buscarUtilizacion->fetchAll();
        } else {
            return $buscarUtilizacion->fetch();
        }

    }

    // Método que permite obtener un tipo de vacacion en particular
    public function metBuscarTipoVacacion($tipoVacacion)
    {
        $buscarTipoVacacion = $this->_db->query(
            "select b.ind_nombre_detalle, b.cod_detalle from a005_miscelaneo_maestro as a, a006_miscelaneo_detalle as b where a.cod_maestro='TIPVAC' and a.pk_num_miscelaneo_maestro=b.fk_a005_num_miscelaneo_maestro and b.pk_num_miscelaneo_detalle=$tipoVacacion"
        );
        $buscarTipoVacacion->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTipoVacacion->fetch();
    }

    // Método que permite manejar la utilización de vacaciones de un periodo determinado
    public function metPeriodoUtilizacion($pkNumPeriodo, $total, $totalInterrumpidos, $numPendientes, $numFlagEstatus, $fechaHora, $usuario)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update rh_c081_periodo_vacacion set num_dias_gozados=$total, num_dias_interrumpidos=$totalInterrumpidos, 	num_total_utilizados=$total, num_pendientes=$numPendientes, num_estatus=$numFlagEstatus, fk_a018_num_seguridad_usuario=$usuario, fec_ultima_modificacion='$fechaHora' where pk_num_periodo=$pkNumPeriodo"
        );
        $this->_db->commit();
    }

    // Método que permite ver las utilizaciones
    public function metVerUtilizacion($numSolicitud)
    {
        $buscarUtilizacion = $this->_db->query(
            "select a.num_dias_utiles, c.cod_detalle, c.ind_nombre_detalle, a.fk_rhb009_num_solicitud_vacacion from rh_c082_vacacion_utilizacion as a, a005_miscelaneo_maestro as b, a006_miscelaneo_detalle as c where a.	pk_num_vacacion_utilizacion=$numSolicitud and b.cod_maestro='TIPVAC' and b.pk_num_miscelaneo_maestro=c.fk_a005_num_miscelaneo_maestro and c.pk_num_miscelaneo_detalle=a.fk_a006_tipo_vacacion"
        );
        $buscarUtilizacion->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarUtilizacion->fetch();
    }

    // Método que permite obtener el antecedente de servicio de un empleado
    public function metConsultarAntecedente($pkNumEmpleado)
    {
        $buscarAntecedente = $this->_db->query(
            "select fec_ingreso, fec_egreso from rh_c011_experiencia where 	fk_rhb001_num_empleado=$pkNumEmpleado and num_flag_antvacacion=1"
        );
        $buscarAntecedente->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarAntecedente->fetchAll();
    }

    // Método que permite ver las utilizaciones
    public function metConsultarNomina($pkNumEmpleado)
    {
        $consultarNomina = $this->_db->query(
            "select fk_nmb001_num_tipo_nomina from vl_rh_persona_empleado_datos_laborales where pk_num_empleado=$pkNumEmpleado"
        );
        $consultarNomina->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarNomina->fetch();
    }

    // Método que permite consultar la tabla de vacaciones
    public function metConsultarTabla($anioTotal, $pkNumTipoNomina)
    {
        $consultarTabla = $this->_db->query(
            "select num_dias_disfrute, num_dias_adicionales, num_total_disfrutar from rh_c024_tabla_vacacion where num_anios=$anioTotal and fk_nmb001_num_nomina=$pkNumTipoNomina"
        );

        $consultarTabla->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarTabla->fetch();
    }

    //Método que permite seleccionar un el ultimo estado de una solicitud
    public function metConsultarEstatus($pkNumSolicitudVacacion)
    {
        $estatus = $this->_db->query(
            "select 
                        ind_estado 
                        from 
                        rh_c080_operacion_vacacion 
                        where 
                        fk_rhb009_num_solicitud_vacacion=$pkNumSolicitudVacacion and num_estatus=1"
        );
        $estatus->setFetchMode(PDO::FETCH_ASSOC);
        return $estatus->fetch();
    }

    // Método que permite consultar si un empleado tiene una solicitud de vacacion pendiente
    public function metComprobarVacacion($pkNumEmpleado)
    {
        $comprobarVacacion = $this->_db->query(
            "select b.ind_estado from rh_b009_vacacion_solicitud as a, rh_c080_operacion_vacacion as b where a.pk_num_solicitud_vacacion=b.fk_rhb009_num_solicitud_vacacion and a.fk_rhb001_num_empleado=$pkNumEmpleado order by b.pk_num_operacion desc"
        );
        $comprobarVacacion->setFetchMode(PDO::FETCH_ASSOC);
        return $comprobarVacacion->fetch();
    }

    // Método que permite listar empleados
    public function metListadoEmpleado()
    {
        $listadoEmpleado = $this->_db->query(
            "select * from vl_rh_persona_empleado where ind_estatus_empleado=1"
        );
        $listadoEmpleado ->setFetchMode(PDO::FETCH_ASSOC);
        return $listadoEmpleado ->fetchAll();
    }

    // Método que permite listar dependencias
    public function metListadoDependencia()
    {
        $listarDependencia =  $this->_db->query(
            "select pk_num_dependencia, ind_dependencia from a004_dependencia where num_estatus=1"
        );
        $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $listarDependencia->fetchAll();
    }

    public function metObtenerEmpleado($pkNumDependencia)
    {
        $listarEmpleado =  $this->_db->query(
            "select pk_num_empleado from vl_rh_persona_empleado_datos_laborales where fk_a004_num_dependencia=$pkNumDependencia and ind_estatus=1"
        );
        $listarEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEmpleado->fetch();
    }

    // método que permite editar un periodo
    public function metGuardarEditarPeriodo($pkNumEmpleado, $numAnio, $diasDerecho,  $diasGozados, $diasInterrumpidos, $totalUtilizados, $totalPendientes, $estado, $usuario, $fecha_hora)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update rh_c081_periodo_vacacion set num_dias_derecho=$diasDerecho, num_dias_gozados=$diasGozados, num_dias_interrumpidos=$diasInterrumpidos, num_total_utilizados=$totalUtilizados, num_pendientes=$totalPendientes, num_estatus= $estado, fk_a018_num_seguridad_usuario=$usuario, fec_ultima_modificacion='$fecha_hora' where fk_rhb001_num_empleado=$pkNumEmpleado and num_anio=$numAnio"
        );

        $this->_db->commit();
    }

// Método que permite manejar un detalle de la solicitud de vacaciones
    public function metVerDetalleVacacion($pkNumVacacionDetalle)
    {
        $detalle =  $this->_db->query(
            "select ind_observacion_conformacion from rh_c079_vacacion_detalle where pk_num_vacacion_detalle=$pkNumVacacionDetalle"
        );
        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetch();
    }

    public function metMostrarDetalle($pkNumSolicitudVacacion)
    {
        $detalle =  $this->_db->query(
            "select date_format(a.fec_solicitud, '%d/%m/%Y') as fecha_solicitud, b.pk_num_vacacion_detalle, b.fk_rhc081_num_periodo_vacacion, b.fec_inicial, b.fec_final, b.fec_incorporacion, b.num_dias_detalle, b.ind_observacion_conformacion, b.num_dias_usados, b.num_dias_derecho, b.num_dias_pendientes, c.num_anio  from rh_b009_vacacion_solicitud as a, rh_c079_vacacion_detalle as b, rh_c081_periodo_vacacion as c where a.pk_num_solicitud_vacacion=$pkNumSolicitudVacacion and a.pk_num_solicitud_vacacion=b.fk_rhb009_num_solicitud_vacacion and b.fk_rhc081_num_periodo_vacacion=c.pk_num_periodo order by b.pk_num_vacacion_detalle desc limit 1"
        );
        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetch();
    }

    // Metodo que permite mostrar el nombre de un miscelaneo
    public function metMostrarMiscelaneo($tipoVacacion)
    {
        $miscelaneo =  $this->_db->query(
            "select ind_nombre_detalle from a006_miscelaneo_detalle where pk_num_miscelaneo_detalle=$tipoVacacion"
        );
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneo->fetch();
    }

    public function metActualizarSolicitud($pkNumSolicitudVacacion, $estadoLista)
    {
        if($estadoLista == 'PR'){
            $estado = 'RE';
            $actualizar = "fk_rhb001_num_empleado_revisa = '$this->atIdEmpleado',";
            $fechactualizar = 'fec_revisado = NOW(),';
        }elseif($estadoLista == 'RE'){
            $estado = 'CO';
            $actualizar = "fk_rhb001_num_empleado_conforma = '$this->atIdEmpleado',";
            $fechactualizar = 'fec_conformado = NOW(),';

        }elseif($estadoLista == 'CO'){
            $estado = 'AP';
            $actualizar = "fk_rhb001_num_empleado_aprueba = '$this->atIdEmpleado',";
            $fechactualizar = 'fec_aprobado = NOW(),';

        }else{
            $estado = 'PR';
            $actualizar = '';
            $fechactualizar = '';
        }

        $this->_db->beginTransaction();
        $revisar = $this->_db->prepare("
                  UPDATE
                    rh_c080_operacion_vacacion
                  SET
                    $actualizar
                    $fechactualizar
                    ind_estado= '$estado'
                  WHERE
                    fk_rhb009_num_solicitud_vacacion=:fk_rhb009_num_solicitud_vacacion
        ");
        $revisar->execute(array(
            'fk_rhb009_num_solicitud_vacacion' => $pkNumSolicitudVacacion
        ));
        $error = $revisar->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $pkNumSolicitudVacacion;
        }
    }

    public function metFirmantesReporte($idEmpleado){

            $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              per.ind_documento_fiscal,
              puesto.ind_descripcion_cargo,
              dependencia.pk_num_dependencia
            FROM rh_b001_empleado AS emp
            INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona
            INNER JOIN rh_c059_empleado_nivelacion AS nivelacion ON nivelacion.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = nivelacion.fk_rhc063_num_puestos
            LEFT JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
            WHERE
              emp.pk_num_empleado='$idEmpleado'
              ORDER BY nivelacion.pk_num_empleado_nivelacion DESC
        ");
            $persona->setFetchMode(PDO::FETCH_ASSOC);
            return $persona->fetch();
    }

    public function metMostrarEmpleadoCargo($idEmpleado){
        $persona = $this->_db->query("
            SELECT
              persona.ind_nombre1,
              persona.ind_nombre2,
              persona.ind_apellido1,
              persona.ind_apellido2,
              persona.ind_documento_fiscal,
              puesto.ind_descripcion_cargo,
              organizacion.fk_a004_num_dependencia
            FROM            
            rh_b001_empleado AS empleado
            LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            LEFT JOIN rh_c059_empleado_nivelacion AS nivelacion ON nivelacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = nivelacion.fk_rhc063_num_puestos
            LEFT JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
            WHERE empleado.pk_num_empleado= '$idEmpleado'
             ORDER BY nivelacion.pk_num_empleado_nivelacion DESC
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }


}// fin de la clase
