<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class viaticosModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    public function metConsultarOrganismos()
    {

        $con = $this->_db->query("
                SELECT * FROM a001_organismo WHERE ind_tipo_organismo='I' ORDER BY  pk_num_organismo ASC
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metUsuario($usuario, $metodo)
    {
       $usuario = $this->_db->query("SELECT
                c.ind_nombre1,
                c.ind_nombre2,
                c.ind_apellido1,
                c.ind_apellido2,
                b.pk_num_empleado
            FROM
                a018_seguridad_usuario AS a
            INNER JOIN rh_b001_empleado AS b ON b.pk_num_empleado = a.fk_rhb001_num_empleado
            INNER JOIN a003_persona AS c ON c.pk_num_persona = b.fk_a003_num_persona
            WHERE
                a.pk_num_seguridad_usuario = '$usuario'
            AND a.fk_rhb001_num_empleado = b.pk_num_empleado
            AND b.fk_a003_num_persona = c.pk_num_persona");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1) {  //para acceder de manera individual: $usuario[0]['ind_apellido1']
            return $usuario->fetch();//una sola fila
        } else {
            return $usuario->fetchAll();
        }

    }

    public function metConsultarDepencias()
    {
        $usuario = $this->_db->query("SELECT * FROM `a004_dependencia`");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }

    public function metConsultarEmpleados()
    {
        $empleado = $this->_db->query("SELECT c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado, c.pk_num_persona, b.num_estatus FROM rh_b001_empleado AS b, a003_persona AS c
WHERE b.fk_a003_num_persona = c.pk_num_persona AND b.num_estatus =1");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    public function metListarEmpleados($opc, $organismo, $dependencia)
    {
	  	if($opc == 1){//filtro por organismo
            $empleado = $this->_db->query("SELECT
             rh_b001_empleado.pk_num_empleado,
             a003_persona.ind_nombre1,
             a003_persona.ind_nombre2,
             a003_persona.ind_apellido1,
             a003_persona.ind_apellido2,
             a003_persona.ind_cedula_documento,
             rh_c005_empleado_laboral.fec_ingreso,
             rh_c006_tipo_cargo.txt_descripcion,
             rh_c009_nivel.num_nivel,
             rh_c007_grado_salarial.ind_grado,
            a001_organismo.pk_num_organismo,
            a001_organismo.ind_descripcion_empresa,
            a004_dependencia.pk_num_dependencia,
            a004_dependencia.ind_dependencia,
            rh_c063_puestos.ind_descripcion_cargo
            FROM a003_persona
            INNER JOIN  rh_b001_empleado ON  rh_b001_empleado.fk_a003_num_persona =  a003_persona.pk_num_persona
            INNER JOIN  rh_c005_empleado_laboral ON   rh_c005_empleado_laboral.fk_rhb001_num_empleado  = rh_b001_empleado.pk_num_empleado
            INNER JOIN  rh_c063_puestos ON  rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
            INNER JOIN  rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
            INNER JOIN  rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
            INNER JOIN  rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = rh_c076_empleado_organizacion.fk_a001_num_organismo
            INNER JOIN  a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
            WHERE a001_organismo.pk_num_organismo = '$organismo' " );

        }else{
            $empleado = $this->_db->query("SELECT
             rh_b001_empleado.pk_num_empleado,
             a003_persona.ind_nombre1,
             a003_persona.ind_nombre2,
             a003_persona.ind_apellido1,
             a003_persona.ind_apellido2,
             a003_persona.ind_cedula_documento,
             rh_c005_empleado_laboral.fec_ingreso,
             rh_c006_tipo_cargo.txt_descripcion,
             rh_c009_nivel.num_nivel,
             rh_c007_grado_salarial.ind_grado,
            a001_organismo.pk_num_organismo,
            a001_organismo.ind_descripcion_empresa,
            a004_dependencia.pk_num_dependencia,
            a004_dependencia.ind_dependencia,
            rh_c063_puestos.ind_descripcion_cargo
            FROM a003_persona
            INNER JOIN  rh_b001_empleado ON  rh_b001_empleado.fk_a003_num_persona =  a003_persona.pk_num_persona
            INNER JOIN  rh_c005_empleado_laboral ON   rh_c005_empleado_laboral.fk_rhb001_num_empleado  = rh_b001_empleado.pk_num_empleado
            INNER JOIN  rh_c063_puestos ON  rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
            INNER JOIN  rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
            INNER JOIN  rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
            INNER JOIN  rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = rh_c076_empleado_organizacion.fk_a001_num_organismo
            INNER JOIN  a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
            WHERE a001_organismo.pk_num_organismo = '$organismo'
            AND a004_dependencia.pk_num_dependencia = '$dependencia' and rh_b001_empleado.num_estatus=1 " );
        }

        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    public function metBuscarDependenciaEmpleado($id){

        $empleado = $this->_db->query("SELECT
            pk_num_empleado_organizacion,
            fk_rhb001_num_empleado,
            fk_a001_num_organismo,
            fk_a004_num_dependencia
        FROM
            rh_c076_empleado_organizacion
        WHERE
            fk_rhb001_num_empleado = '$id'");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();

    }

    public function metConsultarPrueba()
    {
        $empleado = $this->_db->query("SELECT pk_num_miscelaneo_maestro,ind_nombre_maestro,
    ind_descripcion,num_estatus, cod_maestro
    FROM `a005_miscelaneo_maestro`
    LIMIT 0 , 100");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    public function metInsertarSolicitud($fk_rhb001_num_empleado, $fec_fecha_solicitud, $fec_ultima_modificacion, $fk_a018_num_seguridad_usuario)
    {
        $this->_db->beginTransaction();
        $NuevaSolicitud=$this->_db->prepare(
            "insert into rh_b002_solicitud values (null, :fk_rhb001_num_empleado, :fec_fecha_solicitud, :fec_ultima_modificacion, :fk_a018_num_seguridad_usuario)"
        );
        $NuevaSolicitud->execute(array(
            ':fk_rhb001_num_empleado' => $fk_rhb001_num_empleado,
            ':fec_fecha_solicitud' => $fec_fecha_solicitud,
            ':fec_ultima_modificacion' => $fec_ultima_modificacion,
            ':fk_a018_num_seguridad_usuario' => $fk_a018_num_seguridad_usuario

        ));
        $pkSolicitud = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkSolicitud;
    }

    public function metInsertarViatico(
        $anio,$fec_registro_viatico, $fk_rhb001_num_empleado_solicita, $fec_salida, $fec_ingreso, $num_total_dias,
        $ind_destino, $ind_razon_rechazo, $ind_observacion, $ind_motivo, $cod_interno, $fk_rhb001_num_empleado_preparado_por,
        $fk_rha004_dependencia, $fk_a001_num_organismo, $ind_estado, $fk_rhb001_num_empleado_aprobado_por,
        $fec_aprobacion,$unidadCombustible){
        $this->_db->beginTransaction();
        $NuevaSolicitud=$this->_db->prepare(
            "INSERT INTO
                rh_c045_viatico
             SET
                fec_anio=:fec_anio,
                fec_salida=:fec_salida,
                fec_ingreso=:fec_ingreso,
                num_total_dias=:num_total_dias,
                ind_destino=:ind_destino,
                ind_razon_rechazo=:ind_razon_rechazo,
                ind_observacion=:ind_observacion,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_registro_viatico=:fec_registro_viatico,
                ind_motivo=:ind_motivo,
                cod_interno=:cod_interno,
                ind_estado=:ind_estado,
                fec_aprobacion=:fec_aprobacion,
                num_unidad_combustible=:num_unidad_combustible,
                fk_rhb001_num_empleado_solicita=:fk_rhb001_num_empleado_solicita,
                fk_rhb001_num_empleado_preparado_por=:fk_rhb001_num_empleado_preparado_por,
                fk_rhb001_num_empleado_aprobado_por=:fk_rhb001_num_empleado_aprobado_por,
                fk_a001_num_organismo=:fk_a001_num_organismo,
                fk_a004_num_dependencia=:fk_a004_num_dependencia
             ");
        $NuevaSolicitud->execute(array(
            ':fec_anio' => $anio,
            ':fec_registro_viatico' => $fec_registro_viatico,
            ':fk_rhb001_num_empleado_solicita' => $fk_rhb001_num_empleado_solicita,
            ':fec_salida' => $fec_salida,
            ':fec_ingreso' => $fec_ingreso,
            ':num_total_dias' => $num_total_dias,
            ':ind_destino' => $ind_destino,
            ':ind_razon_rechazo' => $ind_razon_rechazo,
            ':ind_observacion' => $ind_observacion,
            ':ind_motivo' => $ind_motivo,
            ':cod_interno' => $cod_interno,
            ':fk_rhb001_num_empleado_preparado_por' => $fk_rhb001_num_empleado_preparado_por,
            ':fk_a004_num_dependencia' => $fk_rha004_dependencia,
            ':fk_a001_num_organismo' => $fk_a001_num_organismo,
            ':ind_estado' => $ind_estado,
            ':fk_rhb001_num_empleado_aprobado_por' => $fk_rhb001_num_empleado_aprobado_por,
            ':fec_aprobacion' => $fec_aprobacion,
            ':num_unidad_combustible' => $unidadCombustible
        ));
        $pkViatico = $this->_db->lastInsertId();
        $error = $NuevaSolicitud->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $pkViatico;
        }

    }

    public function metCorrelativoCodInternoViatico($dependencia,$anio){
        $correlativo = $this->_db->query("
        SELECT
        COUNT( * ) AS total
        FROM rh_c045_viatico
        WHERE fk_a004_num_dependencia = '$dependencia' and fec_anio='$anio'
        ");
        $correlativo->setFetchMode(PDO::FETCH_ASSOC);
        return $correlativo->fetch();
    }

    public function metInsertarDetalleViatico($fk_rhc045_num_viatico, $fk_rhb001_num_empleado_beneficiario,$fec_salida_detalle,
        $fec_ingreso_detalle, $num_total_dias_detalle){

        $this->_db->beginTransaction();
        $detalleViatico=$this->_db->prepare("
              INSERT INTO
                    rh_c004_detalle_viatico
              SET
                    fk_rhc045_num_viatico=:fk_rhc045_num_viatico,
                    fk_rhb001_num_empleado_beneficiario=:fk_rhb001_num_empleado_beneficiario,
                    fec_salida_detalle=:fec_salida_detalle,
                    fec_ingreso_detalle=:fec_ingreso_detalle,
                    num_total_dias_detalle=:num_total_dias_detalle,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
        " );
        $detalleViatico->execute(array(
            ':fk_rhc045_num_viatico' => $fk_rhc045_num_viatico,
            ':fk_rhb001_num_empleado_beneficiario' => $fk_rhb001_num_empleado_beneficiario,
            ':fec_salida_detalle' => $fec_salida_detalle,
            ':fec_ingreso_detalle' => $fec_ingreso_detalle,
            ':num_total_dias_detalle' => $num_total_dias_detalle
        ));


        $pkDetalleViatico = $this->_db->lastInsertId();

        $error = $detalleViatico->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $pkDetalleViatico;
        }


    }

    public function formatFechaAMD($fecha) {
        list($d, $m, $a)=SPLIT( '[/.-]', $fecha);
        if ($fecha == "00-00-0000") return ""; else return "$a-$m-$d";
    }

    public function metListarViaticos($organismo, $dependencia, $estado, $fecha_prepa1, $fecha_prepa2){

        $cad_consulta = "SELECT
                        rh_c045_viatico.pk_num_viatico,
                        rh_c045_viatico.fec_registro_viatico,
                        rh_c045_viatico.fk_rhb001_num_empleado_solicita,
                        rh_c045_viatico.fec_salida,
                        rh_c045_viatico.fec_ingreso,
                        rh_c045_viatico.num_total_dias,
                        rh_c045_viatico.ind_destino,
                        rh_c045_viatico.ind_razon_rechazo,
                        rh_c045_viatico.ind_observacion,
                        rh_c045_viatico.ind_motivo,
                        rh_c045_viatico.cod_interno,
                        rh_c045_viatico.fk_rhb001_num_empleado_preparado_por,
                        rh_c045_viatico.fk_a004_num_dependencia,
                        rh_c045_viatico.fk_a001_num_organismo,
                        rh_c045_viatico.ind_estado,
                        rh_c045_viatico.fk_rhb001_num_empleado_aprobado_por,
                        rh_c045_viatico.fec_aprobacion,
                        rh_c045_viatico.fec_ultima_modificacion,
                        rh_c045_viatico.fk_a018_num_seguridad_usuario,
						a003_persona.ind_nombre1,
						a003_persona.ind_nombre2,
						a003_persona.ind_apellido1,
						a003_persona.ind_apellido2,
						a003_persona.ind_cedula_documento,
						a003_persona.pk_num_persona,
						rh_b001_empleado.pk_num_empleado,
						a004_dependencia.ind_dependencia,
						a004_dependencia.pk_num_dependencia
						FROM
                        rh_c045_viatico
						INNER JOIN  rh_b001_empleado ON  rh_c045_viatico.fk_rhb001_num_empleado_solicita =  rh_b001_empleado.pk_num_empleado
						INNER JOIN   a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c045_viatico.	fk_a004_num_dependencia
						INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        WHERE rh_c045_viatico.fk_a001_num_organismo = '$organismo'  ";


        if(!empty($dependencia)!=''){ //se busca por dependencia tambien
            $cad_consulta = $cad_consulta." AND fk_a004_num_dependencia = '$dependencia'";
        }

        if(!empty($estado)!=''){ //se busca por estado tambien
            $cad_consulta = $cad_consulta." AND ind_estado = '$estado'";
        }

        if(!empty($fecha_prepa1) && !empty($fecha_prepa2)){

           $cad_consulta = $cad_consulta." AND ( fec_registro_viatico >= '".$fecha_prepa1."') AND ( fec_registro_viatico <= '".$fecha_prepa2."')";
        }
		
		$cad_consulta = $cad_consulta." ORDER BY a004_dependencia.pk_num_dependencia";
		
		//echo $cad_consulta;
		
        $listado = $this->_db->query($cad_consulta);
        $listado->setFetchMode(PDO::FETCH_ASSOC);
        $datos = $listado->fetchAll();

        if(is_array($datos)){
            return $datos;
        }else{
            echo -1;
        }


    }
	
	public function metBuscarViatico($pk_num_viatico){


        $cad_consulta = "SELECT
                        rh_c045_viatico.pk_num_viatico,
                        rh_c045_viatico.fec_registro_viatico,
                        rh_c045_viatico.fk_rhb001_num_empleado_solicita,
                        rh_c045_viatico.fec_salida,
                        rh_c045_viatico.fec_ingreso,
                        rh_c045_viatico.num_total_dias,
                        rh_c045_viatico.ind_destino,
                        rh_c045_viatico.ind_razon_rechazo,
                        rh_c045_viatico.ind_observacion,
                        rh_c045_viatico.ind_motivo,
                        rh_c045_viatico.cod_interno,
                        rh_c045_viatico.fk_rhb001_num_empleado_preparado_por,
                        rh_c045_viatico.fk_a004_num_dependencia,
                        rh_c045_viatico.fk_a001_num_organismo,
                        rh_c045_viatico.ind_estado,
                        rh_c045_viatico.fk_rhb001_num_empleado_aprobado_por,
                        rh_c045_viatico.fec_aprobacion,
                        rh_c045_viatico.fec_ultima_modificacion,
                        rh_c045_viatico.fk_a018_num_seguridad_usuario,
                        rh_c045_viatico.num_unidad_combustible,
						a003_persona.ind_nombre1,
						a003_persona.ind_nombre2,
						a003_persona.ind_apellido1,
						a003_persona.ind_apellido2,
						a003_persona.ind_cedula_documento,
						a003_persona.pk_num_persona,
						rh_b001_empleado.pk_num_empleado,
						a004_dependencia.ind_dependencia,
						a004_dependencia.pk_num_dependencia
						FROM
                        rh_c045_viatico
						INNER JOIN  rh_b001_empleado ON  rh_c045_viatico.fk_rhb001_num_empleado_solicita =  rh_b001_empleado.pk_num_empleado
						INNER JOIN  a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c045_viatico.fk_a004_num_dependencia
						INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = rh_c045_viatico.fk_a001_num_organismo
                        WHERE pk_num_viatico = '$pk_num_viatico'";
	
        $listado = $this->_db->query($cad_consulta);
        $listado->setFetchMode(PDO::FETCH_ASSOC);
        $datos = $listado->fetch();

        if(is_array($datos)){
            return $datos;
        }else{
            echo -1;
        }


    }

    public function metBuscarDetalleViatico($pk_num_viatico){

        $empleado = $this->_db->query(
        "SELECT
        pk_num_detalle_viatico,
        fk_rhc045_num_viatico,
        fk_rhb001_num_empleado_beneficiario,
        fec_salida_detalle,
        fec_ingreso_detalle,
        num_total_dias_detalle,
        a003_persona.ind_nombre1,
		a003_persona.ind_nombre2,
		a003_persona.ind_apellido1,
		a003_persona.ind_apellido2,
		a003_persona.pk_num_persona,
		a003_persona.ind_cedula_documento
        FROM rh_c004_detalle_viatico
        INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c004_detalle_viatico.fk_rhb001_num_empleado_beneficiario
        INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
        WHERE fk_rhc045_num_viatico = '$pk_num_viatico'");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();

    }

    public function metConsultarEmpleadoUnico($pk_num_empleado)
    {
        $empleado = $this->_db->query("SELECT c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado, c.pk_num_persona, b.num_estatus FROM rh_b001_empleado AS b, a003_persona AS c
WHERE b.fk_a003_num_persona = c.pk_num_persona AND b.num_estatus =1 AND b.pk_num_empleado = '$pk_num_empleado'");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metBuscarDatosUsuarioSeguridad($num_seguridad)
    {
        $empleado = $this->_db->query("SELECT * FROM `a018_seguridad_usuario` WHERE pk_num_seguridad_usuario = '$num_seguridad'");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metModificarViatico($pk_num_viatico, $fec_salida, $fec_ingreso, $num_total_dias, $ind_destino, $ind_motivo, $fec_ultima_modificacion, $fk_a018_num_seguridad_usuario ){

        $this->_db->beginTransaction();
        $modificacion =  $this->_db->query("
        UPDATE
        `rh_c045_viatico`
        SET `fec_salida`= '$fec_salida',
            `fec_ingreso`= '$fec_ingreso',
            `num_total_dias`= '$num_total_dias',
            `ind_destino`='$ind_destino',
            `ind_motivo`='$ind_motivo',
            `fec_ultima_modificacion`='$fec_ultima_modificacion',
            `fk_a018_num_seguridad_usuario`='$fk_a018_num_seguridad_usuario'
        WHERE
            pk_num_viatico = '$pk_num_viatico'");

        $this->_db->commit();

        return $modificacion;


    }

    public function metEliminarDetalleViatico($pk_num_viatico){

        $this->_db->beginTransaction();
        $respuesta = $this->_db->query(
            "DELETE FROM `rh_c004_detalle_viatico` WHERE  `fk_rhc045_num_viatico` = '$pk_num_viatico'"

        );
        $this->_db->commit();

        return $respuesta;

    }

    public  function metActualizarEstadoViatico($pk_num_viatico, $ind_estado, $razon_rechazo, $observacion, $fec_ultima_modificacion, $fk_a018_num_seguridad_usuario,$unidadCombustible){

        if(strcmp("AP",$ind_estado) == 0){

            $cad = " ind_observacion = '".$observacion."'";

            $fecha = date("Y-m-d");
            $this->_db->beginTransaction();
            $modificacion =  $this->_db->query("
            UPDATE
            rh_c045_viatico
            SET ind_estado= '$ind_estado',
                 ".$cad.",
                 num_unidad_combustible = '$unidadCombustible',
                 fec_aprobacion = '$fecha',
                 fk_rhb001_num_empleado_aprobado_por='$fk_a018_num_seguridad_usuario',
                 fec_ultima_modificacion='$fec_ultima_modificacion',
                 fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
                pk_num_viatico = '$pk_num_viatico'");

                $this->_db->commit();

                return $modificacion;


        }else{

            $cad = " ind_razon_rechazo = '".$razon_rechazo."'";

            $this->_db->beginTransaction();
            $modificacion =  $this->_db->query("
            UPDATE
            rh_c045_viatico
            SET ind_estado= '$ind_estado',
                 ".$cad.",
                 fec_ultima_modificacion='$fec_ultima_modificacion',
                 fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
                pk_num_viatico = '$pk_num_viatico'");

            $this->_db->commit();

            return $modificacion;

        }



    }

    public function metBusquedaUnicaDatosEmpleados($pk_num_empleado)//incluye los datos organizacionales
    {

            $empleado = $this->_db->query("SELECT
                 rh_b001_empleado.pk_num_empleado,
                 a003_persona.ind_nombre1,
                 a003_persona.ind_nombre2,
                 a003_persona.ind_apellido1,
                 a003_persona.ind_apellido2,
                 a003_persona.ind_cedula_documento,
                 rh_c005_empleado_laboral.fec_ingreso,
                 rh_c006_tipo_cargo.txt_descripcion,
                 rh_c009_nivel.num_nivel,
                 rh_c007_grado_salarial.ind_grado,
                a001_organismo.pk_num_organismo,
                a001_organismo.ind_descripcion_empresa,
                a004_dependencia.pk_num_dependencia,
                a004_dependencia.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo
                FROM a003_persona
                INNER JOIN  rh_b001_empleado ON  rh_b001_empleado.fk_a003_num_persona =  a003_persona.pk_num_persona
                INNER JOIN  rh_c005_empleado_laboral ON   rh_c005_empleado_laboral.fk_rhb001_num_empleado  = rh_b001_empleado.pk_num_empleado
                INNER JOIN  rh_c063_puestos ON  rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                INNER JOIN  rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
                INNER JOIN  rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
                INNER JOIN  rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
                INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = rh_c076_empleado_organizacion.fk_a001_num_organismo
                INNER JOIN  a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
                WHERE rh_b001_empleado.pk_num_empleado = '$pk_num_empleado' ");

            $empleado->setFetchMode(PDO::FETCH_ASSOC);
            return $empleado->fetch();

        }




}//fin de la clase
?>