<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO:  Gestión de Permisos / Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de permisos por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php'; 
class permisoModelo extends miscelaneoModelo
{
    public function __construct() 
    {
        parent::__construct();
    }

    // Método que muestra el listado de permisos solicitados por el funcionario
    public function metListarPermisoUsuario($pkNumEmpleado)
    {
        $listarPermiso =  $this->_db->query(
            "select a.pk_num_permiso, a.fec_hora_salida, a.fec_hora_entrada, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_registro, a.ind_motivo, date_format(a.fec_salida, '%d/%m/%Y') as fecha_salida, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, b.ind_estado, d.ind_nombre1, d.ind_nombre2, d.ind_apellido1, d.ind_apellido2, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_motivo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='MOTAUS') as motivo_ausencia, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_tipo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='TIPAUS') as tipo_ausencia from rh_c036_permiso as a, rh_c037_operacion_permiso as b, rh_b001_empleado as c, a003_persona as d where a.pk_num_permiso=b.fk_rhc036_num_permiso and b.num_estatus=1 and a.fk_rhb001_num_empleado='$pkNumEmpleado' and a.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona order by a.pk_num_permiso desc"
        );
        $listarPermiso->setFetchMode(PDO::FETCH_ASSOC);
        return $listarPermiso->fetchAll();
    }

    // Método encargado de listar los funcionarios de la Contraloría
    public function metListarFuncionario()
    {
        $funcionario =  $this->_db->query(
                "SELECT  b.pk_num_empleado,a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2 FROM a003_persona as a, rh_b001_empleado as b where b.fk_a003_num_persona=a.pk_num_persona and b.num_estatus=1"
            );
        $funcionario->setFetchMode(PDO::FETCH_ASSOC);
        return $funcionario->fetchAll();
    }

    public function metListarEmpleado($pkNumDependencia)
    {
        $empleado =  $this->_db->query(
                "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b, rh_c076_empleado_organizacion as c where a.fk_a003_num_persona=b.pk_num_persona and c.fk_a004_num_dependencia=$pkNumDependencia and c.fk_rhb001_num_empleado=a.pk_num_empleado and 	a.num_estatus=1"
            );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite listar a los funcionarios responsables de las dependencias de la Contraloría
    public function metListarResponsables()
    {
        $listarResponsables = $this->_db->query(
            "select b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, c.pk_num_empleado from a004_dependencia as a, a003_persona as b, rh_b001_empleado as c where a.fk_a003_num_persona_responsable=b.pk_num_persona and c.fk_a003_num_persona=b.pk_num_persona"
        );
        $listarResponsables->setFetchMode(PDO::FETCH_ASSOC);
        return $listarResponsables->fetchAll();
    }

    // Director, jefe o persona responsable actualmente de la dependencia a la cual pertenece el funcionario
    public function metApruebaFuncionario($pkNumEmpleado)
    {
        $aprueba = $this->_db->query(
            "select b.pk_num_dependencia, b.ind_dependencia, c.pk_num_empleado, d.ind_nombre1, d.ind_nombre2, d.ind_apellido1, d.ind_apellido2 from rh_c076_empleado_organizacion as a, a004_dependencia as b, rh_b001_empleado as c, a003_persona as d where a.fk_rhb001_num_empleado='$pkNumEmpleado' and a.fk_a004_num_dependencia=b.pk_num_dependencia and b.fk_a003_num_persona_responsable=c.fk_a003_num_persona and c.fk_a003_num_persona=d.pk_num_persona"
        );
        $aprueba->setFetchMode(PDO::FETCH_ASSOC);
       return $aprueba->fetch();
    }

    // Método que permite consultar el horario laboral del empleado solicitante
    public function metConsultarHorario($pkNumEmpleado)
    {
        $consultarHorario =  $this->_db->query(
            "select b.pk_num_horario, c.fec_entrada1, c.fec_salida1, c.fec_entrada2, c.fec_salida2 from rh_c005_empleado_laboral as a, rh_b007_horario_laboral as b, rh_c067_horario_detalle as c where a.fk_rhb001_num_empleado='$pkNumEmpleado' and a.fk_rhb007_num_horario=b.pk_num_horario and b.pk_num_horario=c.fk_rhb007_num_horario"
        );
        $consultarHorario->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarHorario->fetch();
    }

    // Método que permite obtener los datos de un horario en particular
    public function metHorario($fk_rhb007_num_horario)
    {
        $obtenerHorario =  $this->_db->query(
            "select a.pk_num_horario, b.fec_entrada1, b.fec_salida1, b.fec_entrada2, b.fec_salida2 from rh_b007_horario_laboral as a, rh_c067_horario_detalle as b where a.pk_num_horario=b.fk_rhb007_num_horario and a.pk_num_horario='$fk_rhb007_num_horario'"
        );
        $obtenerHorario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerHorario->fetch();
    }

    // Método que permite guardar los permisos solicitados
    public function metGuardarPermiso($pk_num_empleado, $pk_num_empleado_aprueba, $motivo_ausencia, $tipo_ausencia, $periodoContable, $fechaInicio, $fechaFin, $horaInicio, $horaFin, $motivo, $remunerado, $pk_num_horario, $entrada,  $totalDias, $totalHoras, $totalMinutos, $dependencia, $cargo, $fecha_hora, $usuario)
    {
        $this->_db->beginTransaction();
        $NuevoPermiso = $this->_db->prepare(
            "insert into rh_c036_permiso values (null, :fk_a006_num_miscelaneo_motivo_ausencia, :fk_a006_num_miscelaneo_tipo_ausencia, :fk_rhb001_num_empleado, :fk_rhb007_num_horario, :fk_rhb001_num_empleado_aprueba, :fk_a004_num_dependencia, :fk_rhc063_num_puesto, :ind_periodo_contable, :fec_salida, :fec_entrada, :fec_hora_salida, :fec_hora_entrada, :num_total_dia, :num_total_hora, :num_total_minuto, :ind_motivo, :num_remuneracion, null, null, :fec_ingreso, :ind_observacion_aprobacion, :ind_observacion_verificacion, :fec_ultima_modificacion, :fk_a018_num_seguridad_usuario)"
        );
        $NuevoPermiso->execute(array(
            ':fk_a006_num_miscelaneo_motivo_ausencia' => $motivo_ausencia,
            ':fk_a006_num_miscelaneo_tipo_ausencia' => $tipo_ausencia,
            ':fk_rhb001_num_empleado' => $pk_num_empleado,
            ':fk_rhb007_num_horario'=> $pk_num_horario,
            ':fk_rhb001_num_empleado_aprueba' => $pk_num_empleado_aprueba,
            ':fk_a004_num_dependencia' => $dependencia,
            ':fk_rhc063_num_puesto' => $cargo,
            ':ind_periodo_contable' => $periodoContable,
            ':fec_salida' => $fechaInicio,
            ':fec_entrada' => $fechaFin,
            ':fec_hora_salida' => $horaInicio,
            ':fec_hora_entrada' => $horaFin,
            ':num_total_dia' => $totalDias,
            ':num_total_hora' => $totalHoras,
            ':num_total_minuto' => $totalMinutos,
            ':ind_motivo' => $motivo,
            ':num_remuneracion' => $remunerado,
            ':fec_ingreso'=> $entrada,
            ':ind_observacion_aprobacion' => ' ',
            ':ind_observacion_verificacion'=>' ',
            ':fec_ultima_modificacion' => $fecha_hora,
            ':fk_a018_num_seguridad_usuario' => $usuario
        ));
        $pkNumPermiso = $this->_db->lastInsertId();
        $nuevaOperacion = $this->_db->prepare(
            "insert into rh_c037_operacion_permiso values (null, :ind_estado, :fk_rhc036_num_permiso, :fk_rhb001_num_empleado, :num_estatus, :fec_operacion)"
        );
        $nuevaOperacion->execute(array(
            ':ind_estado' => 'PR',
            ':fk_rhc036_num_permiso' => $pkNumPermiso,
            ':fk_rhb001_num_empleado' => $pk_num_empleado,
            ':num_estatus' => 1,
            ':fec_operacion' => $fecha_hora
        ));
        $this->_db->commit();
        return $pkNumPermiso;
    }

    // Método que permite ver un determinado permiso
    public function metVerPermiso($pkNumPermiso, $metodo)
    {
        $verPermiso =  $this->_db->query(
            "select a.pk_num_permiso, a.fk_rhb007_num_horario, a.fk_rhb001_num_empleado_aprueba, a.ind_observacion_aprobacion, a.ind_observacion_verificacion, a.fk_a006_num_miscelaneo_motivo_ausencia, a.fk_a006_num_miscelaneo_tipo_ausencia, a.ind_periodo_contable, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, date_format(a.fec_salida, '%d/%m/%Y') as fecha_salida, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, a.ind_motivo, a.num_total_dia, a.num_total_hora, a.num_total_minuto, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_motivo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='MOTAUS') as motivo_ausencia, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_tipo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='TIPAUS') as tipo_ausencia, a.fec_hora_salida, a.fec_hora_entrada, a.num_remuneracion, a.num_justificativo, a.num_exento, c.pk_num_empleado, b.ind_estado, d.ind_cedula_documento, d.ind_nombre1, d.ind_nombre2, d.ind_apellido1, d.ind_apellido2, e.ind_descripcion_cargo, f.ind_dependencia from rh_c036_permiso as a, rh_c037_operacion_permiso as b, rh_b001_empleado as c, a003_persona as d, rh_c063_puestos as e, a004_dependencia as f where a.pk_num_permiso='$pkNumPermiso' and a.pk_num_permiso=b.fk_rhc036_num_permiso and b.num_estatus=1 and a.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.fk_rhc063_num_puesto=e.pk_num_puestos and a.fk_a004_num_dependencia=f.pk_num_dependencia"
        );
        $verPermiso->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verPermiso->fetch();
        } else {
            return $verPermiso->fetchAll();
        }
    }

    // Método que muestra el listado de responsables de las dependencias
    public function metFuncionarioAprueba($pkNumPermiso)
    {
        $funcionarioAprueba = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from rh_c036_permiso as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_permiso='$pkNumPermiso' and a.fk_rhb001_num_empleado_aprueba=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $funcionarioAprueba->setFetchMode(PDO::FETCH_ASSOC);
        return $funcionarioAprueba->fetchAll();
    }

    public function metFuncionarioApruebaPermiso($pkNumPermiso)
    {
        $funcionarioAprueba = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from rh_c036_permiso as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_permiso='$pkNumPermiso' and a.fk_rhb001_num_empleado_aprueba=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $funcionarioAprueba->setFetchMode(PDO::FETCH_ASSOC);
        return $funcionarioAprueba->fetch();
    }



    // Método que permite ver el último acceso de un determinado permiso
    public function metAcceso($pkNumPermiso)
    {
        $acceso = $this->_db->query(
            "select b.ind_usuario, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%m:%S') as fecha_modificacion, c.pk_num_empleado from rh_c036_permiso as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.pk_num_permiso='$pkNumPermiso' and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona
            "
        );
        $acceso->setFetchMode(PDO::FETCH_ASSOC);
        return $acceso->fetchAll();
    }

    // Método que muestra al funcionario responsable de la dependencia a la cual pertenece el empleado
    public function metAprueba($pkNumEmpleado)
    {
        $aprueba = $this->_db->query(
            "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b where a.fk_a003_num_persona=b.pk_num_persona and a.pk_num_empleado='$pkNumEmpleado'"
        );
        $aprueba->setFetchMode(PDO::FETCH_ASSOC);
        return $aprueba->fetch();
    }

    // Método que muestra al funcionario responsable de la dependencia a la cual pertenece el empleado
    public function metApruebaPermiso($pkNumEmpleado)
    {
        $aprueba = $this->_db->query(
            "select b.fk_a003_num_persona_responsable from vl_rh_persona_empleado_datos_laborales as a, a004_dependencia as b where a.pk_num_empleado=$pkNumEmpleado and a.fk_a004_num_dependencia=b.pk_num_dependencia"
        );
        $aprueba->setFetchMode(PDO::FETCH_ASSOC);
        return $aprueba->fetch();
    }

    // Método que muestra al funcionario responsable de la dependencia a la cual pertenece el empleado
    public function metResponsable($pkNumPersona)
    {
        $aprueba = $this->_db->query(
            "select pk_num_empleado from vl_rh_persona_empleado_datos_laborales where pk_num_persona=$pkNumPersona"
        );
        $aprueba->setFetchMode(PDO::FETCH_ASSOC);
        return $aprueba->fetch();
    }

    // Método que permite editar el permiso
    public function metEditarPermiso($pkNumPermiso, $tipo_ausencia, $motivo_ausencia, $fechaInicio, $fechaFin, $horaInicio, $horaFin, $ind_motivo, $totalDias, $totalHoras, $totalMinutos, $usuario, $fecha_hora, $pk_num_empleado_aprueba)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update rh_c036_permiso set fk_a006_num_miscelaneo_motivo_ausencia='$motivo_ausencia', fk_a006_num_miscelaneo_tipo_ausencia='$tipo_ausencia', fec_salida='$fechaInicio', 	fec_entrada='$fechaFin', fec_hora_salida='$horaInicio', fec_hora_entrada='$horaFin', num_total_dia='$totalDias', num_total_hora='$totalHoras', num_total_minuto='$totalMinutos', ind_motivo='$ind_motivo', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario='$usuario', fk_rhb001_num_empleado_aprueba='$pk_num_empleado_aprueba' where pk_num_permiso=$pkNumPermiso"
        );
        $this->_db->commit();
    }

    // Método que permite ver el listado de permisos solicitados por los funcionarios de una dependencia en particular
    public function metListadoPermiso($idDependencia, $estado = false)
    {

        if($estado!=''){
            $filtro = " and b.ind_estado='$estado'";
        } else {
            $filtro = " ";
        }
        $listarPermiso =  $this->_db->query(
            "select a.pk_num_permiso, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_registro, a.ind_motivo, date_format(a.fec_salida, '%d/%m/%Y') as fecha_salida, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, b.ind_estado, d.ind_nombre1, d.ind_nombre2, d.ind_apellido1, d.ind_apellido2, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_motivo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='MOTAUS') as motivo_ausencia, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_tipo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='TIPAUS') as tipo_ausencia, f.ind_dependencia from rh_c036_permiso as a, rh_c037_operacion_permiso as b, rh_b001_empleado as c, a003_persona as d, rh_c076_empleado_organizacion as e, a004_dependencia as f where a.pk_num_permiso=b.fk_rhc036_num_permiso and b.num_estatus=1 and a.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and c.pk_num_empleado=e.fk_rhb001_num_empleado and e.fk_a004_num_dependencia='$idDependencia' and f.pk_num_dependencia=e.fk_a004_num_dependencia $filtro order by a.pk_num_permiso desc"
        );
        $listarPermiso->setFetchMode(PDO::FETCH_ASSOC);
        return $listarPermiso->fetchAll();
    }

    public function metDependenciaEmpleado($pkNumEmpleado)
    {
        $dependenciaEmpleado = $this->_db->query(
            "select a.pk_num_empleado, b.fk_a004_num_dependencia, c.ind_dependencia, c.pk_num_dependencia, e.pk_num_puestos, e.ind_descripcion_cargo, f.pk_num_organismo, f.ind_descripcion_empresa from rh_b001_empleado as a, rh_c076_empleado_organizacion as b, a004_dependencia as c, rh_c005_empleado_laboral as d, rh_c063_puestos as e, a001_organismo as f where a.pk_num_empleado=b.fk_rhb001_num_empleado and a.pk_num_empleado=$pkNumEmpleado and b.fk_a004_num_dependencia=c.pk_num_dependencia and d.fk_rhc063_num_puestos_cargo=e.pk_num_puestos and b.fk_a001_num_organismo=f.pk_num_organismo and a.pk_num_empleado=d.fk_rhb001_num_empleado"
        );
        $dependenciaEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $dependenciaEmpleado->fetch();
    }

    // Método que permite cambiar el estatus de un permiso
    public function metPermisoEstatus($pkNumPermiso, $num_remunerado, $num_justificativo, $num_exento, $campo, $usuario, $fecha_hora, $valor)
    {
        $this->_db->beginTransaction();
        if($valor=='AP'){
            $this->_db->query(
                "update rh_c036_permiso set ind_observacion_aprobacion='$campo', num_remuneracion='$num_remunerado', num_justificativo='$num_justificativo', num_exento='$num_exento', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario='$usuario' where pk_num_permiso='$pkNumPermiso'"
            );
        } else {
            $this->_db->query(
                "update rh_c036_permiso set ind_observacion_verificacion='$campo', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario='$usuario' where pk_num_permiso='$pkNumPermiso'"
            );
        }
        $this->_db->commit();
    }

    // Método que permite actualizar el estatus de las operaciones del permiso
    public function metEstatus($valor, $pkNumPermiso, $fecha_hora, $pkNumEmpleado, $valorAnterior)
    {
        $this->_db->beginTransaction();
        if($valor=='AN'){
            if($valorAnterior=='VE'){
                $this->_db->query(
                    "delete from rh_c037_operacion_permiso where fk_rhc036_num_permiso='$pkNumPermiso' and ind_estado='VE'"
                );
                $this->_db->query(
                    "update rh_c037_operacion_permiso set num_estatus=1 where fk_rhc036_num_permiso='$pkNumPermiso' and ind_estado='AP'"
                );
            }
            if($valorAnterior=='AP'){
                $this->_db->query(
                    "delete from rh_c037_operacion_permiso where fk_rhc036_num_permiso='$pkNumPermiso' and ind_estado='AP'"
                );
                $this->_db->query(
                    "update rh_c037_operacion_permiso set num_estatus=1 where fk_rhc036_num_permiso='$pkNumPermiso' and ind_estado='PR'"
                );
            }
            if($valorAnterior=='PR'){
                $this->_db->query(
                    "update rh_c037_operacion_permiso set num_estatus=0 where fk_rhc036_num_permiso='$pkNumPermiso'"
                );
                $cambiarEstatus=$this->_db->prepare(
                    "insert into rh_c037_operacion_permiso values (null, :ind_estado, :fk_rhc036_num_permiso, :fk_rhb001_num_empleado, :num_estatus, :fec_operacion)"
                );
                $cambiarEstatus->execute(array(
                    ':ind_estado' => $valor,
                    ':fk_rhc036_num_permiso'=> $pkNumPermiso,
                    ':fk_rhb001_num_empleado' => $pkNumEmpleado,
                    ':num_estatus' => 1,
                    ':fec_operacion' => $fecha_hora
                ));
            }

        } else {
            $this->_db->query(
                "update rh_c037_operacion_permiso set num_estatus=0 where fk_rhc036_num_permiso='$pkNumPermiso'"
            );
            $cambiarEstatus=$this->_db->prepare(
                "insert into rh_c037_operacion_permiso values (null, :ind_estado, :fk_rhc036_num_permiso, :fk_rhb001_num_empleado, :num_estatus, :fec_operacion)"
            );
            $cambiarEstatus->execute(array(
                ':ind_estado' => $valor,
                ':fk_rhc036_num_permiso'=> $pkNumPermiso,
                ':fk_rhb001_num_empleado' => $pkNumEmpleado,
                ':num_estatus' => 1,
                ':fec_operacion' => $fecha_hora
            ));
        }
        $this->_db->commit();
    }

    // Método que permite ver el estatus del permiso
    public function metVerEstatus($pkNumPermiso, $valor)
    {
        $estatus = $this->_db->query(
            "select date_format(a.fec_operacion, '%d/%m/%Y') as fecha_operacion, b.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from rh_c037_operacion_permiso as a, rh_b001_empleado as b, a003_persona as c where a.fk_rhc036_num_permiso='$pkNumPermiso' and a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and a.ind_estado='$valor'"
        );
        $estatus->setFetchMode(PDO::FETCH_ASSOC);
        return $estatus->fetch();
    }

    // Método que permite obtener los resultados de la búsqueda de permisos mediante el filtro
    public function metBuscarPermiso($pk_num_organismo, $pk_num_dependencia, $pk_num_empleado, $fechaInicio, $fechaFin, $pk_num_permiso, $ind_estado, $metodo){
        if ($pk_num_organismo!='') {
            $filtro = "and e.fk_rhb001_num_empleado=c.pk_num_empleado and e.fk_a001_num_organismo=f.pk_num_organismo and  f.pk_num_organismo=$pk_num_organismo";
        }
        if ($pk_num_dependencia!='') {
            $filtro .= " and e.fk_a004_num_dependencia=g.pk_num_dependencia and g.pk_num_dependencia=$pk_num_dependencia";
        }
        if ($pk_num_empleado!='') {
            $filtro .= " and c.pk_num_empleado=$pk_num_empleado";
        }
        if ($pk_num_permiso!='') {
            $filtro .= " and a.pk_num_permiso=$pk_num_permiso";
        }
        if (($fechaInicio!='')&&($fechaFin!='')) {
            $fechaExplodeInicio = explode("/", $fechaInicio);
            $fecha_inicio = $fechaExplodeInicio[2].'-'.$fechaExplodeInicio[1].'-'.$fechaExplodeInicio[0];
            // Fecha Fin
            $fechaExplodeFin = explode("/", $fechaFin);
            $fecha_fin = $fechaExplodeFin[2].'-'.$fechaExplodeFin[1].'-'.$fechaExplodeFin[0];
            $filtro .= " and a.fec_ingreso between '$fecha_inicio' and '$fecha_fin'";
        }
        if ($ind_estado!='') {
            $filtro .= " and b.ind_estado='$ind_estado'";
        }
        $buscarPermiso = $this->_db->query(
            "select a.pk_num_permiso, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_registro, a.ind_motivo, date_format(a.fec_salida, '%d/%m/%Y') as fecha_salida, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, b.ind_estado, d.ind_nombre1, d.ind_nombre2, d.ind_apellido1, d.ind_apellido2, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_motivo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='MOTAUS') as motivo_ausencia, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_tipo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='TIPAUS') as tipo_ausencia from rh_c036_permiso as a, rh_c037_operacion_permiso as b, rh_b001_empleado as c, a003_persona as d, rh_c076_empleado_organizacion as e, a001_organismo as f, a004_dependencia as g where a.pk_num_permiso=b.fk_rhc036_num_permiso and b.num_estatus=1 and a.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona $filtro group by a.pk_num_permiso order by a.pk_num_permiso desc"
        );
        $buscarPermiso->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $buscarPermiso->fetch();
        } else {
            return $buscarPermiso->fetchAll();
        }
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo WHERE ind_tipo_organismo='I'"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }

    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite obtener los datos del empleado
    public function metBuscarEmpleado($pkNumEmpleado)
    {
        $obtenerEmpleado = $this->_db->query(
            "select b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, a.pk_num_empleado from rh_b001_empleado as a, a003_persona as b where a.fk_a003_num_persona=b.pk_num_persona and a.pk_num_empleado=$pkNumEmpleado and num_estatus=1"
        );
        $obtenerEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerEmpleado->fetch();
    }

    //Método que permite obtener los dias feriados dentro de un rango de fechas
    public function metFeriado($fechaComparar)
    {
        //Formato para la base de datoss
        $fechaExplode = explode("-", $fechaComparar);
        $anio = $fechaExplode[0];
        $mesDia = $fechaExplode[1].'-'.$fechaExplode[2];
        $diaFeriado = $this->_db->query(
            "select count(pk_num_feriado) as feriado from rh_c069_feriados where num_estatus=1 and fec_anio=$anio and fec_dia='$mesDia'"
        );
        $diaFeriado->setFetchMode(PDO::FETCH_ASSOC);
        return $diaFeriado->fetch();
    }

    public function metValidarEmpleado()
    {
        $empleado =  $this->_db->query(
            "select pk_num_empleado, ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from vl_rh_persona_empleado where ind_estatus_empleado=1"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }
    

}// fin de la clase