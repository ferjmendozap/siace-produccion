<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class vacacionesModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.pk_num_empleado, c.ind_nombre_detalle from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b. 	fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        }
    }

    // Método que permite listar empleados de acuerdo a la busqueda
    public function metListadoEmpleados($pkNumOrganismo, $pkNumDependencia)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro = " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        $empleado = $this->_db->query(
                "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.fec_nacimiento, b.fec_ingreso, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, c.ind_dependencia, d.ind_descripcion_cargo, date_format(b.fec_ingreso, '%m') as mes_ingreso from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a004_dependencia as c, rh_c063_puestos as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a004_num_dependencia=c.pk_num_dependencia and b.fk_rhc063_num_puestos_cargo=d.pk_num_puestos $filtro"
            );

        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    // Método que permite listar las vacaciones de los empleados
    public function metListadoVacacion($pkNumEmpleado, $fechaInicio, $fechaFin, $estado)
    {
        if (($fechaInicio!='')&&($fechaFin!='')) {
            $filtro = " and a.fec_solicitud between '$fechaInicio' and '$fechaFin'";
        }
        if ($estado!='') {
            switch($estado){
                case 1:
                    $valor = 'PR';
                    break;
                case 2:
                    $valor = 'RE';
                    break;
                case 3:
                    $valor = 'CO';
                    break;
                case 4:
                    $valor = 'AP';
                    break;
                case 5:
                    $valor = 'AN';
                    break;
            }
            $filtro = " and f.ind_estado='$valor'";
        }
        $listarVacacion = $this->_db->query(
            "select a.pk_num_solicitud_vacacion, a.fec_solicitud, date_format(a.fec_solicitud, '%d/%m/%Y') as fecha_solicitud, date_format(a.fec_salida, '%d/%m/%Y') as fecha_salida, date_format(a.fec_termino, '%d/%m/%Y') as fecha_termino, date_format(a.fec_incorporacion, '%d/%m/%Y') as fecha_incorporacion, a.num_dias_solicitados, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, e.ind_nombre_detalle, f.ind_estado, g.num_anio from rh_b009_vacacion_solicitud as a, rh_b001_empleado as b, a003_persona as c, a005_miscelaneo_maestro as d, a006_miscelaneo_detalle as e, rh_c080_operacion_vacacion as f, rh_c081_periodo_vacacion as g, rh_c079_vacacion_detalle as h where a.fk_rhb001_num_empleado=b.pk_num_empleado and b.pk_num_empleado=$pkNumEmpleado and b.fk_a003_num_persona=c.pk_num_persona and d.cod_maestro='TIPVAC' and d.pk_num_miscelaneo_maestro=e.fk_a005_num_miscelaneo_maestro and e.pk_num_miscelaneo_detalle=a.fk_a006_tipo_vacacion and a.pk_num_solicitud_vacacion=f.fk_rhb009_num_solicitud_vacacion and f.num_estatus=1 and h.fk_rhc081_num_periodo_vacacion=g.pk_num_periodo $filtro group by a.pk_num_solicitud_vacacion, f.ind_estado, g.num_anio"
        );
        $listarVacacion->setFetchMode(PDO::FETCH_ASSOC);
        return  $listarVacacion->fetchAll();
    }

    // Método que permite manejar el detalle de la solicitud de vacaciones
    public function metMostrarDetalleVacacion($pkNumSolicitudVacacion)
    {
        $detalle =  $this->_db->query(
            "select date_format(a.fec_solicitud, '%d/%m/%Y') as fecha_solicitud, b.fk_rhc081_num_periodo_vacacion, date_format(b.fec_inicial, '%d/%m/%Y') as fecha_inicial, date_format(b.fec_final, '%d/%m/%Y') as fecha_final, date_format(b.fec_incorporacion, '%d/%m/%Y') as fecha_incorporacion, b.num_dias_detalle, b.num_dias_derecho, b.num_dias_pendientes, c.num_anio  from rh_b009_vacacion_solicitud as a, rh_c079_vacacion_detalle as b, rh_c081_periodo_vacacion as c where a.pk_num_solicitud_vacacion=$pkNumSolicitudVacacion and a.pk_num_solicitud_vacacion=b.fk_rhb009_num_solicitud_vacacion and b.fk_rhc081_num_periodo_vacacion=c.pk_num_periodo"
        );
        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetchAll();
    }
}// fin de la clase

