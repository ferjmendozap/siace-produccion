<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Resumen de Eventos. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class resumenEventoModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.pk_num_empleado, c.ind_nombre_detalle from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b. 	fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        }
    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListadoEmpleados($pkNumOrganismo)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        $empleado = $this->_db->query(
            "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.fec_nacimiento, b.fec_ingreso, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, c.ind_dependencia, d.ind_descripcion_cargo, date_format(b.fec_ingreso, '%m') as mes_ingreso from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a004_dependencia as c, rh_c063_puestos as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a004_num_dependencia=c.pk_num_dependencia and b.fk_rhc063_num_puestos_cargo=d.pk_num_puestos $filtro"
        );

        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    // Método que permite listar los cargos
    public function metListarCargos()
    {
        $cargo =  $this->_db->query(
            "select pk_num_puestos, ind_descripcion_cargo from rh_c063_puestos"
        );
        $cargo->setFetchMode(PDO::FETCH_ASSOC);
        return $cargo->fetchAll();
    }

    // Método que permite seleccionar los periodos aperturados del beneficio de alimentacion
    public function metListarPeriodo()
    {
        $periodo =  $this->_db->query(
            "select fec_anio from rh_c019_beneficio_alimentacion group by fec_anio"
        );
        $periodo->setFetchMode(PDO::FETCH_ASSOC);
        return $periodo->fetchAll();
    }

    //Método que permite listar los meses del periodo correspondiente
    public function metListarMesPeriodo($fec_anio)
    {
        $mesPeriodo =  $this->_db->query(
            "select pk_num_beneficio, ind_periodo, nm_b001_tipo_nomina.ind_nombre_nomina from rh_c019_beneficio_alimentacion INNER JOIN nm_b001_tipo_nomina ON rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina where fec_anio=$fec_anio"
        );
        $mesPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $mesPeriodo->fetchAll();
    }

    //Método que permite listar los dias del periodo correspondiente
    public function metObtenerFechasPeriodo($anio, $mes, $orden)
    {
        if($orden==1){
            $valor = 'asc';
        } else {
            $valor = 'desc';
        }
        $periodo =  $this->_db->query(
            "select date_format(fec_fecha, '%d/%m/%Y') as fecha from rh_c041_beneficio_alimentacion_eventos where year(fec_fecha)=$anio and month(fec_fecha)='$mes' order by pk_num_beneficio_alimentacion_eventos $valor limit 1"
        );
        $periodo->setFetchMode(PDO::FETCH_ASSOC);
        return $periodo->fetch();
    }

    // Método que permite listar los centros de costos
    public function metListarCentroCosto($pkNumOrganismo, $pkNumDependencia)
    {
        if($pkNumDependencia!=0){
            $centroCosto =  $this->_db->query(
                "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo where fk_a004_num_dependencia=$pkNumDependencia"
            );
        } else {
            $centroCosto =  $this->_db->query(
                "select a.pk_num_centro_costo, a.ind_descripcion_centro_costo from a023_centro_costo as a, a004_dependencia as b, a001_organismo as c where a.fk_a004_num_dependencia=b.pk_num_dependencia and b.fk_a001_num_organismo=c.pk_num_organismo and c.pk_num_organismo=$pkNumOrganismo"
            );
        }
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

    //Método que permite obtener el ultimo beneficio registrado
    public function metObtenerUltimoBeneficio($pkNumOrganismo)
    {
        $beneficio =  $this->_db->query(
            "select fec_horas_diarias, fec_horas_semanales, num_dias_periodo, num_dias_pago, num_total_feriados, num_valor_semanal, num_valor_mes, num_valor_diario, pk_num_beneficio, fec_inicio_periodo, fec_fin_periodo, date_format(fec_inicio_periodo, '%d/%m/%Y') as fecha_inicio,  date_format(fec_fin_periodo, '%d/%m/%Y') as fecha_fin from rh_c019_beneficio_alimentacion where fk_a001_num_organismo=$pkNumOrganismo order by pk_num_beneficio desc limit 1 "
        );
        $beneficio->setFetchMode(PDO::FETCH_ASSOC);
        return $beneficio->fetch();
    }

    // Método que permite listar  los empleados
    public function metListarEmpleado($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $fechaInicio, $fechaFin)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if($pkNumDependencia!=''){
            $filtro = " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($centroCosto!=''){
            $filtro .= " and b.fk_a023_num_centro_costo=$centroCosto";
        }
        if($tipoNomina!=''){
            $filtro .= " and b.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if($cargo!=''){
            $filtro .= " and b.fk_rhc063_num_puestos_cargo=$cargo";
        }
        if($edoReg!=''){
            $filtro .= " and b.ind_estatus=$edoReg";
        }
        if($sitTrab!=''){
            $filtro .= " and b.ind_estatus=$sitTrab";
        }
        if($pkNumEmpleado!=''){
            $filtro .= " and b.pk_num_empleado=$pkNumEmpleado";
        }
        if(($fechaInicio!='')&&($fechaFin!='')){
            $filtro .= " and a.fec_fecha between '$fechaInicio' and '$fechaFin'";
        }
        $listarEmpleado =  $this->_db->query(
            "select  b.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_c041_beneficio_alimentacion_eventos as a, vl_rh_persona_empleado_datos_laborales as b where  a.fk_rhb001_num_empleado=b.pk_num_empleado $filtro group by b.pk_num_empleado order by b.pk_num_empleado"
        );
        $listarEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEmpleado->fetchAll();
    }

    // Método utilizado para obtener la fecha del periodo seleccionado
    public function metObtenerPeriodo($periodo, $mes_periodo,$tipo_nomina=false)
    {
        $periodoTotal = $periodo.'-'.$mes_periodo;
        if($tipo_nomina){
            $obtenerPeriodo =  $this->_db->query(
                "select pk_num_beneficio,num_monto_descuento,ind_descripcion_descuento,fec_inicio_periodo, fec_fin_periodo, date_format(fec_inicio_periodo, '%d/%m/%Y') as fechaI, date_format(fec_fin_periodo, '%d/%m/%Y') as fechaF, fec_horas_diarias, fec_horas_semanales, num_valor_semanal, num_valor_diario, num_valor_mes, num_dias_pago, num_total_feriados, num_dias_periodo from rh_c019_beneficio_alimentacion where ind_periodo='$periodoTotal' and fk_nmb001_num_tipo_nomina='$tipo_nomina'"
            );
        }else{
            $obtenerPeriodo =  $this->_db->query(
                "select pk_num_beneficio,num_monto_descuento,ind_descripcion_descuento,fec_inicio_periodo, fec_fin_periodo, date_format(fec_inicio_periodo, '%d/%m/%Y') as fechaI, date_format(fec_fin_periodo, '%d/%m/%Y') as fechaF, fec_horas_diarias, fec_horas_semanales, num_valor_semanal, num_valor_diario, num_valor_mes, num_dias_pago, num_total_feriados, num_dias_periodo from rh_c019_beneficio_alimentacion where ind_periodo='$periodoTotal'"
            );
        }

        $obtenerPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerPeriodo->fetch();
    }

    // Método utilizado para obtener la fecha del periodo seleccionado
    public function metEventoEmpleado($pkNumEmpleado, $fechaISemana, $fechaFSemana)
    {
        $evento =  $this->_db->query(
            "select date_format(a.fec_fecha, '%d/%m/%Y') as fecha, a.fec_hora_salida, a.fec_hora_entrada, a.fec_total_horas, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_motivo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='MOTAUS') as motivo_ausencia, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_tipo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='TIPAUS') as tipo_ausencia, a.txt_observaciones from rh_c041_beneficio_alimentacion_eventos as a where  fk_rhb001_num_empleado=$pkNumEmpleado and fec_fecha between '$fechaISemana' and '$fechaFSemana'"
        );
        $evento->setFetchMode(PDO::FETCH_ASSOC);
        return $evento->fetchAll();
    }

    // Método utilizado para obtener la fecha del periodo seleccionado
    public function metObtenerDetalleBeneficio($periodo, $mes_periodo, $pkNumEmpleado)
    {
        $periodoTotal = $periodo.'-'.$mes_periodo;
        $obtenerBeneficio =  $this->_db->query(
            "select * from rh_c019_beneficio_alimentacion as a, rh_c039_beneficio_alimentacion_detalle as b where a.pk_num_beneficio=b.fk_rhc019_num_beneficio where a.ind_periodo='$periodoTotal' and b.fk_rhb001_num_empleado=$pkNumEmpleado"
        );
        $obtenerBeneficio->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerBeneficio->fetchAll();
    }

    //Método que permite obtener los dias feriados dentro de un rango de fechas
    public function metFeriado($fechaComparar)
    {
        //Formato para la base de datoss
        $fechaExplode = explode("-", $fechaComparar);
        $anio = $fechaExplode[0];
        $mesDia = $fechaExplode[1].'-'.$fechaExplode[2];
        $diaFeriado = $this->_db->query(
            "select count(pk_num_feriado) as feriado from rh_c069_feriados where num_estatus=1 and fec_anio=$anio and fec_dia='$mesDia'"
        );
        $diaFeriado->setFetchMode(PDO::FETCH_ASSOC);
        return $diaFeriado->fetch();
    }

    // Método que permite listar  los empleados
    public function metConsultarEmpleado($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $periodo)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and c.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if($pkNumDependencia!=''){
            $filtro = " and c.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($centroCosto!=''){
            $filtro .= " and c.fk_a023_num_centro_costo=$centroCosto";
        }
        if($tipoNomina!=''){
            $filtro .= " and c.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if($cargo!=''){
            $filtro .= " and c.fk_rhc063_num_puestos_cargo=$cargo";
        }
        if($edoReg!=''){
            $filtro .= " and c.ind_estatus=$edoReg";
        }
        if($sitTrab!=''){
            $filtro .= " and c.ind_estatus=$sitTrab";
        }
        if($pkNumEmpleado!=''){
            $filtro .= " and c.pk_num_empleado=$pkNumEmpleado";
        }
        if($periodo!=''){
            $filtro .= " and a.ind_periodo='$periodo'";
        }
        $listarEmpleado =  $this->_db->query(
            "select a.pk_num_beneficio, a.fec_inicio_periodo, a.fec_fin_periodo, c.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, c.ind_cedula_documento, d.ind_descripcion_cargo from rh_c019_beneficio_alimentacion as a, rh_c039_beneficio_alimentacion_detalle as b, vl_rh_persona_empleado_datos_laborales as c, rh_c063_puestos as d where a.pk_num_beneficio=b.fk_rhc019_num_beneficio and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_rhc063_num_puestos_cargo=d.pk_num_puestos $filtro"
        );
        $listarEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEmpleado->fetchAll();
    }

    // Método que permite listar  los empleados
    public function metConsultarBeneficioDetalle($pkNumEmpleado, $pkNumBeneficio)
    {
        $listarBeneficioDetalle =  $this->_db->query(
            "select  * from rh_c019_beneficio_alimentacion as a, rh_c039_beneficio_alimentacion_detalle as b where a.pk_num_beneficio=b.fk_rhc019_num_beneficio and b.fk_rhb001_num_empleado=$pkNumEmpleado and a.pk_num_beneficio=$pkNumBeneficio"
        );
        $listarBeneficioDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $listarBeneficioDetalle->fetch();
    }


    //Metodo que permite obtener el monto de descuento aplicado al funcionario en el periodo
    public function metMontoDescuentoEmpleadoBeneficio($pkNumEmpleado,$pkNumBeneficio)
    {
        $consulta =  $this->_db->query(
            "SELECT a.num_monto_descuento_otros AS num_monto FROM rh_c039_beneficio_alimentacion_detalle AS a WHERE a.fk_rhb001_num_empleado = $pkNumEmpleado AND a.fk_rhc019_num_beneficio = $pkNumBeneficio"
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $consulta->fetch();
        return $resultado['num_monto'];
    }

}// fin de la clase
