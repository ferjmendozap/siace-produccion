<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class disfruteVacacionesModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.pk_num_empleado, c.ind_nombre_detalle from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b. 	fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        }
    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $sitTrab, $empleado, $tipoNomina)
    {

        if ($pkNumOrganismo!='') {
            $filtro = " and a.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and a.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($tipoNomina!=''){
            $filtro .= " and a.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if($pkNumEmpleado!=''){
            $filtro .= " and a.pk_num_empleado=$pkNumEmpleado";
        }
        if($empleado!=''){
            $filtro .= " and a.pk_num_empleado=$empleado";
        }
        if($sitTrab!=''){
            $filtro .= " and a.ind_estatus=$sitTrab";
        }
        $empleado = $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_estatus, b.ind_dependencia, c.ind_descripcion_cargo from vl_rh_persona_empleado_datos_laborales as a, a004_dependencia as b, rh_c063_puestos as c, vl_rh_persona_empleado as d where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_rhc063_num_puestos_cargo=c.pk_num_puestos and a.pk_num_empleado=d.pk_num_empleado $filtro"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }


    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    // Método que permite listar la utilización
    public function metConsultarUtilizacionPeriodo($pkNumEmpleado)
    {
        $utilizacionPeriodo =  $this->_db->query(
            "SELECT
                        pk_num_periodo,
                        num_anio,
                        num_dias_derecho,
                        num_dias_gozados,
                        num_dias_interrumpidos,
                        num_total_utilizados,
                        num_pendientes,
                        num_estatus
                      FROM
                        rh_c081_periodo_vacacion
                      WHERE
                        fk_rhb001_num_empleado = '$pkNumEmpleado'
                      "
        );
        $utilizacionPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $utilizacionPeriodo->fetchAll();
    }
    public function metConsultarUtilizacionPeriodo2($pkNumEmpleado)
    {
        $utilizacionPeriodo =  $this->_db->query(
            "SELECT
                        pk_num_periodo,
                        num_anio,
                        num_dias_derecho,
                        num_dias_gozados,
                        num_dias_interrumpidos,
                        num_total_utilizados,
                        num_pendientes
                      FROM
                        rh_c081_periodo_vacacion
                      WHERE
                        fk_rhb001_num_empleado = '$pkNumEmpleado' AND 
                        num_pendientes != 0"
        );
        $utilizacionPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $utilizacionPeriodo->fetchAll();
    }


    // Método que permite listar la utilización
    public function metConsultarDetalle($pkNumEmpleado, $pkNumPeriodo)
    {
        $utilizacionPeriodo =  $this->_db->query(
            "SELECT
                        b.num_dias_detalle,
                        DATE_FORMAT(b.fec_inicial,'%d/%m/%Y') AS fecha_inicial,
                        DATE_FORMAT(b.fec_final,'%d/%m/%Y') AS fecha_final,
                        c.ind_nombre_detalle
                      FROM
                        rh_b009_vacacion_solicitud AS a,
                        rh_c079_vacacion_detalle AS b,
                        a006_miscelaneo_detalle AS c
                      WHERE
                        a.pk_num_solicitud_vacacion = b.fk_rhb009_num_solicitud_vacacion AND
                        b.fk_rhc081_num_periodo_vacacion = $pkNumPeriodo AND 
                        a.fk_rhb001_num_empleado = '$pkNumEmpleado' AND 
                        c.pk_num_miscelaneo_detalle = a.fk_a006_tipo_vacacion"
        );

        $utilizacionPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $utilizacionPeriodo->fetchAll();
    }

    // Método que permite sumar la utilización
    public function metCalcularPeriodo($pkNumEmpleado)
    {
        $utilizacionPeriodo =  $this->_db->query(
            "select SUM(a.num_dias_derecho) as dias_derecho, SUM(a.num_dias_gozados) as dias_gozados, SUM(a.num_dias_interrumpidos) as dias_interrumpidos, SUM(a.num_total_utilizados) as total_utilizado, SUM(a.num_pendientes) as pendiente, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, b.pk_num_empleado from rh_c081_periodo_vacacion as a, vl_rh_persona_empleado_datos_laborales as b where a.fk_rhb001_num_empleado=$pkNumEmpleado and a.fk_rhb001_num_empleado=b.pk_num_empleado"
        );
        $utilizacionPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $utilizacionPeriodo->fetch();
    }

    // Método que consulta los periodos de un funcionario
    public function metConsultarPeriodo($pkNumEmpleado)
    {
        $consultarPeriodo = $this->_db->query(
            "select  a.pk_num_periodo, a.fk_rhb001_num_empleado, a.num_anio, a.num_mes, a.num_dias_derecho, a.num_dias_gozados, a.	num_dias_interrumpidos, a.num_total_utilizados, a.num_pendientes, a.num_estatus, (select SUM(num_dias_utiles) as dias_utiles from rh_c082_vacacion_utilizacion where fk_rhc081_num_periodo=a.pk_num_periodo) as total_dias from rh_c081_periodo_vacacion as a where fk_rhb001_num_empleado=$pkNumEmpleado order by pk_num_periodo desc"
        );
        $consultarPeriodo->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarPeriodo->fetchAll();
    }

    // Método que permite obtener la utilizacion de vacaciones de un funcionario en un periodo especifico
    public function metMostrarUtilizacion($pkNumPeriodo)
    {
        $utilizacion = $this->_db->query(
            "select a.pk_num_vacacion_utilizacion, a.num_dias_utiles, date_format(a.fec_inicio, '%d/%m/%Y') as fecha_inicio, date_format(a.fec_fin, '%d/%m/%Y') as fecha_fin, c.cod_detalle, c.ind_nombre_detalle from rh_c082_vacacion_utilizacion as a, a005_miscelaneo_maestro as b, a006_miscelaneo_detalle as c where a.fk_rhc081_num_periodo=$pkNumPeriodo and b.cod_maestro='TIPVAC' and b.pk_num_miscelaneo_maestro=c.fk_a005_num_miscelaneo_maestro and c.pk_num_miscelaneo_detalle=a.fk_a006_tipo_vacacion order by a.pk_num_vacacion_utilizacion asc"
        );
        $utilizacion->setFetchMode(PDO::FETCH_ASSOC);
        return $utilizacion->fetchAll();
    }

}// fin de la clase

