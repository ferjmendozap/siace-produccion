<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Tramites en Ejecución</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 15%">Cod. Tramite.</th>
                            <th style="width: 10%">Tipo de Tramite</th>
                            <th style="width: 25%">Ente</th>
                            <th style="width: 25%">Fase - Actividad en Ejecución</th>
                            <th style="width: 20%">Acción</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var $url = '{$_Parametros.url}modDN/prorroga/prorrogaCONTROL/crearModificarMET';

        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}modDN/prorroga/prorrogaCONTROL/jsonDataTablaMET",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "ind_num_denuncia" },
                { "data": "tipoActuacion" },
                { "data": "ind_nombre_ente" },
                { "data": "FA"},
                { "orderable": false,"data": "acciones", width:150}
            ]
        );
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idDenuncia: $(this).attr('idDenuncia'), idActividad: $(this).attr('idActividad'), ver: 1, tipoActuacion: $(this).attr('tActuacion'), accion:'ver',idProrroga: $(this).attr('idProrroga'), estadoPro:$(this).attr('estadoPro') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.nueva', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idDenuncia:  $(this).attr('idDenuncia'), idActividad: $(this).attr('idActividad'), tipoActuacion: $(this).attr('tActuacion'), accion:'nuevo', idProrroga: 0}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idDenuncia:  $(this).attr('idDenuncia'), idActividad: $(this).attr('idActividad'), tipoActuacion: $(this).attr('tActuacion'), accion:'modificar', idProrroga: $(this).attr('idProrroga'), estadoPro:$(this).attr('estadoPro') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idDenuncia:  $(this).attr('idDenuncia'), idActividad: $(this).attr('idActividad'), tipoActuacion: $(this).attr('tActuacion'), accion:'aprobar', idProrroga: $(this).attr('idProrroga'), estadoPro:$(this).attr('estadoPro') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.revisar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idDenuncia:  $(this).attr('idDenuncia'), idActividad: $(this).attr('idActividad'), tipoActuacion: $(this).attr('tActuacion'), accion:'revisar', idProrroga: $(this).attr('idProrroga'), estadoPro:$(this).attr('estadoPro') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.conformar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idDenuncia:  $(this).attr('idDenuncia'), idActividad: $(this).attr('idActividad'), tipoActuacion: $(this).attr('tActuacion'), accion:'conformar', idProrroga: $(this).attr('idProrroga'), estadoPro:$(this).attr('estadoPro') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
    });
</script>