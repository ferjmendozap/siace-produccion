<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<form action="{$_Parametros.url}modDN/solicitud/solicitudCONTROL/crearModificarPersonaMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body ">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{if isset($persona)}{$persona}{/if}" name="persona" />

        {if isset($idPersona) }
            <input type="hidden" value="{$idPersona}" name="idPersona"/>
        {/if}
        <div class="row">
            <div class="col-sm-12">

                <div class="col-sm-12">
                    <div class="col-sm-8">
                        <div class="form-group floating-label" id="ind_cedula_documentoError">
                            <input type="text" class="form-control"  value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" name="form[alphaNum][ind_cedula_documento]" id="ind_cedula_documento"{if isset($ver) }disabled{/if}>
                            <label for="ind_cedula_documento"><i class="md md-perm-identity"></i> Cedula </label>
                        </div>
                    </div>
                    <div class="col-sm-4">

                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" {if isset($formDB.num_estaus) and $formDB.num_estaus==1} checked{/if} value="1" id="cne" {if isset($ver) }disabled{/if}>
                                <span>Consultar con CNE</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="form-group floating-label" id="ind_nombre1Error">
                            <input type="text" class="form-control" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}" name="form[alphaNum][ind_nombre1]" id="ind_nombre1"{if isset($ver) }disabled{/if}>
                            <label for="ind_nombre1"><i class="fa fa-pencil"></i> Nombre </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group floating-label" id="ind_apellido1Error">
                            <input type="text" class="form-control" value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}" name="form[alphaNum][ind_apellido1]" id="ind_apellido1"{if isset($ver) }disabled{/if}>
                            <label for="ind_apellido1"><i class="fa fa-pencil"></i> Apellido </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="form-group floating-label" id="ind_direccionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}" name="form[alphaNum][ind_direccion]" id="ind_direccion"{if isset($ver) }disabled{/if}>
                        <label for="ind_direccion"><i class="md md-location-city"></i> Domicilio </label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="ind_emailError">
                        <input type="email" class="form-control" value="{if isset($formDB.ind_email)}{$formDB.ind_email}{/if}" name="form[formula][ind_email]" id="ind_email" {if isset($ver) }disabled{/if} >
                        <label for="ind_email"><i class="fa fa-at"></i> Email</label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="ind_telefonoError">
                        <input type="text" class="form-control" data-inputmask="'mask': '(9999)999-9999'" value="{if isset($formDB.ind_telefono)}{$formDB.ind_telefono}{/if}" name="form[alphaNum][ind_telefono]" id="ind_telefono"{if isset($ver) }disabled{/if}>
                        <label for="ind_telefono"><i class="md md-settings-cell"></i> Telefono</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" checked value="1" name="form[int][num_estatus]"{if isset($ver) }disabled{/if}>
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                  <div class="col-sm-5">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                  </div>
                </div>
            </div>
           <span class="clearfix"></span>
        </div>
  {if isset($ver)}

  {else}
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                {if isset($idPersona) and $idPersona!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
        </button>
    </div>
  {/if}
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();
        // anular accion del Formulario
        $("#formAjax").submit(function(){
            return false;
        });
        $('#accion').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {

                var arrayCheck = '';
                var arrayMostrarOrden = '';
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'modificar') {
                    app.metActualizarRegistroTabla(dato, dato['idPersona'], 'idPersona', arrayCheck, arrayMostrarOrden, ' modificaron satisfactoriamente.', 'cerrarModal2', 'ContenidoModal2');
                }else if (dato['status'] == 'nuevo') {
                    app.metNuevoRegistroTabla(dato, dato['idPersona'], 'idPersona', arrayCheck, arrayMostrarOrden, 'Los datos de la Persona se guardaron satisfactoriamente.', 'cerrarModal2', 'ContenidoModal2');
                }
            }, 'json');
        });

        $('#cne').on('change',function () {
            $.post('{$_Parametros.url}modCV/maestros/personaCONTROL/consultaCneMET/', { idCedula: $('#ind_cedula_documento').val() }, function (dato) {

                if(dato['status']=='errorCurl'){
                    app.metValidarError(dato,'Disculpa. la versión de PHP no soporta CURL, contacte al administrador');
                } else if(dato['status']=='correcto'){
                    $('#ind_nombre1').val(dato['nombres']);
                    var cantidad = dato['apellidos'].length;

                    var apellidos = '';
                    for (var i=0; i<cantidad; i=i+1){
                        if(dato['apellidos'][i]!='	'){
                            apellidos += dato['apellidos'][i];
                        } else {
                            i=cantidad;
                        }
                    }
                    $('#ind_apellido1').val(apellidos);
                }
            }, 'json');
        });
    });

</script>