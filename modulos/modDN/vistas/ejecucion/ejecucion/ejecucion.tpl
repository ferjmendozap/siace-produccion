
    <div class="modal-body" style="padding:0;">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                            <form id="formAjax" action="{$_Parametros.url}modDN/ejecucion/ejecucionCONTROL/ejecutarActividadMET" class="form floating-label form-validation" class="form" role="form" method="post">

                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">EJECUCIÓN DE LA ACTIVIDAD</span></a></li>
                                    <li><a id="tab_2" href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">PLANFICACIÓN</span></a></li>
                                </ul>
                            </div>

                            <div class="tab-content clearfix">

                                <!--INFORMACIÓN DE LA EJECUCION-->
                                <div class="tab-pane active" id="tab1">

                                    <input type="hidden" value="1" name="valido"/>
                                    <input type="hidden" value="{$idDenuncia}" id="idDenuncia" name="form[int][idDenuncia]" />
                                    <input type="hidden" value="{$actividad.pk_num_detalle_dc}" id="idActividad" name="form[int][idActividad]" />
                                    <input type="hidden" value="{$actividad.ind_estatus}" id="ind_estatus" name="form[txt][ind_estatus]"/>
                                    <input type="hidden" value="{$actividad.num_secuencia}" id="secuencia" name="form[int][secuencia]"/>
                                    <input type="hidden" value="{if $actividad.ind_estatus=='PE'}EJ{elseif $actividad.ind_estatus=='EJ'}TE{/if}" id="nuevo_estatus" name="form[txt][nuevo_estatus]"/>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="card" >
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">INFORMACIÓN DE LA ACTIVIDAD</header>

                                                </div> <br/>
                                                <div class="card-body" style="padding:0;">
                                                    <div class="col-sm-12" style="margin-bottom: 10px">
                                                        <!--Código de la  Denuncia-->
                                                        <div class="col-sm-4">
                                                            <div class="form-group"
                                                                 id="id_enteError" style="margin-top: -10px;">
                                                                <label for="cod_denuncia"><i
                                                                            class="md md-markunread-mailbox"></i> Tramite Nro.  </label>
                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="codDenuncia"
                                                                           class="form-control" id="codDenuncia"
                                                                           value="{$formDB.ind_num_denuncia}"
                                                                           readonly >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--tipo de tramite-->
                                                        <div class="col-sm-4">
                                                            <div class="form-group"
                                                                 id="tramiteError" style="margin-top: -10px;" >
                                                                <label for="tramite" ><i
                                                                            class="md md-info-outline"></i>Tipo de tramite:</label>
                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="tramite"
                                                                           class="form-control" id="tramite"
                                                                           value="{$tramite}"
                                                                           readonly >
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--Estado de la Denuncia-->
                                                        <div class="col-sm-4">
                                                            <div class="form-group"
                                                                 id="id_enteError" style="margin-top: -10px;" >
                                                                <label for="ind_estado" ><i
                                                                            class="md md-info-outline"></i>Estado de la Actividad:  </label>
                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="estado"
                                                                           class="form-control" id="estado"
                                                                           value="{$actividad.estatus} "
                                                                           readonly >
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-12" style="margin-bottom: 10px">
                                                        <!--organismo ejecutante-->

                                                        <div class="col-sm-4">
                                                            <div class="form-group" style="margin-bottom: 20px;">

                                                                <label for="fec_inicio"
                                                                       class="control-label" > <i class="md md-access-time"></i> Fecha de inicio:</label>

                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="form[txt][fe_inicio]"
                                                                           class="form-control" id="fe_inicio"
                                                                           value="{if isset($actividad.fec_inicio)}{($actividad.fec_inicio)|date_format:"%d-%m-%Y"}{else}{$fecha_actual}{/if}"
                                                                           readonly >
                                                                    <input type="hidden"
                                                                           name="form[txt][fec_inicio_real]"
                                                                           class="form-control" id="fec_inicio_real"
                                                                           value="{if isset($actividad.fec_inicio_real)}{$actividad.fec_inicio_real}{else}{$fecha_actual}{/if}"
                                                                           readonly >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--Fecha registro-->
                                                        <div class="col-sm-4">

                                                            <div class="form-group" style="margin-bottom: 20px;">

                                                                <label for="fec_termino"
                                                                       class="control-label" > <i class="md md-access-time"></i> Fecha de culminacion:</label>

                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="form[txt][fec_termino]"
                                                                           class="form-control" id="fec_termino"
                                                                           value="{if ($actividad.fec_termino_real!='0000-00-00')}{($actividad.fec_termino_real)|date_format:"%d-%m-%Y"}{else}{($actividad.fec_termino)|date_format:"%d-%m-%Y"}{/if}"
                                                                           readonly >
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-sm-4">

                                                            <div class="form-group" style="margin-bottom: 20px;">

                                                                <label for="num_dias_cierre"
                                                                       class="control-label" > <i class="md md-access-time"></i> Días de ejecución:</label>

                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="form[int][num_dias_cierre]"
                                                                           class="form-control" id="num_dias_cierre"
                                                                           value="{if $actividad.num_dias_cierre!=0} {$actividad.num_dias_cierre}{elseif $dias_cierre>$actividad.num_duracion}{$dias_cierre}{else}{$actividad.num_duracion}{/if}"
                                                                           readonly >
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" style="margin-bottom: 10px;">
                                                        <!---descripcion-->
                                                        <div class="form-group" style="margin-bottom: 20px;">
                                                            <label for="ind_actividad" class="control-label" style="margin-bottom: 10px;"><i
                                                                        class="md md-format-indent-increase"></i>   Fase - Actividad:</label>

                                                            <div style="margin-left: 10px; " id="ind_actividadError" >
                                                                    <textarea id="ind_actividad" class="form-control" rows="2" name="ind_actividad"
                                                                              readonly >{$actividad.nombre_fase} - {$actividad.nombre_actividad}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" style="margin-bottom: 10px">
                                                        <!--organismo o ente denunciado-->


                                                            <div class="form-group"
                                                                 id="id_enteError" style="margin-bottom: 10px;">
                                                                <label for="id_ente"><i
                                                                            class="md md-domain"></i> Organismo/Ente Denunciado</label>
                                                                <select id="id_ente" name="form[int][id_ente]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=fila from=$selectEnte}
                                                                        {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente}
                                                                            <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!--origen de los fondos o recursos -->
                                                        <div class="col-sm-12" style="margin-bottom: 10px">

                                                            <div class="form-group"
                                                                 id="id_ente_recursos" style="margin-bottom: 10px;">
                                                                <label for="id_ente_recursos"><i
                                                                            class="md md-local-atm"></i> Origen de los Recursos</label>
                                                                <select id="id_ente_recursos" name="form[int][id_ente_recursos]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=fila from=$selectEnte}
                                                                        {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente_recursos}
                                                                            <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>

                                                        </div>


                                                    <div class="col-sm-12" style="margin-bottom: 10px;">
                                                     <!---descripcion-->
                                                        <div class="form-group">
                                                            <label for="ind_observacion" class="control-label" style="margin-bottom: 10px;"><i
                                                                        class="fa fa-edit"></i>Observación:</label>
                                                            <div style="margin-left: 10px; " id="ind_observacionError" >
                                                                    <textarea id="ind_observacion" class="form-control" rows="2" name="form[txt][ind_observacion]"
                                                                            {if !isset($ver)} readonly {/if}>{if isset($actividad.ind_observaciones)}{$actividad.ind_observaciones}{/if}</textarea>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="card" >
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">ACCIÓN A EJECUTAR</header>

                                                </div> <br/>
                                                <div class="col-sm-12 alert alert-callout alert-info" style="margin-bottom: 10px">
                                                    <!---terminar actividad-->
                                                    {if $actividad.ind_estatus=='PE'}
                                                        <div class="col-sm-6">
                                                        <label class="radio-inline radio-styled">
                                                            <input class="estado" name="form[txt][iniciar_terminar]" type="radio" value="EJ" id="iniciar"
                                                               checked {if $ver==1} disabled > {else} > {/if}

                                                            <span>
                                                                Iniciar Ejecución    <i class="fa fa-play"></i>
                                                            </span>
                                                        </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input class="date form-control" id="fe_ejecucion"
                                                                   name="form[txt][fe_ejecucion]"
                                                                   style="text-align: center"
                                                                   type="text"
                                                                   value="{if $actividad.fec_inicio_real=='' or $actividad.fec_inicio_real=='0000-00-00'}{date('Y-m-d')}{else}{$actividad.fec_inicio_real}{/if}"
                                                                   title="Ingrese fecha de inicio"
                                                                   id="datepicker"
                                                                   readonly >
                                                        </div>
                                                    {else}
                                                        <div class="col-sm-6">
                                                            <label class="radio-inline radio-styled">
                                                                <input class="estado" name="form[txt][iniciar_terminar]" type="radio" value="TE" id="terminar"
                                                                      checked {if $actividad.ind_estatus=='TE' or $ver==1} disabled > {else} > {/if}
                                                                <span>
                                                                {if $actividad.ind_estatus=='TE'} Actividad Terminada  {else} Terminar Ejecución {/if}
                                                                    <i class="fa fa-step-forward"></i></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input class="date form-control" id="fec_termino_act"
                                                                   name="form[txt][fec_termino_act]"
                                                                   style="text-align: center"
                                                                   type="text"
                                                                   value="{if $actividad.fec_termino_real=='' or $actividad.fec_termino_real=='0000-00-00'}{date('Y-m-d')}{else}{$actividad.fec_termino_real}{/if}"
                                                                   id="datepicker"
                                                                   title="Ingrese fecha de culminación"
                                                                   readonly >
                                                        </div>
                                                    {/if}
                                                </div>

                                                <div class="col-sm-12 alert alert-callout alert-warning" id="va_ce" style="margin-bottom: 10px; {if $ver==1 or $actividad.ind_estatus=='EJ'} visibility: visible; {else} visibility: hidden; {/if}">
                                                    <!---terminar actividad-->
                                                    <div class="col-sm-6">
                                                        <label class="radio-inline radio-styled">
                                                            <input class="estado" name="form[txt][valorar_cerrar]" id="valorar" type="radio" value="VA" {if $ver==1} disabled {/if}>
                                                            <span>
                                                                Valorar  Tramite <i class="fa fa-legal"></i></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">

                                                        <label class="radio-inline radio-styled">
                                                            <input class="estado" name="form[txt][valorar_cerrar]" id="cerrar" type="radio" value="CE" {if $ver==1} disabled {/if}>
                                                            <span>
                                                                Cerrar  Tramite <i class="icm icm-blocked"></i></span>
                                                        </label>

                                                    </div>
                                                </div>

                                                <div class="col-sm-12 alert alert-callout alert-warning" style="margin-bottom: 10px; {if $ver==1} visibility: visible; {else} visibility: hidden; {/if}" id="ar_rm">
                                                    <!---terminar actividad-->
                                                    <div class="col-sm-6">
                                                        <label class="radio-inline radio-styled">
                                                            <input class="estado" name="form[txt][archivo_remitir]" id="archivo" type="radio" value="AA"
                                                                {if $actividad.ind_estatus=='AA'}checked disabled {elseif $actividad.num_flag_auto_archivo==0 or $actividad.ind_estatus=='TE' or $ver==1} disabled{/if}>
                                                            <span>
                                                                Auto de Archivo   <i class="icm icm-books"></i></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">

                                                        <label class="radio-inline radio-styled">
                                                            <input class="estado" name="form[txt][archivo_remitir]" id="remitir" type="radio" value="RM"
                                                               {if $actividad.ind_estatus=='RE'}checked disabled {elseif $actividad.ind_estatus=='TE' or $ver==1} disabled{/if}>
                                                            <span>
                                                               Remitir   <i class="icm icm-share"></i>
                                                        </label>

                                                    </div>
                                                </div>

                                                <div id="accordion3" class="panel-group col-sm-12">
                                                    <div class="card panel" id="divValoracion" style="{if $ver==1} visibility: visible; {else} visibility: hidden; {/if}">
                                                        <div class="card-head card-head-sm collapsed" data-toggle="collapse" data-parent="#accordion1" data-target="#accordion1-1" {if $ver==1} aria-expanded="false" {else} aria-expanded="true" {/if}>
                                                            <header>Valoración:</header>
                                                            <div class="tools">
                                                                <a class="btn btn-icon-toggle">
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div id="accordion1-1" class="collapse" {if $ver==1} aria-expanded="true" {else} aria-expanded="false" {/if}  style="height: 0px;">
                                                            <div class="card-body">
                                                                <div class="col-sm-12" style="margin-bottom: 10px">
                                                                    <!---instancia a remitir-->
                                                                    <div class="form-group" style="margin-bottom: 20px;">
                                                                        <label for="ind_antecedente_expediente" class="control-label" style="margin-bottom: 10px;"><i
                                                                                    class="fa fa-edit"></i>  Antecedente del Expediente :</label>

                                                                        <div style="margin-left: 10px; " id="ind_antecedente_expedienteError" >
                                                                            <textarea id="ind_antecedente_expediente" class="form-control" rows="2" name="form[txt][ind_antecedente_expediente]"
                                                                            {if $ver==1} readonly {/if}>{if isset($valoracion.ind_antecedente_expediente)}{$valoracion.ind_antecedente_expediente }{/if}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                                                    <!---instancia a remitir-->
                                                                    <div class="form-group" style="margin-bottom: 20px;">
                                                                        <label for="ind_analisis_hechos_expediente" class="control-label" style="margin-bottom: 10px;"><i
                                                                                    class="fa fa-edit"></i>  Análisis de los Hechos:</label>

                                                                        <div style="margin-left: 10px; " id="ind_analisis_hechos_expedienteError" >
                                                                            <textarea id="ind_analisis_hechos_expediente" class="form-control" rows="2" name="form[txt][ind_analisis_hechos_expediente]"
                                                                                    {if $ver==1} readonly {/if}>{if isset($valoracion.ind_analisis_hechos_expediente)}{$valoracion.ind_analisis_hechos_expediente }{/if}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                                                    <!---instancia a remitir-->
                                                                    <div class="form-group" style="margin-bottom: 20px;">
                                                                        <label for="ind_valoracion_expediente" class="control-label" style="margin-bottom: 10px;"><i
                                                                                    class="fa fa-edit"></i>  Valoración:</label>

                                                                        <div style="margin-left: 10px; " id="ind_valoracion_expedienteError" >
                                                                            <textarea id="ind_valoracion_expediente" class="form-control" rows="2" name="form[txt][ind_valoracion_expediente]"
                                                                                    {if $ver==1} readonly {/if}>{if isset($valoracion.ind_valoracion_expediente)}{$valoracion.ind_valoracion_expediente}{/if}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                                                    <!---instancia a remitir-->
                                                                    <div class="form-group" style="margin-bottom: 20px;">
                                                                        <label for="ind_conclusion_valoracion" class="control-label" style="margin-bottom: 10px;"><i
                                                                            class="fa fa-edit"></i>  Conclusión:</label>

                                                                        <div style="margin-left: 10px; " id="ind_conclusion_valoracionError" >
                                                                            <textarea id="ind_conclusion_valoracion" class="form-control" rows="2" name="form[txt][ind_conclusion_valoracion]"
                                                                                    {if $ver==1} readonly {/if}>{if isset($valoracion.ind_conclusion_valoracion)}{$valoracion.ind_conclusion_valoracion}{/if}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                                                    <!---instancia a remitir-->
                                                                    <div class="form-group" style="margin-bottom: 20px;">
                                                                        <label for="ind_manifiesto" class="control-label" style="margin-bottom: 10px;"><i
                                                                                    class="fa fa-edit"></i>  Manifiesto:</label>

                                                                        <div style="margin-left: 10px; " id="ind_manifiestoError" >
                                                                            <textarea id="ind_manifiesto" class="form-control" rows="2" name="form[txt][ind_manifiesto]"
                                                                                    {if $ver==1} readonly {/if}>{if isset($valoracion.ind_manifiesto)}{$valoracion.ind_manifiesto }{/if}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                                                    <!---instancia a remitir-->
                                                                    <div class="form-group" style="margin-bottom: 20px;">
                                                                        <label for="ind_recomendaciones" class="control-label" style="margin-bottom: 10px;"><i
                                                                                    class="fa fa-edit"></i>  Recomendaciones:</label>

                                                                        <div style="margin-left: 10px; " id="ind_recomendacionesError" >
                                                                            <textarea id="ind_recomendaciones" class="form-control" rows="2" name="form[txt][ind_recomendaciones]"
                                                                                    {if $ver==1} readonly {/if}>{if isset($valoracion.ind_recomendaciones)}{$valoracion.ind_recomendaciones }{/if}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    {*----Panel REMITIR----*}

                                                    <div class="card panel" id="divRemitir" style="{if $ver==1} visibility: visible; {else} visibility: hidden; {/if}">
                                                        <div class="card-head card-head-sm collapsed" data-toggle="collapse" data-parent="#accordion1" data-target="#accordion1-2" {if $ver==1} aria-expanded="false" {else} aria-expanded="true" {/if}>
                                                            <header>Remitir a:</header>
                                                            <div class="tools">
                                                                <a class="btn btn-icon-toggle">
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div id="accordion1-2" class="collapse" {if $ver==1} aria-expanded="true" {else} aria-expanded="false" {/if}  style="height: 0px;">
                                                            <div class="card-body">
                                                                <div class="col-sm-12" style="margin-bottom: 10px">
                                                                    <!---instancia a remitir-->
                                                                    <div class="form-group" id="tipoInstanciaError" style="margin-bottom: 10px;">
                                                                        <label for="tipoInstancia"><i
                                                                                    class="md md-domain"></i> Tipo de Instancia:</label>
                                                                        <select id="tipoInstancia" name="form[int][tipoInstancia]" class="form-control select2" style="height: 27px;" {if $ver==1} disabled {/if}>
                                                                            <option value="0">Seleccione..</option>
                                                                            <option value="1">Dependencia Interna</option>
                                                                            <option value="2">Órgano de Control</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                                                    <!---instancia a remitir-->
                                                                    <div class="form-group" id="instanciaError" style="margin-bottom: 10px;">
                                                                        <label for="instanciaError"><i
                                                                                    class="fa fa-building"></i> Instancia:</label>
                                                                        <select id="instancia" name="form[int][instancia]" class="form-control select2" style="height: 27px;"
                                                                                {if $ver==1} disabled {/if}>
                                                                            <option value="">Seleccione..</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!--cierre INFORMACIÓN DE LA DENUNCIA-->

                                <!--PANEL DE ACTIVIDADES-->
                                <div class="tab-pane" id="tab2">
                                    <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary text-center">
                                                        <header class="text-center">Actividades</header>
                                                    </div>
                                                    <div class="card-body">
                                                        {*<div class="col-lg-12">*}
                                                        <div class="table-responsive">
                                                            <table id="tb_actividades" class="table no-margin table-condensed table-bordered" style="width: 1120px; display:block">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-center" style="width: 2%;">Est.</th>
                                                                    <th class="text-center" style="width: 41%;">Actividad</th>
                                                                    <th class="text-center" style="width: 4%;">Días</th>
                                                                    <th class="text-center" style="width: 8%;">Inicio</th>
                                                                    <th class="text-center" style="width: 8%;">Fin</th>
                                                                    <th class="text-center" style="width: 3%;">Prorr.</th>
                                                                    <th class="text-center" style="width: 8%;">Inicio Real</th>
                                                                    <th class="text-center" style="width: 8%;">Fin Real</th>
                                                                    <th class="text-center" style="width: 3%;">A.P.</th>
                                                                    <th class="text-center" style="width: 3%;">A.A.</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody  id="actividades" style="height: 400px; width: 100%; display:inline-block; overflow: auto">
                                                                {$SubTotal_dias=0}
                                                                {$SubTotal_diasProrroga=0}
                                                                {$Total_dias=0}
                                                                {$Totaldias_prorroga=0}
                                                                {$totalDiasNoAfecta=0}
                                                                {$cuenta_fase=0}
                                                                {if $listaActividades|count > 0}
                                                                    {$contador=0}
                                                                    {foreach key=nombre_fase item=fases from=$listaActividades}
                                                                        <tr class="info">
                                                                            <td width="560" colspan="10" class="">
                                                                                {$nombre_fase}
                                                                            </td>
                                                                        </tr>
                                                                        {foreach item=fila from=$fases}
                                                                            {if $fila.num_flag_no_afecto_plan ==0}
                                                                                {$SubTotal_dias=$SubTotal_dias+$fila.num_duracion}
                                                                                {$SubTotal_diasProrroga=$SubTotal_diasProrroga+$fila.num_prorroga}
                                                                                {$afecta_plan=1}
                                                                                {$no_afecta_plan='md md-check'}
                                                                            {else}
                                                                                {$afecta_plan=0}
                                                                                {$no_afecta_plan=''}
                                                                                {$totalDiasNoAfecta=$totalDiasNoAfecta+$fila.num_duracion}
                                                                            {/if}
                                                                            {if $fila.num_flag_auto_archivo==1}
                                                                                {$auto_archivo='md md-check'}
                                                                            {else}
                                                                                {$auto_archivo=0}
                                                                            {/if}

                                                                            <tr id="tr_actividad_{$fila.pk_num_detalle_dc}" {if isset($fila.semaforo)}class="{$fila.semaforo}"{/if} role="alert" >

                                                                                     <td style="width: 2%;">
                                                                                         {if ($fila.ind_estatus=='PE')}
                                                                                             <button title="PENDIENTE"
                                                                                                     class="btn ink-reaction btn-floating-action btn-xs btn-danger">
                                                                                                 <i class="icm icm-busy2" ></i>
                                                                                             </button>
                                                                                         {elseif ($fila.ind_estatus=='EJ')}
                                                                                             <button title="Consultar Actividad"
                                                                                                     class="btn ink-reaction btn-floating-action btn-xs btn-info">
                                                                                                 <i class="icm icm-spinner" ></i>
                                                                                             </button>

                                                                                         {else}
                                                                                             <button title="Consultar Actividad"
                                                                                                     class="btn ink-reaction btn-floating-action btn-xs btn-success">
                                                                                                 <i class="icm icm-checkmark3" ></i>
                                                                                             </button>

                                                                                         {/if}
                                                                                     </td>
                                                                                <td style="width: 41%;" id="td_descActividad_{$fila.pk_num_detalle_dc}">
                                                                                    <span id="spn_descActividad_{$fila.pk_num_detalle_dc}">{$fila.cod_act}  {$fila.nombre_actividad}</span>
                                                                                    <input type="hidden" id="hdd_dataActividad{$contador}" name="form[txt][actividad][secuencia][pkActividad][{$contador}]" value="{$fila.pk_num_detalle_dc}"/>
                                                                                </td>
                                                                                <td id="afecta_plan_{$afecta_plan}" style="width: 4%; text-align: center">
                                                                                    {$fila.num_duracion}</td>
                                                                                <td id="td_fecha_inicio{$contador}" style="width: 8%;" class="text-center">{$fila.fec_inicio}
                                                                                    </td>
                                                                                <td id="td_fecha_fin{$contador}" style="width: 8%;" class="text-center">{$fila.fec_termino}
                                                                                    </td>
                                                                                <td style="width: 3%;" class="text-center">{$fila.num_prorroga}</td>
                                                                                <td id="td_fecha_inicio_real{$contador}" style="width: 8%;" class="text-center">{$fila.fec_inicio_real}</td>
                                                                                <td id="td_fecha_fin_real{$contador}"style="width: 8%;" class="text-center">{$fila.fec_termino_real}</td>
                                                                                <td id="td_no_afecta{$contador}" valor="{$afecta_plan}" style="width: 3%;" class="text-center"><i class="{$no_afecta_plan}"></i></td>
                                                                                <td id="td_auto_archivo{$contador}"style="width: 3%;" class="text-center"><i class="{$auto_archivo}"></i></td>

                                                                            </tr>
                                                                            {$contador=$contador+1}
                                                                            {$fecha_fin_plan="'{$fila.fec_termino_real}'"}
                                                                    {/foreach}

                                                                        <tr id="tr_subtotal_" class="fondo-Subtotal">
                                                                            <td style="width: 2%;" align="center"></td>
                                                                            <td style="width: 41%;" id="td_1">Total dias Fase:</td>
                                                                            <td id="td_Subtotal{$ifilasubtotal++}" name="td_Subtotal" class="text-center" style="width: 4%;">{$SubTotal_dias}</td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td class="text-center" style="width: 3%;">{$SubTotal_diasProrroga}</td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 3%;"></td>
                                                                            <td style="width: 3%;"></td>
                                                                        </tr>

                                                                        {$Total_dias=$Total_dias+$SubTotal_dias}
                                                                        {$Totaldias_prorroga=$Totaldias_prorroga+$SubTotal_diasProrroga}
                                                                        {$SubTotal_dias=0}
                                                                        {$SubTotal_diasProrroga=0}
                                                                        {$cuenta_fase=$cuenta_fase+1}
                                                                    {/foreach}
                                                                    <tr id="tr_total_" class="fondo-total">
                                                                        <td style="width: 2%;" align="center"><input type="hidden" id="numero" name="numero" value="{$contador}"/></td>
                                                                        <td style="width: 41%;" id="td_1">Total dias de afectación:</td>
                                                                        <td id="td_total" class="text-center" style="width: 4%;">{$Total_dias}</td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td class="text-center" style="width: 3%;">{$Totaldias_prorroga}</td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 3%;"></td>
                                                                        <td style="width: 3%;"></td>
                                                                    </tr>
                                                                    {$totalGeneralDias=$Total_dias+$totalDiasNoAfecta}
                                                                {/if}
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                        {*</div>*}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <!--cierre PANEL DE ACTIVIDADES-->

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="preparado">Preparado por:</label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="revisado">Revisado:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="aprobado">Aprobado por:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="ultmodificacion"><i class="fa fa-calendar"></i>Última modificación:</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="lbl_fehareg" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_ultima_modificacion)} {$formDB.fec_ultima_modificacion}{/if}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 text-left">
                                    <div class="form-group text-left">
                                        <label id="lbl_preparadopor" class="help-block text-left" style="margin-top: 10px;">{*if isset($formDB.fec_ultima_modificacion)} {$formDB.fec_ultima_modificacion}{else}{$fec_ultima_modificacion}{/if*}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label id="lbl_revisadpor" class="help-block" style="margin-top: 10px;">{*if isset($formDB.fec_ultima_modificacion)} {$formDB.fec_ultima_modificacion}{else}{$fec_ultima_modificacion}{/if*}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label id="lbl_aprobadopor" class="help-block" style="margin-top: 10px;">{*if isset($denuncia.fec_ultima_modificacion)} {$denuncia.fec_ultima_modificacion}{else}{$fec_ultima_modificacion}{/if*}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label id="lbl_fechamodificacion" class="help-block" style="margin-top: 10px;">{*if isset($denuncia.fec_ultima_modificacion)} {$denuncia.fec_ultima_modificacion}{else}{$fec_ultima_modificacion}{/if*}</label>
                                    </div>
                                </div>
                            </div>

                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                <li class="next last"><a class="btn-raised" href="javascript:void(0);">&Uacute;ltimo</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Pr&oacute;ximo</a></li>
                            </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <span class="clearfix"></span>
    <div class="modal-footer">
        <button type="button" class="btn btn-default logsUsuarioModal " descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
            <i class="glyphicon glyphicon-floppy-remove"></i> {if !isset($ver) or $ver==0}  Cancelar {else} Cerrar {/if}
        </button>
        {if !isset($ver) or $ver==0}
            <button type="button" id="btn_guarda" class="btn btn-primary logsUsuarioModal">
                <i class="glyphicon glyphicon-floppy-disk"></i> Guardar
            </button>
        {/if}
    </div>

<script type="text/javascript">

    $(document).ready(function () {
        //Activar el wizard
        var app = new AppFunciones();
        app.metWizard();
        // ancho de la Modal
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");
        //Complementos
        $('.select2').select2({ allowClear: true });
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd",language: 'es'});

        $("#formAjax").submit(function(){ return false; });
        // anular accion del Formulario

        $("#formAjax").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        });

        $('#contenidoTabla2').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });

        $('#btn_guarda').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $("#id_contraloria").attr({ disabled: false });
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (result) {

                if (result['status'] == 'error') {
                    app.metValidarError(result, 'Atención. Los campos marcados con X en rojo son obligatorios');
                } else if (result['status'] == 'errorSql') {
                    swal("¡Atención!", result['mensaje'], "error");
                } else if (result['idDenuncia']) {
                    $("#idDenuncia").val(result['idDenuncia']);
                    $("#cod_denuncia").html(result['cod_denuncia']);
                    swal("Información del proceso", result['mensaje'], "success");
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Actividad fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });



        //***************************** Llenado campo pruebas ********************************************/

        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });

        $('#terminar').on( 'click', function() {
            $('#va_ce').css("visibility", 'visible');
        });

        $('#iniciar').on( 'click', function() {
            $('#va_ce').css("visibility", 'hidden');
            $('#ar_rm').css("visibility", 'hidden');
            $('#valorar').attr('checked', false);
            $('#cerrar').attr('checked', false);
            $('#archivo').attr('checked', false);
            $('#remitir').attr('checked', false);
            $('#divRemitir').css("visibility", 'hidden');
            $('#divValoracion').css("visibility", 'hidden');
        });

        $('#valorar').on('click', function () {
            $('#ar_rm').css("visibility", 'visible');
            $('#divValoracion').css("visibility", 'visible');
        });
        $('#cerrar').on('click', function () {
            $('#ar_rm').css("visibility", 'hidden');
            $('#archivo').attr('checked', true);
            $('#remitir').attr('checked', false);
            $('#divValoracion').css("visibility", 'hidden');
            $('#divRemitir').css("visibility", 'hidden');
        });

        $('#remitir').on('click', function () {
            $('#divRemitir').css("visibility", 'visible');
        });

        $('#archivo').on('click', function () {
            $('#divRemitir').css("visibility", 'hidden');
        });

        $('.estado').on('click', function () {
            $val=$(this).val();
            $('#nuevo_estatus').val($val);
        });

        $('#tipoInstancia').change(function(){
            var tipoinstancia = $(this).val();
            $.post('{$_Parametros.url}modDN/ejecucion/ejecucionCONTROL/jsonInstanciaMET', { tipoInstancia: tipoinstancia },function(dato){
                $('#instancia').attr("disabled",false);
                $('#instancia').html('');
                $('#instancia').append('<option value="">Seleccione la instancia</option>');
                $('#s2instancia .select2-chosen').html('Seleccione la instancia');
                if(tipoinstancia==1){
                    $.each(dato,function(index, value){
                        $('#instancia').append('<option value="'+value.pk_num_dependencia+'">'+value.ind_dependencia+'</option>');
                    });
                }else if(tipoinstancia==2){
                    $.each(dato,function(index, value){
                        $('#instancia').append('<option value="'+value.pk_num_ente+'">'+value.ind_nombre_ente+'</option>');
                    });
                }
            },'json');
        });

        $('#tipoInstancia').ready(function(){
            var tipoinstancia= $(this).val();
            var instancia= $("instancia").attr('instancia');
            $.post('{$_Parametros.url}modDN/ejecucion/ejecucionCONTROL/jsonInstanciaMET', { tipoinstancia: tipoinstancia },function(dato){
                $('#instancia').html('');
                $('#instancia').append('<option value="">Seleccione la instancia</option>');
                $('#s2instancia .select2-chosen').html('Seleccione la instancia');
                if(tipoinstancia==1){
                    $.each(dato,function(index, value){
                        if(value.pk_num_dependencia==instancia){
                            $('#instancia').append('<option selected value="'+value.pk_num_dependencia+'">'+value.ind_descripcion+'</option>');
                            $('#s2instancia .select2-chosen').html(dato[i]['ind_descripcion']);
                        } else {
                            $('#instancia').append('<option value="'+value.pk_num_ente+'">'+value.ind_nombre_ente+'</option>');
                        }
                    });
                }else if(tipoinstancia==2){
                    $.each(dato,function(index, value){
                        if(value.pk_num_ente==instancia){
                            $('#instancia').append('<option selected value="'+value.pk_num_organismo+'">'+value.ind_descripcion_empresa+'</option>');
                            $('#s2instancia .select2-chosen').html(value.ind_descripcion_empresa);
                        } else {
                            $('#instancia').append('<option value="'+value.pk_num_ente+'">'+value.ind_nombre_ente+'</option>');
                        }
                    });
                }

            },'json');

        });

    });


</script>