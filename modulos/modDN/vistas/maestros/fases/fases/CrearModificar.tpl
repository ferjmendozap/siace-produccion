<form action="{$_Parametros.url}modDN/maestros/fases/fasesCONTROL/CrearModificarMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idFase}" name="idFase"/>

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="col-sm-3">
                <div class="form-group floating-label" id="cod_faseError">
                    <input type="text" class="form-control" maxlength="3" name="form[alphaNum][cod_fase]" id="cod_fase" {if isset($dataBD.cod_fase)} value="{$dataBD.cod_fase}" disabled {/if} >
                    <label for="cod_fase"><i class="md md-border-color"></i> Codigo</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="ind_descripcionError">
                <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion"  value="{if isset($dataBD.fase)}{$dataBD.fase}{/if}" {if $ver==1 } disabled{/if}>
                <label for="ind_descripcion"><i class="md md-border-color"></i> Descripcion</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="idTipoActuacionError" style="margin-top: -10px;">
                <select id="idTipoActuacion" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_actuacion]"
                        class="form-control select2" {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=i from=$tipoActuacion}
                        {if isset($dataBD.fk_a006_num_miscelaneo_detalle_tipo_actuacion) and $dataBD.fk_a006_num_miscelaneo_detalle_tipo_actuacion == $i.pk_num_miscelaneo_detalle}
                            <option value="{$i.pk_num_miscelaneo_detalle}" selected >{$i.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_tipo_actuacion"> Tipo de Actuación:</label>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($dataBD.num_estatus) and $dataBD.num_estatus==0} checked="" {else} checked="checked" {/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 ">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.ind_usuario)}{$dataBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.fec_ultima_modificacion)}{$dataBD.fec_ultima_modificacion}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro"
                data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if !$ver==1 }
            <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
                {if $idFase!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        /// Complementos
        var app = new  AppFunciones();
        $('.select2').select2({ allowClear: true });

        // ancho de la Modal
        $('#modalAncho').css( "width", "40%" );

        // anular accion del Formulario
        $("#formAjax").submit(function(){
            return false;
        });

        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        //Guardado y modales
        $('#accion').click(function(){

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = '';
                var arrayMostrarOrden = ['cod_fase','ind_descripcion','idTipoActuacion','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_fase'],'idFase',arrayCheck,arrayMostrarOrden,'La Fase fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['pk_num_fase'],'pk_num_fase',arrayCheck,arrayMostrarOrden,'La Fase fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });


    });


</script>