
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">LISTADO DE ACTIVIDADES</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Fase</th>
                            <th>Descripcion</th>
                            <th>Auto Archivo</th>
                            <th>No Afecta</th>
                            <th>Estado</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$grupo=0}
                        {foreach item=i from=$dataBD}
                            {if ($grupo != $i.pkTipoActuacion)}
                                {$grupo = $i.pkTipoActuacion}
                                <tr style="font-weight:bold; background-color:#C7C7C7;">
                                    <td>{$i.tipoActuacion}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            {/if}
                            <tr id="tipoActuacion{$i.pk_num_actividad}">
                                <td>{$i.cod_actividad}</td>
                                <th>{$i.fase}</th>
                                <td>{$i.actividad}</td>
                                <td><i class="{if $i.num_flag_auto_archivo==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td><i class="{if $i.num_flag_no_afecto_plan==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td><i class="{if $i.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td>
                                    <button class="modificar btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idActividad="{$i.pk_num_actividad}" title="Editar"
                                            descipcion="El Usuario ha Modificado una Actividad" titulo="<i class='fa fa-edit'></i> Editar Actividad">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>

                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idActividad="{$i.pk_num_actividad}" title="Consultar"
                                            descipcion="El Usuario esta viendo una Actividad" titulo="<i class='md md-remove-red-eye'></i> Consultar Actividad">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idActividad="{$i.pk_num_actividad}" title="Eliminar"  boton="si, Eliminar"
                                            descipcion="El usuario ha eliminado una Actividad" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Actividad??">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>

                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7"><button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descipcion="el Usuario ha creado un post de actividad" data-toggle="modal"
                                                    data-target="#formModal" titulo="<i class='md md-create'></i>&nbsp;Registrar Nueva Actividad" id="nuevo"
                                                    data-keyboard="false" data-backdrop="static">
                                    <i class="md md-create"></i>&nbsp;Nueva Actividad
                                </button>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {

        var url='{$_Parametros.url}modDN/maestros/actividades/actividadesCONTROL/crearModificarMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idActividad:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idActividad: $(this).attr('idActividad')},function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{ idActividad: $(this).attr('idActividad') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idActividad=$(this).attr('idActividad');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modDN/maestros/actividades/actividadesCONTROL/eliminarMET';
                $.post($url, { idActividad: idActividad },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idActividad'+dato['idActividad'])).html('');
                        swal("Eliminado!", "La Actividad ha sido eliminada satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>