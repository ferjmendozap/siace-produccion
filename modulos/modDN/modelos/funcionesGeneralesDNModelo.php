<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * Proceso: Funciones de propósito general para el módulo de DN.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class funcionesGeneralesDNModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');

    }
    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: extrae los valores del miscelaneo indicado
     * Retorna: Correlativo en texto
     * Parámetros:
     *  $valor, indicador del miscelaneo a extraer.
     */
    public function metMiscelaneo($valor, $valor2=NULL){

        $cargo = $this->_db->query(
            "SELECT pk_num_miscelaneo_detalle, ind_nombre_detalle, cod_detalle
             FROM a006_miscelaneo_detalle
             WHERE fk_a005_num_miscelaneo_maestro=(SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='$valor')
             $valor2"
        );
        $cargo->setFetchMode(PDO::FETCH_ASSOC);
        return $cargo->fetch();
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: extrae el parametro de la tabla a035_parametros de acuerdo al valor indicado
     * Retorna: Correlativo en texto
     * Parámetros:
     *  $valor, indicador del parametro a extraer.
     */
    public function metParametros($valor){

        $cargo = $this->_db->query(
            "SELECT ind_valor_parametro
             FROM a035_parametros
             WHERE ind_parametro_clave='$valor'"
        );
        $cargo->setFetchMode(PDO::FETCH_ASSOC);
        return $cargo->fetch();
    }

    /**
     * CREADO POR: Alexis Ontiveros
     * DESCRIPCIÓN: Genera el corelativo para crear un nuevo código
     * Aplicado para la generación del correlativo correspondiente al codigo de las denuncias, quejas, reclamos y sugerencias
     * Retorna: Correlativo en texto
     * Parámetros:
     *  $tabla, nombre de la tabla.
     *  $campo, campo que correspondencia al código.
     *  $digitos, Cantidad de dígitos.
     *  $campo2, Campo por el cual filtar.
     *  $valor2, Valor de filtro.
    */
    public function metGeneraCodigo($tabla, $campo, $digitos, $campo2=null, $valor2=null){
        $sqlCriterio = "";
        $campos=" COUNT($campo) $campo ";
        if($campo2 and $valor2){
            $sqlCriterio = " WHERE $campo2 = '$valor2' ";
        }
        $sqlCriterio.=" ORDER BY $campo DESC LIMIT 1 ";
        $sql_query = "SELECT $campos FROM $tabla".$sqlCriterio;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetch();
        $codigo=$rcset[$campo]+1;
        $codigo=(string) str_repeat("0", $digitos-strlen($codigo)).$codigo;
        return ($codigo);
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: genera correlativo con los parametros indicados donde
     * $tabla -> indica la tabla a buscar
     * $campo -> indicael campo referencia para la generación del codigo
     * $campo2 -> campo de consulta condicional (opcional)
     * $valor2 -> valor del campo condicional (opcional)
     * Retorna: Array.
     */
    public function metCorrelativo($tabla, $campo, $campo2=null, $valor2=null,$campo3=null, $valor3=null){
        $sqlCriterio = "";
        $campos=" SUBSTRING($campo, -2) $campo ";
        if($campo2 and $valor2){
            $sqlCriterio = " WHERE $campo2 = $valor2 ";
        }
        if($campo3 and $valor3){
            $sqlCriterio = $sqlCriterio." and $campo3 = $valor3 ";
        }
        $sqlCriterio.=" ORDER BY $campo DESC LIMIT 1 ";
        $sql_query = "SELECT $campos FROM $tabla".$sqlCriterio;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetch();
        $codigo=(int) ($rcset[$campo]+1);
        return ($codigo);
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: consulta una tabla x con los parametros indicados retornando un array simple de un registro donde
     * $tabla -> indica la tabla a buscar
     * $cadena_campo -> indica el/los campos a consultar
     * $where -> consicionales a cumplir
     * Retorna: Array.
     */
    public function metConsultaDato($tabla, $cadena_campo, $where){
        $sql_query = "SELECT $cadena_campo FROM $tabla where ".$where;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetch();
        return $rcset;
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: consulta una tabla x con los parametros indicados retornando un array matricial donde
     * $tabla -> indica la tabla a buscar
     * $cadena_campo -> indica el/los campos a consultar
     * $where -> consicionales a cumplir
     * Retorna: Array.
     */
    public function metConsultaTabla($tabla, $cadena_campo, $where){
        $sql_query = "SELECT $cadena_campo FROM $tabla where ".$where;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetchAll();
        return $rcset;
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: Lista los entes sujetos a control activos
     * Retorna: Array.
     */
    public function metEnteSujetoControl(){
    $result = $this->_db->query("
        SELECT pk_num_ente, 
        ind_nombre_ente, num_ente_padre
        FROM a039_ente 
        where num_sujeto_control=1 and 	num_estatus=1
        ORDER BY pk_num_ente, num_ente_padre");

    $result->setFetchMode(PDO::FETCH_ASSOC);
    $rcset = $result->fetchAll();
    return $rcset;
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: Lista los dependencias internas que ejercen control fiscal
     * Retorna: Array.
     */
    public function metDependenciaInternaControl(){
        $result = $this->_db->query("
            SELECT * FROM `a004_dependencia` 
            WHERE `num_flag_controlfiscal` = 1 
            AND `num_estatus` = 1 ");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetchAll();
        return $rcset;
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: Busca la contraloría activa
     * Retorna: Array.
     */
    public function metContraloria($contraloria){
        $result = $this->_db->query("
        SELECT pk_num_organismo, 
        ind_descripcion_empresa
        FROM a001_organismo 
        where pk_num_organismo='$contraloria' and 	num_estatus=1");

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetchAll();
        return $rcset;
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: Lista los sectores activos filtrados por parroquia
     * Retorna: Array.
     */
    public function metJsonSector($idParroquia)
    {
        $sector = $this->_db->query(
            "SELECT
               *
            FROM
                a013_sector
            WHERE fk_a012_num_parroquia='$idParroquia' AND num_estatus=1");

        $sector->setFetchMode(PDO::FETCH_ASSOC);

        return $sector->fetchAll();
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: Lista los centros de costo activos filtrados por dependencia
     * Retorna: Array.
     */
    public function metJsonCentroCosto($idDependencia)
    {
        $sector = $this->_db->query(
            "SELECT
               *
            FROM
                a023_centro_costo
            WHERE fk_a004_num_dependencia='$idDependencia' AND num_estatus=1");

        $sector->setFetchMode(PDO::FETCH_ASSOC);

        return $sector->fetchAll();
    }

    /**
     * CREADO POR       :Alexis Ontiveros, 30 Sep 16
     * DESCRIPCIÓN      :Formatea la fecha para la base de datos
     * Retorna          :la en formato "año-mes-dia".
     * Parámetros.
     *   $campos:       : La fecha en formato "yyyy-mm-dd".
     */
    public function metFormateaFechaMsql($fecha){
        $fecha=trim($fecha);
        if(!$fecha OR $fecha=='null' OR $fecha=='NULL'){
        $fechaMsql="0000-00-00";
        }else{
        $arrfecha=explode('-',$fecha);
           if(strlen($arrfecha[0])==2){
               $arrfecha[0]=(string) str_repeat("0", 2-strlen($arrfecha[0])).$arrfecha[0];
               $arrfecha[1]=(string) str_repeat("0", 2-strlen($arrfecha[1])).$arrfecha[1];
               $fechaMsql=$arrfecha[2].'-'.$arrfecha[1].'-'.$arrfecha[0];
           }else{
               $fechaMsql=$fecha;
           }
        }
        return $fechaMsql;
    }

    /**
     * Sumar días hábiles
     * Creado por: Trino Cordero.
     */
    function metSumarDiasHabiles($fechaInicio, $dias)
    {
    $fecha = new DateTime($fechaInicio);
    $anio = $fecha->format('Y');
    //Lista de dias feriados en el año
    $listaDiasFeriados = $this->metObtenerListaDiasFeriados("$anio-01-01", "$anio-12-31");
    //Contar días hábiles
        for ($i = 2; $i <= $dias; $i++) {
        //Sumar un día y verificar
        $fecha->add(new DateInterval('P01D'));
        //Obtener día de la semana
        $diaSemana = $this->metObtenerDiaSemana($fecha->format('Y-m-d'));
            if ($diaSemana == 6) {
            //Si el día de la semana es sábado, sumarle dos días para que sea lunes
            $fecha->add(new DateInterval('P02D'));
            }elseif ($diaSemana == 0 ) {
            $fecha;
            } else {
            $iterar = true;
                while ($iterar) {
                //VERIFICAR SI ES FERIADO
                $fechaString = $fecha->format('Y-m-d');
                //Si el día es feriado y no es sábado o domingo, sumarle un día
                    if (in_array($fechaString, $listaDiasFeriados) && !in_array($this->metObtenerDiaSemana($fechaString), array(0, 6))) {
                    $fecha->add(new DateInterval('P01D'));
                        if ($this->metObtenerDiaSemana($fecha->format('Y-m-d')) == 6) {
                        $fecha->add(new DateInterval('P02D'));
                        }
                    } else {
                    $iterar = false;
                    }
                }
            }
        }
    return $fecha->format('Y-m-d');
    }

    /**
     * Buscar días feríados
     * Creado por: Trino Cordero.
     */
    public function metObtenerNroDiasFeriados ($fdesde, $fhasta)
    {
    list($anioDesde, $mesDesde, $diaDesde) = explode('-', $fdesde);
    list($anioHasta, $mesHasta, $diaHasta) = explode('-', $fhasta);
    $diamesDesde = "$mesDesde-$diaDesde";
    $diamesHasta = "$mesHasta-$diaHasta";
    $sql = "select *
    from rh_c069_feriados
    where (num_flag_variable = 1
    and (fec_anio = '$anioDesde' or fec_anio = '$anioHasta')
    and (fec_dia >= '$diamesDesde' and fec_dia <= '$diamesHasta'))
    or (num_flag_variable = 0 and fec_dia >= '$diamesDesde' and fec_dia <= '$diamesHasta')";

    $result = $this->_db->query($sql);
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $diasFeriados = $result->fetchAll();
    $nroDiasFeriados = 0;
    foreach ($diasFeriados as $diaFeriado) {
    list($mes, $dia) = explode('-', $diaFeriado['fec_dia']);
    if ($diaFeriado['fec_anio'] == "") {
    $anio = $anioDesde;
    } else {
    $anio = $diaFeriado['fec_anio'];
    }
    $fecha = "$anio-$mes-$dia";
    $diaSemana = $this->metObtenerDiaSemana($fecha);
    if ($diaSemana >= 1 && $diaSemana <= 5) {
    $nroDiasFeriados++;
    }
    if ($anioDesde != $anioHasta) {
    if ($diaFeriado['fec_anio'] == "") {
    $anio = $anioHasta;
    } else {
    $anio = $diaFeriado['fec_anio'];
    }
    $fecha = "$anio-$mes-$dia";
    $diaSemana = $this->metObtenerDiaSemana($fecha);

    if ($diaSemana >= 1 && $diaSemana <= 5) {
    $nroDiasFeriados++;
    }
    }
    }
    return $nroDiasFeriados;
    }

    /**
     * Obtener un arreglo de días feriados
     * Creado por: Trino Cordero.
     */
    public function metObtenerListaDiasFeriados ($fdesde, $fhasta)
    {
    list($anioDesde, $mesDesde, $diaDesde) = explode('-', $fdesde);
    list($anioHasta, $mesHasta, $diaHasta) = explode('-', $fhasta);
    $diamesDesde = "$mesDesde-$diaDesde";
    $diamesHasta = "$mesHasta-$diaHasta";
    $sql = "select *
    from rh_c069_feriados
    where (num_flag_variable = 1
    and (fec_anio = '$anioDesde' or fec_anio = '$anioHasta')
    and (fec_dia >= '$diamesDesde' and fec_dia <= '$diamesHasta'))
    or (num_flag_variable = 0 and fec_dia >= '$diamesDesde' and fec_dia <= '$diamesHasta')";
    $result = $this->_db->query($sql);
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $diasFeriados = $result->fetchAll();
    $listaDiasFeriados = array();
    foreach ($diasFeriados as $diaFeriado) {

    //Falta el caso cuando se cambia a un nuevo año OJO OJO OJO

    $anio = $anioDesde;

    $fecha = "$anio-{$diaFeriado['fec_dia']}";
    $listaDiasFeriados[] = $fecha;
    }
    return $listaDiasFeriados;
    }
    /**
     * Buscar el día de la semana
     * Creado por: Trino Cordero.
     */
    function metObtenerDiaSemana($fecha) {
    // primero creo un array para saber los días de la semana
    $dias = array(0, 1, 2, 3, 4, 5, 6);
    $dia = substr($fecha, 8, 2);
    $mes = substr($fecha, 5, 2);
    $anio = substr($fecha, 0, 4);
    // en la siguiente instrucción $pru toma el día de la semana, lunes, martes,
    $pru = $dias[date("w", mktime(0, 0, 0, $mes, $dia, $anio))];
    return $pru;
    }
    /**
     * Buscar días hábiles
     * Creado por: Trino Cordero.
     */
    function metObtenerDiasHabiles ($fdesde, $fhasta) {
    //Diferencia entre días
    $objFechaDesde = new DateTime($fdesde);
    $objFechaHasta = new DateTime($fhasta);
    $intervalo = $objFechaDesde->diff($objFechaHasta);
    $diasCompletos = (int) $intervalo->format('%a');
    $diasFeriados = $this->metObtenerNroDiasFeriados($fdesde, $fhasta);
    $diaSemana = $this->metObtenerDiaSemana($fdesde);
    $diasHabiles = 0;
    for ($i = 0; $i <= $diasCompletos; $i++) {
    if ($diaSemana >= 1 && $diaSemana <= 5) {
    $diasHabiles++;
    }
    $diaSemana++;

    if ($diaSemana == 8) {
    $diaSemana = 0;
    }
    }
    $diasHabiles -= $diasFeriados;
    return $diasHabiles;
    }

    /**
     * CREADO POR       :Alexis Ontiveros, 19 Otc 16
     * DESCRIPCIÓN      :Crear un array multidimensional a partír de un array clave - valor.
     * Retorna          :Un array agrupado.
     * Parámetros.
     *  $array          :Array a tratar.
     *  $clave_agrupar  :Nombre del elemento contenido en $array el cual su valor a ser el nombre del grupo.
     *  $clave_prefijo  :Nombre del elemento contenido en el array, cuyo valor será el prefijo del nombre del grupo. Nota: OPCIONAL.
     */
    public function metAgrupaArray(&$array, $clave_agrupar, $clave_prefijo=NULL) {
        $array_filtrado = array();
        $prefijo="";
        foreach($array as $index=>$array_value) {//se obtiene en $array_value el set de arrays para asignarlo cada valor de $clave_agrupar.
            if($clave_prefijo){//Se determina el prefijo.
                $prefijo=$array_value[$clave_prefijo].' ';
            }
            $value = $array_value[$clave_agrupar]; //Se obtiene el valor de la clave.
            unset($array_value[$clave_agrupar]);//se elimina la clave.
            $value=$prefijo.$value;//Se concatena el valor de la clave al prefijo.
            $array_filtrado[$value][] = $array_value;//Se asigna el set de arrays al elemento grupo.
        }
        $array = $array_filtrado; // array multidimencional con los rows agrupados.
        return $array;
    }


    /**
     * CREADO POR   :Alexis Ontiveros, 15 Nov 16
     * DESCRIPCIÓN  :Formatea fecha del tipo ####-##-## a ##-##-####.
     * Retorna      :la fecha formateada.
     * Parámetros   :$fecha, la fecha ####-##-##.  $fechaLarga: true, implica mostrar con hora si la fecha es larga
     */
    public function metFormateaFecha($fecha, $fechaLarga=NULL){
        $hora='';
        if(!$fecha OR $fecha=='null' OR $fecha=='NULL'){
            $fechaNormal="00-00-0000";
        }else{
            $arrfecha=explode('-',$fecha);
            if(strlen($arrfecha[2])>2){
                $arrDia=explode(' ',$arrfecha[2]);
                $dia=$arrDia[0];
                if($fechaLarga){$hora=' '.$arrDia[1];}
            }else{
                $dia=$arrfecha[2];
            }
            $fechaNormal=$dia.'-'.$arrfecha[1].'-'.$arrfecha[0].$hora;
        }
        return $fechaNormal;
    }

    /**
     * CREADO POR: GUIDMAR ESPINOZA
     * DESCRIPCIÓN: Suma los días de la actividades activas por tipo de actuación
     * Recibe: pk de la actuación
     * Retorna: Valor entero.
     */
    public function metSumarDiasActividades($tipoActuacion){

        $result = $this->_db->query("
        SELECT sum(num_duracion) as Total
        FROM dn_c002_actividad
        where ind_tipo_actuacion=".$tipoActuacion." and num_estatus=1 and dn_c002_actividad.num_flag_no_afecto_plan=0");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetch();
        return $rcset['Total'];
    }


    public function metBuscarPredeterminado()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *, a004_dependencia.ind_dependencia 
            FROM
            rh_c076_empleado_organizacion 
            INNER JOIN a004_dependencia ON pk_num_dependencia=fk_a004_num_dependencia 
            WHERE
            fk_rhb001_num_empleado='$this->atIdUsuario'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }


    public function metListaDependenciasInt(){
        $sql= $this->_db->query("
            SELECT 
              a004.ind_dependencia, a004.pk_num_dependencia 
            FROM a004_dependencia a004 
            INNER JOIN a019_seguridad_dependencia 
              ON a019_seguridad_dependencia.`fk_a018_num_seguridad_usuario`=$this->atIdUsuario
              and fk_a015_num_seguridad_aplicacion=(SELECT pk_num_seguridad_aplicacion FROM a015_seguridad_aplicacion WHERE cod_aplicacion='DN') 
              and a004.pk_num_dependencia=a019_seguridad_dependencia.fk_a004_num_dependencia 
            WHERE a004.num_estatus='1' 
                    ");

        $sql->setFetchMode(PDO::FETCH_ASSOC);
        return $sql->fetchAll();
    }


    public function metDiferenciaDias($inicio, $fin){
        $inicio = strtotime($inicio);
        $fin = strtotime($fin);
        $dif = $fin - $inicio;
        $diasFalt = (( ( $dif / 60 ) / 60 ) / 24);
        return ceil($diasFalt);
    }
}
