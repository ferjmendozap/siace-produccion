<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class ejecucionControlador extends Controlador
{
    private $atSolicitudModelo;
    private $atMiscelaneoModelo;
    private $atDependenciaModelo;
    private $atFunGn;
    private $atEjecucionModelo;
    private $atDenunciasModelo;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEjecucionModelo = $this->metCargarModelo('ejecucion', 'ejecucion');
        $this->atDenunciasModelo = $this->metCargarModelo('denuncias', 'denuncias');
        $this->atSolicitudModelo = $this->metCargarModelo('solicitud', 'solicitud');
        $this->atFunGn = $this->metCargarModelo('funcionesGeneralesDN');
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('ejecucionPost', 1);
        //$this->atVista->assign('listado', $this->atEjecucionModelo->metListarTramites());
        $this->atVista->metRenderizar('listadoEjecucion');

    }

    public function metEjecutarActividad(){
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js = array('Aplicacion/appFunciones',
            'modRH/modRHFunciones',
            'materialSiace/core/demo/DemoFormWizard');
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idDenuncia = $this->metObtenerInt('idDenuncia');

        if($valido==1){
            $this->metValidarToken();
            if(isset($_POST['valorar_cerrar'])){
                $valorar=$this->metObtenerTexto('valorar_cerrar');
            }else{
                $valorar='false';
            }
            if(isset($_POST['archivo_remitir'])){
                $remitir=$this->metObtenerTexto('archivo_remitir');
            }else{
                $remitir='false';
            }

            if($valorar!='VA' and $remitir!='RM'){
                $Excceccion2 = array('codDenuncia', 'ind_observacion', 'ind_antecedente_expediente', 'ind_analisis_hechos_expediente', 'ind_valoracion_expediente', 'ind_conclusion_valoracion', 'ind_manifiesto', 'ind_recomendaciones');
                $Excceccion = array('idDenuncia', 'tipoInstancia', 'instancia');
            }elseif($valorar=='VA' and $remitir!='RM'){
                $Excceccion2 = array('codDenuncia', 'ind_observacion',  'instancia');
                $Excceccion = array('idDenuncia', 'tipoInstancia', 'instancia');
            }else{
                $Excceccion2 = array('codDenuncia', 'ind_observacion');
                $Excceccion = array('idDenuncia', 'instancia');
            }


            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excceccion);
            $texto = $this->metValidarFormArrayDatos('form', 'txt', $Excceccion2);
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');

            if ($alphaNum != null && $ind == null && $texto == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $texto == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $texto != null) {
                $validacion = $texto;
            } elseif ($alphaNum == null && $ind != null && $texto != null) {
                $validacion = array_merge($texto, $ind);
            } elseif ($alphaNum != null && $ind == null && $texto != null) {
                $validacion = array_merge($texto, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $texto == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $texto);
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            ####   Llama a la Funcion para guardar los cambios ####
            $id = $this->atEjecucionModelo->metEjecutarActividad($validacion);
            $detalles = $this->atEjecucionModelo->metBuscarActividadesTerminadas($validacion['idDenuncia']);
            if($detalles['cantidad']==0) {
                $id2 = $this->atEjecucionModelo->metTerminarDenuncia($validacion['idDenuncia'],$validacion['fec_termino_act']);
                $validacion['id2'] = $id2;
            }

            $validacion['id'] = $id;
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            echo json_encode($validacion);
            exit;
        }

        $tipoActuacion = $this->metObtenerTexto('tipoActuacion');
        $pkActividad=$this->metObtenerInt('idActividad');
        $this->atVista->assign('idActividad', $pkActividad);
        $this->atVista->assign('idDenuncia', $idDenuncia);
        $pk_actuacion = $this->atFunGn->metMiscelaneo('TACT', ' and a006_miscelaneo_detalle.cod_detalle="'.$tipoActuacion.'"');
        $this->atVista->assign('tramite', $pk_actuacion['ind_nombre_detalle']);
        $this->atVista->assign('selectEnte', $this->atFunGn->metEnteSujetoControl());
        $this->atVista->assign('formDB', $this->atSolicitudModelo->metMostrarSolicitud($idDenuncia));
        $result=$this->atDenunciasModelo->metActividadesAsignadas($idDenuncia);

        if(count($result)>0) {
            $fec_inicio_real='';
            foreach ($result as $fila) {

                /////   Semaforo de las actividades   /////
                if($fila['ind_estatus']=='PE' or $fila['ind_estatus']=='EJ'){
                    list($a,$m,$d)=explode('-',$fila['fec_inicio']);
                    if($a==date('Y')){
                        if(($d>date('d') and $m>=date('m')) or ($d==date('d') and $m>date('m')) or ($d<date('d') and $m>date('m'))){
                            $fila['semaforo']='alert alert-warning';
                        }elseif(($d<date('d') and $m<=date('m')) or ($d==date('d') and $m<date('m'))){
                            $fila['semaforo']='alert alert-danger';
                        }else{
                            $fila['semaforo']='alert alert-warning';
                        }
                    }elseif($a<date('Y')){
                        $fila['semaforo']='alert alert-danger';
                    }else{
                        $fila['semaforo']='alert alert-warning';
                    }
                }else{
                    $fila['semaforo']='alert alert-success';
                }
                if($fila['pk_num_detalle_dc']==$pkActividad and $fila['semaforo']!='alert alert-danger'){
                    $fila['semaforo']='alert alert-info';
                }

                if($fila['fec_inicio_real']=='' or $fila['fec_inicio_real']=='00-00-0000' or $fila['fec_inicio_real']=='0000-00-00'){
                    if($fec_inicio_real==''){
                        $fec_inicio_real=$fila['fec_inicio'];
                    }else{
                        $fec_inicio_real=$fec_inicio_real;
                    }

                }else{
                    $fec_inicio_real=$fila['fec_inicio_real'];
                }
                $duracionT=$fila['num_duracion'] + $fila['num_prorroga'];

                $fecha_fin_actividad=$this->atFunGn->metSumarDiasHabiles($fec_inicio_real, (int) $duracionT);
                $fila['fec_inicio'] = $this->atFunGn->metFormateaFecha($fila['fec_inicio']);
                $fila['fec_inicio_real'] = $this->atFunGn->metFormateaFecha($fec_inicio_real);
                $fila['fec_termino'] = $this->atFunGn->metFormateaFecha($fila['fec_termino']);
                if($fila['fec_termino_real']=='0000-00-00' or $fila['fec_termino_real']=='' or $fila['fec_termino_real']=='00-00-0000'){
                    $fila['fec_termino_real'] = $this->atFunGn->metFormateaFecha($fecha_fin_actividad);
                    $fec_inicio_real=$this->atFunGn->metSumarDiasHabiles($fecha_fin_actividad, 2);
                } else{
                    $fila['fec_termino_real'] = $this->atFunGn->metFormateaFecha($fila['fec_termino_real']);
                    $fec_inicio_real=$this->atFunGn->metSumarDiasHabiles($fila['fec_termino_real'], 2);
                }

                $fila['num_flag_no_afecto_plan'] =$fila['num_flag_no_afecto_plan'];
                $fila['num_duracion'] =$fila['num_duracion'];
                $fila['num_prorroga'] =$fila['num_prorroga'];
                $fila['num_flag_auto_archivo'] =$fila['num_flag_auto_archivo'];
                $fila['pk_num_detalle_dc'] =$fila['pk_num_detalle_dc'];
                $fila['ind_estatus'] =$fila['ind_estatus'];
                $arrData[]=$fila;
            }
        }

        $arrData=$this->atFunGn->metAgrupaArray($arrData,'nombre_fase','cod_fase');
        $this->atVista->assign('listaActividades', $arrData);

        $this->atVista->assign('ver', $this->metObtenerInt('ver'));
        $this->atVista->assign('actividad', $this->atEjecucionModelo->metMostarActividad($pkActividad));
        $actividad=$this->atEjecucionModelo->metMostarActividad($pkActividad);
        $this->atVista->assign('selectDependenciaInt', $this->atSolicitudModelo->atDependenciaModelo->metListarTipoDependenciaInt());
        $this->atVista->assign('selectDependenciaExt', $this->atSolicitudModelo->atDependenciaModelo->metListarTipoDependencia());

        if($actividad['fec_inicio_real']!='0000-00-00'){
            $num_dias_cierre=$this->atFunGn->metDiferenciaDias($actividad['fec_inicio_real'], date('Y-m-d'));
            $this->atVista->assign('dias_cierre', $num_dias_cierre);
        }
        $this->atVista->assign('dias_cierre', 0);

        $this->atVista->metRenderizar('ejecucion', 'modales');
    }

    public function metJsonInstancia()
    {
        $tipoInstancia = $this->metObtenerInt('tipoInstancia');
        if($tipoInstancia==1){
            $instancia=$this->atFunGn->metDependenciaInternaControl();
        }else{
            $instancia=$this->atFunGn->metConsultaTabla('a039_ente', ' pk_num_ente, ind_nombre_ente ', ' num_sujeto_control=1 OR num_sujeto_control=0');
        }

        echo json_encode($instancia);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT *, 
                    dn_d001_actividades.ind_estatus as estadoAct,
                    a039_ente.ind_nombre_ente , 
                    act.ind_nombre_detalle as tipoActuacion, 
                    act.cod_detalle as tipoAct,
                    estado.ind_nombre_detalle as estado, 
                    dn_c002_actividad.ind_descripcion as Actividad, 
                    dn_c001_fase.ind_descripcion as Fase,
                    CONCAT(dn_c001_fase.ind_descripcion,'-',dn_c002_actividad.ind_descripcion) as FA
                FROM dn_b001_denuncia 
                INNER JOIN dn_d001_actividades 
                  ON dn_d001_actividades.fk_dnb001_num_denuncia=dn_b001_denuncia.pk_num_denuncia
                INNER JOIN dn_c002_actividad 
                  ON dn_d001_actividades.fk_dnc002_num_actividad=dn_c002_actividad.pk_num_actividad
                INNER JOIN dn_c001_fase 
                  ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase
                LEFT JOIN a039_ente 
                  ON dn_b001_denuncia.fk_a039_num_ente = a039_ente.pk_num_ente 
                LEFT JOIN a006_miscelaneo_detalle as estado 
                  ON estado.fk_a005_num_miscelaneo_maestro= 
                      (SELECT pk_num_miscelaneo_maestro 
                        FROM a005_miscelaneo_maestro 
                        WHERE cod_maestro='DN-EST-ACT') 
                      and estado.cod_detalle = dn_d001_actividades.ind_estatus 
                LEFT JOIN a006_miscelaneo_detalle as act 
                  ON act.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion 
                WHERE ind_num_denuncia!='' 
                  AND dn_d001_actividades.ind_estatus!='TE' 
                  AND dn_d001_actividades.ind_estatus!='VA' 
                  AND dn_d001_actividades.ind_estatus!='RM' 
                  AND (dn_b001_denuncia.ind_estatus_tramite='AP' 
                  OR dn_b001_denuncia.ind_estatus_tramite='EJ')
                ";

        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                AND 
                    ( 
                      dn_c002_actividad.ind_descripcion LIKE '%$busqueda[value]%' OR 
                      act.ind_nombre_detalle LIKE '%$busqueda[value]%' OR 
                      a039_ente.ind_nombre_ente LIKE '%$busqueda[value]%' OR
                      dn_c001_fase.ind_descripcion LIKE '%$busqueda[value]%'
                    )
            ";
        }

        $sql .=" GROUP BY dn_b001_denuncia.ind_num_denuncia ";

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_num_denuncia','tipoActuacion', 'ind_nombre_ente', 'estado','FA');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_detalle_dc';
        #construyo el listado de botones
        $camposExtra = array('pk_num_denuncia','tipoAct');

        if (in_array('DN-01-02-01-M',$rol)) {
            $campos['boton']['Ejecutar'] = array("
                <button accion='ejecutar' title='Ejecutar'
                        class='ejecutar logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Modificado la Actividad Nro.pk_num_detalle_dc del tramite pk_num_denuncia'
                        idActividad='pk_num_detalle_dc' titulo='Ejecutar Actividad'
                        idDenuncia='pk_num_denuncia'  tipo='tipoAct'
                        data-toggle='modal' data-target='#formModal' id='ejecutar'>
                    <i class='fa fa-play'></i>
                </button>
                ",
                'if( $i["estadoAct"] !="TE" OR $i["estadoAct"] !="CO") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Ejecutar'] = false;
        }

        if (in_array('DN-01-02-02-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar Actividad"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado la Actividad Nro. pk_num_detalle_dc del tramite post.pk_num_denuncia"
                        idActividad="pk_num_detalle_dc" titulo="Consultar Actividad"
                        idDenuncia="pk_num_denuncia" tipo="tipoAct"
                        data-toggle="modal" data-target="#formModal" id="ver">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }




        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }
}