<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class denunciasControlador extends Controlador
{
    private $atDenunciasModelo;
    private $atMiscelaneoModelo;
    private $atFunGn;
    private $atSectorModelo;
    private $atParroquiaModelo;
    private $atMunicipioModelo;
    private $atTipoParticular;
    public $atFaseModelo;
    private $atSolicitudModelo;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atDenunciasModelo = $this->metCargarModelo('denuncias', 'denuncias');
        $this->atFunGn = $this->metCargarModelo('funcionesGeneralesDN');
        $this->atTipoParticular = $this->metCargarModelo('particular', '', 'modCD');
        $this->atFaseModelo=$this->metCargarModelo('fases');
        $this->atSolicitudModelo=$this->metCargarModelo('solicitud', 'solicitud');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('denunciasPost', 1);
        $this->atVista->assign('listado', $this->atDenunciasModelo->metListarDenuncias());
        $this->atVista->metRenderizar('listadoDenuncias');

    }

    public function metCrearModificar()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js = array('Aplicacion/appFunciones',
            'modRH/modRHFunciones',
            'materialSiace/core/demo/DemoFormWizard');
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idSolicitud = $this->metObtenerInt('idSolicitud');
        $idDenuncia = $this->metObtenerInt('idDenuncia');
        $ver = $this->metObtenerInt('ver');
        //$tipoActuacion= $this->metObtenerInt('tActuacion');
        $tipoActuacionTexto= $this->metObtenerAlphaNumerico('tipoActuacionTexto');

        $estadoContraloria = $this->atFunGn->metParametros('DEFAULTESTADO');
        $cod='';

        $pk_actuacion = $this->atFunGn->metMiscelaneo('TACT', ' and a006_miscelaneo_detalle.cod_detalle="'.$tipoActuacionTexto.'"');

        if ($valido == 1) {

            $this->metValidarToken();
            $Excceccion = array('idDenuncia');
            $Excceccion2 = array('codDenuncia');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excceccion);
            $texto = $this->metValidarFormArrayDatos('form', 'txt', $Excceccion2);
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');

            if ($alphaNum != null && $ind == null && $texto == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $texto == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $texto != null) {
                $validacion = $texto;
            } elseif ($alphaNum == null && $ind != null && $texto != null) {
                $validacion = array_merge($texto, $ind);
            } elseif ($alphaNum != null && $ind == null && $texto != null) {
                $validacion = array_merge($texto, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $texto == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $texto);
            }

            if($validacion['ind_propuesta'] == 'error'){
                $validacion['ind_propuesta'] = NULL;
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }
            if (!isset($validacion['auditor'])){
                $validacion['status']="errorPer";
                echo json_encode($validacion);
                exit;
            }
            if (!isset($validacion['etatusDenuncia'])) {
                $validacion['etatusDenuncia'] = 0;
            }

            if ($validacion['idDenuncia'] == 0) {
                $validacion['accion']='nuevo';
                $validacion['codDenuncia'] = $this->metJsonCodigo($validacion['tActuacion']);
                $id = $this->atDenunciasModelo->metCrearDenuncia($validacion);
                $validacion['status'] = 'nuevo';
            } else {
                if(isset($validacion['acc'])){
                    $id = $this->atDenunciasModelo->metCrearDenuncia($validacion);
                    $validacion['status'] = 'aprobar';
                }else{
                    $validacion['acc']='modificar';
                    $id = $this->atDenunciasModelo->metCrearDenuncia($validacion);
                    $validacion['status'] = 'modificar';
                }

            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['id'] = $id;

            $validacion['idDenuncia'] = $id;
            $cod=$validacion['codDenuncia'];
            echo json_encode($validacion);
            exit;
        }

        if(is_null($_POST['acc'])){
            $acc='';
        }else{
            $acc=$_POST['acc'];
        }

        $dias_actuacion=$this->atFunGn->metSumarDiasActividades($pk_actuacion['pk_num_miscelaneo_detalle']);

        if ($idDenuncia != 0) {
            $this->atVista->assign('acc', $acc);
            $this->atVista->assign('formDB', $this->atSolicitudModelo->metMostrarSolicitud($idDenuncia));
            $this->atVista->assign('formTab', $this->atSolicitudModelo->metMostrarReceptores($idDenuncia));
            $this->atVista->assign('formTab2', $this->atSolicitudModelo->metMostrarDenunciantes($idDenuncia));
            $this->atVista->assign('formTab3', $this->atSolicitudModelo->metMostrarPruebas($idDenuncia));
            $this->atVista->assign('formAuditor', $this->atDenunciasModelo->metMostrarAuditores($idDenuncia));
            $this->atVista->assign('idDenuncia', $idDenuncia);
            $this->atVista->assign('idSolicitud',$idDenuncia);
            $this->atVista->assign('ver', $ver);
            $opcion_actividades='actualizar';
            $this->atVista->assign('listaActividades', $this->metListarActividades($tipoActuacionTexto, $opcion_actividades, $idDenuncia));
        }else{
            $this->atVista->assign('codDenuncia', $cod);
            $this->atVista->assign('acc', 'nuevo');
            $this->atVista->assign('idSolicitud', $idSolicitud);
            $this->atVista->assign('formDB', $this->atSolicitudModelo->metMostrarSolicitud($idSolicitud));
            $this->atVista->assign('formTab', $this->atSolicitudModelo->metMostrarReceptores($idSolicitud));
            $this->atVista->assign('formTab2', $this->atSolicitudModelo->metMostrarDenunciantes($idSolicitud));
            $this->atVista->assign('formTab3', $this->atSolicitudModelo->metMostrarPruebas($idSolicitud));
            $opcion_actividades='nuevo';
            $this->atVista->assign('listaActividades', $this->metListarActividades($tipoActuacionTexto, $opcion_actividades, $idDenuncia));
        }

        $this->atVista->assign('origen',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('DN-01-02'));
        $this->atVista->assign('tipoActuacion',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('TACT'));
        $this->atVista->assign('tipo',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('DN-01-03'));
        $this->atVista->assign('listadoMunicipio', $this->atDenunciasModelo->atMunicipioModelo->metJsonMunicipio($estadoContraloria['ind_valor_parametro']));
        $this->atVista->assign('listadoParroquia', $this->atDenunciasModelo->atParroquiaModelo->metListarParroquia());
        $this->atVista->assign('listadoSector', $this->atDenunciasModelo->atSectorModelo->metListarSector());
        $this->atVista->assign('selectEnte', $this->atFunGn->metEnteSujetoControl());
        $this->atVista->assign('selectDependencia', $this->atFunGn->metListaDependenciasInt());
        $this->atVista->assign('listadoContraloria', $this->atFunGn->metContraloria(1));
        $this->atVista->assign('listadoEmpleados', $this->atSolicitudModelo->metListarEmpleados());
        $this->atVista->assign('dias_actuacion', $dias_actuacion);
        $this->atVista->assign('tipoActuacionTexto', $tipoActuacionTexto);
        $fe_termino = $this->atFunGn->metSumarDiasHabiles(date("Y-m-d"),$dias_actuacion);
        $this->atVista->assign('fecha_termino', $this->atFunGn->metFormateaFecha($fe_termino));
        $this->atVista->assign('fecha_actual', date("d-m-Y"));
        $this->atVista->assign('listaFase',$this->atFaseModelo->metFiltarFase(' WHERE dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion='.$pk_actuacion['pk_num_miscelaneo_detalle']));

        $this->atVista->metRenderizar('planificarEjecucion', 'modales');
    }

    public function metJsonSector()
    {
        $idParroquia = $this->metObtenerInt('idParroquia');
        $sector = $this->atFunGn->metJsonSector($idParroquia);
        echo json_encode($sector);
        exit;
    }

    public function metParticulares($lista=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        if(!$lista){
            $js[] = 'materialSiace/core/demo/DemoTableDynamic';
            $this->atVista->metCargarCssComplemento($complementosCss);
            $this->atVista->metCargarJs($js);
        }
        $this->atVista->assign('listado',$this->atTipoParticular->metListarTipoParticular());

        $this->atVista->metRenderizar('listadoParticular','modales');
    }

    /**
     * Lista las actividades para una planificación nueva o ya existente.
     */
    public function metListarActividades($tipoActuacion, $opcion, $idDenuncia, $f_inicio=false)
    {
        $retornar=array();
        if($opcion=='modificar'){
            $fecha=$f_inicio;
        }else{
            $fecha=date("d-m-Y");
        }
        if($opcion=="actualizar"){/*Si entra, busca las actividades asignadas a la planificación*/
        //revisar y corregir
            $result=$this->atDenunciasModelo->metActividadesAsignadas($idDenuncia);

            if(count($result)>0) {
                foreach ($result as $fila) {
                    $fila['fec_inicio_actividad'] = $this->atFunGn->metFormateaFecha($fila['fec_inicio']);
                    $fila['fec_inicio_real_actividad'] = $this->atFunGn->metFormateaFecha($fila['fec_inicio_real']);
                    $fila['fec_culmina_actividad'] = $this->atFunGn->metFormateaFecha($fila['fec_termino']);
                    $fila['fec_culmina_real_actividad'] = $this->atFunGn->metFormateaFecha($fila['fec_termino_real']);
                    $fila['input_fecha_fin_actividad'] = $fila['fec_termino'];
                    $fila['input_fecha_inicial'] =$fila['fec_inicio'];
//                    $fila['num_flag_no_afecto_plan'] =$fila['num_flag_no_afecto_plan'];
                    $fila['num_dias_duracion_actividad'] =$fila['num_duracion'];
                    $fila['num_dias_prorroga_actividad'] =0;
//                    $fila['num_flag_auto_archivo'] =$fila['num_flag_auto_archivo'];
                    $fila['pk_num_actividad'] =$fila['fk_dnc002_num_actividad'];
                    $arrData[]=$fila;
                    $fTermino=$fila['fec_termino'];
                }
            }
            $this->atVista->assign('fec_termino', $fTermino);
            $arrData=$this->atFunGn->metAgrupaArray($arrData,'nombre_fase','cod_fase');
            $retornar=$arrData;
        }else{ //Se buscan las actividades para asignar a la denuncia
            $fecha_inicio=$this->atFunGn->metFormateaFechaMsql($fecha);
            $result=$this->atDenunciasModelo->metListaActividades($tipoActuacion);
            if(count($result)>0) {
                //Se define la fecha de inicio y fin de cada actividad de acuerdo a la fecha de inicio de la planificación.
                foreach ($result as $fila) {

                    $duracion_actividad=$fila['num_duracion'];
                    $fila['fec_inicio_actividad'] = $fecha_inicio;
                    $fecha_fin_actividad=$this->atFunGn->metSumarDiasHabiles($fecha_inicio, (int) $duracion_actividad);
                    $fila['fec_inicio_real_actividad'] = $fecha_inicio;
                    $duracion_actividad++;
                    $fecha_inicio=$this->atFunGn->metSumarDiasHabiles($fecha_inicio, (int) $duracion_actividad);
                    $fila['num_dias_duracion_actividad'] = $fila['num_duracion']; unset($fila['num_duracion']);
                    $fila['fec_inicio_actividad'] = $this->atFunGn->metFormateaFecha($fila['fec_inicio_actividad']);
                    $fila['input_fecha_inicial'] = $this->atFunGn->metFormateaFechaMsql($fila['fec_inicio_actividad']);
                    $fila['fec_inicio_real_actividad'] = $this->atFunGn->metFormateaFecha($fila['fec_inicio_real_actividad']);
                    $fila['fec_culmina_actividad'] = $this->atFunGn->metFormateaFecha($fecha_fin_actividad);
                    $fila['input_fecha_fin_actividad'] = $fecha_fin_actividad;
                    $fila['fec_culmina_real_actividad'] = $this->atFunGn->metFormateaFecha($fecha_fin_actividad);
                    $fila['num_dias_prorroga_actividad'] = 0;
                    $arrData[]=$fila;

                }
                //Se agrupan las actividades por fase.
                $arrData=$this->atFunGn->metAgrupaArray($arrData,'nombre_fase','cod_fase');
                $retornar=$arrData;
            }
        }

        //$this->atVista->assign('listaActividades', $retornar);
        if($opcion=='modificar'){
            echo json_encode(array("listaActividades"=>$arrData));
        }else{
            return $retornar;
        }
    }

    /**
     * Recalcula las fecha de las actividades.
     */
    public function  metRecalcularActividades(){
        $fecha_inicial = $this->metObtenerTexto('td_fecha_inicio');
        $set_duracion = $this->metObtenerTexto('txt_duracion_actividad');
        $arrDiasDuracion=explode('S',$set_duracion);
        foreach($arrDiasDuracion as $cadena){
            $arrDatos=explode(',',$cadena);
            $dias_duracion=$arrDatos[0];
            $indiceFila=$arrDatos[1];
            $duracion_actividad=$dias_duracion;
            $fecha_inicial=date_format(date_create($fecha_inicial),'Y-m-d');
            $fecha_fin_actividad=$this->atFunGn->metSumarDiasHabiles($fecha_inicial, (int) $duracion_actividad);
            $fila=array('dias_duracion'=>$dias_duracion,'fecha_inicial'=>$fecha_inicial,'fecha_fin_actividad'=>$fecha_fin_actividad,'indiceFila'=>$indiceFila);
            $duracion_actividad++;
            $fecha_inicial=$this->atFunGn->metSumarDiasHabiles($fecha_inicial, (int) $duracion_actividad);
            $fila['fecha_inicial']=date_format(date_create($fila['fecha_inicial']),'d-m-Y');
            $fila['fecha_fin_actividad']=date_format(date_create($fila['fecha_fin_actividad']),'d-m-Y');
            $fila['input_fecha_inicial']=date_format(date_create($fila['fecha_inicial']),'Y-m-d');
            $fila['input_fecha_fin_actividad']=date_format(date_create($fila['fecha_fin_actividad']),'Y-m-d');
            $arrData[]=$fila;
        }
        echo json_encode(array("filas"=>$arrData));
    }

    function metRecalcular(){
        $tipoActuacion=$_POST['tipoActuacion'];
        $opcion_actividades=$_POST['opcion'];
        $f_inicio=$_POST['f_inicio'];

        return json_encode($this->metListarActividades($tipoActuacion, $opcion_actividades, 0, $f_inicio));
    }

    function metRecalcularFechas(){
        $num=$_POST['num'];
        $tr=$_POST['tr'];
        $j=0;
        $arrDias=explode('/',$_POST['dias']);
        $arrFecha=explode('/',$_POST['fechas']);

        $fecha_inicio_sig='';
        for($i=$num; $i<$tr; $i++){
            if($arrFecha[$j]!='') {
                if($j==0) {
                    $fecha_inicio = $this->atFunGn->metFormateaFechaMsql($arrFecha[$j]);
                }else{
                    $fecha_inicio = $fecha_inicio_sig;
                }
                $duracion_actividad = $arrDias[$j];
                $fila['linea'] = $i;
                $fila['fec_inicio'] = $this->atFunGn->metFormateaFecha($fecha_inicio);
                $fila['fec_inicio_real_actividad'] = $this->atFunGn->metFormateaFecha($fecha_inicio);
                $fila['input_fecha_inicial'] = $fecha_inicio;
                $fecha_fin_actividad = $this->atFunGn->metSumarDiasHabiles($fecha_inicio, (int)$duracion_actividad);
                $duracion_actividad++;
                $fecha_inicio_sig=$this->atFunGn->metSumarDiasHabiles($fecha_inicio, (int)$duracion_actividad);
                $fila['fec_culmina_actividad'] = $this->atFunGn->metFormateaFecha($fecha_fin_actividad);
                $fila['fec_culmina_real_actividad'] = $this->atFunGn->metFormateaFecha($fecha_fin_actividad);
                $fila['input_fecha_fin_actividad'] = $fecha_fin_actividad;
                $arrData[] = $fila;
                $j = $j + 1;
            }
        }
        $j=$j-1;
        echo json_encode(array("filas"=>$arrData, "j"=>$j));
    }

    function metJsonCodigo($tipoActuacion=NULL){
        $annio=date('Y');
        if($tipoActuacion==''){
            $tAct=$_POST['tipoActuacion'];
            $f=1;
        }else{
            $tAct=$tipoActuacion;
            $f=2;
        }
        $dig = $this->atFunGn->metParametros('DIGITOS');
        $tipoActuacionTexto = $this->atFunGn->metConsultaDato('a006_miscelaneo_detalle','cod_detalle','pk_num_miscelaneo_detalle='.$tAct);
        $valor = $this->atFunGn->metConsultaDato('dn_b001_denuncia', 'count(pk_num_denuncia)', 'ind_num_denuncia!="NULL" and ind_num_denuncia!="" and fec_anio='.$annio.' and fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion='.$tAct.' GROUP BY fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion ORDER BY `pk_num_denuncia`' );
        $valor=$valor['count(pk_num_denuncia)']+1;
        $correlativo = str_pad($valor, $dig['ind_valor_parametro'], "0", STR_PAD_LEFT);
        $codDenuncia = $this->atFunGn->metMiscelaneo('DN-COD-01', ' and a006_miscelaneo_detalle.cod_detalle="'.$tipoActuacionTexto['cod_detalle'].'"');
        $codDenuncia = $codDenuncia['ind_nombre_detalle'] . '-' . $correlativo . '-' . $annio;
        if($f==1){
            echo json_encode(array("codigo"=>$codDenuncia, "tipoActuacionTexto"=>$tipoActuacionTexto['cod_detalle']));
        }else{
            return $codDenuncia;
        }

    }

}