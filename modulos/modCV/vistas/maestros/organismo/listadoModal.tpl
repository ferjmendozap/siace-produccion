<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Motivos de Visita - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Check</th>
                            <th>Motivo</th>
                            <th>Estatus</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=motivo from=$listado}
                            <tr id="idMotivo{$motivo.pk_num_motivo}">
                                <td>
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" class="valores"
                                                   idMotivo="{$motivo.pk_num_motivo}"
                                                   motivo="{$motivo.ind_detalle}" ">
                                        </label>
                                    </div>
                                </td>
                                <td><label>{$motivo.ind_detalle}</label></td>
                                <td>
                                    <i class="{if $motivo.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" class="btn btn-default ink-reaction btn-raised" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary ink-reaction btn-raised" id="agregarMotivoSeleccionado">Agregar
    </button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#agregarMotivoSeleccionado').click(function () {
            var input = $('.valores');
            for (i = 0; i < input.length; i++) {
                if (input[i].checked == true) {
                    $(document.getElementsByClassName('idMotivo' + input[i].getAttribute('idMotivo'))).remove();
                    $(document.getElementById('motivoVisita')).append(
                            '<tr class="idMotivo' + input[i].getAttribute('idMotivo') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idMotivo') + '" name="form[int][fk_cvc001_num_motivo][]" class="motivoVisitaInput" motivo="' + input[i].getAttribute('motivo') + '" />' +
                            '<td>' + input[i].getAttribute('motivo') + '</td>' +
                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idMotivo' + input[i].getAttribute('idMotivo') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>'
                    );
                }
            }
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>