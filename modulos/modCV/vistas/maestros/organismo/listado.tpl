<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Organismos / Entes Externos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Organismo</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=organismo from=$listado}
                                <tr id="idOrganismo{$organismo.pk_num_organismo}">
                                    <td><label>{$organismo.ind_descripcion_empresa}</label></td>
                                    <td><i class="{if $organismo.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td align="center">
                                        {if in_array('CV-01-03-03-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idOrganismo="{$organismo.pk_num_organismo}" title="Editar"
                                                    descipcion="El Usuario a Modificado un Organismo o Ente Externo" titulo="Modificar Organismo">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        {if in_array('CV-01-03-03-02-M',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idOrganismo="{$organismo.pk_num_organismo}" title="Cosultar"
                                                descipcion="El Usuario esta viendo un Organismo" titulo="<i class='icm icm-calculate2'></i> Ver Organismo">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                        {if in_array('CV-01-03-03-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idOrganismo="{$organismo.pk_num_organismo}" title="Eliminar"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un Organismo o Ente Externo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Organismo!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3">
                                    {if in_array('CV-01-03-03-01-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado un Nuevo un Organismo o Ente Externo"  titulo="<i class='icm icm-cog3'></i> Crear un Organismo o Ente Externo" id="nuevo" >
                                            <i class="md md-create"></i>&nbsp;Nuevo Organismo
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modCV/maestros/organismoCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idOrganismo:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idOrganismo: $(this).attr('idOrganismo')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idOrganismo: $(this).attr('idOrganismo'), ver:1},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idOrganismo=$(this).attr('idOrganismo');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCV/maestros/organismoCONTROL/eliminarMET';
                $.post($url, { idOrganismo: idOrganismo },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idOrganismo'+dato['idOrganismo'])).html('');
                        swal("Eliminado!", "El Organbismo fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>