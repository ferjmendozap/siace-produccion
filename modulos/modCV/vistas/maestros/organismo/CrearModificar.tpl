<form action="{$_Parametros.url}modCV/maestros/organismoCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body ">
        <input type="hidden" value="1" name="valido" />

        {if isset($idOrganismo) }
            <input type="hidden" value="{$idOrganismo}" name="idOrganismo"/>
        {/if}
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_descripcion_empresaError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion_empresa)}{$formDB.ind_descripcion_empresa}{/if}" {if isset($ver) and $ver==1} disabled {/if} name="form[alphaNum][ind_descripcion_empresa]" id="ind_descripcion_empresa">
                        <label for="ind_descripcion_empresa"><i class="icm icm-cog3"></i> Organismo / Ente</label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_direccionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}" {if isset($ver) and $ver==1} disabled {/if} name="form[alphaNum][ind_direccion]" id="ind_direccion">
                        <label for="ind_direccion"><i class="icm icm-cog3"></i> Direccion</label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_telefonoError">
                        <input type="text" class="form-control telefono" data-inputmask="'mask': '(9999) 999-9999'" value="{if isset($formDB.ind_telefono)}{$formDB.ind_telefono}{/if}" {if isset($ver) and $ver==1} disabled {/if} name="form[alphaNum][ind_telefono]" id="ind_telefono">
                        <label for="ind_telefono"><i class="icm icm-cog3"></i> Telefono</label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} {if isset($ver) and $ver==1} disabled {/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
         </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if !$ver==1 }
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            {if isset($idOrganismo) and $idOrganismo!=0}
                <i class="fa fa-edit"></i>&nbsp;Modificar
            {else}
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            {/if}
        </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $(":input").inputmask();

        $('#modalAncho').css("width","40%");
        $('#accion').click(function () {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = ["num_estatus"];
                var arrayMostrarOrden = ['ind_descripcion_empresa', 'ind_direccion','num_estatus'];
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'modificar') {
                    app.metActualizarRegistroTabla(dato, dato['idOrganismo'], 'idOrganismo', arrayCheck, arrayMostrarOrden, 'El Organismo / Ente fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                } else if (dato['status'] == 'nuevo') {
                    app.metNuevoRegistroTabla(dato, dato['idOrganismo'], 'idOrganismo', arrayCheck, arrayMostrarOrden, 'El Organismo / Ente fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>