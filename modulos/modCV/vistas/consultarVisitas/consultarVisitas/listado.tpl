<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">CONSULTA DE VISITAS </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkOrganismo">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="organismo"
                                               class="control-label" style="margin-top: 10px;"> Entes:</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="org"
                                                data-placeholder="Seleccione1 Organismo">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=fila from=$ente}
                                                {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente}
                                                    <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                {else}
                                                    <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkPersona">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="persona"
                                               class="control-label" style="margin-top: 10px;"> Visitante:</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="col-sm-10">
                                            <input type="hidden" class="form-control"
                                                   id="codigo"
                                                   value=""
                                                   disabled>
                                            <input type="text" class="form-control"
                                                   id="nombre"
                                                   value=""
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCV/consultarVisitas/consultarVisitasCONTROL/listaPersonaMET/visitante/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkDependencia">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="dep"
                                               class="control-label" style="margin-top: 10px;"> Destino Visita:</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="dep"
                                                data-placeholder="Seleccione Dependencia">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=i from=$selectDependencia}
                                                <option value="{$i.id}">{$i.nombre}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="col-sm-2">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkMotivo">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="motivo"
                                               class="control-label" style="margin-top: 10px;"> Motivo:</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="motivo"
                                                data-placeholder="Seleccione Organismo">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=i from=$selectMotivo}
                                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkTipoVisita">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="tipoVisita"
                                               class="control-label" style="margin-top: 10px;"> Tipo de Visita:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="tipoVisita"
                                                data-placeholder="Seleccione Organismo">
                                            <option value="">Seleccione...</option>}

                                            <option value="P">Particular</option>
                                            <option value="I">Organismo Público</option>
                                            <option value="E">Empresa Privada</option>
                                            <option value="J">Jubilados</option>

                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary">
                                    BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="section-body contain-lg" id="listadoConsulta">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="datatable1" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Cedula</th>
                                            <th>Visitante</th>
                                            <th>Organo/Ente Externo</th>
                                            <th>Destino</th>
                                            <th>Entradas</th>
                                            <th>Salida</th>
                                            <th>Accion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        /// Complementos
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        //busqueda segun el filtro
        $('.buscar').click(function(){
            var url='{$_Parametros.url}modCV/consultarVisitas/consultarVisitasCONTROL/actualizarMET/';
            $.post(url,{ organismo: $('#org').val(), dependencia: $('#dep').val(), visitante: $('#codigo').val(),
                         desde: $('#desde').val(), hasta: $('#hasta').val(), motivo: $('#motivo').val(), tipoVisita: $('#tipoVisita').val()},function(dato){
                $('#listadoConsulta').html('');
                $('#listadoConsulta').html(dato);
            });
        });

        //habilitar y deshabilitar campos del filtro
        $('#checkOrganismo').click(function () {
            if(this.checked){
                $('#org').attr('disabled', false);
            }else{
                $('#org').attr('disabled', true);
                $(document.getElementById('org')).val("");
            }
        });
        $('#checkDependencia').click(function () {
            if(this.checked){
                $('#dep').attr('disabled', false);
            }else{
                $('#dep').attr('disabled', true);
                $(document.getElementById('dep')).val("");
            }
        });
        $('#checkPersona').click(function () {
            if(this.checked){
                $('#botonPersona').attr('disabled', false);
            }else{
                $('#botonPersona').attr('disabled', true);
                $(document.getElementById('codigo')).val("");
                $(document.getElementById('nombre')).val("");

            }
        });
        $('#checkFecha').click(function () {
            if(this.checked){
                $('#desde').attr('disabled', false);
                $('#hasta').attr('disabled', false);
            }else{
                $('#desde').attr('disabled', true);
                $(document.getElementById('desde')).val("");
                $('#hasta').attr('disabled', true);
                $(document.getElementById('hasta')).val("");

            }
        });
        $('#checkMotivo').click(function () {
            if(this.checked){
                $('#motivo').attr('disabled', false);
            }else{
                $('#motivo').attr('disabled', true);
                $(document.getElementById('motivo')).val("");

            }
        });
        $('#checkTipoVisita').click(function () {
            if(this.checked){
                $('#tipoVisita').attr('disabled', false);
            }else{
                $('#tipoVisita').attr('disabled', true);
                $(document.getElementById('tipoVisita')).val("");

            }
        });
    });

</script>