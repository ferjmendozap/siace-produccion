<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visita
 * PROCESO: Listar visitas
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Sergio Zabaleta                            |zsergio01@gmail.com                 |0414-3638131                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |08/12/2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class listarVisitasModelo extends Modelo
{

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metMostrarVisita($idPersona=false)
    {
        if ($idPersona) {
            $were ="WHERE cv_b001_registro_visita.fk_a003_num_persona='$idPersona' and cv_b001_registro_visita.fec_fecha_salida IS NULL";
        }
        else{
            $were='';
        }
        $visita = $this->_db->query("
          SELECT
              cv_b001_registro_visita.*,
              a018_seguridad_usuario.ind_usuario,
              a003_persona.ind_foto,
              a003_persona.ind_nombre1,
              a003_persona.ind_apellido1,
              personaAnfitrion.ind_apellido1 AS personaAnfitrionApellido,
              personaAnfitrion.ind_nombre1 AS personaAnfitrionNombre,
              motivo.ind_nombre_detalle AS ind_motivo
            FROM
              cv_b001_registro_visita
            INNER JOIN
              a006_miscelaneo_detalle AS motivo ON motivo.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita
            INNER JOIN
              a003_persona ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
            INNER JOIN
              a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cv_b001_registro_visita.fk_a018_num_seguridad_usuario
            LEFT JOIN
              rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cv_b001_registro_visita.fk_rh_b001_empleado_anfitrion
            LEFT JOIN
              a003_persona AS personaAnfitrion ON personaAnfitrion.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            LEFT JOIN
              a039_ente ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
            LEFT JOIN
              a006_miscelaneo_detalle AS destino ON destino.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino
              
            $were
        ");
        $visita->setFetchMode(PDO::FETCH_ASSOC);
        if($idPersona){
            return $visita->fetch();
        }else{
            return $visita->fetchAll();
        }
    }

    public function metListarVisita()
    {
        $registro = $this->_db->query(
            "SELECT * FROM cv_b001_registro_visita"
        );
        $registro->setFetchMode(PDO::FETCH_ASSOC);
        return $registro->fetchAll();
    }

}
