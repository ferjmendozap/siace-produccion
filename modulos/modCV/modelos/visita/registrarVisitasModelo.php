<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registros de Visita
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Sergio Zabaleta                  |zsergio01@gmail.com                 |         0414-3638131           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        04-12-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modCV' . DS . 'modelos' . DS . 'maestros' . DS . 'organismoModelo.php';
require_once RUTA_Modulo . 'modCV' . DS . 'modelos' . DS . 'visita' . DS . 'listarVisitasModelo.php';
require_once RUTA_MODELO . 'miscelaneoModelo.php';

class registrarVisitasModelo extends miscelaneoModelo
{
    private $atVisita, $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atVisita=Session::metObtener('idVisita');
        $this->atOrganismo = new organismoModelo();
        $this->atListarVisitas = new listarVisitasModelo();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarEntes()
    {
        $menu = $this->_db->query("SELECT * FROM a039_ente WHERE num_estatus=1");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metListarDependencias()
    {
        $menu = $this->_db->query("SELECT * FROM a004_dependencia WHERE num_estatus=1");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metListarPersona($idPersona=false)
    {
        if($idPersona){
            $idPersona="WHERE pk_num_persona='$idPersona'";
        }
        $menu = $this->_db->query("SELECT * FROM a003_persona $idPersona");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        if($idPersona){
            return $menu->fetch();
        }else{
            return $menu->fetchAll();
        }
    }

    public function metRegistrarEntrada(
        $persona, $tipoVisita, $ente, $proveedor,
        $destino, $dependencia, $empleado, $motivo
    )
    {
        if(!$empleado){
            $empleado = null;
        }
        if($ente == 0){
            $ente = null;
        }
        if($proveedor == 0){
            $proveedor = null;
        }
        $this->_db->beginTransaction();
        $nuevoRegistro = $this->_db->prepare("
                  INSERT INTO
                   cv_b001_registro_visita
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_a003_num_persona=:fk_a003_num_persona, fk_a039_num_ente=:fk_a039_num_ente,
                    fk_lg_b022_num_proveedor=:fk_lg_b022_num_proveedor,
                    fk_a006_num_miscelaneo_detalle_destino=:fk_a006_num_miscelaneo_detalle_destino,
                    fk_a004_num_dependencia=:fk_a004_num_dependencia, fk_rh_b001_empleado_anfitrion=:fk_rh_b001_empleado_anfitrion,
                    fk_a006_num_miscelaneo_detalle_motivo_visita=:fk_a006_num_miscelaneo_detalle_motivo_visita,
                    fec_fecha_entrada=NOW(), ind_tipo_visita=:ind_tipo_visita
                ");
        $nuevoRegistro->execute(array(
            'fk_a003_num_persona' => $persona,
            'ind_tipo_visita' => $tipoVisita,
            'fk_a039_num_ente' => $ente,
            'fk_lg_b022_num_proveedor' =>$proveedor,
            'fk_a006_num_miscelaneo_detalle_destino' => $destino,
            'fk_a004_num_dependencia' => $dependencia,
            'fk_rh_b001_empleado_anfitrion' => $empleado,
            'fk_a006_num_miscelaneo_detalle_motivo_visita' => $motivo
        ));

        $idRegistro = $this->_db->lastInsertId();

        $error = $nuevoRegistro->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) ) {
            $this->_db->rollBack();
            return $error = array_merge($error);
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metRegistrarSalida($idVisita)
    {
        $this->_db->beginTransaction();
        $nuevoRegistro = $this->_db->query("
                      UPDATE
                        cv_b001_registro_visita
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                        fec_fecha_salida=NOW()
                      WHERE
                        pk_num_registro_visita='$idVisita'
            ");


            $this->_db->commit();
            return $idVisita;

    }





}
