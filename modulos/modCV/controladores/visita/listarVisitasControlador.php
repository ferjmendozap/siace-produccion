<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registro de Visistas
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Sergio Zabaleta                            |zsergio01@gmail.com                 |0414-3638131                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |08-10-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class listarVisitasControlador extends Controlador
{

    private $atVisita;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atVisita = $this->metCargarModelo('listarVisitas','visita');
    }


    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $complementosJs[] = 'JPEGCam/webcam';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->assign('listado', $this->atVisita->metMostrarVisita());
        $this->atVista->metRenderizar('listado');

    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
          SELECT
            cv_b001_registro_visita.*,
            a003_persona.ind_cedula_documento,
            a018_seguridad_usuario.ind_usuario,
            a003_persona.ind_foto,
            concat_ws(' ',a003_persona.ind_nombre1,a003_persona.ind_apellido1) AS nombre,
            concat_ws(' ',personaAnfitrion.ind_nombre1,personaAnfitrion.ind_apellido1) AS nombreAnfitrion,
            motivo.ind_nombre_detalle AS ind_motivo,
            IF(a006_miscelaneo_detalle.ind_nombre_detalle IS NOT NULL,a006_miscelaneo_detalle.ind_nombre_detalle,a004_dependencia.ind_dependencia) AS destino
          FROM
            cv_b001_registro_visita
          INNER JOIN
            a006_miscelaneo_detalle AS motivo ON motivo.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita
          INNER JOIN
            a003_persona ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
          INNER JOIN
            a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cv_b001_registro_visita.fk_a018_num_seguridad_usuario
          LEFT JOIN
            rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cv_b001_registro_visita.fk_rh_b001_empleado_anfitrion
          LEFT JOIN
            a003_persona AS personaAnfitrion ON personaAnfitrion.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
          LEFT JOIN
            a004_dependencia ON a004_dependencia.pk_num_dependencia = cv_b001_registro_visita.fk_a004_num_dependencia
          LEFT JOIN
            a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino
          WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (
                        a003_persona.ind_cedula_documento LIKE '%$busqueda[value]%' OR
                        a003_persona.ind_nombre1 LIKE '%$busqueda[value]%' OR
                        a003_persona.ind_apellido1 LIKE '%$busqueda[value]%' OR
                        a006_miscelaneo_detalle.ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                        cv_b001_registro_visita.fec_fecha_entrada LIKE '%$busqueda[value]%' OR
                        cv_b001_registro_visita.fec_fecha_salida LIKE '%$busqueda[value]%'
                        )
                        ";
        }
        $sql.=" and cv_b001_registro_visita.fec_fecha_salida IS NULL ";

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_cedula_documento','nombre','destino','ind_motivo','fec_fecha_entrada','fec_fecha_salida');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_registro_visita';
        #construyo el listado de botones

        if (in_array('CV-01-01-02',$rol)) {
            $campos['boton']['Editar'] = '
                <button idVisita="'.$clavePrimaria.'" class="salida logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        boton="si, Salir" titulo="Estas Seguro?" mensaje="Estas seguro que la persona va a salir!!" title="Dar Salida">
                    <i class="md md-launch" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }


        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

}
