<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registro de Visitas
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Sergio Zabaleta                  |zsergio01@gmail.com                 |         0414-3638131           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        04-12-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class visitaControlador extends Controlador
{
    private $atVisita;
    private $atOrganismo;
    private $atListarVisitas;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atVisita = $this->metCargarModelo('registrarVisitas', 'visita');
    }

    public function metIndex()
    {

        $this->atVista->assign('visitasRegistradas', $this->atVisita->atListarVisitas->metMostrarVisita());
        $this->atVista->assign('listado', $this->atVisita->metListarPersona());
        $this->atVista->metRenderizar('listadoPersona','modales');
    }

    public function metRegistrarVisita ()
    {
        header( "Expires: Mon, 20 Dec 1998 01:00:00 GMT" );
        header( "Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT" );
        header( "Cache-Control: no-cache, must-revalidate" );
        header( "Pragma: no-cache" );
        $js[] = 'Aplicacion/appFunciones';
        $complementosJs[] = 'JPEGCam/webcam';

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $valido=$this->metObtenerInt('valido');
        $idVisita=$this->metObtenerInt('idVisita');
        $idPersona=$this->metObtenerInt('idPersona');
        if($idPersona){
            $validarPersona=$this->atVisita->atListarVisitas->metMostrarVisita($idPersona);
            if($validarPersona){
                echo "Tienes que darle salida primero";
                exit;
            }
        }

        if($valido==1){
            $this->metValidarToken();
            $excepcion=array('num_estatus','id_ente','proveedor','asisA');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$excepcion);
            $ind=$this->metValidarFormArrayDatos('form','int',$excepcion);
            if($ind!=null && $alphaNum == null){
                $validacion=$ind;
            }elseif($ind==null && $alphaNum != null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($ind, $alphaNum);
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }
            if($idVisita==0){
                $pos = strpos($validacion['fk_a006_num_miscelaneo_detalle_destino'], 'D');
                if ($pos === false) {
                    $destino = preg_replace('/[^0-9 .,]/', '', $validacion['fk_a006_num_miscelaneo_detalle_destino']);
                    $dependencia = null;
                } else {
                    $destino = null;
                    $dependencia = preg_replace('/[^0-9 .,]/', '', $validacion['fk_a006_num_miscelaneo_detalle_destino']);
                }

                $id=$this->atVisita->metRegistrarEntrada(
                    $validacion['idPersona'], $validacion['ind_tipo_visita'], $validacion['id_ente'], $validacion['proveedor'],
                    $destino, $dependencia, $validacion['asisA'], $validacion['fk_a006_num_miscelaneo_detalle_motivo_visita']
                );
                $validacion['status']='registrarEntrada';


            }
            if(is_array($id)){
                var_dump($id);exit;

                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }

                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idVisita']=$id;
            echo json_encode($validacion);
            exit;
        }


        if($idVisita!=0){
            $this->atVista->assign('formDB',$this->atVisita->metMostrarVisita($idVisita));
            $this->atVista->assign('idVisita',$idVisita);

        }
        $destino=array();
        foreach($this->atVisita->metListarDependencias() AS $i){
            $destino[] = array(
                'id'=>'D'.$i['pk_num_dependencia'],
                'nombre'=>$i['ind_dependencia']
            );
        }
        foreach($this->atVisita->metMostrarSelect('CVDESVIS') AS $i){
            $destino[] = array(
                'id'=>'M'.$i['pk_num_miscelaneo_detalle'],
                'nombre'=>$i['ind_nombre_detalle']
            );
        }

        $this->atVista->assign('idPersona',$idPersona);
        $this->atVista->assign('formDB', $this->atVisita->metListarPersona($idPersona));
        $this->atVista->assign('ente', $this->atVisita->metListarEntes());
        $this->atVista->assign('destino', $destino);
        $this->atVista->assign('motivo', $this->atVisita->metMostrarSelect('CVMOTVIS'));

        $this->atVista->metRenderizar('registrarVisita','modales');
    }

    public function metRegistrarSalida()
    {
        $idVisita = $this->metObtenerInt('idVisita');
          if ($idVisita != 0) {
            $id = $this->atVisita->metRegistrarSalida($idVisita);
            if (is_array($id)) {
                $valido = array(
                    'status' => 'error',
                    'mensaje' => 'Disculpa. Pero el Motivo se encuentra en uso y no se puede eliminar'
                );
            } else {
                $valido = array(
                    'status' => 'ok',
                    'idVisita' => $id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metCargarFoto($ci)
    {
        $filename = ROOT.'publico'.DS.'imagenes'.DS.'modCV'.DS."fotosVisitantes".DS.$ci.'.jpg';//nombre del archivo
        $result = file_put_contents( $filename, file_get_contents('php://input') );//renombramos la fotografia y la subimos
        if (!$result) {
            exit("No se pudo subir al servidor");
        }

        if(BASE_URL!='/'){
            $base=BASE_URL;
        }else{
            $base='http://'.$_SERVER['HTTP_HOST'].'/';
        }

        $url = $base.'publico/imagenes/modCV/fotosVisitantes/'.$ci.'.jpg';
        print "$url\n";
    }

}
