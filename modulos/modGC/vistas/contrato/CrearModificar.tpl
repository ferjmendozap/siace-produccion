<form action="{$_Parametros.url}modGC/contratoCONTROL/OrdenCompraGCMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idOrden)}{$idOrden}{/if}" name="idOrden"/>
    <input type="hidden" value="{if isset($estado)}{$estado}{/if}" name="estado"/>
    <input type="hidden" value="{if isset($formBD.fk_a023_num_centro_costo)}{$formBD.fk_a023_num_centro_costo}{else}{$predeterminado.fk_a023_num_centro_costo}{/if}" id="cc"/>
    <input type="hidden" value="{$ver}" name="ver" id="ver"/>
    <input type="hidden" value="{$idContrato}" name="form[int][idContrato]" />

    {$disabled=''}
    {if isset($ver) and $ver==1}
        {$disabled='disabled'}
    {/if}
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">ITEMS/COMMODITIES</span> </a></li>
                                    <li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <br/> <span class="title">COTIZACIONES</span> </a></li>
                                    <li><a href="#tab5" data-toggle="tab"><span class="step">5</span> <span class="title">DISTRIBUCIÓN PRESUPUESTARIA/CONTABLES</span> </a></li>
                                    <!--li><a href="#tab4" data-toggle="tab"><span class="step">4</span> <br/> <span class="title">AVANCES</span> </a></li-->
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                <div class="col-lg-12">
                                    <div class="card-head card-head-xs style-primary" align="center">
                                        <header>Información General</header>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-12" id="dependenciaError">
                                            <select name="form[int][dependencia]" id="dependencia" class="form-control select2-list select2" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <option value=""></option>
                                                {foreach item=dep from=$dependencia}
                                                    {if $formBD.fk_a004_num_dependencia==$dep.pk_num_dependencia OR $predeterminado.fk_a004_num_dependencia==$dep.pk_num_dependencia}
                                                        <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                                    {else}
                                                        <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="dependencia">Dependencia</label>
                                        </div>

                                        <div class="form-group col-lg-12" id="centro_costoError">
                                            <label for="centro_costo">Centro de Costo</label>
                                            <select id="centro_costo" name="form[int][centro_costo]" class="form-control select2-list select2" {if $estado!='istado de'} readonly {/if} {$disabled}>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-lg-12" id="proveedorError">
                                            <div class="col-lg-8">
                                                <input type="hidden" name="form[int][proveedor]" id="idProveedor" class="form-control" value="{if isset($formBD.fk_lgb022_num_proveedor)}{$formBD.fk_lgb022_num_proveedor}{/if}">
                                                <input type="text" disabled id="persona" class="form-control" value="{if isset($formBD.proveedor)}{$formBD.proveedor}{/if}">
                                            </div>
                                            <div class="col-lg-3">
                                                <input type="text" disabled id="cedula" class="form-control" value="{if isset($formBD.provCed)}{$formBD.provCed}{/if}">
                                            </div>
                                            <div class="col-lg-1">
                                                <button {$disabled}
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal" data-target="#formModal2"
                                                        titulo="Buscar Proveedor"
                                                        url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/proveedorMET/proveedor/"
                                                        >
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                            <label for="proveedor" class="control-label">Proveedor Recomendado:</label>
                                        </div>
                                        <div class="form-group col-lg-12" id="tipo_servicioError">
                                            <input type="hidden" id="tipo_servicio_id" value="{if isset($formBD.fk_cpb017_num_tipo_servicio)}{$formBD.fk_cpb017_num_tipo_servicio}{/if}">
                                            <label for="tipo_servicio">Tipo de Servicio</label>
                                            <select id="tipo_servicio" name="form[int][tipo_servicio]" class="form-control select2-list select2" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <option value=""></option>
                                            </select>
                                            <input type="hidden" value="0" id="iva">
                                            <input type="hidden" value="0" id="retencion">
                                            <input type="hidden" name="form[int][partida]" value="{$partidaCuenta.pk_num_partida_presupuestaria}" id="partidaIva" nombre="{$partidaCuenta.ind_denominacion}" codigo="{$partidaCuenta.cod_partida}">
                                            <input type="hidden" name="form[int][cuenta]" value="{$partidaCuenta.fk_cbb004_num_plan_cuenta_onco}" id="cuentaIva" nombre="{$partidaCuenta.ind_descripcion}" codigo="{$partidaCuenta.cod_cuenta}">
                                        </div>
                                        <div class="form-group col-lg-6" id="formaPagoError">
                                            <select id="formaPago" name="form[int][formaPago]" class="form-control select2-list select2" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                {foreach item=fp from=$formaPago}
                                                    {if isset($formBD.fk_a006_num_miscelaneos_forma_pago) and $formBD.fk_a006_num_miscelaneos_forma_pago == $fp.pk_num_miscelaneo_detalle}
                                                        <option value="{$fp.pk_num_miscelaneo_detalle}" selected>{$fp.ind_nombre_detalle}</option>
                                                    {else}
                                                        <option value="{$fp.pk_num_miscelaneo_detalle}">{$fp.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="formaPago">Forma de Pago</label>
                                        </div>
                                        <div class="form-group col-lg-6" id="tipoOrdenError">
                                            <select id="tipoOrden" name="form[txt][tipoOrden]" class="form-control select2-list select2" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <option value="OC" {if isset($formBD.ind_tipo_orden) AND $formBD.ind_tipo_orden=='OC'} selected {/if}>Orden Compra</option>
                                                <option value="OS" {if isset($formBD.ind_tipo_orden) AND $formBD.ind_tipo_orden=='OS'} selected {/if}>Orden Servicio</option>
                                            </select>
                                            <label for="tipoOrden">Tipo de Orden</label>
                                        </div>
                                        <div class="form-group col-lg-6" id="clasificacionError">
                                            <select id="clasificacion" name="form[int][clasificacion]" class="form-control select2-list select2" {if $estado!='Listado de'} readonly {/if} {$disabled}>

                                                {foreach item=clasificacion from=$clas}
                                                    {if isset($formBD.fk_lgb017_num_clasificacion) and $formBD.fk_lgb017_num_clasificacion == $clasificacion.pk_num_clasificacion}
                                                        <option value="{$clasificacion.pk_num_clasificacion}" flag="{$clasificacion.num_flag_commodity}" selected>{$clasificacion.ind_descripcion}</option>
                                                    {elseif $clasificacion.ind_descripcion!='REQUERIMIENTO DE AUTO REPOSICION'}
                                                        <option value="{$clasificacion.pk_num_clasificacion}" flag="{$clasificacion.num_flag_commodity}" >{$clasificacion.ind_descripcion}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="clasificacion">Clasificación</label>
                                        </div>
                                        <div class="form-group col-lg-6" id="almacenError">
                                            <label for="almacen">Almacen</label>
                                            <select id="almacen" class="form-control select2-list select2" name="form[int][almacen]" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <option value=""></option>
                                            </select>
                                        </div>

                                        <div class="col-lg-12 style-primary" style="text-align: center;">
                                            <header>Información Adicicional</header>
                                        </div>
                                        <div class="col-lg-12" id="aprobadoError">
                                            <div class="form-group">
                                                <input type="text" disabled id="persona6" class="form-control disabled" value="{if isset($formBD.empleadoApr)}{$formBD.empleadoApr}{/if}">
                                                <label for="aprobado"><i class="md md-people"></i>Aprobado por</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" id="revisadoError">
                                            <div class="form-group">
                                                <input type="text" disabled id="persona4" class="form-control disabled" value="{if isset($formBD.empleadoRevisa)}{$formBD.empleadoRevisa}{/if}">
                                                <label for="revisado"><i class="md md-people"></i>Revisado por</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" id="ingresadoError">
                                            <div class="form-group">
                                                <input type="text" disabled id="persona3" class="form-control disabled" value="{if isset($formBD.empleadoCrea)}{$formBD.empleadoCrea}{/if}">
                                                <label for="ingresado"><i class="md md-people"></i>Ingresado por</label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-lg-12 style-default">
                                            <div class="form-group">
                                                <input type="text" disabled class="form-control" value="{if isset($formBD.ind_orden)}{$formBD.ind_orden}{/if}">
                                                <label for="ingresado">Nro. Orden</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 style-default">
                                            <div class="form-group">
                                                <input type="text" disabled class="form-control" value="{if isset($estadoOrden)}{$estadoOrden}{/if}">
                                                <label for="ingresado">Estado de la Orden</label>
                                            </div>
                                        </div>
                                        <div class="style-primary" style="text-align: center;">
                                            <header>Monto de la Compra</header>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group" id="mAfectoError">
                                                <input type="text" readonly name="form[int][mAfecto]" id="mAfecto" value="{if isset($formBD.num_monto_afecto)}{$formBD.num_monto_afecto}{else}0{/if}" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <label for="mAfecto">Monto Afecto</label>
                                            </div>
                                            <div class="form-group" id="mNoAfectoError">
                                                <input type="text" readonly name="form[int][mNoAfecto]" id="mNoAfecto" value="{if isset($formBD.num_monto_no_afecto)}{$formBD.num_monto_no_afecto}{else}0{/if}" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <label for="mNoAfecto">Monto No Afecto</label>
                                            </div>
                                            <div class="form-group" id="mBrutoError">
                                                <input type="text" readonly name="form[int][mBruto]" id="mBruto" value="{if isset($formBD.num_monto_bruto)}{$formBD.num_monto_bruto}{else}0{/if}" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <label for="mBruto">Monto Bruto</label>
                                            </div>
                                            <div class="form-group" id="impuestosError">
                                                <input type="text" readonly name="form[int][impuestos]" id="impuestos" value="{if isset($formBD.num_monto_igv)}{$formBD.num_monto_igv}{else}0{/if}" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <label for="impuestos">Impuestos</label>
                                            </div>
                                            <div class="form-group" id="mTotalError">
                                                <input type="text" readonly name="form[int][mTotal]" id="mTotal" value="{if isset($formBD.num_monto_total)}{$formBD.num_monto_total}{else}0{/if}" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <label for="mTotal">Monto Total</label>
                                            </div>
                                            <div class="form-group" id="mPendienteError">
                                                <input type="text" readonly name="form[int][mPendiente]" id="mPendiente" value="{if isset($formBD.num_monto_pendiente)}{$formBD.num_monto_pendiente}{else}0{/if}" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <label for="mPendiente">Monto Pendiente</label>
                                            </div>
                                            <div class="form-group" id="oCargosError">
                                                <input type="text" readonly name="form[int][oCargos]" id="oCargos" value="{if isset($formBD.num_monto_otros)}{$formBD.num_monto_otros}{else}0{/if}" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>
                                                <label for="oCargos">Otros Cargos</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card-head card-head-xs style-primary" align="center">
                                        <header>Organismo</header>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group" id="plazoError">
                                            <input type="text" maxlength="2" name="form[int][plazo]" id="plazo" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}{if isset($formBD.num_plazo_entrega)} value="{$formBD.num_plazo_entrega}"{/if}>
                                            <label for="plazo"><i class="md md-border-color"></i>Plazo de Entrega (días)</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group" id="entregarError">
                                            <input type="text" name="form[txt][entregar]" id="entregar" class="form-control" {if isset($ver) and $ver==1} disabled {/if} value="{if isset($formBD.ind_entregar_en)}{$formBD.ind_entregar_en}{/if}">
                                            <label for="entregar"><i class="md md-border-color"></i>Entregar En</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="direccionError">
                                            <input type="text" name="form[txt][direccion]" id="direccion" class="form-control" {if isset($ver) and $ver==1} disabled {/if} value="{if isset($formBD.ind_direccion_entrega)}{$formBD.ind_direccion_entrega}{/if}">
                                            <label for="direccion"><i class="md md-border-color"></i>Direccion Entrega</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card-head card-head-xs style-primary" align="center">
                                        <header>Observaciones</header>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <div class="form-group" id="descripcionError">
                                                <textarea name="form[alphaNum][descripcion]" id="descripcion" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>{if isset($formBD.ind_descripcion)}{$formBD.ind_descripcion}{/if}</textarea>
                                                <label for="descripcion"><i class="md md-border-color"></i>Descripcion</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group" id="comentarioError">
                                                <textarea name="form[alphaNum][comentario]" id="comentario" class="form-control" {if $estado!='Listado de'} readonly {/if} {$disabled}>{if isset($formBD.ind_comentario)}{$formBD.ind_comentario}{/if}</textarea>
                                                <label for="comentario"><i class="md md-border-color"></i>Comentario</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group" id="rechazoError">
                                                <textarea name="form[alphaNum][rechazo]" id="rechazo" class="form-control" readonly {$disabled}>{if isset($formBD.ind_motivo_rechazo)}{$formBD.ind_motivo_rechazo}{/if} </textarea>
                                                <label for="rechazo">Razón Rechazo</label>
                                            </div>
                                        </div>
                                        <div class="form-group floating-label col-lg-6">
                                            <input type="text" class="form-control disabled" id="usuPeriodo" disabled value="{if isset($formBD.ind_usuario)}{$formBD.ind_usuario}{/if}">
                                            <label for="usuPeriodo"><i class="md md-border-color"></i>Ultimo Usuario</label>
                                        </div>
                                        <div class="form-group floating-label col-lg-6">
                                            <input type="text" class="form-control disabled" id="fecPeriodo" disabled value="{if isset($formBD.fec_ultima_modificacion)}{$formBD.fec_ultima_modificacion}{/if}">
                                            <label for="fecPeriodo"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                                            <header>Items/Commodities</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="col-lg-6">
                                                    <div class="form-group floating-label">
                                                        <label for="itemBuscar">Buscar Item<i class="md md-computer"></i></label>
                                                        <button disabled {$disabled}
                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                type="button"
                                                                title="Buscar Item"
                                                                id="nuevoItem"
                                                                data-toggle="modal" data-target="#formModal2"
                                                                data-keyboard="false" data-backdrop="static"
                                                                titulo="Buscar Item"
                                                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/item3/"
                                                                >
                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group floating-label">
                                                        <label for="itemBuscar">Buscar Commodities<i class="md md-computer"></i></label>
                                                        <button disabled {$disabled}
                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                type="button"
                                                                title="Buscar Commoditie"
                                                                id="nuevoComm"
                                                                data-toggle="modal" data-target="#formModal2"
                                                                data-keyboard="false" data-backdrop="static"
                                                                titulo="Buscar Commoditie"
                                                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/commodity3/"
                                                                >
                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <table class="table table-striped no-margin" id="contenidoTabla">
                                                    <thead>
                                                    <tr>
                                                        <th width="10px">#</th>
                                                        <th width="10px">Código</th>
                                                        <th width="10px">Descripción</th>
                                                        <th width="10px">Cant. Pedida</th>
                                                        <th width="10px">P. Unit.</th>
                                                        <th width="10px">% Desc.</th>
                                                        <th width="10px">Desc. Fijo</th>
                                                        <th width="10px">Exon.</th>
                                                        <th width="10px">Monto P. Unit.</th>
                                                        <th width="10px">Total</th>
                                                        <th width="10px">Borrar</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item">

                                                    {if isset($formBDD)}
                                                        {$cont=1}
                                                        {$readonly =''}
                                                        {if $estado!='Listado de'} 
                                                            {$readonly ='readonly'}
                                                        {/if} 
                                                        {foreach item=det from=$formBDD}
                                                            {if $det.fk_lgb002_num_item}
                                                                {$id = $det.fk_lgb002_num_item}
                                                                {$descripcion = $det.ind_descripcion_item}
                                                                <input type="hidden" id="cuenta{$det.fk_lgb002_num_item}" class="cuenta"
                                                                       value="{$det.cuentaItem}"
                                                                       codCuenta="{$det.codCuentaItem}"
                                                                       descCuenta="{$det.descCuentaItem}"
                                                                       cantidad="{number_format($det.num_cantidad,0)}"
                                                                       precio="{number_format($det.num_precio_unitario,0)}">
                                                                <input type="hidden" id="partida{$det.fk_lgb002_num_item}" class="partida"
                                                                       value="{$det.partidaItem}"
                                                                       codPartida="{$det.codPartidaItem}"
                                                                       descPartida="{$det.descPartidaItem}"
                                                                       cantidad="{number_format($det.num_cantidad,0)}"
                                                                       precio="{number_format($det.num_precio_unitario,0)}">
                                                                {$partida = $det.partidaItem}
                                                            {else}
                                                                {$id = $det.pk_num_commodity}
                                                                {$descripcion = $det.ind_descripcion_commodity}
                                                                <input type="hidden" id="cuenta{$det.pk_num_commodity}" class="cuenta"
                                                                       value="{$det.cuentaComm}"
                                                                       codCuenta="{$det.codCuentaComm}"
                                                                       descCuenta="{$det.descCuentaComm}"
                                                                       cantidad="{number_format($det.num_cantidad,0)}"
                                                                       precio="{number_format($det.num_precio_unitario,0)}">
                                                                <input type="hidden" id="partida{$det.pk_num_commodity}" class="partida"
                                                                       value="{$det.partidaComm}"
                                                                       codPartida="{$det.codPartidaComm}"
                                                                       descPartida="{$det.descPartidaComm}"
                                                                       cantidad="{number_format($det.num_cantidad,0)}"
                                                                       precio="{number_format($det.num_precio_unitario,0)}">
                                                                {$partida = $det.partidaComm}
                                                            {/if}
                                                            <tr id="item{$id}" class="ic" idIC="{$id}">
                                                                <td width="10px" style="vertical-align: middle;">
                                                                    {$cont}

                                                                    <input type="hidden" id="tipo" value="{if $det.fk_lgb002_num_item}item{else}comm{/if}">

                                                                    <input type="hidden" name="form[int][idOrdenDetalle][{$id}]" value="{$det.pk_num_orden_detalle}">
                                                                    <input type="hidden" name="form[int][sec]" value="{$cont}">
                                                                    <input type="hidden" name="form[txt][tipo]" value="{if $det.fk_lgb002_num_item!=""}item{else}comm{/if}">
                                                                    <input type="hidden" name="form[int][unidad][{$id}]" value="{if $det.fk_lgb002_num_item!=""}{$det.fk_lgb004_num_unidad_compra}{else}{$det.fk_lgb004_num_unidad}{/if}">
                                                                    <input type="hidden" name="form[int][cuentaDetalle][{$id}]" value="{$det.fk_cbb004_plan_cuenta_oncop}">
                                                                    <input type="hidden" name="form[int][partidaDetalle][{$id}]" value="{$partida}">
                                                                </td>
                                                                <td width="10px"><input type="text" readonly class="form-control" value="{$id}" name="form[int][num][{$id}]" size="4" {if isset($ver) and $ver==1} disabled {/if}></td>
                                                                <td width="10px"><input type="text" readonly class="form-control" value="{$descripcion}" name="form[int][desc][{$id}]" size="6" {if isset($ver) and $ver==1} disabled {/if}></td>
                                                                <td width="10px"><input type="text" class="form-control nuevoValor" value="{number_format($det.num_cantidad,6,'.','')}" name="form[int][cant][{$id}]" sec="{$id}" id="cantidad{$id}" size="6" {$disabled}></td>
                                                                <td width="10px"><input type="text" class="form-control nuevoValor" value="{number_format($det.num_precio_unitario,6,'.','')}" name="form[int][precio][{$id}]" sec="{$id}" id="precio{$id}" size="4" {$disabled} {$readonly}></td>
                                                                <td width="10px"><input type="text" class="form-control nuevoValor" value="{number_format($det.num_descuento_porcentaje,6,'.','')}" name="form[int][descPorc][{$id}]" sec="{$id}" id="descPorc{$id}" {$disabled} {$readonly}></td>
                                                                <td width="10px"><input type="text" class="form-control nuevoValor" value="{number_format($det.num_descuento_fijo,6,'.','')}" name="form[int][descFijo][{$id}]" sec="{$id}" id="descFijo{$id}" {$disabled} {$readonly}></td>
                                                                <td width="10px" style="vertical-align: middle;">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label class="checkbox-inline checkbox-styled">
                                                                            <input type="checkbox" class="form-control nuevoValor" name="form[int][exonerado][{$id}]" id="exonerado{$id}" sec="{$id}" {if isset($det.num_flag_exonerado) and $det.num_flag_exonerado==1} checked {/if} value="1" {if isset($ver) and $ver==1} disabled {/if} value="1">
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td width="10px"><input type="text" class="form-control" readonly value="{if $det.num_flag_exonerado==1} {number_format($det.num_precio_unitario,6,'.','')}{else}{number_format($det.num_precio_unitario_iva,6,'.','')}{/if}" name="form[int][montoPU][{$id}]" sec="{$id}" id="montoPU{$id}" {if isset($ver) and $ver==1} disabled {/if}></td>
                                                                <td width="10px"><input type="text" class="form-control monto total" readonly name="form[int][total][{$id}]" value="{number_format($det.num_total,6,'.','')}" id="total{$id}" {if isset($ver) and $ver==1} disabled {/if}></td>
                                                                <td style="vertical-align: middle;">
                                                                    <button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger" id="{$id}" {if (isset($ver) and $ver==1) OR isset($det.fk_lgb023_num_acta_detalle)} disabled {/if}>
                                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            {$cont=$cont+1}
                                                        {/foreach}
                                                        <input type="hidden" value="{$cont-1}" name="form[int][cantidad]" id="cantidad">
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane floating-label" id="tab3">
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                                            <header>Cotizaciones</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" style="white-space:nowrap;overflow-x:scroll;overflow-y:scroll;width:100%;height:250px;display: inline-block;" id="contenidoTabla2">
                                                    <thead>
                                                    <tr>
                                                        <th>Código</th>
                                                        <th>Razón Social<i style="color: #ffffff">________________________________________________</i></th>
                                                        <th>Cantidad<i style="color: #ffffff">___</i></th>
                                                        <th>Precio Unit.</th>
                                                        <th>Precio Unit./Imp.</th>
                                                        <th>Monto Total</th>
                                                        <th>Asig.</th>
                                                        <th>Dias Entrega</th>
                                                        <th>Fecha Entrega</th>
                                                        <th>Cotización #</th>
                                                        <th>Observación<i style="color: #ffffff">________________________________________________</i></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="cotizaciones">
                                                    {if isset($cotizaciones)}
                                                        {$cual = 0}
                                                        {foreach item=c from=$cotizaciones}

                                                            {$ic = $c.fk_lgb002_num_item}
                                                            {$ic = $ic+$c.fk_lgb003_num_commodity}

                                                            {if $cual != $ic}
                                                                {$cual = $ic}
                                                                <tr style="font-weight:bold;">
                                                                    <td style="text-align: center;">{$c.fk_lgb002_num_item}{$c.fk_lgb003_num_commodity}</td>
                                                                    <td colspan="10">{$c.ind_descripcion}</td>
                                                                </tr>
                                                            {/if}
                                                            {$checkAsignado = ''}
                                                            {if $c.num_flag_asignado==1}
                                                                {$checkAsignado = 'checked'}
                                                            {/if}
                                                            <tr>
                                                                <td>{$c.pk_num_proveedor}</td>
                                                                <td>{$c.nombre}</td>
                                                                <td>{$c.num_cantidad}</td>
                                                                <td>{$c.num_precio_unitario}</td>
                                                                <td>{$c.num_precio_unitario_iva}</td>
                                                                <td>{$c.num_total}</td>
                                                                <td style="vertical-align: top;">
                                                                    <div align="center" class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" class="bloqueoCheck" {$checkAsignado}><span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>{$c.num_dias_entrega}</td>
                                                                <td>{$c.fec_entrega}</td>
                                                                <td>{$c.ind_num_cotizacion}</td>
                                                                <td>{$c.ind_observacion}</td>
                                                            </tr>

                                                        {/foreach}
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                                            <header>Requerimientos</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" style="white-space:nowrap;overflow-x:scroll;overflow-y:scroll;width:100%;height:250px;display: inline-block;" id="contenidoTabla3">
                                                    <thead>
                                                    <tr>
                                                        <th>Requerimiento</th>
                                                        <th>Req. Linea</th>
                                                        <th>Cantidad<i style="color: #ffffff">___</i></th>
                                                        <th>Fecha Pedida</th>
                                                        <th>Fecha Aprobación</th>
                                                        <th>Comentarios<i style="color: #ffffff">________________________________________________</i></th>
                                                        <th>Preparado Por<i style="color: #ffffff">________________________________________________</i></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="requerimientos">
                                                    {if isset($requerimientos)}
                                                        {foreach item=r from=$requerimientos}
                                                            <tr>
                                                                <th>{$r.cod_requerimiento}</th>
                                                                <th>{$r.num_secuencia}</th>
                                                                <th>{$r.num_cantidad_pedida}</th>
                                                                <th>{$r.fec_preparacion}</th>
                                                                <th>{$r.fec_aprobacion}</th>
                                                                <th>{$r.ind_comentarios}</th>
                                                                <th>{$r.nombre}</th>
                                                            </tr>

                                                        {/foreach}
                                                    {/if}

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane floating-label" id="tab5">
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" align="center">
                                            <header>Distribución Contable</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped table-hover" style="width: 100%;display:block;overflow: auto;" id="cuenta">
                                                    <thead style="display: inline-block; width: 100%;">
                                                    <tr>
                                                        <th style="width:10%" class="codCuenta">Cuenta</th>
                                                        <th style="width:100%; text-align: center;">Descripción</th>
                                                        <th style="width:10%; text-align: center;"> Monto</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="contable" style="height: 100px;display: inline-block;width: 100%;overflow: auto;">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" align="center">
                                            <header>Distribución Presupuestaria</header><br>
                                            <div style="background-color:red; width:25px; height:20px;"></div> Sin disponibilidad presupuestaria
                                            <div style="background-color:green; width:25px; height:20px;"></div> Disponibilidad presupuestaria
                                            <div style="background-color:yellow; width:25px; height:20px;"></div> Disponibilidad presupuestaria (Tiene ordenes pendientes)
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped table-hover" style="width: 100%;display:block;overflow: auto;" id="partida">
                                                    <thead style="display: inline-block; width: 100%;">
                                                    <tr>
                                                        <th style="width:10px" class="codPartida">Partida</th>
                                                        <th style="width:100%; text-align: center;">Descripción</th>
                                                        <th style="width:10px; text-align: center;"> Monto</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="presupuesto" style="height: 100px;display: inline-block;width: 100%;overflow: auto;">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--end #tab5 -->
                                <ul class="pager wizard">
                                    <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                    <li class="previous"><a class="btn-raised" href="javascript:void(3);">Anterior</a></li>
                                    <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                    <li class="next"><a class="btn-raised" href="javascript:void(3);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col-lg-12 -->
        </div><!--end .row -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>

        {if $estado=="Listado de" AND $ver!=1 AND !isset($idOrden)}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Guardar
            </button>
        {elseif isset($idOrden) AND $ver!=1}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="fa fa-edit"></span>
                Modificar
            </button>
        {/if}

        {if $estado!="Listado de"}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if $estado=="revisar"}
                    <i class="icm icm-rating"></i>
                {elseif $estado=="conformar"}
                    <i class="icm icm-rating2"></i>
                {elseif $estado=="aprobar"}
                    <i class="icm icm-rating3"></i>
                {/if}
                {$estado}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">

    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "80%");
    $(document).ready(function() {
        function calcularPartidas() {

            $('.porcentajeCuenta').val(0);
            $('.porcentajePartida').val(0);
            var mAfecto = 0;
            var mNoAfecto = 0;
            $('.ic').each(function(){
                var id = $(this).attr('idIC');
                var cantidad = $('#cantidad'+id).val();
                var precio = $('#precio'+id).val();
                var monto = cantidad * precio;
                var idCuenta = $('#cuenta'+id).val();
                var idPartida = $('#partida'+id).val();
                var montoCuenta = $('#porcentajeCuenta'+idCuenta).val();
                var montoPartida = $('#porcentajePartida'+idPartida).val();
                var montoTotalCuenta = parseFloat(monto) + parseFloat(montoCuenta);
                var montoTotalPartida = parseFloat(monto) + parseFloat(montoPartida);
                $('#porcentajeCuenta'+idCuenta).val(montoTotalCuenta.toFixed(6));
                $('#porcentajePartida'+idPartida).val(montoTotalPartida.toFixed(6));

                var url2='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/verificarPartidaMET';

                $.post(url2, { idPartida: idPartida, monto: montoTotalPartida }, function (dato) {
                    if(dato['status']=='error'){
                        $('#idPartida'+idPartida+'Error').attr('style','background-color:red;');
                    } else if(dato['status']=='ok'){
                        $('#idPartida'+idPartida+'Error').attr('style','background-color:green;');
                    } else if(dato['status']=='ok2'){
                        $('#idPartida'+idPartida+'Error').attr('style','background-color:yellow;');
                    }
                },'json');

                var dPor = $('#descPorc'+id).val();
                var dFijo = $('#descFijo'+id).val();
                var descuento = 0;
                if(dPor!=0 && dFijo==0){
                    descuento = (precio*dPor)/100;
                } else if(dPor==0 && dFijo!=0){
                    descuento = dFijo;
                }
                var totalMontoSinIva = cantidad * (precio - descuento);
                var exo = $('#exonerado'+id);
                if (exo.attr('checked')=="checked"){
                    mNoAfecto = parseFloat(mNoAfecto) + parseFloat(totalMontoSinIva);
                } else {
                    mAfecto = parseFloat(mAfecto) + parseFloat(totalMontoSinIva);
                }
            });

            var iva = 0;
            var retencion = 0;
            var url3='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/buscarIvaRetencionMET';
            var id2 = $('#tipo_servicio').val();
            if(id2==null){
                id2 = $('#tipo_servicio_id').val();
            }
            if(id2==null){
                id2 = 0;
            }

            var mBruto = parseFloat(mAfecto) + parseFloat(mNoAfecto);
            var mTotal = mBruto;

            $('#mAfecto').val(mAfecto.toFixed(6));
            $('#mNoAfecto').val(mNoAfecto.toFixed(6));
            $('#mBruto').val(mBruto.toFixed(6));
            $('#mTotal').val(mTotal.toFixed(6));
            $('#mPendiente').val(mTotal.toFixed(6));

            $.post(url3, { id: id2 }, function (dato2) {
                if(dato2[0]['ind_signo']==1 && dato2[0]['ind_signo']!='') {
                    iva = dato2[0]['num_factor_porcentaje'];
                } else if(dato2[0]['ind_signo']==2 && dato2[0]['ind_signo']!='') {
                    retencion = dato2[0]['num_factor_porcentaje'];
                }
                if(dato2.length > 1 && dato2[1]['ind_signo']==1) {
                    iva = dato2[1]['num_factor_porcentaje'];
                } else if(dato2.length > 1 && dato2[1]['ind_signo']==2) {
                    retencion = dato2[1]['num_factor_porcentaje'];
                }
                $('#iva').val(iva);
                var impuestos = mAfecto*(iva/100);
                var mTotal = parseFloat(mBruto) + parseFloat(impuestos);

                $('#impuestos').val(impuestos.toFixed(6));
                $('#mTotal').val(mTotal.toFixed(6));
                $('#mPendiente').val(mTotal.toFixed(6));

                $('#idCuenta'+$('#cuentaIva').val()+'Error').remove();
                $('#idPartida'+$('#partidaIva').val()+'Error').remove();
                if(impuestos>0){
                    var id = 1;
                    $(document.getElementById('contable')).append(
                            '<tr id="idCuenta'+$('#cuentaIva').val()+'Error">' +
                            '<td style="width:20%">' +
                            '<input type="text" class="form-control" disabled value="'+$('#cuentaIva').attr('codigo')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$('#cuentaIva').attr('nombre')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control" disabled value="'+impuestos.toFixed(6)+'"></td>'+
                            '</tr>');
                    $(document.getElementById('presupuesto')).append(
                            '<tr id="idPartida'+$('#partidaIva').val()+'Error">' +
                            '<td style="width:20%">' +
                            '<input type="text" class="form-control" disabled value="'+$('#partidaIva').attr('codigo')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$('#partidaIva').attr('nombre')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control" readonly name="form[int][montoPartida]['+$('#partidaIva').val()+']" value="'+impuestos.toFixed(6)+'"></td>'+
                            '</tr>');
                    var url2='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/verificarPartidaMET';

                    $.post(url2, { idPartida: $('#partidaIva').val(), monto: impuestos }, function (dato) {
                        if(dato['status']=='error'){
                            $('#idPartida'+$('#partidaIva').val()+'Error').attr('style','background-color:red;');
                        } else if(dato['status']=='ok'){
                            $('#idPartida'+$('#partidaIva').val()+'Error').attr('style','background-color:green;');
                        } else if(dato['status']=='ok2'){
                            $('#idPartida'+$('#partidaIva').val()+'Error').attr('style','background-color:yellow;');
                        }
                    },'json');
                }
            },'json');
        }

        $("#formAjax").submit(function(){
            return false;
        });
        var app = new  AppFunciones();

        $('.accionModal').click(function () {
            var url = $(this).attr('url');
            if($(this).attr('id')=='nuevoComm') {
                if($('#clasificacion').val()!=''){
                    url = url+$('#clasificacion').val();
                }
            }
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post(url, { cargar:0, tr:$("#contenidoTabla > tbody > tr").length+1 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    var mensaje = 'Modificada';
                    if(dato['estado']=="revisar"){
                        mensaje = 'Revisada';
                    }else if(dato['estado']=="conformar"){
                        mensaje = 'Conformada';
                    }else if(dato['estado']=="aprobar"){
                        mensaje = 'Aprobada';
                    }
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Orden fue '+mensaje+' satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorCantidades'){
                    swal('Error!','Disculpa. hay cantidades erroneas en los items/commodities','error');
                }else if(dato['status']=='errorPrecio'){
                    swal('Error!','Disculpa. hay precios erroneos en los items/commodities','error');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorPresupuesto'){
                    app.metValidarError(dato,'Disculpa. No existe un presupuesto aprobado para una de las partidas');
                }else if(dato['status']=='errorPresupuesto2'){
                    app.metValidarError(dato,'Disculpa. La partida '+dato['cod_partida']+' no tiene suficiente disponibilidad');
                }else if(dato['status']=='errorIC'){
                    swal("Error!", 'Disculpa. No se han seleccionado items/commodities', "error");
                }else if(dato['status']=='nuevo'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Orden fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        $('#tipoOrden').on('change', function () {
            if($(this).val()=='OS'){
                $('#clasificacion option[value=6]').attr("selected",true);
                var id = 6;
            } else {
                $('#clasificacion option[value=1]').attr("selected",true);
                id = 1;
            }
            $('#s2id_clasificacion .select2-chosen').html($('#clasificacion option:selected').text());

            $('#item').html('');
            $('#contable').html('');
            $('#presupuesto').html('');

            colocarAlmacen(id);
        });

        function colocarAlmacen(idClasificacion){

            if(idClasificacion==''){
                idClasificacion = 1;
            }
            $('#almacen > option').remove();
            var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/buscarClasificacionMET';
            $.post(url, { idClas: idClasificacion }, function (dato) {
                $('#almacen').append('<option value="'+dato['pk_num_almacen']+'" selected flag="'+dato['num_flag_commodity']+'">'+dato['ind_descripcion']+'</option>');

                if($('#clasificacion option:selected').attr('flag')==1 && {$ver}!=1) {
                    $("#nuevoItem").attr('disabled', 'disabled');
                    $("#nuevoComm").removeAttr("disabled");
                } else if({$ver}!=1) {
                    $("#nuevoItem").removeAttr("disabled");
                    $("#nuevoComm").attr('disabled', 'disabled');
                }

                $('#s2id_almacen .select2-chosen').html($('#almacen option:selected').text());
                $('#s2id_almacen a').removeClass('select2-default');
            },'json');
        }

        $('#clasificacion').ready(function() {
            colocarAlmacen($('#clasificacion').val());
        });

        $('#clasificacion').on('change' ,function () {
            var id=$(this).val();
            if($('#clasificacion option:selected').text()=='SERVICIOS'){
                $('#tipoOrden option[value=OS]').attr("selected",true);
            } else {
                $('#tipoOrden option[value=OC]').attr("selected",true);
            }
            $('#s2id_tipoOrden .select2-chosen').html($('#tipoOrden option:selected').text());
            $('#contenidoTabla > tbody >').remove();
            colocarAlmacen(id);
            $('#item').html('');
            $('#contable').html('');
            $('#presupuesto').html('');
        });

        $('#item').on('click', '.eliminar' ,function () {
            var id = $(this).attr('id');
            var cuenta = $('#cuenta'+id).val();
            var partida = $('#partida'+id).val();

            if($(document.getElementById('catidadCuentaIC'+cuenta)).val()==1) {
                $('#idCuenta'+cuenta+'Error').remove();
            } else {
                $(document.getElementById('catidadCuentaIC'+cuenta)).val($(document.getElementById('catidadCuentaIC'+cuenta)).val()-1);
            }

            if($(document.getElementById('catidadPartidaIC'+partida)).val()==1) {
                $('#idPartida'+partida+'Error').remove();
            } else {
                $(document.getElementById('catidadPartidaIC'+partida)).val($(document.getElementById('catidadPartidaIC'+partida)).val()-1);
            }
            $('#item'+id).remove();
            calcularPartidas();
        });

        $('#tipo_servicio').on('change' ,function () {
            var iva = 0;
            var retencion = 0;
            var url2='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/buscarIvaRetencionMET';
            var id = $(this).val();
            $.post(url2, { id: id }, function (dato2) {
                if(dato2[0]['ind_signo']==1 && dato2[0]['ind_signo']!='') {
                    iva = dato2[0]['num_factor_porcentaje'];
                } else if(dato2[0]['ind_signo']==2 && dato2[0]['ind_signo']!='') {
                    retencion = dato2[0]['num_factor_porcentaje'];
                }
                if(dato2.length > 1 && dato2[1]['ind_signo']==1) {
                    iva = dato2[1]['num_factor_porcentaje'];
                } else if(dato2.length > 1 && dato2[1]['ind_signo']==2) {
                    retencion = dato2[1]['num_factor_porcentaje'];
                }
                $('#iva').val(iva);
                $('#retencion').val(retencion);
            },'json');

        });

        $('#item').on('change', '.nuevoValor' ,function () {
            var idTipoServicio=$('#tipo_servicio').val();
            var url='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/buscarIvaRetencionMET';
            var iva = 0;
            var retencion = 0;
            var sec=$(this).attr('sec');
            var cant = $('#cantidad'+sec).val();
            var precio = $('#precio'+sec).val();
            var descPorc = $('#descPorc'+sec).val();
            var descFijo = $('#descFijo'+sec).val();
            var exo = $('#exonerado'+sec);
            var exonerado = 0;
            if (exo.attr('checked')=="checked"){
                exonerado =1;
            }/*
            $('#iva').val(iva);
            $('#retencion').val(retencion);
*/
            $.post(url, { id: idTipoServicio }, function (dato) {
                $('#ContenidoModal2').html(dato);
                if(dato[0]['ind_signo']==1 && dato[0]['ind_signo']!='') {
                    iva = dato[0]['num_factor_porcentaje'];
                } else if(dato[0]['ind_signo']==2 && dato[0]['ind_signo']!='') {
                    retencion = dato[0]['num_factor_porcentaje'];
                }
                if(dato.length > 1 && dato[1]['ind_signo']==1) {
                    iva = dato[1]['num_factor_porcentaje'];
                } else if(dato.length > 1 && dato[1]['ind_signo']==2) {
                    retencion = dato[1]['num_factor_porcentaje'];
                }
                $('#iva').val(iva);
                $('#retencion').val(retencion);

            },'json');

            var iva2 = $('#iva').val();

            var url2='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/calcularMontosMET';
            $.post(url2, { sec: sec, iva: iva2, cant: cant, precio: precio, descPorc: descPorc, descFijo: descFijo, exonerado: exonerado }, function (dato2) {
                $('#ContensecoModal2').html(dato2);
                $('#descPorc'+sec).val(dato2['descPorc']);
                $('#descFijo'+sec).val(dato2['descFijo']);
                $('#montoPU'+sec).val(dato2['precioUnitario']);
                $('#total'+sec).val(dato2['total']);
            },'json');
            calcularPartidas();
        });

        $('#idProveedor').ready(function () {
            var idP=$('#idProveedor').val();

            var url='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/buscarServiciosMET';
            $('#tipo_servicio > option').remove();
            $.post(url, { id: idP}, function (dato) {
                var desde = 0;
                var hasta = dato.length;
                while(desde<hasta){
                    if($('#tipo_servicio_id').val()==dato[desde]['pk_num_tipo_servico']){
                        var selected = 'selected';
                    } else {
                        selected = '';
                    }
                    $(document.getElementById('tipo_servicio')).append(
                            '<option value="'+dato[desde]['pk_num_tipo_servico']+'" '+selected+'>'+dato[desde]['ind_descripcion']+'</option>'
                    );
                    desde=desde+1;
                }
                $('#s2id_tipo_servicio .select2-chosen').html($('#tipo_servicio option:selected').text());
                $('#s2id_tipo_servicio a').removeClass('select2-default');
            },'json');
        });

        $('#tipo_servicio_id').ready(function () {

            var iva = 0;
            var retencion = 0;
            var url2='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/buscarIvaRetencionMET';
            var id = $('#tipo_servicio_id').val();
            $.post(url2, { id: id }, function (dato2) {
                if(dato2[0]['ind_signo']==1 && dato2[0]['ind_signo']!='') {
                    iva = dato2[0]['num_factor_porcentaje'];
                } else if(dato2[0]['ind_signo']==2 && dato2[0]['ind_signo']!='') {
                    retencion = dato2[0]['num_factor_porcentaje'];
                }
                if(dato2.length > 1 && dato2[1]['ind_signo']==1) {
                    iva = dato2[1]['num_factor_porcentaje'];
                } else if(dato2.length > 1 && dato2[1]['ind_signo']==2) {
                    retencion = dato2[1]['num_factor_porcentaje'];
                }
                $('#iva').val(iva);
                $('#retencion').val(retencion);
            },'json');
        });
        $('#item').ready(function () {

            $('.cuenta').each(function ( titulo, valor ) {

                var id = $(this).val();
                var porcentajeCuenta = $(this).attr('cantidad') * $(this).attr('precio');
                if($('#catidadCuentaIC'+id).length>0){
                    var contadorCuenta = parseFloat($('#catidadCuentaIC'+id).val())+parseFloat(1);
                    $('#catidadCuentaIC'+id).val(contadorCuenta);
                } else {
                    $(document.getElementById('contable')).append(
                            '<tr id="idCuenta'+id+'Error" class="idCuenta'+id+'Error">' +
                            '<td style="width:20%"><input type="hidden" class="form-control" id="catidadCuentaIC'+id+'" value="1">' +
                            '<input type="text" class="form-control codCuenta" idCuenta="'+id+'" id="codCuenta'+$(this).attr('codCuenta')+'" disabled value="'+$(this).attr('codCuenta')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$(this).attr('descCuenta')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control porcentajeCuenta" disabled id="porcentajeCuenta'+id+'"></td>'+
                            '</tr>');
                }
            });

            $('.partida').each(function ( titulo, valor ) {
                var id = $(this).val();
                var porcentajePartida = $(this).attr('cantidad') * $(this).attr('precio');
                $('#porcentajePartida'+id).val(porcentajePartida);
                if($('#catidadPartidaIC'+id).length>0){
                    var contadorPartida = parseFloat($('#catidadPartidaIC'+id).val())+parseFloat(1);
                    $('#catidadPartidaIC'+id).val(contadorPartida);
                } else {
                    $(document.getElementById('presupuesto')).append(
                            '<tr id="idPartida'+id+'Error" class="idPartida'+id+'Error">' +
                            '<td style="width:20%"><input type="hidden" class="form-control" id="catidadPartidaIC'+id+'" value="1">' +
                            '<input type="text" class="form-control codPartida" idPartida="'+id+'" id="codPartida'+$(this).attr('codPartida')+'" disabled value="'+$(this).attr('codPartida')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$(this).attr('descPartida')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control porcentajePartida" readonly name="form[int][montoPartida]['+id+']" id="porcentajePartida'+id+'"></td>'+
                            '</tr>');
                }

            });
        });

        $('.bloqueoCheck').on('click',function () {
            var checked = $(this).attr('checked');
            if(checked=='checked'){
                $(this).attr('checked',false);
            } else {
                $(this).attr('checked',true);
            }
        });

        $('#dependencia').on('change', function () {
            var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/buscarCentroCostoMET';
            $.post(url, { idDependencia: $(this).attr('value') }, function (dato) {
                $('#centro_costoError > select > option').remove();
                $('#centro_costo').append('<option value=""></option>');

                for(var i=0; i<dato.length; i++){
                    $('#centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                }
                $('#s2id_centro_costo .select2-chosen').html('');

            },'json');
        });

        $('#dependencia').ready( function () {
            var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/buscarCentroCostoMET';
            $.post(url, { idDependencia: $('#dependencia').val() }, function (dato) {
                $('#centro_costoError > select > option').remove();
                $('#centro_costo').append('<option value=""></option>');

                for(var i=0; i<dato.length; i++){
                    if($('#cc').val()==dato[i]['pk_num_centro_costo'] ){
                        $('#centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'" selected>'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    } else {
                        $('#centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    }
                }
                $('#s2id_centro_costo .select2-chosen').html($('#centro_costo option:selected').text());
                $('#s2id_centro_costo a').removeClass('select2-default');

            },'json');

        });

        $('#rootwizard1').ready(function () {
            calcularPartidas();
        });
    });


</script>