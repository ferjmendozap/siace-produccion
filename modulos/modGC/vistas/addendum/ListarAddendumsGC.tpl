<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: GESTIÓN DE CONTRATOS
* PROCESO:
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
     <section class="style-default-bright">

        <!-- Listado -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-actionbar">

                </div>
            </div><!--end .col -->
        </div><!--end .row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <form id="filtro"  action="{$_Parametros.url}modPA/solicitudCONTROL/ListarSolicitudMET"  class="form" role="form" method="post">
                            <thead>
                            <tr>

                                <th class="sort-alpha col-sm-1">N° </th>



                                <th class="sort-alpha col-sm-4">Objeto del Addendum</th>



                                <th class="sort-alpha col-sm-2">Estatus</th>

                                {if in_array('GC-01-01-01-04-P',$_Parametros.perfil)} <th class=" col-sm-1">Ver Addendum</th>      {/if}


                        </tr>
                        </thead>
                        <tbody>

                        {foreach item=post from=$addendums}

                            <tr id="idPost{$post.pk_num_registro_contrato}" class="gradeA">
                                <td> {$post.pk_num_registro_contrato} </td>
                                <td>{$post.ind_objeto_contrato}</td>




                                    <td>
                                        {$post.ind_nombre_detalle}

                                    </td>




                                {if in_array('GC-01-01-01-01-V',$_Parametros.perfil)}
                                    <td>
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Addendum {$post.ind_correlativo_contrato}" descipcion="El Usuario a Visualizado un Addendum  " idVer="{$post.pk_num_registro_contrato}" data-backdrop="false" data-keyboard="false" data-target="#formModal"   >
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    </td>
                                {/if}



                            </tr>
                        {/foreach}
                        </tbody>

                    </form>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->


         <!-- fin listado -->
         <script>


             $('#datatable1 tbody').on( 'click', '.ver', function () {
                 var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/RedactarProyectoMET';
                 $('#modalAncho').css( "width", "95%" );
                 $('#formModalLabel').html($(this).attr('titulo'));
                 $.post($url,{ idPost: $(this).attr('idVer'),accion:"Ver"},function($dato){
                     $('#ContenidoModal').html($dato);
                 });
             });


         </script>

