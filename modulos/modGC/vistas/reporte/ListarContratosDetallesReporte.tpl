<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary"> REPORTE CONTRATOS </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Reporte en pdf de los contratos .</h5>
    </div><!--end .col -->

</div><!--end .card -->

<div class="card   ">
    <div class="card-head">
        <header>Búsqueda</header>
    </div>


         <div class="col-lg-12">


            <div class="col-sm-4">
                <div class="col-sm-2 text-right">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" value="1" id="checkProveedor">
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label for="pk_num_tipo_documento"
                           class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                </div>
                <div class="col-sm-2">
                    <input type="text"
                           name="form[int][fk_a003_num_persona_proveedor]"
                           class="form-control" id="codigoProveedor"
                           value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.fk_a003_num_persona_proveedor}{/if}"
                           readonly size="9%">
                </div>
                <div class="col-sm-6">
                    <div class="col-sm-10">
                        <input type="text" class="form-control"
                               id="nombreProveedor"
                               value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.proveedor}{/if}"
                               disabled>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group floating-label">
                            <button
                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                    type="button"
                                    data-toggle="modal"
                                    data-target="#formModal2"
                                    titulo="Listado de Personas"
                                    id="botonPersona"
                                    disabled="disabled"
                                    url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-2 text-right">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" value="1" id="checkFecha">
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label for="pago"
                           class="control-label" style="margin-top: 10px;"> Fecha :</label>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control text-center date"
                           id="desde"
                           style="text-align: center"
                           value=""
                           placeholder="Desde"
                           disabled="disabled"
                           readonly>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control text-center date"
                           id="hasta"
                           style="text-align: center"
                           value=""
                           placeholder="Hasta"
                           disabled="disabled"
                           readonly>
                </div>
            </div>
             <div class="col-sm-2 ">
                 <div class="form-group   " >
                     <label>  Año</label>
                     <select id="anio" name="form[int][anio]" class="form-control  " >
                         <option value=""> Todos </option>
                         <option value="2016"> 2016 </option>
                         <option value="2017"> 2017 </option>
                         <option value="2018"> 2018 </option>
                         <option value="2018"> 2019 </option>
                     </select>

                 </div>
             </div>








        </div>
<div class="form-group   col-lg-12 ">

    <button type="button"  data-toggle="modal" data-target="#formModal"  class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:7px">Consultar</button>
    </button>

</div>

</div><!--end .card -->

<section class="style-default-bright">




    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
            $('#fechas').datepicker({ format: 'dd/mm/yyyy' });

            $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

            $('.accionModal').click(function () {
                $('#formModalLabel2').html($(this).attr('titulo'));
                $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                    $('#ContenidoModal2').html($dato);
                });
            });

            //busqueda segun el filtro
            $('#btnConsultar').click(function () {
                var proveedor = $('#codigoProveedor').val();
                var desde = $('#desde').val();
                var hasta = $('#hasta').val();

                var anio = $('#anio').val();


                var url = '{$_Parametros.url}modGC/reporteCONTROL/ContratosDetallesReportePdfMET/?proveedor='+proveedor+'&desde='+desde+'&hasta='+hasta +'&anio='+anio ;

                $('#ContenidoModal').html('<iframe src="'+url+'" width="100%" height="950"></iframe>');
            });

            //habilitar y deshabilitar campos del filtro
            $('#checkProveedor').click(function () {
                if($('#botonPersona').attr('disabled') =='disabled') {
                    $('#botonPersona').attr('disabled', false);
                }else{
                    $('#botonPersona').attr('disabled', true);
                    $(document.getElementById('codigoProveedor')).val("");
                    $(document.getElementById('nombreProveedor')).val("");

                }
            });



            $('#checkFecha').click(function () {
                if(this.checked){
                    $('#desde').attr('disabled', false);
                    $('#hasta').attr('disabled', false);
                }else{
                    $('#desde').attr('disabled', true);
                    $(document.getElementById('desde')).val("");
                    $('#hasta').attr('disabled', true);
                    $(document.getElementById('hasta')).val("");

                }
            });


        });
    </script>





