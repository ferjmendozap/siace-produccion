<?php
require_once 'datosCarga/dataBasicaInicialMiscelaneos.php';
require_once 'datosCarga/dataBasicaInicialMenu.php';
require_once 'datosCarga/dataGestionContrato.php';

class scriptCargaControlador extends Controlador
{

    use dataBasicaInicialMiscelaneos;
    use dataBasicaInicialMenu;
    use dataGestionContrato;
    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga');

    }



    public function metIndex()
    {
        echo "INICIANDO <br>";
        echo "Cargando Menu de Gestión de Contrato <br>";
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>MENU<br>';
        $this->atScriptCarga->metCargarMenu($this->metDataMenu());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>MISCELANEOS<br>';
        $this->atScriptCarga->metCargarMiscelaneos($this->metDataMiscelaneos());
        ### Gestión de Contrato ###

        $this->metGestionContrato();

        echo "TERMINADO";
    }


    ### Función ParqueAutomotor
    public function metGestionContrato()
    {
        echo 'INICIO CARGA DE GESTION DE CONTRATO<br><br>';
        echo '-----Carga Tipo de Contrato----<br><br>';
        $this->atScriptCarga->metTipoContrato($this->metDataTipoContrato());
        echo '-----Carga Aplicable Contrato----<br><br>';
        $this->atScriptCarga->metAplicableContrato($this->metDataAplicableContrato());



    }




}
