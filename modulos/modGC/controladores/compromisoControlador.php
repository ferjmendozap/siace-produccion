<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/

class compromisoControlador extends Controlador
{
    private $atCompromisoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atCompromisoModelo = $this->metCargarModelo('compromiso');
        $this->atContratoModelo = $this->metCargarModelo('contrato');
        $this->atProyectoContratoModelo = $this->metCargarModelo('proyectoContrato');
    }
	


    #Metodo Index del controlador Contrato
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );



        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }

        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_compromisoPost',$this->atCompromisoModelo->metGetCompromisos('0'));

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarCompromisos');
    }
    public function metListarRevisar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );



        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_compromisoPost',$this->atCompromisoModelo->metGetCompromisos('PR'));

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarCompromisosRevisar');
    }

    public function metListarAprobar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );



        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_compromisoPost',$this->atCompromisoModelo->metGetCompromisos('RV'));

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarCompromisosAprobar');
    }


    public function metConsultarContrato()
    {


     $idContrato=$this->metObtenerInt('idContrato');
     $consulta = $this->atCompromisoModelo->metGetContratoCompromisoId($idContrato);
     $datos=[];
     $datos['objeto'] = $consulta[0]['ind_objeto_contrato'];
     $datos['monto']=$consulta[0]['num_monto_contrato'];
     $datos['correlativo']=$consulta[0]['ind_correlativo_contrato'];


     echo json_encode($datos);
     exit;

    }


    public function metModificarCompromiso()
    {

        $id=$this->metObtenerInt('id');
        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "modificar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {


            $Estatus=$this->metObtenerAlphaNumerico('Estatus');
            $idContrato=$this->metObtenerInt('Contrato');
            $idComprometido=$this->metObtenerInt('idComprometido');
            if($Estatus=='PR'){
                $NEstatus='RV';
                $this->atCompromisoModelo->metUpdateCompromiso($idComprometido,$NEstatus);
            }else if($Estatus=='RV'){
                $NEstatus='AP';
                $this->atCompromisoModelo->metUpdateCompromiso($idComprometido,$NEstatus);
                $respuesta=$this->atProyectoContratoModelo->metGetPresupuestoDet($idContrato);
                for($i=0;$i < count($respuesta);$i++){

                    $this->atProyectoContratoModelo->metContratoComprometer ( $respuesta[$i]['num_monto'],$idContrato,$respuesta[$i]['pk_num_presupuesto_det']);


                }
                 $this->atProyectoContratoModelo->metUpdateContratoPresupuesto($idContrato);

            }



                $respuesta = $this->atCompromisoModelo->metGetContratoCompromisoId($idContrato);
                $datos=[];
                $datos['status'] = 'modificar';
                $datos['idComprometido']=$idComprometido;
                $datos['Estatus']=$NEstatus;
                $datos['Correlativo']=$respuesta[0]['ind_correlativo_contrato'];
                $datos['Objeto']=$respuesta[0]['ind_objeto_contrato'];
                $datos['Monto']=$respuesta[0]['num_monto_contrato'];

                echo json_encode($datos);
                exit;





        }

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('_compromisoBDPost',$this->atCompromisoModelo->metGetContratoCompromisoId($id));

        $this->atVista->assign('_compromisoPost',$this->atCompromisoModelo->metGetcontratoCompromiso());

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarCompromiso',"modales");




    }

    public function metRegistrarCompromiso()
    {


        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {



            $idContrato=$this->metObtenerInt('idContrato');
            $Correlativo=$this->metObtenerTexto('Correlativo');
            $id = $this->atCompromisoModelo->metSetCompromiso($idContrato);


            if(is_array($id)){

                $validacion=[];
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;

            }else{
                $respuesta = $this->atCompromisoModelo->metGetContratoCompromisoId($idContrato);
                $datos=[];
                $datos['status'] = 'registro';
                $datos['idComprometido']=$id;
                $datos['idContrato']=$respuesta[0]['pk_num_registro_contrato'];
                $datos['Estatus']='PR';
                $datos['Correlativo']=$respuesta[0]['ind_correlativo_contrato'];
                $datos['Objeto']=$respuesta[0]['ind_objeto_contrato'];
                $datos['Monto']=$respuesta[0]['num_monto_contrato'];

                echo json_encode($datos);
                exit;

            }



        }

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('_compromisoPost',$this->atCompromisoModelo->metGetcontratoCompromiso());

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarCompromiso',"modales");




    }

    public function metAnularCompromiso()
    {

        $id_valor=$this->metObtenerInt('id');
        $estado=$this->metObtenerAlphaNumerico('estado');
        $id_contrato=$this->metObtenerInt('idContrato');

        if($estado=="AP"){

            $estado="RV";

        }else if ($estado=="RV"){

            $estado="PR";

        }else if ($estado=="PR"){

            $estado="AN";

        }

        $this->atCompromisoModelo->metUpdateCompromiso($id_valor,$estado,1);
        $this->atCompromisoModelo->metAnularEstadoDistribucion($id_contrato);
    }


}
