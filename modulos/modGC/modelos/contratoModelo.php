<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS .'ordenCompra'. DS .'ordenCompraModelo.php';
require_once RUTA_Modulo . 'modRH' . DS . 'modelos' . DS . 'maestros' . DS . 'beneficiosHcmModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'compras' . DS . 'requerimientoModelo.php';

class contratoModelo extends miscelaneoModelo
{

    public function __construct()
    {

        parent::__construct();
        $this->atOrden = new ordenCompraModelo();
        $this->atbeneficiosHcmModelo = new beneficiosHcmModelo();
        $this->atMiscelaneoModelo = new miscelaneoModelo();


    }

    #metodo para obtener los datos de los proyectos de contratos a mostrar en los listados
    public function metGetcontrato()
    {


        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT gc_b001_registro_contrato.*,gc_b002_registro_contrato_detalle.* ,a003_persona.ind_nombre1,a003_persona.ind_documento_fiscal,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
         a006_miscelaneo_detalle.ind_nombre_detalle
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
        left join a003_persona on (a003_persona.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor )
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )
        where  fk_a006_num_estado_contrato='5' AND   fk_gcb001_addendum=0
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetcontratoPresupuesto()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT gc_b001_registro_contrato.*,gc_b002_registro_contrato_detalle.*, a003_persona.ind_nombre1,a003_persona.ind_apellido1,a003_persona.ind_documento_fiscal,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
         a006_miscelaneo_detalle.ind_nombre_detalle
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
 
        left join a003_persona on (a003_persona.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor )
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )
        where  fk_a006_num_estado_contrato='5' AND num_estado_presupuesto=0
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metGetcontratoObligaciones()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT gc_b001_registro_contrato.*,gc_b002_registro_contrato_detalle.*, a003_persona.ind_nombre1,a003_persona.ind_apellido1,a003_persona.ind_documento_fiscal,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
         a006_miscelaneo_detalle.ind_nombre_detalle
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
 
        left join a003_persona on (a003_persona.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor )
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )
        where  (fk_a006_num_estado_contrato='5' AND num_estado_presupuesto=1 AND num_guia_contrato=1 AND  fk_gcb001_addendum=0) OR  (fk_a006_num_estado_contrato='5' AND num_guia_contrato=2 AND  fk_gcb001_addendum=0 )
        
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetcontratoOrdenes()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT gc_b001_registro_contrato.*,gc_b002_registro_contrato_detalle.*, a003_persona.ind_nombre1,a003_persona.ind_apellido1,a003_persona.ind_documento_fiscal,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
         a006_miscelaneo_detalle.ind_nombre_detalle
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
 
        left join a003_persona on (a003_persona.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor )
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )
        where     (fk_a006_num_estado_contrato='5' AND num_guia_contrato=3 AND fk_gcb001_addendum=0)
        
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }


    public function metRelacionarOrdenContrato($idContrato, $idOrden)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $consulta = $this->_db->prepare(
            "insert into gc_c002_relacion_orden (
             fk_gcb001_registro_contrato,
             fk_lgb019_orden
             )
             values (
             :fk_gcb001_registro_contrato,
             :fk_lgb019_orden
             )"
        );
        $consulta->execute(array(
            ':fk_gcb001_registro_contrato' =>$idContrato,
            ':fk_lgb019_orden' => $idOrden,

        ));


        $id = $this->_db->lastInsertId();

        $fallaTansaccion = $consulta->errorInfo();

        if ((!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            #commit — Consigna una transacción
            $this->_db->commit();

            return $id;

        }

    }



}



