<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class reporteModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    public function metGetProyectoId($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT
        gc_b001_registro_contrato.pk_num_registro_contrato,
		gc_c003_tipo_contrato.ind_descripcion as tipoContrato,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.ind_correlativo,
         gc_b001_registro_contrato.ind_correlativo_contrato,
        gc_b001_registro_contrato.num_monto_contrato,
        gc_b001_registro_contrato.ind_cuerpo,
gc_b001_registro_contrato.fk_a006_num_estado_contrato,
        gc_b001_registro_contrato.fk_a004_dependencia,
        gc_b001_registro_contrato.fk_gcc004_num_aplicable_contrato,
        gc_b001_registro_contrato.fk_gcc003_num_tipo_contrato,
        gc_b001_registro_contrato.fk_a006_num_forma_pago,
        gc_b003_predeterminado.ind_encabezado,
        gc_b001_registro_contrato.fk_a003_proveedor,
        gc_b001_registro_contrato.fk_a003_representante,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_desde,  '%d/%m/%Y') as  fec_desde,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_hasta,  '%d/%m/%Y') as  fec_hasta,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_entrada,'%d/%m/%Y') as  fec_entrada,

        gc_b002_registro_contrato_detalle.ind_obligaciones_contratante,
        gc_b002_registro_contrato_detalle.ind_obligaciones_contratado,
        gc_b002_registro_contrato_detalle.ind_observacion,
        gc_b002_registro_contrato_detalle.ind_lugar_pago,
        lg_b022_proveedor.fk_a003_num_persona_proveedor,proveedor.ind_nombre1 as proveedor,proveedor.ind_documento_fiscal ,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
        a006_miscelaneo_detalle.ind_nombre_detalle,lg_b022_proveedor.fk_a003_num_persona_representante,representante.ind_apellido1,representante.ind_nombre1,representante.ind_cedula_documento
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
        left join lg_b022_proveedor on (lg_b022_proveedor.pk_num_proveedor=gc_b001_registro_contrato.fk_a003_proveedor)
        left join a003_persona as proveedor on (proveedor.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_proveedor )
        left join a003_persona as representante on (representante.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_representante )
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )

        left join gc_b003_predeterminado on (gc_b003_predeterminado.pk_num_predeterminado=gc_b001_registro_contrato.fk_gcc003_num_tipo_contrato)
		
        left join gc_c003_tipo_contrato on (gc_c003_tipo_contrato.pk_num_tipo_contrato=gc_b001_registro_contrato.fk_gcc003_num_tipo_contrato)
        where pk_num_registro_contrato='".$id."' "
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }


    public function metGetProyectoPDF($proveedor= false,$desde= false , $hasta= false, $estatus= false, $anio= false)
    {

        if($proveedor){
            $complemento1= " AND lg_b022_proveedor.fk_a003_num_persona_proveedor='".$proveedor."'";
        }else{
            $complemento1= " ";
        }

        if($desde){
            $complemento2= " AND gc_b002_registro_contrato_detalle.fec_desde>='".$desde."'";
        }else{
            $complemento2= " ";
        }

        if($hasta){
            $complemento3= " AND gc_b002_registro_contrato_detalle.fec_hasta<='".$hasta."'";
        }else{
            $complemento3= " ";
        }

        if($estatus){
            $complemento4= " AND gc_b001_registro_contrato.fk_a006_num_estado_contrato='".$estatus."'";
        }else{
            $complemento4= " ";
        }

        if($anio){
            $complemento5= " AND gc_b001_registro_contrato.num_anio='".$anio."'";
        }else{
            $complemento5= " ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(" SELECT
        gc_b001_registro_contrato.pk_num_registro_contrato,
		gc_c003_tipo_contrato.ind_descripcion as tipoContrato,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.ind_correlativo,
         gc_b001_registro_contrato.ind_correlativo_contrato,
        gc_b001_registro_contrato.num_monto_contrato,
        gc_b001_registro_contrato.ind_cuerpo,
        gc_b001_registro_contrato.fk_a006_num_estado_contrato,
        gc_b001_registro_contrato.fk_a004_dependencia,
        gc_b001_registro_contrato.fk_gcc004_num_aplicable_contrato,
        gc_b001_registro_contrato.fk_gcc003_num_tipo_contrato,
        gc_b001_registro_contrato.fk_a006_num_forma_pago,
        gc_b003_predeterminado.ind_encabezado,
        gc_b001_registro_contrato.fk_a003_proveedor,
        gc_b001_registro_contrato.fk_a003_representante,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_desde,  '%d/%m/%Y') as  fec_desde,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_hasta,  '%d/%m/%Y') as  fec_hasta,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_entrada,'%d/%m/%Y') as  fec_entrada,

 
        proveedor.ind_nombre1 as proveedor,proveedor.ind_documento_fiscal ,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
        a006_miscelaneo_detalle.ind_nombre_detalle, representante.ind_apellido1,representante.ind_nombre1,representante.ind_cedula_documento
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
 
        left join a003_persona as proveedor on (proveedor.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor)
        left join a003_persona as representante on (representante.pk_num_persona=gc_b001_registro_contrato.fk_a003_representante)
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )

        left join gc_b003_predeterminado on (gc_b003_predeterminado.pk_num_predeterminado=gc_b001_registro_contrato.fk_gcc003_num_tipo_contrato)
		
        left join gc_c003_tipo_contrato on (gc_c003_tipo_contrato.pk_num_tipo_contrato=gc_b001_registro_contrato.fk_gcc003_num_tipo_contrato)
        where fk_a006_num_estado_contrato<5 $complemento1  $complemento2 $complemento3 $complemento4 $complemento5"
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }



    public function metGetReporteContrato($proveedor= false,$desde= false , $hasta= false,   $anio= false)
    {



        if($proveedor){
            $complemento1= " AND  gc_b001_registro_contrato.fk_a003_proveedor='".$proveedor."'";
        }else{
            $complemento1= " ";
        }

        if($desde){
            $complemento2= " AND gc_b002_registro_contrato_detalle.fec_desde>='".$desde."'";
        }else{
            $complemento2= " ";
        }

        if($hasta){
            $complemento3= " AND gc_b002_registro_contrato_detalle.fec_hasta<='".$hasta."'";
        }else{
            $complemento3= " ";
        }



        if($anio){
            $complemento4= " AND gc_b001_registro_contrato.num_anio='".$anio."'";
        }else{
            $complemento4= " ";
        }


        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT

        representante.ind_nombre1,
        gc_b001_registro_contrato.pk_num_registro_contrato,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.fk_a004_dependencia,
        gc_b001_registro_contrato.ind_correlativo_contrato,
        gc_b001_registro_contrato.fk_a003_proveedor,

        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_desde,'%d/%m/%Y') as  fec_desde,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_hasta,'%d/%m/%Y') as  fec_hasta,
        proveedor.ind_nombre1 as proveedor,
        gc_b001_registro_contrato.num_monto_contrato

        FROM gc_b001_registro_contrato
        
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
        left join a003_persona as proveedor on (proveedor.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor)
        left join a003_persona as representante on (representante.pk_num_persona=gc_b001_registro_contrato.fk_a003_representante)
        where gc_b001_registro_contrato.fk_a006_num_estado_contrato='5'   AND gc_b001_registro_contrato.fk_gcb001_addendum=0 $complemento1  $complemento2 $complemento3 $complemento4  "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metGetS($anio,$desde,$hasta)
    {
        $parcial_desde=explode("/",$desde);
        $desde=$anio."-".$parcial_desde[1]."-".$parcial_desde[0];

        $parcial_hasta=explode("/",$hasta);
        $hasta=$anio."-".$parcial_hasta[1]."-".$parcial_hasta[0];
        if($anio!="0"){
            $complemento1=" AND gc_b001_registro_contrato.num_anio='".$anio."' ";
        }

        if($desde!="error"){
            $complemento2=" AND gc_b002_registro_contrato_detalle.fec_desde>='".$desde."' ";
        }


        if($hasta!="error"){
            $complemento3=" AND gc_b001_registro_contrato.fec_hasta<='".$hasta."' ";
        }
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT lg_b022_proveedor.fk_a003_num_persona_proveedor,a003_persona.ind_nombre1,
        gc_b001_registro_contrato.pk_num_registro_contrato,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.fk_a004_dependencia,
        gc_b001_registro_contrato.ind_correlativo_contrato,
        gc_b001_registro_contrato.fk_a003_proveedor,

        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_desde,'%d/%m/%Y') as  fec_desde,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_hasta,'%d/%m/%Y') as  fec_hasta,

        gc_b001_registro_contrato.num_monto_contrato
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
        left join lg_b022_proveedor on (lg_b022_proveedor.pk_num_proveedor=gc_b001_registro_contrato.fk_a003_proveedor)
        left join a003_persona on (a003_persona.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_proveedor )
        where gc_b001_registro_contrato.fk_a006_num_estado_contrato='5' $complemento1 $complemento2 $complemento3 "
        );



        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



    public function metGetReporteProyectoContrato($anio,$estatus )
    {


        $complemeto0="AND gc_b001_registro_contrato.fk_a006_num_estado_contrato<'5'";

        if($anio!="error"){
            $complemento1=" AND gc_b001_registro_contrato.num_anio='".$anio."' ";
        }else{
            $complemento1="  ";
        }

      if($estatus!="error"){
            $complemento2=" AND gc_b001_registro_contrato.fk_a006_num_estado_contrato='".$estatus."' ";
            $complemeto0= " ";
        }else{
            $complemento2="  ";

        }



        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT
        lg_b022_proveedor.fk_a003_num_persona_proveedor,
        a003_persona.ind_nombre1,
        gc_b001_registro_contrato.pk_num_registro_contrato,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.fk_a006_num_estado_contrato,
        gc_b001_registro_contrato.fk_a004_dependencia,
        gc_b001_registro_contrato.ind_correlativo_contrato,
        gc_b001_registro_contrato.fk_a003_proveedor,
      gc_b001_registro_contrato.ind_correlativo,
        a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
        a006_miscelaneo_detalle.ind_nombre_detalle,

        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_desde,'%d/%m/%Y') as  fec_desde,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_hasta,'%d/%m/%Y') as  fec_hasta,

        gc_b001_registro_contrato.num_monto_contrato

        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
        left join lg_b022_proveedor on (lg_b022_proveedor.pk_num_proveedor=gc_b001_registro_contrato.fk_a003_proveedor)
        left join a003_persona on (a003_persona.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_proveedor )

        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )



        where 1  $complemeto0 $complemento1 $complemento2   "
        );





        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


}

