<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Control de Eventos.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// // Esta clase se encarga de gestionar los certificados de los eventos
class certificadosModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite obtener los nombres de las imagenes
    public function metListarCertificado($metodo)
    {
        $listarCertificado =  $this->_db->query(
            "select a.pk_num_certificado, a.ind_imagen, a.num_estatus, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%m:%s') as fecha_modificacion, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from ev_c005_certificado as a, a018_seguridad_usuario as b, vl_rh_persona_empleado as c where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado order by a.pk_num_certificado desc"
        );
        $listarCertificado->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $listarCertificado->fetchAll();
        } else {
            return $listarCertificado->fetch();
        }

    }

    // Método que permite guardar una imagen
    public function metGuardarImagen($indImagen, $fechaHora , $usuario)
    {
        $this->_db->beginTransaction();
        $nuevaImagen = $this->_db->prepare(
            "insert into ev_c005_certificado values (null, :ind_imagen, :num_estatus, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $nuevaImagen->execute(array(
            ':ind_imagen' => $indImagen,
            ':num_estatus' => 1,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fec_ultima_modificacion' => $fechaHora
        ));
        $this->_db->commit();
    }

    // Método que permite eliminar un certificado
    public function metEliminarCertificado($indImagen)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ev_c005_certificado where ind_imagen = '$indImagen'"
        );
        $this->_db->commit();
    }

    // Método que permite visualizar un certificado
    public function metVerCertificado($pkNumCertificado)
    {
        $listarCertificado =  $this->_db->query(
            "select a.pk_num_certificado, a.ind_imagen, a.num_estatus, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%m:%s') as fecha_modificacion, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from ev_c005_certificado as a, a018_seguridad_usuario as b, vl_rh_persona_empleado as c where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and a.pk_num_certificado=$pkNumCertificado"
        );
        $listarCertificado->setFetchMode(PDO::FETCH_ASSOC);
        return $listarCertificado->fetch();
    }

    // Método que permite cambiar el estatus de un certificado
    public function metCambiarEstatus($pkNumCertificado, $valor)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ev_c005_certificado set num_estatus=$valor where pk_num_certificado=$pkNumCertificado"
        );
        $this->_db->commit();
    }

}// fin de la clase
?>
