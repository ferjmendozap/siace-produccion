<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Control de Eventos.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// La clase temaModelo permite gestionar los temas de los eventos
class temaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite listar los temas añadidos al sistema
    public function metListarTema()
    {
        $listarTema = $this->_db->query(
            "select a.pk_num_tema_evento, a.num_estatus, a.ind_tema, count(a.pk_num_tema_evento) as total from ev_c002_tema as a left join ev_c004_tema_evento as b on a.pk_num_tema_evento=b.fk_evc002_num_tema group by a.pk_num_tema_evento"
        );
        $listarTema->setFetchMode(PDO::FETCH_ASSOC);
        return $listarTema->fetchAll();
    }

    // Método que permite guardar un tema en el sistema
    public function metGuardarTema($indTemaEvento, $fechaHora , $usuario)
    {
        $this->_db->beginTransaction();
        $nuevoTema = $this->_db->prepare(
            "insert into ev_c002_tema values (null, :ind_tema, :num_estatus, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $nuevoTema->execute(array(
        ':ind_tema' => $indTemaEvento,
        ':num_estatus' => 1,
        ':fk_a018_num_seguridad_usuario' => $usuario,
        ':fec_ultima_modificacion' => $fechaHora
    ));
        $pkNumTemaEvento = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumTemaEvento;
    }

    // Método que permite verificar la existencia en el sistema del tema que se quiere registrar
    public function metBuscarTema($indTema)
    {
        $buscarTema = $this->_db->query(
            "select pk_num_tema_evento from ev_c002_tema where ind_tema='$indTema'"
        );
        $buscarTema->setFetchMode(PDO::FETCH_ASSOC);
        $arreglo = $buscarTema->fetchAll();
        if (count($arreglo) > 0) {
            return true;
        } else {
            return false;
        }
    }

    // Método que permite buscar un tema en especifico
    public function metVerTema($pkNumTemaEvento)
    {
        $verTema = $this->_db->query(
            "select a.pk_num_tema_evento, a.num_estatus, a.ind_tema, a.fec_ultima_modificacion, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, (select count(fk_evc002_num_tema) as total from ev_c004_tema_evento where fk_evc002_num_tema=$pkNumTemaEvento) as total from ev_c002_tema as a, a018_seguridad_usuario as b, vl_rh_persona_empleado as c where a.pk_num_tema_evento=$pkNumTemaEvento and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado"
        );
        $verTema->setFetchMode(PDO::FETCH_ASSOC);
        return $verTema->fetch();
    }

    // Método que permite cambiar el estatus de un tema
    public function metCambiarEstatus($pkNumTemaEvento, $valor)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ev_c002_tema set num_estatus=$valor where pk_num_tema_evento=$pkNumTemaEvento"
        );
        $this->_db->commit();
    }

    // Método que permite editar un tema
    public function metEditarTema($indTema, $fecha_hora, $usuario, $pkNumTemaEvento)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ev_c002_tema set ind_tema='$indTema', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario=$usuario where pk_num_tema_evento=$pkNumTemaEvento"
        );
        $this->_db->commit();
    }

    // Listado de temas
    public function metConsultarTema($pkNumTemaEvento)
    {
        $listarTema = $this->_db->query(
            "select count(fk_evc002_num_tema) as total from ev_c004_tema_evento where fk_evc002_num_tema=$pkNumTemaEvento"
        );
        $listarTema->setFetchMode(PDO::FETCH_ASSOC);
        return $listarTema->fetch();
    }

    public function metEliminarTema($pkNumTemaEvento)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ev_c002_tema where pk_num_tema_evento = $pkNumTemaEvento"
        );
        $this->_db->commit();
    }
}// fin de la clase
?>
