<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Control de Eventos.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | 2 | Maikol J. Isava D    | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// // Esta clase se encarga de gestionar eventos
require_once RUTA_MODELO.'miscelaneoModelo.php';
class gestionEventoModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite listar los Eventos añadidos al sistema
    public function metListarEvento()
    {
        $listarEvento = $this->_db->query(
            "select pk_num_evento, ind_nombre_evento, date_format(fec_registro, '%d/%m/%Y') as fecha_registro, ind_certificado, date_format(fec_inicio, '%d/%m/%Y') as fecha_inicio from ev_b001_evento order by pk_num_evento desc"
        );
        $listarEvento->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEvento->fetchAll();
    }

    // Método que permite listar los lugares añadidos al sistema
    public function metListarLugar()
    {
        $listarLugar = $this->_db->query(
            "select a.pk_num_lugar_evento, a.num_estatus, a.ind_lugar_evento, d.ind_municipio, e.ind_estado, h.ind_ciudad from ev_c003_lugar as a, a013_sector as b, a012_parroquia as c, a011_municipio as d, a009_estado as e, a008_pais as f, a014_ciudad_municipio as g, a010_ciudad as h where a.fk_a013_num_sector=b.pk_num_sector and b.fk_a012_num_parroquia=c.pk_num_parroquia and c.fk_a011_num_municipio=d.pk_num_municipio and d.fk_a009_num_estado=e.pk_num_estado and e.fk_a008_num_pais=f.pk_num_pais and d.pk_num_municipio=g.fk_a011_num_municipio and g.fk_a010_num_ciudad=h.pk_num_ciudad  and a.num_estatus=1"
        );
        $listarLugar->setFetchMode(PDO::FETCH_ASSOC);
        return $listarLugar->fetchAll();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if ($metodo == 1) {
            return $obtenerUsuario->fetch();
        } else {
            return $obtenerUsuario->fetchAll();
        }
    }

    // Método que permite obtener la dependencia, fecha de ingreso y el organismo al que pertenece el empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.fk_a006_num_miscelaneo_detalle_sexo, b.num_sueldo_basico, a.fec_nacimiento, date_format(a.fec_nacimiento, '%d/%m/%Y') as fecha_nacimiento, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, b.fec_ingreso, d.ind_nombre_detalle, e.pk_num_puestos, e.ind_descripcion_cargo, f.ind_descripcion_empresa, g.ind_dependencia, g.pk_num_dependencia, f.pk_num_organismo, b.fk_rhc063_num_puestos_cargo from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a005_miscelaneo_maestro as c, a006_miscelaneo_detalle as d, rh_c063_puestos as e, a001_organismo as f, a004_dependencia as g where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and c.cod_maestro='SEXO' and c.pk_num_miscelaneo_maestro=d.fk_a005_num_miscelaneo_maestro and a.fk_a006_num_miscelaneo_detalle_sexo=d.pk_num_miscelaneo_detalle and b.fk_rhc063_num_puestos_cargo=e.pk_num_puestos and b.fk_a004_num_dependencia=g.pk_num_dependencia and g.fk_a001_num_organismo=f.pk_num_organismo and b.fk_a001_num_organismo=f.pk_num_organismo and a.ind_estatus_empleado=1"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    // Método que permite listar los temas añadidos al sistema
    public function metListarTema()
    {
        $listarTema = $this->_db->query(
            "select pk_num_tema_evento, num_estatus, ind_tema from ev_c002_tema where num_estatus=1"
        );
        $listarTema->setFetchMode(PDO::FETCH_ASSOC);
        return $listarTema->fetchAll();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if ($metodo == 1) {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia, $usuario, $idAplicacion)
    {
        if ($metodo == 1) {
            $listarDependencia = $this->_db->query(
                "select a.pk_num_dependencia, a.ind_dependencia from a004_dependencia as a, a001_organismo as b, a019_seguridad_dependencia as c  where a.fk_a001_num_organismo=$pkNumOrganismo and a.fk_a001_num_organismo=b.pk_num_organismo and a.pk_num_dependencia=c.fk_a004_num_dependencia and c.fk_a018_num_seguridad_usuario=$usuario and c.fk_a015_num_seguridad_aplicacion=$idAplicacion"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia = $this->_db->query(
                "select a.pk_num_dependencia, a.ind_dependencia from a004_dependencia as a, a019_seguridad_dependencia as b where a.pk_num_dependencia=$pk_num_dependencia and a.pk_num_dependencia=b.fk_a004_num_dependencia and b.fk_a018_num_seguridad_usuario=$usuario and b.fk_a015_num_seguridad_aplicacion=$idAplicacion"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }
    }

    // Método que permite listar empleados
    public function metListarEmpleado()
    {
        $empleado = $this->_db->query(
            "select a.pk_num_persona, a.pk_num_empleado, a.ind_cedula_documento, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from vl_rh_persona_empleado as a, rh_b001_empleado as b, a003_persona as c, vl_rh_persona_empleado_datos_laborales as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and d.pk_num_empleado=a.pk_num_empleado and a.ind_estatus_empleado=1"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método que permite  listar a los empleados de acuerdo al buscador de trabajador al momento de registrar un nuevo evento
    public function metBuscarTrabajador($pkNumOrganismo, $pkNumDependencia, $indCedulaDocumento, $opcionBuscar)
    {
        if ($opcionBuscar == 1) {
            if ($pkNumOrganismo != '') {
                $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
            }
            if ($pkNumDependencia != '') {
                $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
            }
            if ($indCedulaDocumento != '') {
                $filtro .= " and a.ind_cedula_documento=$indCedulaDocumento";
            }
            $buscarTrabajador = $this->_db->query(
                "select a.pk_num_persona, a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2 from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b where a.pk_num_empleado=b.pk_num_empleado $filtro"
            );
        } else {
            if ($indCedulaDocumento != '') {
                $filtro = " where ind_cedula_documento=$indCedulaDocumento";
            } else {
                $filtro = '';
            }
            $buscarTrabajador = $this->_db->query(
                "select pk_num_persona, ind_cedula_documento, ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from a003_persona $filtro"
            );
        }
        $buscarTrabajador->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTrabajador->fetchAll();
    }

    // Método que permite consultar una persona
    public function metBuscarPersona($pkNumPersona)
    {
        $buscarTrabajador = $this->_db->query(
            "select pk_num_persona, ind_cedula_documento, ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from a003_persona where pk_num_persona=$pkNumPersona"
        );
        $buscarTrabajador->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTrabajador->fetch();
    }

    // Método que permite guardar el evento
    public function metGuardarEvento($indNombreEvento, $indDescripcionEvento, $pkNumLugarEvento, $tipoEvento, $fechaInicio, $fechaFin, $horaInicio, $horaTotal, $horaFin, $fondo, $fecha_hora, $usuario, $fechaRegistro, $ponente, $participante, $pkNumEnte)
    {
        if($pkNumEnte == 0)
        {
            $pkNumEnte = NULL;
        }


        $this->_db->beginTransaction();
        $nuevoEvento = $this->_db->prepare(
            "insert into ev_b001_evento values (null, :ind_nombre_evento, :ind_descripcion, :fec_registro, :fec_inicio, :fec_fin, :fec_horas_total, :fec_hora_entrada, :fec_hora_salida, :ind_certificado, :num_flag_certificado_participante, :num_flag_certificado_ponente, :fk_a006_num_tipo_evento, :fk_evc003_num_lugar, :fk_a039_num_ente, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $nuevoEvento->execute(array(
            ':ind_nombre_evento' => $indNombreEvento,
            ':ind_descripcion' => $indDescripcionEvento,
            ':fec_registro' => $fechaRegistro,
            ':fec_inicio' => $fechaInicio,
            ':fec_fin' => $fechaFin,
            ':fec_horas_total' => $horaTotal,
            ':fec_hora_entrada' => $horaInicio,
            ':fec_hora_salida' => $horaFin,
            ':ind_certificado' => $fondo,
            ':num_flag_certificado_participante' => $participante,
            ':num_flag_certificado_ponente' => $ponente,
            ':fk_a006_num_tipo_evento' => $tipoEvento,
            ':fk_evc003_num_lugar' => $pkNumLugarEvento,
            ':fk_a039_num_ente' => $pkNumEnte,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fec_ultima_modificacion' => $fecha_hora
        ));
        $pkNumEvento = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumEvento;
    }

    // Método que permite guardar el tema del evento
    public function metGuardarTema($pkNumEvento, $fecha_hora, $usuario, $pkNumTema)
    {
        $this->_db->beginTransaction();
        $guardarTema = $this->_db->prepare(
            "insert into ev_c004_tema_evento values (null, :fk_evb001_num_evento, :fk_evc002_num_tema, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $guardarTema->execute(array(
            ':fk_evb001_num_evento' => $pkNumEvento,
            ':fk_evc002_num_tema' => $pkNumTema,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fec_ultima_modificacion' => $fecha_hora
        ));
        $this->_db->commit();
    }

    // Método que permite guardar los ponentes y participantes del evento
    public function metGuardarAsistencia($pkNumPersona, $pkNumEvento, $tipoPersona, $usuario, $fecha_hora, $culmino, $recibir, $tipoInstruccion, $firmaPonente)
    {
        $this->_db->beginTransaction();
        $guardarAsistencia = $this->_db->prepare(
            "insert into ev_c001_persona_evento values (null, :fk_a003_num_persona, :num_flag_culmino_evento, :num_flag_recibio_certificado, :fk_evb001_num_evento, :fk_a006_num_persona_capacitacion, :fk_a006_num_tipo_instruccion, :num_firma, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $guardarAsistencia->execute(array(
            ':fk_a003_num_persona' => $pkNumPersona,
            ':num_flag_culmino_evento' => $culmino,
            ':num_flag_recibio_certificado' => $recibir,
            ':fk_evb001_num_evento' => $pkNumEvento,
            ':fk_a006_num_persona_capacitacion' => $tipoPersona,
            ':fk_a006_num_tipo_instruccion' => $tipoInstruccion,
            ':num_firma' => $firmaPonente,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fec_ultima_modificacion' => $fecha_hora
        ));
        $pkNumPo = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumPo;
    }

    // Método que permite consultar persona
    public function metConsultarPersona($indCedulaDocumento)
    {
        $persona = $this->_db->query(
            "select pk_num_persona from a003_persona where ind_cedula_documento=$indCedulaDocumento"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    // Método que permite consultar el tipo de persona
    public function metConsultarTipoPersona($codigo)
    {
        $persona = $this->_db->query(
            "select b.pk_num_miscelaneo_detalle from a005_miscelaneo_maestro as a, a006_miscelaneo_detalle as b where a.pk_num_miscelaneo_maestro=b.fk_a005_num_miscelaneo_maestro and a.cod_maestro='TIPEREV' and b.cod_detalle=$codigo"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    // Método que permite ver el evento
    public function metVerEvento($pkNumEvento)
    {
        $consulta = "select
                         a.pk_num_evento,
                         a.ind_nombre_evento,
                         a.ind_descripcion,
                         a.fec_registro,
                         date_format(a.fec_inicio, '%d/%m/%Y') as fecha_inicio,
                         date_format(a.fec_fin, '%d/%m/%Y') as fecha_fin,
                         date_format(a.fec_registro, '%d/%m/%Y') as fecha_registro,
                         date_format(a.fec_registro, '%Y') as anio_registro,
                         a.fec_horas_total,
                         a.fec_hora_entrada,
                         a.fec_hora_salida,
                         a.ind_certificado,
                         a.num_flag_certificado_participante,
                         a.num_flag_certificado_ponente,
                         a.fk_evc003_num_lugar,
                         a.fk_a006_num_tipo_evento,
                         a.fk_a039_num_ente,
                         b.ind_nombre_detalle,
                         b.cod_detalle,
                         c.ind_lugar_evento
                         from
                         ev_b001_evento as a,
                         a006_miscelaneo_detalle as b,
                         ev_c003_lugar as c
                         where
                         a.pk_num_evento=$pkNumEvento
                         and
                         a.fk_a006_num_tipo_evento=b.pk_num_miscelaneo_detalle
                         and
                         a.fk_evc003_num_lugar=c.pk_num_lugar_evento
                         
             ";

        $evento = $this->_db->query($consulta);
        $evento->setFetchMode(PDO::FETCH_ASSOC);
        $evento = $evento->fetch();


        if($this->metVerificarTieneEnteEvento($pkNumEvento) == 1)//tiene ente
        {

            $consulta="SELECT 
                        a006_miscelaneo_detalle.ind_nombre_detalle as nombre_tipo_ente,
                        a006_miscelaneo_detalle.pk_num_miscelaneo_detalle as pk_tipo_ente,
                        a039_ente.ind_nombre_ente
                        FROM
                        ev_b001_evento 
                        INNER JOIN
                        (( a039_ente INNER JOIN a038_categoria_ente ON a039_ente.fk_a038_num_categoria_ente=a038_categoria_ente.pk_num_categoria_ente)
                                     INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle=a038_categoria_ente.fk_a006_miscdet_tipoente)
                        ON ev_b001_evento.fk_a039_num_ente=a039_ente.pk_num_ente
                        WHERE  ev_b001_evento.pk_num_evento = '$pkNumEvento'";

            $ente = $this->_db->query($consulta);
            $ente->setFetchMode(PDO::FETCH_ASSOC);
            $ente = $ente->fetch();

            $evento["nombre_tipo_ente"]= $ente["nombre_tipo_ente"];
            $evento["pk_tipo_ente"]= $ente["pk_tipo_ente"];
            $evento["ind_nombre_ente"]= $ente["ind_nombre_ente"];


        }

        return $evento;

    }

    // Método que permite ver los temas del evento
    public function metVerTema($pkNumEvento)
    {
        $verTema = $this->_db->query(
            "select b.ind_tema, b.pk_num_tema_evento from ev_c004_tema_evento as a, ev_c002_tema as b where a.fk_evb001_num_evento=$pkNumEvento and a.fk_evc002_num_tema=b.pk_num_tema_evento"
        );
        $verTema->setFetchMode(PDO::FETCH_ASSOC);
        return $verTema->fetchAll();
    }

    // Método que permite ver el listado de ponentes del evento
    public function metVerPersona($tipoPersona, $pkNumEvento, $metodo)
    {
        if($metodo==1){
            $verPersona = $this->_db->query(
                "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, a.fk_a006_num_tipo_instruccion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, b.pk_num_persona from ev_c001_persona_evento as a, a003_persona as b where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento"
            );
        } else {
            $verPersona = $this->_db->query(
            "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, a.fk_a006_num_tipo_instruccion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, c.num_certificado, b.pk_num_persona from ev_c001_persona_evento as a, a003_persona as b, ev_c006_persona_certificado as c where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento and a.fk_a003_num_persona=c.fk_a003_num_persona and c.fk_evb001_num_evento=a.fk_evb001_num_evento"
            );
        }

        $verPersona->setFetchMode(PDO::FETCH_ASSOC);
        return $verPersona->fetchAll();
    }

    // Método que permite ver el listado de ponentes del evento
    public function metVerPonente($tipoPersona, $pkNumEvento, $metodo)
    {
        if($metodo==1){
            $verPonente = $this->_db->query(
                "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, a.fk_a006_num_tipo_instruccion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, c.ind_nombre_detalle, c.pk_num_miscelaneo_detalle, a.num_firma, b.pk_num_persona from ev_c001_persona_evento as a, a003_persona as b, a006_miscelaneo_detalle as c where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento and  a.fk_a006_num_tipo_instruccion=c.pk_num_miscelaneo_detalle"
            );
        } else {
            $verPonente = $this->_db->query(
                "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, c.num_certificado, d.ind_nombre_detalle, d.pk_num_miscelaneo_detalle, a.num_firma, b.pk_num_persona, b.pk_num_persona from ev_c001_persona_evento as a, a003_persona as b, ev_c006_persona_certificado as c, a006_miscelaneo_detalle as d where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento and a.fk_a003_num_persona=c.fk_a003_num_persona and c.fk_evb001_num_evento=a.fk_evb001_num_evento and a.fk_a006_num_tipo_instruccion=d.pk_num_miscelaneo_detalle"
            );
        }

        $verPonente->setFetchMode(PDO::FETCH_ASSOC);
        //ECHO "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, a.fk_a006_num_tipo_instruccion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, c.ind_nombre_detalle, c.pk_num_miscelaneo_detalle from ev_c001_persona_evento as a, a003_persona as b, a006_miscelaneo_detalle as c where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento and  a.fk_a006_num_tipo_instruccion=c.pk_num_miscelaneo_detalle";
        return $verPonente->fetchAll();
    }

    // Método que permite eliminar un evento
    public function metEliminarEvento($pkNumEvento)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ev_b001_evento where pk_num_evento = $pkNumEvento"
        );
        $this->_db->commit();
    }

    // Método que permite obtener los nombres de las imagenes
    public function metListarCertificado()
    {
        $listarCertificado = $this->_db->query(
            "select pk_num_certificado, ind_imagen, num_estatus from ev_c005_certificado where num_estatus=1 order by pk_num_certificado desc"
        );
        $listarCertificado->setFetchMode(PDO::FETCH_ASSOC);
        return $listarCertificado->fetchAll();
    }

    // Método que permite editar un evento
    public function metEditarEvento($indNombreEvento, $indDescripcionEvento, $pkNumLugarEvento, $tipoEvento, $fechaInicio, $fechaFin, $horaInicio, $horaTotal, $horaFin, $fondo, $fecha_hora, $usuario, $ponente, $participante, $pkNumEvento, $pkNumEnte)
    {
        if($pkNumEnte == null)
        {
            $cad = " fk_a039_num_ente= NULL ";
        }
        else
        {
            $cad = " fk_a039_num_ente= '$pkNumEnte' ";
        }
//echo "update ev_b001_evento set ind_nombre_evento='$indNombreEvento', ind_descripcion='$indDescripcionEvento', fec_inicio='$fechaInicio', fec_fin='$fechaFin', fec_horas_total='$horaTotal', fec_hora_entrada='$horaInicio', fec_hora_salida='$horaFin', ind_certificado='$fondo', num_flag_certificado_participante=$participante,num_flag_certificado_ponente=$ponente, fk_a006_num_tipo_evento=$tipoEvento, fk_evc003_num_lugar=$pkNumLugarEvento, fk_a018_num_seguridad_usuario=$usuario, fec_ultima_modificacion='$fecha_hora',".$cad." where pk_num_evento=$pkNumEvento";
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ev_b001_evento set ind_nombre_evento='$indNombreEvento', ind_descripcion='$indDescripcionEvento', fec_inicio='$fechaInicio', fec_fin='$fechaFin', fec_horas_total='$horaTotal', fec_hora_entrada='$horaInicio', fec_hora_salida='$horaFin', ind_certificado='$fondo', num_flag_certificado_participante=$participante,num_flag_certificado_ponente=$ponente, fk_a006_num_tipo_evento=$tipoEvento, fk_evc003_num_lugar=$pkNumLugarEvento, fk_a018_num_seguridad_usuario=$usuario, fec_ultima_modificacion='$fecha_hora',".$cad." where pk_num_evento=$pkNumEvento"
        );
        $this->_db->commit();
    }

    // Método que permite eliminar un tema
    public function metEliminarTema($pkNumEvento)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ev_c004_tema_evento where fk_evb001_num_evento=$pkNumEvento"
        );
        $this->_db->commit();
    }

    // Método que permite eliminar a las personas asistentes a un evento
    public function metEliminarPersona($pkNumEvento)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ev_c001_persona_evento where fk_evb001_num_evento=$pkNumEvento"
        );
        $this->_db->commit();
    }

    // Método que permite obtener los nombres de los certificados
    public function metObtenerCertificado($pkNumEvento, $pkNumPersona, $metodo)
    {
        $obtenerCertificado = $this->_db->query(
            "select num_certificado from ev_c006_persona_certificado where fk_a003_num_persona=$pkNumPersona and fk_evb001_num_evento=$pkNumEvento"
        );
        $obtenerCertificado->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $obtenerCertificado->fetchAll();
        } else {
            return $obtenerCertificado->fetch();
        }
    }

    // Método que permite obtener los nombres de los certificados
    public function metConsultarUltimoCertificado()
    {
        $ultimoCertificado = $this->_db->query(
            "select MAX(num_certificado) as maximo from ev_c006_persona_certificado"
        );
        $ultimoCertificado->setFetchMode(PDO::FETCH_ASSOC);
        return $ultimoCertificado->fetch();
    }

    // Método que permite asociar una persona a su certificado
    public function metGuardarPersonaCertificado($pkNumEvento, $pkNumPersona, $proximo, $fecha_hora, $usuario)
    {
        $this->_db->beginTransaction();
        $guardarAsistencia = $this->_db->prepare(
            "insert into ev_c006_persona_certificado values (null, :num_certificado, :fk_a003_num_persona, :fk_evb001_num_evento, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $guardarAsistencia->execute(array(
            ':num_certificado' => $proximo,
            ':fk_a003_num_persona' => $pkNumPersona,
            ':fk_evb001_num_evento' => $pkNumEvento,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fec_ultima_modificacion' => $fecha_hora
        ));
        $this->_db->commit();
    }

    // Método que permite ver el listado de personas que culminaron el evento
    public function metListarPersona($pkNumEvento, $tipoPersona)
    {
        $verPersona = $this->_db->query(
               "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, a.num_firma, 	b.ind_firma_png from ev_c001_persona_evento as a, a003_persona as b where a.fk_a003_num_persona=b.pk_num_persona and a.fk_evb001_num_evento=$pkNumEvento and a.num_flag_culmino_evento=1 and a.fk_a006_num_persona_capacitacion=$tipoPersona"
            );
        $verPersona->setFetchMode(PDO::FETCH_ASSOC);
        return $verPersona->fetchAll();
    }

    // Método que permite obtener los datos laborales del contralor del estado
    public function metConsultarContralor()
    {
        $verContralor = $this->_db->query(
            "select a.pk_num_empleado, a.ind_nombre1, a.ind_apellido1 from vl_rh_persona_empleado_datos_laborales as a, rh_c063_puestos as b where a.fk_rhc063_num_puestos_cargo=b.pk_num_puestos and b.ind_descripcion_cargo LIKE '%CONTRALOR%' and b.num_estatus=1"
        );
        $verContralor->setFetchMode(PDO::FETCH_ASSOC);
        return $verContralor->fetch();
    }

    //Buscar firma contralor
    public function metConsultarFirmaContralor($pk_num_empleado)
    {
        $persona = $this->_db->query(
            "select 
                    ind_firma_png 
                    from a003_persona 
                    inner join rh_b001_empleado 
                    on a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                    where pk_num_empleado =$pk_num_empleado"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metConsultarInfoDetalleOrganismo($pk_num_organismodet)
    {

        $persona = $this->_db->query(
            "SELECT 
                      a.pk_num_organismodet, 
                      a.ind_pagina_web, 
                      a.ind_cargo_representante, 
                      b.ind_ciudad 
                      FROM 
                      a002_organismo_detalle AS a 
                      INNER JOIN a010_ciudad AS b 
                      ON a.fk_a010_num_ciudad = b.pk_num_ciudad 
                      WHERE a.pk_num_organismodet = $pk_num_organismodet"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metInstruccionContralor($num_contralor)
    {
        $persona = $this->_db->query(
            "SELECT 
                      b.ind_nombre_nivel_grado 
                      FROM 
                      `rh_c018_empleado_instruccion` AS a 
                      INNER JOIN 
                      rh_c064_nivel_grado_instruccion AS b
                      ON a.fk_rhc064_num_nivel_grado = b.pk_num_nivel_grado 
                      WHERE a.fk_rhb001_num_empleado = $num_contralor"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    // Método que permite obtener los datos de los ponentes
    public function metConsultarPonentes($pkNumEvento, $tipoPersona)
    {
        $verContralor = $this->_db->query(
            "select
                a.fk_a003_num_persona,
                a.num_flag_culmino_evento,
                a.num_flag_recibio_certificado,
                a.fk_a006_num_tipo_instruccion,
                b.ind_nombre1,
                b.ind_nombre2,
                b.ind_apellido1,
                b.ind_apellido2,
                b.ind_cedula_documento,
                c.ind_nombre_detalle,
                c.pk_num_miscelaneo_detalle,
                c.cod_detalle
                from
                ev_c001_persona_evento as a,
                a003_persona as b,
                a006_miscelaneo_detalle as c
                where
                a.fk_a003_num_persona=b.pk_num_persona
                and a.fk_a006_num_persona_capacitacion=$tipoPersona
                and a.fk_evb001_num_evento=$pkNumEvento
                and  a.fk_a006_num_tipo_instruccion=c.pk_num_miscelaneo_detalle limit 2 
                "
        );
        $verContralor->setFetchMode(PDO::FETCH_ASSOC);
        return $verContralor->fetchAll();
    }

    public function metConsultarPonentesFirmantesCert($pkNumEvento, $tipoPersona)
    {
        $verContralor = $this->_db->query(
            "select
                a.fk_a003_num_persona,
                a.num_flag_culmino_evento,
                a.num_flag_recibio_certificado,
                a.fk_a006_num_tipo_instruccion,
                b.ind_nombre1,
                b.ind_nombre2,
                b.ind_apellido1,
                b.ind_apellido2,
                b.ind_cedula_documento,
                c.ind_nombre_detalle,
                c.pk_num_miscelaneo_detalle,
                c.cod_detalle,
                a.num_firma,
                b.ind_firma_png
                from
                ev_c001_persona_evento as a,
                a003_persona as b,
                a006_miscelaneo_detalle as c
                where
                a.fk_a003_num_persona=b.pk_num_persona
                and a.fk_a006_num_persona_capacitacion=$tipoPersona
                and a.fk_evb001_num_evento=$pkNumEvento
                and  a.fk_a006_num_tipo_instruccion=c.pk_num_miscelaneo_detalle 
                and a.num_firma > 0 
                and a.num_flag_culmino_evento=1
                ORDER BY a.num_firma DESC limit 3 
                "
        );
        $verContralor->setFetchMode(PDO::FETCH_ASSOC);
        return $verContralor->fetchAll();
    }

    // Método que permite obtener el sexo de una persona
    public function metObtenerSexo($pkNumPersona)
    {
        $consultarSexo = $this->_db->query(
            "select b.cod_detalle from a003_persona as a, a006_miscelaneo_detalle as b where a.pk_num_persona=$pkNumPersona and a.fk_a006_num_miscelaneo_detalle_sexo=b.pk_num_miscelaneo_detalle"
        );
        $consultarSexo->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarSexo->fetch();
    }

    // Método que permite obtener el sexo de una persona
    public function metObtenerPersona($cedulaDocumento)
    {
        $persona = $this->_db->query(
            "select pk_num_persona from a003_persona where ind_cedula_documento='$cedulaDocumento'"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    // Método que permite listar los tipos de ente
    public function metListarTipoEnte()
    {
        $tipoEnte = $this->_db->query(
            "SELECT pk_num_miscelaneo_detalle, cod_detalle , ind_nombre_detalle FROM a006_miscelaneo_detalle INNER JOIN a005_miscelaneo_maestro ON a005_miscelaneo_maestro.pk_num_miscelaneo_maestro = a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro WHERE a005_miscelaneo_maestro.cod_maestro = 'PFTIPOENTE'"
        );
        $tipoEnte->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoEnte->fetchAll();
    }

    // Método que permite buscar un tema
    public function metConsultarTema($pkNumTemaEvento)
    {
        $tema = $this->_db->query(
            "select pk_num_tema_evento, ind_tema from ev_c002_tema where pk_num_tema_evento=$pkNumTemaEvento"
        );
        $tema->setFetchMode(PDO::FETCH_ASSOC);
        return  $tema->fetch();
    }

    // Método que permite consultar un lugar
    public function metConsultarLugar($pkNumLugarEvento)
    {
        $consultarLugar = $this->_db->query(
            "select pk_num_lugar_evento, num_estatus, ind_lugar_evento from ev_c003_lugar where pk_num_lugar_evento=$pkNumLugarEvento"
        );
        $consultarLugar->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarLugar->fetch();
    }

    // Método que permite consultar la cantidad de participantes o ponentes de un evento en particular
    public function metBuscarTipoPersona($codigo, $pkNumEvento)
    {
        $persona = $this->_db->query(
            "select a.pk_num_persona_evento from ev_c001_persona_evento as a, a005_miscelaneo_maestro as b, a006_miscelaneo_detalle as c where a.fk_evb001_num_evento=$pkNumEvento and a.fk_a006_num_persona_capacitacion=c.pk_num_miscelaneo_detalle and  b.pk_num_miscelaneo_maestro=c.fk_a005_num_miscelaneo_maestro and b.cod_maestro='TIPEREV' and c.cod_detalle=$codigo"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }

    // Metodo que permite obtener la direccion de correo electronico del personal
    public function metObtenerCorreoPersona($pkNumPersona)
    {
        $persona = $this->_db->query(
            "select ind_email from a003_persona where pk_num_persona=$pkNumPersona"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }


    // Método que permite ver el listado de ponentes del evento
    public function metListarPersonaCertificado($pkNumEvento, $tipoPersona, $pkNumPersona)
    {
        $verPersona = $this->_db->query(
            "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, b.ind_email  from ev_c001_persona_evento as a, a003_persona as b where a.fk_a003_num_persona=b.pk_num_persona and a.fk_evb001_num_evento=$pkNumEvento and a.num_flag_culmino_evento=1 and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_a003_num_persona=$pkNumPersona"
        );
        $verPersona->setFetchMode(PDO::FETCH_ASSOC);
        return $verPersona->fetch();
    }

    // Método que permite listar los entes dependiendo del tipo
    public function metBuscarEntePadre($pk_num_miscelaneo_detalle)
    {
        $listarPadre = $this->_db->query(
            "SELECT 
                       pk_num_ente,
                       ind_nombre_ente,
                       a006_miscelaneo_detalle.ind_nombre_detalle,
                       a038_categoria_ente.ind_categoria_ente
                       FROM `a039_ente` 
                       inner join a038_categoria_ente
                       on (a039_ente.fk_a038_num_categoria_ente=a038_categoria_ente.pk_num_categoria_ente)
                       INNER join a006_miscelaneo_detalle
                       on (a038_categoria_ente.fk_a006_miscdet_tipoente=a006_miscelaneo_detalle.pk_num_miscelaneo_detalle) 
                       where a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = '$pk_num_miscelaneo_detalle' 
                       ORDER BY a039_ente.pk_num_ente"
            //select pk_num_ente, ind_nombre_ente from a039_ente where fk_a037_num_tipo_ente=$pkNumTipoEnte and num_ente_padre=
        );
        $listarPadre->setFetchMode(PDO::FETCH_ASSOC);
        return $listarPadre->fetchAll();
    }

    // Método que permite listar los entes dependiendo del tipo
    public function metConsultarEnte()
    {
        $listarPadre = $this->_db->query(
            "select pk_num_ente, ind_nombre_ente from a039_ente"
        );
        $listarPadre->setFetchMode(PDO::FETCH_ASSOC);
        return $listarPadre->fetchAll();
    }

    public function metBuscarCargo($persona)
    {
        $cargoEmpleado = $this->_db->query("
          	SELECT
                cargo.ind_nombre_cargo
            FROM
              rh_b001_empleado AS empleado
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
              INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
              INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
            WHERE
                persona.pk_num_persona = '$persona'
          ");
        $cargoEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $cargoEmpleado->fetch();
    }

    public function metRegistratFirmaPng($pk_persona, $nombre_firma)
    {
        $this->_db->beginTransaction();
        $modificacion =  $this->_db->query("
        UPDATE `a003_persona`
        SET
        `ind_firma_png`= '$nombre_firma'
        WHERE pk_num_persona = '$pk_persona' ");

        $this->_db->commit();

        return $modificacion;
    }

    public function metBuscarFirmaImpresa($pk_persona)
    {
        $firma = $this->_db->query("
          	SELECT
          	`ind_firma_png`
          	FROM `a003_persona`
            WHERE pk_num_persona = '$pk_persona'
          ");
        $firma->setFetchMode(PDO::FETCH_ASSOC);
        return $firma->fetch();
    }

    public function metVerificarTieneEnteEvento($pkNumEvento)
    {
        $ente = $this->_db->query("
          	SELECT
          	`fk_a039_num_ente`
          	FROM `ev_b001_evento`
            WHERE pk_num_evento = '$pkNumEvento'
          ");
        $ente->setFetchMode(PDO::FETCH_ASSOC);
        $resp = $ente->fetch();

        if(!$resp["fk_a039_num_ente"])
        {
            return 0;//no tiene ente
        }
        else
        {
            return 1;
        }
    }

}
?>