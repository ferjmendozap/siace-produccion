<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Reporte de Eventos.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class reporteEventoModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite obtener los años de los cuales ha habido eventos
    public function metConsultarAnio()
    {
        $consultarAnio = $this->_db->query(
            "select date_format(fec_inicio, '%Y') as anio from ev_b001_evento group by anio order by anio desc"
        );
        $consultarAnio->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarAnio->fetchAll();
    }

    // Método que permite listar los Eventos añadidos al sistema
    public function metListadoEvento()
    {
        $listarEvento = $this->_db->query(
            "select a.pk_num_evento, a.ind_nombre_evento, date_format(a.fec_inicio, '%d-%m-%Y') as fecha_inicio, date_format(a.fec_fin, '%d-%m-%Y') as fecha_fin, a.fec_hora_entrada, b.ind_lugar_evento from ev_b001_evento as a, ev_c003_lugar as b where a.fk_evc003_num_lugar=b.pk_num_lugar_evento"
        );
        $listarEvento->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEvento->fetchAll();
    }

    // Método que permite listar los Eventos añadidos al sistema
    public function metListarEvento($mesEvento, $anioEvento, $tipo)
    {
        if($tipo==1){
            $listarEvento = $this->_db->query(
                "select a.pk_num_evento, a.ind_nombre_evento, date_format(a.fec_inicio, '%d-%m-%Y') as fecha_inicio, date_format(a.fec_fin, '%d-%m-%Y') as fecha_fin, a.fec_hora_entrada, b.ind_lugar_evento from ev_b001_evento as a, ev_c003_lugar as b where a.fk_evc003_num_lugar=b.pk_num_lugar_evento and MONTH(a.fec_inicio)='$mesEvento' and YEAR(a.fec_inicio)=$anioEvento"
            );
        }
        if($tipo==2){
            if($mesEvento==1){
                $valor1 = '01';
                $valor2 = '03';
            } else if($mesEvento==2){
                $valor1 = '04';
                $valor2 = '06';
            } else if($mesEvento==3){
                $valor1 = '07';
                $valor2 = '09';
            } else {
                $valor1 = '10';
                $valor2 = '12';
            }
            $listarEvento = $this->_db->query(
                "select a.pk_num_evento, a.ind_nombre_evento, date_format(a.fec_inicio, '%d-%m-%Y') as fecha_inicio, date_format(a.fec_fin, '%d-%m-%Y') as fecha_fin, a.fec_hora_entrada, b.ind_lugar_evento from ev_b001_evento as a, ev_c003_lugar as b where a.fk_evc003_num_lugar=b.pk_num_lugar_evento and MONTH(a.fec_inicio) between $valor1 and $valor2 and YEAR(a.fec_inicio)=$anioEvento"
            );
        }
        if($tipo==3){
            if($mesEvento==1){
                $valor1 = '01';
                $valor2 = '06';
            } else {
                $valor1 = '07';
                $valor2 = '12';
            }
            $listarEvento = $this->_db->query(
                "select a.pk_num_evento, a.ind_nombre_evento, date_format(a.fec_inicio, '%d-%m-%Y') as fecha_inicio, date_format(a.fec_fin, '%d-%m-%Y') as fecha_fin, a.fec_hora_entrada, b.ind_lugar_evento from ev_b001_evento as a, ev_c003_lugar as b where a.fk_evc003_num_lugar=b.pk_num_lugar_evento and MONTH(a.fec_inicio) between $valor1 and $valor2 and YEAR(a.fec_inicio)=$anioEvento"
            );
        }
        $listarEvento->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEvento->fetchAll();
    }

    // Método que permite obtener la cantidad de participantes de un evento
    public function metObtenerParticipante($pkNumEvento, $tipoPersona)
    {
        $obtenerParticipante = $this->_db->query(
            "select count(pk_num_persona_evento) as total from ev_c001_persona_evento where fk_evb001_num_evento=$pkNumEvento and fk_a006_num_persona_capacitacion=$tipoPersona"
        );
        $obtenerParticipante->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerParticipante->fetch();
    }

    // Método que permite consultar el tipo de persona
    public function metConsultarTipoPersona($codigo)
    {
        $persona = $this->_db->query(
            "select b.pk_num_miscelaneo_detalle from a005_miscelaneo_maestro as a, a006_miscelaneo_detalle as b where a.	pk_num_miscelaneo_maestro=b.fk_a005_num_miscelaneo_maestro and a.cod_maestro='TIPEREV' and b.cod_detalle=$codigo"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    // Método que permite obtener los datos laborales del contralor del estado
    public function metConsultarPonentes($pkNumEvento, $tipoPersona)
    {
        $verContralor = $this->_db->query(
            "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, a.fk_a006_num_tipo_instruccion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, c.ind_nombre_detalle, c.pk_num_miscelaneo_detalle, c.cod_detalle from ev_c001_persona_evento as a, a003_persona as b, a006_miscelaneo_detalle as c where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento and  a.fk_a006_num_tipo_instruccion=c.pk_num_miscelaneo_detalle limit 2"
        );
        $verContralor->setFetchMode(PDO::FETCH_ASSOC);
        return $verContralor->fetchAll();
    }

    // Método que permite ver el evento
    public function metVerEvento($pkNumEvento)
    {
        $evento = $this->_db->query(
            "select a.pk_num_evento, a.ind_nombre_evento, date_format(a.fec_inicio, '%d/%m/%Y') as fecha_inicio, date_format(a.fec_fin, '%d/%m/%Y') as fecha_fin, b.ind_lugar_evento from ev_b001_evento as a, ev_c003_lugar as b where a.pk_num_evento=$pkNumEvento and a.fk_evc003_num_lugar=b.pk_num_lugar_evento"
        );
        $evento->setFetchMode(PDO::FETCH_ASSOC);
        return $evento->fetch();
    }

    // Método que permite ver el listado de ponentes del evento
    public function metVerPersona($tipoPersona, $pkNumEvento, $metodo)
    {
        if($metodo==1){
            $verPersona = $this->_db->query(
                "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, a.fk_a006_num_tipo_instruccion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento from ev_c001_persona_evento as a, a003_persona as b where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento"
            );
        } else {
            $verPersona = $this->_db->query(
                "select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, a.fk_a006_num_tipo_instruccion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, c.num_certificado from ev_c001_persona_evento as a, a003_persona as b, ev_c006_persona_certificado as c where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento and a.fk_a003_num_persona=c.fk_a003_num_persona and c.fk_evb001_num_evento=a.fk_evb001_num_evento"
            );
        }

        $verPersona->setFetchMode(PDO::FETCH_ASSOC);
        return $verPersona->fetchAll();
    }

    // Método que permite obtener los nombres de los certificados
    public function metObtenerCertificado($pkNumEvento, $pkNumPersona, $metodo)
    {
        $obtenerCertificado = $this->_db->query(
            "select num_certificado from ev_c006_persona_certificado where fk_a003_num_persona=$pkNumPersona and fk_evb001_num_evento=$pkNumEvento"
        );
        $obtenerCertificado->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $obtenerCertificado->fetchAll();
        } else {
            return $obtenerCertificado->fetch();
        }

    }

    // Método que permite listar los eventos de un año en específico
    public function metConsultarEvento($tipoEventoPrevio, $anioTipoEvento)
    {
        $consultarEvento = $this->_db->query(
            "select a.pk_num_evento, a.ind_nombre_evento, date_format(a.fec_inicio, '%d-%m-%Y') as fecha_inicio, date_format(a.fec_fin, '%d-%m-%Y') as fecha_fin, b.ind_lugar_evento from ev_b001_evento as a, ev_c003_lugar as b where YEAR(a.fec_inicio) = $anioTipoEvento and a.fk_a006_num_tipo_evento=$tipoEventoPrevio and a.fk_evc003_num_lugar=b.pk_num_lugar_evento"
        );
        $consultarEvento->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarEvento->fetchAll();
    }

    // Método que permite listar los eventos de un año en específico
    public function metConsultarTipoEvento($tipoEventoPrevio)
    {
        $consultarEvento = $this->_db->query(
            "select ind_nombre_detalle from a006_miscelaneo_detalle where pk_num_miscelaneo_detalle=$tipoEventoPrevio"
        );
        $consultarEvento->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarEvento->fetch();
    }

    // Metodo que permite listar los meses de los eventos
    public function metListarMes($anioEvento)
    {
        $consultarEvento = $this->_db->query(
            "select date_format(fec_inicio, '%m') as mes from ev_b001_evento where date_format(fec_inicio, '%Y')=$anioEvento group by mes order by mes desc"
        );
        $consultarEvento->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarEvento->fetchAll();
    }

    // Método que permite listar eventos por lugares
    public function metBuscarEventos($pkNumLugarEvento, $anioTipoEvento, $mes)
    {
        $buscarEventos = $this->_db->query(
            "select a.pk_num_evento, a.fec_horas_total, a.ind_nombre_evento, date_format(a.fec_inicio, '%d/%m/%Y') as fecha_inicio, date_format(a.fec_fin, '%d/%m/%Y') as fecha_fin, b.ind_lugar_evento, c.ind_nombre_detalle from ev_b001_evento as a, ev_c003_lugar as b, a006_miscelaneo_detalle as c where a.fk_evc003_num_lugar=b.pk_num_lugar_evento and b.pk_num_lugar_evento=$pkNumLugarEvento and date_format(a.fec_inicio, '%m')=$mes and date_format(a.fec_inicio, '%Y')=$anioTipoEvento and a.fk_a006_num_tipo_evento=c.pk_num_miscelaneo_detalle"
        );
        $buscarEventos->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarEventos->fetchAll();
    }

    // Método que permite listar los tipos de ente
    public function metListarEnte()
    {
        $tipoEnte = $this->_db->query(
            "select pk_num_ente, ind_nombre_ente from a039_ente"
        );
        $tipoEnte->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoEnte->fetchAll();
    }

    // Método que permite listar eventos por lugares
    public function metBuscarEventosEnte($pkNumEnte, $anioEnte, $mesEnte)
    {
        $buscarEventos = $this->_db->query(
            "select a.pk_num_evento, a.fec_horas_total, a.ind_nombre_evento, date_format(a.fec_inicio, '%d/%m/%Y') as fecha_inicio, date_format(a.fec_fin, '%d/%m/%Y') as fecha_fin, b.ind_lugar_evento, c.ind_nombre_detalle, d.ind_nombre_ente from ev_b001_evento as a, ev_c003_lugar as b, a006_miscelaneo_detalle as c, a039_ente as d where a.fk_evc003_num_lugar=b.pk_num_lugar_evento and date_format(a.fec_inicio, '%m')=$mesEnte and date_format(a.fec_inicio, '%Y')=$anioEnte and a.fk_a006_num_tipo_evento=c.pk_num_miscelaneo_detalle and a.fk_a039_num_ente=d.pk_num_ente and d.pk_num_ente=$pkNumEnte"
        );
        $buscarEventos->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarEventos->fetchAll();
    }

    //Metodo que permite traer datos del organismo para las cabeceras de reportes
    public function metDetallesCabeceraOrganismo($id_organismo_defecto)
    {    
        $buscarOrganismo = $this->_db->query(
            "SELECT pk_num_organismo, ind_descripcion_empresa, ind_logo, ind_logo_secundario,ind_direccion FROM `a001_organismo` WHERE pk_num_organismo = $id_organismo_defecto"    
        );
        $buscarOrganismo->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrganismo->fetch();
        
    }
    
}// fin de la clase
?>