<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
	<form id="formAjax" action="{$_Parametros.url}modEV/gestionEventoCONTROL/EnviarCertificadoMET" class="form floating-label form-validation" role="form" novalidate>
		<input type="hidden" name="pk_num_evento" id="pk_num_evento" value="{$evento.pk_num_evento}" />
		<input type="hidden" name="flag_certificado_participante" id="flag_certificado_participante" value="{$evento.num_flag_certificado_participante}" />
		<input type="hidden" name="flag_certificado_ponente" id="flag_certificado_ponente" value="{$evento.num_flag_certificado_ponente}" />
		<div class="form-wizard-nav">
			<div class="progress"><div class="progress-bar progress-bar-primary"></div>
			</div>
			<ul class="nav nav-justified">
				<li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">Evento</span></a></li>
				<li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Ponentes y Participantes</span></a></li>
				<li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Certificado</span></a></li>
			</ul>
		</div><!--end .form-wizard-nav -->
		<!-- *********** PASO 1 ******************-->
		<div class="tab-content clearfix">
			<br/>
			<div class="tab-pane active" id="step1">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Datos del Evento</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<textarea class="form-control" rows="2" disabled>{$evento.ind_nombre_evento}</textarea>
								<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Nombre del Evento</label>
							</div>
							<div class="form-group">
								<textarea class="form-control" rows="2">{$evento.ind_descripcion}</textarea>
								<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Descripción del Evento</label>
							</div>
							<div class="form-group floating-label">
								<select class="form-control dirty">
									<option selected>{$evento.ind_lugar_evento}</option>
								</select>
								<label for=""><i class="glyphicon glyphicon-home"></i> Lugar</label>
							</div>
							<div class="form-group floating-label">
								<select class="form-control dirty">
                                        {foreach item=ente from=$tipoEnte}
                                         	{if $ente.pk_num_miscelaneo_detalle==$evento.pk_tipo_ente}
												<option selected value="{$ente.pk_num_miscelaneo_detalle}">{$ente.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
								</select>
								<label for="tipo_evento"><i class="glyphicon glyphicon-triangle-right"></i>Tipo de Ente</label>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="form-group floating-label">
								<select class="form-control dirty">
									<option selected>{$evento.ind_nombre_detalle}</option>
								</select>
								<label for=""><i class="glyphicon glyphicon-home"></i> Tipo de Evento</label>
							</div>
							<div class="form-group" style="width:100%">
								<div class="input-daterange input-group" id="demo-date-range">
									<div class="input-group-content">
										<input type="text" class="datepicker form-control" value="{$evento.fecha_inicio}" disabled>
										<label><i class="glyphicon glyphicon-calendar"></i> Fecha del Evento</label>
									</div>
									<span class="input-group-addon">al</span>
									<div class="input-group-content">
										<input type="text" class="datepicker form-control" value="{$evento.fecha_fin}" disabled>
										<div class="form-control-line"></div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="col-md-6 col-sm-6">
									<div class="col-md- col-sm-6">
										<div class="form-group">
											<input type="text" value="{$datoHora.horaInicio|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$datoHora.minutoInicio|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}" class="form-control dirty" data-rule-minlength="5" maxlength="5" disabled>
											<label class="control-label"><i class="glyphicon glyphicon-time"></i> Hora Entrada</label>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group floating-label">
											<select name="horario_inicio" class="form-control" id="horario_inicio">
												<option disabled selected>{$datoHora.horarioInicio}</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											<input type="text" value="{$datoHora.horaFin|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$datoHora.minutoFin|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}" class="form-control dirty" data-rule-minlength="5" maxlength="5" id="hora_final" disabled>
											<label class="control-label">Hora Salida</label>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group floating-label">
											<select name="horario_fin" class="form-control" id="horario_fin">
												<option disabled selected>{$datoHora.horarioFin}</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-4">
									<div class="form-group">
										<input type="text" class="form-control dirty" value="{$evento.fec_horas_total}">
										<label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Total de Horas</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="form-group floating-label">
								<select class="form-control dirty">
									<option selected value="0">{$evento.ind_nombre_ente}</option>
								</select>
								<label for="tipo_evento"><i class="glyphicon glyphicon-triangle-right"></i>Ente</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Tema del Evento</h3>
						</div>
						<div class="panel-body">
							{assign var="numero" value="1"}
							<div class="table-responsive">
								<table class="table no-margin">
									{foreach item=tema from=$verTema}
										<tr>
											<td>{$numero++}</td><td>{$tema.ind_tema}</td>
										</tr>
									{/foreach}
								</table>
							</div>
						</div>
					</div>
				</div>
			</div><!--end #step1 -->
			<!-- *********** PASO 2 ******************PARTICIPANTES Y PONENTES-->
			<div class="tab-pane" id="step2">
				<div class="col-sm-12">
					<div class="col-sm-12">
						<h3 class="text-primary">Ponentes</h3>
						{assign var="numeroPonente" value="1"}
						<table id="datatable2" class="table table-striped table-hover">
							<thead>
							<tr align="center">
								<th width="30">Seleccionar</th>
								<th> N°</th>
								<th> Nombre y Apellido</th>
								<th> Nivel de Instrucción</th>
								<th width="60"> Culminó</th>
								<th width="60"> Recibió Certificado</th>
								<th width="60"> N° de Certificado</th>
							</tr>
							</thead>
							<tbody>
								{foreach item=ponente from=$cargarPonente}
									<tr>
										<td width="30">
											<div class="checkbox checkbox-styled">
												<label>
                                                    {if $evento.num_flag_certificado_ponente>0}
													<input name="ponente[]" type="checkbox" value="{$ponente.fk_a003_num_persona}">
													{else}
													<input name="" type="checkbox" value="" disabled>
													{/if}
												</label>
											</div>
										</td>
										<td>{$numeroPonente++}</td>
										<td>{$ponente.ind_nombre1} {$ponente.ind_nombre2} {$ponente.ind_apellido1} {$ponente.ind_apellido2}</td>
										<td>{$ponente.ind_nombre_detalle}</td>
										<td width="60">{if $ponente.num_flag_culmino_evento>0}Si{/if}</td>
										<td width="60">{if $ponente.num_flag_recibio_certificado>0}Si{/if}</td>
										<td width="60">
											{if $evento.num_flag_certificado_ponente>0}
												{if $ponente.num_flag_culmino_evento>0}{$ponente.num_certificado}{/if}
											{/if}
										</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
						<em class="text-caption" >Solo se emiten certificados a los ponentes que culminen el evento.</em>
					</div>
					<div class="col-sm-12">
						<h3 class="text-primary">Participantes</h3>
						{assign var="numeroParticipante" value="1"}
						<table id="datatable3" class="table table-striped table-hover">
							<thead>
							<tr align="center">
								<th width="30">Seleccionar</th>
								<th> N°</th>
								<th> Nombre y Apellido</th>
								<th width="60"> Culminó</th>
								<th width="60"> Recibió Certificado</th>
								<th width="60"> N° de Certificado</th>
							</tr>
							</thead>
							<tbody>
							{foreach item=participante from=$cargarParticipante}
								<tr>
									<td width="30">
										<div class="checkbox checkbox-styled">
											<label>
                                                {if $evento.num_flag_certificado_participante>0}
												<input name="participante[]" type="checkbox" value="{$participante.fk_a003_num_persona}">
												{else}
												<input name="" type="checkbox" value="" disabled>
												{/if}
											</label>
										</div>
									</td>
									<td>{$numeroParticipante++}</td>
									<td>{$participante.ind_nombre1} {$participante.ind_nombre2} {$participante.ind_apellido1} {$participante.ind_apellido2}</td>
									<td width="60">{if $participante.num_flag_culmino_evento>0}Si{/if}</td>
									<td width="60">{if $participante.num_flag_recibio_certificado>0}Si{/if}</td>
									<td width="60">
										{if $evento.num_flag_certificado_participante>0}
											{if $participante.num_flag_culmino_evento>0}{$participante.num_certificado}{/if}

										{/if}
									</td>
								</tr>
							{/foreach}
							</tbody>
						</table>
						<div class="col-sm-12" align="center">
							{if $evento.num_flag_certificado_ponente < 1 && $evento.num_flag_certificado_participante < 1 }
								<button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion" disabled><i class="glyphicon glyphicon-envelope"></i>&nbsp;Enviar Certificado(s)</button>
							{else}
								<button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-envelope"></i>&nbsp;Enviar Certificado(s)</button>
							{/if}
						</div>
					</div>
				</div>
			</div><!--end #step2 -->
			<!-- *********** PASO 3 ******************CERTIFICADOS-->
			<div class="tab-pane" id="step3">
				<h3 class="text-primary">Certificados</h3>
				<div class="well clearfix">
					<div class="col-md-12 col-sm-12">
						<div class="col-md-2 col-sm-2">
							<div class="checkbox checkbox-styled">
								<label>
									{if $evento['num_flag_certificado_participante']>0}
										<input disabled type="checkbox" checked>
									{else}
										<input disabled type="checkbox">
									{/if}
									<span>Participantes</span>
								</label>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="checkbox checkbox-styled">
								<label>
									{if $evento['num_flag_certificado_ponente']>0}
										<input disabled type="checkbox" checked>
									{else}
										<input disabled type="checkbox">
									{/if}
									<span>Ponentes</span>
								</label>
							</div>
						</div>
						<br/>
						<div class="col-md-12 col-sm-12">
							<br/>
							{if $evento['num_flag_certificado_ponente']>0||$evento['num_flag_certificado_participante']}
								<div align="center">
									<iframe src="{$_Parametros.url}modEV/gestionEventoCONTROL/GenerarCertificadoMET/?pk_num_evento={$evento.pk_num_evento}" width="100%" height="950"></iframe>
								</div>
							{else}
								<h3>No se agregó certificado a este evento</h3>
							{/if}
						</div>
					</div>
				</div>
				<div align="center">
					<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>&nbsp;&nbsp;
				</div>
			</div><!--end #step3 -->
		</div><!--end .tab-content -->
	</form>
	<ul class="pager wizard">
		<li class="previous first"><a class="btn-raised" href="javascript:void(0);"> Primero </a></li>
		<li class="previous"><a class="btn-raised" href="javascript:void(0);"> Anterior </a></li>
		<li class="next last"><a class="btn-raised" href="javascript:void(0);"> Final </a></li>
		<li class="next"><a class="btn-raised" href="javascript:void(0);"> Siguiente </a></li>
	</ul>
</div><!--end #rootwizard -->
<script type="text/javascript">
	/*$(".form-wizard-nav").on('click', function(){ //enables click event
		return false;
	});*/

	$(document).ready(function() {
		$('#datatable2').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)"
			}
		} );
	} );


	if ( $.fn.dataTable.isDataTable( '#datatable2' ) ) {
		table = $('#datatable2').DataTable();
	}
	else {
		table = $('#datatable2').DataTable( {
			paging: true
		} );
	}

	$(document).ready(function() {
		$('#datatable3').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)"
			}
		} );
	} );

	if ( $.fn.dataTable.isDataTable( '#datatable3' ) ) {
		table = $('#datatable3').DataTable();
	}
	else {
		table = $('#datatable3').DataTable( {
			paging: true
		} );
	}

    $('#accion').click(function(){

        var fondo_evento = "{$evento['ind_certificado']}";

        if(fondo_evento !='' )
        {
            var entrega_participantes = "{$evento['num_flag_certificado_participante']}";
            var entrega_ponentes = "{$evento['num_flag_certificado_ponente']}";
            if(entrega_participantes > 0 || entrega_ponentes > 0)
            {
                swal({
                    title: '¿Desea enviar los certificados?',
                    text: 'Cada certificado se enviará al email respectivo de la persona que vaya a recibir certificado y que haya culminado el evento',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Sí, Enviar',
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                }, function () {

                    swal({
                        title: "¡Por favor espere!",
                        text: "Se esta procesando su solicitud, puede demorar un poco.",
                        timer: 50000000,
                        showConfirmButton: false
                    });
                    $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                        swal("Certificado(s) Enviado(s)", "Envio realizado satisfactoriamente", "success");
                    }, 'json');
                });
            }
            else
            {
                swal("No se puede realizar el envio", "No se ha generado certificados pora Docentes o Participantes.", "warning");
			}
        }
        else
        {
            swal("No se puede realizar el envio", "No se asigno la imagen de fondo del certificado.", "warning");
		}

    });

</script>