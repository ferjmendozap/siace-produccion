<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
<form id="formAjax" action="{$_Parametros.url}modEV/gestionEventoCONTROL/NuevoEventoMET" class="floating-label form form-validation" role="form" novalidate="novalidate" >
	<input type="hidden" value="1" name="valido" />
	<input type="hidden" name="cantidadPonente" id="cantidadPonente" value="0" /> {* el modulo asumira que solo es el contralor *}
	<input type="hidden" name="cantidadParticipante" id="cantidadParticipante" value="0" />
	<input type="hidden" name="cantidadTema" id="cantidadTema" value="0" />
	<input type="hidden" name="cadenaTema" id="cadenaTema" value="0" />
	<input type="hidden" name="cadenaPonentes" id="cadenaPonentes" value="0" />
	<input type="hidden" name="cadenaParticipantes" id="cadenaParticipantes" value="0" />
	<input type="hidden" name="pk_contralor" id="pk_contralor" value="{$pk_contralor}" />
		<div class="form-wizard-nav">
			<div class="progress"><div class="progress-bar progress-bar-primary"></div>
			</div>
			<ul class="nav nav-justified">
				<li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">Evento</span></a></li>
				<li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Ponentes y Participantes</span></a></li>
				<li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Certificado</span></a></li>
			</ul>
		</div><!--end .form-wizard-nav -->
		<!-- *********** PASO 1 ******************-->
		<div class="tab-content clearfix">
			<br/>
			<div class="tab-pane active" id="step1">
				<div class="well clearfix">
					<div class="col-md-6 col-sm-6">
						<div class="form-group">
							<textarea class="form-control" rows="2" name="ind_nombre_evento" id="ind_nombre_evento" required="" aria-required="true" aria-invalid="true"></textarea>
							<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Nombre del Evento</label>
						</div>
						<div class="form-group">
							<textarea class="form-control" rows="2" name="ind_descripcion_evento" id="ind_descripcion_evento" ></textarea>
							<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Descripción del Evento</label>
							<p class="help-block">Opcional</p>
						</div>
						<div class="form-group floating-label">
							<div class="form-group floating-label">
								<div class="input-group">
									<div class="input-group-content">
										<input type="text" class="form-control dirty" id="ind_lugar_evento" name="ind_lugar_evento" required="" onkeypress="return false" placeholder="Realice la búsqueda...">
										<input type="hidden" class="form-control" id="pk_num_lugar_evento" name="pk_num_lugar_evento" required="">
										<label for="groupbutton10">Lugar</label>
									</div>
									<div class="input-group-btn">
										<button class="btn btn-default" type="button" onclick="buscarLugar()" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Lugares"><i class="glyphicon glyphicon-search"></i></button>
									</div>
								</div>
							</div><!--end .form-group -->
						</div>
						<div class="form-group floating-label">
							<select id="pk_num_tipo_ente" name="pk_num_tipo_ente" class="select2-container form-control select2" onchange="consultarEnte(this.value)">
								<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
									<option value="-1">...</option>
									{foreach item=ente from=$tipoEnte}
										<option value="{$ente.pk_num_miscelaneo_detalle}">{$ente.ind_nombre_detalle}</option>
									{/foreach}
							</select>
							<label for="pk_num_tipo_ente"><i class="glyphicon glyphicon-triangle-right"></i>Tipo de Ente</label>
							<p class="help-block">Opcional</p>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="form-group floating-label">
							<select id="tipo_evento" name="tipo_evento" class="select2-container form-control select2" required="" aria-required="true" aria-invalid="true">
								<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
									<option value=""></option>
									{foreach item=evento from=$tipoEvento}
										<option value="{$evento.pk_num_miscelaneo_detalle}">{$evento.ind_nombre_detalle}</option>
									{/foreach}
							</select>
							<label for="tipo_evento"><i class="glyphicon glyphicon-triangle-right"></i>Tipo de Evento</label>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="form-group" style="width:100%">
								<div class="input-daterange input-group" id="demo-date-range">
									<div class="input-group-content">
										<input type="text" class="form-control dirty" name="fecha_inicio" id="fecha_inicio" onchange="calcularTiempo()" required="">
										<label><i class="glyphicon glyphicon-calendar"></i> Fecha del Evento</label>
									</div>
									<span class="input-group-addon">a</span>
									<div class="input-group-content">
										<input type="text" class="form-control dirty" name="fecha_fin" id="fecha_fin" onchange="calcularTiempo()" required="">
										<div class="form-control-line"></div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12">
							<div class="col-md-6 col-sm-6">
								<div class="col-md- col-sm-6">
									<div class="form-group">
										<input type="text" name="hora_inicio" id="hora_inicio" class="form-control dirty time-mask" data-rule-minlength="5" maxlength="5" onchange="calcularTiempo()" required="" aria-required="true" aria-invalid="true" >
										<label class="control-label"><i class="glyphicon glyphicon-time"></i> Hora Entrada</label>
										<p class="help-block">00:00</p>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="form-group floating-label">
										<select name="horario_inicio" class="form-control" id="horario_inicio" onchange="calcularTiempo()" >
											<option value="">Seleccione...</option>
											<option value="AM">AM</option>
											<option value="PM">PM</option>
										</select>
										<p class="help-block">am/pm</p>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="col-md-6 col-sm-6">
									<div class="form-group">
										<input type="text"  name="hora_fin" id="hora_fin" class="form-control dirty time-mask" data-rule-minlength="5" maxlength="5" id="hora_final" onchange="calcularTiempo()" required="" aria-required="true" aria-invalid="true" >
										<label class="control-label">Hora Salida</label>
										<p class="help-block">00:00</p>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="form-group floating-label">
										<select name="horario_fin" class="form-control " id="horario_fin" onchange="calcularTiempo()" >
											<option value="">Seleccione...</option>
											<option value="AM">AM</option>
											<option value="PM">PM</option>
										</select>
										<p class="help-block">am/pm</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<input type="text" class="form-control dirty time-mask" id="hora_total" name="hora_total"  required="">
								<label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Total de Horas</label>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6" id="ente">
						<div class="form-group floating-label">
							<select  class="select2-container form-control select2" name="pk_num_ente" id="pk_num_ente">
								<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
									<option value="">&nbsp;</option>
							</select>
							<p class="help-block">Opcional</p>
							<label><i class="glyphicon glyphicon-triangle-right"></i>Ente</label>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<h3 class="text-primary">Tema(s) del Evento - <em id="totalTemasTitu" class="text-xs">Total 0</em></h3></h3>
						<div class="card">
							<div class="card-body no-padding">
								<div style="overflow: auto; height: 200px;" class="table-responsive no-margin">
									<table class="table table-striped no-margin" id="contenidoTablaTema">
										<thead>
										<tr>
											<th width="100" class="text-center">Identificador</th>
											<th>Tema</th>
											<th></th>
											<th></th>
										</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
										<tr>
											<td colspan="5" align="center"></td>
										</tr>
										</tfoot>
									</table>
								</div>
							</div>
							<div class="card-actionbar">
								<div class="" align="center">
									<button type="button" id="nuevoCampoTema" class="btn btn-info ink-reaction btn-raised">
										<i class="md md-add"></i> Insertar Nuevo Campo
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!--end #step1 -->
			<!-- *********** PASO 2 ******************PARTICIPANTES Y PONENTES-->
			<div class="tab-pane" id="step2">
				<div class="col-sm-12">
					<div class="col-lg-12">
						<h3 class="text-primary">Ponentes - <em id="totalPonentesTitu" class="text-xs">Total 0</em></h3>
						<div class="card">
							<div class="card-body no-padding">
								<div class="table-responsive no-margin">
									<table class="table table-striped no-margin" id="contenidoTabla">
										<thead>
										<tr>
											<!-- <th class="text-center">ID</th> -->
											<th width="30">Cédula</th>
											<th class="text-left">Nombre y Apellido</th>
											<th style="vertical-align: middle;" width="150">Nivel de Instrucción</th>
											<th width="60">Seleccionar</th>
											<th width="60">Firma</th>
											<th width="100">Tipo firma</th>
											<th width="60">Eliminar</th>
										</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
										<tr>
											<td colspan="6" align="center">
											</td>
										</tr>
										</tfoot>
									</table>
								</div>
							</div>
							<div class="card-actionbar">
								<div class="" align="center">
									<button type="button" id="nuevoCampo" class="btn btn-info ink-reaction btn-raised">
										<i class="md md-add"></i> Insertar Nuevo Campo
									</button>
								</div>
							</div>
						</div>
						<em class="text-caption" >Solo se permiten dos (2) firmantes a demás de la máxima Autoridad. La máxima autoridad firmará todo certificado emitido.</em>
						<!-- ------------------- -->
						<h3 class="text-primary">Participantes - <em id="totalParticipantesTitu" class="text-xs">Total 0</em></h3>
						<div class="card">
							<div class="card-body no-padding">
								<div style="overflow: auto; height: 200px;" class="table-responsive no-margin">
									<table class="table table-striped no-margin" id="contenidoTabla2">
										<thead>
										<tr>
											<!-- <th class="text-center">ID</th> -->
											<th width="130">Cédula</th>
											<th class="text-left">Nombre y Apellido</th>
											<th width="60">Seleccionar</th>
											<th width="60">Eliminar</th>
										</tr>
										</thead>
										<tbody>

										</tbody>
										<tfoot>
										<tr>
											<td colspan="5" align="center"></td>
										</tr>
										</tfoot>
									</table>
								</div>

							</div>
							<div class="card-actionbar">
								<div class="" align="center">
									<button type="button" id="nuevoParticipante" class="btn btn-info ink-reaction btn-raised">
										<i class="md md-add"></i> Insertar Nuevo Campo
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!--end #step2 -->
			<!-- *********** PASO 3 ******************CERTIFICADOS-->
			<div class="tab-pane" id="step3">
				<h3 class="text-primary">Certificados</h3>
				<div class="well clearfix">
					<div class="col-md-12 col-sm-12">
						<div class="col-md-2 col-sm-2">
							<div class="checkbox checkbox-styled">
								<label>
									<input type="checkbox" value="1" name="participante" id="participante">
									<span>Participantes</span>
								</label>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="checkbox checkbox-styled">
								<label>
									<input type="checkbox" value="1" name="ponente" id="ponente">
									<span>Ponentes</span>
								</label>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<input type="radio" name="fondo" value="">
							<span>Desactivar Fondo</span>
						</div>
						<br/>
						<div class="col-md-12 col-sm-12">
							<br/>
							{foreach item=certificado from=$listarCertificado}
							<div class="col-md-2 col-sm-2"><img src="{$_Parametros.url}publico/imagenes/modEV/fondo/{$certificado.ind_imagen}" alt="No disponible" class="img-thumbnail" width=140 height=210><br/><div class="radio radio-styled" align="center"><label><span>Fondo {$certificado.ind_imagen}</span>&nbsp;<input type="radio" name="fondo" value="{$certificado.ind_imagen}"></label></div></div>
							{/foreach}
						</div>
					</div>
				</div>
				<div align="center">
					<button type="button" id="cancelar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario
					ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
					<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
				</div>
			</div><!--end #step3 -->
		</div><!--end .tab-content -->

	<ul class="pager wizard">
		{*<li class="previous first"><a class="btn-raised" href="javascript:void(0);"> Primero </a></li>*}
		<li class="previous"><a class="btn-raised" href="javascript:void(0);"> Anterior </a></li>
		{*<li class="next last"><a class="btn-raised" href="javascript:void(0);"> Final </a></li>*}
		<li class="next"><a class="btn-raised" href="javascript:void(0);"> Siguiente </a></li>
	</ul>
</form>

</div><!--end #rootwizard -->

<style>
</style>
<script type="text/javascript">

    arrayPersona.length=0;
    arrayTema.length=0;

	//Inhabilitar la navegación por los pasos directamente
	$(".form-wizard-nav").on('click', function () {
		return false;
	});
	//***********************************
	$(".datepicker").datepicker({
		todayHighlight: true,
		format:'dd-mm-yyyy',
		autoclose: true,
		language:'es'
	});

	$(document).ready(function(){
		$("#hora_inicio").inputmask("99:99");
		$("#hora_fin").inputmask("99:99");
	});


	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}

	function repoFormatSelection( repo ) {
		return repo.full_name;
	}



	var $url = '{$_Parametros.url}modEV/gestionEventoCONTROL/ConsultarEmpleadoMET';
	function buscarEmpleado(valorBoton, valor){
		$('#modalAncho2').css( "width", "90%" );
		$('#ContenidoModal2').html("");
		$('#formModalLabel2').html('Listado de Empleado');
		$.post($url,{ valorBoton: valorBoton, valor: valor},function($dato){
			$('#ContenidoModal2').html($dato);
		});
	}

	function eliminarFila(valorBoton, valor){ //ponentes

		//**************************
		if( $("#check_firma"+ valorBoton).is(':checked') )//Parte de la validacion de dos firmantes
		{
			CONTADOR_FIRMANTES--;
		}
		//**************************

		var cedulaDocumento = $("#ind_cedula_documento" + valorBoton).val();
		$('#fila'+valorBoton).remove();

		var $urlVerificarPersona = '{$_Parametros.url}modEV/gestionEventoCONTROL/VerificarPersonaMET';
		$.post($urlVerificarPersona,{ cedulaDocumento: cedulaDocumento},function($dato){
			var pkNumPersona = $dato['pkNumPersona'];
			for(var i = 0; i < arrayPersona.length; i++) {
				if (arrayPersona[i] == pkNumPersona) {
					arrayPersona[i] = 0;
					break;
				}
			}
		},'json');

		//**************************************
		var tr= $("#contenidoTabla > tbody > tr");
		$("#cantidadPonente").val(tr.length);
		$("#totalPonentesTitu").html('Total '+$("#cantidadPonente").val());
	}

	function eliminarFilaTema( valorBoton ){

		var identificacionTema = $("#pk_num_tema_evento" + valorBoton ).val();
		$('#filaTema'+valorBoton).remove();

			for(var i = 0; i < arrayTema.length; i++) {
				if (arrayTema[i] == identificacionTema) {

				    arrayTema[i] = 0;
					break;
				}
			}

		//**************************************
		var tr= $("#contenidoTablaTema > tbody > tr");
		$("#cantidadTema").val(tr.length);
		$("#totalTemasTitu").html('Total '+$("#cantidadTema").val());
	}

	$(document).ready(function() {
		var app = new  AppFunciones();
		$('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
			var input = $(e.currentTarget);
			if ($.trim(input.val()) !== '') {
				input.addClass('dirty').removeClass('static');
			} else {
				input.removeClass('dirty').removeClass('static');
			}
		});

		$('#contenidoTabla2  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
			var input = $(e.currentTarget);
			if ($.trim(input.val()) !== '') {
				input.addClass('dirty').removeClass('static');
			} else {
				input.removeClass('dirty').removeClass('static');
			}
		});

		$('#contenidoTablaTema  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
			var input = $(e.currentTarget);
			if ($.trim(input.val()) !== '') {
				input.addClass('dirty').removeClass('static');
			} else {
				input.removeClass('dirty').removeClass('static');
			}
		});


		//PONENTES***************************************************************************************************
		var numTr= - 1; //variables globales que controlan los id de los campos de los ponentes
		var nuevoTr='';
		var numero='';

		$("#nuevoCampo").click(function() {
			var idtabla= $("#contenidoTabla");
			nuevoTr=numTr+1;
			numero=nuevoTr+1;
			numTr++;
			//===========================================
			var cedula='<div class="form-group ">'+
					'<input placeholder="..." type="text" class="form-control dirty" value="" name="ind_cedula_documento'+nuevoTr+'" id="ind_cedula_documento'+nuevoTr+'"  onkeypress="return false" tipo_input="ci">'+
					'<label for="ind_cedula_documento'+nuevoTr+'"><i class="sm sm-insert-comment"></i>  </label></div>';
			var nombreApellido='<div class="form-group ">'+
					'<input  placeholder="Realice la búsqueda..." type="text" class="form-control dirty" value="" name="ind_nombre'+nuevoTr+'" id="ind_nombre'+nuevoTr+'" onkeypress="return false" tipo_input="nombre">'+
					'<label for="ind_nombre'+nuevoTr+'"><i class="lg lg-insert-comment"></i>  </label></div>';
			var instruccion = '<div class="form-group">'+
					'<select id="tipo_instruccion'+nuevoTr+'" name="tipo_instruccion'+nuevoTr+'" class="form-control input-sm" tipo_input="intruc">'+
					'<option value="">Seleccione...</option>'+
					'{foreach item=instruccion from=$tipoInstruccion}'+
					'<option value="{$instruccion.pk_num_miscelaneo_detalle}">{$instruccion.ind_nombre_detalle}</option>'+
					'{/foreach}'+
					'</select>'+
					'</div>';
			var cargar = '<div align="center"><button class=" btn ink-reaction btn-raised btn-xs btn-primary" id="buscar_empl'+nuevoTr+'" value="'+nuevoTr+'" type="button" onclick="buscarEmpleado(this.value, 1)" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado"><i class="glyphicon glyphicon-search"></i></button></div>';
			var eliminar = '<div align="center"><button onclick="eliminarFila(this.value, 1)" value="'+nuevoTr+'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="si, Eliminar" descripcion="El usuario ha eliminado un ponente" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el ponente?" title="Eliminar Ponente"><i class="md md-delete"></i></button></div>';
			//=========================================================
			var caja_firma = '<div class="checkbox checkbox-inline checkbox-styled form-group floating-label" align="center"><label><input tipo_input="firma" class="form-control dirty check_firma" disabled="disabled" value="n" id="check_firma'+nuevoTr+'" type="checkbox"><span></span></label></div >';
			var seleccion_firma = '<div class="form-group">'+
					'<select tipo_input="tipoFirma" id="tipo_firma'+nuevoTr+'" pk_persona_firma="" valor_indice="'+nuevoTr+'" disabled="disabled" name="tipo_firma'+nuevoTr+'" class="form-control input-sm selecTipoForma " >'+
					'<option value="0">...</option>'+
					'<option value="1">Escrita</option>'+
					'<option value="2" data-toggle="modal" data-target="#formModal3" data-keyboard="false" data-backdrop="static" >Imagen</option>'+
					'</select>'+
					'</div>';
			//===============================================
			idtabla.append('<tr id="fila'+nuevoTr+'">'+
					//'<td class="text-center" style="vertical-align: middle;">0'+numero+'</td>'+
					'<td style="vertical-align: middle;" width="130"><div >'+cedula+'</div></td>'+
					'<td style="vertical-align: middle;"><div >'+nombreApellido+'</div></td>'+
					'<td style="vertical-align: middle;" width="150">'+instruccion+'</td>'+
					'<td style="vertical-align: middle;" width="60">'+cargar+'</td>'+
					'<td style="vertical-align: middle;" width="60">'+caja_firma+'</td>'+
					'<td style="vertical-align: middle;" width="60">'+seleccion_firma+'</td>'+
					'<td style="vertical-align: middle;" width="60">'+eliminar+'</td>'+
					'</tr>');

			//=============================================
			var tr= $("#contenidoTabla > tbody > tr");
			$("#cantidadPonente").val(tr.length);
			$("#totalPonentesTitu").html('Total '+$("#cantidadPonente").val());

		});
		//PARTICIPANTES***********************************************************************************
		var numTrp= - 1; //variables globales que controlan los id de los campos de los participantes
		var nuevoTrp='';
		var numerop='';
		//================
		$("#nuevoParticipante").click(function() {
			var idtablap= $("#contenidoTabla2");
			nuevoTrp=numTrp+1;
			numerop=nuevoTrp+1;
			numTrp++;

			var cedula = '<div class="form-group floating-label">'+
					'<input placeholder="..." type="text" class="form-control dirty" value="" name="ind_cedula_participante'+nuevoTrp+'" id="ind_cedula_participante'+nuevoTrp+'" onkeypress="return false">'+
					'<label for="ind_cedula_participante'+nuevoTrp+'"><i class="sm sm-insert-comment"></i>  </label></div>';
			var nombreApellido = '<div class="form-group floating-label">'+
					'<input placeholder="Realice la búsqueda..." type="text" class="form-control dirty" value="" name="ind_nombre_participante'+nuevoTrp+'" id="ind_nombre_participante'+nuevoTrp+'" onkeypress="return false" >'+
					'<label for="ind_nombre_participante'+nuevoTrp+'"><i class="lg lg-insert-comment"></i>  </label></div>';
			var cargar = '<div align="center"><button class=" btn ink-reaction btn-raised btn-xs btn-primary" id="buscar_participante'+nuevoTrp+'" value="'+nuevoTrp+'" type="button" onclick="buscarEmpleado(this.value, 2)" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado"><i class="glyphicon glyphicon-search"></i></button></div>';
			var eliminar = '<div align="center"><button onclick="eliminarFilaParticipante(this.value, 2)" value="'+nuevoTrp+'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="si, Eliminar" descripcion="El usuario ha eliminado un participante" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el participante?" title="Eliminar Ponente"><i class="md md-delete"></i></button></div>';
			idtablap.append('<tr id="filaTabla'+nuevoTrp+'">'+
					//'<td class="text-center" style="vertical-align: middle;"> 0'+numerop+'</td>'+
					'<td style="vertical-align: middle;" width="130"><div class="col-sm-12">'+cedula+'</div></td>'+
					'<td style="vertical-align: middle;"><div class="col-lg-12">'+nombreApellido+'</div></td>'+
					'<td style="vertical-align: middle;" width="60">'+cargar+'</td>'+
					'<td style="vertical-align: middle;" width="60">'+eliminar+'</td>'+
					'</tr>');
			//=======================================
			var tr= $("#contenidoTabla2 > tbody > tr");
			$("#cantidadParticipante").val(tr.length);
			$("#totalParticipantesTitu").html('Total '+$("#cantidadParticipante").val());
			//=======================================
			//$("#ind_cedula_participante" + nuevoTrp ).focus();
		});

		//******************Envio del formulario******************************************************
		$("#formAjax").validate({

			submitHandler: function(form) {

				var swEnviar = 0;
				if($('select[id="tipo_evento"]').val()=="")//valida tipo evento
				{
					swal("Seleccione el tipo de evento", "Debe indicar en el paso \"Evento\" el tipo de evento a registrar", "warning");
					swEnviar = 1;
				}
				else
				{
					if($('select[id="horario_inicio"]').val()=="" || $('select[id="horario_fin"]').val()=="")//valida los horarios de las horas del evento
					{
						swal("Seleccione el horario del evento", "Debe indicar en el paso \"Evento\" el horario de la hora de entrada y salida respectivamente", "warning");
						swEnviar = 1;
					}
					else
					{
						//=======VALIDACIÓN PONENTES=======
						var cad_campos_ponentes='';

						$("#contenidoTabla").find(':input').each(function() {

							if($(this).attr('tipo_input') == "ci" || $(this).attr('tipo_input') == "nombre" )
							{
								if( $(this).val() == "")
								{
									swEnviar = 1;
								}
							}
							else//validar nivel de instruccion y firmas
							{
								if( $(this).attr('tipo_input') == "intruc" && $(this).val() == "")
								{
									swEnviar = 1;
								}
								else
								{
									if($(this).attr('tipo_input') == "firma")//check de firma
									{
										if($(this).attr('valor_caja') == "s")//la caja ha sido chekeada
										{
											if( $('select[id="tipo_firma'+$(this).val()+'"]').val() == 0 )
											{
												swEnviar = 1;//no se ha seleccionado la firma del ponente
											}
										}
									}
								}
							}
						});

						if(swEnviar == 1)
						{
							swal("Campos vacios en los ponentes", "Debe ingresar los campos, si no debe eliminarlos de las lista.", "warning");
						}
						else
						{
							//VALIDAR LOS TEMAS
							$("#contenidoTablaTema").find(':input').each(function() {
								if( $(this).val() == "")
								{
									swEnviar = 1;
								}
							});

							if(swEnviar == 1)
							{
								swal("Campos vacios en los temas", "Debe ingresar los campos, si no debe eliminarlos de la lista", "warning");
							}
							else
							{
								//VALIDAR PARTICIPANTES

								var totalParticipantes = $("#cantidadParticipante").val();

								if(totalParticipantes > 0)
								{
									$("#contenidoTabla2").find(':input').each(function () {
										if ($(this).val() == "") {
											swEnviar = 1;
										}
									});

									if (swEnviar == 1) {
										swal("Campos vacios en los participantes", "Debe ingresar los datos en los campos, al menos debe haber un participante en la lista.", "warning");
									}
								}
								else
								{
									swEnviar = 1;
									swal("Debe existir al menos un participante en el evento.", "Ingrese almenos un participante.", "warning");
								}
							}
						}

						//=================================
					}
				}



				if(swEnviar==0)
				{
					//=====RECOPILACION DE PONENTES=====
					var cad_ponentes = '';
					var cedula_ponente = '';
					var nombre_ponente = '';
					var grado_ponente = '';
					var tipo_firma_ponente = '';

					var fila='';
					$("#contenidoTabla > tbody > tr").each(function() {

						fila = $(this);//la fila actual
						fila.find(':input').each(function(){

							if($(this).attr('tipo_input') == "ci" )
							{
								cedula_ponente = $(this).val();
							}

							if($(this).attr('tipo_input') == "nombre" )
							{
								nombre_ponente = $(this).val();
							}

							if($(this).attr('tipo_input') == "intruc" )
							{
								grado_ponente = $(this).val();
							}

							if($(this).attr('tipo_input') == "firma")
							{
								tipo_firma_ponente = $('select[id="tipo_firma'+$(this).val()+'"]').val();
							}

						});

						cad_ponentes = cad_ponentes+cedula_ponente+'/'+nombre_ponente+'/'+grado_ponente+'/'+tipo_firma_ponente+'#';
					});

					//=====RECOPILACION DE PARTICIPANTES=====
					var cad_participante = '';
					var cedula_participante = '';
					var nombre_participante = '';

					$("#contenidoTabla2 > tbody > tr").each(function() {

						$(this).find(':input').each(function(index){

							if( index == 0 )
							{
								cedula_participante = $(this).val();
							}

							if(index == 1 )
							{
								nombre_participante = $(this).val();
							}

						});

						cad_participante = cad_participante+cedula_participante+'/'+nombre_participante+'#';
					});

					//=====RECOPILACION TEMAS =====
					var pk_tema = '';
					var nombre_tema = '';
					var cad_temas = '';

					$("#contenidoTablaTema > tbody > tr").each(function() {

						$(this).find(':input').each(function(index){

							if( index == 0 )
							{
								pk_tema = $(this).val();
							}

							if(index == 1 )
							{
								nombre_tema = $(this).val();
							}

						});

						cad_temas = cad_temas+pk_tema+'/'+nombre_tema+'#';
					});

					//================================================================

					$("#cadenaPonentes").val(cad_ponentes);
					$("#cadenaParticipantes").val(cad_participante);
					$("#cadenaTema").val(cad_temas);


					//=================================================================
					$.post($(form).attr('action'), $(form).serialize(), function (dato) {

						if(dato['pk_num_evento'] > 0)//Array.isArray()
						{
							arrayPersona.length = 0;
							arrayTema.length = 0;
							var botonCertificado = '';
							if(dato['fondo'] !='')
							{
								botonCertificado = '<div class="aniimated-thumbnials" align="left"><a href="{$_Parametros.url}publico/imagenes/modEV/fondo/' + dato['fondo'] + '"><img width="70" height="50" src="{$_Parametros.url}publico/imagenes/modEV/fondo/' + dato['fondo'] + '"/></a></div>';
							}
							else
							{
								botonCertificado = '<div class="aniimated-thumbnials" align="center"><i class="md-not-interested"></i></div>';
							}

							$(document.getElementById('datatable1')).append('<tr id="pk_num_evento' + dato['pk_num_evento'] + '"><td>' + dato['pk_num_evento'] + '</td><td>' + dato['ind_nombre_evento'] + '</td><td>' + dato['fecha_registro'] + '</td>' +
									'{if in_array('EV-01-01-02-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver Evento" titulo="Ver Evento" id="ver"  pk_num_evento="' + dato['pk_num_evento'] + '"><i class="glyphicon glyphicon-search"></i></button></td>{/if}' +
									'{if in_array('EV-01-01-03-C',$_Parametros.perfil)}<td align="center">' + botonCertificado + '</td>{/if}' +
									'{if in_array('EV-01-01-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descipcion="El Usuario ha Modificado un evento" title="Modificar Evento"  titulo="Modificar tema" pk_num_evento="' + dato['pk_num_evento'] + '"><i class="fa fa-edit"></i></button></td>{/if}' +
									'{if in_array('EV-01-01-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="si, Eliminar" descipcion="El usuario ha eliminado un evento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el evento?" title="Eliminar Evento" pk_num_evento="' + dato['pk_num_evento'] + '"><i class="md md-delete"></i></button></td>{/if}</tr>');
							swal("Evento Guardado", "Evento guardado satisfactoriamente", "success");
							$(document.getElementById('cerrarModal')).click();
							$(document.getElementById('ContenidoModal')).html('');
						}
						else
						{
							swal("Error al guardar evento", "Error: "+dato, "error");
						}

					}, 'json');
					//====================================
				}
			}
		});
	});

	//*******************************************************
	function calcularTiempo() {
		var urlComprueba = "{$_Parametros.url}modEV/gestionEventoCONTROL/CalcularTiempoEventoMET";
		var fechaInicial = $("#fecha_inicio").val();
		var fechaFinal = $("#fecha_fin").val();
		var horaInicio = $("#hora_inicio").val();
		var horaFin = $("#hora_fin").val();
		var horarioInicio = $("#horario_inicio").val();
		var horarioFin = $("#horario_fin").val();
		$.post(urlComprueba,{ fechaInicio:""+fechaInicial, fechaFin:""+fechaFinal, horaInicio:""+horaInicio, horarioInicio:""+horarioInicio, horaFin:""+horaFin, horarioFin:""+horarioFin },function(dato){
			$("#hora_total").val(dato['horaTotal']);
		},'json');
	}

	//TEMAS******************************************************
	var numTrt= - 1; //variables globales que controlan los id de los campos de los temas del evento
	var nuevoTrt='';
	var numerot='';

	$("#nuevoCampoTema").click(function() {
		var idtablat= $("#contenidoTablaTema");
		nuevoTrt=numTrt+1;
		numerot=nuevoTrt+1;
		numTrt++;

		var id='<div class="form-group floating-label">'+
				'<input placeholder="..." type="text" class="form-control dirty" value="" name="pk_num_tema_evento'+nuevoTrt+'" id="pk_num_tema_evento'+nuevoTrt+'" onkeypress="return false">'+
				'<label for="pk_num_tema_evento'+nuevoTrt+'"><i class="sm sm-insert-comment"></i></label></div>';
		var tema='<div class="form-group floating-label">'+
				'<input type="text" class="form-control dirty" value="" name="ind_tema'+nuevoTrt+'" id="ind_tema'+nuevoTrt+'" onkeypress="return false" placeholder="Realice la búsqueda...">'+
				'<label for="ind_tema'+nuevoTrt+'"><i class="sm sm-insert-comment"></i>  </label></div>';

		var cargarTema = '<div align="center"><button id="buscar_tem'+nuevoTrt+'" class=" btn ink-reaction btn-raised btn-xs btn-primary" value="'+nuevoTrt+'" type="button" onclick="buscarTema(this.value)" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Temas"><i class="glyphicon glyphicon-search"></i></button></div>';
		var eliminar = '<div align="center"><button onclick="eliminarFilaTema(this.value)" value="'+nuevoTrt+'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="si, Eliminar" descripcion="El usuario ha eliminado un tema" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el tema?" title="Eliminar Tema"><i class="md md-delete"></i></button></div>';
		idtablat.append('<tr id="filaTema'+nuevoTrt+'">'+

				'<td class="text-center" style="vertical-align: middle;">'+id+'</td>'+
				'<td style="vertical-align: middle;"><div class="col-sm-12">'+tema+'</div></td>'+
				'<td style="vertical-align: middle;" width="60">'+cargarTema+'</td>'+
				'<td style="vertical-align: middle;" width="60">'+eliminar+'</td>'+
				'</tr>');
		//=====================================
		var tr= $("#contenidoTablaTema > tbody > tr");
		$("#cantidadTema").val(tr.length);
		$("#totalTemasTitu").html('Total '+$("#cantidadTema").val());
		//=======================================
		//$("#pk_num_tema_evento" + nuevoTrt ).focus();

	});


	function buscarTema(valorBoton){
        var $urlTema = '{$_Parametros.url}modEV/gestionEventoCONTROL/ConsultarTemaMET';
		$('#ContenidoModal2').html("");
		$('#formModalLabel2').html('Listado de Temas');
		$.post($urlTema,{ valorBoton: valorBoton},function($dato){
			$('#ContenidoModal2').html($dato);
		});
	}

	function buscarLugar(){
        var $urlLugar = '{$_Parametros.url}modEV/gestionEventoCONTROL/ConsultarLugarMET';
		$('#ContenidoModal2').html("");
		$('#formModalLabel2').html('Listado de Lugares');
		$.post($urlLugar,"",function($dato){
			$('#ContenidoModal2').html($dato);
		});
	}

	function consultarEnte(pk_num_tipo_ente)
	{
        $("#ente").html("");
        $.post("{$_Parametros.url}modEV/gestionEventoCONTROL/BuscarEnteMET",{ pk_num_tipo_ente:""+pk_num_tipo_ente }, function (dato) {
            $("#ente").html(dato);
        });
    }

	//*************************************************
	var CONTADOR_FIRMANTES = 0;

	$("#formAjax").on("change", ".check_firma", function(){

		var total_firmantes = 2;
		//***************************************
		var pk_contralor = $("#pk_contralor").val();
		for(var i = 0; i < arrayPersona.length; i++) {
			if (arrayPersona[i] == pk_contralor) {
				total_firmantes = 3;
				break;
			}
		}
		//***************************************
		if (this.checked) {

			CONTADOR_FIRMANTES++;
			if (CONTADOR_FIRMANTES> total_firmantes)//2
			{
				CONTADOR_FIRMANTES--;
				$(this).prop('checked', false);
				swal("¡Aviso!", "Yá existen dos (2) firmantes a demás de la maxima Autoridad.", "warning");
			}


		}
		else
		{
			CONTADOR_FIRMANTES--;
		}
	});

	$("#formAjax").on('change', '.check_firma', function(){


		if( $(this).is(':checked') )
		{
			$(this).attr('valor_caja','s');
			$('select[id="tipo_firma'+$(this).val()+'"]').prop('disabled', false);
		}
		else
		{
			$(this).attr('valor_caja','n');
			$('select[id="tipo_firma'+$(this).val()+'"]').prop('disabled', true);
		}

		$('select[id="tipo_firma'+$(this).val()+'"]').val(0);
		$('select[id="tipo_firma'+$(this).val()+'"]').change();

	});


	var BAND_FORM = 0;

	$("#formAjax").on('click', '.selecTipoForma', function(){

		var pk_persona = $(this).attr("pk_persona_firma");
		var valor_indice = $(this).attr("valor_indice");//nuevoTr

		var ind_cedula_documento = $('#ind_cedula_documento' + valor_indice).val();
		var ind_nombre = $('#ind_nombre' + valor_indice).val();

		if($(this).val() == '2')
		{
			$('#modalAncho3').css("width", "30%");
			$('#ContenidoModal3').html("");
			$('#formModalLabel3').html('Firma Impresa (Imagen PNG sin fondo)');
			$.post("{$_Parametros.url}modEV/gestionEventoCONTROL/FirmaImpresaMET", { pk_persona: pk_persona, valor_indice: valor_indice, ind_cedula_documento: ind_cedula_documento, ind_nombre: ind_nombre },function($dato){
				$('#ContenidoModal3').html($dato);
			});
		}

	});

	function eliminarFilaParticipante(valorBoton, valor){

		var cedulaDocumento = $("#ind_cedula_participante" + valorBoton).val();
		$('#filaTabla'+valorBoton).remove();

		var $urlVerificarPersona = '{$_Parametros.url}modEV/gestionEventoCONTROL/VerificarPersonaMET';
		$.post($urlVerificarPersona,{ cedulaDocumento: cedulaDocumento},function($dato){
			var pkNumPersona = $dato['pkNumPersona'];
			for(var i = 0; i < arrayPersona.length; i++) {
				if (arrayPersona[i] == pkNumPersona) {
					arrayPersona[i] = 0;
					break;
				}
			}
		},'json');

		//=========================================
		var tr= $("#contenidoTabla2 > tbody > tr");
		$("#cantidadParticipante").val(tr.length);
		$("#totalParticipantesTitu").html('Total '+$("#cantidadParticipante").val());
	}
	//*************************************************
	$('#cerrarModal').click(function(){
		arrayPersona.length = 0;
		arrayTema.length = 0;
	});

	$('#cancelar').click(function(){
		arrayPersona.length = 0;
		arrayTema.length = 0;
	});

</script>
