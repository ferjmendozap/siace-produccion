<br/>
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;Gestión de Eventos</h2>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <br/>
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th width="60"> N°</th>
                        <th> Evento</th>
                        <th width="60" align="center">Fecha</th>
                        {if in_array('EV-01-01-02-V',$_Parametros.perfil)}<th width="60" align="center">Ver</th>{/if}
                        {if in_array('EV-01-01-03-C',$_Parametros.perfil)}<th width="70" align="center">Certificado</th>{/if}
                        {if in_array('EV-01-01-04-M',$_Parametros.perfil)}<th width="60" align="center">Editar</th>{/if}
                        {if in_array('EV-01-01-05-E',$_Parametros.perfil)}<th width="60" align="center">Eliminar</th>{/if}
                    </tr>
                    </thead>
                    <tbody>
                        {foreach item=listado from=$listadoEvento}
                            <tr id="pk_num_evento{$listado.pk_num_evento}">
                                <td>{$listado.pk_num_evento}</td>
                                <td>{$listado.ind_nombre_evento}</td>
                                <td align="center">{$listado.fecha_inicio}</td>
                                {if in_array('EV-01-01-02-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" pk_num_evento="{$listado.pk_num_evento}" data-keyboard="false" data-backdrop="static" title="Ver Evento" titulo="Ver Evento"><i class="glyphicon glyphicon-search"></i></button></td>{/if}
                                {if in_array('EV-01-01-03-C',$_Parametros.perfil)}<td align="center">
                                    {if $listado.ind_certificado!=''}
                                    <div align="left">
                                        <a href="{$_Parametros.url}publico/imagenes/modEV/fondo/{$listado.ind_certificado}" target="_blank">
                                            <img width="70" height="50" src="{$_Parametros.url}publico/imagenes/modEV/fondo/{$listado.ind_certificado}"/>
                                        </a>
                                    </div>
                                    {else}
                                        <div align="center">
                                            <i class="md-not-interested"></i>
                                        </div>
                                    {/if}
                                </td>{/if}
                                {if in_array('EV-01-01-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descipcion="El Usuario ha Modificado un evento" title="Modificar Evento" titulo="Modificar evento" pk_num_evento="{$listado.pk_num_evento}"><i class="fa fa-edit"></i></button></td>{/if}
                                {if in_array('EV-01-01-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_evento="{$listado.pk_num_evento}"  boton="si, Eliminar" descipcion="El usuario ha eliminado un evento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el evento?" title="Eliminar almacén"><i class="md md-delete"></i></button></td>{/if}
                            </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="7">
                            {if in_array('EV-01-01-01-N',$_Parametros.perfil)}<button data-backdrop="static" data-keyboard="false"  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado un nuevo evento" data-toggle="modal" data-target="#formModal" titulo="Registrar Nuevo Evento" id="nuevo"> <i class="md md-create"></i> Nuevo Evento</button>{/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var arrayPersona =  new Array();
    var arrayTema =  new Array();

    $(document).ready(function() {
        var $url='{$_Parametros.url}modEV/gestionEventoCONTROL/NuevoEventoMET';
        $('#nuevo').click(function(){
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
                $.post($url,'',function($dato){
                arrayPersona.length=0;
                $('#ContenidoModal').html($dato);
            });
        });

        var $url_modificar='{$_Parametros.url}modEV/gestionEventoCONTROL/EditarEventoMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            arrayTema.length=0;
            $.post($url_modificar,{ pk_num_evento: $(this).attr('pk_num_evento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var pk_num_evento=$(this).attr('pk_num_evento');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modEV/gestionEventoCONTROL/EliminarEventoMET';
                $.post($url, { pk_num_evento: pk_num_evento},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('pk_num_evento'+$dato['pk_num_evento'])).html('');
                        swal("Eliminado!", "El evento fue eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });


    var $urlVer='{$_Parametros.url}modEV/gestionEventoCONTROL/VerEventoMET';
    $('#datatable1 tbody').on( 'click', '.ver', function () {
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_evento: $(this).attr('pk_num_evento')},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    $('.aniimated-thumbnials').lightGallery({
        thumbnail:true
    });
</script>
