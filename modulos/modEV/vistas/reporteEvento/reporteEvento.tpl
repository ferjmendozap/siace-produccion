<section class="style-default-bright">
    <br/>
    <div class="form floating-label">
        <h2 class="text-primary">&nbsp;Reporte de Eventos</h2>
    <br/>
        {assign var="numero" value="1"}
            <div class="table-responsive">
                <div class="col-md-12 col-sm-12" align="center">
                <table class="table table-bordered table-hover">
                    <thead style="background-color: #e1e1e8">
                    <tr>
                        <th width="100" align="center"><b>N° de Reporte</b></th>
                        <th align="left" colspan="3"><b>Reportes</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="100" align="center">{$numero++}</td>
                        <td width="200"><input type="radio" checked name="busqueda" id="busqueda" value="1"><span> Eventos por mes:</span></td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <select id="mes_evento" name="mes_evento" class="select2-container form-control select2">
                                    <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                    <option value="1">ENERO</option>
                                    <option value="2">FEBRERO</option>
                                    <option value="3">MARZO</option>
                                    <option value="4">ABRIL</option>
                                    <option value="5">MAYO</option>
                                    <option value="6">JUNIO</option>
                                    <option value="7">JULIO</option>
                                    <option value="8">AGOSTO</option>
                                    <option value="9">SEPTIEMBRE</option>
                                    <option value="10">OCTUBRE</option>
                                    <option value="11">NOVIEMBRE</option>
                                    <option value="12">DICIEMBRE</option>
                                </select>
                            </div>
                        </td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <select id="anio_evento" name="anio_evento" class="form-control">
                                    {foreach item=anio from=$anioEvento}
                                        <option value="{$anio.anio}">{$anio.anio}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="100" align="center">{$numero++}</td>
                        <td width="200"><input type="radio" name="busqueda" id="busqueda" value="2"><span> Eventos por trimestre:</span></td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <select id="trimestre" name="trimestre" class="form-control">
                                    <option value="1">I</option>
                                    <option value="2">II</option>
                                    <option value="3">III</option>
                                    <option value="4">IV</option>
                                </select>
                            </div>
                        </td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <select id="anio_trimestre" name="anio_trimestre" class="form-control">
                                    {foreach item=anio from=$anioEvento}
                                        <option value="{$anio.anio}">{$anio.anio}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="100" align="center">{$numero++}</td>
                        <td width="200"><input type="radio" name="busqueda" id="busqueda" value="3"><span> Eventos por semestre:</span></td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <select id="semestre" name="semestre" class="form-control">
                                    <option value="1">PRIMER</option>
                                    <option value="2">SEGUNDO</option>
                                </select>
                            </div>
                        </td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <select id="anio_semestre" name="anio_semestre" class="form-control">
                                    {foreach item=anio from=$anioEvento}
                                        <option value="{$anio.anio}">{$anio.anio}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="100" align="center">{$numero++}</td>
                        <td width="200"><input type="radio" name="busqueda" id="busqueda" value="4"><span> Participantes por eventos:</span></td>
                        <td colspan="2">
                            <div class="form-group floating-label">
                                <select id="pk_num_evento" name="pk_num_evento" class="select2-container form-control select2">
                                    <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                        {foreach item=evento from=$listarEvento}
                                            <option value="{$evento.pk_num_evento}">{$evento.ind_nombre_evento}</option>
                                        {/foreach}
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="100" align="center">{$numero++}</td>
                        <td width="200"><input type="radio" name="busqueda" id="busqueda" value="5"><span> Reporte Anual:</span></td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <select id="tipo_evento" name="tipo_evento" class="select2-container form-control select2">
                                    <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                    <option value="0">TODOS</option>
                                    {foreach item=evento from=$tipoEvento}
                                        <option value="{$evento.pk_num_miscelaneo_detalle}">{$evento.ind_nombre_detalle}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <select id="anio_tipo_evento" name="anio_tipo_evento" class="form-control">
                                    {foreach item=anio from=$anioEvento}
                                        <option value="{$anio.anio}">{$anio.anio}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="100" align="center">{$numero++}</td>
                        <td width="200"><input type="radio" name="busqueda" id="busqueda" value="6"><span> Ubicaci&oacute;n:</span></td>
                        <td width="200">
                            <div class="form-group floating-label">
                                <div class="form-group floating-label">
                                    <div class="input-group">
                                        <div class="input-group-content">
                                            <input type="text" class="form-control dirty" id="ind_lugar_evento" name="ind_lugar_evento">
                                            <input type="hidden" class="form-control" id="pk_num_lugar_evento" name="pk_num_lugar_evento">
                                            <label for="groupbutton10">Lugar</label>
                                        </div>
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="button" onclick="buscarLugar()" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Lugares"><i class="glyphicon glyphicon-search"></i></button>
                                        </div>
                                    </div>
                                </div><!--end .form-group -->
                            </div>
                        </td>
                        <td width="200">
                            <table align="center" width="70%">
                                <tr>
                                    <td>
                                        <div class="form-group floating-label">
                                            <select id="anio_tipo" name="anio_tipo" class="form-control dirty" onchange="buscarMes(this.value)">
                                                <option value="0"></option>
                                                {foreach item=anio from=$anioEvento}
                                                    <option value="{$anio.anio}">{$anio.anio}</option>
                                                {/foreach}
                                            </select>
                                            <label><i class="glyphicon glyphicon-briefcase"></i> Año</label></div>
                                         </div>
                                     </td>
                                     <td>
                                        <div id="mesEvento">
                                            <div class="form-group floating-label">
                                                <select class="form-control dirty" name="mes" id="mes">
                                                    <option value="0">&nbsp;</option>
                                                </select><label for="mes"><i class="glyphicon glyphicon-briefcase"></i> Mes</label>
                                            </div>
                                        </div>
                                     </td>
                                 </tr>
                            </table>
                         </td>
                    </tr>
                <tr>
                    <td width="100" align="center">{$numero++}</td>
                    <td width="200"><input type="radio" name="busqueda" id="busqueda" value="7"><span> Ente:</span></td>
                    <td width="200">
                        <div class="form-group floating-label">
                            <select id="pk_num_ente" name="pk_num_ente" class="select2-container form-control select2">
                                <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                    <option value="">&nbsp;</option>
                                    {foreach item=ente from=$listaEnte}
                                        <option value="{$ente.pk_num_ente}">{$ente.ind_nombre_ente}</option>
                                    {/foreach}
                            </select>
                        </div>
                    </td>
                    <td width="200">
                        <table align="center" width="70%">
                            <tr>
                                <td>
                                    <div class="form-group floating-label">
                                        <select id="anio_ente" name="anio_ente" class="form-control dirty" onchange="buscarMesEnte(this.value)">
                                            <option value="0"></option>
                                            {foreach item=anio from=$anioEvento}
                                                <option value="{$anio.anio}">{$anio.anio}</option>
                                            {/foreach}
                                        </select>
                                        <label><i class="glyphicon glyphicon-briefcase"></i> Año</label></div>
                                    </div>
                                 </td>
                                 <td>
                                    <div id="mesEventoEnte">
                                        <div class="form-group floating-label">
                                            <select class="form-control dirty" name="mes_ente" id="mes_ente">
                                                <option value="0">&nbsp;</option>
                                            </select><label for="mesEnte"><i class="glyphicon glyphicon-briefcase"></i> Mes</label>
                                        </div>
                                    </div>
                                 </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            </div>
            <div class="col-sm-12 col-sm-12" align="center">
                <button type="submit" id="buscarEvento" data-toggle="modal" data-target="#formModal" titulo="Reporte de Eventos" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('#buscarEvento').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var porNombre=document.getElementsByName("busqueda");
            for(var i=0;i<porNombre.length;i++) {
                if(porNombre[i].checked)
                    $resultado =porNombre[i].value;
            }
            var mes_evento = $("#mes_evento").val();
            var anio_evento = $("#anio_evento").val();
            var trimestre = $("#trimestre").val();
            var anio_trimestre = $("#anio_trimestre").val();
            var semestre = $("#semestre").val();
            var anio_semestre = $("#anio_semestre").val();
            var pk_num_evento = $("#pk_num_evento").val();
            var tipo_evento = $("#tipo_evento").val();
            var anio_tipo_evento = $("#anio_tipo_evento").val();
            var pk_num_lugar_evento = $("#pk_num_lugar_evento").val();
            var anio_tipo = $("#anio_tipo").val();
            var mes = $("#mes").val();
            var pk_num_ente = $("#pk_num_ente").val();
            var anio_ente = $("#anio_ente").val();
            var mes_ente = $("#mes_ente").val();
            
            var urlReporte = '{$_Parametros.url}modEV/reporteEventoCONTROL/GenerarReporteEventoMET/?busqueda='+$resultado+'&mes_evento='+mes_evento+'&anio_evento='+anio_evento+'&trimestre='+trimestre+'&anio_trimestre='+anio_trimestre+'&semestre='+semestre+'&anio_semestre='+anio_semestre+'&pk_num_evento='+pk_num_evento+'&tipo_evento='+tipo_evento+'&anio_tipo_evento='+anio_tipo_evento+'&pk_num_lugar_evento='+pk_num_lugar_evento+'&anio_tipo='+anio_tipo+'&mes='+mes+'&pk_num_ente='+pk_num_ente+'&anio_ente='+anio_ente+'&mes_ente='+mes_ente;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
            
        });
    });

    var placeholder = "";

    $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
    $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult( repo ) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    function buscarLugar(){
        var $urlLugar = '{$_Parametros.url}modEV/gestionEventoCONTROL/ConsultarLugarMET';
        $('#ContenidoModal2').html("");
        $('#formModalLabel2').html('Listado de Lugares');
        $.post($urlLugar,"",function($dato){
            $('#ContenidoModal2').html($dato);
        });
    }

    function buscarMes(anio){
        $("#mesEvento").html("");
        var $urlMes = '{$_Parametros.url}modEV/reporteEventoCONTROL/ConsultarMesEventoMET';
        $.post($urlMes,{ anio_evento: anio, tipo: 1},function($dato){
            $('#mesEvento').html($dato);
        });
    }

    function buscarMesEnte(anio){
        $("#mesEventoEnte").html("");
        var $urlMes = '{$_Parametros.url}modEV/reporteEventoCONTROL/ConsultarMesEventoMET';
        $.post($urlMes,{ anio_evento: anio, tipo: 2},function($dato){
            $('#mesEventoEnte').html($dato);
        });
    }
</script>