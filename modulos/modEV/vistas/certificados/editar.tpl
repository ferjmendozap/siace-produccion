<input type="hidden" name="ultimoId" id="ultimoId" />
<div id="my-dropzone" class="dropzone"></div>
<br/>
<div align="right">
	<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cerrado el Registro" data-dismiss="modal" onclick="recargar()"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<script type="text/javascript">
	var cleanFilename = function (name) {
		//return name.toLowerCase().replace(/[^\w]/gi, '');
		return 'bob.png';
	};

	Dropzone.autoDiscover = false;
	Dropzone.options.myDropzone = {
		init: function() {
			thisDropzone = this;
			$.post("{$_Parametros.url}modEV/certificadosCONTROL/CargarImagenMET",{ cargarImagenes:1 }, function(data) {
				//alert("dropzone inicializando");
				$.each(data, function(key,value){

					var mockFile = { name: value.name, size: value.size };

					thisDropzone.options.addedfile.call(thisDropzone, mockFile);

					thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "{$_Parametros.url}publico/imagenes/modEV/fondo/"+value.name);

				});

			});
		}
	};
	$("#my-dropzone").dropzone({
		url: "{$_Parametros.url}modEV/certificadosCONTROL/CargarImagenMET",
		addRemoveLinks: true,
		dictRemoveFile: "Eliminar",
		dictCancelUpload: "Eliminar",
		maxFileSize: 1000,
		dictResponseError: "Ha ocurrido un error en el server",
		acceptedFiles: '.jpeg,.jpg,.png,.gif',
		autoProcessQueue: true,
		renameFilename: false,
		complete: function(file)
		{
			if(file.status == "success")
			{
				//swal("Carga exitosa", "Archivo cargado satisfactoriamente", "success");
			}
		},
		error: function(file)
		{
			swal("Error", "Error subiendo el archivo", "success");
		},
		removedfile: function(file, serverFileName)
		{
			var name = file.name;
			$.ajax({
				type: "POST",
				url: "{$_Parametros.url}modEV/certificadosCONTROL/CargarImagenMET",
				data: { filename:""+name,delete:"true" },
				success: function(data)
				{
					var json = JSON.parse(data);
					if(json.res == true)
					{
						var element;
						(element = file.previewElement) != null ?
								element.parentNode.removeChild(file.previewElement) :
								false;
						swal("Elemento eliminado", "Elemento eliminado satisfactoriamente", "success");
					}
				}
			});
		}
	});

	$('.aniimated-thumbnials').lightGallery({
        thumbnail:true
    });


	function recargar()
	{
		var url_listar='{$_Parametros.url}modEV/certificadosCONTROL/ConsultarCertificadoMET';
		$.post(url_listar, '',function(respuesta_post) {
			var tabla_listado = $('#datatable1').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					if(respuesta_post[i].num_estatus==1){
						var botonEstatus =  '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_certifica="'+respuesta_post[i].pk_num_certificado+'" descripcion="El Usuario ha deshabilitado un certificado" title="Deshabilitar Certificado"><i class="md-done"></i></button>'
					} else {
						var botonEstatus = '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_certificado="'+respuesta_post[i].pk_num_certificado+'" descipcion="El Usuario ha deshabilitado un certificado" title="Deshabilitar Certificado"><i class="md md-not-interested"></i></button>';
					}
					var botonCargar = '<div align="center"><a target="_blank" href="{$_Parametros.url}publico/imagenes/modEV/fondo/'+respuesta_post[i].ind_imagen+'"><img width="180" height="140" src="{$_Parametros.url}publico/imagenes/modEV/fondo/'+respuesta_post[i].ind_imagen+'"/></a></div>';
					var fila = tabla_listado.row.add([
						respuesta_post[i].pk_num_certificado,
						respuesta_post[i].ind_imagen,
						botonCargar,
						botonEstatus,
						respuesta_post[i].ind_nombre1+' '+respuesta_post[i].ind_nombre2+' '+respuesta_post[i].ind_apellido1+' '+respuesta_post[i].ind_apellido2,
						respuesta_post[i].fecha_modificacion
					]).draw()
					.nodes()
					.to$()
					 $(fila)
                                .attr('id','pk_num_certificado'+respuesta_post[i].pk_num_certificado+'');
				}
			}

		},'json');
	}

</script>


