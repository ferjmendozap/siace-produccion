<form id="formAjax" action="{$_Parametros.url}modEV/temaCONTROL/NuevoTemaMET" class="form floating-label" novalidate="novalidate">
<input type="hidden" name="valido" value="1" />
<div class="modal-body">
	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_tema" id="ind_tema">
		<label for="ind_tema"><i class="md md-create"></i> Tema de Evento </label>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				ind_tema:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modEV/temaCONTROL/VerificarTemaMET",
						type:"post"
					}
				}
			},
			messages:{
				ind_tema:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					if(dato['num_estatus']==1){
						var botonEstatus =  '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_tema_evento="'+dato['pk_num_tema_evento']+'" descripcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md-done"></i></button>'
					} else {
						var botonEstatus = '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_tema_evento="'+dato['pk_num_tema_evento']+'" descipcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md md-not-interested"></i></button>';
					}
                    if(dato['total']==0){
                        var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_tema_evento="'+dato['pk_num_tema_evento']+'"  boton="si, Eliminar" descipcion="El usuario ha eliminado un lugar evento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el lugar del evento?" title="Eliminar lugar de evento"><i class="md md-delete"></i></button>';
                    } else if(dato['total']>0){
                        var botonEliminar = '';
                    }
					$(document.getElementById('datatable1')).append('<tr id="pk_num_tema_evento'+dato['pk_num_tema_evento']+'"><td>'+dato['pk_num_tema_evento']+'</td><td>'+dato['ind_tema']+'</td><td align="center">'+botonEstatus+'</td><td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado un lugar" titulo="Modificar Lugar" title="Modificar Lugar" data-toggle="modal" data-target="#formModal" pk_num_tema_evento="'+dato['pk_num_tema_evento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td><td align="center">'+ botonEliminar +'</td></tr>');
					swal("Tema Guardado", "Tema guardado satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
</script>
