<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Control de Eventos.
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar eventos
class certificadosControlador extends Controlador
{
	private $atCertificadoModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atCertificadoModelo = $this->metCargarModelo('certificados');
	}

	// Método index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'dropzone//downloads/css/dropzone',
			'lightGallery-master/dist/css/lightgallery.min'
		);
		$complementosJs = array(
			'dropzone/downloads/dropzone.min',
			'lightGallery-master/dist/js/lightgallery.min',
			'lightGallery-master/dist/js/lg-thumbnail.min',
			'lightGallery-master/dist/js/lg-fullscreen.min'
		);
		$js = array(
			'materialSiace/core/demo/DemoTableDynamic'
		);
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$listarCertificado = $this->atCertificadoModelo->metListarCertificado(1);
		$this->atVista->assign('listarCertificado', $listarCertificado);
		$this->atVista->metRenderizar('listado');
	}

	//Método que permite registrar un nuevo certificado
	public function metGestionCertificado()
	{
		$certificado = $this->atCertificadoModelo->metlistarCertificado(1);
		$this->atVista->assign('cargarCertificado', $certificado);
		$this->atVista->metRenderizar('editar', 'modales');
	}

	// Método que permite cargar las imágenes del evento
	public function metCargarImagen()
	{
		$ultimo = $this->atCertificadoModelo->metlistarCertificado(2);
		if($ultimo['pk_num_certificado']!='') {
			$cuenta = $ultimo['pk_num_certificado'] + 1;
		} else {
			$cuenta = 1;
		}

		$path = ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'fondo';
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if (isset($_POST["cargarImagenes"]) && $_POST["cargarImagenes"] == 1) {
				$result = array();
				$files = scandir($path);
				if (false !== $files) {
					foreach ($files as $file) {
						if ('.' != $file && '..' != $file) {       //2
							$obj['name'] = $file;
							$obj['size'] = filesize($path .DS . $file);
							$result[] = $obj;
						}
					}
				}
				header('Content-type: text/json');              //3
				header('Content-type: application/json');
				echo json_encode($result);
			} else
				if (isset($_POST["delete"]) && $_POST["delete"] == true) {
					$name = $_POST["filename"];
					//echo $path . DS . $name;
					if (file_exists($path . DS . $name)) {
						unlink($path . DS . $name);
						echo json_encode(array("res" => true));
					} else {
						echo json_encode(array("res" => false));
					}
					$this->atCertificadoModelo->metEliminarCertificado($name);
				} else { // para guardar las imagenes
					//se obtienen los datos de la imagen
					$file = $_FILES["file"]["name"];
					if (!is_dir($path)) // si no es un directorio, entonces se crea
						mkdir($path, 0777);
					//se hace el renombrado de la imagen
					$path_parts = pathinfo("/$file");
					$extension = NULL;
					$extension = $path_parts['extension'];
					$nom_imagen = NULL;
					$nom_imagen = $cuenta . '.' . $extension;
					$fechaHora = date('Y-m-d H:i:s');
					$usuario = Session::metObtener('idUsuario');//1
					$this->atCertificadoModelo->metGuardarImagen($nom_imagen, $fechaHora , $usuario);
					if ($file && move_uploaded_file($_FILES["file"]["tmp_name"], $path .DS. $nom_imagen)) {

					}
				}
		}
	}

	// Método que permite listar certificados
	public function metConsultarCertificado()
	{
		$listarCertificado = $this->atCertificadoModelo->metListarCertificado(1);
		echo json_encode($listarCertificado);
	}

	// Método que permite cambiar el estatus de un certificado
	public function metCambiarEstatus()
	{
		$pkNumCertificado = $this->metObtenerInt('pk_num_certificado');
		$verCertificado = $this->atCertificadoModelo->metVerCertificado($pkNumCertificado);
		$estatus = $verCertificado['num_estatus'];
		if($estatus==1){
			$valor = 0;
		} else {
			$valor = 1;
		}
		$this->atCertificadoModelo->metCambiarEstatus($pkNumCertificado, $valor);
		$consultarCertificado = $this->atCertificadoModelo->metVerCertificado($pkNumCertificado);
		echo json_encode($consultarCertificado);
	}
}
?>
