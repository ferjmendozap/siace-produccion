<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Control de Eventos.
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar los lugares donde se realizan los eventos
class lugarControlador extends Controlador
{
	private $atLugarModelo;
	private $atPaisModelo;
	private $atEstadoModelo;
	private $atMunicipioModelo;
	private $atCiudadModelo;
	private $atParroquiaModelo;
	private $atSectorModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atLugarModelo = $this->metCargarModelo('lugar');
		$this->atPaisModelo   = $this->metCargarModelo('pais',false,'APP');
		$this->atEstadoModelo = $this->metCargarModelo('estado',false,'APP');
		$this->atMunicipioModelo = $this->metCargarModelo('municipio',false,'APP');
		$this->atCiudadModelo = $this->metCargarModelo('ciudad',false,'APP');
		$this->atParroquiaModelo = $this->metCargarModelo('parroquia',false,'APP');
		$this->atSectorModelo = $this->metCargarModelo('sector',false,'APP');
	}

	// Método index del controlador
	public function metIndex(){
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('listadoLugar', $this->atLugarModelo->metListarLugar());
		$this->atVista->metRenderizar('listado');
	}

	//Método que permite registrar un nuevo lugar
	public function metNuevoLugar()
	{
		$valido = $this->metObtenerInt('valido');
		$usuario = Session::metObtener('idUsuario');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$indLugarEvento = $this->metObtenerAlphaNumerico('ind_lugar_evento');
			$pkNumSector = $_POST['pk_num_sector'];
			$fechaHora = date('Y-m-d H:i:s');
			$pkNumLugarEvento = $this->atLugarModelo->metGuardarLugar($indLugarEvento, $fechaHora , $usuario, $pkNumSector);
			$listarLugar = $this->atLugarModelo->metConsultarLugar($pkNumLugarEvento);
			$arrayPost = array(
				'pk_num_lugar_evento' => $pkNumLugarEvento,
				'ind_lugar_evento' => $indLugarEvento,
				'num_estatus' => 1,
				'total' => $listarLugar['total']
			);
			echo json_encode($arrayPost);
			exit;
		}
		$form = array(
			'pk_num_lugar_evento' => null,
			'ind_lugar_evento' => null
		);
		$listarPais = $this->atLugarModelo->metVerUsuario($usuario);
		$pkNumPais= $listarPais['pk_num_pais'];
		$paisUsuario = array('pk_num_pais' => $pkNumPais);
		$this->atVista->assign('paisActual', $paisUsuario);
		$this->atVista->assign('form', $form);
		$pais = $this->atPaisModelo->metListarPais();
		$this->atVista->assign('pais', $pais);
		$listarEstado = $this->atEstadoModelo->metJsonEstado($pkNumPais);
		$this->atVista->assign('estado', $listarEstado);
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	//Método que permite editar un lugar
	public function metEditarLugar()
	{
		$pkNumLugarEvento = $this->metObtenerInt('pk_num_lugar_evento');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$ind_lugar_evento = $_POST['ind_lugar_evento'];
			$pkNumSector = $this->metObtenerInt('pk_num_sector');
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atLugarModelo->metEditarLugar($ind_lugar_evento, $fecha_hora, $usuario, $pkNumLugarEvento, $pkNumSector);
			$lugarEvento = $this->atLugarModelo->metVerLugar($pkNumLugarEvento);
            $listarLugar = $this->atLugarModelo->metConsultarLugar($pkNumLugarEvento);
			$arrayPost = array(
				'status' => 'modificar',
				'ind_lugar_evento' => $lugarEvento['ind_lugar_evento'],
				'pk_num_lugar_evento' => $pkNumLugarEvento,
				'num_estatus' => $lugarEvento['num_estatus'],
				'total' => $listarLugar['total']
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumLugarEvento)) {
			$pais = $this->atPaisModelo->metListarPais();
			$this->atVista->assign('pais', $pais);
			$listarLugar = $this->atLugarModelo->metVerLugar($pkNumLugarEvento);
			$pkNumSector = $listarLugar['fk_a013_num_sector'];
			$listarUbicacion = $this->atLugarModelo->metConsultarUbicacion($pkNumSector);
			$listarEstado = $this->atEstadoModelo->metJsonEstado($listarUbicacion['pk_num_pais']);
			$listarCiudad = $this->atCiudadModelo->metJsonCiudad($listarUbicacion['pk_num_estado']);
			$listarMunicipio = $this->atMunicipioModelo->metJsonMunicipio($listarUbicacion['pk_num_estado']);
			$listarParroquia = $this->atParroquiaModelo->metJsonParroquia($listarUbicacion['pk_num_municipio']);
			$listarSector = $this->atSectorModelo->metJsonSector($listarUbicacion['pk_num_parroquia']);
			$this->atVista->assign('ubicacion', $listarUbicacion);
			$this->atVista->assign('evento', $listarLugar);
			$this->atVista->assign('estado', $listarEstado);
			$this->atVista->assign('ciudad', $listarCiudad);
			$this->atVista->assign('municipio', $listarMunicipio);
			$this->atVista->assign('parroquia', $listarParroquia);
			$this->atVista->assign('sector', $listarSector);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que permite verificar la existencia de un lugar en el sistema
	public function metVerificarLugar(){
		$indLugarEvento = $this->metObtenerAlphaNumerico('ind_lugar_evento');
		$lugarExiste = $this->atLugarModelo->metBuscarLugar($indLugarEvento);
		echo json_encode(!$lugarExiste);
	}

	// Método que permite cambiar el estatus de un lugar
	public function metCambiarEstatus()
	{
		$pkNumLugarEvento = $this->metObtenerInt('pk_num_lugar_evento');
		$verLugar = $this->atLugarModelo->metVerLugar($pkNumLugarEvento);
		$estatus = $verLugar['num_estatus'];
		if($estatus==1){
			$valor = 0;
		} else {
			$valor = 1;
		}
		$this->atLugarModelo->metCambiarEstatus($pkNumLugarEvento, $valor);
		$consultarLugar = $this->atLugarModelo->metVerLugar($pkNumLugarEvento);
		echo json_encode($consultarLugar);
	}
	
	// Método que permite cargar selectores 
	public function metCargarSelect()
	{
		$valor = $_POST['valor'];
		switch($valor){
			case 1:
				$pkNumPais = $_POST['pk_num_pais'];
				$listarEstado = $this->atEstadoModelo->metJsonEstado($pkNumPais);
				$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_estado" id="pk_num_estado" onchange="cargarCiudad(this.value)"><option value="0">&nbsp;</option>';
				foreach($listarEstado as $estado){
					$a .='<option value="'.$estado['pk_num_estado'].'">'.$estado['ind_estado'].'</option>';
				}
				$a .= '</select><label for="pk_num_estado"><i class="glyphicon glyphicon-briefcase"></i> Estado</label></div>';
				echo $a;
				break;
			case 2:
				$pkNumEstado = $_POST['pk_num_estado'];
				$listarCiudad = $this->atCiudadModelo->metJsonCiudad($pkNumEstado);
				$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_ciudad" id="pk_num_ciudad"><option value="0">&nbsp;</option>';
				foreach($listarCiudad as $ciudad){
					$a .='<option value="'.$ciudad['pk_num_ciudad'].'">'.$ciudad['ind_ciudad'].'</option>';
				}
				$a .= '</select><label for="pk_num_ciudad"><i class="glyphicon glyphicon-briefcase"></i> Ciudad</label></div>';
				echo $a;
				break;
			case 3:
				$pkNumEstado = $_POST['pk_num_estado'];
				$listarMunicipio = $this->atMunicipioModelo->metJsonMunicipio($pkNumEstado);
				$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_municipio" id="pk_num_municipio" onchange="cargarParroquia(this.value)"><option value="0">&nbsp;</option>';
				foreach($listarMunicipio as $municipio){
					$a .='<option value="'.$municipio['pk_num_municipio'].'">'.$municipio['ind_municipio'].'</option>';
				}
				$a .= '</select><label for="pk_num_municipio"><i class="glyphicon glyphicon-briefcase"></i> Municipio</label></div>';
				echo $a;
				break;
			case 4:
				$pkNumMunicipio = $_POST['pk_num_municipio'];
				$listarParroquia = $this->atParroquiaModelo->metJsonParroquia($pkNumMunicipio);
				$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_parroquia" id="pk_num_parroquia" onchange="cargarSector(this.value)"><option value="0">&nbsp;</option>';
				foreach($listarParroquia as $parroquia){
					$a .='<option value="'.$parroquia['pk_num_parroquia'].'">'.$parroquia['ind_parroquia'].'</option>';
				}
				$a .= '</select><label for="pk_num_parroquia"><i class="glyphicon glyphicon-briefcase"></i> Parroquia</label></div>';
				echo $a;
				break;
			case 5:
				$pkNumParroquia = $_POST['pk_num_parroquia'];
				$listarSector = $this->atSectorModelo->metJsonSector($pkNumParroquia);
				$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_sector" id="pk_num_sector"><option value="0">&nbsp;</option>';
				foreach($listarSector as $sector){
					$a .='<option value="'.$sector['pk_num_sector'].'">'.$sector['ind_sector'].'</option>';
				}
				$a .= '</select><label for="pk_num_parroquia"><i class="glyphicon glyphicon-briefcase"></i> Sector</label></div>';
				echo $a;
				break;

		}
	}

	// Eliminar lugar
	public function metEliminarLugar()
	{
		$pkNumLugarEvento = $this->metObtenerInt('pk_num_lugar_evento');
		$this->atLugarModelo->metEliminarLugar($pkNumLugarEvento);
        $arrayPost = array(
            'status' => 'OK',
            'pk_num_lugar_evento' => $pkNumLugarEvento
        );
        echo json_encode($arrayPost);
	}
}
?>
