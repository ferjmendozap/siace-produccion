<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class claseVehiculoControlador extends Controlador
{
    private $atClaseVehiculoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atClaseVehiculoModelo = $this->metCargarModelo('claseVehiculo');
    }
	


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {

    }

    #Metodo parac cargar el listado de las diferentes clases de   vehículos
    public function metListarClaseVehiculo()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_ClasePost', $this->atClaseVehiculoModelo->metGetClaseVehiculo());
        $this->atVista->metRenderizar('ListarClaseVehiculo');
    }
	

    #Metodo para cargar el form registro clase de  vehiculo
    public function metRegistrarClaseVehiculo()
    {

        $idClase= $this->metObtenerInt('idClase');
        $valido=$this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');




        $complementosJs = array(

            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(

            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $this->metValidarToken();

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt])) {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = '';

                }
            }

            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    $validacion[$tituloInt] = '';
                }
            }

            if ($idClase === 0) {


                $validacion['status'] = 'creacion';
                $id = $this->atClaseVehiculoModelo->metRegistrarClase(
                    $validacion['cod_detalle'],
                    $validacion['clase'],
                    $validacion['idMaestro']
                );

                $validacion['idClase'] = $id;
                echo json_encode($validacion);
                exit;


            } else {


                $validacion['status'] = 'modificacion';

                $this->atClaseVehiculoModelo->metModificarClase($idClase,
                    $validacion['cod_detalle'],
                    $validacion['clase']
                );
                $validacion['idClase'] = $idClase;
                echo json_encode($validacion);
                exit;


            }
        }

        if($idClase!=0){
            $db = $this->atClaseVehiculoModelo->metMostrarClase($idClase);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->assign('idClase',$idClase);

        $this->atVista->metRenderizar('RegistrarClaseVehiculo',"modales");




    }

    #Metodo para eliminar  un  registro de clase vehiculo
    public function metEliminarClase()
    {
        $idClase = $this->metObtenerInt('idClase');
        $this->atClaseVehiculoModelo->metEliminarClase($idClase);
        $array = array(
            'status' => 'OK',
            'idClase' => $idClase
        );

        echo json_encode($array);
    }


}