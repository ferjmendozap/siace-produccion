<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class entradaTallerControlador extends Controlador
{
    private $atEntradaTallerModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEntradaTallerModelo = $this->metCargarModelo('entradaTaller');
        $this->atObligacionModelo = $this->metCargarModelo('obligacion', false, 'modCP');

    }
	



    public function metIndex()
    {


    }


    public function metListarEntradaTaller()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_EntradaTallerPost', $this->atEntradaTallerModelo->metGetEntradaTaller());
        $this->atVista->metRenderizar('ListadoEntradaTaller');
    }
	


    public function metRegistrarEntradaTaller()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }




            $hasta=$this->metFormatoFecha($_POST['entrada']);

            $hora_hasta=$validacion['hora'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $fecha=trim( $hasta." ".$hora_hasta.":00");
            if($validacion['Motivo']=='error'){
                $validacion['Motivo'] = '';
            }
            if($validacion['Observacion']=='error'){
                $validacion['Observacion'] = '';
            }


            $id = $this->atEntradaTallerModelo->metSetEntradaTaller($fecha,$validacion['Motivo'],$validacion['Kilometraje'],$validacion['Observacion'],$validacion['Pago'] ,$validacion['Vehiculo'],$validacion['Chofer'],$validacion['Taller']);
            $respuesta=$this->atEntradaTallerModelo->metMostrar( $id);


            $datos=[];
            $datos['idEntrada']=$id ;
            $datos['modelo']=$respuesta[0]['ind_modelo']." - ".$respuesta[0]['ind_placa'];
            $datos['taller']=$respuesta[0]['ind_nombre1'];
            $datos['motivo']=$respuesta[0]['ind_motivo'];

            echo json_encode($datos);
            exit;
        }



        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculoActivo());


        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_ChoferModelo = $this->metCargarModelo('chofer');
        $this->atVista->assign('_ChoferPost',$this->_ChoferModelo->metGetChofer());

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_TallerModelo = $this->metCargarModelo('taller');
        $this->atVista->assign('_TallerPost',$this->_TallerModelo->metGetTaller());


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarEntradaTaller',"modales");


    }


    public function metModificarEntradaTaller()
    {

        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "modificar",);

        $id_valor=$this->metObtenerInt('idPost');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );
        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $hasta=$this->metFormatoFecha($_POST['entrada']);

            $hora_hasta=$validacion['hora'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $fecha=trim( $hasta." ".$hora_hasta.":00");
            if($validacion['Motivo']=='error'){
                $validacion['Motivo'] = '';
            }
            if($validacion['Observacion']=='error'){
                $validacion['Observacion'] = '';
            }


            $VehiculoBD = $this->atEntradaTallerModelo->metGetFkVehiculo($id_valor);


            $this->atEntradaTallerModelo->metUpdateEntradaTaller($fecha,$validacion['Motivo'],$validacion['Kilometraje'],$validacion['Observacion'],$validacion['Pago'] ,$validacion['Chofer'],$validacion['Taller'],$id_valor,$validacion['Vehiculo'],$VehiculoBD[0]['fk_pab001_num_vehiculo']);

            $respuesta=$this->atEntradaTallerModelo->metMostrar($id_valor);

            $datos=[];
            $datos['idEntrada']=$id_valor;
            $datos['modelo']=$respuesta[0]['ind_modelo']." - ".$respuesta[0]['ind_placa'];
            $datos['taller']=$respuesta[0]['ind_nombre1'];
            $datos['motivo']=$respuesta[0]['ind_motivo'];

            echo json_encode($datos);
            exit;

        }
        $EntradaTallerPost=$this->atEntradaTallerModelo->metMostrar($id_valor);
        $this->atVista->assign('_EntradaTallerPost',$EntradaTallerPost);


        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());


        # Llamado al modelo  Solicitud  para la carga de select en el formulario.
        $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
        $this->atVista->assign('_SolicitudPost',$this->_SolicitudModelo->metMostrar($id_valor));


        # Llamado al modelo  Chofer para la carga de select en el formulario.
        $this->_ChoferModelo = $this->metCargarModelo('chofer');
        $this->atVista->assign('_ChoferPost',$this->_ChoferModelo->metGetChofer());

        # Llamado al modelo  Taller para la carga de select en el formulario.
        $this->_TallerModelo = $this->metCargarModelo('taller');
        $this->atVista->assign('_TallerPost',$this->_TallerModelo->metGetTaller());


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarEntradaTaller',"modales");

    }


    public function metAnularEntradaTaller()
    {
        $id_valor=$this->metObtenerInt('idPost');
        $consulta=$this->atEntradaTallerModelo->metGetFkVehiculo($id_valor);

        $this->atEntradaTallerModelo->metDeleteEntradaTaller($id_valor,$consulta[0]['fk_pab001_num_vehiculo']);


    }



    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }


    public function metPersona($persona, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atObligacionModelo->metListaPersona());
        $this->atVista->assign('listaPersona', $persona);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoPersona', 'modales');
    }

}