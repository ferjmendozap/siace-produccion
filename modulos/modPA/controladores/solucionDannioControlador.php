<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/

class solucionDannioControlador extends Controlador
{
    private $atSolucionDannioModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSolucionDannioModelo = $this->metCargarModelo('solucionDannio');
    }
	



    public function metIndex()
    {
    }


    public function metListarSolucionDannio()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',

            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);


        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_SolucionDannioPost', $this->atSolucionDannioModelo->metGetSolucionDannio());

        $this->atVista->metRenderizar('ListadoSolucionDannio');
    }
	


    public function metRegistrarSolucionDannio()
    {



        $Accion=array( "accion" => "registrar",);

        $valido=$this->metObtenerInt('valido');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $formMonto=$this->metObtenerMonto('form','monto');
            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

            $registro=$this->metFormatoFecha($_POST['entrada']);
            $registro=trim($registro." 00:00:00");

            $MontoPagado=str_replace(",",".",$validacion['MontoPagado']);




            $id= $this->atSolucionDannioModelo->metSetSolucionDannio($registro,$MontoPagado,$validacion['menu'],$validacion['ObservacionPago']);
            $respuesta=$this->atSolucionDannioModelo->metMostrar($id);
            $datos=[];
            $datos['idSolucionDanio']=$id;
            $datos['modelo']=$respuesta['ind_modelo']." - ".$respuesta['ind_placa'];
            $datos['fecha']=$respuesta['fec_solucion'];
            $datos['monto']=number_format($respuesta['num_monto'] , 2, ',', '.');

            echo json_encode($datos);
            exit;

        }



        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_DannioModelo = $this->metCargarModelo('dannio');
        $this->atVista->assign('_VehiculoPost',$this->_DannioModelo->metGetVehiculosConDannio());




        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_PiezaModelo = $this->metCargarModelo('piezaVehiculo');
        $this->atVista->assign('_PiezaPost',$this->_PiezaModelo->metGetPiezaVehiculo());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarSolucionDannio',"modales");


    }


    public function metModificarSolucionDannio()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "modificar",);



        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $formMonto=$this->metObtenerMonto('form','monto');
            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

            $registro=$this->metFormatoFecha($_POST['entrada']);
            $registro=trim($registro." 00:00:00");

            $MontoPagado=str_replace(",",".",$validacion['MontoPagado']);



            $this->atSolucionDannioModelo-> metUpdateSolucionDannio($validacion['ObservacionPago'],$registro,$MontoPagado,$validacion['menu'] ,$validacion['idSolucion']);
            $respuesta=$this->atSolucionDannioModelo->metMostrar($validacion['idSolucion']);

            $datos=[];
            $datos['idSolucionDanio']=$validacion['idSolucion'];
            $datos['modelo']=$respuesta['ind_modelo']." - ".$respuesta['ind_placa'];
            $datos['fecha']=$respuesta['fec_solucion'];
            $datos['monto']=number_format($respuesta['num_monto'] , 2, ',', '.');

            echo json_encode($datos);
            exit;

        }



        $consulta=$this->atSolucionDannioModelo->metMostrar($id_valor);

        $this->atVista->assign('_Solucion',$consulta);

        $this->_DannioModelo = $this->metCargarModelo('dannio');


        $Vehiculo_select=array( "vehiculo" => $consulta['fk_pab001_num_vehiculo']);
        $this->atVista->assign('_Vehiculo',$Vehiculo_select);




        $this->atVista->assign('_VehiculoPost',$this->atSolucionDannioModelo->metGetSolucionDannioModificar($consulta['fk_pab001_num_vehiculo'],$id_valor));
        $this->atVista->assign('_VehiculoPieza',$this->atSolucionDannioModelo->metGetSolucionDannioModificar($consulta['fk_pab001_num_vehiculo'],$id_valor));

        $this->atVista->metCargarJs($js);

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        

        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarSolucionDannio',"modales");





    }

    #Metodo para actualizar la clase cuando ocurra un cambio en un select
    public function metActualizarDannio()
    {

        $Vehiculo=$this->metObtenerInt('Vehiculo');



        $Vehiculo_select=array( "vehiculo" => $Vehiculo);

        $Accion=array( "accion" => "registrar",);


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_DannioModelo = $this->metCargarModelo('dannio');

        $this->atVista->assign('_VehiculoPost' ,$this->_DannioModelo->metGetVehiculosConDannio());
        $this->atVista->assign('_VehiculoPieza',$this->_DannioModelo->metGetDannioVehiculo($Vehiculo));



        $this->atVista->assign('_Vehiculo',$Vehiculo_select);

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.



        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarSolucionDannio',"modales");

    }


    #Metodo para eliminar  un  registro de marca vehiculo
    public function metAnularSolucionDannio()
    {

        $id_valor=$this->metObtenerInt('idPost');
        $consulta=$this->atSolucionDannioModelo->metGetFkDannio($id_valor);


        if ($consulta) {
            for ($i = 0; $i < count($consulta); $i++) {
                $dbDannio[] = $consulta[$i]['fk_pae004_num_averia_vehiculo'];
            }
        } else {
            $dbDannio = null;
        }



        $this->atSolucionDannioModelo->metDeleteSolucionDannio($id_valor,$dbDannio);

    }

    protected function metObtenerMonto($clave,$posicion=false)
    {
        if((isset($_POST[$clave]) && !empty($_POST[$clave]))){
            if(is_array($_POST[$clave])){
                if($posicion) {
                    if (isset($_POST[$clave][$posicion]) && !empty($_POST[$clave][$posicion])) {
                        foreach ($_POST[$clave][$posicion] as $titulo => $valor) {
                            if(is_array($_POST[$clave][$posicion][$titulo])){
                                foreach($_POST[$clave][$posicion][$titulo] as $titulo1 => $valor1){
                                    $_POST[$clave][$posicion][$titulo][$titulo1] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo][$titulo1]));
                                }
                            }else{
                                if(isset($_POST[$clave][$posicion][$titulo]) && !empty($_POST[$clave][$posicion][$titulo])) {
                                    $_POST[$clave][$posicion][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo]));
                                }
                            }
                        }
                        return $_POST[$clave][$posicion];
                    }
                }else{
                    if (isset($_POST[$clave])) {
                        foreach ($_POST[$clave] as $titulo => $valor) {
                            $_POST[$clave][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$titulo]));
                        }
                        return $_POST[$clave];
                    }
                }
            }else{
                $_POST[$clave] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave]));
                return $_POST[$clave];
            }
        }
    }

    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }
}