<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class tallerControlador extends Controlador
{
    private $atTallerModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTallerModelo = $this->metCargarModelo('taller');
    }



    public function metIndex()
    {

    }


    public function metListarTaller()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );
        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('_TallerPost', $this->atTallerModelo->metGetTaller());
        $this->atVista->metRenderizar('ListarTaller');
    }



    public function metRegistrarTaller()
    {



        $valido = $this->metObtenerInt('valido');



        $Accion = array("accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if ($valido == 1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

            $id = $this->atTallerModelo->metSetTaller($validacion['Nombre'],$validacion['Documento'],$validacion['Direccion'],$validacion['Ciudad']);
            $this->atVista->metRenderizar('RegistrarTaller');
            echo json_encode($id);

        }
        $this->atVista->assign('_CiudadPost', $this->atTallerModelo->metGetCiudad());


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);


        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarTaller', "modales");


    }


    public function metModificarTaller()
    {


        $valido = $this->metObtenerInt('valido');
        $id_valor = $this->metObtenerInt('idPost');

        $Accion = array("accion" => "modificar");


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if ($valido == 1) {
            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $id = $this->atTallerModelo->metUpdateTaller($validacion['Nombre'],$validacion['Documento'],$validacion['Direccion'],$validacion['Ciudad'],$id_valor);
            $this->atVista->metRenderizar('RegistrarTaller');
            echo json_encode($id);

        }

        $this->atVista->assign('_CiudadPost', $this->atTallerModelo->metGetCiudad());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);


        $this->atVista->assign('_TallerPost', $this->atTallerModelo->metMostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarTaller', "modales");

    }


    public function metEliminarTaller()
    {

        $id_valor = $this->metObtenerInt('idPost');

        $this->atTallerModelo->metDeleteTaller($id_valor);


    }




}