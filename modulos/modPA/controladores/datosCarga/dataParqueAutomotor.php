<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          José Pereda                      |                                    |         0424-8040078           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-05-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

trait dataParqueAutomotor
{



    public function metDataTipoVehiculo(){
        $pa_b009_tipo_vehiculo = array(
            array('ind_descripcion' => 'Sedán', 'fk_a006_num_clase_vehiculo' => '1', 'fk_a018_num_seguridad_usuario' => '1', 'fec_ultima_modificacion' => '2016-03-07 15:06:28'),
            array('ind_descripcion' => ' X10', 'fk_a006_num_clase_vehiculo' => '2', 'fk_a018_num_seguridad_usuario' => '1', 'fec_ultima_modificacion' => '2016-01-14 15:23:41'),
            array('ind_descripcion' => 'SUV', 'fk_a006_num_clase_vehiculo' => '3', 'fk_a018_num_seguridad_usuario' => '1', 'fec_ultima_modificacion' => '2016-03-04 16:19:53')
        );
        return $pa_b009_tipo_vehiculo;
    }



}
?>