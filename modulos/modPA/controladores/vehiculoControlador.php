<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class vehiculoControlador extends Controlador
{
    private $atVehiculoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atVehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atObligacionModelo = $this->metCargarModelo('obligacion', false, 'modCP');
    }
	



    public function metIndex()
    {

    }


    public function metListarVehiculo()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);



        $this->atVista->assign('_VehiculoPost', $this->atVehiculoModelo->metGetVehiculo());
        $this->atVista->metRenderizar('ListarVehiculo');
    }
	


    public function metRegistrarVehiculo()
    {

        $RutaImagenes = array("ruta" => "publico/imagenes/modPA/" );
        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {

            #Recibiendo las variables del formulario que utiliza la clase vehículo

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');

            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }


            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

            $desde=$this->metFormatoFecha($_POST['desde']);

            $fechadesde=trim($desde);

            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $fechahasta=trim($hasta);


            $id = $this->atVehiculoModelo->metSetVehiculo($validacion['Annio'],$validacion['Placa'],$validacion['SerialMotor'],$validacion['SerialCarroceria'],$validacion['Color'],$validacion['Modelo'],$validacion['TipoVehiculo'],$validacion['MarcaVehiculo'],$validacion['ClaseVehiculo'],$validacion['VLD'],$validacion['VLI'],$validacion['VF'],$validacion['VT'],$validacion['VS'],$validacion['VI']);

            # Llamado al modelo  poliza
            $this->_PolizaModelo = $this->metCargarModelo('poliza');
            # registramos  la póliza y la realcionamos con el vehículo

            $this->_PolizaModelo->metSetPoliza($validacion['NumeroPoliza'],$validacion['NumeroSeguro'],$fechadesde,$fechahasta,"1",str_replace(",",".",$validacion['MontoPagado']),str_replace(",",".",$validacion['MontoCobertura']),$id,$validacion['Aseguradora']);

            $datos=[];
            $datos['idVehiculo']=$id;
            $datos['modelo']=$validacion['Modelo'];
            $datos['placa']=$validacion['Placa'];
            $datos['anio']=$validacion['Annio'];
            echo json_encode($datos);
            exit;


        }

        # Llamado al modelo claseVehiculo para la carga de select en el formulario.
        $this->_ClaseModelo = $this->metCargarModelo('claseVehiculo');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_ClaseVehiculoPost', $this->_ClaseModelo->metGetClaseVehiculo());

        # Llamado al modelo tipoVehiculo para la carga de select en el formulario.
        $this->_TipoModelo = $this->metCargarModelo('tipoVehiculo');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_TipoVehiculoPost',  $this->_TipoModelo->metGetTipoVehiculo());

        # Llamado al modelo marcaVehiculo para la carga de select en el formulario.
        $this->_MarcaModelo = $this->metCargarModelo('marca');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_MarcaVehiculoPost',  $this->_MarcaModelo->metGetMarcas());


        $this->_TallerModelo = $this->metCargarModelo('taller');
        $this->atVista->assign('_TallerPost',$this->_TallerModelo->metGetTaller());


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        


        $this->atVista->assign('_VehiculoBD',"");
        $this->atVista->assign('_RutaModuloImagen',$RutaImagenes);
        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarVehiculo',"modales");




    }


    public function metModificarVehiculo()
    {

        $valido=$this->metObtenerInt('valido');
 
		$RutaImagenes = array("ruta" => "publico/imagenes/modPA/" );
        $Valores = array("ind_marca" => " " );
        $Accion=array( "accion" => "modificar",);

        $id_valor=$this->metObtenerInt('idPost');


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {


            $formMonto=$this->metObtenerMonto('form','monto');
            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            $desde=$this->metFormatoFecha($_POST['desde']);

            $fechadesde=trim($desde);

            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $fechahasta=trim($hasta);


            $this->atVehiculoModelo->metUpdateVehiculo($validacion['Annio'],$validacion['Placa'],$validacion['SerialMotor'],$validacion['SerialCarroceria'],$validacion['Color'],$validacion['Modelo'],$validacion['TipoVehiculo'],$validacion['MarcaVehiculo'],$validacion['ClaseVehiculo'],$validacion['Vehiculo']
			,$validacion['VLD'],$validacion['VLI'],$validacion['VF'],$validacion['VT'],$validacion['VS'],$validacion['VI']);

            $this->_PolizaModelo = $this->metCargarModelo('poliza');
            if($validacion['Poliza']!="error") {

                $this->_PolizaModelo->metUpdatePoliza($validacion['NumeroPoliza'], $validacion['NumeroSeguro'], $fechadesde, $fechahasta, "1", str_replace(",",".",$validacion['MontoPagado']),str_replace(",",".",$validacion['MontoCobertura']), $validacion['Vehiculo'], $validacion['Aseguradora'], $validacion['Poliza']);
            }else{

                $this->_PolizaModelo->metSetPoliza($validacion['NumeroPoliza'],$validacion['NumeroSeguro'],$fechadesde,$fechahasta,"1",str_replace(",",".",$validacion['MontoPagado']),str_replace(",",".",$validacion['MontoCobertura']),$validacion['Vehiculo'] ,$validacion['Aseguradora']);
            }


            $datos=[];
            $datos['idVehiculo']=$validacion['Vehiculo'];
            $datos['modelo']=$validacion['Modelo'];
            $datos['placa']=$validacion['Placa'];
            $datos['anio']=$validacion['Annio'];
            echo json_encode($datos);
            exit;



        }


        $Vehiculo=$this->atVehiculoModelo->metMostrar($id_valor);

        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_VehiculoBD',$Vehiculo);

        # Llamado al modelo claseVehiculo para la carga de select en el formulario.
        $this->_ClaseModelo = $this->metCargarModelo('claseVehiculo');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_ClaseVehiculoPost', $this->_ClaseModelo->metGetClaseVehiculo());

        # Llamado al modelo tipoVehiculo para la carga de select en el formulario.
        $this->_TipoModelo = $this->metCargarModelo('tipoVehiculo');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_TipoVehiculoPost',  $this->_TipoModelo->metGetTipoVehiculo());

        # Llamado al modelo marcaVehiculo para la carga de select en el formulario.
        $this->_MarcaModelo = $this->metCargarModelo('marca');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_MarcaVehiculoPost',  $this->_MarcaModelo->metGetMarcas());

        $this->_TallerModelo = $this->metCargarModelo('taller');
        $this->atVista->assign('_TallerPost',$this->_TallerModelo->metGetTaller());


        # Llamado al modelo  poliza
        $this->_PolizaModelo = $this->metCargarModelo('poliza');

        $this->atVista->assign('_PolizaPost', $this->_PolizaModelo->metMostrarVinculacion($id_valor));

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
		 
		$this->atVista->assign('_RutaModuloImagen',$RutaImagenes);
        
        $this->atVista->assign('_VehiculoPost',$Valores);
        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarVehiculo',"modales");


    }


    public function metEliminarVehiculo()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $this->atVehiculoModelo->metDeleteVehiculo($id_valor);




    }

    public function metActualizarClase()
    {

        $ClaseVehiculo=$this->metObtenerAlphaNumerico('ClaseVehiculo');

        # Llamado al modelo tipoVehiculo para la carga de select en el formulario.
        $this->_TipoModelo = $this->metCargarModelo('tipoVehiculo');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $consulta= $this->_TipoModelo->metMostrarSelect($ClaseVehiculo);
        $resultado="";
        for($i=0;$i<sizeof($consulta);$i++){
        $resultado.= "<option value=".$consulta[$i]['pk_num_tipo_vehiculo'].">".$consulta[$i]['ind_descripcion']."</option>";
        }



        echo json_encode($resultado);
        exit;




    }


    public function metSubirFoto()
    { 

        $carpeta=$this->metObtenerTexto('Carpeta');
        $vista=$this->metObtenerTexto('Vista');

        $destino =  "publico/imagenes/modPA/";


        if (!file_exists($destino.$carpeta)) {
            chmod("publico/", 0777);
            chmod("publico/imagenes/", 0777);
            chmod($destino, 0777);
            mkdir($destino.$carpeta,0777,TRUE);
        }

        move_uploaded_file($_FILES['files-0'][tmp_name],$destino.$carpeta.'/'.$_FILES['files-0'][name]);
        chmod($destino.$carpeta.'/'.$vista.".".$tipo[1], 0777);
      exit;
    }


    protected function metObtenerMonto($clave,$posicion=false)
    {
        if((isset($_POST[$clave]) && !empty($_POST[$clave]))){
            if(is_array($_POST[$clave])){
                if($posicion) {
                    if (isset($_POST[$clave][$posicion]) && !empty($_POST[$clave][$posicion])) {
                        foreach ($_POST[$clave][$posicion] as $titulo => $valor) {
                            if(is_array($_POST[$clave][$posicion][$titulo])){
                                foreach($_POST[$clave][$posicion][$titulo] as $titulo1 => $valor1){
                                    $_POST[$clave][$posicion][$titulo][$titulo1] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo][$titulo1]));
                                }
                            }else{
                                if(isset($_POST[$clave][$posicion][$titulo]) && !empty($_POST[$clave][$posicion][$titulo])) {
                                    $_POST[$clave][$posicion][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo]));
                                }
                            }
                        }
                        return $_POST[$clave][$posicion];
                    }
                }else{
                    if (isset($_POST[$clave])) {
                        foreach ($_POST[$clave] as $titulo => $valor) {
                            $_POST[$clave][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$titulo]));
                        }
                        return $_POST[$clave];
                    }
                }
            }else{
                $_POST[$clave] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave]));
                return $_POST[$clave];
            }
        }
    }


    public function metVerificarPlaca()
    {
        $placa=$_POST['Placa'];

        $respuesta=$this->atVehiculoModelo->metBuscarPlaca($placa);

        echo json_encode($respuesta);
    }



    

    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }
    public function metPersona($persona, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atObligacionModelo->metListaPersona());
        $this->atVista->assign('listaPersona', $persona);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoPersona', 'modales');
    }

    public function metProveedor($proveedor, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atVehiculoModelo->metListarProveedor());
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->assign('proveedor', $proveedor);
        $this->atVista->metRenderizar('proveedores','modales');
    }
}