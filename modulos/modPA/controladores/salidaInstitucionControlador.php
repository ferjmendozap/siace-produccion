<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class salidaInstitucionControlador extends Controlador
{
    private $atSalidaInstitucionModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSalidaInstitucionModelo = $this->metCargarModelo('salidaInstitucion');
    }



    #Metodo Index del controlador
    public function metIndex()
    {

    }

    public function metListarSalidaInstitucion()
    {


        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_SalidaInstitucionPost', $this->atSalidaInstitucionModelo->metGetSalidaInstitucion());
        $this->atVista->metRenderizar('ListadoSalidaInstitucion');

    }


    #Metodo para cargar el form registro  de tipo de  vehiculo
    public function metRegistrarSalidaInstitucion()
    {



        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "registrar",);



        $js = array(
            'materialSiace/core/demo/DemoFormWizard',


        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");

            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim( $hasta." ".$hora_hasta.":00");



            $estatus="6";

            if(!isset($validacion['menu']) ){

                $validacion['menu'] = "";

            }
            if($validacion['ObservacionSalida']=="error" ){

                $validacion['ObservacionSalida'] = NULL;

            }


            $id=$this->atSalidaInstitucionModelo->metSetSalidaInstitucion($desde,$hasta,$validacion['kilometraje'],$validacion['ObservacionSalida'],$validacion['idSolicitud'],$validacion['Chofer'],$validacion['Vehiculo'],$estatus,$validacion['menu'],$validacion['Gasolina']);

            $respuesta=$this->atSalidaInstitucionModelo->metMostrar($id );

            $datos=[];
            $datos['idSalida']=$id;
            $datos['modelo']=$respuesta[0]['ind_modelo']." - ".$respuesta[0]['ind_placa'];
            $datos['salida']=$respuesta[0]['fec_hora_salida'];
            $datos['hora']=$respuesta[0]['hora_salida'] ;
            $datos['observacion']=$validacion['ObservacionSalida'];
            echo json_encode($datos);
            exit;


        }else {

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
            $this->atVista->assign('_SolicitudesPost', $this->_SolicitudModelo->metGetSolicitudes('5'));

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
            $this->atVista->assign('_VehiculoPost', $this->_VehiculoModelo->metGetVehiculo());

            $this->atVista->assign('_TanqueGasolina', $this->atSalidaInstitucionModelo->metObtenerGasolina());
            $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
            $this->atVista->assign('_TipoSalidaPost', $this->_TipoSalidaModelo->metGetTipoSalida());

            $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
            $this->atVista->assign('_MotivoSalidaPost', $this->_MotivoSalidaModelo->metGetMotivoSalida());

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_AccesorioModelo = $this->metCargarModelo('accesorio');
            $this->atVista->assign('_AccesorioPost', $this->_AccesorioModelo->metGetAccesorio());

            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarCssComplemento($complementosCss);


            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->metRenderizar('RegistrarSalidaInstitucion', "modales");
        }

    }


    #Metodo para cargar el form para modificar una tipo de vehiculo
    public function metActualizarDatos()
    {

        $id_valor=$this->metObtenerInt('idSolicitud');


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($this->metObtenerAlphaNumerico('accion')=="registrar") {

            $Accion=array( "accion" => "registrar",);


            $this->_SolicitudModelo = $this->metCargarModelo('solicitud');

            $consulta=$this->_SolicitudModelo->metGetSolicitudes('5');

            $this->atVista->assign('_SolicitudesPost',$consulta  );

            $this->atVista->assign('_VehiculoAsignado', $consulta[0]['fk_pab001_num_vehiculo_solicitud']);

            $consulta = $this->_SolicitudModelo->metMostrar($id_valor);
            $this->atVista->assign('_SolicitudPost', $consulta);


            $this->_AccesorioModelo = $this->metCargarModelo('accesorio');
            $this->atVista->assign('_AccesorioPost',$this->_AccesorioModelo->metGetAccesorio());
            $dbAccesorio=$this->_AccesorioModelo->metMostrarSalida($consulta[0]['pk_num_salida_vehiculo']);




        }else{

            $Accion=array( "accion" => "modificar",);
            # Llamado al modelo  salidaInstitucion para la carga de select en el formulario.
            $this->_SalidaInstitucionModelo = $this->metCargarModelo('salidaInstitucion');

            $this->atVista->assign('_SolicitudesPost',$this->_SalidaInstitucionModelo->metGetSalidaInstitucion());




            $consulta = $this->_SalidaInstitucionModelo->metGetPkSalida($id_valor);



            $this->atVista->assign('_SolicitudPost',$consulta);


            $this->atVista->assign('_VehiculoAsignado', $consulta[0]['fk_pab001_num_vehiculo']);

            # Llamado al modelo  accesorio para la carga de select en el formulario.
            $this->_AccesorioModelo = $this->metCargarModelo('accesorio');
            $this->atVista->assign('_AccesorioPost',$this->_AccesorioModelo->metGetAccesorio());

            $dbAccesorio=$this->_AccesorioModelo->metMostrarSalida($consulta[0]['pk_num_salida_vehiculo']);
        }



        if($dbAccesorio) {
            for ($i = 0; $i < count($dbAccesorio); $i++) {
                $dbAccesorios[] = $dbAccesorio[$i]['fk_pab011_num_accesorio'];
            }
        }else{
            $dbAccesorios=null;
        }
        $this->atVista->assign('_TanqueGasolina', $this->atSalidaInstitucionModelo->metObtenerGasolina());

        $this->atVista->assign('_AccesorioSelectPost',$dbAccesorios);

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());

        # Llamado al modelo  Chofer para la carga de select en el formulario.
        $this->_ChoferModelo = $this->metCargarModelo('chofer');
        $this->atVista->assign('_ChoferPost',$this->_ChoferModelo->metGetChofer());


        $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
        $this->atVista->assign('_TipoSalidaPost',$this->_TipoSalidaModelo->metGetTipoSalida());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo->metGetMotivoSalida());





        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);


        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarSalidaInstitucion',"modales");


    }


    #Metodo para cargar el form para modificar una tipo de vehiculo
    public function metModificarSalidaInstitucion()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "modificar",);

        $id_valor=$this->metObtenerInt('idPost');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");

            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim( $hasta." ".$hora_hasta.":00");



            $estatus="6";

            if($validacion['ObservacionSalida']=="error" ){

                $validacion['ObservacionSalida'] = NULL;

            }

            if(!isset($validacion['menu']) ){

                $validacion['menu'] = "";

            }
            $this->atSalidaInstitucionModelo->metUpdateSalidaInstitucion($desde,$hasta,$validacion['kilometraje'],$validacion['ObservacionSalida'],$validacion['idSolicitud'],$validacion['Chofer'],$validacion['Vehiculo'],$estatus,$validacion['menu'],$validacion['idSalida'],$validacion['Gasolina']);
            $respuesta=$this->atSalidaInstitucionModelo->metMostrar($validacion['idSalida']);

            $datos=[];
            $datos['idSalida']=$validacion['idSalida'];
            $datos['modelo']=$respuesta[0]['ind_modelo']." - ".$respuesta[0]['ind_placa'];
            $datos['salida']=$respuesta[0]['fec_hora_salida'];
            $datos['hora']=$respuesta[0]['hora_salida'] ;
            $datos['observacion']=$validacion['ObservacionSalida'];
            echo json_encode($datos);
            exit;

        }else {


            # Llamado al modelo  Vehiculo para la carga de select en el formulario.metGetSalidaInstitucion()
            $this->_SalidaInstitucionModelo = $this->metCargarModelo('salidaInstitucion');
            $this->atVista->assign('_SolicitudesPost', $this->_SalidaInstitucionModelo->metGetSalidaInstitucion());

            $this->atVista->assign('_TanqueGasolina', $this->atSalidaInstitucionModelo->metObtenerGasolina());
            $consulta = $this->_SalidaInstitucionModelo->metMostrar($id_valor);

            $this->atVista->assign('_SolicitudPost', $consulta);
            $this->atVista->assign('_CombustibleSalida', $consulta[0]['fk_a006_miscelaneo_gasolina_sal']);

            $this->atVista->assign('_VehiculoAsignado', $consulta[0]['fk_pab001_num_vehiculo']);

            # Llamado al modelo  accesorio para la carga de select en el formulario.
            $this->_AccesorioModelo = $this->metCargarModelo('accesorio');

            $this->atVista->assign('_AccesorioPost', $this->_AccesorioModelo->metGetAccesorio());


            $dbAccesorio = $this->_AccesorioModelo->metMostrarSalida($consulta[0]['pk_num_salida_vehiculo']);

            if ($dbAccesorio) {
                for ($i = 0; $i < count($dbAccesorio); $i++) {
                    $dbAccesorios[] = $dbAccesorio[$i]['fk_pab011_num_accesorio'];
                }
            } else {
                $dbAccesorios = null;
            }

            $this->atVista->assign('_AccesorioSelectPost', $dbAccesorios);

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
            $this->atVista->assign('_VehiculoPost', $this->_VehiculoModelo->metGetVehiculo());

            # Llamado al modelo  chofer para la carga de select en el formulario.
            $this->_ChoferModelo = $this->metCargarModelo('chofer');
            $this->atVista->assign('_ChoferPost', $this->_ChoferModelo->metGetChofer());


            $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
            $this->atVista->assign('_TipoSalidaPost', $this->_TipoSalidaModelo->metGetTipoSalida());

            $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
            $this->atVista->assign('_MotivoSalidaPost', $this->_MotivoSalidaModelo->metGetMotivoSalida());


            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarCssComplemento($complementosCss);


            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->metRenderizar('RegistrarSalidaInstitucion', "modales");
        }

    }

    #Metodo para eliminar  un  registro de marca vehiculo
    public function metAnularSalidaInstitucion()
    {

        $id_valor=$this->metObtenerInt('idPost');
        $consulta=$this->atSalidaInstitucionModelo->metGetFkSolicitud($id_valor);

        $this->atSalidaInstitucionModelo->metDeleteSalidaInstitucion($id_valor,$consulta[0]['fk_pad001_num_solicitud_vehiculo']);




    }

    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }
}