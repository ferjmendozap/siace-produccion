<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * | 2 |    Marcos Jose Gonzalez Marcano        |dt.ait.programador2@cgesucre.gob.ve  |         04141909537                         |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class choferControlador extends Controlador
{
    private $atChoferModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atChoferModelo = $this->metCargarModelo('chofer');
        $this->atObligacionModelo = $this->metCargarModelo('obligacion', false, 'modCP');
    }
	


    #Metodo Index del controlador Chofer.
    public function metIndex()
    {
    }

    #Metodo parac cargar el listado de los Choferes de los vehículos
    public function metListarChofer()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('_ChoferPost', $this->atChoferModelo->metGetChofer());
        $this->atVista->metRenderizar('ListarChofer');
    }
	

    #Metodo para cargar el form registro Chofer vehiculo
    public function metRegistrarChofer()
    {


        $valido=$this->metObtenerInt('valido');


         
        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {
            $Observacion=$this->metObtenerAlphaNumerico('ChoferObservacion');
            $fk_empleado=$this->metObtenerAlphaNumerico('idEmpleado');
            $id = $this->atChoferModelo->metSetChofer($Observacion,$fk_empleado);
            $funcionario = $this->atChoferModelo->metObtenerFuncionario($fk_empleado);

            if(is_array($id)){

                $validacion=[];
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;

            }else{
                
                $datos=[];
                $datos['idChofer']=$id;
                $datos['nombre']=$funcionario[0]['ind_nombre1']." ".$funcionario[0]['ind_apellido1'];
                echo json_encode($datos);
                exit;


            }






        }

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        
        $this->atVista->assign('_ChoferPost', $this->atChoferModelo->metGetPersonas());
        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarChofer',"modales");




    }

    #Metodo para cargar el form modificar Chofer vehiculo
    public function metModificarChofer()
    {


        $valido=$this->metObtenerInt('valido');
        $id_valor=$this->metObtenerInt('idPost');

        $Accion=array( "accion" => "modificar");


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {
            $Observacion=$this->metObtenerAlphaNumerico('ChoferObservacion');
            $fk_empleado=$this->metObtenerAlphaNumerico('idEmpleado');


            $this->atChoferModelo->metUpdateChofer($Observacion,$fk_empleado,$id_valor);
            $funcionario = $this->atChoferModelo->metObtenerFuncionario($fk_empleado);
            $datos=[];
            $datos['idChofer']=$id_valor;
            $datos['nombre']=$funcionario[0]['ind_nombre1']." ".$funcionario[0]['ind_apellido1'];
            echo json_encode($datos);
            exit;
        }

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_ChoferBD',$this->atChoferModelo->metMostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarChofer',"modales");



    }


    public function metEliminarChofer()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $this->atChoferModelo->metDeleteChofer($id_valor);




    }

    public function metPersona($persona, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atChoferModelo->metGetPersonas());
        $this->atVista->assign('listaPersona', $persona);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoPersona', 'modales');
    }



}