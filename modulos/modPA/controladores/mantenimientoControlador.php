<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class mantenimientoControlador extends Controlador
{
    private $atMantenimientoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atMantenimientoModelo = $this->metCargarModelo('mantenimiento');

    }
	


    #Metodo Index del controlador Mantenimiento.
    public function metIndex()
    {

    }

    #Metodo parac cargar el listado de las Mantenimiento
    public function metListarMantenimiento()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);



        $this->atVista->assign('_MantenimientoPost', $this->atMantenimientoModelo->metGetMantenimiento());
        $this->atVista->metRenderizar('ListadoMantenimiento');
    }


    #Funcion para redireccionar a un lugar dentro de la aplicacion



    public function metRegistrarMantenimiento()
    {


        $valido=$this->metObtenerInt('valido');




        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            #Recibiendo las variables del formulario que utiliza la clase mantenimiento
            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');

            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }


            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            if($validacion['observacion']=='error') {
                $validacion['observacion'] = '';
            }
            $validacion['mantenimiento']=$this->metFormatoFecha($_POST['mantenimiento']);

            $validacion['mantenimiento']=trim( $validacion['mantenimiento']." 00:00:00");


            $validacion['prox_mantenimiento']=$this->metFormatoFecha($_POST['prox_mantenimiento']);


            $validacion['prox_mantenimiento']=trim( $validacion['prox_mantenimiento']." 00:00:00");

            $validacion['MontoPagado']=str_replace(",",".",$validacion['MontoPagado']);
            $id = $this->atMantenimientoModelo->metSetMantenimiento($validacion['ocasion'],$validacion['mantenimiento'],$validacion['prox_mantenimiento'],$validacion['kilometraje'],$validacion['observacion'],$validacion['TipoMantenimiento'],$validacion['Vehiculo'],$validacion['MontoPagado']);
            $respuesta=$this->atMantenimientoModelo->metMostrar($id);

            $datos=[];
            $datos['fecha']=$respuesta[0]['fec_mant'];
            $datos['idMantenimiento']=$id;
            $datos['modelo']=$respuesta[0]['ind_modelo']." - ".$respuesta[0]['ind_placa'];
            $datos['ocasion']=$respuesta[0]['ind_en_ocacion'];
            $datos['observacion']=$respuesta[0]['ind_observacion'] ;
            echo json_encode($datos);
            exit;





        }
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());

        $this->_tipoMantenimientoModelo = $this->metCargarModelo('tipoMantenimiento');
        $this->atVista->assign('_tipoMantenimientoPost',$this->_tipoMantenimientoModelo->metGetTipoMantenimiento());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        
        //$this->atVista->assign('_MantenimientoPost',$this->atMantenimientoModelo->metMostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarMantenimiento',"modales");



    }



    public function metModificarMantenimiento()
    {



        $valido=$this->metObtenerInt('valido');
        $id_valor=$this->metObtenerInt('idPost');

        $Accion=array( "accion" => "modificar");



        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {

            #Recibiendo las variables del formulario que utiliza la clase mantenimiento
            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            if($validacion['observacion']=='error') {
                $validacion['observacion'] = '';
            }
            $validacion['mantenimiento']=$this->metFormatoFecha($_POST['mantenimiento']);

            $validacion['mantenimiento']=trim( $validacion['mantenimiento']." 00:00:00");


            $validacion['prox_mantenimiento']=$this->metFormatoFecha($_POST['prox_mantenimiento']);


            $validacion['prox_mantenimiento']=trim( $validacion['prox_mantenimiento']." 00:00:00");
            $validacion['MontoPagado']=str_replace(",",".",$validacion['MontoPagado']);

            $this->atMantenimientoModelo->metUpdateMantenimiento($validacion['ocasion'],$validacion['mantenimiento'],$validacion['prox_mantenimiento'],$validacion['kilometraje'],$validacion['observacion'],$validacion['TipoMantenimiento'],$validacion['Vehiculo'],$validacion['MontoPagado'],$validacion['idPost']);
            $respuesta=$this->atMantenimientoModelo->metMostrar($validacion['idPost']);

            $datos=[];
            $datos['fecha']=$respuesta[0]['fec_mant'];
            $datos['idMantenimiento']=$validacion['idPost'];
            $datos['modelo']=$respuesta[0]['ind_modelo']." - ".$respuesta[0]['ind_placa'];
            $datos['ocasion']=$respuesta[0]['ind_en_ocacion'];
            $datos['observacion']=$respuesta[0]['ind_observacion'] ;
            echo json_encode($datos);
            exit;


        }



        $this->atVista->assign('_MantenimientoPost',$this->atMantenimientoModelo->metMostrar($id_valor));

        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());

        $this->_tipoMantenimientoModelo = $this->metCargarModelo('tipoMantenimiento');
        $this->atVista->assign('_tipoMantenimientoPost',$this->_tipoMantenimientoModelo->metGetTipoMantenimiento());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        
        $this->atVista->assign('_MantenimientoPost',$this->atMantenimientoModelo->metMostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarMantenimiento',"modales");



    }


    public function metAnularMantenimiento()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $this->atMantenimientoModelo->metDeleteMantenimiento($id_valor);




    }
    protected function metObtenerMonto($clave,$posicion=false)
    {
        if((isset($_POST[$clave]) && !empty($_POST[$clave]))){
            if(is_array($_POST[$clave])){
                if($posicion) {
                    if (isset($_POST[$clave][$posicion]) && !empty($_POST[$clave][$posicion])) {
                        foreach ($_POST[$clave][$posicion] as $titulo => $valor) {
                            if(is_array($_POST[$clave][$posicion][$titulo])){
                                foreach($_POST[$clave][$posicion][$titulo] as $titulo1 => $valor1){
                                    $_POST[$clave][$posicion][$titulo][$titulo1] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo][$titulo1]));
                                }
                            }else{
                                if(isset($_POST[$clave][$posicion][$titulo]) && !empty($_POST[$clave][$posicion][$titulo])) {
                                    $_POST[$clave][$posicion][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo]));
                                }
                            }
                        }
                        return $_POST[$clave][$posicion];
                    }
                }else{
                    if (isset($_POST[$clave])) {
                        foreach ($_POST[$clave] as $titulo => $valor) {
                            $_POST[$clave][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$titulo]));
                        }
                        return $_POST[$clave];
                    }
                }
            }else{
                $_POST[$clave] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave]));
                return $_POST[$clave];
            }
        }
    }
    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }

}