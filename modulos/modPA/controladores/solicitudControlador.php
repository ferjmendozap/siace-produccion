<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class solicitudControlador extends Controlador
{
    private $atSolicitudModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSolicitudModelo = $this->metCargarModelo('solicitud');
 
    }



    #Metodo Index del controlador Solicitud.
    public function metIndex()
    {

    }

    #Metodo parac cargar el listado de  solicitudes
    public function metListarSolicitud()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_DependenciaPost', $this->atSolicitudModelo->metGetDependencias());

        $this->_EstatusModelo = $this->metCargarModelo('estatus');
        $this->atVista->assign('_EstatusPost', $this->_EstatusModelo->metGetEstatus());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo->metGetMotivoSalida());

        $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
        $this->atVista->assign('_TipoSalidaPost',$this->_TipoSalidaModelo->metGetTipoSalida());

        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1){
            $formInt=$this->metObtenerInt('form','int');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            #Asignamos al select los valores en caso de no haber errores
            if($validacion['Estatus']!='error'){ $this->atVista->assign('_EstatusSelect',$validacion['Estatus']); }
            if($validacion['MotivoSalida']!='error'){ $this->atVista->assign('_MotivoSalidaSelect',$validacion['MotivoSalida']); }
            if($validacion['TipoSalida']!='error'){ $this->atVista->assign('_TipoSalidaSelect',$validacion['TipoSalida']); }


        }else{
            $this->atVista->assign('_SolicitudPost', $this->atSolicitudModelo->metGetSolicitud());
        }


        $this->atVista->metRenderizar('ListarSolicitud');
    }

    // Método que permite listar empleados de acuerdo a la busqueda
    public function metFiltroProyecto()
    {
        $filtro_estatus = intval($_POST['filtro_estatus']);
        $filtro_motivo = $_POST['filtro_motivo'];
        $filtro_tipo = $_POST['filtro_tipo'];
        $resultados= $this->atSolicitudModelo->metGetSolicitudFiltros($filtro_estatus,$filtro_motivo,$filtro_tipo);

        echo json_encode($resultados);

    }





    #Metodo para cargar el form registroSolicitud
    public function metRegistrarSolicitud()
    {

        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'


        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',

        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {


            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }






            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");


            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim($hasta." ".$hora_hasta.":00");


            $estatus="1";




            $id = $this->atSolicitudModelo->metSetSolicitud($desde,$hasta,$estatus,$validacion['Observacion'],$validacion['MotivoSalida'],$validacion['TipoSalida']);
            $respuesta=$this->atSolicitudModelo->metMostrar($id );

            $datos=[];
            $datos['idSolicitud']=$id ;
            $datos['fechas']=$respuesta[0]['fec_requerida']." ".$respuesta[0]['fec_hasta'];
            $datos['estatus']=$respuesta[0]['ind_nombre_detalle'];
            $datos['nombre']=$respuesta[0]['ind_nombre1']." ".$respuesta[0]['ind_apellido1'];
            $datos['observacion']=$validacion['Observacion'];
            echo json_encode($datos);
            exit;

        }

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());

        $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
        $this->atVista->assign('_TipoSalidaPost',$this->_TipoSalidaModelo->metGetTipoSalida());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo->metGetMotivoSalida());







        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);



        




        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarSolicitud',"modales");




    }

    #Metodo para cargar el form registro Solicitud
    public function metModificarSolicitud()
    {


        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "modificar",);


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',

        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        $id_valor=$this->metObtenerInt('idPost');




        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");


            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim($hasta." ".$hora_hasta.":00");


            $estatus="1";



            $this->atSolicitudModelo->metUpdateSolicitud($desde,$hasta , $estatus ,$validacion['Observacion'],$validacion['MotivoSalida'], $validacion['TipoSalida'] ,$validacion['idPost']);
            $respuesta=$this->atSolicitudModelo->metMostrar($validacion['idPost']);

            $datos=[];
            $datos['idSolicitud']=$validacion['idPost'];
            $datos['fechas']=$respuesta[0]['fec_requerida']." ".$respuesta[0]['fec_hasta'];
            $datos['estatus']=$respuesta[0]['ind_nombre_detalle'];
            $datos['nombre']=$respuesta[0]['ind_nombre1']." ".$respuesta[0]['ind_apellido1'];
            $datos['observacion']=$validacion['Observacion'];
            echo json_encode($datos);
            exit;


        }else {

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
            $this->atVista->assign('_SolicitudPost', $this->_SolicitudModelo->metMostrar($id_valor));

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
            $this->atVista->assign('_VehiculoPost', $this->_VehiculoModelo->metGetVehiculo());

            $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
            $this->atVista->assign('_TipoSalidaPost', $this->_TipoSalidaModelo->metGetTipoSalida());

            $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
            $this->atVista->assign('_MotivoSalidaPost', $this->_MotivoSalidaModelo->metGetMotivoSalida());


            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarCssComplemento($complementosCss);


            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->metRenderizar('RegistrarSolicitud', "modales");
        }

    }

    public function metVerSolicitud()
    {


        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "ver",);


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',

        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        $id_valor=$this->metObtenerInt('idPost');




        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");


            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim($hasta." ".$hora_hasta.":00");


            $estatus="1";


            $id = $this->atSolicitudModelo->metUpdateSolicitud($desde,$hasta , $estatus ,$validacion['Observacion'],$validacion['MotivoSalida'], $validacion['TipoSalida'] ,$validacion['idPost']);


        }else {

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
            $this->atVista->assign('_SolicitudPost', $this->_SolicitudModelo->metMostrar($id_valor));

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
            $this->atVista->assign('_VehiculoPost', $this->_VehiculoModelo->metGetVehiculo());

            $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
            $this->atVista->assign('_TipoSalidaPost', $this->_TipoSalidaModelo->metGetTipoSalida());

            $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
            $this->atVista->assign('_MotivoSalidaPost', $this->_MotivoSalidaModelo->metGetMotivoSalida());


            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarCssComplemento($complementosCss);


            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->metRenderizar('RegistrarSolicitud', "modales");
        }

    }

    #Metodo para cargar el form registro Solicitud
    public function metRechazarSolicitud()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "rechazar",);

        $id_valor=$this->metObtenerInt('idPost');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');

            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


             
			$Solicitud=$this->atSolicitudModelo->metMostrarEstatus($validacion['idPost']);
			$estatus=$Solicitud[0]['fk_a006_num_estado_solicitud'];
			if($estatus>1 && $estatus<4){
				$estatus--;
			}else if($estatus==1){
				$estatus=8;
			}
            $this->atSolicitudModelo->metEstatusSolicitud($validacion['MotivoRechazo'],$estatus ,$validacion['idPost']);
            echo json_encode($validacion['idPost']);
            exit;

        }else {

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
            $this->atVista->assign('_SolicitudPost', $this->_SolicitudModelo->metMostrar($id_valor));

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
            $this->atVista->assign('_VehiculoPost', $this->_VehiculoModelo->metGetVehiculo());

            $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
            $this->atVista->assign('_TipoSalidaPost', $this->_TipoSalidaModelo->metGetTipoSalida());

            $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
            $this->atVista->assign('_MotivoSalidaPost', $this->_MotivoSalidaModelo->metGetMotivoSalida());

            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarCssComplemento($complementosCss);


            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->metRenderizar('RechazarSolicitud', "modales");
        }

    }


    #Metodo para cargar el form registro Solicitud
    public function metConformarSolicitud()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "conformar",);

        $id_valor=$this->metObtenerInt('idPost');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );



        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');

            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');

            $formTxt=$this->metObtenerTexto('form','txt');

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");


            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim($hasta." ".$hora_hasta.":00");

            $estatus="3";


             $this->atSolicitudModelo->metConformar( $estatus ,$validacion['idPost'],$desde,$hasta,$validacion['Observacion']);

            echo json_encode($validacion['idPost']);
            exit;

        }

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
        $this->atVista->assign('_SolicitudPost',$this->_SolicitudModelo->metMostrar($id_valor));

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());

        $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
        $this->atVista->assign('_TipoSalidaPost',$this->_TipoSalidaModelo->metGetTipoSalida());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo->metGetMotivoSalida());




        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('ConformarSolicitud',"modales");


    }





    public function metListadoVerificarSolicitud()
    {

        $Accion=array( "accion" => "conformar",);

        $id_valor=$this->metObtenerInt('idPost');


        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',      );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->assign('_SolicitudPost', $this->atSolicitudModelo->metGetSolicitudes('1'));
        $this->atVista->metRenderizar('ListadoVerificarSolicitud' );


    }

    #Metodo para cargar el form registro Solicitud
    public function metListadoConformarSolicitud()
    {

        $Accion=array( "accion" => "conformar",);

        $id_valor=$this->metObtenerInt('idPost');


        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',      );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->assign('_SolicitudPost', $this->atSolicitudModelo->metGetSolicitudes('2'));
        $this->atVista->metRenderizar('ListadoConformarSolicitud' );


    }

    #Metodo para cargar el form registro Solicitud
    public function metListadoAprobarSolicitud()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',      );


        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_SolicitudPost', $this->atSolicitudModelo->metGetSolicitudes('3'));
        $this->atVista->metRenderizar('ListadoAprobarSolicitud' );


    }




    public function metListadoAsignaciones()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',      );
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_SolicitudPost', $this->atSolicitudModelo->metGetSolicitudes('5'));
        $this->atVista->metRenderizar('ListadoAsignaciones');

    }


    public function metListadoAsignacion()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',      );

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('_SolicitudPost', $this->atSolicitudModelo->metGetSolicitudes('4'));
        $this->atVista->metRenderizar('ListadoAsignacion' );


    }




    public function metDisponibilidadSolicitud()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "disponibilidad",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );



        $id_valor=$this->metObtenerInt('idPost');



        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');

            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');

            $formTxt=$this->metObtenerTexto('form','txt');

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");


            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim($hasta." ".$hora_hasta.":00");

            $estatus="2";


            $this->atSolicitudModelo->metGuardarDisponibilidad($validacion['Vehiculo'],$estatus ,$validacion['idPost'],$desde,$hasta,$validacion['Observacion']);

            echo json_encode($validacion['idPost']);
            exit;

        }

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
        $this->atVista->assign('_SolicitudPost',$this->_SolicitudModelo->metMostrar($id_valor));

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculoActivo());

        $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
        $this->atVista->assign('_TipoSalidaPost',$this->_TipoSalidaModelo->metGetTipoSalida());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo->metGetMotivoSalida());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('DisponibilidadSolicitud',"modales");


    }


    public function metAsignarVehiculo()
    {
        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "asignar",);

        $id_valor=$this->metObtenerInt('idSolicitud');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');

            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');

            $formTxt=$this->metObtenerTexto('form','txt');

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");


            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim($hasta." ".$hora_hasta.":00");

            $estatus="5";


            $this->atSolicitudModelo->metGuardarDisponibilidad($validacion['Vehiculo'],$estatus ,$validacion['idPost'],$desde,$hasta,$validacion['Observacion']);
			$Solicitud=$this->atSolicitudModelo->metMostrar($validacion['idPost']);
			$datos['Solicitante']=$Solicitud[0]['ind_nombre1'];
			$datos['Fechas']=$Solicitud[0]['fec_requerida']." ".$Solicitud[0]['hora_requerida']." - ".$Solicitud[0]['fec_hasta']." ".$Solicitud[0]['hora_hasta'];
			$datos['Estatus']=$Solicitud[0]['ind_nombre_detalle'];
			$datos['Observacion']=$Solicitud[0]['ind_observacion'];
			$datos['idSolicitud']=$validacion['idPost'];
            echo json_encode($datos);
            exit;


        }else {

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
            $this->atVista->assign('_SolicitudPost', $this->_SolicitudModelo->metMostrar($id_valor));

            # Llamado al modelo  Vehiculo para la carga de select en el formulario.
            $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
            $this->atVista->assign('_VehiculoPost', $this->_VehiculoModelo->metGetVehiculoActivo());

            $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
            $this->atVista->assign('_TipoSalidaPost', $this->_TipoSalidaModelo->metGetTipoSalida());

            $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
            $this->atVista->assign('_MotivoSalidaPost', $this->_MotivoSalidaModelo->metGetMotivoSalida());

            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarCssComplemento($complementosCss);


            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->metRenderizar('AsignarVehiculo', "modales");
        }

    }


    public function metAprobarSolicitud()
    {


        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "aprobar",);

        $id_valor=$this->metObtenerInt('idPost');


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');

            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');

            $formTxt=$this->metObtenerTexto('form','txt');

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            $desde=$this->metFormatoFecha($_POST['desde']);

            $hora_desde=$validacion['hora_desde'];
            $hora_desde = strtotime($hora_desde);
            $hora_desde = date("H:i", $hora_desde);
            $desde=trim($desde." ".$hora_desde.":00");


            $hasta=$this->metFormatoFecha($_POST['hasta']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim($hasta." ".$hora_hasta.":00");

            $estatus="4";


            $this->atSolicitudModelo->metAprobar( $estatus ,$validacion['idPost'],$desde,$hasta,$validacion['Observacion']);
            echo json_encode($validacion['idPost']);
            exit;



        }



        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_SolicitudModelo = $this->metCargarModelo('solicitud');
        $this->atVista->assign('_SolicitudPost',$this->_SolicitudModelo->metMostrar($id_valor));

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());

        $this->_TipoSalidaModelo = $this->metCargarModelo('tipoSalida');
        $this->atVista->assign('_TipoSalidaPost',$this->_TipoSalidaModelo->metGetTipoSalida());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo->metGetMotivoSalida());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('AprobarSolicitud',"modales");


    }


    public function metMantenimiento()
    {
                $id_valor=$this->metObtenerInt('id');
                $Mantenimiento=$this->atSolicitudModelo->metRevisarMantenimiento($id_valor);
                $Mensaje=[];
        
                if( isset($Mantenimiento[0]['pk_num_mantenimiento'])){


                    $datetime1 = date_create(date('Y-m-d'));
                    $datetime2 = date_create($Mantenimiento[0]['fec_proximo_mantenimiento']);
                    $interval = date_diff($datetime1, $datetime2);
                    $dias=$interval->format('%R%a');

                    if($dias<=0){


                        $Mensaje['mensaje']="  El Vehículo necesita una revisión de aceite , el dia de su mantenimiento ya ha pasado por ".$dias." dia(s)";
                        $Mensaje['color']="red";

                    }else if ($dias<=15){
                        $Mensaje['mensaje']=" Quedan ".$dias." dia(s) para el próximo cambio de aceite programado. ";
                        $Mensaje['color']="red";


                    }else{
                            $Mensaje['mensaje']="  El Vehículo esta al dia con el mantenimiento de aceite. ";
                            $Mensaje['color']="green";


                        }

                }else{

                    $Mensaje['mensaje']="  El Vehículo no tiene registrado un cambio de aceite previo.".$Mantenimiento[0]['pk_num_mantenimiento'];
                    $Mensaje['color']="red";

                }


        echo json_encode($Mensaje);
        exit;
    }

    #Metodo para actualizar la clase cuando ocurra un cambio en un select
    public function metDisponibilidadVehiculo()
    {

        $Vehiculo=$this->metObtenerAlphaNumerico('Vehiculo');
        $Solicitud=$this->metObtenerInt('idPost');

        $desde=$this->metFormatoFecha( $_POST['desde']);

        $hora_desde=$this->metObtenerAlphaNumerico('hora_desde');
        $hora_desde = strtotime($hora_desde);
        $hora_desde = date("H:i", $hora_desde);
        $desde=trim($desde." ".$hora_desde.":00");


        $hasta=$this->metFormatoFecha( $_POST['hasta']);

        $hora_hasta=$this->metObtenerTexto('hora_hasta');
        $hora_hasta = strtotime($hora_hasta);
        $hora_hasta = date("H:i", $hora_hasta);
        $hasta=trim($hasta." ".$hora_hasta.":00");

        $consulta= $this->atSolicitudModelo->metDisponibilidad($desde,$hasta,$Vehiculo,$Solicitud);

        if(sizeof($consulta)==0){
            $resultado="Disponible";
        }else{
            $resultado="No Disponible";
        }



        echo json_encode($resultado);
        exit;




    }

    #Metodo para cargar el form registro   Solicitud
    public function metEliminarSolicitud()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $this->atSolicitudModelo->metDeleteSolicitud($id_valor);

    }

    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }
 


}
























































