<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">LISTAR TALLER</h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de los talleres registrados en el sistema.</h5>
    </div><!--end .col -->

</div><!--end .card -->

</div><!--end .card -->

<section class="style-default-bright">





    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-2"> N° </th>
                        <th class="sort-alpha col-sm-6"> Nombre del Taller</th>



                    </tr>
                    </thead>
                    <tbody>


                    {foreach item=post from=$_TallerPost}
                        <tr id="idPost{$post.fk_a003_num_persona_proveedor}" class="gradeA">
                            <td>{$post.fk_a003_num_persona_proveedor}</td>
                            <td>{$post.ind_nombre1}</td>

                        </tr>








                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>

                    </tr>
                    </tfoot>

                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->


