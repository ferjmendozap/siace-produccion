<link type="text/css"   rel="stylesheet" href="{$_Parametros.ruta_Complementos}theme-default/libs/wizard/wizard.css?1425466601" />





<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >



        <input type="hidden" value="1" name="valido"  id="valido" />
        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">
                                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>

                                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> COBERTURA DE LA REPARACIÓN</span></a></li>



                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="step1">





                            <input type="hidden" value="{if isset($_EntradaTallerPost[0]['pk_num_entrada_taller'])}{$_EntradaTallerPost[0]['pk_num_entrada_taller']}{/if}"  name="idPost"      id="idPost" />





                            <input type="hidden" value="{$_Acciones.accion}"    id="accion" />


                            <div class="col-lg-6">


                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select   required="" class="form-control" name="form[int][Vehiculo]" id="Vehiculo" aria-required="true">
                                            <option value="">&nbsp;</option>



                                            {foreach item=tipo from=$_VehiculoPost}

                                                {if isset($_EntradaTallerPost[0]['fk_pab001_num_vehiculo'])}

                                                    {if $tipo.pk_num_vehiculo==$_EntradaTallerPost[0]['fk_pab001_num_vehiculo']}
                                                        <option selected value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa} </option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                {/if}
                                            {/foreach}




                                        </select>
                                        <label for="rangelength2"> Vehículo </label>
                                    </div>
                                </div>

                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <select    required="" class="form-control" name="form[int][Chofer]" id="Chofer" aria-required="true">
                                            <option value="">&nbsp;</option>


                                            {foreach item=tipo from=$_ChoferPost}

                                                {if isset($_EntradaTallerPost[0]['fk_pab012_num_chofer'])}

                                                    {if $tipo.pk_num_chofer ==$_EntradaTallerPost[0]['fk_pab012_num_chofer']}
                                                        <option selected value="{$tipo.pk_num_chofer}">{$tipo.ind_nombre1} {$tipo.ind_apellido1}</option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_chofer}">{$tipo.ind_nombre1} {$tipo.ind_apellido1}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_chofer}">{$tipo.ind_nombre1} {$tipo.ind_apellido1}</option>
                                                {/if}
                                            {/foreach}




                                        </select>
                                        <label for="rangelength2"> Chofer</label>
                                    </div>

                                </div>


                                <div class="col-lg-12">

                                    <div class="form-group   col-lg-6  ">
                                        <input required="" type="text" readonly name="entrada" class="form-control fechas2" value="{if isset($_EntradaTallerPost[0]['fec_entrada'])}{$_EntradaTallerPost[0]['fec_entrada']}{/if}">
                                        <label>Fecha de Entrada</label>
                                    </div>

                                    <div class="form-group col-lg-6 ">
                                        <input required=""  type="text" name="form[alphaNum][hora]" id="hora" class="form-control time12-mask" value="{if isset($_EntradaTallerPost[0]['hora'])}{$_EntradaTallerPost[0]['hora']}{/if}">
                                        <label>Hora de Entrada </label>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">


                                        <input type="text" required="" value="{if isset($_EntradaTallerPost[0]['ind_kilometraje'])}{$_EntradaTallerPost[0]['ind_kilometraje']}{/if}"name="form[int][Kilometraje]"  data-rule-rangelength="[0, 9]" id="kilometraje" class="form-control"   >



                                        <label for="rangelength2">Kilometraje</label>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="col-sm-10 form-group ">

                                        <input type="text" class="form-control"
                                               id="nombreProveedor"
                                               value="{if isset($_EntradaTallerPost[0]['ind_nombre1'])}{$_EntradaTallerPost[0]['ind_nombre1']}
                                               {else}
                                               {if isset($_EntradaTallerPost[0]['ind_nombre2'])}{$_EntradaTallerPost[0]['ind_nombre2']}  {$_EntradaTallerPost[0]['ind_nombre3']}{/if}
                                               {/if}
                                                "
                                               readonly >

                                        <input type="hidden" class="form-control"
                                               id="codProveedor" name="form[int][Taller]"
                                               value="{if isset($_EntradaTallerPost[0]['fk_lgb022_num_proveedor'])}{$_EntradaTallerPost[0]['fk_lgb022_num_proveedor']}{/if}"
                                                >

                                        <label for="rangelength2">Taller</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group floating-label">
                                            <button
                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#formModal2"
                                                    titulo="Listado de Personas"
                                                    {if isset($ver) and $ver==1}disabled{/if}
                                                    url="{$_Parametros.url}modPA/entradaTallerCONTROL/personaMET/persona/">
                                                <i class="md md-search"></i>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <textarea  required=""  placeholder="" rows="3" class="form-control " id="Motivo" name="form[alphaNum][Motivo]">{if isset($_EntradaTallerPost[0]['ind_motivo'])}{$_EntradaTallerPost[0]['ind_motivo']}{/if}</textarea>
                                        <label for="textarea1">Motivo </label>
                                    </div>

                                </div>




                            </div>





                        </div><!--end #step1 -->

                        <div class="tab-pane" id="step2">



                            <div class="col-lg-6">
                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <textarea  required="" placeholder="" rows="3" class="form-control" id="Direccion" name="form[alphaNum][Observacion]">{if isset($_EntradaTallerPost[0]['ind_observacion'])}{$_EntradaTallerPost[0]['ind_observacion']}{/if}</textarea>
                                        <label for="textarea1"> Observación </label>
                                    </div>

                                </div>


                            </div>
                            <div class="  col-md-3">

                                <label class="radio-inline radio-styled">
                                    <input {if isset($_EntradaTallerPost[0]['ind_cobertura'])} {if ($_EntradaTallerPost[0]['ind_cobertura'])==0}checked{/if} {/if} name="form[int][Pago]" value="0" type="radio"><span>Por Seguro </span>
                                </label>
                                <label class="radio-inline radio-styled">
                                    <input  {if isset($_EntradaTallerPost[0]['ind_cobertura'])}  {if ($_EntradaTallerPost[0]['ind_cobertura'])==1}checked{/if} {/if}  name="form[int][Pago]" value="1" type="radio"><span>Por Contraloria</span>
                                </label>

                            </div><!--end .col -->


                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <span class="clearfix"></span>

                            <div class="modal-footer">

                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>

                            </div>
                        </div><!--end #step2 -->
                        </div>

        <ul class="pager wizard">

            <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

            <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
        </ul>




    </form>
</div>

<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();




            var accion = $("#accion").val();
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/entradaTallerCONTROL/RegistrarEntradaTallerMET", datos, function (dato) {
                    $(document.getElementById('datatable1')).append('<td>'+dato['idEntrada']+'</td>' +
                            '<td>     </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['taller']+' </td>' +
                            '<td>'+dato['motivo']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idEntrada="'+dato['idEntrada']+'"' +
                            'descipcion="El Usuario a Modificado un Salida" titulo="Modificar Salida">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEntrada="'+dato['idEntrada']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Salida!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');

                }, 'json');
                swal("Registro Agregado!", "La marca del vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
                $.post("{$_Parametros.url}modPA/entradaTallerCONTROL/ModificarEntradaTallerMET", datos, function (dato) {
                    $(document.getElementById('idEntrada'+dato['idEntrada'])).html('<td>'+dato['idEntrada']+'</td>' +
                            '<td>  </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['taller']+' </td>' +
                            '<td>'+dato['motivo']+' </td>' +

                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idEntrada="'+dato['idEntrada']+'"' +
                            'descipcion="El Usuario a Modificado un Salida" titulo="Modificar Salida">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEntrada="'+dato['idEntrada']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Salida!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Registro Modifcado!", "La marca del vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }


        }
    });

    $('.accionModal').click(function () {

        accionModal(this,'url')
    });

    function accionModal(id,attr){

        $('#formModalLabel2').html($(id).attr('titulo'));
        $.post($(id).attr(attr), {
            cargar: 0,

            tr: $("#"+$(id).attr('idTabla')+" > tbody > tr").length + 1
        }, function ($dato) {
            $('#ContenidoModal2').html($dato);
        });
    }




    $(document).ready(function() {
        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es'});
        $("#kilometraje").inputmask( "999999",{ numericInput: true});







    });
</script>