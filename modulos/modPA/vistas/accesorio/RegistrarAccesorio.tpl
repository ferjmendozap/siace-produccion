<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido"  id="valido" />



                                <div class="col-lg-6">
                                    <form class="form form-validate " novalidate="novalidate">
                                    <div class="form-group">
                                        {foreach item=post from=$_AccesorioPost}


                                        <input maxlength="100" value="{if isset($post.ind_descripcion_accesorio)}{$post.ind_descripcion_accesorio}{/if}" type="text" class="form-control" id="AccesorioVehiculo" name="AccesorioVehiculo" data-rule-rangelength="[2, 100]" required>
                                        <input type="hidden" value="{if isset($post.pk_num_accesorio)}{$post.pk_num_accesorio}{/if}"  name="idPost"    id="idPost" />
                                        {/foreach}

                                        <input type="hidden" value="{$_Acciones.accion}"    id="accion" />


                                        <label for="rangelength2">Nombre del Accesorio</label>
                                    </div>
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>
                                <span class="clearfix"></span>

                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                                </div>
                            </div>       </div>





</form>
</div>


<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="registrar") {

                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });

          $.post("{$_Parametros.url}modPA/accesorioCONTROL/RegistrarAccesorioMET", datos, function (dato) {


              if(dato['status']=='error') {


                  swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");

              }else{
                      $(document.getElementById('datatable1')).append('<tr  id="idAccesorio'+dato['idAccesorio']+'">' +
                      '<td>'+dato['idAccesorio']+'</td>' +
                      '<td> '+dato['accesorio']+'</td>' +
                      '<td   class="sort-alpha col-sm-1" >' +
                      '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                      'data-keyboard="false" data-backdrop="static" idAccesorio="'+dato['idAccesorio']+'"' +
                      'descipcion="El Usuario a Modificado un Accesorio" titulo="Modificar Accesorio">' +
                      '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                      '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAccesorio="'+dato['idAccesorio']+'"  boton="si, Eliminar"' +
                      'descipcion="El usuario a eliminado un Accesorio" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Accesorio!!">' +
                      '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                      '</td>' +
                      '</tr>');

                        swal("Registro Agregado!", "Accesorio ha sido agregado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                  }

                }, 'json');



            }else {
                $.post("{$_Parametros.url}modPA/accesorioCONTROL/ModificarAccesorioMET", datos, function (dato) {

                    $(document.getElementById('idAccesorio'+dato['idAccesorio'])).html('<td>'+dato['idAccesorio']+'</td>' +
                            '<td>'+dato['accesorio']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idAccesorio="'+dato['idAccesorio']+'"' +
                            'descipcion="El Usuario a Modificado un Accesorio" titulo="Modificar Accesorio">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAccesorio="'+dato['idAccesorio']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Accesorio" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Accesorio!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Registro Modificado!", "Accesorio ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');

            }



        }
    });


    $(document).ready(function() {


        $("#formAjax").validate();


    });


</script>