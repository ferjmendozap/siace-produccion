<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido"  id="valido" />


        <div class="col-lg-6">
            <form class="form form-validate " novalidate="novalidate">
                <div class="form-group">
                    {foreach item=post from=$_PiezaVehiculoPost}


                        <input value="{if isset($post.ind_descripcion)}{$post.ind_descripcion}{/if} " type="text" class="form-control" id="PiezaVehiculo" name="PiezaVehiculo" data-rule-rangelength="[2, 150]" required>
                        <input type="hidden" value="{if isset($post.ind_descripcion)}{$post.pk_num_piezas}{/if} "  name="idPost"    id="idPost" />
                        <input type="hidden" value="{if isset($post.ind_descripcion)}{$post.fk_pab008_num_ubicacion_pieza}{/if}"  name="idUbicacion"    id="idUbicacion" />
                    {/foreach}

                    <input type="hidden" value="{$_Acciones.accion}"    id="accion" />


                    <label for="rangelength2">Pieza de Vehículo</label>
                </div>


                <div class="form-group">
                    <select required="" class="form-control" name="form[int][UbicacionPiezaA]" id="UbicacionPiezaA" aria-required="true">
                        <option value="">&nbsp;</option>

                        {foreach item=post from=$_UbicacionPiezaA}

                            {if isset($_PiezaVehiculoPost[0]['fk_a006_num_ubicacionA'])}

                                {if $post.pk_num_miscelaneo_detalle==$_PiezaVehiculoPost[0]['fk_a006_num_ubicacionA']}

                                    <option selected value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>
                                {else}
                                    <option   value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>
                                {/if}
                            {else}
                                <option   value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>
                            {/if}



                        {/foreach}

                    </select>
                    <label for="rangelength2">Ubicación de la Pieza (A)</label>
                </div>


                <div class="form-group">
                    <select required="" class="form-control" name="form[int][UbicacionPiezaB]" id="UbicacionPiezaB" aria-required="true">
                        <option value="">&nbsp;</option>

                        {foreach item=post from=$_UbicacionPiezaB}

                            {if isset($_PiezaVehiculoPost[0]['fk_a006_num_ubicacionB'])}

                                {if $post.pk_num_miscelaneo_detalle==$_PiezaVehiculoPost[0]['fk_a006_num_ubicacionB']}

                                    <option selected value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>

                                {else}

                                    <option   value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>

                                {/if}
                            {else}
                                <option   value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>
                            {/if}



                        {/foreach}

                    </select>
                    <label for="rangelength2">Ubicación de la Pieza (B)</label>
                </div>



                <div class="form-group">
                    <select required="" class="form-control" name="form[int][UbicacionPiezaC]" id="UbicacionPiezaC" aria-required="true">
                        <option value="">&nbsp;</option>

                        {foreach item=post from=$_UbicacionPiezaC}

                            {if isset($_PiezaVehiculoPost[0]['fk_a006_num_ubicacionC'])}

                                {if $post.pk_num_miscelaneo_detalle==$_PiezaVehiculoPost[0]['fk_a006_num_ubicacionC']}

                                    <option selected value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>
                                {else}
                                    <option   value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>
                                {/if}
                            {else}
                                <option   value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>
                            {/if}



                        {/foreach}

                    </select>
                    <label for="rangelength2">Ubicación de la Pieza (C)</label>
                </div>


        </div>


        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

        <div class="modal-footer">

            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

            <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


        </div>
    </div>       </div>





</form>
</div>


<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="registrar") {

                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });


                $.post("{$_Parametros.url}modPA/piezaVehiculoCONTROL/RegistrarPiezaVehiculoMET", datos, function (dato) {

                    $(document.getElementById('datatable1')).append('<tr  id="idPieza'+dato['idPieza']+'">' +
                            '<td>'+dato['idPieza']+'</td>' +
                            '<td> '+dato['pieza']+'</td>' +
                            '<td   class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-06-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPieza="'+dato['idPieza']+'"' +
                            'descipcion="El Usuario a Modificado un Pieza " titulo="Modificar Pieza ">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-06-01-03-E',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPieza="'+dato['idPieza']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Pieza " titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Pieza !!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                }, 'json');


                swal("Registro Agregado!", "Pieza de vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {

                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });

                
                $.post("{$_Parametros.url}modPA/piezaVehiculoCONTROL/ModificarPiezaVehiculoMET", datos, function (dato) {
                    $(document.getElementById('idPieza'+dato['idPieza'])).html('<td>'+dato['idPieza']+'</td>' +
                            '<td>'+dato['pieza']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-06-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPieza="'+dato['idPieza']+'"' +
                            'descipcion="El Usuario a Modificado un Pieza " titulo="Modificar Pieza ">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-06-01-03-E',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPieza="'+dato['idPieza']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Pieza " titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Pieza !!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Registro Modificado!", "Pieza de vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');

            }




        }
    });


    $(document).ready(function() {
        $("#formAjax").validate();
        // Asignamos el valor que trae el fk en el select

    });

    $('.accionModal').click(function () {

        accionModal(this,'url')
    });

    function accionModal(id,attr){

        $('#formModalLabel2').html($(id).attr('titulo'));
        $.post($(id).attr(attr), {
            cargar: 0,

            tr: $("#"+$(id).attr('idTabla')+" > tbody > tr").length + 1
        }, function ($dato) {
            $('#ContenidoModal2').html($dato);
        });
    }

</script>