<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >



        <input type="hidden" value="1" name="valido"  id="valido" />
        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">


                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>

                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> INFORMACIÓN DE SALIDA</span></a></li>

                <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">ACCESORIOS</span></a></li>


            </ul>
        </div><!--end .form-wizard-nav -->
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="step1">

                <input type="hidden" value="{if isset($_SolicitudPost[0]['pk_num_salida_vehiculo'])}{$_SolicitudPost[0]['pk_num_salida_vehiculo']}{/if}"  name="form[int][idSalida]"      id="idSalida" />

                <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />

                <div class="col-lg-6">

                    <div class="col-lg-12">
                        <div class="form-group">
                            <select  required="" class="form-control" name="form[int][idSolicitud]" id="idSolicitud" aria-required="true">
                                <option value="">&nbsp;</option>



                                {foreach item=tipo from=$_SolicitudesPost}

                                    {if isset($_SolicitudPost[0]['pk_num_solicitud'])}

                                        {if $tipo.pk_num_solicitud==$_SolicitudPost[0]['pk_num_solicitud']}
                                            <option selected value="{$tipo.pk_num_solicitud}">{$tipo.ind_modelo} - {$tipo.ind_placa} </option>
                                        {else}
                                            <option value="{$tipo.pk_num_solicitud}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                        {/if}
                                    {else}
                                        <option value="{$tipo.pk_num_solicitud}">  {$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                    {/if}
                                {/foreach}




                            </select>
                            <label for="rangelength2"> Solicitudes con Vehículo Asignado </label>
                        </div>
                    </div>


                </div>

            </div><!--end #step1 -->

            <div class="tab-pane" id="step2">




                <div class="col-lg-6">
                    <div class="col-lg-12">
                        <div class="form-group">

                            <div id="fechas2" class="input-daterange input-group">
                                <div class="input-group-content">
                                    <input class="form-control" type="text" id="desde" readonly name="desde" value="{if isset($_SolicitudPost[0]['fec_hora_salida'])}{$_SolicitudPost[0]['fec_hora_salida']}{else}{$_SolicitudPost[0]['fec_requerida']}{/if}">
                                    <label>Fecha de Salida</label>
                                </div>

                                <span class="input-group-addon">Hasta</span>
                                <div class="input-group-content">
                                    <input class="form-control" type="text"  id="hasta" readonly  name="hasta" value="{if isset($_SolicitudPost[0]['fec_hora_hasta'])}{$_SolicitudPost[0]['fec_hora_hasta']}{else}{$_SolicitudPost[0]['fec_hasta']}{/if}">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <input type="text" name="form[txt][hora_desde]" id="hora_desde"  class="form-control time12-mask" value="{if isset($_SolicitudPost[0]['hora_salida'])}{$_SolicitudPost[0]['hora_salida']}{else}{$_SolicitudPost[0]['hora_requerida']}{/if}">
                            <label>Hora de Salida</label>

                        </div>

                        <div class="form-group col-lg-6">
                            <input type="text"name="form[txt][hora_hasta]" id="hora_hasta" class="form-control time12-mask" value="{if isset($_SolicitudPost[0]['hora_hasta2'])}{$_SolicitudPost[0]['hora_hasta2']}{else}{$_SolicitudPost[0]['hora_hasta']}{/if}">
                            <label>Hora de Llegada</label>
                        </div>
                    </div>



                    <div class="col-lg-12">
                        <div class="form-group">



                            <select  required="" class="form-control" name="form[int][Vehiculo]" id="Vehiculo" aria-required="true">
                                <option value="">&nbsp;</option>


                                {foreach item=tipo from=$_VehiculoPost}

                                    {if isset($_VehiculoAsignado)}

                                        {if $tipo.pk_num_vehiculo==$_VehiculoAsignado}
                                            <option selected value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa} </option>
                                        {else}
                                            <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa}</option>
                                        {/if}
                                    {else}
                                        <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa} </option>
                                    {/if}
                                {/foreach}




                            </select>
                            <label for="rangelength2"> Vehículo </label>
                        </div>

                    </div>


                    <div class="col-lg-12">


                        <div class="col-sm-12">
                            <div class="col-sm-10 form-group ">

                                <input required type="text" class="form-control"
                                       id="nombreProveedor"
                                       value="{if isset($_SolicitudPost[0]['chofer1'])}{$_SolicitudPost[0]['chofer1']} {$_SolicitudPost[0]['chofer2']}{/if}"
                                       readonly >

                                <input type="hidden" class="form-control"
                                       id="codProveedor" name="form[alphaNum][Chofer]"
                                       value="{if isset($_SolicitudPost[0]['fk_pab012_num_chofer'])}{$_SolicitudPost[0]['fk_pab012_num_chofer']}{/if}"
                                >

                                <label for="rangelength2"> Chofer </label>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group floating-label">
                                    <button
                                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                            type="button"
                                            data-toggle="modal"
                                            data-target="#formModal2"
                                            titulo="Listado de Personas"
                                            {if isset($ver) and $ver==1}disabled{/if}
                                            url="{$_Parametros.url}modPA/choferCONTROL/personaMET/persona/">
                                        <i class="md md-search"></i>
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>



                    <div class="col-lg-12">
                        <div class="form-group">


                            <input type="text" required="" value="{if isset($_SolicitudPost[0]['ind_kilometraje'])}{$_SolicitudPost[0]['ind_kilometraje']}{/if}"name="form[int][kilometraje]" data-rule-rangelength="[2, 30]"  id="kilometraje" class="form-control"  >



                            <label for="rangelength2">Kilometraje</label>
                        </div>
                    </div>


                </div>
                <div class="col-xs-2">
                    <div class="form-group">



                        <select  required="" class="form-control" name="form[int][Gasolina]" id="Gasolina" aria-required="true">
                            <option value="">&nbsp;</option>


                            {*foreach item=gasolina from=$_TanqueGasolina}

                                <option value="{$gasolina.pk_num_miscelaneo_detalle}">{$gasolina.ind_nombre_detalle} </option>

                            {/foreach*}


                            {foreach item=gasolina from=$_TanqueGasolina}

                                {if isset($_CombustibleSalida)}

                                    {if $gasolina.pk_num_miscelaneo_detalle==$_CombustibleSalida}
                                        <option selected value="{$gasolina.pk_num_miscelaneo_detalle}">{$gasolina.ind_nombre_detalle} </option>
                                    {else}
                                        <option value="{$gasolina.pk_num_miscelaneo_detalle}">{$gasolina.ind_nombre_detalle}</option>
                                    {/if}
                                {else}
                                    <option value="{$gasolina.pk_num_miscelaneo_detalle}">{$gasolina.ind_nombre_detalle} </option>
                                {/if}
                            {/foreach}




                        </select>
                        <label for="rangelength3"> Nivel de Combustible </label>
                    </div>

                </div>

                <div class="col-lg-6">

                    <div class="form-group">
                        <textarea required="" placeholder="" rows="3" class="form-control" id="ObservacionSalida" name="form[alphaNum][ObservacionSalida]">{if isset($_SolicitudPost[0]['observacionSalida'])}{$_SolicitudPost[0]['observacionSalida']}{/if}</textarea>
                        <label for="textarea1">Observación de la Salida</label>
                    </div>



                </div>

                <div class="col-lg-6">
                    <div id="foto_perfil_auto">
                        {if !is_null($_SolicitudPost[0].ind_url_foto_frontal)&&trim($_SolicitudPost[0].ind_url_foto_frontal)!=""&&trim($_SolicitudPost[0].ind_url_foto_frontal)!="faceman_Suburban_Assault_Vehicle_(Front).svg"}

                            <img src="{$_Parametros['ruta_Img']}modPA/{$_SolicitudPost[0]['ind_placa']}/{$_SolicitudPost[0]['ind_url_foto_frontal']}" class="height-5 width-5 img-rounded "  style="margin-left: 200px;"  />

                        {else}
                            <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Front).svg" class="height-5 width-5 img-rounded "  style="margin-left: 200px;"  />
                        {/if}
                    </div>
                </div>
            </div><!--end #step2 -->

            <div class="tab-pane" id="step3">

                <div class="card-body">
                    <form class="form">
                        <div class="col-lg-6">Accesorios  </div> <div class="col-lg-6"> &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Accesorios a llevar</div>
                        <select multiple="multiple"   id="optgroup"  name="form[int][menu][]" style="position: absolute; left: -9999px;">

                            {foreach item=menu from=$_AccesorioPost}

                                {if in_array($menu.pk_num_accesorio,$_AccesorioSelectPost)}
                                    <option selected="selected" value="{$menu.pk_num_accesorio}">{$menu.ind_descripcion_accesorio}   </option>
                                {else}
                                    <option value="{$menu.pk_num_accesorio}">{$menu.ind_descripcion_accesorio}   </option>
                                {/if}
                            {/foreach}

                        </select>


                </div>
                <div class="row">
                    &nbsp;
                </div>
                <div class="row">
                    &nbsp;
                </div>

                <div class="row">
                    &nbsp;
                </div>

                <div class="row">
                    &nbsp;
                </div>

                <div class="row">
                    &nbsp;
                </div>
                <div class="row">
                    &nbsp;
                </div>
                <span class="clearfix"></span>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                </div>
            </div>       </div>

        <ul class="pager wizard">

            <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

            <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
        </ul>




    </form>
</div>

<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/salidaInstitucionCONTROL/RegistrarSalidaInstitucionMET", datos, function (dato) {

                    $(document.getElementById('datatable1')).append('<td>'+dato['idSalida']+'</td>' +
                        '<td>  Nuevo Registro  </td>' +
                        '<td>'+dato['modelo']+' </td>' +
                        '<td>'+dato['salida']+' </td>' +
                        '<td>'+dato['hora']+' </td>' +
                        '<td>'+dato['observacion']+' </td>' +
                        '<td  class="sort-alpha col-sm-1" >' +
                        '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                        'data-keyboard="false" data-backdrop="static" idSalida="'+dato['idSalida']+'"' +
                        'descipcion="El Usuario a Modificado un Salida" titulo="Modificar Salida">' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                        '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSalida="'+dato['idSalida']+'"  boton="si, Eliminar"' +
                        'descipcion="El usuario a eliminado un Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Salida!!">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                        '</td>' +
                        '</tr>');

                }, 'json');
                swal("Registro Agregado!", "El registro de salida de vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {


                $.post("{$_Parametros.url}modPA/salidaInstitucionCONTROL/ModificarSalidaInstitucionMET", datos, function (dato) {

                    $(document.getElementById('idSalida'+dato['idSalida'])).html('<td>'+dato['idSalida']+'</td>' +
                        '<td> Registro Modificado </td>' +
                        '<td>'+dato['modelo']+' </td>' +
                        '<td>'+dato['salida']+' </td>' +
                        '<td>'+dato['hora']+' </td>' +
                        '<td>'+dato['observacion']+' </td>' +
                        '<td  class="sort-alpha col-sm-1" >' +
                        '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                        'data-keyboard="false" data-backdrop="static" idSalida="'+dato['idSalida']+'"' +
                        'descipcion="El Usuario a Modificado un Salida" titulo="Modificar Salida">' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                        '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSalida="'+dato['idSalida']+'"  boton="si, Eliminar"' +
                        'descipcion="El usuario a eliminado un Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Salida!!">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                        '</td>');

                }, 'json');
                swal("Registro Modificado!", "El registro de salida de vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }



        }
    });


    $(document).ready(function() {

        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('#fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es'});
        $('#optgroup').multiSelect({ selectableOptgroup: true });
        $("#kilometraje").inputmask( "999999",{ numericInput: true});


        $("#idSolicitud").change(function () {

            $("#idSolicitud option:selected").each(function () {

                $.post("{$_Parametros.url}modPA/salidaInstitucionCONTROL/ActualizarDatosMET", { idSolicitud: $('#idSolicitud').val(),accion: $('#accion').val() }, function(dato){
                    $('#ContenidoModal').html(dato);



                });
            });
        });

        $("#Vehiculo").change(function () {

            $("#Vehiculo option:selected").each(function () {

                $.post("{$_Parametros.url}modPA/vehiculoCONTROL/CambiarFotoMET", { Vehiculo: $('#Vehiculo').val() }, function(data){
                    var json = data,
                        obj = JSON.parse(json);

                    if(obj[0]["ind_url_foto_frontal"]!="faceman_Suburban_Assault_Vehicle_(Front).svg") {
                        $("#foto_perfil_auto").html("<img src=\'{$_Parametros['ruta_Img']}modPA/"+obj[0]["ind_placa"] +"/" + obj[0]["ind_url_foto_frontal"] + "' class='height-5 width-5 img-rounded'  style='margin-left: 200px;'  /> ");
                    }else{
                        $("#foto_perfil_auto").html("<img src=\'{$_Parametros['ruta_Img']}modPA/" +obj[0]["ind_url_foto_frontal"] + "' class='height-5 width-5 img-rounded'  style='margin-left: 200px;'  /> ");
                    }
                });
            });
        });





    });


    $('.accionModal').click(function () {

        accionModal(this,'url')
    });

    function accionModal(id,attr){

        $('#formModalLabel2').html($(id).attr('titulo'));
        $.post($(id).attr(attr), {
            cargar: 0,

            tr: $("#"+$(id).attr('idTabla')+" > tbody > tr").length + 1
        }, function ($dato) {
            $('#ContenidoModal2').html($dato);
        });
    }



</script>