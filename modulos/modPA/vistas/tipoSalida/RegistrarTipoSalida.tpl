<form action="" id="formAjax" class="form" role="form" method="post" AUTOCOMPLETE="off">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idTipoSalida}" name="idTipoSalida"/>
        <input type="hidden" value="{if isset($idMaestro)}{$idMaestro}{/if}" name="form[int][idMaestro]"/>

        <div class="col-xs-6">

            <div class="form-group floating-label">

                <input type="text" class="form-control input-sm" maxlength="4" value="{if isset($formDB.cod_detalle)}{$formDB.cod_detalle}{/if}" name="form[alphaNum][cod_detalle]" id="cod_detalle" required data-msg-required="Introduzca Codigo Detalle"  >
                <label for="cod_detalle">Codigo Detalle</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label">

                <input type="text" class="form-control input-sm" maxlength="100" value="{if isset($formDB.ind_nombre_detalle)}{$formDB.ind_nombre_detalle}{/if}" name="form[alphaNum][tipoSalida]" id="tipoSalida" required data-msg-required="Introduzca la Descripcion del Tipo de Salida" onchange="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_detalle"> Tipo de Salida </label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>


        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"> <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"> <span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>



<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();


                $.post("{$_Parametros.url}modPA/tipoSalidaCONTROL/RegistrarTipoSalidaMET", datos, function (dato) {
                    
                    if(dato['estado']=='modificacion'){

                        swal({
                            title: "¡Por favor espere!",
                            text: "Se esta procesando su solicitud, puede demorar un poco.",
                            timer: 50000000,
                            showConfirmButton: false
                        });

                            $(document.getElementById('idTipoSalida'+dato['idTipoSalida'])).html('<td>'+dato['cod_detalle']+'</td>' +
                                    '<td>'+dato['tipoSalida']+'</td>' +
                                    '<td  class="sort-alpha col-sm-1" >' +
                                    '{if in_array('PA-01-06-09',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                    'data-keyboard="false" data-backdrop="static" idTipoSalida="'+dato['idTipoSalida']+'"' +
                                    'descipcion="El Usuario a Modificado el Tipo de Salida" titulo="Modificar Tipo de Salida">' +
                                    '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                    '{if in_array('PA-01-06-09',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTipoSalida="'+dato['idTipoSalida']+'"  boton="si, Eliminar"' +
                                    'descipcion="El usuario a eliminado el Tipo de Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Salida!!">' +
                                    '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                    '</td>');
                            swal("Registro Modificado!", "El tipo de salida  fue modificado satisfactoriamente.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');





                    }else if(dato['estado']=='creacion'){

                        swal({
                            title: "¡Por favor espere!",
                            text: "Se esta procesando su solicitud, puede demorar un poco.",
                            timer: 50000000,
                            showConfirmButton: false
                        });


                        if(dato['status']=='error') {


                            swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");

                        }else{

                            $(document.getElementById('datatable1')).append('<tr  id="idTipoSalida'+dato['idTipoSalida']+'">' +
                                    '<td>'+dato['cod_detalle']+'</td>' +
                                    '<td>'+dato['tipoSalida']+'</td>' +
                                    '<td   class="sort-alpha col-sm-1" >' +
                                    '{if in_array('PA-01-06-09',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                    'data-keyboard="false" data-backdrop="static" idTipoSalida="'+dato['idTipoSalida']+'"' +
                                    'descipcion="El Usuario a Modificado el Tipo de Salida" titulo="Modificar Tipo de Salida">' +
                                    '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                    '{if in_array('PA-01-06-09',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTipoSalida="'+dato['idTipoSalida']+'"  boton="si, Eliminar"' +
                                    'descipcion="El usuario a eliminado el Tipo de Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Salida!!">' +
                                    '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                    '</td>' +
                                    '</tr>');
                            swal("Registro Guardado!", "El tipo de salida  fue guardado satisfactoriamente.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');

                        }




                    }

                }, 'json');

                //




        }
    });

    $(document).ready(function() {


        $("#formAjax").validate();


    });




</script>