

<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
                        <input type="hidden" value="1" name="valido"  id="valido" />

                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="step1">

                                <input type="hidden" value="{if isset($_SolicitudPost[0]['pk_num_solicitud'])}{$_SolicitudPost[0]['pk_num_solicitud']}{/if}"  name="form[int][idPost]"      id="idPost" />



                                <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />

                                <div class="col-lg-6">
                                    <div class="col-lg-12">
                                        <div class="form-group">

                                            <div id="fechas" class="input-daterange input-group">
                                                <div class="input-group-content">
                                                    <input class="form-control" type="text" id="desde" name="desde"  readonly value="{if isset($_SolicitudPost[0]['fec_requerida'])}{$_SolicitudPost[0]['fec_requerida']}{/if}">
                                                    <label>Fecha a Solicitar</label>
                                                </div>

                                                <span class="input-group-addon">Hasta</span>
                                                <div class="input-group-content">
                                                    <input class="form-control" type="text"  id="hasta"  name="hasta" readonly value="{if isset($_SolicitudPost[0]['fec_hasta'])}{$_SolicitudPost[0]['fec_hasta']}{/if}">
                                                    <div class="form-control-line"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group col-lg-6">
                                            <input readonly type="text" name="form[txt][hora_desde]" class="form-control time12-mask" value="{if isset($_SolicitudPost[0]['hora_requerida'])}{$_SolicitudPost[0]['hora_requerida']}{/if}">
                                            <label>Hora de Salida</label>

                                        </div>

                                        <div class="form-group col-lg-6">
                                            <input readonly type="text"name="form[txt][hora_hasta]"  class="form-control time12-mask" value="{if isset($_SolicitudPost[0]['hora_hasta'])}{$_SolicitudPost[0]['hora_hasta']}{/if}">
                                            <label>Hora de Llegada</label>
                                        </div>
                                    </div>



                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <select disabled required="" class="form-control" name="form[int][TipoSalida]" id="TipoSalida" aria-required="true">
                                                <option value="">&nbsp;</option>


                                                {foreach item=tipo from=$_TipoSalidaPost}

                                                    {if isset($_SolicitudPost[0]['fk_a006_num_tipo_salida'])}

                                                        {if $tipo.pk_num_miscelaneo_detalle==$_SolicitudPost[0]['fk_a006_num_tipo_salida']}
                                                            <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {else}
                                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                    {/if}
                                                {/foreach}




                                            </select>
                                            <label for="rangelength2"> Tipo de Salida </label>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <select disabled required="" class="form-control" name="form[int][MotivoSalida]" id="MotivoSalida" aria-required="true">
                                                <option value="">&nbsp;</option>

                                                {foreach item=tipo from=$_MotivoSalidaPost}

                                                    {if isset($_SolicitudPost[0]['fk_a006_num_motivo_salida'])}

                                                        {if $tipo.pk_num_miscelaneo_detalle==$_SolicitudPost[0]['fk_a006_num_motivo_salida']}
                                                            <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {else}
                                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                    {/if}
                                                {/foreach}



                                            </select>
                                            <label for="rangelength2"> Motivo de Salida </label>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-lg-6">

                                    <div class="form-group  ">
                                        <textarea readonly placeholder="" rows="3" class="form-control" id="Observacion" name="form[txt][Observacion]">{if isset($_SolicitudPost[0]['ind_observacion'])}{$_SolicitudPost[0]['ind_observacion']}{/if}</textarea>
                                        <label for="textarea1">Observación</label>
                                    </div>

                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group  ">
                                        <textarea readonly placeholder="" rows="1" class="form-control" id="Estado_mantenimiento" name="form[txt][Estado_mantenimiento]">{if isset($_SolicitudPost[0]['Estado_mantenimiento'])}{$_SolicitudPost[0]['Estado_mantenimiento']}{/if}</textarea>
                                        <label for="textarea1">Estado del Mantenimiento </label>
                                    </div>

                                </div>

                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <input disabled class="form-control" type="text"  value="{if isset($_SolicitudPost[0]['ind_nombre1'])}{$_SolicitudPost[0]['ind_nombre1']} {$_SolicitudPost[0]['ind_apellido1']}{/if}"/>
                                        <label for="textarea1"> Funcionario Solicitante</label>
                                    </div>

                                </div>

                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <input disabled class="form-control" type="text"  value="{if isset($_SolicitudPost[0]['ind_nombre1'])}{$_SolicitudPost[0]['ind_nombre_conformado']} {$_SolicitudPost[0]['ind_apellido_conformado']}{/if}"/>
                                        <label for="textarea1"> Conformado Por</label>
                                    </div>

                                </div>


                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>
                                <span class="clearfix"></span>




                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                    {if  ($_Acciones.accion=="modificar")}
                                        <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Modificar</button>
                                    {else}
                                        <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Aprobar</button>
                                    {/if}

                                </div>


</form>



<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $("#formAjax" ).serialize();


            var accion = $("#accion").val();
            if(accion=="aprobar") {
                $.post("{$_Parametros.url}modPA/solicitudCONTROL/AprobarSolicitudMET", datos, function (dato) {
                    $(document.getElementById('idSolicitud'+ dato)).html('');

                }, 'json');
                swal("Solicitud Aprobada!", "La solicitud del vehículo ha sido aprobada.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }

        }
    });


    $(document).ready(function() {
        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });

        $.post("{$_Parametros.url}modPA/solicitudCONTROL/MantenimientoMET", { id:$("#idPost").val() } , function (dato) {
            $("#Estado_mantenimiento").val(dato['mensaje']);
            $("#Estado_mantenimiento").css("color", dato['color']);
            $("#Estado_mantenimiento").prop('disabled', true);
        }, 'json');

    });





</script>