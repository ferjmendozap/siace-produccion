<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">LISTAR CLASES DE VEHÍCULOS </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de las diferentes clases  existentes de vehículos.</h5>
    </div><!--end .col -->

</div><!--end .card -->


<div class="form-group   col-lg-12 ">

    <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
</div>

</div><!--end .card -->

<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">
                <div class="card-actionbar-row">

                </div>
            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">

                <input type="hidden" value="{if isset($_ClasePost[0].pk_num_miscelaneo_maestro)}{$_ClasePost[0].pk_num_miscelaneo_maestro}{/if}" name="idMaestro" id="idMaestro"/>

                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-2">N° </th>
                        <th class="sort-alpha col-sm-6">Clase de Vehículo</th>
                        <th class="sort-alpha col-sm-1"> Editar</th>
                        <th class="sort-alpha col-sm-1"> Eliminar</th>


                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_ClasePost}
                        <tr id="idPost{$post.cod_detalle}" class="gradeA">
                            <td>{$post.cod_detalle}</td>
                            <td>{$post.ind_nombre_detalle}</td>

                        <td>{if in_array('PA-01-06-05-01-02-M',$_Parametros.perfil)}
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Tipo de Vehículo" descipcion="El Usuario a Modificado un post" idClase="{$post.pk_num_miscelaneo_detalle}"  data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            {/if}
                        </td>
                        <td>{if in_array('PA-01-06-05-01-03-E',$_Parametros.perfil)}
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idClase="{$post.pk_num_miscelaneo_detalle}" mensaje="Estas seguro que desea eliminar la el post!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" idmenu="7">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            {/if}
                        </td>

                        </tr>






                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th  >
                            {if in_array('PA-01-06-05-01-01-N',$_Parametros.perfil)}
                                <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descripcion="El usuario a registrado una Clase de vehículo"  titulo="Registrar Clase Vehículo" id="nuevo" >
                                    <i class="md md-create"></i> Registrar Clase Vehícular &nbsp;&nbsp;
                                </button>
                            {/if}


                        </th>

                    </tr>
                    </tfoot>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->






    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modPA/claseVehiculoCONTROL/RegistrarClaseVehiculoMET';
            var maestro = $("#idMaestro").val();
            $('.nuevo').click(function(){
                $('#modalAncho').css( "width", "55%" );

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ maestro:maestro },function($dato){
                    $('#ContenidoModal').html($dato);
                });



            });



            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idClase: $(this).attr('idClase')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                var idClase=$(this).attr('idClase');
                swal({
                    title: $(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    var $url='{$_Parametros.url}modPA/claseVehiculoCONTROL/EliminarClaseMET';
                    $.post($url, { idClase: idClase},function($dato){
                        if($dato['status']=='OK'){
                            $(document.getElementById('idClase'+$dato['idClase'])).html('');
                            swal("Eliminado!", "La Clase ha sido eliminada.", "success");
                            $('#cerrar').click();
                        }
                    },'json');
                });
            });





        });
    </script>
