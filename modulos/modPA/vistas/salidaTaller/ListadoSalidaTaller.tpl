<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary">LISTAR SALIDAS DE VEHÍCULOS DEL TALLER</h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de las salidas de vehículos del taller.</h5>
        </div><!--end .col -->

    </div><!--end .card -->


    <div class="form-group   col-lg-12 ">

        <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
    </div>

</div><!--end .card -->

<section class="style-default-bright">





    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">

                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-2">Vehículo</th>
                        <th class="sort-alpha col-lg-3">Fecha Salida </th>
                        <th class="sort-alpha col-lg-3">Modelo-Placa</th>
                        <th class="sort-alpha col-lg-2">Pago</th>
                        <th class="sort-alpha col-lg-3">Observacion</th>

                        {if in_array('PA-01-04-02-02-M',$_Parametros.perfil)} <th class="  col-lg-1"> Modificar Salida</th>{/if}
                        {if in_array('PA-01-04-02-02-M',$_Parametros.perfil)}<th class="  col-lg-1"> Anular Salida</th>{/if}





                    </tr>
                    </thead>
                    <tbody>


                    {foreach item=post from=$_SalidaTallerPost}
                        <tr id="idSalida{$post.pk_num_salida_taller}" class="gradeA">
                            <td>{$post.pk_num_salida_taller }</td>
                            <td>
                                {if !is_null($post.ind_url_foto_frontal)&&trim($post.ind_url_foto_frontal)!=""&&trim($post.ind_url_foto_frontal)!="faceman_Suburban_Assault_Vehicle_(Front).svg"}

                                    <img src="{$_Parametros['ruta_Img']}modPA/{$post.ind_placa}/{$post.ind_url_foto_frontal}" class="height-1 width-1 img-circle "  />
                                {else}
                                    <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Front).svg" class="height-1 width-1 img-circle "  />

                                {/if}
                            </td>



                            <td>{$post.fec_salida} {$post.hora}</td>

                            <td>{$post.ind_modelo} {$post.ind_placa}</td>
                            <td>Bs. {$post.num_monto|number_format:2:",":"."}</td>
                            <td>{$post.ind_observacion}</td>




                            {if in_array('PA-01-04-02-02-M',$_Parametros.perfil)}
                                <td>
                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Salida Taller" descipcion="--" idSalida="{$post.pk_num_salida_taller}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            {/if}

                            {if in_array('PA-01-04-02-02-M',$_Parametros.perfil)}
                                <td>
                                    <button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSalida="{$post.pk_num_salida_taller}" mensaje="Estas seguro que desea anular la salida del vehículo!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" idmenu="7">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            {/if}



                        </tr>


                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th >

                            {if in_array('PA-01-04-02-01-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descripcion="Registro de salida de un vehículo del taller"  titulo="Registrar Salida Taller" id="nuevo" >
                                    <i class="md md-create"></i> Registrar Salida Taller &nbsp;&nbsp;
                                </button>
                            {/if}
                        </th>

                    </tr>
                    </tfoot>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->




    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modPA/salidaTallerCONTROL/RegistrarSalidaTallerMET';
            $('#nuevo').click(function(){
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });
            $('#modalAncho').css( "width", "85%" );




            $('#datatable1 tbody').on( 'click', '.modificar', function () {

                var $url='{$_Parametros.url}modPA/salidaTallerCONTROL/ModificarSalidaTallerMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idSalida')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.anular', function () {


                var $url='{$_Parametros.url}modPA/salidaTallerCONTROL/AnularSalidaTallerMET';
                var idPost=$(this).attr('idSalida');
                var idMenu=$(this).attr('idMenu');
                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modPA/salidaTallerCONTROL/AnularSalidaTallerMET';
                    $(document.getElementById('idSalida'+idPost)).html('');

                    $.post($url,{ idPost: idPost},function($dato){
                        swal("Eliminado!", "el registro fue eliminado satisfactoriamente.", "success");
                    });

                    //;
                });
            });

        });
    </script>
    </div>


