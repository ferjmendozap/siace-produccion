<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary">LISTAR DAÑOS DE VEHÍCULOS</h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de los daños en los vehículos registrados en la institución.</h5>
        </div><!--end .col -->

    </div><!--end .card -->




</div><!--end .card -->

<section class="style-default-bright">

    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-1">Vehículo</th>
                        <th class="sort-alpha col-sm-1">Fecha Registro</th>
                        <th class="sort-alpha col-sm-2">Modelo-Placa</th>

                        <th class="sort-alpha col-sm-4">Pieza Dañada</th>

                        <th class="sort-alpha col-sm-1">Modificar Daño</th>
                        <th class="sort-alpha col-sm-1">Anular Daño</th>
 






                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_DannioPost}
                        <tr id="idDanio{$post.pk_num_averia_vehiculo}" class="gradeA">
                            <td>{$post.pk_num_averia_vehiculo}</td>
                            <td>
                                {if !is_null($post.ind_url_foto_frontal)&&trim($post.ind_url_foto_frontal)!=""&&trim($post.ind_url_foto_frontal)!="faceman_Suburban_Assault_Vehicle_(Front).svg"}

                                    <img src="{$_Parametros['ruta_Img']}modPA/{$post.ind_placa}/{$post.ind_url_foto_frontal}" class="height-1 width-1 img-circle "  />
                                {else}
                                    <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Front).svg" class="height-1 width-1 img-circle "  />

                                {/if}
                            </td>

                            <td>{$post.fec_registro}</td>
                            <td>{$post.vehiculo} - {$post.ind_placa}</td>

                            <td>{$post.pieza}</td>



 
                            <td>{if in_array('PA-01-03-01-02-M',$_Parametros.perfil)}
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Daño Vehicular" descipcion="--" idDanio="{$post.pk_num_averia_vehiculo}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                                {/if}
                            </td>

                            <td>{if in_array('PA-01-03-01-03-A',$_Parametros.perfil)}
                            
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDanio="{$post.pk_num_averia_vehiculo}" mensaje="Estas seguro que desea eliminar la el post!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" idmenu="7">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                          
                                {/if}
                            </td>


                        </tr>








                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="7">
                            {if in_array('PA-01-03-01-01-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descripcion="Registro de daño a vehículo de la institución"  titulo="Registrar Daño" id="nuevo" >
                                    <i class="md md-create"></i> Registrar Daño &nbsp;&nbsp;
                                </button>
                            {/if}
                        </th>

                    </tr>
                    </tfoot>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->




    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modPA/dannioCONTROL/RegistrarDannioMET';
            $('#nuevo').click(function(){
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });
            $('#modalAncho').css( "width", "85%" );




            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modPA/dannioCONTROL/ModificarDannioMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idDanio')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.eliminar', function () {


                var $url='{$_Parametros.url}modPA/dannioCONTROL/AnularDannioMET';
                var idPost=$(this).attr('idDanio');
                var idMenu=$(this).attr('idMenu');
                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modPA/dannioCONTROL/AnularDannioMET';


                    $.post($url,{ idPost: idPost},function($dato){
                        swal("Eliminado!", "el menu fue eliminado satisfactoriamente.", "success");
                    });
                    $(document.getElementById('idDanio'+idPost)).html('');



                });
            });


        });
    </script>
    </div>




