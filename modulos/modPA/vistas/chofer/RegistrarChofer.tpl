<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido"  id="valido" />

                                        <div class="col-lg-6 form-group">




                                            <div class="col-sm-12">
                                                <div class="col-sm-10 form-group ">

                                                    <input type="text" class="form-control"
                                                           id="nombreProveedor"
                                                           value="{if isset($_ChoferBD[0]['ind_nombre1'])}{$_ChoferBD[0]['ind_nombre1']} {$_ChoferBD[0]['ind_apellido1']}{/if}"
                                                           readonly >

                                                    <input type="hidden" class="form-control"
                                                           id="codProveedor" name="idEmpleado"
                                                           value="{if isset($_ChoferBD[0]['fk_rhb001_num_empleado'])}{$_ChoferBD[0]['fk_rhb001_num_empleado']}{/if}"
                                                    >

                                                    <label for="rangelength2"> Chofer </label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group floating-label">
                                                        <button
                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                type="button"
                                                                data-toggle="modal"
                                                                data-target="#formModal2"
                                                                titulo="Listado de Personas"
                                                                {if isset($ver) and $ver==1}disabled{/if}
                                                                url="{$_Parametros.url}modPA/choferCONTROL/personaMET/persona/">
                                                            <i class="md md-search"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-lg-6 form-group">

                                            <input  required="" type="hidden" value="{$_ChoferBD[0]['pk_num_chofer']}"  name="idPost"    id="idPost" />

                                            <div class="form-group">
                                                <textarea placeholder=""  required=""   rows="3" class="form-control" id="ChoferObservacion" name="ChoferObservacion" >{if isset($_ChoferBD[0]['ind_observacion'])}{$_ChoferBD[0]['ind_observacion']}{/if}</textarea>
                                                <label for="textarea1">Observación</label>
                                            </div>

                                        </div>


                                                <input type="hidden" value="{$_Acciones.accion}"    id="accion" />



                                        <div class="row">
                                            &nbsp;
                                        </div>
                                        <div class="row">
                                            &nbsp;
                                        </div>
                                        <span class="clearfix"></span>

                                        <div class="modal-footer">

                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised"  ><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                                        </div>





                    </form>
                </div>


<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();

            if(accion=="registrar") {

                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });

                $.post("{$_Parametros.url}modPA/choferCONTROL/RegistrarChoferMET",datos, function (dato) {


                    if(dato['status']=='error') {


                        swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");

                    }else{


                        $(document.getElementById('datatable1')).append('<tr  id="idChofer'+dato['idChofer']+'">' +
                                '<td>'+dato['idChofer']+'</td>' +
                                '<td> '+dato['nombre']+' </td>' +
                                '<td   class="sort-alpha col-sm-1" >' +
                                '{if in_array('PA-01-06-11-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" idmodificar="'+dato['idChofer']+'"' +
                                'descipcion="El Usuario a Modificado un Grupo Ocupacional" titulo="Modificar Grupo Ocupacional">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('PA-01-06-11-01-03-E',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idChofer="'+dato['idChofer']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado un Grupo Ocupacional" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Grupo Ocupacional!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>' +
                                '</tr>');

                        swal("Registro Agregado!", "El Chofer ha sido agregado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                    }



                }, 'json');




            }else {

                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });

                $.post("{$_Parametros.url}modPA/choferCONTROL/ModificarChoferMET", datos, function (dato) {

                    $(document.getElementById('idChofer'+dato['idChofer'])).html('<td>'+dato['idChofer']+'</td>' +
                            '<td> '+dato['nombre']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-11-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idmodificar="'+dato['idChofer']+'"' +
                            'descipcion="El Usuario a Modificado un Grupo Ocupacional" titulo="Modificar Grupo Ocupacional">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-11-01-03-E',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idChofer="'+dato['idChofer']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Grupo Ocupacional" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Grupo Ocupacional!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');


                }, 'json');
                swal("Registro Modificado!", "El chofer ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }



        }
    });

    $(document).ready(function() {

        $("#formAjax").validate();

    });



    $('.accionModal').click(function () {

        accionModal(this,'url')
    });

    function accionModal(id,attr){

        $('#formModalLabel2').html($(id).attr('titulo'));
        $.post($(id).attr(attr), {
            cargar: 0,

            tr: $("#"+$(id).attr('idTabla')+" > tbody > tr").length + 1
        }, function ($dato) {
            $('#ContenidoModal2').html($dato);
        });
    }






</script>