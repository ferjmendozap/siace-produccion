<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class mantenimientoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetMantenimiento()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             pa_e002_mantenimiento.pk_num_mantenimiento,

             pa_e002_mantenimiento.ind_en_ocacion,
             pa_e002_mantenimiento.fec_proximo_mantenimiento,
             pa_e002_mantenimiento.ind_num_siguiente_kilometraje,
             pa_e002_mantenimiento.ind_observacion,
             pa_e002_mantenimiento.fk_pab001_num_vehiculo,
             DATE_FORMAT(pa_e002_mantenimiento.fec_mant,'%d/%m/%Y') as fec_mant,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_url_foto_frontal,
             pa_b001_vehiculo.ind_placa


             FROM `pa_e002_mantenimiento`

             left join  pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e002_mantenimiento.fk_pab001_num_vehiculo)
            "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetMantenimiento($ocasion,$fec_mantenimiento,$fec_prox_mantenimiento,$kilometraje,$observacion,$tipo,$vehiculo,$monto)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        $NuevoPost=$this->_db->prepare(
            "insert into  pa_e002_mantenimiento (ind_en_ocacion,fec_mant,fec_proximo_mantenimiento ,ind_num_siguiente_kilometraje,ind_observacion,fk_a018_num_seguridad_usuario,fec_ultima_modificacion,fk_a006_num_tipo_mantenimiento,fk_pab001_num_vehiculo,num_monto)
                                              values ( :ind_en_ocacion,:fec_mant,:fec_proximo_mantenimiento,:ind_num_siguiente_kilometraje,:ind_observacion,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion,:fk_a006_num_tipo_mantenimiento,:fk_pab001_num_vehiculo,:num_monto )"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':ind_en_ocacion' => $ocasion,
            ':fec_mant' => $fec_mantenimiento,
            ':fec_proximo_mantenimiento' => $fec_prox_mantenimiento,
            ':ind_num_siguiente_kilometraje' => $kilometraje,
            ':ind_observacion' => $observacion,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_a006_num_tipo_mantenimiento' => $tipo,
            ':fk_pab001_num_vehiculo' => $vehiculo,
            ':num_monto' => $monto
        ));

        $idPost= $this->_db->lastInsertId();

        #commit — Consigna una transacción
        $this->_db->commit();




        return $idPost;
    }


    #metodo para modificar el registro del post
    public function metUpdateMantenimiento($ocasion,$mantenimiento,$prox_mantenimiento,$kilometraje, $observacion ,$tipo,$vehiculo,$pago,$id)
    {
 
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar=$this->_db->prepare(
            "UPDATE pa_e002_mantenimiento
              SET
              ind_en_ocacion=:ind_en_ocacion,
              fec_mant=:fec_mant,
              fec_proximo_mantenimiento=:fec_proximo_mantenimiento,
              ind_num_siguiente_kilometraje=:ind_num_siguiente_kilometraje,
              ind_observacion=:ind_observacion,
              fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
              fec_ultima_modificacion=:fec_ultima_modificacion,
              fk_a006_num_tipo_mantenimiento=:fk_a006_num_tipo_mantenimiento,
              num_monto=:num_monto,
              fk_pab001_num_vehiculo=:fk_pab001_num_vehiculo
              WHERE pk_num_mantenimiento='".$id."'"
        );
        $modificar->execute(array(
            ':ind_en_ocacion' =>  $ocasion,
            ':fec_mant' => $mantenimiento,
            ':fec_proximo_mantenimiento' => $prox_mantenimiento,

            ':ind_num_siguiente_kilometraje' => $kilometraje,
            ':ind_observacion' => $observacion,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,

            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_pab001_num_vehiculo'=> $vehiculo,
            ':num_monto'=> $pago,
            ':fk_a006_num_tipo_mantenimiento'=> $tipo
        ));


        $this->_db->commit();



        return 1;
    }



    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
        "SELECT
             pa_e002_mantenimiento.pk_num_mantenimiento,
             pa_e002_mantenimiento.ind_en_ocacion,
             pa_e002_mantenimiento.num_monto,

             pa_e002_mantenimiento.fec_proximo_mantenimiento,
             pa_e002_mantenimiento.ind_num_siguiente_kilometraje,
             pa_e002_mantenimiento.ind_observacion,
             pa_e002_mantenimiento.fk_pab001_num_vehiculo,
             pa_e002_mantenimiento.fk_a006_num_tipo_mantenimiento,

             DATE_FORMAT(pa_e002_mantenimiento.fec_mant,'%d/%m/%Y') as fec_mant,
             DATE_FORMAT(pa_e002_mantenimiento.fec_proximo_mantenimiento,'%d/%m/%Y') as fec_proximo_mantenimiento,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa



        FROM `pa_e002_mantenimiento`

        left join  pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e002_mantenimiento.fk_pab001_num_vehiculo)

        where pa_e002_mantenimiento.pk_num_mantenimiento='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }





    public function metDeleteMantenimiento($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_e002_mantenimiento` " .
            "where pk_num_mantenimiento= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }





}
