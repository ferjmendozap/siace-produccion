<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class tallerModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
		$this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetTaller()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query( "SELECT
        lg_b022_proveedor.fk_a003_num_persona_proveedor,
        lg_b022_proveedor.pk_num_proveedor,
        a003_persona.ind_nombre1
        FROM   lg_b022_proveedor

        left join a003_persona on (a003_persona.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_proveedor)
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetCiudad()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT * FROM  a010_ciudad");
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetTaller($nombre,$documento,$direccion,$ciudad)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $Nuevo=$this->_db->prepare(
            "insert into   a003_persona (ind_cedula_documento ,ind_documento_fiscal,ind_nombre1,ind_direccion_domicilio_fiscal,ind_tipo_persona,fk_a006_num_estado_solicitud_persona,fk_a010_num_ciudad) values (:ind_cedula_documento ,:ind_documento_fiscal,:ind_nombre1,:ind_direccion_domicilio_fiscal,:ind_tipo_persona,:fk_a006_num_estado_solicitud_persona,:fk_a010_num_ciudad)"
        );

        #execute — Ejecuta una sentencia preparada
        $Nuevo->execute(array(
            ':ind_cedula_documento' => $documento,
            ':ind_documento_fiscal' => $documento,
            ':ind_nombre1'=> $nombre,
            ':ind_direccion_domicilio_fiscal' => $direccion,
            ':ind_tipo_persona' => "3",
            ':fk_a006_num_estado_solicitud_persona' => "1",
            ':fk_a010_num_ciudad' => $ciudad,

        ));

        $idPersona= $this->_db->lastInsertId();

        $Proveedor=$this->_db->prepare(
            "insert into lg_b022_proveedor (
             cod_tipo_pago,
             cod_forma_pago,
             cod_tipo_servicio,
             fec_constitucion,
             fec_emision_snc,
             fec_validacion_snc,
             ind_nacionalidad,
             ind_condicion_rcn,
             fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion,
             ind_calificacion,
             ind_nivel,
             num_capacidad_financiera,
             fk_lgb016_num_tipo_documento,
             fk_a003_num_persona_representante,
             fk_a003_num_persona_vendedor,
             fk_a003_num_persona_proveedor

             ) values (
             :cod_tipo_pago ,
             :cod_forma_pago,
             :cod_tipo_servicio,
             :fec_constitucion,
             :fec_emision_snc,
             :fec_validacion_snc,
             :ind_nacionalidad,
             :ind_condicion_rcn,
             :fk_a018_num_seguridad_usuario,
             :fec_ultima_modificacion,
             :ind_calificacion,
             :ind_nivel,
             :num_capacidad_financiera,
             :fk_lgb016_num_tipo_documento,
             :fk_a003_num_persona_representante,
             :fk_a003_num_persona_vendedor,
             :fk_a003_num_persona_proveedor
             )"
        );

        #execute — Ejecuta una sentencia preparada
        $Proveedor->execute(array(
            ':cod_tipo_pago' => "1",
            ':cod_forma_pago' => "1",
            ':cod_tipo_servicio'=> "1",
            ':fec_constitucion' => date('Y-m-d H:i:s'),
            ':fec_emision_snc' => date('Y-m-d H:i:s'),
            ':fec_validacion_snc' => date('Y-m-d H:i:s'),
            ':ind_nacionalidad' => "1",
            ':ind_condicion_rcn' => "1",
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario ,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':ind_calificacion' => "1",
            ':ind_nivel' => "1",
            ':num_capacidad_financiera' => "1",
            ':fk_lgb016_num_tipo_documento' => "1",
            ':fk_a003_num_persona_representante' =>$idPersona,
            ':fk_a003_num_persona_vendedor' => $idPersona,
            ':fk_a003_num_persona_proveedor' => $idPersona,


        ));


        $idPost= $this->_db->lastInsertId();
        #commit — Consigna una transacción
        $this->_db->commit();
        return $idPost;
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT * FROM `a003_persona` where pk_num_persona ='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metUpdateTaller($nombre,$documento,$direccion,$ciudad,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $modificar=$this->_db->prepare(
            "UPDATE a003_persona
              SET ind_cedula_documento=:ind_cedula_documento, ind_documento_fiscal=:ind_documento_fiscal,
                  ind_nombre1=:ind_nombre1,ind_direccion_domicilio_fiscal=:ind_direccion_domicilio_fiscal,fk_a010_num_ciudad=:fk_a010_num_ciudad WHERE pk_num_persona='".$id."'"
        );
        $modificar->execute(array(
            ':ind_cedula_documento' => $documento,
            ':ind_documento_fiscal' => $documento,
            ':ind_nombre1'=> $nombre,
            ':ind_direccion_domicilio_fiscal' => $direccion,

            ':fk_a010_num_ciudad' => $ciudad,
        ));



        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metDeleteTaller($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `a003_persona` " .
            "where pk_num_persona='$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }






}
