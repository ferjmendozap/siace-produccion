<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';

class piezaVehiculoModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
         $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetPiezaVehiculo()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("
        
        SELECT pa_b007_pieza_vehiculo.*,
        detalles_ubia.ind_nombre_detalle as nombre_ubia,
        detalles_ubib.ind_nombre_detalle as nombre_ubib,
        detalles_ubic.ind_nombre_detalle as nombre_ubic 
        
        FROM  pa_b007_pieza_vehiculo
        
        left join a005_miscelaneo_maestro as maestro_ubia on (maestro_ubia.cod_maestro='UBIA')
        left join a006_miscelaneo_detalle as detalles_ubia on (detalles_ubia.fk_a005_num_miscelaneo_maestro=maestro_ubia.pk_num_miscelaneo_maestro AND pa_b007_pieza_vehiculo.fk_a006_num_ubicacionA=detalles_ubia.pk_num_miscelaneo_detalle)
        
        left join a005_miscelaneo_maestro as maestro_ubib on (maestro_ubib.cod_maestro='UBIB')
        left join a006_miscelaneo_detalle as detalles_ubib on (detalles_ubib.fk_a005_num_miscelaneo_maestro=maestro_ubib.pk_num_miscelaneo_maestro AND pa_b007_pieza_vehiculo.fk_a006_num_ubicacionB=detalles_ubib.pk_num_miscelaneo_detalle)
        
        left join a005_miscelaneo_maestro as maestro_ubic on (maestro_ubic.cod_maestro='UBIC')
        left join a006_miscelaneo_detalle as detalles_ubic on (detalles_ubic.fk_a005_num_miscelaneo_maestro=maestro_ubic.pk_num_miscelaneo_maestro AND pa_b007_pieza_vehiculo.fk_a006_num_ubicacionC=detalles_ubic.pk_num_miscelaneo_detalle)
             
        ");
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetPiezaVehiculo($titulo,$UbicacionA,$UbicacionB,$UbicacionC)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into  pa_b007_pieza_vehiculo (ind_descripcion ,fk_a018_num_seguridad_usuario,fec_ultima_modificacion ,fk_a006_num_ubicacionA,fk_a006_num_ubicacionB,fk_a006_num_ubicacionC ) values ( :ind_descripcion,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion ,:fk_a006_num_ubicacionA,:fk_a006_num_ubicacionB,:fk_a006_num_ubicacionC )"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':ind_descripcion' => $titulo,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_a006_num_ubicacionA'=>$UbicacionA,
            ':fk_a006_num_ubicacionB'=>$UbicacionB,
            ':fk_a006_num_ubicacionC'=>$UbicacionC

        ));

        $idPost= $this->_db->lastInsertId();
        #commit — Consigna una transacción
        $this->_db->commit();
        return $idPost;
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT pk_num_piezas ,ind_descripcion , fk_a006_num_ubicacionA,fk_a006_num_ubicacionB,fk_a006_num_ubicacionC  FROM `pa_b007_pieza_vehiculo` where  pk_num_piezas='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metUpdatePiezaVehiculo($tipo,$ubicacionA,$ubicacionB,$ubicacionC,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update `pa_b007_pieza_vehiculo` set ind_descripcion = '$tipo' ,fk_a006_num_ubicacionA='".$ubicacionA."',fk_a006_num_ubicacionB='".$ubicacionB."',fk_a006_num_ubicacionC='".$ubicacionC."' ,fk_a018_num_seguridad_usuario = '".$this->atIdUsuario."' ,fec_ultima_modificacion ='".date('Y-m-d H:i:s')."'
             where pk_num_piezas= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metDeletePiezaVehiculo($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_b007_pieza_vehiculo` " .
            "where pk_num_piezas= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }






}
