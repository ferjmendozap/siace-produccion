<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class entradaInstitucionModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetEntradaInstitucion()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            " SELECT pa_e003_entrada_vehiculo.*,
              pa_e003_entrada_vehiculo.pk_num_entrada as pk_num_salida_vehiculo,
              pa_e001_salida_vehiculo.fk_pab001_num_vehiculo,
              pa_b001_vehiculo.ind_url_foto_frontal,
              pa_b001_vehiculo.ind_modelo,
              pa_b001_vehiculo.ind_placa

              FROM `pa_e003_entrada_vehiculo`
              left join pa_e001_salida_vehiculo on (pa_e001_salida_vehiculo.pk_num_salida_vehiculo=pa_e003_entrada_vehiculo.fk_pae001_num_salida_vehiculo)
              left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e001_salida_vehiculo.fk_pab001_num_vehiculo)
              --"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetEntradaInstitucion($hora_entrada,$kilometraje,$observacion,$salida,$idsolicitud,$gasolina)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into  `pa_e003_entrada_vehiculo` (fec_hora_entrada,ind_kilometraje_entrada,ind_observacion_entrada,fk_a006_miscelaneo_gasolina_ent,fk_a018_num_seguridad_usuario,fec_ultima_modificacion,fk_pae001_num_salida_vehiculo) values
                                                     (:fec_hora_entrada,:ind_kilometraje_entrada,:ind_observacion_entrada,:fk_a006_miscelaneo_gasolina_ent,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion,:fk_pae001_num_salida_vehiculo)"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':fec_hora_entrada' => $hora_entrada,
            ':ind_kilometraje_entrada' => $kilometraje,
            ':ind_observacion_entrada' =>$observacion,
            ':fk_a006_miscelaneo_gasolina_ent' => $gasolina,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_pae001_num_salida_vehiculo' =>$salida,

        ));

        $idPost= $this->_db->lastInsertId();

        $modificar=$this->_db->prepare(
            "UPDATE `pa_d001_solicitud_vehiculo`
              SET fk_a006_num_estado_solicitud=:fk_a006_num_estado_solicitud  WHERE 	pk_num_solicitud='".$idsolicitud."'"
        );


        $modificar->execute(array(
            ':fk_a006_num_estado_solicitud' => '7',
        ));


        $this->_db->commit();





        return $idPost;
    }


    #metodo para modificar el registro del post
    public function metUpdateEntradaInstitucion($hora_entrada,$kilometraje,$observacion ,$idEntrada,$gasolina)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar=$this->_db->prepare(
            "UPDATE pa_e003_entrada_vehiculo
              SET fec_hora_entrada=:fec_hora_entrada, ind_kilometraje_entrada=:ind_kilometraje_entrada,
                 ind_observacion_entrada=:ind_observacion_entrada,fk_a006_miscelaneo_gasolina_ent=:fk_a006_miscelaneo_gasolina_ent,fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,fec_ultima_modificacion=:fec_ultima_modificacion WHERE pk_num_entrada='".$idEntrada."'"
        );
        $modificar->execute(array(
            ':fec_hora_entrada' => $hora_entrada,
            ':ind_kilometraje_entrada' => $kilometraje,
            ':ind_observacion_entrada' =>$observacion,
            ':fk_a006_miscelaneo_gasolina_ent' => $gasolina,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),


        ));






        #commit — Consigna una transacción
        $this->_db->commit();


        return $idEntrada;
    }



    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
         DATE_FORMAT(pa_e003_entrada_vehiculo.fec_hora_entrada ,'%d/%m/%Y') as fec_entrada,
         DATE_FORMAT(pa_e003_entrada_vehiculo.fec_hora_entrada,'%h:%i %p') as hora_hasta,
         pa_e003_entrada_vehiculo.fk_a006_miscelaneo_gasolina_ent,
         pa_e003_entrada_vehiculo.ind_kilometraje_entrada,
         pa_e003_entrada_vehiculo.ind_observacion_entrada,
         pa_e003_entrada_vehiculo.fk_a018_num_seguridad_usuario,
         pa_e003_entrada_vehiculo.fk_pae001_num_salida_vehiculo,
         pa_e003_entrada_vehiculo.pk_num_entrada,
         pa_b001_vehiculo.ind_modelo,
         pa_b001_vehiculo.ind_placa,
         pa_e001_salida_vehiculo.fk_pab001_num_vehiculo,
		 pa_e001_salida_vehiculo.fk_pad001_num_solicitud_vehiculo
         

         FROM `pa_e003_entrada_vehiculo` 
		 left join pa_e001_salida_vehiculo on (pa_e001_salida_vehiculo.pk_num_salida_vehiculo=pa_e003_entrada_vehiculo.fk_pae001_num_salida_vehiculo)	
         left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e001_salida_vehiculo.fk_pab001_num_vehiculo)
			
		where   pk_num_entrada='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetch();
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrarSelects($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
         DATE_FORMAT(pa_e003_entrada_vehiculo.fec_hora_entrada ,'%d/%m/%Y') as fec_entrada,
         DATE_FORMAT(pa_e003_entrada_vehiculo.fec_hora_entrada,'%h:%i %p') as hora_hasta,

         pa_e003_entrada_vehiculo.ind_kilometraje_entrada,
         pa_e003_entrada_vehiculo.ind_observacion_entrada,
         pa_e003_entrada_vehiculo.fk_a018_num_seguridad_usuario,
         pa_e003_entrada_vehiculo.fk_pae001_num_salida_vehiculo,
         pa_e003_entrada_vehiculo.pk_num_entrada


            FROM `pa_e003_entrada_vehiculo` where   pk_num_entrada='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetch();
    }



    public function metDeleteEntradaInstitucion($id,$idsolicitud)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_e003_entrada_vehiculo` " .
            "where pk_num_entrada= '$id'"
        );



        $modificar=$this->_db->prepare(
            "UPDATE `pa_d001_solicitud_vehiculo`
              SET fk_a006_num_estado_solicitud=:fk_a006_num_estado_solicitud  WHERE 	pk_num_solicitud='".$idsolicitud."'"
        );


        $modificar->execute(array(
            ':fk_a006_num_estado_solicitud' => '6',
        ));


        #commit — Consigna una transacción
        $this->_db->commit();
    }






}
