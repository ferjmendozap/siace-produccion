<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class tipoVehiculoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetTipoVehiculo()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT * FROM  pa_b009_tipo_vehiculo");
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetTipoVehiculo($titulo,$pk_clase)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into  pa_b009_tipo_vehiculo (ind_descripcion, 	fk_a006_num_clase_vehiculo,fk_a018_num_seguridad_usuario,fec_ultima_modificacion) values ( :ind_descripcion, :fk_a006_num_clase_vehiculo,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion )"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':ind_descripcion' => $titulo,
            ':fk_a006_num_clase_vehiculo' => $pk_clase,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),
        ));
        $idPost= $this->_db->lastInsertId();
        #commit — Consigna una transacción
        $this->_db->commit();
        return $idPost;
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT pk_num_tipo_vehiculo ,ind_descripcion,fk_a006_num_clase_vehiculo FROM `pa_b009_tipo_vehiculo` where  pk_num_tipo_vehiculo='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metUpdateTipoVehiculo($tipo,$pk_clase,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update `pa_b009_tipo_vehiculo` set ind_descripcion = '$tipo',fk_a006_num_clase_vehiculo = '$pk_clase' ,fk_a018_num_seguridad_usuario ='".$this->atIdUsuario."',fec_ultima_modificacion = '".date('Y-m-d H:i:s')."' " .
            "where pk_num_tipo_vehiculo= '$id'"
        );
 
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metDeleteTipoVehiculo($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_b009_tipo_vehiculo` " .
            "where pk_num_tipo_vehiculo='$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }


    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrarSelect($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "

            SELECT a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle, a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,pa_b009_tipo_vehiculo.pk_num_tipo_vehiculo,pa_b009_tipo_vehiculo.ind_descripcion

			FROM a005_miscelaneo_maestro
left join a006_miscelaneo_detalle on 	(a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro)

INNER JOIN pa_b009_tipo_vehiculo on (pa_b009_tipo_vehiculo.fk_a006_num_clase_vehiculo=a006_miscelaneo_detalle.pk_num_miscelaneo_detalle AND a006_miscelaneo_detalle.cod_detalle='".$id."')
            WHERE cod_maestro='CLASEVEH'

            " );
        //echo "SQL ".$pruebaPost;
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }





}
