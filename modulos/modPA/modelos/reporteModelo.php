<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class reporteModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }


    #ejecuto la consulta a la base de datos
    public function metGetReporteInstitucion($vehiculo,$desde,$hasta,$motivo)
    {
        if($vehiculo!="error"){
            $complemento1=" AND pa_e001_salida_vehiculo.fk_pab001_num_vehiculo='".$vehiculo."'";
        }else{
            $complemento1=" ";
        }

        if($desde!="error"){
            $complemento2=" AND pa_e001_salida_vehiculo.fec_hora_salida>='".$desde."'";
        }else{
            $complemento2=" ";
        }

        if($hasta!="error"){
            $complemento3=" AND pa_e003_entrada_vehiculo.fec_hora_entrada<='".$hasta."'";
        }else{
            $complemento3=" ";
        }
        if($motivo!="error"){
            $complemento4=" AND pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida='".$motivo."'";
        }else{
            $complemento4=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT 
pa_e003_entrada_vehiculo.*,
             pa_e001_salida_vehiculo.ind_observacion,
             pa_e001_salida_vehiculo.fk_pab012_num_chofer,
             pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
             pa_b012_chofer.fk_rhb001_num_empleado,
     
             a003_persona.ind_nombre1,
             a003_persona.ind_apellido1,
             pa_e001_salida_vehiculo.fk_pab001_num_vehiculo,
             pa_b001_vehiculo.ind_modelo ,
             pa_b001_vehiculo.ind_placa,
             DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%d/%m/%Y') as fec_hora_salida,
             DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%h:%i %p') as hora_salida,
             DATE_FORMAT(pa_e003_entrada_vehiculo.fec_hora_entrada,'%d/%m/%Y') as fec_hora_entrada,
             DATE_FORMAT(pa_e003_entrada_vehiculo.fec_hora_entrada,'%h:%i %p') as hora_entrada,
             pa_e003_entrada_vehiculo.ind_kilometraje_entrada,

             a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
             a006_miscelaneo_detalle.ind_nombre_detalle as ind_motivo,

             tipoMaestro.pk_num_miscelaneo_maestro,
             tipoDetalle.ind_nombre_detalle as ind_tipo_salida,

             pa_e001_salida_vehiculo.ind_kilometraje,
             pa_e003_entrada_vehiculo.ind_observacion_entrada,
             
             DetalleCombustible.ind_nombre_detalle as combustible_salida,
DetalleCombustible2.ind_nombre_detalle as combustible_entrada
             
             FROM pa_e003_entrada_vehiculo



             left join pa_e001_salida_vehiculo on (pa_e001_salida_vehiculo.pk_num_salida_vehiculo= pa_e003_entrada_vehiculo.fk_pae001_num_salida_vehiculo )
             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e001_salida_vehiculo.fk_pab001_num_vehiculo)
             
             
             left join pa_b012_chofer on  (pa_b012_chofer.fk_rhb001_num_empleado=pa_e001_salida_vehiculo.fk_pab012_num_chofer)
             
             
   
             left join a003_persona on (a003_persona.pk_num_persona=pa_b012_chofer.fk_rhb001_num_empleado)
             left join pa_d001_solicitud_vehiculo on (pa_d001_solicitud_vehiculo.pk_num_solicitud=pa_e001_salida_vehiculo.fk_pad001_num_solicitud_vehiculo)

             left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='MOTSAL')
             left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida=a006_miscelaneo_detalle.pk_num_miscelaneo_detalle)

             left join a005_miscelaneo_maestro as tipoMaestro on (tipoMaestro.cod_maestro='TIPOSAL')
             left join a006_miscelaneo_detalle as tipoDetalle on (tipoDetalle.fk_a005_num_miscelaneo_maestro=tipoMaestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida=tipoDetalle.pk_num_miscelaneo_detalle)
             
               left join a005_miscelaneo_maestro as tipoMaestroCombustible on (tipoMaestroCombustible.cod_maestro='CGPA-CES')
             left join a006_miscelaneo_detalle as DetalleCombustible on (DetalleCombustible.fk_a005_num_miscelaneo_maestro=tipoMaestroCombustible.pk_num_miscelaneo_maestro AND pa_e001_salida_vehiculo.fk_a006_miscelaneo_gasolina_sal=DetalleCombustible.pk_num_miscelaneo_detalle)
             
             left join a006_miscelaneo_detalle as DetalleCombustible2 on (DetalleCombustible2.fk_a005_num_miscelaneo_maestro=tipoMaestroCombustible.pk_num_miscelaneo_maestro AND pa_e003_entrada_vehiculo.fk_a006_miscelaneo_gasolina_ent=DetalleCombustible2.pk_num_miscelaneo_detalle)
               where 1  $complemento1 $complemento2 $complemento3 $complemento4 ORDER by pa_e003_entrada_vehiculo.pk_num_entrada ASC "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #ejecuto la consulta a la base de datos
    public function metGetReporteEntradaTaller($vehiculo,$desde,$hasta)
    {
        if($vehiculo!="error"){
            $complemento1=" AND pa_b001_vehiculo.pk_num_vehiculo='".$vehiculo."'";
        }else{
            $complemento1=" ";
        }

        if($desde!="error"){
            $complemento2=" AND pa_e006_entrada_taller.fec_hora>='".$desde."'";
        }else{
            $complemento2=" ";
        }

        if($hasta!="error"){
            $complemento3=" AND pa_e006_entrada_taller.fec_hora<='".$hasta."'";
        }else{
            $complemento3=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT
             DATE_FORMAT(pa_e006_entrada_taller.fec_hora  ,'%d/%m/%Y') as fec_entrada,
             pa_e006_entrada_taller.ind_kilometraje,
             pa_e006_entrada_taller.fk_pab001_num_vehiculo,
             pa_e006_entrada_taller.fk_pab012_num_chofer,
             pa_e006_entrada_taller.pk_num_entrada_taller,
             pa_e006_entrada_taller.ind_motivo,
             pa_e006_entrada_taller.ind_observacion,
             pa_e006_entrada_taller.ind_cobertura,
             pa_b012_chofer.fk_rhb001_num_empleado,
           
             a003_persona.ind_nombre1,
             a003_persona.ind_apellido1,
             pa_b001_vehiculo.ind_modelo ,
             pa_b001_vehiculo.ind_placa,

             fk_a003_num_persona_proveedor,
             Taller.ind_nombre1 as nombreTaller



             FROM pa_e006_entrada_taller

             left join pa_b012_chofer on  (pa_b012_chofer.pk_num_chofer=pa_e006_entrada_taller.fk_pab012_num_chofer)
         
             left join a003_persona on (a003_persona.pk_num_persona=pa_b012_chofer.fk_rhb001_num_empleado)
             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e006_entrada_taller.fk_pab001_num_vehiculo  )
             left join lg_b022_proveedor on (lg_b022_proveedor.fk_a003_num_persona_proveedor=pa_e006_entrada_taller.fk_lgb022_num_proveedor)
             left join a003_persona as Taller on (Taller.pk_num_persona=fk_a003_num_persona_proveedor)


             where 1  $complemento1 $complemento2 $complemento3  "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metGetReporteSalidaTaller($vehiculo,$desde,$hasta)
    {
        if($vehiculo!="error"){
            $complemento1=" AND pa_b001_vehiculo.pk_num_vehiculo='".$vehiculo."'";
        }else{
            $complemento1=" ";
        }

        if($desde!="error"){
            $complemento2=" AND pa_e007_salida_taller.fec_hora>='".$desde."'";
        }else{
            $complemento2=" ";
        }

        if($hasta!="error"){
            $complemento3=" AND pa_e007_salida_taller.fec_hora<='".$hasta."'";
        }else{
            $complemento3=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT

             pa_e007_salida_taller.*,
             DATE_FORMAT( pa_e007_salida_taller.fec_hora  ,'%d/%m/%Y') as fec_salida,
             pa_b001_vehiculo.ind_modelo ,
             pa_b001_vehiculo.ind_placa,
             pa_e006_entrada_taller.fk_pab001_num_vehiculo,
             pa_e006_entrada_taller.ind_motivo,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa


             FROM pa_e007_salida_taller


             left join  pa_e006_entrada_taller on ( pa_e006_entrada_taller.pk_num_entrada_taller=pa_e007_salida_taller.fk_pae006_num_entrada_taller)

             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e006_entrada_taller.fk_pab001_num_vehiculo )



             where 1  $complemento1 $complemento2 $complemento3  "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metGetReporteMantenimiento($vehiculo,$desde,$hasta)
    {
        if($vehiculo!="error"){
            $complemento1=" AND pa_b001_vehiculo.pk_num_vehiculo='".$vehiculo."'";
        }else{
            $complemento1=" ";
        }

        if($desde!="error"){
            $complemento2=" AND pa_e002_mantenimiento.fec_mant>='".$desde."'";
        }else{
            $complemento2=" ";
        }

        if($hasta!="error"){
            $complemento3=" AND pa_e002_mantenimiento.fec_mant<='".$hasta."'";
        }else{
            $complemento3=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT
             pa_e002_mantenimiento.pk_num_mantenimiento,
             pa_e002_mantenimiento.ind_en_ocacion,
             pa_e002_mantenimiento.fec_proximo_mantenimiento,
             pa_e002_mantenimiento.ind_num_siguiente_kilometraje,

             pa_e002_mantenimiento.ind_observacion,
             pa_e002_mantenimiento.fk_pab001_num_vehiculo,
             DATE_FORMAT(pa_e002_mantenimiento.fec_mant,'%d/%m/%Y') as fec_mant,
             DATE_FORMAT(pa_e002_mantenimiento.fec_proximo_mantenimiento,'%d/%m/%Y') as fec_proximo_mantenimiento,
             pa_b001_vehiculo.ind_modelo,

             pa_b001_vehiculo.ind_placa,
             mantenimientoMaestro.pk_num_miscelaneo_maestro,
             mantenimientoDetalle.ind_nombre_detalle as tipoMantenimiento

             FROM `pa_e002_mantenimiento`

             left join  pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e002_mantenimiento.fk_pab001_num_vehiculo)


             left join a005_miscelaneo_maestro as mantenimientoMaestro on (mantenimientoMaestro.cod_maestro='TIPOMANT')
             
             left join a006_miscelaneo_detalle as mantenimientoDetalle on (mantenimientoDetalle.fk_a005_num_miscelaneo_maestro=mantenimientoMaestro.pk_num_miscelaneo_maestro 
                                                                           AND pa_e002_mantenimiento.fk_a006_num_tipo_mantenimiento=mantenimientoDetalle.pk_num_miscelaneo_detalle)
             where 1 $complemento1 $complemento2 $complemento3  "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metGetReporteDannio($vehiculo,$desde,$hasta)
    {
        if($vehiculo!="error"){
            $complemento1=" AND pa_b001_vehiculo.pk_num_vehiculo='".$vehiculo."'";
        }else{
            $complemento1=" ";
        }

        if($desde!="error"){
            $complemento2=" AND pa_e004_averia_vehiculo.fec_registro>='".$desde."'";
        }else{
            $complemento2=" ";
        }

        if($hasta!="error"){
            $complemento3=" AND pa_e004_averia_vehiculo.fec_registro<='".$hasta."'";
        }else{
            $complemento3=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT
             pa_e004_averia_vehiculo.pk_num_averia_vehiculo,
             pa_e004_averia_vehiculo.ind_observacion_averia,
             DATE_FORMAT(pa_e004_averia_vehiculo.fec_registro ,'%d/%m/%Y') as fec_registro,
             pa_e004_averia_vehiculo.fk_pab007_num_piezas,

             DATE_FORMAT(pa_e008_solucion_averia.fec_solucion ,'%d/%m/%Y') as fec_solucion,
             pa_b007_pieza_vehiculo.ind_descripcion  as pieza,
             pa_b001_vehiculo.ind_modelo  ,
             pa_e008_solucion_averia.pk_num_solucion,
             FORMAT(pa_e008_solucion_averia.num_monto,2) as num_monto,
             pa_b001_vehiculo.ind_placa
             FROM `pa_e004_averia_vehiculo`

             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e004_averia_vehiculo.fk_pab001_num_vehiculo)
             left join pa_b007_pieza_vehiculo on (pa_b007_pieza_vehiculo.pk_num_piezas=pa_e004_averia_vehiculo.fk_pab007_num_piezas)
             left join  pa_e008_solucion_averia on (pa_e008_solucion_averia.fk_pae004_num_averia_vehiculo =pa_e004_averia_vehiculo.pk_num_averia_vehiculo)

             where 1 $complemento1 $complemento2 $complemento3 order by  pa_e004_averia_vehiculo.fk_pab001_num_vehiculo,pa_e004_averia_vehiculo.fec_registro "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



    public function metGetReporteAsignaciones($vehiculo,$desde,$hasta)
    {
        if($vehiculo!="error"){
            $complemento1=" AND pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo='".$vehiculo."'";
        }else{
            $complemento1=" ";
        }

        if($desde!="error"){
            $complemento2=" AND pa_d001_solicitud_vehiculo.fec_requerida>='".$desde."'";
        }else{
            $complemento2=" ";
        }

        if($hasta!="error"){
            $complemento3=" AND pa_d001_solicitud_vehiculo.fec_requerida<='".$hasta."'";
        }else{
            $complemento3=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT
        pa_d001_solicitud_vehiculo.pk_num_solicitud,
         DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%d/%m/%Y') as fec_requerida,
         DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%d/%m/%Y') as fec_hasta,

         DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%h:%i %p') as hora_requerida,
         DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%h:%i %p') as hora_hasta,

        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante ,
        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
        pa_d001_solicitud_vehiculo.fec_aprobado,

        pa_d001_solicitud_vehiculo.ind_observacion,
        pa_d001_solicitud_vehiculo.ind_motivo_rechazo,
        pa_d001_solicitud_vehiculo.fk_a018_num_seguridad_usuario,
        pa_d001_solicitud_vehiculo.fec_ultima_modificacion,
        pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
        pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida,
        pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo,
        a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
        a006_miscelaneo_detalle.ind_nombre_detalle  as estatus,
        pa_b001_vehiculo.ind_modelo,
        pa_b001_vehiculo.ind_placa,

        rh_b001_empleado.fk_a003_num_persona,
        a003_persona.ind_nombre1,
        a003_persona.ind_apellido1

        FROM `pa_d001_solicitud_vehiculo`

        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='ESTADOSOL')
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud=a006_miscelaneo_detalle.cod_detalle )

        left join pa_b001_vehiculo ON (pa_b001_vehiculo.pk_num_vehiculo=pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo)
        left join rh_b001_empleado on (rh_b001_empleado.fk_a003_num_persona=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante)
        left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
        where  pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud>='5' AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud!='8' $complemento1 $complemento2 $complemento3
          "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metGetReporteSolicitud($vehiculo,$desde,$hasta,$estatus,$motivo)
    {
        if($vehiculo!="error"){
            $complemento1=" AND pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo='".$vehiculo."'";
        }else{
            $complemento1=" ";
        }

        if($desde!="error"){
            $complemento2=" AND pa_d001_solicitud_vehiculo.fec_requerida>='".$desde."'";
        }else{
            $complemento2=" ";
        }

        if($hasta!="error"){
            $complemento3=" AND pa_d001_solicitud_vehiculo.fec_requerida<='".$hasta."'";
        }else{
            $complemento3=" ";
        }


        if($estatus!="error"){
            $complemento4=" AND pa_b015_estatus.cod_detalle='".$estatus."'";
        }else{
            $complemento4=" ";
        }


        if($motivo!="error"){
            $complemento5=" AND pa_b013_motivo_salida.fk_a006_num_motivo_salida='".$motivo."'";
        }else{
            $complemento5=" ";
        }



        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT pa_d001_solicitud_vehiculo.pk_num_solicitud,
              DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_preparacion,'%d/%m/%Y') as fec_preparacion,
               pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante ,
               pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
               pa_d001_solicitud_vehiculo.fec_aprobado,
               pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud,
               pa_d001_solicitud_vehiculo.ind_observacion,
               pa_d001_solicitud_vehiculo.ind_motivo_rechazo,
               pa_d001_solicitud_vehiculo.fk_a018_num_seguridad_usuario,
               pa_d001_solicitud_vehiculo.fec_ultima_modificacion,
               pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
               pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida,
               pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo,
               pa_b001_vehiculo.ind_modelo,
               pa_b001_vehiculo.ind_placa,
               a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
               a006_miscelaneo_detalle.ind_nombre_detalle as estatus,
               motivoMaestro.pk_num_miscelaneo_maestro,
               motivoDetalle.ind_nombre_detalle as ind_motivo,
               rh_b001_empleado.fk_a003_num_persona,
               a003_persona.ind_nombre1,
               a003_persona.ind_apellido1,
               
               tipoMaestro.pk_num_miscelaneo_maestro,
               tipoDetalle.ind_nombre_detalle as ind_tipo_salida

            FROM `pa_d001_solicitud_vehiculo` 
              left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='ESTADOSOL') 
              left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud=a006_miscelaneo_detalle.cod_detalle )
              left join a005_miscelaneo_maestro as motivoMaestro on (motivoMaestro.cod_maestro='MOTSAL') 
              left join a006_miscelaneo_detalle as motivoDetalle on (motivoDetalle.fk_a005_num_miscelaneo_maestro=motivoMaestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida=motivoDetalle.pk_num_miscelaneo_detalle) 
              left join pa_b001_vehiculo ON (pa_b001_vehiculo.pk_num_vehiculo=pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo) 
              left join rh_b001_empleado on (rh_b001_empleado.fk_a003_num_persona=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante) 
              left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona) 
              left join a005_miscelaneo_maestro as tipoMaestro on (tipoMaestro.cod_maestro='TIPOSAL')
              left join a006_miscelaneo_detalle as tipoDetalle on (tipoDetalle.fk_a005_num_miscelaneo_maestro=tipoMaestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida=tipoDetalle.pk_num_miscelaneo_detalle) 
    

        
            where  pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud>='5' AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud!='8' $complemento1 $complemento2 $complemento3 $complemento4 $complemento5
          "
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metGetReporteDisponibilidad()
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "		SELECT
        pa_b001_vehiculo.ind_modelo,
        pa_b001_vehiculo.ind_placa,
        pa_b001_vehiculo.num_estatus,
        claseMaestro.pk_num_miscelaneo_maestro,
        claseDetalle.ind_nombre_detalle as ind_descripcion,
        pa_b001_vehiculo.fk_a006_num_clase_vehiculo,
        pa_b001_vehiculo.fk_pab009_num_tipo_vehiculo,
        a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
        a006_miscelaneo_detalle.ind_nombre_detalle as ind_marca,
claseDetalle.cod_detalle,
pa_b001_vehiculo.fk_pab009_num_tipo_vehiculo,

		pa_b009_tipo_vehiculo.ind_descripcion as ind_tipo

		FROM `pa_b001_vehiculo`

	    left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='MARVEH')
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_b001_vehiculo.fk_a006_num_marca_vehiculo=a006_miscelaneo_detalle.cod_detalle)

        left join a005_miscelaneo_maestro as claseMaestro on (claseMaestro.cod_maestro='CLASEVEH')
        left join a006_miscelaneo_detalle as claseDetalle on (claseDetalle.fk_a005_num_miscelaneo_maestro=claseMaestro.pk_num_miscelaneo_maestro AND pa_b001_vehiculo.fk_a006_num_clase_vehiculo=claseDetalle.cod_detalle)


        left join pa_b009_tipo_vehiculo on (pa_b009_tipo_vehiculo.pk_num_tipo_vehiculo=pa_b001_vehiculo.fk_pab009_num_tipo_vehiculo)
          "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

}
