<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/


require_once 'trait'.DS.'consultasTrait.php';
require_once 'obligacionModelo.php';
require_once RUTA_Modulo . 'modCB' . DS . 'modelos' . DS . 'ListaVoucherModelo.php';
class imprimirTransferirModelo extends Modelo

{
    use consultasTrait;
    private $atIdUsuario;
    public $atObligacionModelo;
    public $atListaVoucherModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atObligacionModelo = new obligacionModelo();
        $this->atListaVoucherModelo = new ListaVoucherModelo();
    }

    public function metListarPago()
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d010_pago.pk_num_pago,
              cp_d010_pago.ind_num_pago,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_pago, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_pago,2), 2), '.', -1)
                ) AS num_monto_pago,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_retenido, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_retenido,2), 2), '.', -1)
                ) AS num_monto_retenido,
              a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              a006_miscelaneo_detalle.ind_nombre_detalle AS a006_miscelaneo_detalle_tipoPago,
              cp_d009_orden_pago.ind_num_orden
            FROM
              cp_d010_pago
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
            INNER JOIN a006_miscelaneo_detalle ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle


             WHERE
              cp_d010_pago.ind_estado = 'GE'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metListarImpuestos($idObligacion)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d012_obligacion_impuesto.num_monto_afecto,
              cp_d012_obligacion_impuesto.num_monto_impuesto,
              cp_b015_impuesto.pk_num_impuesto,
              a006_miscelaneo_detalle.cod_detalle
            FROM
              cp_d001_obligacion
              INNER JOIN cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
              INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
              INNER JOIN a006_miscelaneo_detalle ON cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_tipo_comprobante = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
             WHERE
              pk_num_obligacion='$idObligacion' AND
              (
                a006_miscelaneo_detalle.cod_detalle='IVA' OR
                a006_miscelaneo_detalle.cod_detalle='ISLR' OR
                a006_miscelaneo_detalle.cod_detalle='1X1000' OR
                a006_miscelaneo_detalle.cod_detalle='DESCCS'
              )
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metBuscarNroCheque($idCuenta)
    {
        $listar = $this->_db->query(
            "SELECT
                cp_b012_chequera.pk_num_chequera,
                cp_b012_chequera.ind_num_cheque,
                cp_b012_chequera.num_chequera_desde,
                cp_b012_chequera.num_chequera_hasta,
                cp_b012_chequera.ind_num_chequera
             FROM
                cp_b012_chequera
             WHERE
                fk_cpb014_num_cuenta_bancaria = '$idCuenta' and cp_b012_chequera.num_estatus = 1
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metActualizarNroCheque($idChequera, $numero)
    {
        $this->_db->beginTransaction();
        $actualizarNroCheque = $this->_db->prepare("
            UPDATE
                cp_b012_chequera
            SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                ind_num_cheque=:ind_num_cheque
            WHERE
              pk_num_chequera=:pk_num_chequera
        ");
        $actualizarNroCheque->execute(array(
            'ind_num_cheque' => $numero,
            'pk_num_chequera' => $idChequera
        ));
        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCambioChequera($idCuenta, $numeroChequera, $estado)
    {
        $this->_db->beginTransaction();
        $actualizarNroCheque = $this->_db->prepare("
            UPDATE
                cp_b012_chequera
            SET
                    fec_ultima_modificacion = NOW(),
                    fk_a018_num_seguridad_usuario = '$this->atIdUsuario',
                    num_estatus = '$estado'
             WHERE
                fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria
                and ind_num_chequera=:ind_num_chequera
        ");
        $actualizarNroCheque->execute(array(
            'fk_cpb014_num_cuenta_bancaria' => $idCuenta,
            'ind_num_chequera' => $numeroChequera
        ));
        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
    public function metBuscarDistribucion($idObligacion)
    {
        $listar = $this->_db->query(
            "SELECT
                pk_num_estado_distribucion,
                num_monto,
                fk_prc002_num_presupuesto_det,
                fk_lgb019_num_orden,
                pr_c002_presupuesto_det.fk_prb002_num_partida_presupuestaria
              FROM
                pr_d008_estado_distribucion
              INNER JOIN pr_c002_presupuesto_det on pr_c002_presupuesto_det.pk_num_presupuesto_det = pr_d008_estado_distribucion.fk_prc002_num_presupuesto_det
              WHERE
                fk_cpd001_num_obligacion='$idObligacion' AND
                ind_estado='AC' AND
                ind_tipo_distribucion='CA'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metBuscarDistribucionParcial($idOrdenPago, $partida = false)
    {
        if($partida){
            $and = "AND fk_prb002_num_partida_presupuestaria = '$partida'";
        }else{
            $and = "";
        }
        $listar = $this->_db->query(
            "SELECT
                  pk_num_orden_pago_parcial,
                  num_monto
                FROM
                  cp_d015_orden_pago_parcial
                WHERE
                  fk_cpd009_num_orden_pago = '$idOrdenPago' 
                  AND num_estatus = '0' 
                  $and
                ORDER BY 
                  num_secuencia_pago ASC 
                LIMIT 1
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }


    public function metActualizarDetallePP($idDetallePP)
    {

        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
                  cp_d015_orden_pago_parcial
                SET
                  num_estatus=:num_estatus
                WHERE
                fk_cpd009_num_orden_pago='$idDetallePP'
               ");
        $NuevoRegistro->execute(array(
            ':num_estatus' => 1
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metConsultarPagoSeleccionado($idPago)
    {
        $listar = $this->_db->query(
            "SELECT
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_pago, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_pago,2), 2), '.', -1)
                ) AS num_monto_pago,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_retenido, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_retenido,2), 2), '.', -1)
                ) AS num_monto_retenido,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_pago+cp_d010_pago.num_monto_retenido, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_pago+cp_d010_pago.num_monto_retenido,2), 2), '.', -1)
                ) AS montoTotal,
                CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS fk_a003_num_persona_proveedor_a_pagar,
                CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS fk_a003_num_persona_proveedor,
              
                a018_seguridad_usuario.ind_usuario,
                cp_d010_pago.fec_ultima_modificacion,
                a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
                cp_b014_cuenta_bancaria.ind_num_cuenta,
                a006_miscelaneo_detalle.ind_nombre_detalle AS a006_miscelaneo_detalle_tipoPago,
                cp_d009_orden_pago.ind_num_orden,
                cp_d009_orden_pago.fk_cpd001_num_obligacion,
                cp_d010_pago.pk_num_pago,
                cp_d010_pago.ind_num_pago,
                date_format(cp_d010_pago.fec_pago,'%d-%m-%Y') AS fec_pago,
                cp_d010_pago.fk_cpd009_num_orden_pago,
                cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago,
                cp_d001_obligacion.fk_cpb014_num_cuenta,
                cp_d009_orden_pago.ind_estado AS ind_estado_orden,
                cp_d001_obligacion.ind_comentarios,
                cp_d001_obligacion.num_flag_presupuesto,
                cp_d001_obligacion.fk_cpb002_num_tipo_documento,
                cp_d001_obligacion.fk_a023_num_centro_de_costo,
                cp_d001_obligacion.fec_factura,
                cp_d001_obligacion.ind_nro_control,
                cp_d001_obligacion.fk_cpb014_num_cuenta,
                cp_d010_pago.num_monto_pago AS pago,
                cp_d010_pago.num_monto_retenido AS retenido,
                a003_persona.pk_num_persona

            FROM
              cp_d010_pago
              INNER JOIN cp_d009_orden_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
              INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
              INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar = a003_persona.pk_num_persona
              INNER JOIN a003_persona AS a003_persona_proveedor ON cp_d001_obligacion.fk_a003_num_persona_proveedor = a003_persona_proveedor.pk_num_persona
              INNER JOIN a018_seguridad_usuario ON cp_d010_pago.fk_a018_num_seguridad_usuario = a018_seguridad_usuario.pk_num_seguridad_usuario
              INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
              INNER JOIN a006_miscelaneo_detalle ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle

            WHERE
              pk_num_pago='$idPago'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metImprimirTransferir($idPago, $chequeUsuario = false, $chequeSis = false)
    {
        $this->_db->beginTransaction();
        $idOrdenPago = $this->metConsultarPagoSeleccionado($idPago);
        $idImpuestos = $this->metListarImpuestos($idOrdenPago['fk_cpd001_num_obligacion']);

        $Contrato=$this->atObligacionModelo->metConsultarObligacionGC($idOrdenPago['fk_cpd001_num_obligacion']);

        if($idOrdenPago['ind_estado_orden'] == 'PP'){
            $estadoOrden = 'PP';
        }else{
            $estadoOrden = 'PA';
        }

        $actualizarPago = $this->_db->prepare("
            UPDATE
                cp_d010_pago
            SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                ind_estado='IM',
                ind_num_cheque=:ind_num_cheque,
                ind_cheque_usuario=:ind_cheque_usuario
            WHERE
              pk_num_pago=:pk_num_pago
        ");
        $actualizarPago->execute(array(
            'pk_num_pago' => $idPago,
            'ind_num_cheque' => $chequeSis,
            'ind_cheque_usuario' => $chequeUsuario
        ));
        $actualizarOrdenPago = $this->_db->prepare("
            UPDATE
                cp_d009_orden_pago
            SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                ind_estado='$estadoOrden'
            WHERE
              pk_num_orden_pago=:pk_num_orden_pago
        ");
        $actualizarOrdenPago->execute(array(
            'pk_num_orden_pago' => $idOrdenPago['fk_cpd009_num_orden_pago']
        ));
        $actualizarObligacionPago = $this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                ind_estado='PA'
            WHERE
              pk_num_obligacion=:pk_num_obligacion
        ");
        $actualizarObligacionPago->execute(array(
            'pk_num_obligacion' => $idOrdenPago['fk_cpd001_num_obligacion']
        ));
        $afectarPresupuesto = $this->_db->prepare("
            INSERT INTO
                pr_d008_estado_distribucion
            SET
                fec_periodo=NOW(),
                num_monto=:num_monto,
                ind_tipo_distribucion='PA',
                ind_estado='AC',
                fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                fk_prd008_num_estado_distribucion=:fk_prd008_num_estado_distribucion,
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                fk_lgb019_num_orden=:fk_lgb019_num_orden,
                fk_gcb001_num_contrato=:fk_gcb001_num_contrato
        ");
        $distribucion = $this->metBuscarDistribucion($idOrdenPago['fk_cpd001_num_obligacion']);

        if($idOrdenPago['ind_estado_orden'] == 'PP' or $idOrdenPago['ind_estado_orden'] == 'GP'){
            $tipoPago = 'pagoParcial';
        }else{
            $tipoPago = 'pago';
        }
        foreach ($distribucion AS $dist) {
            if($tipoPago == 'pagoParcial'){

                $distribucionParcial = $this->metBuscarDistribucionParcial($idOrdenPago['fk_cpd009_num_orden_pago'],$dist['fk_prb002_num_partida_presupuestaria']);
                $monto = $distribucionParcial['num_monto'];
                $NuevoRegistro=$this->_db->prepare(
                    "UPDATE
                  cp_d015_orden_pago_parcial
                SET
                  num_estatus=:num_estatus
                WHERE
                pk_num_orden_pago_parcial= '".$distribucionParcial['pk_num_orden_pago_parcial']."'
               ");
                $NuevoRegistro->execute(array(
                    ':num_estatus' => 1
                ));
            }else{
                $monto = $dist['num_monto'];
            }
            if($monto != NULL){
                $afectarPresupuesto->execute(array(
                    'fk_cpd001_num_obligacion' => $idOrdenPago['fk_cpd001_num_obligacion'],
                    'num_monto' => $monto,
                    'fk_prc002_num_presupuesto_det' => $dist['fk_prc002_num_presupuesto_det'],
                    'fk_prd008_num_estado_distribucion' => $dist['pk_num_estado_distribucion'],
                    'fk_lgb019_num_orden' => $dist['fk_lgb019_num_orden'],
                    'fk_gcb001_num_contrato'=>$Contrato['idContrato']
                ));
            }
        }
        $tipoTransaccion = $this->metBuscarTipoTransaccion(Session::metObtener('TRANSPAGO'));
        if ($tipoTransaccion['cod_detalle'] == 'E') {
            $signo = -1;
        } else {
            $signo = 1;
        }
        $bancoTransaccion = $this->_db->prepare("
            INSERT INTO
                cp_d011_banco_transaccion
            SET
                fk_cpb006_num_banco_tipo_transaccion =:fk_cpb006_num_banco_tipo_transaccion,
                fk_cpb002_num_tipo_documento =:fk_cpb002_num_tipo_documento,
                fk_a023_num_centro_costo =:fk_a023_num_centro_costo,
                fk_cpd010_num_pago = '$idPago',
                fk_rhb001_num_empleado_crea = '$this->atIdEmpleado',
                ind_num_transaccion =:ind_num_transaccion,
                ind_periodo_contable =:ind_periodo_contable,
                ind_estado = 'AP',
                fec_anio = NOW(),
                fec_transaccion = NOW(),
                fec_ultima_modificacion=NOW(),
                num_monto=:num_monto,
                fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
                num_secuencia=:num_secuencia,
                num_flag_presupuesto =:num_flag_presupuesto,
                txt_comentarios =:txt_comentarios,
                ind_mes =LPAD( MONTH (NOW()),2,'0'),
                fec_preparacion=NOW(),
                fk_a003_num_persona_proveedor =:fk_a003_num_persona_proveedor,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
        ");
        $secuencia = $this->metSecuencia('cp_d011_banco_transaccion', 'ind_num_transaccion');
        $bancoTransaccion->execute(array(
            'fk_cpb006_num_banco_tipo_transaccion' => $tipoTransaccion['pk_num_banco_tipo_transaccion'],
            'fk_cpb002_num_tipo_documento' => $idOrdenPago['fk_cpb002_num_tipo_documento'],
            'fk_a023_num_centro_costo' => $idOrdenPago['fk_a023_num_centro_de_costo'],
            'ind_num_transaccion' => $secuencia['secuencia'],
            'ind_periodo_contable' => date('Y-m'),
            'num_monto' => $idOrdenPago['pago'] * $signo,
            'fk_cpb014_num_cuenta_bancaria' => $idOrdenPago['fk_cpb014_num_cuenta'],
            'num_secuencia' => 1,
            'num_flag_presupuesto' => $idOrdenPago['num_flag_presupuesto'],
            'txt_comentarios' => $idOrdenPago['ind_comentarios'],
            'fk_a003_num_persona_proveedor' => $idOrdenPago['pk_num_persona']
        ));
        $retencionesReporte = $this->_db->prepare("
            INSERT INTO
                cp_e001_retenciones
            SET
                fk_b015_num_impuesto=:fk_b015_num_impuesto,
                fec_anio = NOW(),
                ind_num_comprobante=:ind_num_comprobante,
                ind_num_control=:ind_num_control,
                ind_tipo_comprobante=:ind_tipo_comprobante,
                ind_periodo_fiscal=:ind_periodo_fiscal,
                fec_comprobante = NOW(),
                fec_factura=:fec_factura,
                num_monto_afecto=:num_monto_afecto,
                num_monto_no_afecto=:num_monto_no_afecto,
                num_monto_impuesto=:num_monto_impuesto,
                num_monto_factura=:num_monto_factura,
                num_monto_retenido=:num_monto_retenido,
                ind_estado = 'PA',
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_cpd010_num_orden_pago=:fk_cpd010_num_orden_pago
        ");

        foreach ($idImpuestos AS $idImpuesto) {
            $secuencia = $this->metSecuencia('cp_e001_retenciones', 'ind_num_comprobante', 'ind_tipo_comprobante', $idImpuesto['cod_detalle']);
            $retencionesReporte->execute(array(
                'fk_b015_num_impuesto' => $idImpuesto['pk_num_impuesto'],
                'ind_num_comprobante' => str_pad($secuencia['secuencia'], 8, "0", STR_PAD_LEFT),
                'ind_num_control' => $idOrdenPago['ind_nro_control'],
                'ind_tipo_comprobante' => $idImpuesto['cod_detalle'],
                'ind_periodo_fiscal' => date('Y-m'),
                'fec_factura' => $idOrdenPago['fec_factura'],
                'num_monto_afecto' => $idImpuesto['num_monto_afecto'],
                'num_monto_no_afecto' => 0,
                'num_monto_impuesto' => $idImpuesto['num_monto_impuesto'],
                'num_monto_factura' => $idOrdenPago['pago'],
                'num_monto_retenido' => $idOrdenPago['retenido'],
                'fk_cpd010_num_orden_pago' => $idOrdenPago['fk_cpd009_num_orden_pago']
            ));
        }

        $error1 = $actualizarPago->errorInfo();
        $error2 = $actualizarOrdenPago->errorInfo();
        $error3 = $actualizarObligacionPago->errorInfo();
        $error4 = $afectarPresupuesto->errorInfo();
        $error5 = $bancoTransaccion->errorInfo();
        $error6 = $retencionesReporte->errorInfo();

        if ((!empty($error1[1]) && !empty($error1[2]))) {
            $this->_db->rollBack();
            return $error1;
        } elseif ((!empty($error2[1]) && !empty($error2[2]))) {
            $this->_db->rollBack();
            return $error2;
        } elseif ((!empty($error3[1]) && !empty($error3[2]))) {
            $this->_db->rollBack();
            return $error3;
        } elseif ((!empty($error4[1]) && !empty($error4[2]))) {
            $this->_db->rollBack();
            return $error4;
        } elseif ((!empty($error5[1]) && !empty($error5[2]))) {
            $this->_db->rollBack();
            return $error5;
        } elseif ((!empty($error6[1]) && !empty($error6[2]))) {
            $this->_db->rollBack();
            return $error6;
        } else {
            $this->_db->commit();
            return $idPago . '-' . $chequeUsuario . '-' . $tipoPago;
        }
    }

    public function metVouchersPago($idPago, $codVocuher, $idObligacion = false)
    {
        $this->_db->beginTransaction();
        $actualizarPago = $this->_db->prepare("
            UPDATE
                cp_d010_pago
            SET
              fk_cbb001_num_voucher_pago=:fk_cbb001_num_voucher_pago
            WHERE
              pk_num_pago='$idPago'
        ");
        $actualizarPago->execute(array(
            ':fk_cbb001_num_voucher_pago' => $codVocuher
        ));
        $actualizarOblig = $this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
              num_contabilizacion_pendiente=:num_contabilizacion_pendiente
            WHERE
              pk_num_obligacion='$idObligacion'
        ");
        $actualizarOblig->execute(array(
            ':num_contabilizacion_pendiente' => 1
        ));
        $fallaTansaccion = $actualizarPago->errorInfo();
        $fallaTansaccion = $actualizarPago->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metVoucherAnulacion($idPago, $codVoucher)
    {
        $this->_db->beginTransaction();
        $actualizarPago = $this->_db->prepare("
            UPDATE
                cp_d010_pago
            SET
              fk_cbb001_num_voucher_anulacion=:fk_cbb001_num_voucher_anulacion
            WHERE
              pk_num_pago='$idPago'
        ");
        $actualizarPago->execute(array(
            ':fk_cbb001_num_voucher_anulacion' => $codVoucher
        ));
        $fallaTansaccion = $actualizarPago->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metImpuesto($idObligacion)
    {
        $impuesto = $this->_db->query("
            SELECT
              ABS(SUM(cp_d012_obligacion_impuesto.num_monto_afecto)) AS Monto
            FROM
              cp_d012_obligacion_impuesto
            WHERE
              cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion='$idObligacion'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }
}


