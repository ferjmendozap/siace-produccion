<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class impuestoModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
    }

    public function metCuentaListar($cuenta)
    {   if($cuenta == 'cuentaPub20' or $cuenta == 'cuentaAdePub20') {
            $where = "WHERE	num_flag_tipo_cuenta='1'";
        }else {
        $where = "WHERE	num_flag_tipo_cuenta='2'";
        }

        $cuentaListar =  $this->_db->query(
            "SELECT
              *
            FROM
              cb_b004_plan_cuenta
            $where
            ORDER BY cod_cuenta ASC
             " );
        //var_dump($cuentaListar);
        $cuentaListar->setFetchMode(PDO::FETCH_ASSOC);
        return $cuentaListar->fetchAll();
    }


    public function metImpuestoListar($idImpuesto = false)
    {
        if($idImpuesto){
            $where = "WHERE i.pk_num_impuesto = '$idImpuesto'";
        }else{
            $where = "";
        }
        $impuestoListar =  $this->_db->query(
            "SELECT
                i.*,
                rf.ind_nombre_detalle AS RegimenFiscal,
                Tipo.ind_nombre_detalle As Tipo,
                Provision.ind_nombre_detalle As Provision,
                imponible.cod_detalle AS imponible,
                imponible.ind_nombre_detalle AS nombreImponible
              FROM
              cp_b015_impuesto AS i
              INNER JOIN a006_miscelaneo_detalle AS rf ON i.fk_a006_num_miscelaneo_detalle_regimen_fiscal = rf.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle AS Tipo ON i.fk_a006_num_miscelaneo_detalle_tipo_comprobante = Tipo.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle AS Provision ON i.fk_a006_num_miscelaneo_detalle_flag_provision = Provision.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle as imponible ON imponible.pk_num_miscelaneo_detalle = i.fk_a006_num_miscelaneo_detalle_flag_imponible

              $where
              ORDER BY cod_impuesto ASC
             " );
        //var_dump($impuestoListar);

        $impuestoListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idImpuesto){
            return $impuestoListar->fetch();
        }else{
            return $impuestoListar->fetchAll();
        }

    }

    public function metImpuestoConsultaDetalle($idImpuesto)
    {
        $consultaImpuesto =  $this->_db->query(
            "SELECT
                i.*,
                rf.ind_nombre_detalle AS RegimenFiscal,
                Tipo.ind_nombre_detalle As Tipo,
                Provision.ind_nombre_detalle As Provision,
                i.fec_ultima_modificacion,
                a018.ind_usuario,
                cuenta.cod_cuenta AS cuenta,
                cuentaPub20.cod_cuenta AS cuentaPub20,
                pr_b002_partida_presupuestaria.cod_partida

              FROM
              cp_b015_impuesto AS i
              INNER JOIN a018_seguridad_usuario AS a018 ON a018.pk_num_seguridad_usuario = i.fk_a018_num_seguridad_usuario
              INNER JOIN a006_miscelaneo_detalle AS rf ON rf.pk_num_miscelaneo_detalle = i.fk_a006_num_miscelaneo_detalle_regimen_fiscal
              INNER JOIN a006_miscelaneo_detalle AS Tipo ON i.fk_a006_num_miscelaneo_detalle_tipo_comprobante = Tipo.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle AS Provision ON i.fk_a006_num_miscelaneo_detalle_flag_provision = Provision.pk_num_miscelaneo_detalle
              LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = i.fk_cbb004_num_plan_cuenta
              LEFT JOIN cb_b004_plan_cuenta AS cuentaPub20 ON cuentaPub20.pk_num_cuenta = i.fk_cbb004_num_plan_cuenta_pub20
              LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = i.fk_prb002_num_partida_presupuestaria
               WHERE
              i.pk_num_impuesto = '$idImpuesto' "
        );
        //var_dump($consultaImpuesto);
        $consultaImpuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaImpuesto->fetch();
    }

    public function metNuevoImpuesto($codImpuesto,$txtDescripcion,$indSigno,$indFactorPrcentaje,
                                     $indFlagPrrovision,$indFlagImponible,$IndFlagGeneral,
                                     $indTipoComprobante,$partida,$cuenta20,$cuenta,$fkCodRegimenFiscal,$estatus)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
              cp_b015_impuesto
              SET
               	cod_impuesto=:cod_impuesto,
                txt_descripcion=:txt_descripcion,
                ind_signo=:ind_signo,
                num_factor_porcentaje=:num_factor_porcentaje,
                fk_a006_num_miscelaneo_detalle_flag_provision=:fk_a006_num_miscelaneo_detalle_flag_provision,
                fk_a006_num_miscelaneo_detalle_flag_imponible=:fk_a006_num_miscelaneo_detalle_flag_imponible,
                fk_a006_num_miscelaneo_detalle_flag_general=:fk_a006_num_miscelaneo_detalle_flag_general,
                fk_a006_num_miscelaneo_detalle_tipo_comprobante=:fk_a006_num_miscelaneo_detalle_tipo_comprobante,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                num_estatus=:num_estatus,
                fk_a006_num_miscelaneo_detalle_regimen_fiscal=:fk_a006_num_miscelaneo_detalle_regimen_fiscal,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
               ");
        $NuevoRegistro->execute(array(
            ':cod_impuesto' => $codImpuesto,
            ':txt_descripcion' => $txtDescripcion,
            ':ind_signo' => $indSigno,
            ':num_factor_porcentaje' => $indFactorPrcentaje,
            ':fk_a006_num_miscelaneo_detalle_flag_provision' => $indFlagPrrovision,
            ':fk_a006_num_miscelaneo_detalle_flag_imponible' => $indFlagImponible,
            ':fk_a006_num_miscelaneo_detalle_flag_general' => $IndFlagGeneral,
            ':fk_a006_num_miscelaneo_detalle_tipo_comprobante' => $indTipoComprobante,
            ':fk_prb002_num_partida_presupuestaria' => $partida,
            ':fk_cbb004_num_plan_cuenta_pub20' => $cuenta20,
            ':fk_cbb004_num_plan_cuenta' => $cuenta,
            ':num_estatus' => $estatus,
            ':fk_a006_num_miscelaneo_detalle_regimen_fiscal' => $fkCodRegimenFiscal
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }



    }

    public function metModificarImpuesto($idImpuesto,$txtDescripcion,$indSigno,$indFactorPrcentaje,
                                         $indFlagPrrovision,$indFlagImponible,$IndFlagGeneral,
                                         $indTipoComprobante,$partida,$cuenta20,$cuenta,$fkCodRegimenFiscal,$estatus)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              cp_b015_impuesto
              SET
                txt_descripcion=:txt_descripcion,
                ind_signo=:ind_signo,
                num_factor_porcentaje=:num_factor_porcentaje,
                fk_a006_num_miscelaneo_detalle_flag_provision=:fk_a006_num_miscelaneo_detalle_flag_provision,
                fk_a006_num_miscelaneo_detalle_flag_imponible=:fk_a006_num_miscelaneo_detalle_flag_imponible,
                fk_a006_num_miscelaneo_detalle_flag_general=:fk_a006_num_miscelaneo_detalle_flag_general,
                fk_a006_num_miscelaneo_detalle_tipo_comprobante=:fk_a006_num_miscelaneo_detalle_tipo_comprobante,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                num_estatus=:num_estatus,
                fk_a006_num_miscelaneo_detalle_regimen_fiscal=:fk_a006_num_miscelaneo_detalle_regimen_fiscal,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
                WHERE
                pk_num_impuesto='$idImpuesto'
               ");
        $NuevoRegistro->execute(array(
            ':txt_descripcion' => $txtDescripcion,
            ':ind_signo' => $indSigno,
            ':num_factor_porcentaje' => $indFactorPrcentaje,
            ':fk_a006_num_miscelaneo_detalle_flag_provision' => $indFlagPrrovision,
            ':fk_a006_num_miscelaneo_detalle_flag_imponible' => $indFlagImponible,
            ':fk_a006_num_miscelaneo_detalle_flag_general' => $IndFlagGeneral,
            ':fk_a006_num_miscelaneo_detalle_tipo_comprobante' => $indTipoComprobante,
            ':fk_prb002_num_partida_presupuestaria' => $partida,
            ':fk_cbb004_num_plan_cuenta_pub20' => $cuenta20,
            ':fk_cbb004_num_plan_cuenta' => $cuenta,
            ':num_estatus' => $estatus,
            ':fk_a006_num_miscelaneo_detalle_regimen_fiscal' => $fkCodRegimenFiscal
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


}
