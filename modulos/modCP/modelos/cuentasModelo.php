<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class cuentasModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atVoucherModelo;
    public function __construct()
    {
        parent::__construct();

        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();

    }
    public function metCuentaListar($idCuenta = false)
    {
        if($idCuenta){
            $where = "WHERE c.pk_num_cuenta = '$idCuenta'";
        }else{
            $where = "";
        }
        $cuentaListar =  $this->_db->query(
            "SELECT
                b.ind_nombre_detalle AS banco,
                c.ind_descripcion,
                c.ind_num_cuenta,
                c.num_estatus,
                c.pk_num_cuenta
              FROM
                cp_b014_cuenta_bancaria AS c
              INNER JOIN  a006_miscelaneo_detalle AS b ON c.fk_a006_num_miscelaneo_detalle_banco = b.pk_num_miscelaneo_detalle
              $where
        ");
        //var_dump($cuentaListar);
        $cuentaListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idCuenta){
            return $cuentaListar->fetch();
        }else{
            return $cuentaListar->fetchAll();
        }
    }

    public function metMostrarTipoPago($idCuenta)
    {
        $consulta = $this->_db->query(
            "SELECT
              *
             FROM
              cp_b019_cuenta_bancaria_tipo_pago
            INNER JOIN
            cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_b019_cuenta_bancaria_tipo_pago.fk_cpb014_num_cuenta_bancaria
            WHERE
            cp_b014_cuenta_bancaria.pk_num_cuenta= $idCuenta"
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metCuentaConsultaDetalle($idCuenta)
    {
        $consultaCuenta =  $this->_db->query(
            "SELECT
            cuenta.*,
            banco.ind_nombre_detalle,
            tipoC.ind_nombre_detalle,
            a018.ind_usuario,
            planCuenta.cod_cuenta AS cuenta,
            planCuenta20.cod_cuenta AS cuenta20,
            tipoPago.pk_num_cuenta_bancaria_tipo_pago,
            tipoPago.fk_cpb014_num_cuenta_bancaria,
            tipoPago.fk_a006_num_miscelaneo_detalle_tipo_pago
            FROM
            cp_b014_cuenta_bancaria AS cuenta
            INNER JOIN a018_seguridad_usuario AS a018 ON a018.pk_num_seguridad_usuario = cuenta.fk_a018_num_seguridad_usuario
            INNER JOIN a006_miscelaneo_detalle AS banco ON banco.pk_num_miscelaneo_detalle = cuenta.fk_a006_num_miscelaneo_detalle_banco
            INNER JOIN a006_miscelaneo_detalle AS tipoC ON tipoC.pk_num_miscelaneo_detalle = cuenta.fk_a006_num_miscelaneo_detalle_tipo_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS planCuenta ON planCuenta.pk_num_cuenta = cuenta.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS planCuenta20 ON planCuenta20.pk_num_cuenta = cuenta.fk_cbb004_num_plan_cuenta_pub20
            INNER JOIN cp_b019_cuenta_bancaria_tipo_pago AS tipoPago ON tipoPago.fk_cpb014_num_cuenta_bancaria = cuenta.pk_num_cuenta

            WHERE
            cuenta.pk_num_cuenta =$idCuenta "
        );
        $consultaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaCuenta->fetch();
    }

    public function metCuentaConsulta($idCuenta)
    {
        $consultaCuenta =  $this->_db->query(
            "SELECT
            cuenta.*,
            banco.ind_nombre_detalle,
            tipoC.ind_nombre_detalle,
            a018.ind_usuario,
            planCuenta.cod_cuenta AS cuenta,
            planCuenta20.cod_cuenta AS cuenta20
            FROM
            cp_b014_cuenta_bancaria AS cuenta
            INNER JOIN a018_seguridad_usuario AS a018 ON a018.pk_num_seguridad_usuario = cuenta.fk_a018_num_seguridad_usuario
            INNER JOIN a006_miscelaneo_detalle AS banco ON banco.pk_num_miscelaneo_detalle = cuenta.fk_a006_num_miscelaneo_detalle_banco
            INNER JOIN a006_miscelaneo_detalle AS tipoC ON tipoC.pk_num_miscelaneo_detalle = cuenta.fk_a006_num_miscelaneo_detalle_tipo_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS planCuenta ON planCuenta.pk_num_cuenta = cuenta.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS planCuenta20 ON planCuenta20.pk_num_cuenta = cuenta.fk_cbb004_num_plan_cuenta_pub20

            WHERE
            cuenta.pk_num_cuenta =$idCuenta
        ");
        $consultaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaCuenta->fetch();
    }


    public function metNuevaCuenta($form)
    {
        $this->_db->beginTransaction();
        $nuevaTransaccion = $this->_db->prepare(
            "INSERT INTO
             cp_b014_cuenta_bancaria
              SET
               fk_a006_num_miscelaneo_detalle_banco=:fk_a006_num_miscelaneo_detalle_banco,
               ind_num_cuenta=:ind_num_cuenta,
               ind_descripcion=:ind_descripcion,
               fk_a006_num_miscelaneo_detalle_tipo_cuenta=:fk_a006_num_miscelaneo_detalle_tipo_cuenta,
               fec_apertura=:fec_apertura,
               fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
               fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
               ind_agencia=:ind_agencia,
               ind_distrito=:ind_distrito,
               ind_atencion=:ind_atencion,
               ind_cargo=:ind_cargo,
               num_flag_debito_bancario=:num_flag_debito_bancario,
               num_flag_conciliacion_cp=:num_flag_conciliacion_cp,
               num_flag_conciliacion_bancaria=:num_flag_conciliacion_bancaria,
               num_estatus=:num_estatus,
               num_saldo_inicial=:num_saldo_inicial,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $nuevaTransaccion->execute(array(
            'fk_a006_num_miscelaneo_detalle_banco' => $form['fk_a006_num_miscelaneo_detalle_banco'],
            'ind_num_cuenta' => $form['ind_num_cuenta'],
            'ind_descripcion' => $form['ind_descripcion'],
            'fk_a006_num_miscelaneo_detalle_tipo_cuenta' => $form['fk_a006_num_miscelaneo_detalle_tipo_cuenta'],
            'fec_apertura' => $form['fec_apertura'],
            'fk_cbb004_num_plan_cuenta' => $form['fk_cbb004_num_plan_cuenta'],
            'fk_cbb004_num_plan_cuenta_pub20'=>$form['fk_cbb004_num_plan_cuenta_pub20'],
            'ind_agencia' => $form['ind_agencia'],
            'ind_distrito' => $form['ind_distrito'],
            'ind_atencion' => $form['ind_atencion'],
            'ind_cargo' => $form['ind_cargo'],
            'num_flag_debito_bancario' => $form['num_flag_debito_bancario'],
            'num_flag_conciliacion_cp' => $form['num_flag_conciliacion_cp'],
            'num_flag_conciliacion_bancaria' => $form['num_flag_conciliacion_bancaria'],
            'num_estatus' => $form['num_estatus'],
            'num_saldo_inicial' => $form['num_saldo_inicial']
        ));
        $idCuenta = $this->_db->lastInsertId();
        #  Tipo Pago
        $nuevo = $this->_db->prepare("
                  INSERT INTO
                  cp_b019_cuenta_bancaria_tipo_pago
                  SET
                  fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
                  fk_a006_num_miscelaneo_detalle_tipo_pago=:fk_a006_num_miscelaneo_detalle_tipo_pago,
                  int_numero_ultimo_cheque=:int_numero_ultimo_cheque,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");
        if(isset($form['nuevo'])) {
            $detalle = $form['nuevo'];
            $secuencia = 1;
            foreach ($detalle['secuencia'] AS $i) {
                $ids = $detalle[$i];
                $nuevo->execute(array(
                    ':fk_cpb014_num_cuenta_bancaria' => $idCuenta,
                    ':fk_a006_num_miscelaneo_detalle_tipo_pago' => $ids['pk_tipo_pago'],
                    ':int_numero_ultimo_cheque' => $ids['int_numero_ultimo_cheque']
                ));
                $secuencia++;
            }
        }
        ##Cuenta Bancaria Balance
        $cuentaBalance = $this->_db->prepare("
                  INSERT INTO
                  cp_d006_cuenta_bancaria_balance
                  SET
                  ind_comentarios=:ind_comentarios,
                  fec_transaccion=NOW(),
                  num_monto_transaccion=:num_monto_transaccion,
                  num_saldo_inicial=:num_saldo_inicial,
                  num_saldo_actual=:num_saldo_actual,
                  fk_cpb014_pk_num_cuenta_bancaria=:fk_cpb014_pk_num_cuenta_bancaria,
                  fec_anio=YEAR(NOW()),
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");
        $cuentaBalance->execute(array(
            ':ind_comentarios' => $form['ind_comentarios'],
            ':num_monto_transaccion' => $form['num_saldo_inicial'],
            ':num_saldo_inicial' => $form['num_saldo_inicial'],
            ':num_saldo_actual' => $form['num_saldo_inicial'],
            ':fk_cpb014_pk_num_cuenta_bancaria' => $idCuenta,

        ));

        $fallaTansaccion = $nuevaTransaccion->errorInfo();
        $fallaTansaccion2 = $nuevo->errorInfo();
        $fallaTansaccion3 = $cuentaBalance->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            var_dump($fallaTansaccion);
            return $fallaTansaccion;
        }elseif ( !empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }elseif ( (!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion3;

        } else {
            $this->_db->commit();
            return $idCuenta;
        }

    }

    public function metModificarCuenta($idCuenta,$form)
    {
        $this->_db->beginTransaction();
        $nuevaTransaccion = $this->_db->prepare(
            "UPDATE
             cp_b014_cuenta_bancaria
              SET
               fk_a006_num_miscelaneo_detalle_banco=:fk_a006_num_miscelaneo_detalle_banco,
               ind_num_cuenta=:ind_num_cuenta,
               ind_descripcion=:ind_descripcion,
               fk_a006_num_miscelaneo_detalle_tipo_cuenta=:fk_a006_num_miscelaneo_detalle_tipo_cuenta,
               fec_apertura=:fec_apertura,
               fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
               fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
               ind_agencia=:ind_agencia,
               ind_distrito=:ind_distrito,
               ind_atencion=:ind_atencion,
               ind_cargo=:ind_cargo,
               num_flag_debito_bancario=:num_flag_debito_bancario,
               num_flag_conciliacion_cp=:num_flag_conciliacion_cp,
               num_flag_conciliacion_bancaria=:num_flag_conciliacion_bancaria,
               num_estatus=:num_estatus,
               num_saldo_inicial=:num_saldo_inicial,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               WHERE
               pk_num_cuenta='$idCuenta'
               ");
        $nuevaTransaccion->execute(array(
            'fk_a006_num_miscelaneo_detalle_banco' => $form['fk_a006_num_miscelaneo_detalle_banco'],
            'ind_num_cuenta' => $form['ind_num_cuenta'],
            'ind_descripcion' => $form['ind_descripcion'],
            'fk_a006_num_miscelaneo_detalle_tipo_cuenta' => $form['fk_a006_num_miscelaneo_detalle_tipo_cuenta'],
            'fec_apertura' => $form['fec_apertura'],
            'fk_cbb004_num_plan_cuenta' => $form['fk_cbb004_num_plan_cuenta'],
            'fk_cbb004_num_plan_cuenta_pub20'=>$form['fk_cbb004_num_plan_cuenta_pub20'],
            'ind_agencia' => $form['ind_agencia'],
            'ind_distrito' => $form['ind_distrito'],
            'ind_atencion' => $form['ind_atencion'],
            'ind_cargo' => $form['ind_cargo'],
            'num_flag_debito_bancario' => $form['num_flag_debito_bancario'],
            'num_flag_conciliacion_cp' => $form['num_flag_conciliacion_cp'],
            'num_flag_conciliacion_bancaria' => $form['num_flag_conciliacion_bancaria'],
            'num_estatus' => $form['num_estatus'],
            'num_saldo_inicial' => $form['num_saldo_inicial']
        ));
        #  Tipo Pago
        $this->_db->query("DELETE FROM cp_b019_cuenta_bancaria_tipo_pago WHERE fk_cpb014_num_cuenta_bancaria='$idCuenta'");
        if(isset($form['nuevo'])) {
            $detalle = $form['nuevo'];
            $secuencia = 1;
            foreach ($detalle['secuencia'] AS $i) {
                $ids = $detalle[$i];

                $nuevo = $this->_db->prepare("
                INSERT INTO
                  cp_b019_cuenta_bancaria_tipo_pago
                  SET
                  fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
                  fk_a006_num_miscelaneo_detalle_tipo_pago=:fk_a006_num_miscelaneo_detalle_tipo_pago,
                  int_numero_ultimo_cheque=:int_numero_ultimo_cheque,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 ");

                $nuevo->execute(array(
                    ':fk_cpb014_num_cuenta_bancaria' => $idCuenta,
                    ':fk_a006_num_miscelaneo_detalle_tipo_pago' => $ids['pk_tipo_pago'],
                    ':int_numero_ultimo_cheque' => $ids['int_numero_ultimo_cheque']
                ));
                $secuencia++;
            }
        }
        ##Cuenta Bancaria Balance
        $cuentaBalance = $this->_db->prepare("
                  UPDATE
                      cp_d006_cuenta_bancaria_balance
                  SET
                      ind_comentarios=:ind_comentarios,
                      fec_transaccion=NOW(),
                      num_monto_transaccion=:num_monto_transaccion,
                      num_saldo_inicial=:num_saldo_inicial,
                      num_saldo_actual=:num_saldo_actual,
                      fec_anio=YEAR(NOW()),
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  WHERE
                      fk_cpb014_pk_num_cuenta_bancaria='$idCuenta'
                  ");
        $cuentaBalance->execute(array(
            ':ind_comentarios' => $form['ind_comentarios'],
            ':num_monto_transaccion' => $form['num_saldo_inicial'],
            ':num_saldo_inicial' => $form['num_saldo_inicial'],
            ':num_saldo_actual' => $form['num_saldo_inicial']
        ));
        $fallaTansaccion = $nuevaTransaccion->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $this->_db->commit();
            return $idCuenta;
        }

    }

    public function metBuscarTipoPago($idCuenta)
    {
        $consulta =  $this->_db->query(
            "SELECT
              *
            FROM
            cp_b019_cuenta_bancaria_tipo_pago
            WHERE
             fk_cpb014_num_cuenta_bancaria = $idCuenta
            ");
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metBuscarIdTipoPago($idTipoPago){
        $consulta =  $this->_db->query(
            "SELECT
              a006_miscelaneo_detalle.ind_nombre_detalle AS tipoPago
            FROM
            a006_miscelaneo_detalle
            INNER JOIN a005_miscelaneo_maestro ON a005_miscelaneo_maestro.pk_num_miscelaneo_maestro = a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro
            WHERE
             a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = '$idTipoPago'
            ");
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metEliminarCuenta($idCuenta){

        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
            DELETE FROM
            cp_b014_cuenta_bancaria
             WHERE
            pk_num_cuenta=:pk_num_cuenta
              ");
        $elimar->execute(array(
            'pk_num_cuenta'=>$idCuenta
        ));
        $elimarTipoPago=$this->_db->prepare("
            DELETE FROM
            cp_b019_cuenta_bancaria_tipo_pago
             WHERE
            fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria
              ");
        $elimarTipoPago->execute(array(
            'fk_cpb014_num_cuenta_bancaria'=>$idCuenta
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCuenta;
        }
    }

}