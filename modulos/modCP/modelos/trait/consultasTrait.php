<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

trait consultasTrait
{
    public function metSecuencia($tabla, $campo, $campoBusqueda = false,$valorBusqueda=false)
    {
        if($campoBusqueda){
            $campoBusqueda="WHERE $campoBusqueda = '$valorBusqueda'";
        }
        $secuencia = $this->_db->query(
            "SELECT
                  IF(
                        MAX($campo),
                        MAX($campo)+1,
                        1
                  ) AS secuencia
            FROM $tabla $campoBusqueda"
        );
        $secuencia->setFetchMode(PDO::FETCH_ASSOC);
        return $secuencia->fetch();
    }

    public function metTipoTransaccion($pk)
    {

        $secuencia = $this->_db->query("
            SELECT
              a006_miscelaneo_detalle.cod_detalle
            FROM
              cp_b006_banco_tipo_transaccion
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
            WHERE
              pk_num_banco_tipo_transaccion = '$pk'
             
             ");
        $secuencia->setFetchMode(PDO::FETCH_ASSOC);
        return $secuencia->fetch();
    }

    public function metBuscarTipoTransaccion($cod=false, $pk=false)
    {
        if($cod!=''){
            $where="WHERE cod_tipo_transaccion='$cod'";
        }else{
            $where="WHERE pk_num_banco_tipo_transaccion='$pk'";
        }
        $listar = $this->_db->query(
            "SELECT
                pk_num_banco_tipo_transaccion,
                a006_miscelaneo_detalle.cod_detalle
              FROM
                cp_b006_banco_tipo_transaccion
                INNER JOIN a006_miscelaneo_detalle ON cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
              $where
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metBuscarRetencionesReporte($idPago=false,$tipo=false)
    {
        $listar = $this->_db->query(
            "SELECT
                cp_e001_retenciones.fec_comprobante,
                cp_e001_retenciones.ind_num_comprobante,
                cp_e001_retenciones.ind_periodo_fiscal,
                cp_e001_retenciones.num_monto_retenido,
                cp_e001_retenciones.num_monto_impuesto AS montoRetenido,
                cp_e001_retenciones.ind_tipo_comprobante,
                cp_d009_orden_pago.ind_num_orden,
                cp_d010_pago.fec_pago,
                cp_d001_obligacion.ind_nro_factura,
                cp_d001_obligacion.ind_nro_control,
                cp_d001_obligacion.num_monto_afecto,
                cp_d001_obligacion.num_monto_no_afecto,
                cp_d001_obligacion.num_monto_impuesto,
                cp_d001_obligacion.fec_factura,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                a003_persona.ind_documento_fiscal,
                cp_b015_impuesto.num_factor_porcentaje,
                cp_d001_obligacion.ind_comentarios
              FROM
                cp_e001_retenciones
                INNER JOIN cp_d009_orden_pago ON cp_e001_retenciones.fk_cpd010_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
                INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                INNER JOIN cp_d010_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
                INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar = a003_persona.pk_num_persona
                INNER JOIN cp_b015_impuesto ON cp_e001_retenciones.fk_b015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
              WHERE
                pk_num_pago='$idPago' AND
                cp_d010_pago.ind_estado != 'AND'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    #vouchers ONCO

    #voucherProvision
    public function metArmarOrdenPagoContabilidadFinanciera($idObligacion, $tipoProceso = false, $tipoNomina = false)
    {
        if($tipoProceso == NULL ){
            $tipoProceso = '';
        }
        if($tipoNomina == NULL ){
            $tipoNomina = '';
        }
        $codImpuestoIva=Session::metObtener('IVGCODIGO');
        $listar = $this->_db->query(
            "
                (SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    (cp_d001_obligacion.num_monto_obligacion) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '01' AS orden,
                    'Haber' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta AS CUENTA20 ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
            )UNION(
                SELECT
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta20,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pk_num_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pk_num_cuenta20,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_descripcion
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_descripcion,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_descripcion
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_descripcion20,
                    (cp_d001_obligacion.num_monto_impuesto) AS MontoVoucher,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_tipo_saldo
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_tipo_saldo,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_tipo_saldo
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_tipo_saldo20,
                    '02' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                WHERE
                    pk_num_obligacion='$idObligacion' and cp_d001_obligacion.num_monto_impuesto > 0
                GROUP BY
                    cod_cuenta
            )UNION(
                SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    ABS(
                      SUM(cp_d012_obligacion_impuesto.num_monto_impuesto)
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '03' AS orden,
                    'Haber' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d012_obligacion_impuesto  ON cp_d001_obligacion.pk_num_obligacion=cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                    INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
                    INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                    INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_provision
                WHERE
                    pk_num_obligacion='$idObligacion' and a006_miscelaneo_detalle.cod_detalle = 'N'
                GROUP BY
                    cod_cuenta
            )
            UNION(
                SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto)
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '04' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
            )
            UNION(
                SELECT
              CUENTA.cod_cuenta AS cod_cuenta,
              CUENTA20.cod_cuenta AS cod_cuenta20,
              CUENTA.pk_num_cuenta AS pk_num_cuenta,
              CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
              CUENTA.ind_descripcion AS ind_descripcion,
              CUENTA20.ind_descripcion AS ind_descripcion20,
              (SELECT
                SUM(i.num_monto_afecto) 
            FROM
              cp_d012_obligacion_impuesto AS i
            WHERE
              i.fk_cpd001_num_obligacion = '$idObligacion' AND i.fk_cbb004_num_cuenta = CUENTA.pk_num_cuenta
              GROUP BY i.fk_cbb004_num_cuenta
              ) AS MontoVoucher,
              CUENTA.ind_tipo_saldo,
              CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
              '05' AS orden,
              'Haber' AS columna
            FROM
              cp_d001_obligacion
            INNER JOIN
            cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
            INNER JOIN
            nm_b002_concepto ON cp_d012_obligacion_impuesto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
            INNER JOIN
            nm_d001_concepto_perfil_detalle ON nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
            INNER JOIN
            cb_b004_plan_cuenta CUENTA ON cp_d012_obligacion_impuesto.fk_cbb004_num_cuenta = CUENTA.pk_num_cuenta
            LEFT JOIN
            cb_b004_plan_cuenta CUENTA20 ON cp_d012_obligacion_impuesto.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
            WHERE
              pk_num_obligacion = '$idObligacion' AND (nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso = '29' or nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso = '$tipoProceso' )
            GROUP BY
              cod_cuenta
            )
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    #voucherPago
    public function metArmarPagoContabilidad($idObligacion, $flagProvision=false,$impuesto = false)
    {
        if($impuesto == NULL ){
            $impuesto = 0;
        }

        if($flagProvision == '1'){
            $condicion = "UNION
                (SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    (
                        cp_d001_obligacion.num_monto_obligacion + ($impuesto)
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '03' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta AS CUENTA20 ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion = '$idObligacion'
                GROUP BY
                    cod_cuenta
                )";
        }else{
            $condicion = "UNION
                (SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    (
                        cp_d001_obligacion.num_monto_obligacion
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '03' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta AS CUENTA20 ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion = '$idObligacion'
                GROUP BY
                    cod_cuenta
                )
                UNION(
                SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto)
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '04' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
            )";
        }
        $listar = $this->_db->query(
            "
                (
                    SELECT
                        CUENTA.cod_cuenta AS cod_cuenta,
                        CUENTA20.cod_cuenta AS cod_cuenta20,
                        CUENTA.pk_num_cuenta AS pk_num_cuenta,
                        CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                        CUENTA.ind_descripcion AS ind_descripcion,
                        CUENTA20.ind_descripcion AS ind_descripcion20,
                        ABS(
                            SUM(
                                cp_d001_obligacion.num_monto_obligacion
                                )
                        ) AS MontoVoucher,
                        CUENTA.ind_tipo_saldo,
                        CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                        '01' AS orden,
                        'Haber' AS columna
                    FROM
                        cp_d001_obligacion
                        INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA ON CUENTA.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
                    LEFT JOIN cb_b004_plan_cuenta AS CUENTA20 ON CUENTA.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta_pub20
                    WHERE
                        pk_num_obligacion = '$idObligacion'
                    )
                UNION
                (
                    SELECT
                        CUENTA.cod_cuenta AS cod_cuenta,
                        CUENTA20.cod_cuenta AS cod_cuenta20,
                        CUENTA.pk_num_cuenta AS pk_num_cuenta,
                        CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                        CUENTA.ind_descripcion AS ind_descripcion,
                        CUENTA20.ind_descripcion AS ind_descripcion20,
                        ABS(
                            SUM(
                                cp_d012_obligacion_impuesto.num_monto_impuesto
                            )
                        ) AS MontoVoucher,
                        CUENTA.ind_tipo_saldo,
                        CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                        '02' AS orden,
                        'Haber' AS columna
                    FROM
                        cp_d001_obligacion
                        INNER JOIN cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                        INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
                        INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                        LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                        INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_provision
                    WHERE
                        pk_num_obligacion = '$idObligacion' and a006_miscelaneo_detalle.cod_detalle = 'P'
                    GROUP BY
                        cod_cuenta
                )
                $condicion
                ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    //pago parcial
    public function metArmarPagoParcial($nroPagoParcial, $idPago, $idObligacion, $flagProvision=false,$impuesto = false)
    {
        if($impuesto == NULL ){
            $impuesto = 0;
        }

        if($flagProvision == '1'){
            $condicion = "UNION
                (SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    (
                        cp_d010_pago.num_monto_pago + ($impuesto)
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '03' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                    INNER JOIN cp_d010_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
                    INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta AS CUENTA20 ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion = '$idObligacion'
                    AND cp_d010_pago.pk_num_pago = '$idPago' 
                GROUP BY
                    cod_cuenta
                )";
        }else{
            $condicion = "UNION
                (SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    (
                        cp_d010_pago.num_monto_pago
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '03' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                    INNER JOIN cp_d010_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
                    INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta AS CUENTA20 ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion = '$idObligacion'
                    AND cp_d010_pago.pk_num_pago = '$idPago' 
                GROUP BY
                    cod_cuenta
                )
                UNION(
                SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto)
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '04' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                    AND cp_d010_pago.pk_num_pago = '$idPago' 
                GROUP BY
                    cod_cuenta
            ORDER BY columna, CodCuenta DESC
            )";
        }
        $listar = $this->_db->query(
            "
                (
                    SELECT
                      CUENTA.cod_cuenta AS cod_cuenta,
                      CUENTA20.cod_cuenta AS cod_cuenta20,
                      CUENTA.pk_num_cuenta AS pk_num_cuenta,
                      CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                      CUENTA.ind_descripcion AS ind_descripcion,
                      CUENTA20.ind_descripcion AS ind_descripcion20,
                      ABS(
                        SUM(
                          cp_d010_pago.num_monto_pago
                        )
                      ) AS MontoVoucher,
                      CUENTA.ind_tipo_saldo,
                      CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                      '01' AS orden,
                      'Haber' AS columna
                    FROM
                      cp_d001_obligacion
                    INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                    INNER JOIN cp_d010_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
                    INNER JOIN
                      cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
                    INNER JOIN
                      cb_b004_plan_cuenta AS CUENTA ON CUENTA.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
                    LEFT JOIN
                      cb_b004_plan_cuenta AS CUENTA20 ON CUENTA.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta_pub20
                    WHERE
                      pk_num_obligacion = '$idObligacion'
                      AND cp_d010_pago.pk_num_pago = '$idPago' 
                )
                UNION
                (
                    SELECT
                        CUENTA.cod_cuenta AS cod_cuenta,
                        CUENTA20.cod_cuenta AS cod_cuenta20,
                        CUENTA.pk_num_cuenta AS pk_num_cuenta,
                        CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                        CUENTA.ind_descripcion AS ind_descripcion,
                        CUENTA20.ind_descripcion AS ind_descripcion20,
                        ABS(
                            SUM(
                                cp_d012_obligacion_impuesto.num_monto_impuesto
                            )
                        ) AS MontoVoucher,
                        CUENTA.ind_tipo_saldo,
                        CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                        '02' AS orden,
                        'Haber' AS columna
                    FROM
                        cp_d001_obligacion
                        INNER JOIN cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                        INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
                        INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                        LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                        INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_provision
                    WHERE
                        pk_num_obligacion = '$idObligacion' and a006_miscelaneo_detalle.cod_detalle = 'P'
                    GROUP BY
                        cod_cuenta
                )
                $condicion
                ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    #### vouchers PUB20 ###

    #voucherProvision
    public function metArmarOrdenPagoPub20($idObligacion)
    {
        $codImpuestoIva=Session::metObtener('IVGCODIGO');
        $listar = $this->_db->query(
            "
                (SELECT
                    CUENTA20.cod_cuenta AS cod_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.ind_descripcion AS ind_descripcion,
                    (cp_d001_obligacion.num_monto_obligacion + cp_d001_obligacion.num_monto_impuesto_otros) AS MontoVoucher,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                    '01' AS orden,
                    'Haber' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA20 ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
            )UNION(
                SELECT
                    CUENTA20.cod_cuenta AS cod_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.ind_descripcion AS ind_descripcion,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto)
                    ) AS MontoVoucher,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                    '02' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN cb_b004_plan_cuenta CUENTA20 ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
                ORDER BY columna, cod_cuenta DESC
            )UNION(
                SELECT
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pk_num_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_descripcion
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_descripcion,
                    (cp_d001_obligacion.num_monto_impuesto) AS MontoVoucher,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_tipo_saldo
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_tipo_saldo,
                    '03' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                WHERE
                    pk_num_obligacion='$idObligacion' and cp_d001_obligacion.num_monto_impuesto > 0
                GROUP BY
                    cod_cuenta
            )
            ");
        // var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    #voucherOrdenacioPago
    public function metOrdenacionPago($idObligacion, $impuesto1, $impuesto2,$tipoProceso = false, $tipoNomina = false)
    {
        $codImpuestoIva=Session::metObtener('IVGCODIGO');
        if($impuesto1 == NULL ){
            $impuesto1 = 0;
        }
        if($impuesto2 == NULL ){
            $impuesto2 = 0;
        }
        if($tipoProceso == NULL ){
            $tipoProceso = '';
        }
        if($tipoNomina == NULL ){
            $tipoNomina = '';
        }
        $listar = $this->_db->query(
            "
                (SELECT
                    CUENTA20.cod_cuenta AS cod_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.ind_descripcion AS ind_descripcion,
                    (cp_d001_obligacion.num_monto_obligacion + $impuesto1 + $impuesto2) AS MontoVoucher,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                    '01' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA20 ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
            )UNION(
                SELECT
                    CUENTA20.cod_cuenta AS cod_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.ind_descripcion AS ind_descripcion,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto) - $impuesto1
                    ) AS MontoVoucher,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                    '02' AS orden,
                    'Haber' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_d013_obligacion_cuentas.fk_prb002_num_partida_presupuestaria
                    INNER JOIN cb_b004_plan_cuenta CUENTA20 ON CUENTA20.pk_num_cuenta = pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_gastopub20
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
                ORDER BY columna, cod_cuenta DESC
            )UNION(
                SELECT
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto

                      INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_b015_impuesto.fk_prb002_num_partida_presupuestaria
                      INNER JOIN cb_b004_plan_cuenta ON pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_gastopub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                         INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_b015_impuesto.fk_prb002_num_partida_presupuestaria
                         INNER JOIN cb_b004_plan_cuenta ON pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_gastopub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pk_num_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_descripcion
                      FROM
                        cp_b015_impuesto
                      INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_b015_impuesto.fk_prb002_num_partida_presupuestaria
                      INNER JOIN cb_b004_plan_cuenta ON pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_gastopub20 = cb_b004_plan_cuenta.pk_num_cuenta

                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_descripcion,
                    (cp_d001_obligacion.num_monto_impuesto) AS MontoVoucher,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_tipo_saldo
                      FROM
                        cp_b015_impuesto
                        INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_b015_impuesto.fk_prb002_num_partida_presupuestaria
                        INNER JOIN cb_b004_plan_cuenta ON pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_gastopub20 = cb_b004_plan_cuenta.pk_num_cuenta

                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_tipo_saldo,
                    '03' AS orden,
                    'Haber' AS columna
                FROM
                    cp_d001_obligacion
                WHERE
                    pk_num_obligacion='$idObligacion' and cp_d001_obligacion.num_monto_impuesto > 0
                GROUP BY
                    cod_cuenta
            )UNION(
                SELECT
                        CUENTA20.cod_cuenta AS cod_cuenta,
                        CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                        CUENTA20.ind_descripcion AS ind_descripcion,
                        ABS(
                          SUM(cp_d012_obligacion_impuesto.num_monto_impuesto)
                        ) AS MontoVoucher,
                        CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                        '04' AS orden,
                        'Haber' AS columna
                    FROM
                        cp_d001_obligacion
                        INNER JOIN cp_d012_obligacion_impuesto  ON cp_d001_obligacion.pk_num_obligacion=cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                        INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
                        INNER JOIN cb_b004_plan_cuenta CUENTA20 ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                        INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_provision
                    WHERE
                        pk_num_obligacion='$idObligacion' and a006_miscelaneo_detalle.cod_detalle = 'N'
                    GROUP BY
                        cod_cuenta
                    )UNION(
                        SELECT
                            CUENTA.cod_cuenta AS cod_cuenta,
                            CUENTA.pk_num_cuenta AS pk_num_cuenta,
                            CUENTA.ind_descripcion AS ind_descripcion,
                            ABS(
                              SUM(cp_d012_obligacion_impuesto.num_monto_afecto)
                            ) AS MontoVoucher,
                            CUENTA.ind_tipo_saldo,
                            '05' AS orden,
                            'Haber' AS columna
                        FROM
                            cp_d001_obligacion
                            INNER JOIN cp_d012_obligacion_impuesto  ON cp_d001_obligacion.pk_num_obligacion=cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                            INNER JOIN nm_b002_concepto ON cp_d012_obligacion_impuesto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
                            INNER JOIN nm_d001_concepto_perfil_detalle ON nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
                            INNER JOIN cb_b004_plan_cuenta CUENTA ON nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber = CUENTA.cod_cuenta

                        WHERE
                            pk_num_obligacion='$idObligacion' and nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso = '$tipoProceso'
                            and nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina ='$tipoNomina'
                        GROUP BY
                            cod_cuenta
                    )
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    #voucherPago
    //pago directo
    public function metPago($idObligacion, $flagProvision=false,$impuesto1 = false, $impuesto2 = false)
    {
        $codImpuestoIva=Session::metObtener('IVGCODIGO');
        if($impuesto1 == NULL ){
            $impuesto1 = 0;
        }
        if($flagProvision == '1'){
            $condicion = "UNION
                (SELECT
                    CUENTA20.cod_cuenta AS cod_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.ind_descripcion AS ind_descripcion,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto) - $impuesto1
                    ) AS MontoVoucher,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                    '03' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_d013_obligacion_cuentas.fk_prb002_num_partida_presupuestaria
                    INNER JOIN cb_b004_plan_cuenta CUENTA20 ON CUENTA20.pk_num_cuenta = pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_gastopub20
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
                ORDER BY columna, cod_cuenta DESC
                )";
        }else{
            $condicion = "UNION
                (SELECT
                    CUENTA20.cod_cuenta AS cod_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.ind_descripcion AS ind_descripcion,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto - ($impuesto1 + $impuesto2))
                    ) AS MontoVoucher,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                    '05' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN cb_b004_plan_cuenta CUENTA20 ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
                    )";
        }
        $listar = $this->_db->query(
            "
                (
                    SELECT
                        CUENTA20.cod_cuenta AS cod_cuenta,
                        CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                        CUENTA20.ind_descripcion AS ind_descripcion,
                        ABS(
                            SUM(
                                cp_d001_obligacion.num_monto_obligacion
                                )
                        ) AS MontoVoucher,
                        CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                        '01' AS orden,
                        'Haber' AS columna
                    FROM
                        cp_d001_obligacion
                        INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
                        INNER JOIN cb_b004_plan_cuenta AS CUENTA20 ON CUENTA20.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta_pub20
                    WHERE
                        pk_num_obligacion = '$idObligacion'
                    )
                UNION
                (
                    SELECT
                        CUENTA20.cod_cuenta AS cod_cuenta,
                        CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                        CUENTA20.ind_descripcion AS ind_descripcion,
                        ABS(
                            SUM(
                                cp_d012_obligacion_impuesto.num_monto_impuesto
                            )
                        ) AS MontoVoucher,
                        CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                        '02' AS orden,
                        'Haber' AS columna
                    FROM
                        cp_d001_obligacion
                        INNER JOIN cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                        INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
                        INNER JOIN cb_b004_plan_cuenta CUENTA20 ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                        INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_provision
                    WHERE
                        pk_num_obligacion = '$idObligacion' and a006_miscelaneo_detalle.cod_detalle = 'P'
                    GROUP BY
                        cod_cuenta
                )UNION(
                    SELECT
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pk_num_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_descripcion
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_descripcion,
                    (cp_d001_obligacion.num_monto_impuesto) AS MontoVoucher,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_tipo_saldo
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_tipo_saldo,
                    '04' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                WHERE
                    pk_num_obligacion='$idObligacion' and cp_d001_obligacion.num_monto_impuesto > 0
                GROUP BY
                    cod_cuenta
                )
                $condicion
                ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }
    //pago parcial
    public function metPagoParcial($nroPago,$idPago, $idObligacion, $flagProvision=false, $impuesto1 = false, $impuesto2 = false)
    {
        $codImpuestoIva = Session::metObtener('IVGCODIGO');
        if($impuesto1 == NULL ){
            $impuesto1 = 0;
        }
        $condicion = '';
        //var_dump($nroPago);
        if($nroPago == 1){
            $condicion .= "UNION
                (
                    SELECT
                        CUENTA20.cod_cuenta AS cod_cuenta,
                        CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                        CUENTA20.ind_descripcion AS ind_descripcion,
                        ABS(
                            SUM(
                                cp_d012_obligacion_impuesto.num_monto_impuesto
                            )
                        ) AS MontoVoucher,
                        CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                        '02' AS orden,
                        'Haber' AS columna
                    FROM
                        cp_d001_obligacion
                        INNER JOIN cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                        INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
                        INNER JOIN cb_b004_plan_cuenta CUENTA20 ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                        INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_provision
                    WHERE
                        pk_num_obligacion = '$idObligacion' and a006_miscelaneo_detalle.cod_detalle = 'P'
                    GROUP BY
                        cod_cuenta
                )UNION(
                    SELECT
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pk_num_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_descripcion
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_descripcion,
                    (cp_d001_obligacion.num_monto_impuesto) AS MontoVoucher,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_tipo_saldo
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_tipo_saldo,
                    '04' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                WHERE
                    pk_num_obligacion='$idObligacion' and cp_d001_obligacion.num_monto_impuesto > 0
                GROUP BY
                    cod_cuenta
                )";
            if($flagProvision == '1'){
                //monto partidas
                $condicion .= "UNION
                (SELECT
                    CUENTA20.cod_cuenta AS cod_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.ind_descripcion AS ind_descripcion,
                    ABS(
                      SUM(cp_d015_orden_pago_parcial.num_monto) - $impuesto1
                    ) AS MontoVoucher,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                    '03' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                    INNER JOIN cp_d015_orden_pago_parcial ON cp_d001_obligacion.pk_num_obligacion = cp_d015_orden_pago_parcial.fk_cpd009_num_orden_pago
                    INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_d015_orden_pago_parcial.fk_prb002_num_partida_presupuestaria
                    INNER JOIN cb_b004_plan_cuenta CUENTA20 ON CUENTA20.pk_num_cuenta = pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_gastopub20
                WHERE
                    pk_num_obligacion = '$idObligacion'
                    AND cp_d015_orden_pago_parcial.fk_cpd010_num_pago = '$idPago' 
                    AND ind_descripcion != 'Impuestos al Valor Agregado'
                GROUP BY
                    cod_cuenta
                ORDER BY columna, cod_cuenta DESC
                )";
            }else{
                $condicion .= "UNION
                (SELECT
                    CUENTA20.cod_cuenta AS cod_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.ind_descripcion AS ind_descripcion,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto - ($impuesto1 + $impuesto2))
                    ) AS MontoVoucher,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                    '05' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN cb_b004_plan_cuenta CUENTA20 ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
                    INNER JOIN cb_b004_plan_cuenta CUENTA20 ON CUENTA20.pk_num_cuenta = pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_gastopub20
                WHERE
                    pk_num_obligacion='$idObligacion'
                    AND cp_d015_orden_pago_parcial.fk_cpd010_num_pago = '$idPago' 
                GROUP BY
                    cod_cuenta
                    )";
            }
        }

        $listar = $this->_db->query(
            "
                (
                    SELECT
                      CUENTA20.cod_cuenta AS cod_cuenta,
                      CUENTA20.pk_num_cuenta AS pk_num_cuenta,
                      CUENTA20.ind_descripcion AS ind_descripcion,
                      ABS(
                        SUM(
                          cp_d010_pago.num_monto_pago
                        )
                      ) AS MontoVoucher,
                      CUENTA20.ind_tipo_saldo AS ind_tipo_saldo,
                      '01' AS orden,
                      'Haber' AS columna
                    FROM
                      cp_d001_obligacion
                    INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                    INNER JOIN cp_d010_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
                    INNER JOIN
                      cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
                    INNER JOIN
                      cb_b004_plan_cuenta AS CUENTA20 ON CUENTA20.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta_pub20
                    WHERE
                      cp_d001_obligacion.pk_num_obligacion = '$idObligacion' 
                      AND cp_d010_pago.pk_num_pago = '$idPago' 
                )
                $condicion
                ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }







}
