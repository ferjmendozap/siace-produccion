<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once 'documentoModelo.php';
require_once 'obligacionModelo.php';
require_once 'cuentasModelo.php';
require_once 'trait'.DS.'consultasTrait.php';
require_once 'trait'.DS.'updateTrait.php';
require_once RUTA_MODELO . 'miscelaneoModelo.php';


class generacionVouchersModelo extends Modelo
{
    use consultasTrait;
    use updateTrait;
    private $atIdUsuario;
    private $atIdEmpleado;
    public $atDocumentosModelo;
    public $atMiscelaneoModelo;
    public $atVoucherModelo;
    public $atLibroContableModelo;
    public $atObligacionModelo;


    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atDocumentosModelo = new documentoModelo();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atVoucherModelo = new TipoVoucherModelo();
        $this->atObligacionModelo = new obligacionModelo();
        $this->atLibroContableModelo = new LibroContableModelo();
    }

    //voucher generados de desde generar voucher de obligaciones
    public function metListarObligaciones()
    {
        $listar = $this->_db->query(
            "SELECT
                      cp_d001_obligacion.*,
                      cp_b002_tipo_documento.cod_tipo_documento,
                      cp_d010_pago.pk_num_pago,
                      cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
                      CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS nombreProveedor
                    FROM
                      cp_d001_obligacion
                    INNER JOIN
                      a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar
                    INNER  JOIN  
                      rh_b001_empleado on rh_b001_empleado.pk_num_empleado =cp_d001_obligacion.fk_rhb001_num_empleado_ingresa
                    INNER JOIN
                      a003_persona AS a003_persona2 ON a003_persona2.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                    LEFT JOIN
                      cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
                    LEFT JOIN
                      cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                    LEFT JOIN
                      cp_d010_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
                    LEFT JOIN
                      cp_d011_banco_transaccion ON cp_d011_banco_transaccion.fk_cpd010_num_pago = cp_d010_pago.pk_num_pago
                    LEFT JOIN
                      cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
                    WHERE
                      (cp_d001_obligacion.ind_estado = 'AP' OR cp_d001_obligacion.ind_estado = 'PA') AND cp_d001_obligacion.num_contabilizacion_pendiente = 0
                      AND cp_b002_tipo_documento.num_flag_provision = 1
                    ORDER BY
                      cp_d001_obligacion.ind_nro_registro

            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }


    public function meConsultarDatosObligacion($idObligacion)
    {
        $listar = $this->_db->query(
            "SELECT
                cp_d001_obligacion.*,
                cp_b002_tipo_documento.cod_tipo_documento,
                a006_miscelaneo_detalle.cod_detalle,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreProveedor
            FROM
                cp_d001_obligacion
                INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
                INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                INNER JOIN cp_d010_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
                INNER JOIN cp_d011_banco_transaccion ON cp_d011_banco_transaccion.fk_cpd010_num_pago = cp_d010_pago.pk_num_pago
                INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
                INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion

                INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d011_banco_transaccion.fk_a003_num_persona_proveedor
            WHERE
                cp_d001_obligacion.pk_num_obligacion = '$idObligacion'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metVouchersOblig($idObligacion,$codVocuher)
    {
        $this->_db->beginTransaction();
        //	actualizo obligacion
        $actualizar=$this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
              num_contabilizacion_pendiente=:num_contabilizacion_pendiente,
              fk_cbb001_num_voucher_mast=:fk_cbb001_num_voucher_mast
            WHERE
              pk_num_obligacion = '$idObligacion'
        ");
        $actualizar->execute(array(
            ':num_contabilizacion_pendiente'=>'1',
            ':fk_cbb001_num_voucher_mast'=>$codVocuher
        ));
        //	actualizo orden de pago
        $actualizar=$this->_db->prepare("
            UPDATE
                cp_d009_orden_pago
            SET
              fk_cbb001_num_voucher_asiento=:fk_cbb001_num_voucher_asiento
            WHERE
              fk_cpd001_num_obligacion = '$idObligacion'
        ");
        $actualizar->execute(array(
            ':fk_cbb001_num_voucher_asiento'=>$codVocuher
        ));
        //var_dump($actualizar);
        $fallaTansaccion = $actualizar->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    //voucher generados de desde generar voucher de pagos
    public function metListarPagos()
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d010_pago.*,
              CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS nombreProveedor,
              a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle AS tipoPago,
              cp_b014_cuenta_bancaria.ind_num_cuenta
            FROM
              cp_d010_pago
            INNER JOIN cp_d009_orden_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
            INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar = a003_persona.pk_num_persona
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle
            INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
            WHERE
            cp_d010_pago.ind_estado = 'IM'
            and cp_d001_obligacion.num_contabilizacion_pendiente=0
            ORDER BY ind_num_cuenta, ind_num_pago
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    //voucher generados de desde generar voucher de transaciones
    public function metListarTransacciones()
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cb_c003_tipo_voucher.cod_voucher
             FROM
              cp_d011_banco_transaccion

             INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.	fk_cpb006_num_banco_tipo_transaccion
             LEFT JOIN cb_c003_tipo_voucher on cb_c003_tipo_voucher.pk_num_voucher = cp_b006_banco_tipo_transaccion.fk_cbc003_tipo_voucher
             WHERE
              cp_d011_banco_transaccion.ind_estado = 'AP'
              and cp_d011_banco_transaccion.num_flag_genera_voucher = 1

            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metBuscarTransacciones($id)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cb_c003_tipo_voucher.cod_voucher,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              a006_miscelaneo_detalle.cod_detalle,
              cp_b006_banco_tipo_transaccion.fk_cbc003_tipo_voucher,
              cp_b002_tipo_documento.cod_tipo_documento,
              CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS proveedor
             FROM
              cp_d011_banco_transaccion
             INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.	fk_cpb006_num_banco_tipo_transaccion
             LEFT JOIN cb_c003_tipo_voucher on cb_c003_tipo_voucher.pk_num_voucher = cp_b006_banco_tipo_transaccion.fk_cbc003_tipo_voucher
             INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
             INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d011_banco_transaccion.fk_cpb002_num_tipo_documento
             INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d011_banco_transaccion.fk_a003_num_persona_proveedor
             WHERE
              cp_d011_banco_transaccion.ind_estado = 'AP'
              and cp_d011_banco_transaccion.num_flag_genera_voucher = 1
              AND cp_d011_banco_transaccion.pk_num_banco_transaccion = '$id'

            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metObtenerPersona($idEmpleado)
    {
        $listar = $this->_db->query(
            "SELECT
              a003_persona.pk_num_persona
            FROM
              rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            WHERE
              rh_b001_empleado.pk_num_empleado = '$idEmpleado'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    //
    public function metBuscar($tabla,$consulta,$cod)
    {
        $pk = $this->_db->query("
            SELECT
                *
            FROM
                $tabla
            WHERE
                $consulta='$cod'
        ");
        $pk->setFetchMode(PDO::FETCH_ASSOC);
        return $pk->fetch();
    }

    public function metArmarVocher($idPago,$tipoTransaccion)
    {
        if($tipoTransaccion == 'I'){
            $sql = '(
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Debe" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.fk_cpd010_num_pago = "'.$idPago.'"
            )
            UNION
            (
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Haber" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.fk_cpd010_num_pago = "'.$idPago.'"
        )';
        }elseif($tipoTransaccion == 'E'){
            $sql = '(
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Haber" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.fk_cpd010_num_pago = "'.$idPago.'"
            )
            UNION
            (
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Debe" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.fk_cpd010_num_pago = "'.$idPago.'"
        )';
        }else{
            $sql = '(
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Haber" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.fk_cpd010_num_pago = "'.$idPago.'" and cp_d011_banco_transaccion.num_monto < 0
            )
            UNION
            (
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Debe" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.fk_cpd010_num_pago = "'.$idPago.'" and cp_d011_banco_transaccion.num_monto > 0
        )';
        }
        //var_dump($sql);
        $listar = $this->_db->query("
                $sql

            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metArmarVocherTransacciones($idTransaccion, $tipoTransaccion)
    {
        if($tipoTransaccion == 'I'){
            $sql = '(
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Debe" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.pk_num_banco_transaccion = "'.$idTransaccion.'"
            )
            UNION
            (
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Haber" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.pk_num_banco_transaccion = "'.$idTransaccion.'"
        )';
        }elseif($tipoTransaccion == 'E'){
            $sql = '(
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Haber" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.pk_num_banco_transaccion = "'.$idTransaccion.'"
            )
            UNION
            (
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Debe" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.pk_num_banco_transaccion = "'.$idTransaccion.'"
        )';
        }else{
            $sql = '(
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Haber" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.pk_num_banco_transaccion = "'.$idTransaccion.'" and cp_d011_banco_transaccion.num_monto < 0
            )
            UNION
            (
            SELECT
                cp_d011_banco_transaccion.*,
                cb_b004_plan_cuenta.pk_num_cuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cp_d011_banco_transaccion.num_monto as MontoVoucher,
                "Debe" AS columna
            FROM
                cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
            WHERE
                cp_d011_banco_transaccion.pk_num_banco_transaccion = "'.$idTransaccion.'" and cp_d011_banco_transaccion.num_monto > 0
        )';
        }
        //var_dump($sql);
        $listar = $this->_db->query("
                $sql

            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metVouchersTransaccion($idTransaccion,$codVocuher)
    {
        $this->_db->beginTransaction();
        //	actualizo banco transaccion
        $actualizar=$this->_db->prepare("
            UPDATE
                cp_d011_banco_transaccion
            SET
                fk_cbb001_num_voucher=:fk_cbb001_num_voucher,
                ind_estado=:ind_estado
            WHERE
              pk_num_banco_transaccion = '$idTransaccion'
        ");
        $actualizar->execute(array(
            ':fk_cbb001_num_voucher' => $codVocuher,
            ':ind_estado' => 'CO'
        ));
        $fallaTansaccion = $actualizar->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metArmarOrdenPagoContabilidadFinanciera($idObligacion, $tipoProceso = false, $tipoNomina = false)
    {
        if($tipoProceso == NULL ){
            $tipoProceso = '';
        }
        if($tipoNomina == NULL ){
            $tipoNomina = '';
        }
        $codImpuestoIva=Session::metObtener('IVGCODIGO');
        $listar = $this->_db->query(
            "
                (SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    (cp_d001_obligacion.num_monto_obligacion) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '01' AS orden,
                    'Haber' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
                    INNER JOIN cb_b004_plan_cuenta AS CUENTA ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta AS CUENTA20 ON cp_b002_tipo_documento.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
            )UNION(
                SELECT
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta20,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pk_num_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pk_num_cuenta20,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_descripcion
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_descripcion,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_descripcion
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_descripcion20,
                    (cp_d001_obligacion.num_monto_impuesto) AS MontoVoucher,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_tipo_saldo
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_tipo_saldo,
                    (
                      SELECT
                        cb_b004_plan_cuenta.ind_tipo_saldo
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS ind_tipo_saldo20,
                    '02' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                WHERE
                    pk_num_obligacion='$idObligacion' and cp_d001_obligacion.num_monto_impuesto > 0
                GROUP BY
                    cod_cuenta
            )UNION(
            
                SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    ABS(
                      SUM(cp_d012_obligacion_impuesto.num_monto_impuesto)
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '03' AS orden,
                    'Haber' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d012_obligacion_impuesto  ON cp_d001_obligacion.pk_num_obligacion=cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                    INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
                    INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                    INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_provision
                WHERE
                    pk_num_obligacion='$idObligacion' and a006_miscelaneo_detalle.cod_detalle = 'N'
                GROUP BY
                    cod_cuenta
            )
            UNION(
                SELECT
                    CUENTA.cod_cuenta AS cod_cuenta,
                    CUENTA20.cod_cuenta AS cod_cuenta20,
                    CUENTA.pk_num_cuenta AS pk_num_cuenta,
                    CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
                    CUENTA.ind_descripcion AS ind_descripcion,
                    CUENTA20.ind_descripcion AS ind_descripcion20,
                    ABS(
                      SUM(cp_d013_obligacion_cuentas.num_monto)
                    ) AS MontoVoucher,
                    CUENTA.ind_tipo_saldo,
                    CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
                    '04' AS orden,
                    'Debe' AS columna
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
                    INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
                WHERE
                    pk_num_obligacion='$idObligacion'
                GROUP BY
                    cod_cuenta
            )
            UNION(
              SELECT
              CUENTA.cod_cuenta AS cod_cuenta,
              CUENTA20.cod_cuenta AS cod_cuenta20,
              CUENTA.pk_num_cuenta AS pk_num_cuenta,
              CUENTA20.pk_num_cuenta AS pk_num_cuenta20,
              CUENTA.ind_descripcion AS ind_descripcion,
              CUENTA20.ind_descripcion AS ind_descripcion20,
              (SELECT
                SUM(i.num_monto_afecto) 
            FROM
              cp_d012_obligacion_impuesto AS i
            WHERE
              i.fk_cpd001_num_obligacion = '$idObligacion' AND i.fk_cbb004_num_cuenta = CUENTA.pk_num_cuenta
              GROUP BY i.fk_cbb004_num_cuenta
              ) AS MontoVoucher,
              CUENTA.ind_tipo_saldo,
              CUENTA20.ind_tipo_saldo AS ind_tipo_saldo20,
              '05' AS orden,
              'Haber' AS columna
            FROM
              cp_d001_obligacion
INNER JOIN
  cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
INNER JOIN
  nm_b002_concepto ON cp_d012_obligacion_impuesto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
 INNER JOIN
  nm_d001_concepto_perfil_detalle ON nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
INNER JOIN
  cb_b004_plan_cuenta CUENTA ON cp_d012_obligacion_impuesto.fk_cbb004_num_cuenta = CUENTA.pk_num_cuenta
LEFT JOIN
  cb_b004_plan_cuenta CUENTA20 ON cp_d012_obligacion_impuesto.fk_cbb004_num_cuenta_pub20 = CUENTA20.pk_num_cuenta
WHERE
              pk_num_obligacion = '$idObligacion' AND nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso = '$tipoProceso'
            GROUP BY
              cod_cuenta
            )
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

}