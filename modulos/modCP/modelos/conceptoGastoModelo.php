<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once 'clasificacionGastoModelo.php';
require_once 'impuestoModelo.php';
require_once RUTA_Modulo . 'modPR' . DS . 'modelos' . DS . 'maestros' . DS . 'partidaModelo.php';
class conceptoGastoModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atClasificacionGastoModelo;
    public $atPartidaModelo;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
        $this->atClasificacionGastoModelo=new clasificacionGastoModelo();
        $this->atImpuestoModelo=new impuestoModelo();
        $this->atPartidaModelo=new partidaModelo();
    }
    public function metConceptoGastoListar($idConceptoGasto = false)
    {
        if($idConceptoGasto){
            $where = "WHERE cp_b005_concepto_gasto.pk_num_concepto_gasto = '$idConceptoGasto'";
        } else {
            $where = "";
        }
        $impuestoListar =  $this->_db->query(
            "SELECT
              cp_b005_concepto_gasto.*,
              a018.ind_usuario,
              partida.cod_partida,
              partida.pk_num_partida_presupuestaria,
              partida.ind_denominacion,
              cuenta.cod_cuenta,
              cuenta20.cod_cuenta AS cod_cuenta20,
              cuenta.pk_num_cuenta,
              cuenta20.pk_num_cuenta AS pk_num_cuenta20,
              cuenta.ind_descripcion AS nombreCuenta,
              cuenta20.ind_descripcion AS nombreCuenta20,
              a006_miscelaneo_detalle.ind_nombre_detalle,
              a006_miscelaneo_detalle.cod_detalle
             FROM
              cp_b005_concepto_gasto
            INNER JOIN a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cp_b005_concepto_gasto.fk_a018_num_seguridad_usuario
            INNER JOIN pr_b002_partida_presupuestaria AS partida ON partida.pk_num_partida_presupuestaria = cp_b005_concepto_gasto.fk_prb002_num_partida_presupuestaria
            LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = cp_b005_concepto_gasto.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cuenta20 ON cuenta20.pk_num_cuenta = cp_b005_concepto_gasto.fk_cbb004_num_plan_cuenta_pub20
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b005_concepto_gasto.fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo

            ORDER BY fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo ASC

             " );
        $impuestoListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idConceptoGasto){
            return $impuestoListar->fetch();
        } else {
            return $impuestoListar->fetchAll();
        }

    }

    public function metConceptoGastoConsultaDetalle($idConceptoGasto)
    {
        $consultaClasifGasto = $this->_db->query(
            "SELECT
              cp_b005_concepto_gasto.*,
              a018.ind_usuario,
              partida.cod_partida,
              partida.pk_num_partida_presupuestaria,
              cuenta.cod_cuenta,
              cuenta.pk_num_cuenta,
              clasif.fk_cpb004_num_clasificacion_gastos,
              clasif.pk_num_concepto_clasificacion_gastos
             FROM
              cp_b005_concepto_gasto
            INNER JOIN a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cp_b005_concepto_gasto.fk_a018_num_seguridad_usuario
            INNER JOIN pr_b002_partida_presupuestaria AS partida ON partida.pk_num_partida_presupuestaria = cp_b005_concepto_gasto.fk_prb002_num_partida_presupuestaria
            INNER JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = cp_b005_concepto_gasto.fk_cbb004_num_plan_cuenta
            INNER JOIN cp_c001_concepto_clasificacion_gasto AS clasif ON clasif.fk_cpb005_num_concepto_gasto = cp_b005_concepto_gasto.pk_num_concepto_gasto
             WHERE
             cp_b005_concepto_gasto.pk_num_concepto_gasto = $idConceptoGasto"
        );
        $consultaClasifGasto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaClasifGasto->fetch();
    }

    public function metConceptoGastoConsulta($idConceptoGasto)
    {
        $consultaClasifGasto = $this->_db->query(
            "SELECT
              cp_b005_concepto_gasto.*,
              a018.ind_usuario,
              partida.cod_partida,
              partida.pk_num_partida_presupuestaria,
              cuenta.cod_cuenta,
              cuenta.pk_num_cuenta,
              cuentaPub20.cod_cuenta AS cod_cuentaPub20

             FROM
              cp_b005_concepto_gasto
            INNER JOIN a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cp_b005_concepto_gasto.fk_a018_num_seguridad_usuario
            INNER JOIN pr_b002_partida_presupuestaria AS partida ON partida.pk_num_partida_presupuestaria = cp_b005_concepto_gasto.fk_prb002_num_partida_presupuestaria
            INNER JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = cp_b005_concepto_gasto.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cuentaPub20 ON cuentaPub20.pk_num_cuenta = cp_b005_concepto_gasto.fk_cbb004_num_plan_cuenta_pub20
            WHERE
             cp_b005_concepto_gasto.pk_num_concepto_gasto = $idConceptoGasto"
        );
        $consultaClasifGasto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaClasifGasto->fetch();
    }

    public function metMostrarClasif($idConceptoGasto)
    {
        $consultaClasifGasto = $this->_db->query(
            "SELECT
              *
             FROM
              cp_c001_concepto_clasificacion_gasto
            INNER JOIN
            cp_b005_concepto_gasto ON cp_b005_concepto_gasto.pk_num_concepto_gasto = cp_c001_concepto_clasificacion_gasto.fk_cpb005_num_concepto_gasto
          WHERE
             cp_b005_concepto_gasto.pk_num_concepto_gasto = $idConceptoGasto"
        );
        $consultaClasifGasto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaClasifGasto->fetchAll();
    }

    public function metNuevoConceptoGasto($form)
    {
        $this->_db->beginTransaction();
        $nuevoConceptoGasto = $this->_db->prepare(
            "INSERT INTO
             cp_b005_concepto_gasto
              SET
              cod_concepto_gasto=:cod_concepto_gasto,
              ind_descripcion=:ind_descripcion,
              fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo=:fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo,
              fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
              fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
              fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");
        $nuevoConceptoGasto->execute(array(
            ':cod_concepto_gasto' => $form['cod_concepto_gasto'],
            ':ind_descripcion' => $form['ind_descripcion'],
            ':fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo' => $form['fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo'],
            ':fk_prb002_num_partida_presupuestaria' => $form['fk_prb002_num_partida_presupuestaria'],
            ':fk_cbb004_num_plan_cuenta' => $form['fk_cbb004_num_plan_cuenta'],
            ':fk_cbb004_num_plan_cuenta_pub20' => $form['fk_cbb004_num_plan_cuenta_pub20'],
            ':num_estatus' => $form['num_estatus']
        ));
        $idConcepto = $this->_db->lastInsertId();
        ## Clasificacion de Gasto
        $nuevaClasif = $this->_db->prepare("
          INSERT INTO
          cp_c001_concepto_clasificacion_gasto
          SET
          fk_cpb004_num_clasificacion_gastos=:fk_cpb004_num_clasificacion_gastos,
          fk_cpb005_num_concepto_gasto=:fk_cpb005_num_concepto_gasto,
          fec_ultima_modificacion=NOW(),
          fk_a018_num_seguridad_usuario='$this->atIdUsuario'
          ");
        if(isset($form['nuevo'])) {
            $detalle = $form['nuevo'];
            $secuencia = 1;
            foreach ($detalle['secuencia'] AS $i) {
                $ids = $detalle[$i];
                $nuevaClasif ->execute(array(
                    ':fk_cpb004_num_clasificacion_gastos' => $ids['pk_num_clasificacion_gastos'],
                    ':fk_cpb005_num_concepto_gasto' => $idConcepto
                ));
                $secuencia++;
            }
        }

        $fallaTansaccion = $nuevoConceptoGasto->errorInfo();
        $fallaTansaccion2 = $nuevaClasif->errorInfo();
        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])) {
        $this->_db->rollBack();
        return $fallaTansaccion2;
        } else {
                $idRegistro = $this->_db->lastInsertId();
                $this->_db->commit();
                return $idRegistro;
        }
    }

    public function metModificarConceptoGasto($idConceptoGasto, $form)
    {
        $this->_db->beginTransaction();
        $nuevoConceptoGasto = $this->_db->prepare(
            "UPDATE
             cp_b005_concepto_gasto
              SET
              ind_descripcion=:ind_descripcion,
              fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo=:fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo,
              fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
              fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
              fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE
              pk_num_concepto_gasto='$idConceptoGasto'
               ");
        $nuevoConceptoGasto->execute(array(
            ':ind_descripcion' => $form['ind_descripcion'],
            ':fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo' => $form['fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo'],
            ':fk_prb002_num_partida_presupuestaria' => $form['fk_prb002_num_partida_presupuestaria'],
            ':fk_cbb004_num_plan_cuenta' => $form['fk_cbb004_num_plan_cuenta'],
            ':fk_cbb004_num_plan_cuenta_pub20' => $form['fk_cbb004_num_plan_cuenta_pub20'],
            ':num_estatus' => $form['num_estatus']
        ));
        ## Clasificacion de Gasto
        $this->_db->query("DELETE FROM cp_c001_concepto_clasificacion_gasto WHERE fk_cpb005_num_concepto_gasto='$idConceptoGasto'");
        if(isset($form['nuevo'])) {
            $detalle = $form['nuevo'];
            $secuencia = 1;
            foreach ($detalle['secuencia'] AS $i) {
                $ids = $detalle[$i];
                $nuevaClasif = $this->_db->prepare("
                  INSERT INTO
                  cp_c001_concepto_clasificacion_gasto
                  SET
                  fk_cpb004_num_clasificacion_gastos=:fk_cpb004_num_clasificacion_gastos,
                  fk_cpb005_num_concepto_gasto=:fk_cpb005_num_concepto_gasto,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");
                $nuevaClasif ->execute(array(
                    ':fk_cpb004_num_clasificacion_gastos' => $ids['pk_num_clasificacion_gastos'],
                    ':fk_cpb005_num_concepto_gasto' => $idConceptoGasto
                ));
                $secuencia++;
            }
        }
        $fallaTansaccion = $nuevoConceptoGasto->errorInfo();
        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idConceptoGasto;
        }
    }

    public function metEliminarConceptoGasto($idConceptoGasto){
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
           DELETE FROM cp_b005_concepto_gasto
           WHERE
           pk_num_concepto_gasto=:pk_num_concepto_gasto
            ");
        $elimar->execute(array(
            'pk_num_concepto_gasto'=>$idConceptoGasto
        ));
        $error=$elimar->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idConceptoGasto;
        }
    }



}
