<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{if $estado =='PR'}APROBAR {elseif $estado=='AP'}GENERAR OBLIGACION DE {else}LISTADO DE {/if}CAJA CHICA
                                         </h2>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#Caja</th>
                            <th>Descripcion</th>
                            <th>Monto Total</th>
                            <th>Fecha Preparacion</th>
                            <th>Fecha Aprobacion</th>
                            <th>Estado</th>
                            <th>Dependencia</th>
                            <th>C.Costo</th>
                            <th>Obligacion</th>
                            <th>Beneficiario</th>
                            <td>Acción</td>
                        </tr>
                        </thead>
                        <tbody style="overflow: auto;">
                        {foreach item=cajaChica from=$cajaBD}
                                <tr id="idCajaChica{$cajaChica.pk_num_caja_chica}">
                                    <td>{$cajaChica.ind_num_caja_chica}</td>
                                    <td>{$cajaChica.ind_descripcion}</td>
                                    <td>{$cajaChica.num_monto_total}</td>
                                    <td>{$cajaChica.fec_preparacion}</td>
                                    <td>{$cajaChica.fec_aprobacion}</td>
                                    <td>{$cajaChica.ind_estado}</td>
                                    <td>{$cajaChica.fk_a004_num_dependencia}</td>
                                    <td>{$cajaChica.fk_a023_num_centro_costo}</td>
                                    <td>{$cajaChica.ind_nro_factura}</td>
                                    <td>{$cajaChica.fk_rhb001_num_empleado_beneficiario}</td>

                                    <td>
                                        {if $estado == 'PR'}
                                            {if in_array('CP-01-06-01-01-02',$_Parametros.perfil)}
                                            <button class="accion cajaChica btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" id="PR" ver="1" idCajaChica="{$cajaChica.pk_num_caja_chica}" title="APROBAR"
                                                    descipcion="El Usuario ha Aprobado una Caja Chica" titulo="<i class='icm icm-rating3'></i> Aprobar Caja Chica">
                                                <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                            </button>
                                            {/if}
                                        {/if}
                                        {if $cajaChica.ind_estado == 'PREPARADO' AND $estado == ''}
                                            {if in_array('CP-01-06-01-01-01-M',$_Parametros.perfil)}
                                                <button class="modificar cajaChica btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" id="" idCajaChica="{$cajaChica.pk_num_caja_chica}" title="EDITAR"
                                                        descipcion="El Usuario ha Modificado una Caja Chica" titulo="<i class='fa fa-edit'></i> Modificar Caja Chica">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {/if}
                                        {if $estado == 'AP'}
                                            <button class="accion btn ink-reaction btn-raised btn-xs btn-primary-light" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" id="GE" ver="1" idCajaChica="{$cajaChica.pk_num_caja_chica}" title="Generar Obligación"
                                                    descipcion="El Usuario ha GENERADO una Obligación" titulo="<i class='icm icm-spinner12'></i> Generar Obligación">
                                                <i class="icm icm-spinner12" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        {if $cajaChica.ind_estado != 'ANULADO' and $cajaChica.ind_estado != 'GENERADO'}
                                            <button class="accion btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" id="AN" ver="1" idCajaChica="{$cajaChica.pk_num_caja_chica}" title="Anular"
                                                    descipcion="El Usuario ha ANULADO una Caja Chica" titulo="<i class='fa fa-edit'></i> Anular Caja Chica">
                                                <i class="icm icm-blocked" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        {if in_array('CP-01-06-01-01-02-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idCajaChica="{$cajaChica.pk_num_caja_chica}" title="VER"
                                                    descipcion="El Usuario esta viendo una Caja Chica" titulo="<i class='icm icm-calculate2'></i> Ver Caja Chica">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        <button class="imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-success" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idCajaChica="{$cajaChica.pk_num_caja_chica}" title="IMPRIMIR"
                                                descipcion="El Usuario esta Imprimiendo una Caja Chica" titulo="<i class='md md-print'></i> Imprimir Caja Chica">
                                            <i class="md md-print" style="color: #ffffff;"></i>
                                        </button>

                                    </td>
                                </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="11">
                                {if $estado !='PR' and $estado !='AP' }
                                {if in_array('CP-01-06-01-01-03-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado un post de Caja Chica"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="Registrar Nueva Caja Chica" id="nuevo" data-keyboard="false"
                                        data-backdrop="static">Nueva Caja Chica&nbsp;&nbsp;<i class="md md-create"></i>
                                </button>
                                {/if}
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var url='{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/nuevaCajaChicaMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idCajaChica: $(this).attr('idCajaChica'), estado: $(this).attr('id') },function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.accion', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idCajaChica: $(this).attr('idCajaChica'), estado: $(this).attr('id'), ver:$(this).attr('ver') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{ idCajaChica: $(this).attr('idCajaChica') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.imprimir', function () {
            var idCajaChica = $(this).attr('idCajaChica');
            var url='{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/imprimirMET/?idCajaChica='+idCajaChica;
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{  },function(dato){

                $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="540px"></iframe>');

            });
        });
    });
</script>