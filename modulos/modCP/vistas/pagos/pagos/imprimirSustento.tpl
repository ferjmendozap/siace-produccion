<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card tabs-left style-default-light">
                <ul class="card-head nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#sustento" class="pestaña" id="imprimirSustentoPagoMET"><i class="icm icm-cog3"></i> SUSTENTO</a></li>
                        <li><a href="#cheque" class="pestaña" id="imprimirChequePagoMET"><i class="icm icm-cog3"></i> CHEQUE</a></li>
                        <li><a href="#retenciones" class="pestaña" id="imprimirRetencionesPagoMET"><i class="icm icm-cog3"></i> RETENCIONES</a></li>
                </ul>
                <div class="card-body tab-content style-default-bright">
                    <div class="tab-pane active" id="sustento">
                        <div id="iframe_imprimirSustentoPagoMET"></div>
                    </div>
                    <div class="tab-pane" id="cheque">
                        <div id="iframe_imprimirChequePagoMET"></div>
                    </div>
                    <div class="tab-pane" id="retenciones">
                        <div id="iframe_imprimirRetencionesPagoMET"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#modalAncho').css("width", "80%");
        var url='{$_Parametros.url}modCP/pagos/pagosCONTROL/';
        var idPago = '{$idPago}';
        $('#iframe_imprimirSustentoPagoMET').html('<iframe frameborder="0" src="'+url+'imprimirSustentoPagoMET/'+idPago+'" width="100%" height="540px"></iframe>');
        $('.pestaña').click(function () {
            var met = $(this).attr('id');
            var urls=url+met+'/'+idPago;
            $('#iframe_'+met).html('<iframe frameborder="0" src="'+urls+'" width="100%" height="540px"></iframe>');

        });
    });
</script>