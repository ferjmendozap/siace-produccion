<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">LIBRO DE CHEQUES </h2>
                    </div>

                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="{$_Parametros.url}modCP/pagos/chequesCONTROL" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idOrdenPago}" id="idOrdenPago" name="idOrdenPago"/>
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#libroCheques" data-toggle="tab" id="libroChequesPDF"><span class="step">1</span> <span class="title" style="font-weight:bold;">LIBRO DE CHEQUES</span></a></li>
                                    <li><a href="#chequesEntregados" data-toggle="tab" id="chequesEntregadosPDF"><span class="step">2</span> <span class="title" style="font-weight:bold;">CHEQUES ENTREGADOS</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="libroCheques">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="libro" style="height: 560px" >

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="chequesEntregados">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="entregados" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Último</a></li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();

        $('#libroChequesPDF').click(function () {
            var url = '{$_Parametros.url}modCP/pagos/chequesCONTROL/imprimirLibroChequeMET/';
                $('#libro').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

        });

       $('#chequesEntregadosPDF').click(function () {
           var url = '{$_Parametros.url}modCP/pagos/chequesCONTROL/imprimirChequeEntregadosMET/';
           $('#entregados').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

       });

    });

</script>