
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{if isset($lista) and $lista =='devolver'}DEVOLVER CHEQUES{elseif ($lista =='cobrarCheque')} INGRESO DE CHEQUES COBRADOS{else}ENTREGAR CHEQUES {/if}</h2>
    </div>

    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    {if ($lista =='cobrarCheque')}
                        <div class="col-sm-4">
                            <div class=" col-sm-3 text-right" >
                                <label for="fec_cobrado"
                                       class="control-label" style="margin-top: 10px;"> Fecha de Cobranza:</label>
                            </div>
                            <div class="col-sm-5" id="fec_cobradoError">
                                <input type="text" class="form-control text-center date"
                                       id="fec_cobrado" name="fec_cobrado"
                                       style="font-weight:bold;"
                                       value=""
                                        readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-3 text-right">
                                <label for="nroOperacion"
                                       class="control-label" style="margin-top: 10px;"> Nro. de Operacion:</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control"
                                       id="nroOperacion"
                                       style="font-weight:bold;"
                                       value="">
                            </div>
                        </div>
                    {/if}
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>CHEQUE</th>
                            <th>PROVEEDOR</th>
                            <th>FECHA PAGO</th>
                            <th>MONTO</th>
                            <th>PREPAGO</th>
                            <th>ACCIONES</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$grupo=0}
                        {foreach item=i from=$dataBD}
                            {if ($grupo != $i.pk_num_cuenta)}
			                   {$grupo = $i.pk_num_cuenta}
                                    <tr style="font-weight:bold; background-color:#C7C7C7;">
                                        <td colspan="5">
                                            Cta. Bancaria: &nbsp;
                                            {$i.ind_num_cuenta} &nbsp;
                                            {$i.a006_miscelaneo_detalle_banco}
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
		                     {/if}
                            <tr id="idPago{$i.pk_num_pago}">
                                <td>{$i.ind_num_pago}</td>
                                <td>{$i.nombreProveedor}</td>
                                <td>{$i.fec_pago}</td>
                                <td>{$i.num_monto_pago}</td>
                                <td>{$i.ind_nro_proceso}</td>
                                <td>
                                    {if in_array('CP-01-03-05-01-V',$_Parametros.perfil)}
                                    <button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static"  id="VER" idPago="{$i.pk_num_pago}" title="Consultar"
                                            descipcion="El Usuario esta viendo un cheque" titulo="<i class='md md-remove-red-eye'></i> Consultar Cheque">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                    {if in_array('CP-01-03-05-02-P',$_Parametros.perfil)}
                                    <button class="procesar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary-light"
                                            data-keyboard="false" data-backdrop="static"
                                            {if $lista==''} idAccion="entregar" {elseif $lista=='cobrarCheque'} idAccion="cobrar" {else} idAccion="devolver" {/if} idTipoBoton="PROCESAR" idPago="{$i.pk_num_pago}" title="Procesar"
                                            descipcion="El Usuario esta procesando un cheque" titulo="<i class='icm icm-spinner9'></i> Procesar Cheque">
                                        <i class="icm icm-spinner9" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>

<script type="text/javascript">

    $(document).ready(function() {
        var app = new AppFunciones();
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        var url='{$_Parametros.url}modCP/pagos/chequesCONTROL/accionesMET/';

        $('#datatable1 tbody').on( 'click', '.acciones', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idPago: $(this).attr('idPago'), estado: $(this).attr('id') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.procesar', function () {
            var idPago = $(this).attr('idPago');
            var idTipoBoton = $(this).attr('idTipoBoton');
            var idAccion = $(this).attr('idAccion');
            var fechaCobrado = $('#fec_cobrado').val();

            var url = '{$_Parametros.url}modCP/pagos/chequesCONTROL/accionesMET/';
            swal({
                title: 'Confirmar Cambio',
                text: 'Usted esta seguro(a) que desea cambiar el Estado???',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Si, Confirmar',
                closeOnConfirm: false
            }, function(){
                $.post(url,{  idPago: idPago, estado: idTipoBoton, accion: idAccion, fechaCobrado: fechaCobrado }, function(dato){
                   if (dato['status'] == 'errorSQL') {
                        swal("Error!", dato['mensaje'], "error");
                    } else if (dato['status'] == 'ok') {
                        swal("Cambio Realizado!", "El Cambio fue Realizado Satisfactoriamente.", "success");
                       $('#fec_cobrado').attr('value','');
                       $(document.getElementById('idPago'+idPago)).remove();
                    } else if (dato['status'] == 'errorFecha') {
                        app.metValidarError(dato, 'Disculpa. Debe ingresar el campo Fecha de Cobranza');
                    }
                }, 'json');

            });
        });
    });
</script>