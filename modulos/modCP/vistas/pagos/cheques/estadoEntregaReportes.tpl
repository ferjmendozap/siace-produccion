<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">ESTADO DE ENTREGA DE CHEQUES </h2>
                    </div>

                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="{$_Parametros.url}modCP/pagos/chequesCONTROL" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idOrdenPago}" id="idOrdenPago" name="idOrdenPago"/>
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#enCartera" data-toggle="tab" id="enCarteraPDF"><span class="step">1</span> <span class="title" style="font-weight:bold;">EN CARTERA</span></a></li>
                                    <li><a href="#entregados" data-toggle="tab" id="entregadosPDF"><span class="step">2</span> <span class="title" style="font-weight:bold;">ENTREGADOS</span></a></li>
                                    <li><a href="#cobrados" data-toggle="tab" id="cobradosPDF"><span class="step">3</span> <span class="title" style="font-weight:bold;">COBRADOS</span></a></li>
                                    <li><a href="#devueltos" data-toggle="tab" id="devueltosPDF"><span class="step">4</span> <span class="title" style="font-weight:bold;">DEVUELTOS</span></a></li>
                                    <li><a href="#carteraProveedor" data-toggle="tab" id="proveedorPDF"><span class="step">5</span> <span class="title" style="font-weight:bold;">EN CARTERA POR PROVEEDOR</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="enCartera">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="libro" style="height: 560px" >

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="entregados">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="estadoEntregados" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="cobrados">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="estadoCobrados" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="devueltos">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="estadoDevueltos" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="carteraProveedor">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="proveedor" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="ultio">Último</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Siguiente</a></li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();

        $('#enCarteraPDF').click(function () {
            var url = '{$_Parametros.url}modCP/pagos/chequesCONTROL/imprimirChequeCarteraMET/';
                $('#libro').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');
        });
       $('#entregadosPDF').click(function () {
           var url = '{$_Parametros.url}modCP/pagos/chequesCONTROL/imprimirChequeEstadoEntregadosMET/';
           $('#estadoEntregados').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');
       });
       $('#cobradosPDF').click(function () {
           var url = '{$_Parametros.url}modCP/pagos/chequesCONTROL/imprimirChequeEstadoCobradosMET/';
           $('#estadoCobrados').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');
       });
       $('#devueltosPDF').click(function () {
           var url = '{$_Parametros.url}modCP/pagos/chequesCONTROL/imprimirChequeEstadoDevueltosMET/';
           $('#estadoDevueltos').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');
       });
       $('#proveedorPDF').click(function () {
           var url = '{$_Parametros.url}modCP/pagos/chequesCONTROL/imprimirChequeEstadoProveedorMET/';
           $('#proveedor').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');
       });

    });

</script>