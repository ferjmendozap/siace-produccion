<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">

                        <form action="" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">

                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#general" data-toggle="tab"><span class="step">1</span> <span class="title" >INFORMACION GENERAL</span></a></li>
                                    <li><a href="#sustento" data-toggle="tab"><span class="step">2</span> <span class="title">SUSTENTO DEL PAGO</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="general">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="card-head card-head-xs style-primary text-center">
                                                        <header class="text-center">Informacion Adicional</header>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="proveedor"
                                                                       class="control-label" style="margin-top: 10px;"> Pagar A:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       id="proveedor"
                                                                       style="font-weight:bold;"
                                                                       value="{if isset($pagoChequeBD.nombreProveedor)}{$pagoChequeBD.nombreProveedor}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="pk_num_tipo_documento"
                                                                       class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="pk_num_tipo_documentoError" style="margin-top: -10px;">
                                                                    <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"  disabled  >
                                                                        <option value="">Seleccione Cuenta Bancaria</option>}
                                                                        {foreach item=proceso from=$listadoCuentas}
                                                                            {if isset($pagoChequeBD.pk_num_cuenta) and $pagoChequeBD.pk_num_cuenta == $proceso.pk_num_cuenta }
                                                                                <option value="{$proceso.pk_num_cuenta}" selected >{$proceso.ind_num_cuenta}</option>
                                                                            {else}
                                                                                <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="pago"
                                                                       class="control-label" style="margin-top: 10px;"> Pago:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="ind_nro_proceso"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.ind_nro_proceso)}{$pagoChequeBD.ind_nro_proceso}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <input type="text"
                                                                       id="num_proceso_secuencia"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.num_proceso_secuencia)}{$pagoChequeBD.num_proceso_secuencia}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="ind_num_pago"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.ind_num_pago)}{$pagoChequeBD.ind_num_pago}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="card-head card-head-xs style-primary text-center">
                                                            <header class="text-center">Datos del Pago</header>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="card-head card-head-xs style-primary text-center">
                                                            <header class="text-center">Estados del Pago</header>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="card-head card-head-xs style-primary text-center">
                                                            <header class="text-center">Contabilizacion</header>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="fec_Pago"
                                                                       class="control-label" style="margin-top: 10px;"> Fecha del Pago:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text"
                                                                       id="fec_Pago"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.fechaPago)}{$pagoChequeBD.fechaPago}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> De Impresion:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text"
                                                                       id="ind_estado"
                                                                       class="form-control" id="ind_estado"
                                                                       value="{if isset($pagoChequeBD.ind_estado_pago)}{$pagoChequeBD.ind_estado_pago}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Contabilizado:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="contabilizado"
                                                                       class="form-control"
                                                                       value=""
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Tipo de Pago:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="fk_a006_num_miscelaneo_tipo_pagoError" style="margin-top: -10px;">
                                                                    <select class="form-control select2-list select2" required disabled
                                                                            data-placeholder="Seleccione Tipo de Pago">
                                                                        <option value="">Seleccione...</option>
                                                                        {foreach item=tipoPago from=$listadoTipoPago}
                                                                            {if isset($pagoChequeBD.fk_a006_num_miscelaneo_tipo_pago) and $pagoChequeBD.fk_a006_num_miscelaneo_tipo_pago == $tipoPago.pk_num_miscelaneo_detalle}
                                                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}"
                                                                                        selected>{$tipoPago.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}">{$tipoPago.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> De Entrega:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text"
                                                                       id="ind_estado_entrega"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.ind_estado_entrega)}{$pagoChequeBD.ind_estado_entrega}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Voucher:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="periodo"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.periodo)}{$pagoChequeBD.periodo}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="fk_cbb001_num_voucher_pago"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.fk_cbb001_num_voucher_pago)}{$pagoChequeBD.fk_cbb001_num_voucher_pago}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Origen:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text"
                                                                       id="origen_generacion"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.periodo)}{$pagoChequeBD.periodo}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Fecha de Entrega:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text"
                                                                       id="FechaEntregado"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.fec_entregado)}{$pagoChequeBD.fec_entregado}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="card-head card-head-xs style-primary text-center">
                                                                <header class="text-center">Inf. Adicional</header>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Monto Pago:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text"
                                                                       id="num_monto_pago"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.num_monto_pago)}{$pagoChequeBD.num_monto_pago}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="col-sm-4 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> De Cobro:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text"
                                                                       id="ind_num_caja_chica"
                                                                       class="form-control" id="ind_num_caja_chica"
                                                                       value="{if isset($pagoChequeBD.num_flag_cobrado) and $pagoChequeBD.num_flag_cobrado==0}Pendiente{else}Cobrado{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="col-sm-9">
                                                                <div class="checkbox checkbox-styled">
                                                                    <label>
                                                                        <input type="checkbox" {if isset($pagoChequeBD.num_flag_negociable) and $pagoChequeBD.num_flag_negociable==1} checked{/if} value="1" disabled>
                                                                        <span>Cheque No Negociable</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="card-head card-head-xs style-primary text-center">
                                                            <header class="text-center">Anulacion / Reemplazo</header>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_anulaError">
                                                                <input type="text" class="form-control" value="{if isset($pagoChequeBD.fk_rhb001_num_empleado_anula)}{$pagoChequeBD.EMPLEADO_ANULA}{/if}" disabled id="fk_rhb001_num_empleado_anula">
                                                                <label for="fk_rhb001_num_empleado_anula"><i class="md md-person"></i> Anulado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_anulacionError">
                                                                <input type="text" class="form-control" value="{if isset($pagoChequeBD.fec_anulacion)}{$pagoChequeBD.fec_anulacion}{/if}" disabled name="form[alphaNum][fec_anulacion]" id="fec_anulacion" >
                                                                <label for="fec_anulacion"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-4 form-group floating-label text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Voucher Anulacion:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="periodo"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.periodo)}{$pagoChequeBD.periodo}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="fk_cbb001_num_voucher_pago"
                                                                       class="form-control"
                                                                       value="{if isset($pagoChequeBD.fk_cbb001_num_voucher_anulacion)}{$pagoChequeBD.fk_cbb001_num_voucher_anulacion}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="ind_motivo_anulacionError">
                                                                <textarea id="ind_motivo_anulacion" class="form-control" rows="2" placeholder="" disabled >{if isset($pagoChequeBD.ind_motivo_anulacion)}{$pagoChequeBD.ind_motivo_anulacion}{/if}</textarea>
                                                                <label for="ind_motivo_anulacion"><i class="md md-comment"></i> Razon Anulación:</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="NomReemplazadoPorError">
                                                                <input type="text" class="form-control" value="{if isset($pagoChequeBD.NomReemplazadoPor)}{$pagoChequeBD.NomReemplazadoPor}{/if}" name="form[int][fk_rhb001_num_empleado_anula]" disabled id="NomReemplazadoPor">
                                                                <label for="NomReemplazadoPor"><i class="md md-person"></i> Reemplazado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_anulacionError">
                                                                <input type="text" class="form-control" value="{if isset($pagoChequeBD.fec_anulacion)}{$pagoChequeBD.fec_anulacion}{/if}" disabled name="form[alphaNum][fec_anulacion]" id="fec_anulacion">
                                                                <label for="fec_anulacion"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="sustento">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="table-responsive">
                                                        <table id="datatable1" class="table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>PROVEEDOR</th>
                                                                <th>DOCUMENTO</th>
                                                                <th>FECHA</th>
                                                                <th>ESTADO</th>
                                                                <th>MONTO PAGADO</th>
                                                                <th>MONTO RETENCION</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>{$pagoChequeBD.nombreProveedor}</td>
                                                                <td>{$pagoChequeBD.ind_nro_control}</td>
                                                                <td>{$pagoChequeBD.fec_registro}</td>
                                                                <td>{$pagoChequeBD.ind_estado}</td>
                                                                <td>{$pagoChequeBD.num_monto_obligacion}</td>
                                                                <td>{$pagoChequeBD.num_monto_impuesto_otros}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div></div>
                                </div>

                                <ul class="pager wizard">
                                    <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Último</a></li>
                                </ul>

                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label">
                                                    <input type="text" disabled class="form-control disabled"
                                                           value="{if isset($pagoChequeBD.ind_usuario)}{$pagoChequeBD.ind_usuario}{/if}"
                                                           id="ind_usuario">
                                                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label" id="fec_ultima_modificacionError">
                                                <input type="text" disabled class="form-control disabled"
                                                       value="{if isset($pagoChequeBD.fec_ultima_modificacion)}{$pagoChequeBD.fec_ultima_modificacion}{/if}"
                                                       id="fec_ultima_modificacion">
                                                <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal"
            descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">Cancelar
    </button>

    {if isset($estado)}
        {if $estado == 'AN'}
            <button type="button" class="btn btn-danger accionesEstado logsUsuarioModal" id="ANULAR">Anular</button>
        {/if}
    {/if}
</div>

<script type="text/javascript">

    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();
        /// Complementos
        $('.select2').select2({ allowClear: true});
        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "80%");

        {if isset($estado)}
        $('.accionesEstado').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post(
                    $("#formAjax").attr("action"),
                    { idPago: $('#idPago').val(), estado: $(this).attr('id'), motivo: $('#ind_motivo_anulacion').val()},
                    function(dato){
                        if (dato['status'] == 'errorSQL') {
                            app.metValidarError(dato, 'Disculpa. Contacta Con la Direccion Tecnica porque hay un ERROR interno');
                        } else if (dato['status'] == 'OK') {
                            $(document.getElementById('idPago'+dato['idPago'])).remove();
                            swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        }
                    },'JSON');
        });
        {/if}

    });

</script>