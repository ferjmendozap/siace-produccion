<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">

                        <form action="{$_Parametros.url}modCP/pagos/ordenPagoCONTROL/accionesOrdenPagoMET" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="GENERAR" id="estado" name="estado"/>
                            <input type="hidden" value="{$idOrdenPago}" id="idOrdenPago" name="idOrdenPago"/>

                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#obligaciones" data-toggle="tab"><span class="step">1</span> <span class="title">DETALLADO POR OBLIGACIONES</span></a></li>
                                    <li><a href="#cuentas" data-toggle="tab"><span class="step">2</span> <span class="title">TOTAL POR CUENTAS BANCARIAS</span></a></li>
                                    <li><a href="#pagoParcialtab" data-toggle="tab"><span class="step">3</span> <span class="title">PAGO PARCIAL</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="obligaciones">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="datatable1" class="table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>TIPO PAGO</th>
                                                    <th>PAGAR A</th>
                                                    <th>PROVEEDOR</th>
                                                    <th>DOC. FISCAL</th>
                                                    <th>TOTAL A PAGAR</th>
                                                    <th>NRO. DOCUMENTO</th>
                                                    <th>IMPONIBLE</th>
                                                    <th>NO AFECTO</th>
                                                    <th>MONTO IMPUESTO</th>
                                                    <th>MONTO RETENIDO</th>
                                                    <th>TOTAL OBLIGACION</th>
                                                    <th>MONTO ADELANTOS</th>
                                                    <th>MONTO PAGO PARCIAL</th>
                                                    <th>FECHA DOCUMENTO</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr id="idOrdenPago{$i.pk_num_orden_pago}">
                                                        <td>{$ordenPagoBD.a006_miscelaneo_detalle_tipo_pago}</td>
                                                        <td>{$ordenPagoBD.fk_a003_num_persona_proveedor_a_pagar}</td>
                                                        <td>{$ordenPagoBD.fk_a003_num_persona_proveedor}</td>
                                                        <td>{$ordenPagoBD.ind_documento_fiscal}</td>
                                                        <td style="font-weight:bold; text-align: center">{number_format($ordenPagoBD.num_monto_obligacion,2,',','.')}</td>
                                                        <td>{$ordenPagoBD.cod_tipo_documento} {$ordenPagoBD.ind_nro_control}</td>
                                                        <td>{number_format($ordenPagoBD.num_monto_afecto,2,',','.')}</td>
                                                        <td>{number_format($ordenPagoBD.num_monto_no_afecto,2,',','.')}</td>
                                                        <td>{number_format($ordenPagoBD.num_monto_impuesto,2,',','.')}</td>
                                                        <td>{number_format($ordenPagoBD.num_monto_impuesto_otros,2,',','.')}</td>
                                                        <td>{number_format($ordenPagoBD.num_monto_obligacion,2,',','.')}</td>
                                                        <td>{number_format($ordenPagoBD.num_monto_adelanto,2,',','.')}</td>
                                                        <td>{number_format($ordenPagoBD.num_monto_pago_parcial,2,',','.')}</td>
                                                        <td>{$ordenPagoBD.fec_documento}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="cuentas">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="table-responsive">
                                                        <table id="datatable1" class="table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>CUENTA BANCARIA</th>
                                                                <th>MONTO</th>
                                                                <th>PAGOS PENDIENTES</th>
                                                                <th>SALDO EN BANCO</th>
                                                                <th>DISPONIBLE</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr id="idOrdenPago{$detalleCuentaBD.pk_num_orden_pago}">
                                                                <td>{$detalleCuentaBD.ind_num_cuenta} {$detalleCuentaBD.a006_miscelaneo_detalle_banco}</td>
                                                                <td>{number_format($detalleCuentaBD.montoTotal,2,',','.')}</td>
                                                                <td>{number_format($saldoPendiente.montoPendiente,2,',','.')}</td>
                                                                <td>{number_format($dispFinanciera.montoActual,2,',','.')}</td>
                                                                <td>{number_format($saldoDisponible,2,',','.')}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="pagoParcialtab">
                                    <div class="card-body">
                                        {if isset($detallePagoParcial[0])}
                                            <div class="table-responsive">
                                                <table id="datatable3" class="table table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th># </th>
                                                        <th>FECHA DE PAGO </th>
                                                        <th>NRO. PAGO</th>
                                                        <th>MONTO </th>
                                                        <th>MONTO RESTANTE</th>
                                                        <th>ESTADO </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {$monto = 0} {$sec = 1}
                                                    {foreach item=i from=$detallePagoParcial}
                                                        {if $i.ind_estado_pago != 'ANULADO'}
                                                            {$montoRestante = $ordenPagoBD.num_monto_obligacion - $i.num_monto_pago - $monto}
                                                        {else}
                                                            {$montoRestante = $ordenPagoBD.num_monto_obligacion - $monto}
                                                        {/if}
                                                        <tr>
                                                            <td>{$sec}</td>
                                                            <td>{$i.fec_pago}</td>
                                                            <td>{$i.ind_num_pago}</td>
                                                            <td>{number_format($i.num_monto_pago,2,',','.')}</td>
                                                            <td>{number_format($montoRestante,2,',','.')}</td>
                                                            <td>{$i.ind_estado_pago}</td>
                                                        </tr>
                                                        {if $i.ind_estado_pago != 'ANULADO'}
                                                            {$monto = $monto + $i.num_monto_pago}
                                                        {/if}
                                                        {$sec = $sec +1}
                                                    {/foreach}
                                                    <input type="hidden" value="{$sec}" id="num_secuencia_pago" />
                                                    <input type="hidden" value="{$montoRestante}" id="montoRestante" />
                                                    </tbody>
                                                </table>
                                            </div>
                                        {else}
                                            <input type="hidden" value="1" id="num_secuencia_pago" />
                                            <input type="hidden" value="{$ordenPagoBD.ind_estado}" id="ind_estado" />
                                        {/if}
                                        <input type="hidden" value="{$ordenPagoBD.num_monto_obligacion}" id="montoRestante" />
                                        <div align="center">
                                            <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                                    id="pagoParcial"><i class="md md-add"></i> PAGO PARCIAL
                                            </button>
                                        </div>
                                        <div id="divPagoParciales" align="center">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                <li class="next last"><a class="btn-raised" href="javascript:void(0);">&Uacute;ltimo</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Pr&oacute;ximo</a></li>
                            </ul>

                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label">
                                                <input type="text" disabled class="form-control disabled"
                                                       value="{if isset($ordenPagoBD.ind_usuario)}{$ordenPagoBD.ind_usuario}{/if}"
                                                       id="ind_usuario">
                                                <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="col-sm-12">
                                        <div class="form-group floating-label" id="fec_ultima_modificacionError">
                                            <input type="text" disabled class="form-control disabled"
                                                   value="{if isset($ordenPagoBD.fec_ultima_modificacion)}{$ordenPagoBD.fec_ultima_modificacion}{/if}"
                                                   id="fec_ultima_modificacion">
                                            <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-primary logsUsuarioModal accionesEstado" id="GENERARBoton">
        <i class="md md-done"></i>&nbsp;Aceptar</button>
    <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
            data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>

    {if isset($estado)}
        {if $estado == 'AN'}
            <button type="button" class="btn btn-danger accionesEstado logsUsuarioModal" id="ANULAR">
                <i class="icm icm-blocked"></i>&nbsp;Anular</button>
        {/if}
    {/if}
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();
        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "90%");

        {if isset($estado)}
            $('.accionesEstado').click(function () {
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                    if (dato['status'] == 'errorSQL') {
                        app.metValidarError(dato, 'Disculpa. Contacta Con la Direccion Tecnica porque hay un ERROR interno');
                    }else if (dato['status'] == 'noDispFinanciera') {
                        swal('Error','Disculpe. No hay Disponibilidad Financiera en Banco para Generar el Pre-Pago', 'error');
                    }else if (dato['status'] == 'errorMonto') {
                        swal('Error','Disculpe. El monto a pagar debe ser solo numero', 'error');
                    }else if (dato['status'] == 'errorMontoPagado') {
                        swal('Error','Disculpe. El monto a pagar no puede ser mayor al monto restante', 'error');
                    }  else if (dato['status'] == 'OK') {
                        $(document.getElementById('idOrdenPago'+dato['idOrdenPago'])).remove();
                        swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }
                },'JSON');
            });
        {/if}

        $('#pagoParcial').on('click', function () {

            var montoPartida = $('#montoRestante').val();
            var num_secuencia_pago = $('#num_secuencia_pago').val();
            var num_monto_impuesto = 0;
            var num_monto_impuesto_otros = 0;
            if(num_secuencia_pago == 1){
                num_monto_impuesto = {$ordenPagoBD.num_monto_impuesto};
                num_monto_impuesto_otros = {$ordenPagoBD.num_monto_impuesto_otros};
            }else{
                num_monto_impuesto = 0;
                num_monto_impuesto_otros = 0;
            }

           {$sec = 1}
            $(document.getElementById('divPagoParciales')).append(
                '<table id="tablePagoParcialCalculo" class="table table-striped table-hover" style="width: 50%;">'+
                '<thead>'+
                '<tr>'+
                '<th>PORCENTAJE </th>'+
                '<th>MONTO </th>'+
                '<th>MONTO TOTAL</th>'+
                '<th></th>'+
                '</tr>'+
                '</thead>'+
                '<tbody>'+
                '<tr>'+
                '<input type="hidden" id="num_secuencia_pago" name="form[int][num_secuencia_pago]" value="'+num_secuencia_pago+'">' +
                '<input type="hidden" id="num_monto_impuesto" name="form[int][num_monto_impuesto]" value="'+num_monto_impuesto+'">' +
                '<input type="hidden" id="num_monto_impuesto_otros" name="form[int][num_monto_impuesto_otros]" value="'+num_monto_impuesto_otros+'">' +
                '<td style="text-align: center"><input type="text" class="form-control text-center" id="porcentaje" value=""></td>' +
                '<td style="text-align: center"><input type="text" class="form-control text-center" id="monto" value=""></td>' +
                '<td style="text-align: center"><input type="text" class="form-control text-center" name="montoTotal" id="montoTotal" readonly value="">' +
                '<td><button class="btn ink-reaction btn-raised btn-xs btn-danger delete"><i class="md md-delete"></i></button></td>' +
                '</tr>'+
                '</tbody>'+
                '</table>'+

                '<table id="tablePagoParcial" class="table table-striped table-hover" style="width: 75%;">'+
                '<thead>'+
                '<tr>'+
                    '<th># </th>'+
                    '<th>CUENTA CONTABLE </th>'+
                    '<th>CUENTA CONTABLE PUB20</th>'+
                    '<th>PARTIDA</th>'+
                    '<th>MONTO </th>'+
                    '<th>MONTO A PAGAR</th>'+
                    '<th>MONTO RESTANTE</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody>'+
                {foreach item=i from=$detallePartidaBD}
                    '<tr>'+
                    '<td><input type="text" class="form-control text-center" name="form[int][detalle][num_secuencia][]" readonly value="{$sec}" ></td>'+
                    '<td>' +
                        '<input type="hidden" name="form[int][detalle][{$sec}][fk_cbb004_num_cuenta]" value="{$i.pkCuenta}" >'+
                        '<input type="text" class="form-control text-center" readonly value="{$i.cod_cuenta}" > '+
                    '</td>'+
                    '<td>' +
                        '<input type="hidden" name="form[int][detalle][{$sec}][fk_cbb004_num_cuenta_pub20]" value="{$i.pkCuenta20}" > '+
                        '<input type="text" class="form-control text-center" readonly value="{$i.cod_cuenta20}" > '+
                    '</td>' +
                    '<td>' +
                        '<input type="hidden" name="form[int][detalle][{$sec}][fk_prb002_num_partida_presupuestaria]" value="{$i.pkPartida}" > '+
                        '<input type="text" class="form-control text-center" readonly value="{$i.cod_partida}" > '+
                    '</td>'+
                    '<td style="text-align: center"><input type="text" class="form-control text-center {$i.clase}" id="montoPartida{$i.pkPartida}" pkPartida="{$i.pkPartida}" value="{$i.num_monto}" readonly></td>' +
                    '<td style="text-align: center"><input type="text" class="form-control text-center" id="montoPagar{$i.pkPartida}" name="form[int][detalle][{$sec}][num_monto]" value="{$i.num_monto_pagar}" readonly></td>' +
                    '<td style="text-align: center"><input type="text" class="form-control text-center" id="montoRestante{$i.pkPartida}"  value="{$i.montoRestante}" readonly></td>' +
                    '<input type="hidden" id="montoRestante" name="montoRestante" value=""></td>' +
                    '</tr>'+
                {$sec = $sec +1}
                {/foreach}

                '</tbody>'+
                '</table>'
            );
            $('#pagoParcial').attr('disabled',true);
            $('#estado').attr('value','generarPP');
        });


        $('#divPagoParciales').on('change', '#porcentaje', function () {
            cambioPorcentaje($(this).val());
        });

        $('#divPagoParciales').on('change', '#monto', function () {
            cambioMonto($(this).val());
        });

        $('#divPagoParciales').on('click', '.delete', function () {
            $('#tablePagoParcialCalculo').remove();
            $('#tablePagoParcial').remove();
            $('#pagoParcial').attr('disabled',false);
            $('#generarPP').attr('id','GENERAR');
        });

        function cambioPorcentaje (porcentaje) {
            if (porcentaje <= 100) {
                var montoTotal = 0; var montoResta = 0;
                var num_monto_impuesto = $('#num_monto_impuesto').val();
                var num_monto_impuesto_otros = $('#num_monto_impuesto_otros').val();
                var montoTotalPagar = 0;
                $('.montoPartida').each(function (titulo, valor) {
                    var pkPartida = $(this).attr('pkPartida');

                    var montoPartida = $('#montoPartida' + pkPartida).val();
                    var montoPagar = montoPartida * (porcentaje / 100);
                    var montoRestante = montoPartida - montoPagar;
                    //monto total + impuesto
                    montoTotal = parseFloat(montoTotal) + parseFloat(montoPagar);
                    $('#montoPagar' + pkPartida).attr('value', montoPagar);
                    $('#montoRestante' + pkPartida).attr('value', montoRestante);
                    $('#monto').attr('value', montoTotal);

                    montoTotalPagar = parseFloat(montoTotal) + parseFloat(num_monto_impuesto) - parseFloat(num_monto_impuesto_otros);
                    montoResta = montoResta + montoRestante;

                    $('#montoTotal').attr('value', montoTotalPagar);
                    $('#tablePagoParcial #montoRestante').attr('value', montoResta);

                });

            } else {
                swal('ERROR', 'Disculpe, no se puede pagar mas del 100%', 'error');
                $('#porcentaje').attr('value', 0);
            }

        }

        function cambioMonto(monto) {
            var montoTotal = 0;
            $('.montoPartida').each(function (titulo, valor) {
                var pkPartida = $(this).attr('pkPartida');
                var montoPartida = $('#montoPartida' + pkPartida).val();
                montoTotal = parseFloat(montoTotal) + parseFloat(montoPartida);
            });
            var porcentaje = (monto * 100) / montoTotal;
            $('#porcentaje').attr('value', porcentaje);
            cambioPorcentaje(porcentaje);
        }

        function restaurarValores () {
            $('#montoPagar' + pkPartida).attr('value', montoPagar);
            $('#montoRestante' + pkPartida).attr('value', montoResta);
            $('#monto').attr('value', montoTotal);
            $('#montoTotal').attr('value', montoTotal);
        }

    });

</script>