<form action="{$_Parametros.url}modCP/maestros/obligaciones/tiposServicios/serviciosCONTROL/nuevoServicioMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idServicio}" name="idServicio"/>

    <div class="modal-body">
        <div class="col-lg-12">
            <div class="form-group floating-label" id="cod_tipo_servicioError">
                <input type="text" class="form-control" name="form[alphaNum][cod_tipo_servicio]" id="cod_tipo_servicio" {if isset($dataBD.cod_tipo_servicio)} value="{$dataBD.cod_tipo_servicio}" disabled {/if}>
                <label for="cod_tipo_servicio"><i class="md md-border-color"></i> Codigo</label>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group floating-label" id="ind_descripcionError">
                <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion" value="{if isset($dataBD.ind_descripcion)}{{$dataBD.ind_descripcion}}{/if}" {if $ver==1} disabled {/if}>
                <label for="ind_descripcion"><i class="md md-border-color"></i> Descripción</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_regimen_fiscalError">
                <select id="fk_a006_num_miscelaneo_regimen_fiscal" name="form[int][fk_a006_num_miscelaneo_regimen_fiscal]"
                        class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=proceso from=$listadoRegimen}
                        {if isset($dataBD.fk_a006_num_miscelaneo_regimen_fiscal) and $dataBD.fk_a006_num_miscelaneo_regimen_fiscal == $proceso.pk_num_miscelaneo_detalle }
                            <option value="{$proceso.pk_num_miscelaneo_detalle}" selected >{$proceso.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$proceso.pk_num_miscelaneo_detalle}">{$proceso.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_regimen_fiscal">Regimen Fiscal:</label>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-6">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($dataBD.num_estatus) and $dataBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1} disabled {/if}>
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary" align="center">
                    <header>Impuestos Aplicables</header>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <table class="table">
                        <tbody id="imput_nuevo">
                        {if isset($servicioDetalle.fk_cpb017_num_tipo_servico)}
                            {$nuevo = 0}
                            {foreach item=impuesto from=$servicioImpuesto}
                                {$nuevo = $nuevo + 1}
                                <tr id="nuevo{$nuevo}">
                                    <input type="hidden" value="{$nuevo}" name="form[int][nuevo][secuencia][{$nuevo}]">
                                    <td>
                                        {$var = $impuesto.fk_cpb015_num_impuesto}
                                        <select class="form-control" data-placeholder="Seleccione el Impuesto" name="form[int][nuevo][{$nuevo}][pk_num_impuesto]" idNum="{$nuevo}" {if $ver==1 } disabled{/if}>
                                            {foreach item=imp from=$listadoImpuesto}
                                                {if $var == $imp.pk_num_impuesto}
                                                    <option value="{$imp.pk_num_impuesto}" selected>{$imp.txt_descripcion}</option>
                                                {else}
                                                    <option value="{$imp.pk_num_impuesto}" >{$imp.txt_descripcion}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </td>
                                    <td style="text-align: center">
                                        <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$nuevo}" {if $ver==1}disabled{/if}><i class="md md-delete"></i></button>
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                        </tbody>
                    </table>
                </div>

                <div class="col-sm-12" align="center">
                    <button id="nuevoCampo" class="btn btn-info ink-reaction btn-raised" type="button" {if $ver==1 } disabled{/if}>
                        <i class="md md-add"></i> Agregar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $ver!=1}
            <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
                {if $idServicio!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
            </button>
            {/if}
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function() {
        var app = new  AppFunciones();

        /// Complementos
        $('.select2').select2({ allowClear: true });
        // anular accion del Formulario
        $("#formAjax").submit(function(){
            return false;
        });
        // ancho de la Modal
        $('#modalAncho').css( "width", "35%" );
        //
        $('#nuevoCampo').click(function () {
            var nuevo = $('#imput_nuevo tr').length + 1;
            $(document.getElementById('imput_nuevo')).append(
                    '<tr id="nuevo' + nuevo + '">' +
                    '<input type="hidden" value="'+nuevo+'" name="form[int][nuevo][secuencia]['+nuevo+']"> '+
                    '<td>' +
                    '<select class="form-control" data-placeholder="Seleccione el Impuesto" name="form[int][nuevo]['+nuevo+'][pk_num_impuesto]" idNum="'+nuevo+'" >' +
                    '{foreach item=imp from=$listadoImpuesto}'+
                    '<option value="{$imp.pk_num_impuesto}">{$imp.txt_descripcion}</option>' +
                    '{/foreach}' +
                    '</select>' +
                    '</td>' +
                    '<td style="text-align: center">'+
                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+nuevo+'"><i class="md md-delete"></i></button>' +
                    '</td>'+
                    '</tr>'
            );
        });
        $('#imput_nuevo').on('click', '.delete', function () {
            var campo = $(this).attr('id');
            $('#nuevo' + campo).remove();
        });
        //Guardado y modales
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ['num_estatus'];
                var arrayMostrarOrden = ['cod_tipo_servicio','ind_descripcion','RegimenFiscal','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Tipo de Servicio fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Tipo de Servicio fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            },'json');
        });
    });
</script>