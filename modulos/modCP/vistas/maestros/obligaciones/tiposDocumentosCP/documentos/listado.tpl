
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipos de documentos de Obligaciones</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Cod.</th>
                            <th>Descripcion</th>
                            <th>Ade.</th>
                            <th>Pro.</th>
                            <th>Fis</th>
                            <th>Nom</th>
                            <th>Tipo de Voucher</th>
                            <th>Clasificación</th>
                            <th>Regimen Fiscal</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            {if in_array('CP-01-07-01-03-03-E',$_Parametros.perfil)}
                            <th colspan="11"><button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descipcion="el Usuario a creado un post de Documento" data-toggle="modal"
                                                    data-target="#formModal" titulo="<i class='md md-create'></i>&nbsp;Registrar Nuevo Tipo de Documento" id="nuevo"
                                                    data-keyboard="false" data-backdrop="static">
                                    <i class="md md-create"></i>&nbsp;Nuevo Documento
                                </button></th>
                            {/if}
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/obligaciones/tiposDocumentosCP/documentosCONTROL/nuevoDocumentoMET';

        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}modCP/maestros/obligaciones/tiposDocumentosCP/documentosCONTROL/jsonDataTablaMET",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "cod_tipo_documento" },
                { "data": "ind_descripcion" },
                { "orderable": false,"data": "num_flag_adelanto"},
                { "orderable": false,"data": "num_flag_provision"},
                { "orderable": false,"data": "num_flag_fiscal"},
                { "orderable": false,"data": "num_flag_auto_nomina"},
                { "data": "cod_voucher" },
                { "data": "clasificacion" },
                { "data": "regimen" },
                { "orderable": false,"data": "num_estatus"},
                { "orderable": false,"data": "acciones"}
            ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
            $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idDocumento: $(this).attr('idDocumento') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idDocumento=$(this).attr('idDocumento');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/obligaciones/tiposDocumentosCP/documentosCONTROL/eliminarMET';
                $.post($url, { idDocumento: idDocumento },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idDocumento'+dato['idDocumento'])).html('');
                        swal("Eliminado!", "el Tipo de Documento ha sido eliminado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>