<form action="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/nuevoImpuestoMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idImpuesto}" name="idImpuesto"/>

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="col-sm-3">
                <div class="form-group floating-label" id="cod_impuestoError">
                    <input type="text" class="form-control" maxlength="3" name="form[alphaNum][cod_impuesto]" id="cod_impuesto" {if isset($dataBD.cod_impuesto)} value="{$dataBD.cod_impuesto}" disabled {/if} >
                    <label for="cod_impuesto"><i class="md md-border-color"></i> Codigo</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="txt_descripcionError">
                <input type="text" class="form-control" name="form[alphaNum][txt_descripcion]" id="txt_descripcion"  value="{if isset($dataBD.txt_descripcion)}{$dataBD.txt_descripcion}{/if}" {if $ver==1 } disabled{/if}>
                <label for="txt_descripcion"><i class="md md-border-color"></i> Descripcion</label>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="col-sm-4 form-group">
                <label for="ind_signo"><i class="md md-border-color"></i> Signo: </label>
            </div>
            <div class="radio radio-styled">
                <label>
                    <span>Positivo</span> <input type="radio" id="ind_signo" name="form[alphaNum][ind_signo]" value="1"
                           {if isset($dataBD.ind_signo) and $dataBD.ind_signo == 1 }checked {/if}
                            {if !isset($dataBD.ind_signo)}checked {/if}{if $ver==1 } disabled{/if}>
                </label>
                <label>
                    <span>Negativo</span> <input type="radio" id="ind_signo" name="form[alphaNum][ind_signo]" value="2"
                               {if isset($dataBD.ind_signo) and $dataBD.ind_signo == 2 }checked {/if} {if $ver==1 } disabled{/if}>
                </label>
            </div>
        </div>
        <div class="col-sm-10">
                <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_regimen_fiscalError" style="margin-top: -10px;">
                    <select id="fk_a006_num_miscelaneo_detalle_regimen_fiscal" name="form[int][fk_a006_num_miscelaneo_detalle_regimen_fiscal]"
                            class="form-control select2" {if $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=proceso from=$listadoRegimen}
                            {if isset($dataBD.fk_a006_num_miscelaneo_detalle_regimen_fiscal) and $dataBD.fk_a006_num_miscelaneo_detalle_regimen_fiscal == $proceso.pk_num_miscelaneo_detalle }
                                <option value="{$proceso.pk_num_miscelaneo_detalle}" selected >{$proceso.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$proceso.pk_num_miscelaneo_detalle}">{$proceso.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_a006_num_miscelaneo_detalle_flag_provision"> Regimen Fiscal:</label>
                </div>
        </div>
        <div class="col-sm-10">
            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_flag_provisionError" style="margin-top: -10px;">
                <select id="fk_a006_num_miscelaneo_detalle_flag_provision" name="form[int][fk_a006_num_miscelaneo_detalle_flag_provision]"
                        class="form-control select2" {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=provision from=$selectPROVDOC}
                        {if isset($dataBD.fk_a006_num_miscelaneo_detalle_flag_provision) and $dataBD.fk_a006_num_miscelaneo_detalle_flag_provision == $provision.pk_num_miscelaneo_detalle}
                            <option value="{$provision.pk_num_miscelaneo_detalle}" selected >{$provision.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$provision.pk_num_miscelaneo_detalle}">{$provision.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_flag_provision"> Provision En:</label>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_flag_imponibleError" style="margin-top: -10px;">
                <select id="fk_a006_num_miscelaneo_detalle_flag_imponible" name="form[int][fk_a006_num_miscelaneo_detalle_flag_imponible]"
                        class="form-control select2" {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=provision from=$selectIVAIMP}
                        {if isset($dataBD.fk_a006_num_miscelaneo_detalle_flag_imponible) and $dataBD.fk_a006_num_miscelaneo_detalle_flag_imponible == $provision.pk_num_miscelaneo_detalle}
                            <option value="{$provision.pk_num_miscelaneo_detalle}" selected >{$provision.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$provision.pk_num_miscelaneo_detalle}">{$provision.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_flag_imponible"> Imponible:</label>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_tipo_comprobanteError" style="margin-top: -10px;">
                <select id="fk_a006_num_miscelaneo_detalle_tipo_comprobante" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_comprobante]"
                        class="form-control select2" {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=provision from=$selectCOMPIMP}
                        {if isset($dataBD.fk_a006_num_miscelaneo_detalle_tipo_comprobante) and $dataBD.fk_a006_num_miscelaneo_detalle_tipo_comprobante == $provision.pk_num_miscelaneo_detalle}
                            <option value="{$provision.pk_num_miscelaneo_detalle}" selected >{$provision.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$provision.pk_num_miscelaneo_detalle}">{$provision.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_flag_imponible"> Tipo:</label>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_flag_generalError" style="margin-top: -10px;">
                <select id="fk_a006_num_miscelaneo_detalle_flag_general" name="form[int][fk_a006_num_miscelaneo_detalle_flag_general]"
                        class="form-control select2" {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=clasificacion from=$selectCLASIMP}
                        {if isset($dataBD.fk_a006_num_miscelaneo_detalle_flag_general) and $dataBD.fk_a006_num_miscelaneo_detalle_flag_general == $clasificacion.pk_num_miscelaneo_detalle}
                            <option value="{$clasificacion.pk_num_miscelaneo_detalle}" selected >{$clasificacion.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$clasificacion.pk_num_miscelaneo_detalle}">{$clasificacion.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_flag_imponible"> Clasificaci&oacute;n:</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8">
                <div class="form-group " id="fk_prb002_num_partida_presupuestariaError">
                    <input type="text" class="form-control"   value="{if isset($dataBD.fk_prb002_num_partida_presupuestaria)}{$dataBD.cod_partida}{/if}" id="partida" disabled >
                    <input type="hidden" name="form[int][fk_prb002_num_partida_presupuestaria]" id="fk_prb002_num_partida_presupuestaria" value="{if isset($dataBD.fk_prb002_num_partida_presupuestaria)}{$dataBD.fk_prb002_num_partida_presupuestaria}{/if}" >
                    <label for="fk_prb002_num_partida_presupuestaria">Partida</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group floating-label">
                    <div class="form-group floating-label">
                        <button
                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                type="button"
                                data-toggle="modal" data-target="#formModal2"
                                titulo="Listado - Partidas"
                                url="{$_Parametros.url}modCP/maestros/cajaChicaReportes/conceptoGasto/conceptoGastoCONTROL/partidaMET/partida/"
                                {if $ver==1 } disabled {/if}>
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8">
                <div class="form-group" id="fk_cbb004_num_plan_cuentaError">
                        <input type="text" class="form-control" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta)}{$dataBD.cuenta}{/if}" id="indCuenta" disabled>
                        <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta]" id="fk_cbb004_num_plan_cuenta" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta)}{$dataBD.fk_cbb004_num_plan_cuenta}{/if}">
                        <label for="fk_cbb004_num_plan_cuenta">Cuenta Contable</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group floating-label">
                    <button
                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                            type="button"
                            data-toggle="modal" data-target="#formModal2"
                            titulo="Listado de Plan Cuenta"
                            url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuenta/"
                            {if $ver==1} disabled {/if}>
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8">
                <div class="form-group" id="fk_cbb004_num_plan_cuenta_pub20Error">
                    <input type="text" class="form-control"   value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_pub20)}{$dataBD.cuentaPub20}{/if}" id="indCuentaPub20" disabled>
                    <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta_pub20]" id="fk_cbb004_num_plan_cuenta_pub20" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_pub20)}{$dataBD.fk_cbb004_num_plan_cuenta_pub20}{/if}">
                    <label for="fk_cbb004_num_plan_cuenta_pub20">Cuenta Public. 20</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group floating-label">
                    <button
                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                            type="button"
                            data-toggle="modal" data-target="#formModal2"
                            titulo="Listado de Plan Cuenta"
                            url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuentaPub20"
                            {if $ver==1} disabled {/if}>
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group floating-label" id="num_factor_porcentajeError">
                <input type="text" name="form[int][num_factor_porcentaje]" value="{if isset($dataBD.num_factor_porcentaje)}{number_format($dataBD.num_factor_porcentaje,2,',','.')}{else}{/if}" class="form-control" id="num_factor_porcentaje" {if $ver==1 } disabled{/if}>
                <label for="ind_factor_porcentaje">Porcentaje</label>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($dataBD.num_estatus) and $dataBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 ">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.ind_usuario)}{$dataBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.fec_ultima_modificacion)}{$dataBD.fec_ultima_modificacion}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro"
                data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if !$ver==1 }
        <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
            {if $idImpuesto!=0}
                <i class="fa fa-edit"></i>&nbsp;Modificar
            {else}
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            {/if}
                </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $("#num_factor_porcentaje").inputmask('999,99 %', {
            numericInput: true, rightAlignNumerics: false
        });

        /// Complementos
        var app = new  AppFunciones();
        $('.select2').select2({ allowClear: true });

        // ancho de la Modal
        $('#modalAncho').css( "width", "40%" );

        // anular accion del Formulario
        $("#formAjax").submit(function(){
            return false;
        });

        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        //Guardado y modales
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
               if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Impuesto fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Impuesto fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            },'json');
        });
    });
</script>