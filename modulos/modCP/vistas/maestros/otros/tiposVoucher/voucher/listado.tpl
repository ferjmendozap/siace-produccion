
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">TIPOS DE VOUCHER</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Descripcion</th>
                            <th>Manual</th>
                            <th>Estado</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=voucher from=$voucherBD}
                            <tr id="idVoucher{$voucher.pk_num_voucher_mast}">
                                <td>{$voucher.cod_voucher}</td>
                                <td>{$voucher.ind_descripcion}</td>
                                <td><i class="{if $voucher.num_flag_manual==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>
                                <td>
                                    <i class="{if $voucher.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>

                                <td>
                                    {if in_array('CP-01-07-05-01-01-M',$_Parametros.perfil)}
                                    <button class="modificar tipoVoucher btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idVoucher="{$voucher.pk_num_voucher_mast}" title="Editar"
                                            descipcion="El Usuario a Modificado un Tipo de Voucher" titulo="<i class='fa fa-edit'></i> Editar Tipo de Voucher">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                    {if in_array('CP-01-07-05-01-02-V',$_Parametros.perfil)}
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idVoucher="{$voucher.pk_num_voucher_mast}" title="Consultar"
                                                descipcion="El Usuario esta viendo un Tipo de Voucher" titulo="<i class='md md-remove-red-eye'></i> Consultar Tipo de Voucher">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                     </button>
                                    {/if}
                                    {if in_array('CP-01-07-05-01-03-E',$_Parametros.perfil)}
                                        <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idVoucher="{$voucher.pk_num_voucher_mast}" title="Eliminar"  boton="si, Eliminar"
                                                descipcion="El usuario ha eliminado un Tipo de Voucher" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Voucher!!">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                    </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="3">
                                {if in_array('CP-01-07-05-01-04-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado un post de Tipo de Voucher"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="<i class='md md-create'></i>&nbsp;Registrar Nuevo Tipo de Voucher" id="nuevo"
                                        data-keyboard="false"
                                        data-backdrop="static"><i class="md md-create"></i>&nbsp;Nuevo Tipo de Voucher
                                </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/otros/tiposVoucher/voucherCONTROL/nuevoVoucherMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idVoucher:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idVoucher: $(this).attr('idVoucher') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idVoucher: $(this).attr('idVoucher')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idVoucher=$(this).attr('idVoucher');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/otros/tiposVoucher/voucherCONTROL/eliminarMET';
                $.post($url, { idVoucher: idVoucher },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idVoucher'+dato['idVoucher'])).html('');
                        swal("Eliminado!", "el Tipo de Voucher ha sido eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>