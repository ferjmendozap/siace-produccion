<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">LISTADO DE UNIDAD TRIBUTARIA</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Año</th>
                                <th>Fecha</th>
                                <th>Gaceta</th>
                                <th>Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=unidadTributaria from=$listado}
                                <tr id="idUnidadTributaria{$unidadTributaria.pk_num_unidad_tributaria}">
                                    <td><label>{$unidadTributaria.ind_anio}</label></td>
                                    <td><label>{$unidadTributaria.fec}</label></td>
                                    <td><label>{$unidadTributaria.txt_gaceta_oficial}</label></td>
                                    <td><label>{$unidadTributaria.ind_valor}</label></td>
                                    <td align="center">
                                        {if in_array('CP-01-07-05-02-01-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idUnidadTributaria="{$unidadTributaria.pk_num_unidad_tributaria}" title="Modificar"
                                                    descipcion="El Usuario ha Modificado una Unidad Tributaria" titulo="<i class='md md-map'></i> Modificar Unidad Tributaria">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        {if in_array('CP-01-07-05-02-02-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idUnidadTributaria="{$unidadTributaria.pk_num_unidad_tributaria}" title="Consultar"
                                                    descipcion="El Usuario esta viendo una Unidad Tributaria" titulo="<i class='md md-remove-red-eye'></i> Consultar Unidad Tributaria">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('CP-01-07-05-02-03-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario ha creado una Unidad Tributaria"  titulo="<i class='md md-map'></i> Crear Unidad Tributaria" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nueva Unidad Tributaria
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modCP/maestros/otros/unidadTributaria/unidadTributariaCONTROL/crearModificarMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idUnidadTributaria:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idUnidadTributaria: $(this).attr('idUnidadTributaria') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
        $('#datatable tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{ idUnidadTributaria: $(this).attr('idUnidadTributaria'), ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

    });
</script>