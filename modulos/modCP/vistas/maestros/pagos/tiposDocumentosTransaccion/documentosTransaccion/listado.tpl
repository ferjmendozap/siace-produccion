
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipos de documentos de Transacciones Bancarias</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Cod.</th>
                            <th>Descripcion</th>
                            <th>Tipo de Transaccion</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$Documentos}
                                <tr id="idDocumento{$documento.pk_num_tipo_documento}">
                                    <td>{$documento.cod_tipo_documento}</td>
                                    <td>{$documento.ind_descripcion}</td>
                                    <td>{$documento.ind_nombre_detalle}</td>
                                    <td><i class="{if $documento.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>

                                    <td width="120">
                                        {if in_array('CP-01-07-01-03-01-M',$_Parametros.perfil)}
                                        <button class="modificar Tipo Documento btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_tipo_documento}" title="Editar"
                                                descipcion="El Usuario ha Modificado un Tipo de Documento" titulo="<i class='fa fa-edit'></i> Editar Tipo Documento">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                        {if in_array('CP-01-07-01-03-02-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_tipo_documento}" title="Consultar"
                                                    descipcion="El Usuario esta viendo un Tipo de Documento" titulo="<i class='md md-remove-red-eye'></i> Consultar Tipo de Documento">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        {if in_array('CP-01-07-01-03-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDocumento="{$documento.pk_num_tipo_documento}" title="Eliminar"  boton="si, Eliminar"
                                                    descipcion="El usuario ha eliminado un Tipo de Documento" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Documento!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}

                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            {if in_array('CP-01-07-01-03-03-E',$_Parametros.perfil)}
                            <th colspan="5"><button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descipcion="el Usuario a creado un post de Documento" data-toggle="modal"
                                                    data-target="#formModal" titulo="<i class='md md-create'></i>&nbsp;Registrar Nuevo Tipo de Documento" id="nuevo"
                                                    data-keyboard="false" data-backdrop="static">
                                    <i class="md md-create"></i>&nbsp;Nuevo Documento
                                </button></th>
                            {/if}
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {
        var $url='{$_Parametros.url}modCP/maestros/pagos/tiposDocumentosTransaccion/documentosTransaccionCONTROL/nuevoDocumentoMET';

        $('#nuevo').click(function(){

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function(dato){
            $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idDocumento: $(this).attr('idDocumento') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idDocumento=$(this).attr('idDocumento');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/obligaciones/tiposDocumentosCP/documentosCONTROL/eliminarMET';
                $.post($url, { idDocumento: idDocumento },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idDocumento'+dato['idDocumento'])).html('');
                        swal("Eliminado!", "el Tipo de Documento ha sido eliminado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>