<form action="{$_Parametros.url}modCP/maestros/pagos/cuentasBancarias/cuentasCONTROL/nuevaCuentaMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idCuenta}" name="idCuenta"/>

    <div class="modal-body">

        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-head card-head-xs style-primary">
                        <header>Datos de la cuenta</header>
                    </div>
                    <div class="card-body" style="padding: 11px;">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_bancoError">
                    <select id="fk_a006_num_miscelaneo_detalle_banco" name="form[int][fk_a006_num_miscelaneo_detalle_banco]"
                            class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=banco from=$selectBANCOS}
                            {if isset($cuentasBD.fk_a006_num_miscelaneo_detalle_banco) and $cuentasBD.fk_a006_num_miscelaneo_detalle_banco == $banco.pk_num_miscelaneo_detalle}
                                <option value="{$banco.pk_num_miscelaneo_detalle}" selected >{$banco.ind_nombre_detalle}</option>
                            {else}

                                <option value="{$banco.pk_num_miscelaneo_detalle}">{$banco.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_a006_num_miscelaneo_detalle_banco">Banco:</label>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group floating-label" id="ind_num_cuentaError">
                    <input type="text" class="form-control" name="form[int][ind_num_cuenta]" id="ind_num_cuenta"  value="{if isset($cuentasBD.ind_num_cuenta)}{$cuentasBD.ind_num_cuenta}{/if}" {if $ver==1 } disabled{/if}>
                    <label for="ind_num_cuenta"><i class="md md-border-color"></i> Numero de Cuenta</label>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group floating-label" id="ind_descripcionError">
                    <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion"  value="{if isset($cuentasBD.ind_descripcion)}{$cuentasBD.ind_descripcion}{/if}" {if $ver==1 } disabled{/if}>
                    <label for="ind_descripcion"><i class="md md-border-color"></i> Descripcion</label>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-head card-head-xs style-primary">
                     <header>Caracteristica de la cuenta</header>
                    </div>
                    <div class="card-body" style="padding: 4px;">
                        <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_tipo_cuentaError">
                            <select id="fk_a006_num_miscelaneo_detalle_tipo_cuenta" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_cuenta]"
                                    class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                                <option value=""></option>
                                {foreach item=cuenta from=$selectTIPCUENTAB}
                                    {if isset($cuentasBD.fk_a006_num_miscelaneo_detalle_tipo_cuenta) and $cuentasBD.fk_a006_num_miscelaneo_detalle_tipo_cuenta == $cuenta.pk_num_miscelaneo_detalle}
                                        <option value="{$cuenta.pk_num_miscelaneo_detalle}" selected >{$cuenta.ind_nombre_detalle}</option>
                                    {else}
                                        <option value="{$cuenta.pk_num_miscelaneo_detalle}">{$cuenta.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle_tipo_cuenta">Tipo de Cuenta:</label>
                        </div>

                        <div class="form-group floating-label col-sm-12" id="fec_aperturaError">
                                <div class="input-group date" id="fec_apertura">
                                    <div class="input-group-content">
                                        <input type="text" id="fec_apertura" class="form-control" name="form[txt][fec_apertura]" value="{if isset($cuentasBD.fec_apertura)}{$cuentasBD.fec_apertura}{/if}" {if $ver==1 } disabled{/if}>
                                        <label>Fecha Apertura</label>
                                    </div>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="fk_cbb004_num_plan_cuentaError">
                                <input type="text" class="form-control"   value="{if isset($cuentasBD.fk_cbb004_num_plan_cuenta)}{$cuentasBD.cuenta}{/if}" id="indCuenta" disabled>
                                <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta]" id="fk_cbb004_num_plan_cuenta" value="{if isset($cuentasBD.fk_cbb004_num_plan_cuenta)}{$cuentasBD.fk_cbb004_num_plan_cuenta}{/if}">
                                <label for="fk_cbb004_num_plan_cuenta"><i class="md md-border-color"></i>Cuenta Contable</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group floating-label">
                                <button
                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                        type="button"
                                        data-toggle="modal" data-target="#formModal2"
                                        titulo="Listado de Plan Cuenta"
                                        url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuenta"
                                        {if $ver==1} disabled {/if}>
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="fk_cbb004_num_plan_cuenta_pub20Error">
                                <input type="text" class="form-control"   value="{if isset($cuentasBD.fk_cbb004_num_plan_cuenta_pub20)}{$cuentasBD.cuenta20}{/if}" id="indCuentaPub20" disabled>
                                <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta_pub20]" id="fk_cbb004_num_plan_cuenta_pub20" value="{if isset($cuentasBD.fk_cbb004_num_plan_cuenta_pub20)}{$cuentasBD.fk_cbb004_num_plan_cuenta_pub20}{/if}">
                                <label for="fk_cbb004_num_plan_cuenta_pub20"><i class="md md-border-color"></i>Cuenta Public. 20</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group floating-label">
                                <button
                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                        type="button"
                                        data-toggle="modal" data-target="#formModal2"
                                        titulo="Listado de Plan Cuenta"
                                        url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuentaPub20"
                                        {if $ver==1} disabled {/if}>
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-head card-head-xs style-primary">
                        <header>Información para Cartas de Transferencia de Fondo</header>
                    </div>
                    <div class="card-body" style="padding: 4px;">
                        <div class="col-sm-12">
                            <div class="form-group floating-label" id="ind_agenciaError">
                                <input type="text" class="form-control" name="form[alphaNum][ind_agencia]" id="ind_agencia"  value="{if isset($cuentasBD.ind_agencia)}{$cuentasBD.ind_agencia}{/if}" {if $ver==1 } disabled{/if}>
                                <label for="ind_agencia"><i class="md md-border-color"></i> Agencia</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group floating-label" id="ind_distritoError">
                                <input type="text" class="form-control" name="form[alphaNum][ind_distrito]" id="ind_distrito"  value="{if isset($cuentasBD.ind_distrito)}{$cuentasBD.ind_distrito}{/if}" {if $ver==1 } disabled{/if}>
                                <label for="ind_distrito"><i class="md md-border-color"></i> Distrito</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group floating-label" id="ind_atencionError">
                                <input type="text" class="form-control" name="form[alphaNum][ind_atencion]" id="ind_atencion"  value="{if isset($cuentasBD.ind_atencion)}{$cuentasBD.ind_atencion}{/if}" {if $ver==1 } disabled{/if}>
                                <label for="ind_atencion"><i class="md md-border-color"></i> Atencion</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group floating-label" id="ind_cargoError">
                                <input type="text" class="form-control" name="form[alphaNum][ind_cargo]" id="ind_cargo"  value="{if isset($cuentasBD.ind_cargo)}{$cuentasBD.ind_cargo}{/if}" {if $ver==1 } disabled{/if}>
                                <label for="ind_cargo"><i class="md md-border-color"></i> Cargo</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-head card-head-xs style-primary" align="center">
                        <header>Procesos Aplicables</header>
                    </div>
                    <div class="card-body" style="padding: 4px;">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="checkbox checkbox-styled">
                                    <label>
                                        <input type="checkbox" {if isset($cuentasBD.num_flag_conciliacion_bancaria) and $cuentasBD.num_flag_conciliacion_bancaria==1} checked{/if} value="1" name="form[int][num_flag_conciliacion_bancaria]" {if $ver==1 } disabled{/if}>
                                        <span>Conciliación Bancaria</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="col-lg-6">
                                    <div class="form-group floating-label" id="ind_comentariosError">
                                        <input type="text" class="form-control" name="form[alphaNum][ind_comentarios]" id="ind_comentarios"  value="{if isset($cuentasBD.ind_comentarios)}{$cuentasBD.ind_comentarios}{/if}" {if $ver==1 } disabled{/if}>
                                        <label for="ind_comentarios"><i class="md md-border-color"></i> Comentario</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="checkbox checkbox-styled">
                                    <label>
                                        <input type="checkbox" {if isset($cuentasBD.num_flag_conciliacion_cp) and $cuentasBD.num_flag_conciliacion_cp==1} checked{/if} value="1" name="form[int][num_flag_conciliacion_cp]" {if $ver==1 } disabled{/if}>
                                        <span> Conciliación Resumida en CxP</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="col-lg-6">
                                    <div class="form-group floating-label" id="num_saldo_inicialError">
                                        <input type="text" class="form-control" name="form[alphaNum][num_saldo_inicial]" id="num_saldo_inicial"  value="{if isset($cuentasBD.num_saldo_inicial)}{$cuentasBD.num_saldo_inicial|number_format:2:',':'.'}{/if}" {if $ver==1 } disabled{/if}>
                                        <label for="num_saldo_inicial"><i class="md md-border-color"></i>Saldo Inicial</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="checkbox checkbox-styled">
                                    <label>
                                        <input type="checkbox" {if isset($cuentasBD.num_flag_debito_bancario) and $cuentasBD.num_flag_debito_bancario==1} checked{/if} value="1" name="form[int][num_flag_debito_bancario]" {if $ver==1 } disabled{/if}>
                                        <span>ITF/Debito Bancario</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkbox checkbox-styled">
                                    <label>
                                        <input type="checkbox" {if isset($cuentasBD.num_estatus) and $cuentasBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                                        <span>Estatus</span>
                                    </label>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-12">
                           <div class="col-lg-6">
                                <div class="form-group floating-label" id="usuarioError">
                                    <input type="text" class="form-control disabled" value="{if isset($cuentasBD.ind_usuario)}{$cuentasBD.ind_usuario}{/if}" disabled>
                                    <label for="usuario"><i class="md md-border-color"></i> Ultimo Usuario</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group floating-label" id="fec_ultima_modificacionError">
                                    <input type="text" class="form-control disabled" value="{if isset($cuentasBD.fec_ultima_modificacion)}{$cuentasBD.fec_ultima_modificacion}{/if}" disabled>
                                    <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Fecha</label>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
            <div class="row">

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-head card-head-xs style-primary" align="center">
                            <header>Tipos de Pagos Disponibles</header>
                        </div>
                        <div class="card-body" style="padding: 4px;">
                            <table class="table">
                                <tbody id="imput_nuevo">
                                    {if isset($cuentasDetalle.fk_cpb014_num_cuenta_bancaria)}
                                    {$nuevo = 0}
                                        {foreach item=tPago from=$tipoPago}
                                            {$nuevo = $nuevo + 1}
                                            <tr id="nuevo{$nuevo}">
                                                <input type="hidden" value="{$nuevo}" name="form[formula][nuevo][secuencia][{$nuevo}]">
                                                <td>
                                                    {$var = $tPago.fk_a006_num_miscelaneo_detalle_tipo_pago}
                                                    <select class="form-control tipoPago" data-placeholder="Seleccione el Tipo de Pago" name="form[formula][nuevo][{$nuevo}][pk_tipo_pago]" idNum="{$nuevo}" {if $ver==1 } disabled{/if}>
                                                        {foreach item=tipoPago from=$selectdocPago}
                                                            {if $var == $tipoPago.pk_num_miscelaneo_detalle}
                                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}" selected>{$tipoPago.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}" >{$tipoPago.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </td>
                                                <td><input type=text size="15" name="form[formula][nuevo][{$nuevo}][int_numero_ultimo_cheque]" class="form-control"  readonly value="{$tPago.int_numero_ultimo_cheque}" id="int_numero_ultimo_cheque{$nuevo}"></td>
                                                <td style="text-align: center">
                                                    <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$nuevo}" {if $ver==1}disabled{/if}><i class="md md-delete"></i></button>
                                                </td>
                                            </tr>

                                            <div class="col-sm-3">

                                            </div>
                                        {/foreach}
                                    {/if}
                                </tbody>
                            </table>
                        </div>
                             <div class="col-sm-12" align="center">
                                <button id="nuevoCampo" class="btn btn-info ink-reaction btn-raised" type="button" {if $ver==1 } disabled{/if}>
                                   <i class="md md-add"></i> Agregar</button>
                             </div>
                        </div>
                    </div>
                </div>

            </div>

        <div class="modal-footer" >
            <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                    data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
            {if $ver!=1 }
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                {if $idCuenta!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
            </button>
            {/if}
        </div>
        </div>
                    </div>
                </div>
            </div>

    </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //valido formulario
        $('#formAjax').validate();
        //agregar nuevo campo
        $('#nuevoCampo').click(function () {
            var nuevo = $('#imput_nuevo tr').length + 1;
            $(document.getElementById('imput_nuevo')).append(
                    '<tr id="nuevo' + nuevo + '">' +
                    '<input type="hidden" value="'+nuevo+'" name="form[formula][nuevo][secuencia][]"> '+
                    '<td>' +
                    '<select class="form-control tipoPago" data-placeholder="Seleccione el Tipo de Pago" name="form[formula][nuevo]['+nuevo+'][pk_tipo_pago]" idNum="'+nuevo+'" ' +

                    '<option value="" >Seleccione el Tipo de Pago</option>' +
                    '{foreach item=tipoPago from=$selectdocPago}'+
                    '<option value="{$tipoPago.pk_num_miscelaneo_detalle}" >{$tipoPago.ind_nombre_detalle}</option>' +
                    '{/foreach}' +
                    '</select>' +
                    '</td>' +
                    '<td><input type=text size="15" name="form[formula][nuevo]['+nuevo+'][int_numero_ultimo_cheque]" class="form-control"  readonly id="int_numero_ultimo_cheque'+nuevo+'"></td>' +
                    '<td style="text-align: center">'+
                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+nuevo+'"><i class="md md-delete"></i></button>' +
                    '</td>'+
                    '</tr>'
            );
        });

        $('#imput_nuevo').on('click', '.delete', function () {
            var campo = $(this).attr('id');
            $('#nuevo' + campo).remove();
        });

        $('#imput_nuevo').on('change', '.tipoPago', function () {
            var num = $(this).attr('idNum');
            $.post('{$_Parametros.url}modCP/maestros/pagos/cuentasBancarias/cuentasCONTROL/buscarTipoPagoMET', {
                idTipoPago: $(this).val()
            },function (dato) {
                if (dato['tipoPago'] == 'CHEQUE') {
                    $('#int_numero_ultimo_cheque' + num).attr('readonly', false);
                }
                else {
                    $('#int_numero_ultimo_cheque' + num).attr('readonly', true);
                }
            }, 'json');

        });

        $("#formAjax").submit(function(){
            return false;
        });

        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        var app = new  AppFunciones();
        $('.select2').select2({ allowClear: true });
        $('#modalAncho').css( "width", "75%" );
        $('#fec_apertura').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#accion').click(function(){
           /* swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });*/
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ['num_flag_conciliacion_bancaria','num_flag_conciliacion_cp','num_flag_debito_bancario','num_estatus'];
                var arrayMostrarOrden = ['banco','ind_num_cuenta','ind_descripcion','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorCuenta'){
                    app.metValidarError(dato,'Disculpa. el Campo Numero de Cuenta solo debe contener valores numericos');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_cuenta'],'idCuenta',arrayCheck,arrayMostrarOrden,'La Cuenta Bancaria fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idCuenta'],'idCuenta',arrayCheck,arrayMostrarOrden,'La Cuenta Bancaria fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>
