<form action="{$_Parametros.url}modCP/maestros/pagos/chequera/chequeraCONTROL/accionesMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idChequera}" name="idChequera"/>
    <input type="hidden" value="{$idCuenta}" name="idCuenta"/>

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                    <select name="form[int][fk_a006_num_miscelaneo_detalle_banco]"
                            class="form-control select2-list select2" required
                            data-placeholder="Seleccione el Tipo de Banco"
                            {if $ver == 1}disabled{/if} id="BANCO">
                        <option value="">Seleccione el Banco</option>
                        {foreach item=banco from=$selectBANCOS}
                            {if isset($dataBD[0].pk_num_miscelaneo_detalle) and $dataBD[0].pk_num_miscelaneo_detalle == $banco.pk_num_miscelaneo_detalle}
                                <option value="{$banco.pk_num_miscelaneo_detalle}" selected >{$banco.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$banco.pk_num_miscelaneo_detalle}" >{$banco.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group"
                     id="fk_cpb014_num_cuenta_bancariaError" style="margin-top: -10px;">
                    <select name="form[int][fk_cpb014_num_cuenta_bancaria]" class="form-control select2"
                            data-placeholder="Seleccione Cuenta Bancaria"
                            {if $ver == 1}disabled{/if}
                            id="CUENTA">
                        {if !isset($dataBD[0].fk_cpb014_num_cuenta_bancaria)}
                            <option value="">Seleccione Cuenta Bancaria</option>
                        {else}
                            {foreach item=i from=$selectCuentas}
                                {if isset($dataBD[0].fk_cpb014_num_cuenta_bancaria) and $dataBD[0].fk_cpb014_num_cuenta_bancaria == $i.pk_num_cuenta}
                                    <option value="{$i.pk_num_cuenta}" >{$i.ind_num_cuenta}</option>
                                {/if}
                            {/foreach}
                        {/if}
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary" align="center">
                    <header>Correlativos de Chequera</header>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th scope="col" width="25">#</th>
                        <th scope="col">Nro. Desde </th>
                        <th scope="col">Nro. Hasta </th>
                        <th scope="col">Nro. Correlativo </th>
                        <th scope="col" width="15">Estatus</th>
                        <th scope="col" width="15"></th>
                    </tr>
                    </thead>
                    <tbody id="correlativo">
                        {if isset($dataBD[0].fk_cpb014_num_cuenta_bancaria)}
                            {$sec = 0}
                            {foreach item=i from=$dataBD}
                                {$sec = $sec + 1}
                            <tr id="chequera{$sec}">
                            <input type="hidden" value="{$sec}" name="form[int][chequera][num_secuencia][{$sec}]">
                            <input type="hidden" value="{$i.pk_num_chequera}" name="form[int][chequera][{$sec}][pk_num_chequera]">
                            <td>{$sec}</td>
                            <td width="120">
                                <input type="text" class="form-control" name="form[int][chequera][{$sec}][num_chequera_desde]" maxlength="4" id="num_chequera_desde" value="{$i.num_chequera_desde}" {if $ver == 1}disabled{/if}>
                            </td>
                            <td width="120">
                                <input type="text" class="form-control" name="form[int][chequera][{$sec}][num_chequera_hasta]" maxlength="4" id="num_chequera_hasta" value="{$i.num_chequera_hasta}" {if $ver == 1}disabled{/if}>
                            </td>
                            <td width="120">
                                <input type="text" class="form-control" name="form[int][chequera][{$sec}][ind_num_cheque]" id="ind_num_cheque" value="{$i.ind_num_cheque}" {if $ver == 1}disabled{/if}>
                            </td>
                            <td>
                                <div class="checkbox checkbox-styled" align="center" style="vertical-align: middle;">
                                    <label>
                                        <input type="checkbox" value="1" name="form[int][chequera][{$sec}][num_estatus]" {if $i.num_estatus == 1} checked {/if} {if $ver == 1}disabled{/if}><span></span>
                                        </label>
                                    </div>
                            </td>
                            <td width="35" style="text-align: center; color: #ffffff">
                                <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$sec}" {if $ver == 1}disabled{/if}><i class="md md-delete"></i></button>
                                </td>
                            </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                </table>
                <div class="col-sm-12" align="center">
                    <button id="agregar" class="btn btn-info ink-reaction btn-raised" type="button" {if $ver==1 } disabled{/if}>
                        <i class="md md-add "></i> Agregar</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled"
                           value="{if isset($dataBD.ind_usuario)}{$dataBD.ind_usuario}{/if}"
                           id="ind_usuario">
                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_ultima_modificacionError">
                    <input type="text" disabled class="form-control disabled"
                           value="{if isset($dataBD.fec_ultima_modificacion)}{$dataBD.fec_ultima_modificacion}{/if}"
                           id="fec_ultima_modificacion">
                    <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                </div>
            </div>
        </div>
    </div>


    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $ver!=1}
        <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
            {if $idChequera!=0}
                <i class="fa fa-edit"></i>&nbsp;Modificar
            {else}
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            {/if}
        </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        $('.select2').select2({ allowClear: true });
        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#modalAncho').css( "width", "45%" );
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ['num_estatus'];
                var arrayMostrarOrden = ['nombreBanco','ind_num_cuenta','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorChequera'){
                   swal('ERROR!','Disculpa. Debe agregar los datos de la chequera','error');
                }else if(dato['status']=='errorCorrelativo'){
                   swal('ERROR!','Disculpa. El Nro. Desde no puede ser mayor que el Nro. Hasta de la Chequera','error');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_tipo_documento_transaccion'],'idChequera',arrayCheck,arrayMostrarOrden,'El Tipo de Documento fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idChequera'],'idChequera',arrayCheck,arrayMostrarOrden,'El Tipo de Documento fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });

        $("#BANCO").change(function(){
            var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/cuentasBancariasMET/';
            var idBanco = $(this).val();
            $.post(url,{  idBanco: idBanco },
                    function (dato) {
                        if (dato) {
                            $('#CUENTA').html('');
                            var id = dato['id'];
                            for (var i = 0; i < id.length; i++) {
                                $('#CUENTA').append('<option value="' + id[i]['pk_num_cuenta'] + '">' + id[i]['ind_num_cuenta'] + '</option>');
                                $('#s2id_CUENTA .select2-chosen').html(id[0]['ind_num_cuenta']);
                            }
                        }
                    }, 'json');
        });
        //agregar nueva chequera a la cuenta bancaria seleccionada
        $('#agregar').click(function () {
            var sec=$('#correlativo tr').length + 1;

            $(document.getElementById('correlativo')).append(
                    '<tr id="chequera'+sec+'">'+
                    '<input type="hidden" value="'+sec+'" name="form[int][chequera][num_secuencia]['+sec+']"> '+
                    '<td>'+sec+'</td>'+
                    '<td width="120">' +
                        '<input type="text" class="form-control" name="form[int][chequera]['+sec+'][num_chequera_desde]" maxlength="4" id="num_chequera_desde" value="" '+
                    '</td>'+
                    '<td width="120">' +
                        '<input type="text" class="form-control" name="form[int][chequera]['+sec+'][num_chequera_hasta]" maxlength="4" id="num_chequera_hasta" value="" '+
                    '</td>'+
                    '<td width="120">' +
                        '<input type="text" class="form-control" name="form[int][chequera]['+sec+'][ind_num_cheque]" id="ind_num_cheque" value="" >'+
                    '</td>'+
                    '<td>' +
                    '<div class="checkbox checkbox-styled" align="center" style="vertical-align: middle;">' +
                    '<label>' +
                    '<input type="checkbox" value="1" name="form[int][chequera]['+sec+'][num_estatus]"><span></span>' +
                    '</label>'+
                    '</div>' +

                    '</td>'+
                    '<td width="35" style="text-align: center">'+
                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+sec+'"><i class="md md-delete"></i></button>' +
                    '</td>'+
                    '</tr>'
            );
        });
        $('#correlativo').on('click', '.delete', function () {
            var campo = $(this).attr('id');
            $('#chequera' + campo).remove();
        });

    });
</script>