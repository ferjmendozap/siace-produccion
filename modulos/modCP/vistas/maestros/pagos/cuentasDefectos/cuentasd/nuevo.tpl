<form action="{$_Parametros.url}modCP/maestros/pagos/cuentasDefectos/cuentasdCONTROL/nuevaCuentadMET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idCuentad}" name="idCuentad"/>

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_tipo_pagoError">
                <select id="fk_a006_num_miscelaneo_detalle_tipo_pago" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_pago]"
                        class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=tipoPago from=$listadoTipoPago}
                        {if isset($cuentasdBD.fk_a006_num_miscelaneo_detalle_tipo_pago) and $cuentasdBD.fk_a006_num_miscelaneo_detalle_tipo_pago == $tipoPago.pk_num_miscelaneo_detalle}
                            <option value="{$tipoPago.pk_num_miscelaneo_detalle}" selected >{$tipoPago.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$tipoPago.pk_num_miscelaneo_detalle}">{$tipoPago.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_tipo_pago">Tipo de Pago:</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="fk_cpb014_num_cuenta_bancariaError">
                <select id="fk_cpb014_num_cuenta_bancaria" name="form[int][fk_cpb014_pk_num_cuenta_bancaria]"
                        class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=proceso from=$listadoCuentas}
                        {if isset($cuentasdBD.fk_cpb014_num_cuenta_bancaria) and $cuentasdBD.fk_cpb014_num_cuenta_bancaria == $proceso.pk_num_cuenta }
                            <option value="{$proceso.pk_num_cuenta}" selected >{$proceso.ind_num_cuenta}</option>
                        {else}
                            <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_cpb014_num_cuenta_bancaria">Cuenta Bancaria:</label>
            </div>
        </div>
    </div>

    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $ver!=1 }
            <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
            {if $idCuentad!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
                    </button>
            {/if}
    </div>

</form>

<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        var app = new  AppFunciones();
        $('.select2').select2({ allowClear: true });
        $('#modalAncho').css( "width", "35%" );
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = [''];
                var arrayMostrarOrden = ['ind_nombre_detalle','ind_num_cuenta'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_cuenta_bancaria_defecto'],'idCuentad',arrayCheck,arrayMostrarOrden,'La Cuenta Bancaria por Defecto fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idCuentad'],'idCuentad',arrayCheck,arrayMostrarOrden,'La Cuenta Bancaria por Defecto fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>