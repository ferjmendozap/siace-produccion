<form action="{$_Parametros.url}modCP/maestros/obligaciones/tiposDocumentosCP/documentosCONTROL/nuevoDocumentoMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idDocumento}" name="idDocumento"/>

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="form-group floating-label" id="cod_tipo_documentoError">
                    {if isset($dataBD.cod_tipo_documento)}
                        <input type="text" class="form-control" name="form[alphaNum][cod_tipo_documento]" id="cod_tipo_documento"
                             value="{$dataBD.cod_tipo_documento}" disabled >
                    {else}
                        <input type="text" class="form-control" name="form[alphaNum][cod_tipo_documento]" id="cod_tipo_documento" {if $ver==1} disabled {/if}>
                    {/if}
                    <label for="cod_tipo_documento"><i class="md md-border-color"></i> Codigo</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="ind_descripcionError">
                <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion"
                       value="{if isset($dataBD.ind_descripcion)}{$dataBD.ind_descripcion}{/if}" {if $ver==1} disabled {/if}>
                <label for="ind_descripcion"><i class="md md-border-color"></i> Descripcion</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_regimen_fiscalError">
                    <select id="fk_a006_num_miscelaneo_detalle_regimen_fiscal" name="form[int][fk_a006_num_miscelaneo_detalle_regimen_fiscal]"
                            class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=proceso from=$listadoRegimen}
                            {if isset($dataBD.fk_a006_num_miscelaneo_detalle_regimen_fiscal) and $dataBD.fk_a006_num_miscelaneo_detalle_regimen_fiscal == $proceso.pk_num_miscelaneo_detalle }
                                <option value="{$proceso.pk_num_miscelaneo_detalle}" selected >{$proceso.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$proceso.pk_num_miscelaneo_detalle}">{$proceso.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_a006_num_miscelaneo_detalle_regimen_fiscal">Regimen Fiscal:</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_clasificacionError">
                    <select id="fk_a006_num_miscelaneo_detalle_clasificacion" name="form[int][fk_a006_num_miscelaneo_detalle_clasificacion]"
                            class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=proceso from=$selectCLASIFICAC}
                            {if isset($dataBD.fk_a006_num_miscelaneo_detalle_clasificacion) and $dataBD.fk_a006_num_miscelaneo_detalle_clasificacion == $proceso.pk_num_miscelaneo_detalle}
                                <option value="{$proceso.pk_num_miscelaneo_detalle}" selected >{$proceso.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$proceso.pk_num_miscelaneo_detalle}">{$proceso.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_a006_num_miscelaneo_detalle_clasificacion">Clasificacion:</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_cbc003_num_tipo_voucherError">
                    <select id="fk_cbc003_num_tipo_voucher" name="form[int][fk_cbc003_num_tipo_voucher]"
                            class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=proceso from=$listadoVoucher}
                            {if isset($dataBD.fk_cbc003_num_tipo_voucher) and $dataBD.fk_cbc003_num_tipo_voucher == $proceso.pk_num_voucher }
                                <option value="{$proceso.pk_num_voucher}" selected >{$proceso.ind_descripcion}</option>
                            {else}
                                <option value="{$proceso.pk_num_voucher}">{$proceso.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_cbb001_num_voucher"> Voucher (Provisión):</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_cbc003_num_tipo_voucher_ordPagoError">
                    <select id="fk_cbc003_num_tipo_voucher_ordPago" name="form[int][fk_cbc003_num_tipo_voucher_ordPago]"
                            class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=proceso from=$listadoVoucher}
                            {if isset($dataBD.fk_cbc003_num_tipo_voucher_ordPago) and $dataBD.fk_cbc003_num_tipo_voucher_ordPago == $proceso.pk_num_voucher }
                                <option value="{$proceso.pk_num_voucher}" selected >{$proceso.ind_descripcion}</option>
                            {else}
                                <option value="{$proceso.pk_num_voucher}">{$proceso.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_cbb001_num_voucher_ordPago"> Voucher (Ord. de Pago):</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_flag_provisionError" >
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($dataBD.num_flag_provision) and $dataBD.num_flag_provision==1} checked{/if} value="1" name="form[int][num_flag_provision]" {if $ver==1} disabled {/if}>
                            <span>Genera Voucher de Provisión </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-8">
                    <div class="form-group" id="fk_cbb004_num_plan_cuentaError">
                        <input type="text" class="form-control"   value="{if isset($dataBD.fk_cbb004_num_plan_cuenta)}{$cuenta.cuenta}{/if}" id="indCuenta" disabled>
                        <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta]" id="fk_cbb004_num_plan_cuenta" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta)}{$dataBD.fk_cbb004_num_plan_cuenta}{/if}">
                        <label  for="fk_cbb004_num_plan_cuenta"><i class="md md-border-color"></i>Cuenta Contable</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group floating-label">
                        <button
                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                type="button"
                                data-toggle="modal" data-target="#formModal2"
                                titulo="Listado de Plan Cuenta"
                                url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuenta"
                                {if $ver==1}disabled{/if}>
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_flag_adelantoError">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($dataBD.num_flag_adelanto) and $dataBD.num_flag_adelanto==1} checked{/if} value="1" name="form[int][num_flag_adelanto]" {if $ver==1} disabled {/if}>
                            <span>Se le considera Adelanto  </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-8">
                    <div class="form-group" id="fk_cbb004_num_plan_cuenta_adeError">
                        <input type="text" class="form-control"   value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_ade)}{$cuenta.cuentaAde}{/if}" id="indCuentaAde" disabled>
                        <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta_ade]" id="fk_cbb004_num_plan_cuenta_ade" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_ade)}{$dataBD.fk_cbb004_num_plan_cuenta_ade}{/if}">
                        <label for="fk_cbb004_num_plan_cuenta_ade"><i class="md md-border-color"></i>Cuenta Adelanto</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group floating-label">
                        <button
                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                type="button"
                                data-toggle="modal" data-target="#formModal2"
                                titulo="Listado de Plan Cuenta"
                                url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuentaAde"
                                {if $ver==1}disabled{/if}>
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_flag_fiscalError">

                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($dataBD.num_flag_fiscal) and $dataBD.num_flag_fiscal==1} checked{/if} value="1" name="form[int][num_flag_fiscal]" {if $ver==1} disabled {/if}>
                            <span>Fiscal</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-8">
                    <div class="form-group" id="fk_cbb004_num_plan_cuenta_pub20Error">
                        <input type="text" class="form-control"   value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_pub20)}{$cuenta.cuentaPub20}{/if}" id="indCuentaPub20" disabled>
                        <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta_pub20]" id="fk_cbb004_num_plan_cuenta_pub20" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_pub20)}{$dataBD.fk_cbb004_num_plan_cuenta_pub20}{/if}">
                        <label for="fk_cbb004_num_plan_cuenta_pub20"><i class="md md-border-color"></i>Cuenta Public. 20</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group floating-label">
                        <button
                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                type="button"
                                data-toggle="modal" data-target="#formModal2"
                                titulo="Listado de Plan Cuenta"
                                url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuentaPub20"
                                {if $ver==1}disabled{/if}>
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_flag_auto_nominaError">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($dataBD.num_flag_auto_nomina) and $dataBD.num_flag_auto_nomina==1} checked{/if} value="1" name="form[int][num_flag_auto_nomina]" {if $ver==1} disabled {/if}>
                            <span>Automático de Nómina </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-8">
                    <div class="form-group" id="fk_cbb004_num_plan_cuenta_ade_pub20Error">
                        <input type="text" class="form-control"   value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_ade_pub20)}{$cuenta.cuentaPub20Ade}{/if}" id="indCuentaAdePub20" disabled>
                        <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta_ade_pub20]" id="fk_cbb004_num_plan_cuenta_ade_pub20" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_ade_pub20)}{$dataBD.fk_cbb004_num_plan_cuenta_ade_pub20}{/if}">
                        <label for="fk_cbb004_num_plan_cuenta_ade_pub20"><i class="md md-border-color"></i>Cuenta Adelanto Pub. 20</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group floating-label">
                        <button
                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                type="button"
                                data-toggle="modal" data-target="#formModal2"
                                titulo="Listado de Plan Cuenta"
                                url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuentaAdePub20"
                                {if $ver==1}disabled{/if}>
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_estatusError">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($dataBD.num_estatus) and $dataBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1} disabled {/if}>
                            <span>Estatus </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group col-sm-6" id="cod_fiscalError">
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="form[alphaNum][cod_fiscal]" id="cod_fiscal" value="" disabled>
                        <label for="cod_fiscal"><i class="md md-border-color"></i>Cod.</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled"
                           value="{if isset($dataBD.ind_usuario)}{$dataBD.ind_usuario}{/if}"
                           id="ind_usuario">
                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_ultima_modificacionError">
                    <input type="text" disabled class="form-control disabled"
                           value="{if isset($dataBD.fec_ultima_modificacion)}{$dataBD.fec_ultima_modificacion}{/if}"
                           id="fec_ultima_modificacion">
                    <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                </div>
            </div>
        </div>
    </div>


    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $ver!=1}
        <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
            {if $idDocumento!=0}
                <i class="fa fa-edit"></i>&nbsp;Modificar
            {else}
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            {/if}
        </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        $('.select2').select2({ allowClear: true });
        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#modalAncho').css( "width", "60%" );
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ['num_flag_adelanto','num_flag_fiscal','num_flag_auto_nomina','num_flag_provision','num_estatus'];
                var arrayMostrarOrden = ['cod_tipo_documento','ind_descripcion','num_flag_adelanto','num_flag_fiscal','num_flag_auto_nomina','num_flag_provision','fk_cbb001_num_voucher','clasificacion','regimen','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_tipo_documento'],'idDocumento',arrayCheck,arrayMostrarOrden,'El Tipo de Documento fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idDocumento'],'idDocumento',arrayCheck,arrayMostrarOrden,'El Tipo de Documento fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });

    });
</script>