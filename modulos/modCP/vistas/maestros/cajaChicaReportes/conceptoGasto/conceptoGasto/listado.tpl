
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">CONCEPTO DE GASTOS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-hover">
                        <thead>
                        <tr>
                            <th>Concepto</th>
                            <th>Descripcion</th>
                            <th>Partida</th>
                            <th>Cta. Contable</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$grupo=0}
                        {foreach item=conceptoGasto from=$conceptoGastoBD}
                            {if ($grupo != $conceptoGasto.fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo)}
                                {$grupo = $conceptoGasto.fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo}
                                <tr style="font-weight:bold; background-color:#C7C7C7;">
                                    <td>{$conceptoGasto.cod_detalle}</td>
                                    <td>{$conceptoGasto.ind_nombre_detalle}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            {/if}
                            <tr id="idConceptoGasto{$conceptoGasto.pk_num_concepto_gasto}">
                                <td  align="center">{$conceptoGasto.cod_concepto_gasto}</td>
                                <td>{$conceptoGasto.ind_descripcion}</td>
                                <td>{$conceptoGasto.cod_partida}</td>
                                <td>{$conceptoGasto.cod_cuenta}</td>
                                <td><i class="{if $conceptoGasto.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                <td>
                                    {if in_array('CP-01-07-03-02-01-M',$_Parametros.perfil)}
                                    <button class="modificar ClasifGasto btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idConceptoGasto="{$conceptoGasto.pk_num_concepto_gasto}"  title="Editar"
                                            descipcion="El Usuario ha Modificado una Concepto de Gastos" titulo="<i class='fa fa-edit'></i> Editar Concepto de Gastos">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                    {if in_array('CP-01-07-03-02-02-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idConceptoGasto="{$conceptoGasto.pk_num_concepto_gasto} " title="Consultar"
                                                descipcion="El Usuario esta viendo un Concepto de Gastos" titulo="<i class='md md-remove-red-eye'></i> Consultar Concepto de Gastos">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                    {if in_array('CP-01-07-03-02-03-E',$_Parametros.perfil)}
                                        <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idConceptoGasto="{$conceptoGasto.pk_num_concepto_gasto}" title="Eliminar"  boton="si, Eliminar"
                                                descipcion="El usuario ha eliminado un Concepto de gasto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Concepto de Gastos!!">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('CP-01-07-03-02-04-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado un Concepto de Gasto"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="<i class='md md-create'></i>&nbsp;Registrar Nuevo Concepto de Gasto" id="nuevo"
                                        data-keyboard="false" data-backdrop="static"><i class="md md-create"></i>&nbsp;Nuevo Concepto de Gasto
                                </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/cajaChicaReportes/conceptoGasto/conceptoGastoCONTROL/nuevoConceptoGastoMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idConceptoGasto:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idConceptoGasto: $(this).attr('idConceptoGasto')} ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idConceptoGasto: $(this).attr('idConceptoGasto') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idConceptoGasto=$(this).attr('idConceptoGasto');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/cajaChicaReportes/conceptoGasto/conceptoGastoCONTROL/eliminarMET';
                $.post($url, { idConceptoGasto: idConceptoGasto },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idConceptoGasto'+dato['idConceptoGasto'])).html('');
                        swal("Eliminado!", "el Concepto de Gastos ha sido eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>