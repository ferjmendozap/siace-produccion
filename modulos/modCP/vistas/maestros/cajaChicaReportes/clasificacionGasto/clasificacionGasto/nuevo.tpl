<form action="{$_Parametros.url}modCP/maestros/cajaChicaReportes/clasificacionGasto/clasificacionGastoCONTROL/NuevaClasifGastoMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idClasifGasto}" name="idClasifGasto"/>

<div class="modal-body">

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group floating-label" id="cod_clasificacionError">
                        <input type="text" class="form-control" name="form[alphaNum][cod_clasificacion]" id="cod_clasificacion" {if isset($clasifGastoBD.cod_clasificacion)} value="{$clasifGastoBD.cod_clasificacion}" disabled {/if} >
                        <label for="cod_clasificacion"><i class="md md-border-color"></i> Codigo</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion"  value="{if isset($clasifGastoBD.ind_descripcion)}{$clasifGastoBD.ind_descripcion}{/if}" {if $ver==1 } disabled{/if}>
                        <label for="ind_descripcion"><i class="md md-border-color"></i> Descripcion</label>
                    </div>
                </div>
            </div>
            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_aplicacionError">
                <select id="fk_a006_num_miscelaneo_detalle_aplicacion" name="form[alphaNum][fk_a006_num_miscelaneo_detalle_aplicacion]"
                        class="form-control select2-list select2" {if $ver==1 } disabled{/if} >
                    <option value=""></option>
                    {foreach item=aplicacion from=$selectAPLICG}
                        {if isset($clasifGastoBD.fk_a006_num_miscelaneo_detalle_aplicacion) and $clasifGastoBD.fk_a006_num_miscelaneo_detalle_aplicacion == $aplicacion.pk_num_miscelaneo_detalle}
                            <option value="{$aplicacion.pk_num_miscelaneo_detalle}" selected >{$aplicacion.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$aplicacion.pk_num_miscelaneo_detalle}">{$aplicacion.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_aplicacion">Seleccione la Aplicacion</label>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($clasifGastoBD.num_estatus) and $clasifGastoBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                    <span>Estatus</span>
                </label>
        </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-head card-head-xs style-primary" align="center">
                        <header>Conceptos Validos</header>
                    </div>
                    <div class="card-body" style="padding: 4px;">
                        <table class="table">
                            <tbody id="imput_nuevo">
                            {if isset($clasifGastoDetalle.fk_cpb005_num_concepto_gasto)}
                                {$nuevo=0}
                                {foreach item=concep from=$concepto}
                                    {$nuevo = $nuevo+1}
                                    <tr id="nuevo{$nuevo}">
                                        <input type="hidden" value="{$nuevo}" name="form[int][nuevo][secuencia][{$nuevo}]">
                                        <td>
                                            {$var = $concep.fk_cpb005_num_concepto_gasto}
                                            <select class="form-control" data-placeholder="Seleccione el Concepto de Gasto" name="form[int][nuevo][{$nuevo}][pk_num_concepto_gasto]" {if $ver==1 } disabled{/if}>
                                                {foreach item=concepto from=$selectConceptoGasto}
                                                    {if $var == $concepto.pk_num_concepto_gasto}
                                                        <option value="{$concepto.pk_num_concepto_gasto}" selected>{$concepto.ind_descripcion}</option>
                                                    {else}
                                                        <option value="{$concepto.pk_num_concepto_gasto}" >{$concepto.ind_descripcion}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                        </td>
                                        <td style="text-align: center">
                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$nuevo}" {if $ver==1}disabled{/if}><i class="md md-delete"></i></button>
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                        <div class="col-sm-12" align="center">
                            <button id="nuevoCampo" class="btn btn-info ink-reaction btn-raised" type="button" {if $ver==1 } disabled{/if}>
                                <i class="md md-add "></i> Agregar</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 ">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled"
                                   value="{if isset($clasifGastoBD.ind_usuario	)}{$clasifGastoBD.ind_usuario	}{/if}"
                                   id="fk_a018_num_seguridad_usuario	">
                            <label for="fk_a018_num_seguridad_usuario	"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <div class="form-group floating-label" id="fec_ultima_modificacionError">
                            <input type="text" disabled class="form-control disabled"
                                   value="{if isset($clasifGastoBD.fec_ultima_modificacion)}{$clasifGastoBD.fec_ultima_modificacion}{/if}"
                                   id="fec_ultima_modificacion">
                            <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                        </div>
                    </div>
                </div>
            </div>
    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if !$ver==1 }
        <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
            {if $idClasifGasto!=0}
                <i class="fa fa-edit"></i>&nbsp;Modificar
            {else}
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            {/if}
        </button>
        {/if}
    </div>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        $('.select2').select2({ allowClear: true });
        $('#nuevoCampo').click(function () {
            var nuevo = $('#imput_nuevo tr').length + 1;
            $(document.getElementById('imput_nuevo')).append(
                    '<tr id="nuevo' + nuevo + '">' +
                    '<input type="hidden" value="'+nuevo+'" name="form[int][nuevo][secuencia]['+nuevo+']"> '+
                    '<td>' +
                    '<select class="form-control tipoPago" data-placeholder="Seleccione el Concepto de Gasto" name="form[int][nuevo]['+nuevo+'][pk_num_concepto_gasto]" idNum="'+nuevo+'" >' +
                    '{foreach item=clasif from=$selectConceptoGasto}'+
                    '<option value="{$clasif.pk_num_concepto_gasto}">{$clasif.ind_descripcion}</option>' +
                    '{/foreach}' +
                    '</select>' +
                    '</td>' +
                    '<td style="text-align: center">'+
                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+nuevo+'"><i class="md md-delete"></i></button>' +
                    '</td>'+
                    '</tr>'
            );
        });
        $('#imput_nuevo').on('click', '.delete', function () {
            var campo = $(this).attr('id');
            $('#nuevo' + campo).remove();
        });

        $('#modalAncho').css( "width", "40%" );
        var app = new  AppFunciones();

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ['num_estatus'];
                var arrayMostrarOrden = ['cod_clasificacion','ind_descripcion','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_clasificacion_gastos'],'idClasifGasto',arrayCheck,arrayMostrarOrden,'La Clasificacion de Gastos fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idClasifGasto'],'idClasifGasto',arrayCheck,arrayMostrarOrden,'La Clasificacion de Gastos fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>