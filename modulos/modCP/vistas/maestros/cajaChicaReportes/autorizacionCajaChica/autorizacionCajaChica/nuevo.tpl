<form action="{$_Parametros.url}modCP/maestros/cajaChicaReportes/autorizacionCajaChica/autorizacionCajaChicaCONTROL/nuevaAutorizacionMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idAutorizacionCC}" name="idAutorizacionCC"/>

    <div class="modal-body">
        <div class="form-group" id="empleadoError">
            <label for="empleado"><i class="md md-border-color"></i> Empleado:</label>
        </div>
        <div class="col-sm-3">
            <input type="text"
                   name="form[int][fk_rhb001_num_empleado]"
                   class="form-control"
                   id="fk_rhb001_num_empleado"
                   value="{if isset($autorizacionBD.fk_rhb001_num_empleado)}{$autorizacionBD.fk_rhb001_num_empleado}{/if}"
                   readonly size="9%">
        </div>
        <div class="col-sm-9">
            <div class="col-sm-10">
                <input type="text" class="form-control"
                       id="nombreEmpleado"
                       value="{if isset($autorizacionBD.fk_rhb001_num_empleado)}{$autorizacionBD.nombreEmpleado}{/if}"
                       readonly>
            </div>
            <div class="col-sm-2">
                <div class="form-group floating-label">
                    <button
                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal {if $ver==1 or $mod==1}disabled{/if}"
                            type="button"
                            data-toggle="modal"
                            data-target="#formModal2"
                            titulo="Listado de Empleados"
                            url="{$_Parametros.url}modCP/maestros/cajaChicaReportes/autorizacionCajaChica/autorizacionCajaChicaCONTROL/empleadoMET/empleado/">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
        </div>

            <div class="col-sm-6">
                <div class="form-group" id="num_monto_autorizadoError">
                    <input type="text" class="form-control" name="form[int][num_monto_autorizado]" id="num_monto_autorizado"
                           placeholder="0.000000"
                           value="{if isset($autorizacionBD.num_monto_autorizado)}{$autorizacionBD.num_monto_autorizado}{/if}"
                            {if $ver==1 } disabled{/if}>
                    <label for="num_monto_autorizado"><i class="md md-border-color"></i> Monto:</label>
                </div>
            </div>

        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($autorizacionBD.estatus) and $autorizacionBD.estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                    <span>Estatus</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5 ">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($autorizacionBD.ind_usuario)}{$autorizacionBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($autorizacionBD.fecha)}{$autorizacionBD.fecha}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer" >
            <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                    data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
            {if $ver!=1}
                <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
                    {if $idAutorizacionCC!=0}
                        <i class="fa fa-edit"></i>&nbsp;Modificar
                    {else}
                        <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                    {/if}
                </button>
            {/if}
        </div>
    </div>


<script type="text/javascript">
         $(document).ready(function() {
            var app = new  AppFunciones();
            $("#formAjax").submit(function(){
                return false;
            });
            $('.accionModal').click(function () {
                $('#formModalLabel2').html($(this).attr('titulo'));
                $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                    $('#ContenidoModal2').html($dato);
                });
            });
            $('#modalAncho').css( "width", "35%" );
            $('#accion').click(function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                    var arrayCheck = ['num_estatus'];
                    var arrayMostrarOrden = ['pk_num_empleado','nombreEmpleado','num_monto_autorizado','num_estatus'];
                    if(dato['status']=='error'){
                        app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                    }else if(dato['status']=='errorSQL'){
                        app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    }else if(dato['status']=='errorMonto'){
                        app.metValidarError(dato,'Disculpa. el Campo Monto solo debe contener valores numericos');
                    }else if(dato['status']=='modificar'){
                        app.metActualizarRegistroTabla(dato,dato['pk_num_cajachica_autorizacion'],'idAutorizacionCC',arrayCheck,arrayMostrarOrden,'La Autorizacion de Caja Chica fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                    }else if(dato['status']=='nuevo'){
                        app.metNuevoRegistroTabla(dato,dato['idAutorizacionCC'],'idAutorizacionCC',arrayCheck,arrayMostrarOrden,'La Autorizacion de Caja Chica fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                },'json');
            });
        });
</script>