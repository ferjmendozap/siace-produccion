<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">
            <div class="col-sm-11">OBLIGACIONES</div>
        </h2>
    </div>
    <input type="hidden" id="estadoListado" value="{$estado}">
    <div class="card">
        <div class="card-body">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="col-sm-2">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkProveedor">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="pk_num_tipo_documento"
                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text"
                               class="form-control" id="codigoProveedorLista"
                               value=""
                               readonly size="9%">
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-10">
                            <input type="text" class="form-control"
                                   id="nombreProveedorLista"
                                   value=""
                                   disabled>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group floating-label">
                                <button
                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                        type="button"
                                        data-toggle="modal"
                                        data-target="#formModal2"
                                        titulo="Listado de Personas"
                                        id="botonPersona"
                                        disabled="disabled"
                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/personaLista/">
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkIngresadoPor">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="ingresadoPor"
                               class="control-label" style="margin-top: 10px;"> Ingresado por:</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text"
                               class="form-control" id="codigoIngresadoPor"
                               value=""
                               readonly size="9%">
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-10">
                            <input type="text" class="form-control"
                                   id="nombreIngresadoPor"
                                   value=""
                                   disabled>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group floating-label">
                                <button
                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                        type="button"
                                        data-toggle="modal"
                                        data-target="#formModal2"
                                        titulo="Listado de Personas"
                                        id="botonPersonaIngresado"
                                        disabled="disabled"
                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/ingresadoPor/">
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="col-sm-4">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkDoc">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="cuenta"
                               class="control-label" style="margin-top: 10px;"> Tipo Doc.: </label>
                    </div>
                    <div class="col-sm-6">
                        <select class="form-control select2"
                                disabled="disabled"
                                id="doc"
                                data-placeholder="Seleccione Tipo de Documento">
                            <option value="">Seleccione...</option>
                            {foreach item=proceso from=$listadoDocumento}
                                <option value="{$proceso.pk_num_tipo_documento}">{$proceso.ind_descripcion}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="col-sm-2 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkNroDoc" >
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> Nro Doc.:</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control text-center"
                               id="nroDoc"
                               style="text-align: center"
                               value=""
                               disabled="disabled">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="col-sm-1 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkCosto">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="centroCosto"
                               class="control-label"  style="margin-top: 10px;">C. Costo:</label>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group" id="centroCostoError">
                            <input type="text" class="form-control" value="" id="centroCosto" readonly>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group floating-label">
                            <button
                                    type="button"
                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                    data-toggle="modal"
                                    data-target="#formModal2"
                                    id="bcentroCosto"
                                    titulo="Listado Centro de Costo"
                                    disabled="disabled"
                                    url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="col-sm-4">
                    <div class="col-sm-1 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkFechaDoc" checked="checked">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> F.Documento: </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="desde"
                               style="text-align: center"
                               value="{date('Y-m')}-01"
                               placeholder="Desde"
                               disabled="disabled"
                               readonly>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="hasta"
                               style="text-align: center"
                               value="{date('Y-m-d')}"
                               placeholder="Hasta"
                               disabled="disabled"
                               readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="col-sm-2 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkFechaReg">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> F.Registro: </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="desdeR"
                               style="text-align: center"
                               value=""
                               placeholder="Desde"
                               disabled="disabled"
                               readonly>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="hastaR"
                               style="text-align: center"
                               value=""
                               placeholder="Hasta"
                               disabled="disabled"
                               readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkEstado">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="cuenta"
                               class="control-label" style="margin-top: 10px;"> Estado:</label>
                    </div>
                    <div class="col-sm-6">
                        <select class="form-control select2"
                                id="estado"
                                disabled="disabled"
                                data-placeholder="Seleccione Tipo de Documento">
                            <option value="false">Seleccione...</option>
                            {foreach item=e from=$listadoEstado}
                                {if $estado!=''}
                                    {if $estado==$e.cod_detalle}
                                        <option value="{$e.cod_detalle}" selected>{$e.ind_nombre_detalle}</option>
                                    {/if}
                                {else}
                                    <option value="{$e.cod_detalle}">{$e.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>

            <div align="center">
                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                >BUSCAR
                </button>
            </div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>PROVEEDOR</th>
                                <th>TIPO</th>
                                <th>N° DOC.</th>
                                <th>FECHA DOC.</th>
                                <th>MONTO</th>
                                <th>PROCEDENCIA</th>
                                <th>ESTADO</th>
                                <td>ACCIÓN</td>
                            </tr>
                        </thead>
                        <!--tbody>
                            {foreach item=i from=$obligacionBD}
                                <tr id="idObligacion{$i.pk_num_obligacion}">
                                    <td>{$i.fk_a003_num_persona_proveedor}</td>
                                    <td width="50" class="text-center">{$i.cod_tipo_documento}</td>
                                    <td>{$i.ind_nro_factura}</td>
                                    <td width="90">{$i.fec_documento}</td>
                                    <td width="90" class="text-right">{$i.num_monto_obligacion|number_format:2:',':'.'}</td>
                                    <td width="90" class="text-center">{$i.ind_tipo_procedencia}</td>
                                    <td width="90" class="text-center">{$i.ind_estado}</td>
                                    <td width="100">
                                        {if $estado == 'PR' }

                                            {elseif $estado == 'RE' }
                                                {if in_array('CP-01-01-03-C',$_Parametros.perfil)}
                                                <button class="accion Obligacion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" id="RE" data-backdrop="static" idObligacion="{$i.pk_num_obligacion}" title="Conformar"
                                                        descipcion="El Usuario ha Conformar una Obligacion" titulo="<i class='icm icm-rating2'></i> Conformar Obligacion">
                                                    <i class="icm icm-rating2" style="color: #ffffff;"></i>
                                                </button>
                                                {/if}
                                            {elseif $estado == 'CO' }
                                                {if in_array('CP-01-01-04-AP',$_Parametros.perfil)}
                                                <button class="accion Obligacion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" id="CO" data-backdrop="static" idObligacion="{$i.pk_num_obligacion}" title="Aprobar"
                                                        descipcion="El Usuario ha Aprobado una Obligacion" titulo="<i class='icm icm-rating3'></i> Aprobar Obligacion">
                                                    <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                                </button>
                                                {/if}
                                            {elseif $estado == 'AP' }
                                            {else}
                                                {if $i.estado == 'PR'}
                                                    {if in_array('CP-01-01-06-M',$_Parametros.perfil)}
                                                    <button class="modificar Obligacion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static" id="modificar" idObligacion="{$i.pk_num_obligacion}" title="Editar"
                                                            descipcion="El Usuario ha Modificado una Obligacion" titulo="<i class='fa fa-edit'></i> Editar Obligacion">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                    {/if}
                                                    &nbsp;
                                                {/if}
                                        {/if}
                                        {if $i.estado == 'CO' || $i.estado == 'AP' || $i.estado == 'PA'}
                                        <button class="imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-success" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idObligacion="{$i.pk_num_obligacion}" title="Imprimir Voucher"
                                                descipcion="El Usuario esta Imprimiendo una Vouchers" titulo="<i class='md md-print'></i> Imprimir Voucher">
                                            <i class="md md-print" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}

                                    </td>
                                </tr>
                            {/foreach}
                        </tbody-->
                        <tfoot>
                        {if $estado !='PR' and $estado!='RE' and $estado!='CO'}
                        <tr>
                            <th colspan="9">
                                {if in_array('CP-01-01-05-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descipcion="el Usuario ha creado un post de Obligacion" data-toggle="modal"
                                                    data-target="#formModal" titulo="Registrar Nueva Obligacion" id="nuevo"
                                                    data-keyboard="false" data-backdrop="static">
                                                    <i class="md md-create"></i>&nbsp;Nueva Obligacion
                                </button>
                                {/if}
                            </th>
                        </tr>
                      {/if}
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {

        /// Complementos
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        var $url='{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/nuevaObligacionMET';
        var estado = 'false';
        var proveedor = 'false';
        var codigoIngresadoPor = 'false';
        var doc = 'false';
        var nroDoc = 'false';
        var centroCosto = 'false';
        var desde = 'false';
        var hasta = 'false';
        var desdeR = 'false';
        var hastaR = 'false';

        var url2 = "{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/jsonDataTablaMET";
        if($('#estado').val().length > 0) { estado = $('#estado').val(); }
        if($('#codigoProveedorLista').val().length > 0) { proveedor = $('#codigoProveedorLista').val(); }
        if($('#codigoIngresadoPor').val().length > 0) { codigoIngresadoPor= $('#codigoIngresadoPor').val(); }
        if($('#doc').val().length > 0) { doc= $('#doc').val(); }
        if($('#nroDoc').val().length > 0) { nroDoc= $('#nroDoc').val(); }
        if($('#centroCosto').val().length > 0) { centroCosto= $('#centroCosto').val(); }


        if($('#checkFechaDoc').attr('checked')=='checked'){
            $('#desde').attr('disabled',false);
            $('#hasta').attr('disabled',false);
            desde= $('#desde').val();
            hasta= $('#hasta').val();
        }
        if($('#checkFechaReg').attr('checked')=='checked'){
            desdeR= $('#desdeR').val();
            hastaR= $('#hastaR').val();
        }
        if($('#checkEstado').attr('checked')=='checked'){
            $('#estado').attr('disabled',false);
            estado = $('#estado').val();
        }
        if($('#estadoListado').val().length > 0) { estado = $('#estadoListado').val(); }
        var urlListado = url2
            +'/'+estado
            +'/'+proveedor
            +'/'+codigoIngresadoPor
            +'/'+doc
            +'/'+nroDoc
            +'/'+centroCosto
            +'/'+desde
            +'/'+hasta
            +'/'+desdeR
            +'/'+hastaR;
        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            urlListado,
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "pk_num_obligacion" },
                { "data": "fk_a003_num_persona_proveedor" },
                { "data": "cod_tipo_documento" },
                { "data": "ind_nro_factura" },
                { "data": "fec_documento" },
                { "data": "num_monto_obligacion" },
                { "data": "ind_tipo_procedencia" },
                { "data": "ind_estado" },
                { "orderable": false,"data": "acciones"}
            ]
        );

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idObligacion: $(this).attr('idObligacion'), estado: $(this).attr('id') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.accion', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idObligacion: $(this).attr('idObligacion'), estado: $(this).attr('id') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idObligacion: $(this).attr('idObligacion') , estado: $(this).attr('id'), ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.imprimir', function () {
            var idObligacion = $(this).attr('idObligacion');
            var url='{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/imprimirVoucherMET/?idObligacion='+idObligacion;
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{  },function(dato){

                $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="640px"></iframe>');

            });
        });

        //accionModal
        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('.buscar').click(function () {
            var estado = 'false';
            var proveedor = 'false';
            var codigoIngresadoPor = 'false';
            var doc = 'false';
            var nroDoc = 'false';
            var centroCosto = 'false';
            var desde = 'false';
            var hasta = 'false';
            var desdeR = 'false';
            var hastaR = 'false';

            var url = "{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/jsonDataTablaMET";

            if($('#codigoProveedorLista').val().length > 0) { proveedor = $('#codigoProveedorLista').val(); }
            if($('#codigoIngresadoPor').val().length > 0) { codigoIngresadoPor= $('#codigoIngresadoPor').val(); }
            if($('#doc').val().length > 0) { doc= $('#doc').val(); }
            if($('#nroDoc').val().length > 0) { nroDoc= $('#nroDoc').val(); }
            if($('#centroCosto').val().length > 0) { centroCosto= $('#centroCosto').val(); }


            if($('#checkFechaDoc').attr('checked')=='checked'){
                desde= $('#desde').val();
                hasta= $('#hasta').val();
            }
            if($('#checkFechaReg').attr('checked')=='checked'){
                desdeR= $('#desdeR').val();
                hastaR= $('#hastaR').val();
            }
            if($('#checkEstado').attr('checked')=='checked'){
                estado = $('#estado').val();
            }
            if($('#estadoListado').val().length > 0) { estado = $('#estadoListado').val(); }

            $('#dataTablaJson').DataTable().ajax.url(url
                +'/'+estado
                +'/'+proveedor
                +'/'+codigoIngresadoPor
                +'/'+doc
                +'/'+nroDoc
                +'/'+centroCosto
                +'/'+desde
                +'/'+hasta
                +'/'+desdeR
                +'/'+hastaR
            ).load();

        });
        $('#checkProveedor').click(function () {
            if(this.checked){
                $('#botonPersona').attr('disabled', false);
            }else{
                $('#botonPersona').attr('disabled', true);
                $(document.getElementById('codigoProveedorLista')).val("");
                $(document.getElementById('nombreProveedor1')).val("");

            }
        });
        $('#checkIngresadoPor').click(function () {
            if(this.checked){
                $('#botonPersonaIngresado').attr('disabled', false);
            }else{
                $('#botonPersonaIngresado').attr('disabled', true);
                $(document.getElementById('codigoIngresadoPor')).val("");
                $(document.getElementById('nombreIngresadoPor')).val("");

            }
        });
        $('#checkDoc').click(function () {
            if($('#doc').attr('disabled') =='disabled') {
                $('#doc').attr('disabled', false);
            }else{
                $('#doc').attr('disabled', true);
                $(document.getElementById('doc')).val("");
            }
        });
        $('#checkNroDoc').click(function () {
            if(this.checked){
                $('#nroDoc').attr('disabled', false);
            }else{
                $('#nroDoc').attr('disabled', true);
                $(document.getElementById('nroDoc')).val("");

            }
        });
        $('#checkFechaDoc').click(function () {
            if(this.checked){
                $('#desde').attr('disabled', false);
                $('#hasta').attr('disabled', false);
            }else{
                $('#desde').attr('disabled', true);
                $(document.getElementById('desde')).val("");
                $('#hasta').attr('disabled', true);
                $(document.getElementById('hasta')).val("");

            }
        });
        $('#checkFechaReg').click(function () {
            if(this.checked){
                $('#desdeR').attr('disabled', false);
                $('#hastaR').attr('disabled', false);
            }else{
                $('#desdeR').attr('disabled', true);
                $(document.getElementById('desdeR')).val("");
                $('#hastaR').attr('disabled', true);
                $(document.getElementById('hastaR')).val("");

            }
        });
        $('#checkCosto').click(function () {
            if(this.checked){
                $('#bcentroCosto').attr('disabled', false);
            }else{
                $('#bcentroCosto').attr('disabled', true);
                $(document.getElementById('centroCosto')).val("");

            }
        });

        $('#checkEstado').click(function () {
            if(this.checked){
                $('#estado').attr('disabled', false);
            }else{
                $('#estado').attr('disabled', true);
                $(document.getElementById('estado')).val("");
            }
        });

    });
</script>
