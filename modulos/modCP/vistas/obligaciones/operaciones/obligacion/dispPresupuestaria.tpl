<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">

                        <form action="{$_Parametros.url}modCP/pagos/ordenPagoCONTROL/accionesOrdenPagoMET" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idOrdenPago}" id="idOrdenPago" name="idOrdenPago"/>

                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#obligaciones" data-toggle="tab"><span class="step">1</span> <span class="title">DISPONIBILIDAD PRESUPUETSO</span></a></li>
                                    <li><a href="#cuentas" data-toggle="tab"><span class="step">2</span> <span class="title">DISTRIBUCION PRE-COMPROMISO</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="obligaciones">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="datatable1" class="table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th width="80">Partida</th>
                                                    <th align="left">Descripci&oacute;n</th>
                                                    <th width="100" align="right">Ajustado</th>
                                                    <th width="100" align="right">Comprometido</th>
                                                    <th width="100" align="right">Disponible</th>
                                                    <th width="100" align="right">Monto</th>
                                                    <th width="100" align="right">Resta</th>
                                                    <th width="100" align="right">Pre-Compromiso</th>
                                                    <th width="100" align="right">Resta</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="cuentas">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="table-responsive">
                                                        <table id="datatable1" class="table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th width="125">Documento</th>
                                                                <th align="left">Descripci&oacute;n</th>
                                                                <th width="100" align="right">Monto</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                                        <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Último</a></li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <table style="width:100%;">
        <tr>
            <td width="35"><div style="background-color:red; width:25px; height:20px;"></div></td>
            <td align="left">Sin disponibilidad presupuestaria</td>
            <td width="35"><div style="background-color:green; width:25px; height:20px;"></div></td>
            <td align="left">Disponibilidad presupuestaria</td>
            <td width="35"><div style="background-color:yellow; width:25px; height:20px;"></div></td>
            <td align="left">Sin disponibilidad presupuestaria (+ Pre-Compromiso)</td>
        </tr>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();
        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "90%");

        {if isset($estado)}
            $('.accionesEstado').click(function () {
                $.post(
                    $("#formAjax").attr("action"),
                    { idOrdenPago: $('#idOrdenPago').val(), estado: $(this).attr('id'), motivo: $('#ind_motivo_anulacion').val()},
                    function(dato){
                        if (dato['status'] == 'errorSQL') {
                            app.metValidarError(dato, 'Disculpa. Contacta Con la Direccion Tecnica porque hay un ERROR interno');
                        }else if (dato['status'] == 'noDispFinanciera') {
                            swal('Error','Disculpa. Se encontraron Obligaciones sin Disponibilidad Financiera en Banco para Generar el Pre-Pago', 'error');
                        }  else if (dato['status'] == 'OK') {
                            $(document.getElementById('idOrdenPago'+dato['idOrdenPago'])).remove();
                            swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        }
                    },'JSON');
            });
        {/if}







    });

</script>