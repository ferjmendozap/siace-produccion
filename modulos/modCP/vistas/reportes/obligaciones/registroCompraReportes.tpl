<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">REGISTRO DE COMPRA </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkProveedor">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               name="form[int][fk_a003_num_persona_proveedor]"
                                               class="form-control" id="codigoProveedor"
                                               value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.fk_a003_num_persona_proveedor}{/if}"
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.proveedor}{/if}"
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="col-sm-3 text-right">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Periodo:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center datePeriodo"
                                               id="periodo_desde"
                                               style="text-align: center"
                                               value="{date('Y-m')}"
                                               placeholder="Desde"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center datePeriodo"
                                               id="periodo_hasta"
                                               style="text-align: center"
                                               value="{date('Y-m')}"
                                               placeholder="Hasta"
                                               readonly>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="col-sm-3 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Ordenar por:</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group"
                                             id="pk_num_tipo_documentoError" style="margin-top: -10px;">
                                            <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"
                                                    id="CUENTA">
                                                <option value="">Seleccione Cuenta Bancaria</option>}
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div align="center">
                                <button class="buscarRetencion logsUsuario btn ink-reaction btn-raised btn-primary"
                                        id="buscar" idTipo="RegistroCompra">BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="{$_Parametros.url}modCP/reportes/obligacionesReportesCONTROL/" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#registroCompra" data-toggle="tab" id="RegistroCompra" class="buscarRetencion"><span class="step">1</span> <span class="title" style="font-weight:bold;">REGISTRO DE COMPRAS</span></a></li>
                                    <li><a href="#iva" data-toggle="tab" id="IVA" idTipo="IVA" class="buscarRetencion"><span class="step">2</span> <span class="title" style="font-weight:bold;">RETENCION I.V.A</span></a></li>
                                    <li><a href="#islr" data-toggle="tab" id="ISLR" idTipo="ISLR" class="buscarRetencion"><span class="step">3</span> <span class="title" style="font-weight:bold;">RETENCION I.S.L.R</span></a></li>
                                    <li><a href="#unopormil" data-toggle="tab" id="1X1000" idTipo="1X1000" class="buscarRetencion"><span class="step">4</span> <span class="title" style="font-weight:bold;">RETENCION 1X1000</span></a></li>
                                </ul>
                            </div>
                            <div id="rangoFecha" class="col-sm-12" style="background: #aeb3b9; display: none">
                                <div class="col-sm-7">
                                    <div class="col-sm-6">
                                        <div class="col-sm-2">
                                            <label for="desde"
                                                   class="control-label" style="font-weight:bold; margin-top: 10px;"> Desde:</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control text-center date"
                                                   id="desde"
                                                   value="{date("Y-m")}-01"
                                                   readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-2">
                                            <label for="hasta"
                                                   class="control-label" style="font-weight:bold; margin-top: 10px;"> Hasta:</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control text-center date"
                                                   id="hasta"
                                                   value="{date("Y-m-d")}"
                                                   readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="registroCompra">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="listadoRegistroCompra" style="height: 560px" >

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="iva">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body listado" id="listadoIva" style="height: 560px">

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="islr">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body listado" id="listadoISLR" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="unopormil">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body listado" id="listadoUnoxMil" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="ultio">Último</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Siguiente</a></li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           var app = new AppFunciones();
           app.metWizard();

           /// Complementos
           $('.datePeriodo').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es' });
           $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

           $('.accionModal').click(function () {
               $('#formModalLabel2').html($(this).attr('titulo'));
               $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                   $('#ContenidoModal2').html($dato);
               });
           });
           //busqueda segun el filtro
           $('.buscarRetencion').click(function () {
               var proveedor = $('#codigoProveedor').val();
               var desde = $('#periodo_desde').val();
               var hasta = $('#periodo_hasta').val();
               var tipo = $(this).attr('idTipo');
               $('#buscar').attr('idTipo', tipo);

               //
               if (tipo == 'IVA') {
                   $('#rangoFecha').css("display", "inline");
                   var diaDesde = $('#desde').val();
                   var diaHasta = $('#hasta').val();
               }else{
                   $('#rangoFecha').css("display", "none");
                   var diaDesde = '';
                   var diaHasta = '';
               }

               if(tipo == 'RegistroCompra'){
                   var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirRegistroCompraMET/?proveedor=' + proveedor + '&' + 'desde=' + desde + '&' + 'hasta=' + hasta + '&' + 'tipo=' + tipo;
                   $('#listadoRegistroCompra').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

               }else {
                   var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirRetencionMET/?proveedor=' + proveedor + '&' + 'desde=' + desde + '&' + 'hasta=' + hasta + '&' + 'tipo=' + tipo + '&' + 'diaDesde=' + diaDesde + '&' + 'diaHasta=' + diaHasta;
                   $('.listado').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');
               }
           });

           //habilitar y deshabilitar campos del filtro
           $('#checkProveedor').click(function () {
               if(this.checked){
                   $('#botonPersona').attr('disabled', false);
               }else{
                   $('#botonPersona').attr('disabled', true);
                   $(document.getElementById('codigoProveedor')).val("");
                   $(document.getElementById('nombreProveedor')).val("");

               }
           });


    });

</script>