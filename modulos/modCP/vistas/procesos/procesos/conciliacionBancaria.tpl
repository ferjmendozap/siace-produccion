<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">CONCILIACION BANCARIA</h2>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="col-sm-12">
                <div class="col-sm-5">
                    <div class="col-sm-3 text-right">
                        <label for="pk_num_tipo_documento"
                               class="control-label" style="margin-top: 10px;"> Banco:</label>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                            <select name="form[int][fk_a006_num_miscelaneo_detalle_banco]"
                                    class="form-control select2-list select2" required
                                    data-placeholder="Seleccione el Tipo de Banco"
                                    id="BANCO">
                                <option value="">Seleccione el Banco</option>
                                {foreach item=banco from=$selectBANCOS}
                                   <option value="{$banco.pk_num_miscelaneo_detalle}" >{$banco.ind_nombre_detalle}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="col-sm-2 text-right">
                        <label for="fSaldo"
                               class="control-label" style="margin-top: 10px;"> F. Saldo:</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control text-center date"
                               id="fSaldo"
                               style="text-align: center"
                               value=""
                               readonly>
                    </div>
                    <div class="col-sm-2 text-right">
                        <label for="fconc"
                               class="control-label" style="margin-top: 10px;"> F. Conciliación:</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control text-center date"
                               id="fconc"
                               style="text-align: center"
                               value=""
                               readonly>
                    </div>
                    <div class="col-sm-2 text-right">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> Periodo:</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control text-center"
                               id="periodo"
                               style="text-align: center"
                               value=""
                               readonly>
                    </div>

                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-5">
                    <div class="col-sm-3 text-right">
                        <label for="pk_num_tipo_documento"
                               class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group"
                             id="pk_num_tipo_documentoError" style="margin-top: -10px;">
                            <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"
                                    id="CUENTA">
                                <option value="">Seleccione Cuenta Bancaria</option>}
                                    <option value=""></option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="col-sm-4 text-right">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> Conciliados:</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                            <select class="form-control select2" data-placeholder="" >
                                <option value=""></option>
                                    <option value="">Si</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-right">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> Cobrados:</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                            <select class="form-control select2" data-placeholder="" >
                                <option value=""></option>
                                <option value="">Si</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div align="center">
                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                        id="buscar" idCuenta="{$proceso.pk_num_cuenta}" >BUSCAR
                </button>
            </div>
        </div>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th scope="col" width="60">NRO.</th>
                            <th scope="col" width="25">#</th>
                            <th scope="col" width="200">TRANSACCION</th>
                            <th scope="col" width="125">CARGO</th>
                            <th scope="col" width="125">ABONO</th>
                            <th scope="col" width="35">CONC</th>
                            <th scope="col" width="100">NRO. PAGO</th>
                            <th scope="col" width="100">DOC. REFERENCIA BANCO</th>
                            <th scope="col" width="75">FECHA BANCO</th>
                            <th scope="col">COMENTARIO</th>
                            <th scope="col" width="60">ESTADO</th>
                            <th scope="col" width="60">ACCIONES</th>
                        </tr>
                        </thead>
                        <tbody id="detalleTransaccion">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div></section>


<script type="text/javascript">

    $(document).ready(function() {

        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});
        $('#periodo').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es' });

        $('.buscar').click(function () {
            var idCuenta = $('#CUENTA').val();
            var fSaldo = $('#fSaldo').val();
            var fconc = $('#fconc').val();
            var periodo = $('#periodo').val();
            if(idCuenta){
                var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/buscarMET/';
                $.post(url, { idCuenta: idCuenta, fSaldo: fSaldo, fconc: fconc, periodo:periodo }, function (dato) {
                    if (dato) {
                        var actual = dato['actual'];
                        var anterior = dato['anterior'];
                        $("#datatable tbody").html('');
                        for (var i = 0; i < anterior.length; i++) {
                            $("#idTransaccion" + anterior[i]["pk_num_banco_transaccion"]).remove();
                            if (anterior[i]['ind_num_pago'] == null) {
                                var ind_num_pago = '';
                            }

                            $(document.getElementById('detalleTransaccion')).append(
                                '<tr id="idTransaccion' + anterior[i]["pk_num_banco_transaccion"] + '">' +
                                '<td><label>' + anterior[i]['ind_num_transaccion'] + '</label></td>' +
                                '<td><label>' + anterior[i]['num_secuencia'] + '</label></td>' +
                                '<td><label>' + anterior[i]['decripcionTransaccion'] + '</label></td>' +
                                '<td><label>' + anterior[i]['num_monto'] + '</label></td>' +
                                '<td>-</td>' +
                                '<td><label><i class="md md-check" id="conc' + anterior[i]["pk_num_banco_transaccion"] + '"></i></label></td>' +
                                '<td><label>' + anterior[i]['ind_num_pago'] + '</label></td>' +
                                '<td>-</td>' +
                                '<td><label>' + anterior[i]['fec_transaccion'] + '</label></td>' +
                                '<td><label>' + anterior[i]['txt_comentarios'] + '</label></td>' +
                                '<td><label>' + anterior[i]['ind_estado'] + '</label></td>' +
                                '<td>' +
                                /*'<button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"' +
                                 'data-keyboard="false" data-backdrop="static" id="VER" idTransaccion="' + anterior[i]["pk_num_banco_transaccion"] + '" title="VER"' +
                                 'descipcion="El Usuario a Modificado un miscelaneo" titulo="Ver Orden de Pago">' +
                                 '<i class="md md-remove-red-eye" style="color: #ffffff;"></i>' +
                                 '</button>' +*/
                                '&nbsp;' +
                                '<button class="actualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-info"' +
                                'data-keyboard="false" data-backdrop="static"' +
                                'id="ACTUALIZAR' + anterior[i]["pk_num_banco_transaccion"] + '" accion="ACTUALIZAR" idTransaccion="' + anterior[i]["pk_num_banco_transaccion"] + '" title="ACTUALIZAR"' +
                                'descipcion="El Usuario h actualizado una transaccion" titulo="Actualizar Transaccion">' +
                                '<i class="md md-refresh" style="color: #ffffff;"></i>' +
                                '</button>' +
                                '</td>' +
                                '</tr>'
                            );
                            if (anterior[i]['num_flag_conciliacion'] == '1') {
                                $('#ACTUALIZAR' + anterior[i]["pk_num_banco_transaccion"]).remove();
                                $('#conc' + anterior[i]["pk_num_banco_transaccion"]).attr('class', 'md md-check');
                            } else
                                $('#conc' + anterior[i]["pk_num_banco_transaccion"]).attr('class', 'md md-not-interested');
                        }
                        for (var i = 0; i < actual.length; i++) {
                            $("#idTransaccion" + actual[i]["pk_num_banco_transaccion"]).remove();
                            if (actual[i]['ind_num_pago'] == null) {
                                var ind_num_pago = '';
                            }

                            $(document.getElementById('detalleTransaccion')).append(
                                    '<tr id="idTransaccion' + actual[i]["pk_num_banco_transaccion"] + '">' +
                                    '<td><label>' + actual[i]['ind_num_transaccion'] + '</label></td>' +
                                    '<td><label>' + actual[i]['num_secuencia'] + '</label></td>' +
                                    '<td><label>' + actual[i]['decripcionTransaccion'] + '</label></td>' +
                                    '<td><label>' + actual[i]['num_monto'] + '</label></td>' +
                                    '<td>-</td>' +
                                    '<td><label><i class="md md-check" id="conc' + actual[i]["pk_num_banco_transaccion"] + '"></i></label></td>' +
                                    '<td><label>' + actual[i]['ind_num_pago'] + '</label></td>' +
                                    '<td>-</td>' +
                                    '<td><label>' + actual[i]['fec_transaccion'] + '</label></td>' +
                                    '<td><label>' + actual[i]['txt_comentarios'] + '</label></td>' +
                                    '<td><label>' + actual[i]['ind_estado'] + '</label></td>' +
                                    '<td>' +
                                    /*'<button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"' +
                                    'data-keyboard="false" data-backdrop="static" id="VER" idTransaccion="' + actual[i]["pk_num_banco_transaccion"] + '" title="VER"' +
                                    'descipcion="El Usuario a Modificado un miscelaneo" titulo="Ver Orden de Pago">' +
                                    '<i class="md md-remove-red-eye" style="color: #ffffff;"></i>' +
                                    '</button>' +*/
                                    '&nbsp;' +
                                    '<button class="actualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-info"' +
                                    'data-keyboard="false" data-backdrop="static"' +
                                    'id="ACTUALIZAR' + actual[i]["pk_num_banco_transaccion"] + '" accion="ACTUALIZAR" idTransaccion="' + actual[i]["pk_num_banco_transaccion"] + '" title="ACTUALIZAR"' +
                                    'descipcion="El Usuario h actualizado una transaccion" titulo="Actualizar Transaccion">' +
                                    '<i class="md md-refresh" style="color: #ffffff;"></i>' +
                                    '</button>' +
                                    '</td>' +
                                    '</tr>'
                            );
                            if (actual[i]['num_flag_conciliacion'] == '1') {
                                $('#ACTUALIZAR' + actual[i]["pk_num_banco_transaccion"]).remove();
                                $('#conc' + actual[i]["pk_num_banco_transaccion"]).attr('class', 'md md-check');
                            } else
                                $('#conc' + actual[i]["pk_num_banco_transaccion"]).attr('class', 'md md-not-interested');
                        }

                    }
                    if(dato==""){
                        $(document.getElementById('detalleTransaccion')).append(
                                '<tr id="idTransaccion">' +
                                '<td colspan="12" align="center"><label>No Hay Registros Disponibles</label></td>' +
                                '</tr>'
                        );
                    }
                }, 'json');
            }
        });

        $("#BANCO").change(function(){
            var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/cuentasBancariasMET/';
            var idBanco = $(this).val();
            $.post(url,{  idBanco: idBanco },
                function (dato) {
                    if (dato) {
                        $('#CUENTA').html('');
                        $('#CUENTA').append('<option value="">Seleccione...</option>');
                        var id = dato['id'];
                        for (var i = 0; i < id.length; i++) {
                            $('#CUENTA').append('<option value="' + id[i]['pk_num_cuenta'] + '" selected>' + id[i]['ind_num_cuenta'] + '</option>');
                        }
                    }
                }, 'json');
        });

        $('#datatable tbody').on( 'click', '.actualizar', function () {
            var idTransaccion = $(this).attr('idTransaccion');
            var estado = $(this).attr('accion');

            var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/accionesMET/';
            swal({
                title: 'Confirmar Cambio',
                text: 'Usted esta seguro(a)de realizar esta acción???',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Si, Confirmar',
                closeOnConfirm: false
            }, function(){
                $.post(url,{  idTransaccion: idTransaccion, estado: estado}, function(dato){
                    if (dato['status'] == 'errorSQL') {
                        swal("Error!", dato['mensaje'], "error");
                    } else if (dato['status'] == 'ok') {
                        swal("Cambio Realizado!", "El Cambio fue Realizado Satisfactoriamente.", "success");
                        $('#conc'+idTransaccion).removeAttr('class');
                        $('#conc'+idTransaccion).attr('class','md md-check');
                        $('#ACTUALIZAR'+idTransaccion).remove();
                    }
                }, 'json');

            });
        });
    });
</script>