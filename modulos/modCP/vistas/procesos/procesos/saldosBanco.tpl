<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">DIFERENCIAS DE SALDOS BANCO</h2>
    </div>
    <form action="{$_Parametros.url}modCP/procesos/procesosCONTROL/accionesMET/" id="formAjax"
          class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
        <input type="hidden" value="1" name="valido"/>
        <input type="hidden" value="saldoBanco" name="estado"/>
        <div class="card">
            <div class="card-body">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="col-sm-3 text-right">
                            <label for="BANCO"
                                   class="control-label" style="margin-top: 10px;"> Banco:</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="BANCOError" style="margin-top: -10px;">
                                <select class="form-control select2-list select2" required
                                        data-placeholder="Seleccione el Tipo de Banco"
                                        id="BANCO">
                                    <option value="">Seleccione el Banco</option>
                                    {foreach item=banco from=$selectBANCOS}
                                       <option value="{$banco.pk_num_miscelaneo_detalle}" >{$banco.ind_nombre_detalle}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-3 text-right">
                            <label for="fk_cpb014_num_cuenta_bancaria"
                                   class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group"
                                 id="fk_cpb014_num_cuenta_bancariaError" style="margin-top: -10px;">
                                <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"
                                        name="form[int][fk_cpb014_num_cuenta_bancaria]" id="CUENTA">
                                    <option value="">Seleccione Cuenta Bancaria</option>
                                    <option value=""></option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="col-sm-3 text-right">
                            <label for="num_estado_cuenta"
                                   class="control-label" style="margin-top: 10px;"> Estado de Cuenta:</label>
                        </div>
                        <div class="col-sm-4">
                            <input type="text"
                                   id="num_estado_cuenta"
                                   name="form[int][num_estado_cuenta]"
                                   class="form-control"
                                   value="">
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-3 text-right">
                            <label for="pago"
                                   class="control-label" style="margin-top: 10px;"> Fecha:</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control text-center date"
                                   id="desde"
                                   name="form[txt][fechaDesde]"
                                   style="text-align: center"
                                   value=""
                                   placeholder="Desde"
                                   readonly>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control text-center date"
                                   id="hasta"
                                   name="form[txt][fechaHasta]"
                                   style="text-align: center"
                                   value=""
                                   placeholder="Hasta"
                                   readonly>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <label for="pago"
                           class="control-label" style="margin-top: 10px;"> Segun Banco al {date('Y-m-d')}</label>
                </div>
            </div>
        </div>
        <div class="section-body contain-lg">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th style="text-align: center" width="40">FECHA</th>
                                <th style="text-align: center" width="60">NRO. DEPOSITO</th>
                                <th style="text-align: center" width="60">BOLIVARES</th>
                                <th style="text-align: center" width="10">ACCION</th>
                            </tr>
                            </thead>
                            <tbody id="diferenciaBanco">
                            <tr id="idTransaccion">
                                <td><input type="text" class="form-control text-center date" id="fec_fecha" style="text-align: center" value=""  name="form[txt][fec_fecha]"></td>
                                <td><input type="text" id="ind_descripcion" class="form-control" value="" name="form[alphaNum][ind_descripcion]"></td>
                                <td><input type="text" id="num_bolivares" class="form-control" value="" name="form[int][num_bolivares]"></td>
                                <td align="center">
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                            data-keyboard="false" data-backdrop="static" id="mas" title="MAS"
                                            descipcion="El Usuario esta procesando un cheque" titulo="<i class='md md-add'></i>">
                                        <i class="md md-add" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div align="center">
                        <button class="registrar logsUsuario btn ink-reaction btn-raised btn-primary" id="registrar" >Registrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

</section>


<script type="text/javascript">

    $(document).ready(function() {
        var app = new AppFunciones();
        /// Complementos
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        $("#BANCO").change(function(){
            var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/cuentasBancariasMET/';
            var idBanco = $(this).val();
            $.post(url,{  idBanco: idBanco },
                    function (dato) {
                        if (dato) {
                            $('#CUENTA').html('');
                            $('#CUENTA').append('<option value="">Seleccione...</option>');
                            var id = dato['id'];
                            for (var i = 0; i < id.length; i++) {
                                $('#CUENTA').append('<option value="' + id[i]['pk_num_cuenta'] + '" selected>' + id[i]['ind_num_cuenta'] + '</option>');
                            }
                        }
                    }, 'json');
        });

        $('#mas').click(function () {
            var sec=$('#diferenciaBanco tr').length + 1;
            $(document.getElementById('diferenciaBanco')).append(
                '<tr id="idTr'+sec+'">' +
                    '<td><input type="text" type="text" class="form-control text-center date" id="fec_fecha" style="text-align: center"value="" name="form[txt][transacciones][fec_fecha]"></td>'+
                    '<td><input type="text" id="ind_descripcion" class="form-control" value="" name="form[alphaNum][transacciones][ind_descripcion]"></td>'+
                    '<td><input type="text" id="num_bolivares" class="form-control" value="" name="form[int][transacciones][num_bolivares]"></td>'+
                    '<td align="center">' +
                        '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"' +
                        'data-keyboard="false" data-backdrop="static" id="'+sec+'" title="ELIMINAR"' +
                        'descipcion="El Usuario esta procesando un cheque" titulo="<i class="md md-add"></i>' +
                        '<i class="md md-delete" style="color: #ffffff;"></i>' +
                        '</button>' +
                    '</td>' +
                '</tr>'
            );

        });

        $('#diferenciaBanco').on('click', '.eliminar', function () {
            var campo = $(this).attr('id');
            $('#idTr' + campo).remove();
        });

        $('.registrar').click( function () {
            swal({
                title: 'Confirmar',
                text: '¿Esta seguro de Guardar Los Datos?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Si, Confirmar',
                closeOnConfirm: false
            }, function(){
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                    if (dato['status'] == 'errorSQL') {
                        swal("Error!", dato['mensaje'], "error");
                    } else if (dato['status'] == 'ok') {
                        var arrayCheck = '';
                        var arrayMostrarOrden = '';
                        app.metNuevoRegistroTabla(dato, dato['idSaldoBanco'], 'idSaldoBanco', arrayCheck, arrayMostrarOrden, 'La Diferencia de Saldo Banco fue guardada satisfactoriamente', 'cerrarModal', 'ContenidoModal');
                    }
                }, 'json');

            });
        });




    });
</script>