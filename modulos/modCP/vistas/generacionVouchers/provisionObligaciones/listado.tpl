<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">GENERACIÓN DE VOUCHERS DE PROVISIÓN DE OBLIGACIONES</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th># REGISTRO</th>
                            <th>PROVEEDOR / EMPLEADO</th>
                            <th>FECHA</th>
                            <th>DOCUMENTO</th>
                            <th>MONTO OBLIGACION</th>
                            <th>PREPARADO POR</th>
                            <td>ACCIÓN</td>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            {$debitos=0} {$creditos=0}
                            <tr id="idObligacion{$i.pk_num_obligacion}">
                                <td>{$i.ind_nro_registro}</td>
                                <td>{$i.nombreProveedor}</td>
                                <td>{$i.fec_aprobado}</td>
                                <td>{$i.cod_tipo_documento}-{$i.ind_nro_control}</td>
                                <td>{$i.num_monto_obligacion}</td>
                                <td>{$i.nombrePreparadoPor}</td>
                                <td>
                                    <button class="acciones btn ink-reaction btn-raised btn-xs btn-primary-light" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idObligacion="{$i.pk_num_obligacion}" idPago="{$i.pk_num_pago}" title="Generar Vouchers"
                                            descipcion="El Usuario ha GENERADO un voucher" titulo="<i class='icm icm-spinner12'></i> Generar Voucher de Provision">
                                        <i class="icm icm-spinner12" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div></section>


<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/generacionVouchers/provisionObligacionesCONTROL/generarVouchersMET';

        $('#datatable1 tbody').on( 'click', '.acciones', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idObligacion: $(this).attr('idObligacion'), idPago: $(this).attr('idPago')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

    });
</script>