<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
class bancosControlador extends Controlador
{
    use funcionesTrait;
    private $atBancosModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atBancosModelo = $this->metCargarModelo('reportes');
        $this->metObtenerLibreria('cabeceraCP', 'modCP');

    }

    public function metIndex($lista=false)
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoCuentas', $this->atBancosModelo->atCuentasModelo->metCuentaListar());
        $this->atVista->assign('selectBANCOS', $this->atBancosModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));
        $this->atVista->assign('selectTrans', $this->atBancosModelo->metTransaccionListar());

        if($lista=='libroBanco'){
            $this->atVista->metRenderizar('libroBancoReportes');
        }else{
            $this->atVista->metRenderizar('listadoTransaccionesReportes');
        }

    }
    public function metImprimirLibroBanco()
    {
        $this->atFPDF = new PDF('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 10);
        $this->atFPDF->AddPage();

        $idCuenta = $_GET['idCuenta'];
        $desde = $_GET['desde'];
        $hasta = $_GET['hasta'];

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(165, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(165, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(190, 5, utf8_decode('REPORTE DE BANCOS'), 0, 1, 'C', 0);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 30); $this->atFPDF->Cell(190, 5, "Desde $desde  Hasta $hasta", 0, 1, 'C', 0);
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 15, 17, 75, 15, 20, 20, 20));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'C', 'R', 'R', 'R'));
        $this->atFPDF->Row(array('Fecha',
            'Voucher',
            'Comprobante',
            'Concepto',
            utf8_decode('Transacción'),
            'Debe',
            'Haber',
            'Saldo'));
        $this->atFPDF->Ln(1);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $this->atFPDF->Rect(10, 270, 197, 0.1, 'DF');
//---------------------------------------------------
       $datosBanco = $this->atBancosModelo->metConsultaSaldoBanco($desde, $idCuenta);

        ##	imprimo cuenta bancaria
        foreach ($datosBanco as $i){
            $saldoIncialBanco = $this->atBancosModelo->metConsultaSaldoIncialBanco($i['pk_num_cuenta']);
            $saldoCuenta = $i['saldoCuenta'] + $saldoIncialBanco['num_saldo_inicial'];

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(35, 5, utf8_decode('CUENTA . '.$i['ind_num_cuenta']), 0, 0, 'L', 0);
            $this->atFPDF->Cell(147, 5, utf8_decode($i['ind_nombre_detalle']), 0, 0, 'L', 0);
            $this->atFPDF->Cell(16, 5, $saldoCuenta, 0, 1, 'R', 0);
            $saldo = $saldoCuenta;

            $datosTransacciones = $this->atBancosModelo->metConsultaTransaccionesBanco($i['pk_num_cuenta'], $desde, $hasta);
            $totalDebe = 0; $totalHaber = 0; 
            foreach ($datosTransacciones as $ii) {
                if ($ii['num_monto'] > 0) {
                    $debe = $ii['num_monto'];
                    $haber = 0;
                } else {
                    $debe = 0;
                    $haber = $ii['num_monto'];
                }
                $totalDebe = $totalDebe + $debe;
                $totalHaber = $totalHaber + $haber;
                $saldo = $saldo + $ii['num_monto'];
                $concepto = $ii['persona_proveedor'];

                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->Row(array(
                    $ii['fec_transaccion'],
                    $ii['ind_voucherTransaccion'].$ii['ind_voucherPago'],
                    $ii['ind_cheque_usuario'],
                    $concepto,
                    $ii['ind_num_transaccion'],
                    number_format($debe, 2, ',', '.'),
                    number_format($haber, 2, ',', '.'),
                    number_format($saldo, 2, ',', '.')
                ));
            }
            ##
            $this->atFPDF->Ln(1);
            ##
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(149, $y, 18, 0.1, 'DF');
            $this->atFPDF->Rect(169, $y, 18, 0.1, 'DF');
            $this->atFPDF->Rect(189, $y, 18, 0.1, 'DF');
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(137, 5, 'TOTAL. ', 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($totalDebe, 2, ',', '.'), 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($totalHaber, 2, ',', '.'), 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($saldo, 2, ',', '.'), 0, 0, 'R', 0);
            $this->atFPDF->Ln(8);
            $TotalDebe = 0;
            $TotalHaber = 0;
        }



////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirListadoTransacciones()
    {
        $idCuenta = $_GET['idCuenta'];
        $desde = $_GET['desde'];
        $hasta = $_GET['hasta'];

        $this->atFPDF = new PDF('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 10);
        $this->atFPDF->AddPage();

        ##
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(165, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(165, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(198, 5, utf8_decode('LISTADO DE TRANSACCIONES'), 0, 1, 'C', 0);
        $this->atFPDF->Ln(5);
        ##
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 10, 5, 55, 20, 20, 12, 12, 30, 20));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'R', 'C', 'C', 'C', 'L', 'C'));
        $this->atFPDF->Row(array('Fecha',
            'Nro.',
            '#',
            utf8_decode('Tipo de Transacción'),
            'Monto',
            'Cuenta Bancaria',
            'Periodo',
            'Voucher',
            'Doc. Ref. Banco',
            'Cheque'));
        $this->atFPDF->Ln(1);
        ##
        $this->atFPDF->SetWidths(array(15, 10, 5, 7, 48, 20, 20, 12, 12, 30, 20));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'L', 'R', 'C', 'C', 'C', 'L', 'C'));

        $datos = $this->atBancosModelo->metConsultaListadoTransacciones($idCuenta, $desde, $hasta);

        foreach ($datos as $i){
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->Row(array($i['fec_transaccion'],
                $i['ind_num_transaccion'],
                $i['num_secuencia'],
                $i['cod_tipo_transaccion'],
                utf8_decode($i['ind_descripcion']),
                number_format($i['num_monto'], 2, ',', '.'),
                $i['ind_num_cuenta'],
                $i['ind_periodo_contable'],
                $i['fk_cbb001_num_voucher_pago'],
                '',
                $i['ind_num_pago']
                ));
            $this->atFPDF->Ln(1);
        }
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $this->atFPDF->Rect(10, 270, 197, 0.1, 'DF');

        // Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

}
