<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
class transaccionesControlador extends Controlador
{
    use funcionesTrait;
    private $atTransaccionesModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atTransaccionesModelo = $this->metCargarModelo('generacionVouchers');

    }

    public function metIndex()
    {
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementosJs = array(
            'materialSiace/core/demo/DemoTableDynamic',
        );
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($complementosJs);

        $this->atVista->assign('dataBD', $this->atTransaccionesModelo->metListarTransacciones());
        $this->atVista->metRenderizar('listado');

    }

    public function metGenerarVouchers()
    {
        $idTransaccion = $this->metObtenerInt('idTransaccion');
        //Enviar a la vista el Centro de Costo por defecto para los vouchers
        $this->atVista->assign('centroCosto', $this->atTransaccionesModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER')));
        //Enviar a la vista el organismo
        $this->atVista->assign('organismo', $this->atTransaccionesModelo->atObligacionModelo->metOrganismo());

        $this->atVista->assign('libroContable', $this->atTransaccionesModelo->atLibroContableModelo->metListar());
        $this->atVista->assign('voucher', $this->atTransaccionesModelo->atVoucherModelo->metListar());

        $transacion = $this->atTransaccionesModelo->metBuscarTransacciones($idTransaccion);
        $codVoucher = $transacion['fk_cbc003_tipo_voucher'];
        if(Session::metObtener('CONTABILIDAD') == 'ONCOP') {
            $contab = 'T';
        }else{
            $contab = 'F';
        }
        $contabilidades = $this->atTransaccionesModelo->atObligacionModelo->metBuscar('cb_b005_contabilidades', 'ind_tipo_contabilidad', $contab);

        $datos =  $this->atTransaccionesModelo->metArmarVocherTransacciones($idTransaccion,$transacion['cod_detalle']);
        $debitos=0; $creditos=0;
        foreach ($datos as $i) {
            if($i['MontoVoucher'] < 0){
                $i['MontoVoucher'] = $i['MontoVoucher'] * -1;
            }
            if($i['columna']=='Debe'){

                $debitos = $debitos+$i['MontoVoucher'];

                $datoArreglo['Debe'][]=array(
                    'pk_num_cuenta' => $i['pk_num_cuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'ind_descripcion' => $i['ind_descripcion'],
                    'MontoVoucher' => $i['MontoVoucher']
                );
            }else{
                $creditos = $creditos+$i['MontoVoucher'];
                $datoArreglo['Haber'][]=array(
                    'pk_num_cuenta' => $i['pk_num_cuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'ind_descripcion' => $i['ind_descripcion'],
                    'MontoVoucher' => $i['MontoVoucher']
                );
            }
        }
        if(!isset($datoArreglo)){
            $datoArreglo=false;
        }
        $this->atVista->assign('datoArreglo',$datoArreglo);
        $this->atVista->assign('creditos', $creditos);
        $this->atVista->assign('debitos', $debitos);
        $this->atVista->assign('transacion', $transacion);
        $this->atVista->assign('codVoucher', $codVoucher);
        $this->atVista->assign('idTransaccion', $idTransaccion);
        $this->atVista->assign('contabilidad', $contabilidades['pk_num_contabilidades']);
        $this->atVista->metRenderizar('vouchersTransaccion', 'modales');
    }


}