<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba   | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
trait dataCxP

{
    #maestros relacionados a obligaciones
    public function metDataImpuestos()
    {
        $cp_b015_impuesto = array(
            array('cod_impuesto' => 'I01', 'txt_descripcion' => 'IVA GENERAL 12%', 'cod_detalle_Regimen' => 'I', 'ind_signo' => '1', 'plan_cuenta' => '6131701', 'plan_cuenta_pub20' => '61300031801', 'num_factor_porcentaje' => '12.00', 'cod_detalle_Provision' => 'N', 'cod_detalle_Imponible' => 'N', 'cod_detalle_Comprob' => 'IVA', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_impuesto' => 'I02', 'txt_descripcion' => 'IVA GENERAL 8%', 'cod_detalle_Regimen' => 'I', 'ind_signo' => '1', 'plan_cuenta' => '6131701', 'plan_cuenta_pub20' => '61300031801', 'num_factor_porcentaje' => '8.00', 'cod_detalle_Provision' => 'N', 'cod_detalle_Imponible' => 'N', 'cod_detalle_Comprob' => 'IVA', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_impuesto' => 'R01', 'txt_descripcion' => 'IVA RETENCION 75%', 'cod_detalle_Regimen' => 'R', 'ind_signo' => '2', 'plan_cuenta' => '214990102', 'plan_cuenta_pub20' => '12133010102', 'num_factor_porcentaje' => '75.00', 'cod_detalle_Provision' => 'P', 'cod_detalle_Imponible' => 'I', 'cod_detalle_Comprob' => 'IVA', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_impuesto' => 'R02', 'txt_descripcion' => 'ISLR SERVICIO 2%', 'cod_detalle_Regimen' => 'R', 'ind_signo' => '2', 'plan_cuenta' => '214990101', 'plan_cuenta_pub20' => '61300031801', 'num_factor_porcentaje' => '2.00', 'cod_detalle_Provision' => 'P', 'cod_detalle_Imponible' => 'N', 'cod_detalle_Comprob' => 'ISLR', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_impuesto' => 'R03', 'txt_descripcion' => 'ISLR SERVICIO 3%', 'cod_detalle_Regimen' => 'R', 'ind_signo' => '2', 'plan_cuenta' => '214990101', 'plan_cuenta_pub20' => '61300031801', 'num_factor_porcentaje' => '3.00', 'cod_detalle_Provision' => 'P', 'cod_detalle_Imponible' => 'N', 'cod_detalle_Comprob' => 'ISLR', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_impuesto' => 'R04', 'txt_descripcion' => 'ISLR SERVICIO 5%', 'cod_detalle_Regimen' => 'R', 'ind_signo' => '2', 'plan_cuenta' => '214990101', 'plan_cuenta_pub20' => '61300031801', 'num_factor_porcentaje' => '5.00', 'cod_detalle_Provision' => 'P', 'cod_detalle_Imponible' => 'N', 'cod_detalle_Comprob' => 'ISLR', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_impuesto' => 'R05', 'txt_descripcion' => 'UNO POR MIL (1x1000)', 'cod_detalle_Regimen' => 'R', 'ind_signo' => '2', 'plan_cuenta' => '214990103', 'plan_cuenta_pub20' => '12133010103', 'num_factor_porcentaje' => '0.10', 'cod_detalle_Provision' => 'P', 'cod_detalle_Imponible' => 'N', 'cod_detalle_Comprob' => '1X1000', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_impuesto' => 'R06', 'txt_descripcion' => 'IVA RETENCION 100%', 'cod_detalle_Regimen' => 'R', 'ind_signo' => '2', 'plan_cuenta' => '214990102', 'plan_cuenta_pub20' => '12133010102', 'num_factor_porcentaje' => '100.00', 'cod_detalle_Provision' => 'P', 'cod_detalle_Imponible' => 'I', 'cod_detalle_Comprob' => 'IVA', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1',),
            array('cod_impuesto' => 'D01', 'txt_descripcion' => 'DESCUENTO 3%', 'cod_detalle_Regimen' => 'N', 'ind_signo' => '2', 'plan_cuenta' => '61308', 'plan_cuenta_pub20' => '111020201', 'num_factor_porcentaje' => '3.00', 'cod_detalle_Provision' => 'N', 'cod_detalle_Imponible' => 'N', 'cod_detalle_Comprob' => 'DESC', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1',),
            array('cod_impuesto' => 'D02', 'txt_descripcion' => 'DESCUENTO 5%', 'cod_detalle_Regimen' => 'N', 'ind_signo' => '2', 'plan_cuenta' => '61308', 'plan_cuenta_pub20' => '111020201', 'num_factor_porcentaje' => '5.00', 'cod_detalle_Provision' => 'N', 'cod_detalle_Imponible' => 'N', 'cod_detalle_Comprob' => 'DESC', 'cod_detalle_General' => 'G', 'fk_a018_num_seguridad_usuario' => '1',)
        );

        return $cp_b015_impuesto;
    }

    public function metDataServicios()
    {
        $cp_b017_tipo_servicio = array(
            array('cod_tipo_servicio' => 'IVA', 'ind_descripcion' => 'IVA GENERAL 12%', 'cod_detalle' => 'I',
                'Det' => array(
                    array('impuesto' => 'I01')
                )
            ),
            array('cod_tipo_servicio' => 'IVA8', 'ind_descripcion' => 'IVA GENERAL 8%', 'cod_detalle' => 'I',
                'Det' => array(
                    array('impuesto' => 'I02')
                )
            ),
            array('cod_tipo_servicio' => 'IVAR1', 'ind_descripcion' => 'IVA 12% RETIVA 75%', 'cod_detalle' => 'M',
                'Det' => array(
                    array('impuesto' => 'I01'),
                    array('impuesto' => 'R01')
                )
            ),
            array('cod_tipo_servicio' => 'IVAR2', 'ind_descripcion' => 'IVA 12% ISLR 2%', 'cod_detalle' => 'M',
                'Det' => array(
                    array('impuesto' => 'I01'),
                    array('impuesto' => 'R02')
                )
            ),
            array('cod_tipo_servicio' => 'IVAR3', 'ind_descripcion' => 'IVA 12% ISLR 3%', 'cod_detalle' => 'M',
                'Det' => array(
                    array('impuesto' => 'I01'),
                    array('impuesto' => 'R03')
                )
            ),
            array('cod_tipo_servicio' => 'IVAR4', 'ind_descripcion' => 'IVA 12% ISLR 5%', 'cod_detalle' => 'M',
                'Det' => array(
                    array('impuesto' => 'I01'),
                    array('impuesto' => 'R04')
                )
            ),
            array('cod_tipo_servicio' => 'IVAR5', 'ind_descripcion' => 'SIN IVA ISLR 2%', 'cod_detalle' => 'M',
                'Det' => array(
                    array('impuesto' => 'R02')
                )
            ),
            array('cod_tipo_servicio' => 'IVAR6', 'ind_descripcion' => 'IVA 12% Uno por Mil (1x10', 'cod_detalle' => 'M',
                'Det' => array(
                    array('impuesto' => 'I01'),
                    array('impuesto' => 'R05')
                )
            ),
            array('cod_tipo_servicio' => 'NING', 'ind_descripcion' => 'NINGUNO', 'cod_detalle' => 'N'),

            array('cod_tipo_servicio' => 'NOAFE', 'ind_descripcion' => 'No Afecto', 'cod_detalle' => 'M'),

            array('cod_tipo_servicio' => 'SOIVA', 'ind_descripcion' => 'SOLO IVA', 'cod_detalle' => 'M',
                'Det' => array(
                    array('impuesto' => 'I01')
                )
            ),
            array('cod_tipo_servicio' => 'IVAR7', 'ind_descripcion' => 'IVA 12% RETIVA 100%', 'cod_detalle' => 'M',
                'Det' => array(
                    array('impuesto' => 'I01'),
                    array('impuesto' => 'R06')
                )
            ),
        );
        return $cp_b017_tipo_servicio;
    }

    public function metDataDocumetos()
    {
        $cp_b002_tipo_documento = array(
            array('cod_tipo_documento' => 'AB','ind_descripcion' => 'Abonos','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'AE','ind_descripcion' => 'Aguinaldos Empleados','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010201','plan_cuenta_pub20' => '12103010301','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'AF','ind_descripcion' => 'Aguinaldos Altos Funcionarios','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010204','plan_cuenta_pub20' => '12103010313','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'AO','ind_descripcion' => 'Aguinaldos Obreros','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010202','plan_cuenta_pub20' => '12103010304','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'AP','ind_descripcion' => 'Adelanto Proveedor','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '1','plan_cuenta_ade' => '2110401','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'AV','ind_descripcion' => 'Otros','cod_detalle_clasif' => 'C','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '19','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '121030201','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'BB','ind_descripcion' => 'Pago de Bonos Obreros','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010302','plan_cuenta_pub20' => '12103010402','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'BE','ind_descripcion' => 'Pago de Bonos Empleados','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010301','plan_cuenta_pub20' => '12103010401','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'BF','ind_descripcion' => 'Pago de Bonos Altos Funcionarios','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010304','plan_cuenta_pub20' => '12103010405','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'BJ','ind_descripcion' => 'Pago de Bono Jubilados','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010199','plan_cuenta_pub20' => '1210307010116','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'BO','ind_descripcion' => 'Boleta de Deposito','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'BP','ind_descripcion' => 'Pago de Bono Pensionado','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110199','plan_cuenta_pub20' => '1210307010112','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'CA','ind_descripcion' => 'Cargos','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '15','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'CB','ind_descripcion' => 'Comisiones Bancarias','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '39','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110499','plan_cuenta_pub20' => '121030204','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'CC','ind_descripcion' => 'Caja Chica','cod_detalle_clasif' => 'C','cod_detalle_regimen' => 'M','fk_cbc003_num_tipo_voucher' => '16','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '111010101','plan_cuenta_pub20' => '111020101','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'CD','ind_descripcion' => 'Cheque Devuelto','cod_detalle_clasif' => 'C','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '15','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'CH','ind_descripcion' => 'Cheque','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'CN','ind_descripcion' => 'Nomina Contratados','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010103','plan_cuenta_pub20' => '12103010103','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'EL','ind_descripcion' => 'Electricidad','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '34','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '1','plan_cuenta_ade' => '2110401','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'ER','ind_descripcion' => 'Entrega a Rendir','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '1140199','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'FG','ind_descripcion' => 'Factura Caja Chica','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'I','fk_cbc003_num_tipo_voucher' => '16','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '121030201','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '1','cod_fiscal' => '01','num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'FP','ind_descripcion' => 'Factura por Pagar','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'M','fk_cbc003_num_tipo_voucher' => '01','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '121030201','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '1','cod_fiscal' => '01','num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'JA','ind_descripcion' => 'Aguinaldos Personal Jubilados','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211019999','plan_cuenta_pub20' => '12103019999','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'JN','ind_descripcion' => 'Nomina Jubilados','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211019999','plan_cuenta_pub20' => '1210307010102','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'NC','ind_descripcion' => 'Nota de Credito','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'M','fk_cbc003_num_tipo_voucher' => '04','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '121030201','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '1','cod_fiscal' => '03','num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'ND','ind_descripcion' => 'Nota de Debito','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'M','fk_cbc003_num_tipo_voucher' => '05','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '121030201','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '1','cod_fiscal' => '02','num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'NE','ind_descripcion' => 'Nomina Empleados','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010101','plan_cuenta_pub20' => '12103010101','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'NF','ind_descripcion' => 'Nomina Altos Funcionarios','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010105','plan_cuenta_pub20' => '12103010108','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'NJ','ind_descripcion' => 'Ajustes de Prov. Neg','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '19','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '121030201','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'NO','ind_descripcion' => 'Nomina Obreros','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010102','plan_cuenta_pub20' => '12103010102','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'NP','ind_descripcion' => 'Ajustes de Prov. Pos','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '19','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '121030201','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'PA','ind_descripcion' => 'Aguinaldos Personal Pensionado','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211019999','plan_cuenta_pub20' => '12103019999','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'PE','ind_descripcion' => 'Prestaciones Sociales Empleados','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010401','plan_cuenta_pub20' => '12103010501','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'PF','ind_descripcion' => 'Pago Aporte Paro Forzoso','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '12103010604','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'PG','ind_descripcion' => 'Prestaciones de Antiguedad','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110403','plan_cuenta_pub20' => '12103110105','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'PH','ind_descripcion' => 'Pago Aporte FAOV','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '12103010605','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'PJ','ind_descripcion' => 'Pago Aportes Fondo Jubilación y Pensiones','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110203','plan_cuenta_pub20' => '12103010603','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'PL','ind_descripcion' => 'Transferencia de Nomina','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '39','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'PN','ind_descripcion' => 'Nomina Pensionados','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211019999','plan_cuenta_pub20' => '1210307010101','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'PO','ind_descripcion' => 'Prestaciones Sociales Obreros','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010402','plan_cuenta_pub20' => '12103010502','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'PP','ind_descripcion' => 'Pago Parcial','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '1','plan_cuenta_ade' => '2110401','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'PV','ind_descripcion' => 'Pagos de Viaticos','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '30','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110499','plan_cuenta_pub20' => '121030204','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'RB','ind_descripcion' => 'Recibo','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '37','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'RE','ind_descripcion' => 'Regularizaciones','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '37','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'RF','ind_descripcion' => 'Retenciones FAOV','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'RI','ind_descripcion' => 'Retencion de IVA','cod_detalle_clasif' => 'C','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '01','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '214990102','plan_cuenta_pub20' => '12133010102','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'RJ','ind_descripcion' => 'Retenciones Fondo de Jubilación','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'RP','ind_descripcion' => 'Retenciones Laborales Regimen de Empleo','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'SE','ind_descripcion' => 'Seniat Ret. IVA','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '35','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'SF','ind_descripcion' => 'Solicitud de Fondos','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '1','plan_cuenta_ade' => '1140199','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'SN','ind_descripcion' => 'Seniat Ret. ISLR','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '28','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'SO','ind_descripcion' => 'Retencionale Laborales (SSO)','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '02','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'SR','ind_descripcion' => 'Seniat Declaracion ISLR','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '28','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110310','plan_cuenta_pub20' => '12133020210','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'SS','ind_descripcion' => 'Pago Aporte SSO','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '2110401','plan_cuenta_pub20' => '12103010601','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'SY','ind_descripcion' => 'Saldos Iniciales','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '30','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'TE','ind_descripcion' => 'Transf. entre Cuentas','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '15','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'TR','ind_descripcion' => 'Transaccion','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '15','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'UM','ind_descripcion' => 'Pago Retenciones 1x1000','cod_detalle_clasif' => 'O','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '30','fk_cbc003_num_tipo_voucher_ordPago' => '','num_flag_provision' => '0','plan_cuenta' => '','plan_cuenta_pub20' => '','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '0'),
            array('cod_tipo_documento' => 'VE','ind_descripcion' => 'Bono Vacacional Empleados','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010201','plan_cuenta_pub20' => '12103010303','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'VF','ind_descripcion' => 'Bono Vacacional Altos Funcionarios','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010204','plan_cuenta_pub20' => '12103010315','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '0','num_flag_auto_nomina' => '1'),
            array('cod_tipo_documento' => 'VO','ind_descripcion' => 'Bono Vacacional Obreros','cod_detalle_clasif' => 'E','cod_detalle_regimen' => 'N','fk_cbc003_num_tipo_voucher' => '20','fk_cbc003_num_tipo_voucher_ordPago' => '03','num_flag_provision' => '1','plan_cuenta' => '211010202','plan_cuenta_pub20' => '12103010306','num_flag_adelanto' => '0','plan_cuenta_ade' => '','plan_cuenta_ade_pub20' => '','num_flag_fiscal' => '0', 'num_flag_monto_negativo' => '','num_flag_auto_nomina' => '1')
        );
        return $cp_b002_tipo_documento;
    }

    public function metDataClasificacionGasto()
    {
        $cp_b004_clasificacion_gastos = array(
            array('cod_clasificacion' => 'CC', 'ind_descripcion' => 'Reposicion de Gastos de Caja Chica', 'cod_detalle' => 'CC', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_clasificacion' => 'CR', 'ind_descripcion' => 'CIERRE DE CAJA CHICA', 'cod_detalle' => 'CC', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_clasificacion' => 'FG', 'ind_descripcion' => 'Reposicion de Gastos por Factura de Caja Chica', 'cod_detalle' => 'CC', 'fk_a018_num_seguridad_usuario' => '1')
        );
        return $cp_b004_clasificacion_gastos;
    }

    public function metDataConceptoGasto()
    {
        $cp_b005_concepto_gasto = array(
            array('cod_concepto_gasto' => '0001', 'cod_detalle' => '001', 'ind_descripcion' => 'Alimentos y Bebidas', 'partida_presupuestaria' => '402.01.01.00', 'plan_cuenta' => '6120103',
                'Det' => array(
                    array('clasificacion_gastos' => 'CC')
                )
            ),
            array('cod_concepto_gasto' => '0002', 'cod_detalle' => '014', 'ind_descripcion' => 'Fletes y Embalajes', 'partida_presupuestaria' => '403.06.01.00', 'plan_cuenta' => '6120103'),
            array('cod_concepto_gasto' => '0003', 'cod_detalle' => '003', 'ind_descripcion' => 'Textiles', 'partida_presupuestaria' => '402.03.01.00', 'plan_cuenta' => '6120103'),
            array('cod_concepto_gasto' => '0004', 'cod_detalle' => '003', 'ind_descripcion' => 'Prendas de Vestir', 'partida_presupuestaria' => '402.03.02.00', 'plan_cuenta' => '6120103'),
            array('cod_concepto_gasto' => '0005', 'cod_detalle' => '011', 'ind_descripcion' => 'Otros Materiales y Suministros', 'partida_presupuestaria' => '402.99.01.00', 'plan_cuenta' => '6120199'),
            array('cod_concepto_gasto' => '0006', 'cod_detalle' => '006', 'ind_descripcion' => 'Tintas, pinturas y colorantes', 'partida_presupuestaria' => '402.06.03.00', 'plan_cuenta' => '6120199'),
            array('cod_concepto_gasto' => '0007', 'cod_detalle' => '006', 'ind_descripcion' => 'Productos plásticos', 'partida_presupuestaria' => '402.06.08.00', 'plan_cuenta' => '6120199'),
            array('cod_concepto_gasto' => '0008', 'cod_detalle' => '008', 'ind_descripcion' => 'Otros productos metálicos', 'partida_presupuestaria' => '402.08.99.00', 'plan_cuenta' => '6120108'),
            array('cod_concepto_gasto' => '0009', 'cod_detalle' => '010', 'ind_descripcion' => 'Otros productos y útiles diversos', 'partida_presupuestaria' => '402.10.99.00', 'plan_cuenta' => '6120110'),
            array('cod_concepto_gasto' => '0010', 'cod_detalle' => '010', 'ind_descripcion' => 'Artículos de deporte, recreación y juguetes', 'partida_presupuestaria' => '402.10.01.00', 'plan_cuenta' => '6120110'),
            array('cod_concepto_gasto' => '0011', 'cod_detalle' => '015', 'ind_descripcion' => 'Publicidad y propaganda', 'partida_presupuestaria' => '403.07.01.00', 'plan_cuenta' => '61305'),
            array('cod_concepto_gasto' => '0012', 'cod_detalle' => '019', 'ind_descripcion' => 'Conservación y reparaciones menores transporte, tr', 'partida_presupuestaria' => '403.11.02.00', 'plan_cuenta' => '61309'),
            array('cod_concepto_gasto' => '0013', 'cod_detalle' => '015', 'ind_descripcion' => 'Imprenta y reproducción', 'partida_presupuestaria' => '403.07.02.00', 'plan_cuenta' => '61305'),
            array('cod_concepto_gasto' => '0014', 'cod_detalle' => '025', 'ind_descripcion' => 'Impuesto al valor agregado', 'partida_presupuestaria' => '403.18.01.00', 'plan_cuenta' => '6131701'),
            array('cod_concepto_gasto' => '0015', 'cod_detalle' => '026', 'ind_descripcion' => 'Otros servicios no personales', 'partida_presupuestaria' => '403.99.01.00', 'plan_cuenta' => '61399'),
            array('cod_concepto_gasto' => '0016', 'cod_detalle' => '010', 'ind_descripcion' => 'Materiales para instalaciones sanitarias', 'partida_presupuestaria' => '402.10.12.00', 'plan_cuenta' => '6120110'),
            array('cod_concepto_gasto' => '0017', 'cod_detalle' => '010', 'ind_descripcion' => 'Materiales eléctricos', 'partida_presupuestaria' => '402.10.11.00', 'plan_cuenta' => '6120110'),
            array('cod_concepto_gasto' => '0018', 'cod_detalle' => '005', 'ind_descripcion' => 'Productos de papel y cartón para oficina', 'partida_presupuestaria' => '402.05.03.00', 'plan_cuenta' => '6120105'),
            array('cod_concepto_gasto' => '0019', 'cod_detalle' => '010', 'ind_descripcion' => 'Útiles de escritorio, oficina y materiales de inst', 'partida_presupuestaria' => '402.10.05.00', 'plan_cuenta' => '6120110'),
            array('cod_concepto_gasto' => '0020', 'cod_detalle' => '008', 'ind_descripcion' => 'Repuestos y accesorios para equipos de transporte', 'partida_presupuestaria' => '402.08.09.00', 'plan_cuenta' => '6120108'),
            array('cod_concepto_gasto' => '0021', 'cod_detalle' => '010', 'ind_descripcion' => 'Condecoraciones, ofrendas y similares', 'partida_presupuestaria' => '402.10.06.00', 'plan_cuenta' => '6120110'),
            array('cod_concepto_gasto' => '0022', 'cod_detalle' => '010', 'ind_descripcion' => 'Materiales para equipos de computación', 'partida_presupuestaria' => '402.10.08.00', 'plan_cuenta' => '6120110'),
            array('cod_concepto_gasto' => '0023', 'cod_detalle' => '006', 'ind_descripcion' => 'Combustibles y lubricantes', 'partida_presupuestaria' => '402.06.06.00', 'plan_cuenta' => '6120106'),
            array('cod_concepto_gasto' => '0024', 'cod_detalle' => '019', 'ind_descripcion' => 'Conservación y reparaciones menores de máq, mueble', 'partida_presupuestaria' => '403.11.07.00', 'plan_cuenta' => '61309'),
            array('cod_concepto_gasto' => '0025', 'cod_detalle' => '010', 'ind_descripcion' => 'Materiales y útiles de limpieza y aseo', 'partida_presupuestaria' => '402.10.02.00', 'plan_cuenta' => '61309'),
            array('cod_concepto_gasto' => '0026', 'cod_detalle' => '008', 'ind_descripcion' => 'Herramientas menores,cuchillería y Ferreteria gral', 'partida_presupuestaria' => '402.08.03.00', 'plan_cuenta' => '61309'),
            array('cod_concepto_gasto' => '0027', 'cod_detalle' => '010', 'ind_descripcion' => 'Utensilios de cocina y comedor', 'partida_presupuestaria' => '402.10.03.00', 'plan_cuenta' => '6120110'),
            array('cod_concepto_gasto' => '0028', 'cod_detalle' => '016', 'ind_descripcion' => 'Primas y gastos de seguros', 'partida_presupuestaria' => '403.08.01.00', 'plan_cuenta' => '61306'),
            array('cod_concepto_gasto' => '0029', 'cod_detalle' => '005', 'ind_descripcion' => 'Pulpa de madera, papel y cartón', 'partida_presupuestaria' => '402.05.01.00', 'plan_cuenta' => '6120105'),
            array('cod_concepto_gasto' => '0030', 'cod_detalle' => '027', 'ind_descripcion' => 'Reparaciones, mejoras y adiciones mayores de equip', 'partida_presupuestaria' => '404.01.02.02', 'plan_cuenta' => '1230103',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR')
                )
            ),
            array('cod_concepto_gasto' => '0031', 'cod_detalle' => '004', 'ind_descripcion' => 'Cauchos y tripas para vehículos', 'partida_presupuestaria' => '402.04.03.00', 'plan_cuenta' => '6120104',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR')
                )
            ),
            array('cod_concepto_gasto' => '0032', 'cod_detalle' => '014', 'ind_descripcion' => 'Servicios de protección en traslado de fondos y de', 'partida_presupuestaria' => '403.06.05.00', 'plan_cuenta' => '61304',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR')
                )
            ),
            array('cod_concepto_gasto' => '0033', 'cod_detalle' => '006', 'ind_descripcion' => 'Sustancias químicas y de uso industrial', 'partida_presupuestaria' => '402.06.01.00', 'plan_cuenta' => '6120106',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR')
                )
            ),
            array('cod_concepto_gasto' => '0034', 'cod_detalle' => '008', 'ind_descripcion' => 'Repuestos y accesorios para otros equipos', 'partida_presupuestaria' => '402.08.10.00', 'plan_cuenta' => '6120108',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR'),
                    array('clasificacion_gastos' => 'FG')
                )
            ),
            array('cod_concepto_gasto' => '0035', 'cod_detalle' => '018', 'ind_descripcion' => 'Otros servicios profesionales y técnicos', 'partida_presupuestaria' => '403.10.99.00', 'plan_cuenta' => '61308',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR'),
                    array('clasificacion_gastos' => 'FG')
                )
            ),
            array('cod_concepto_gasto' => '0036', 'cod_detalle' => '010', 'ind_descripcion' => 'Productos de seguridad en el trabajo', 'partida_presupuestaria' => '402.10.07.00', 'plan_cuenta' => '6120110',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR'),
                    array('clasificacion_gastos' => 'FG')
                )
            ),
            array('cod_concepto_gasto' => '0037', 'cod_detalle' => '017', 'ind_descripcion' => 'Viáticos y pasajes dentro del país', 'partida_presupuestaria' => '403.09.01.00', 'plan_cuenta' => '61307',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR'),
                    array('clasificacion_gastos' => 'FG')
                )
            ),
            array('cod_concepto_gasto' => '0038', 'cod_detalle' => '007', 'ind_descripcion' => 'Vidrios y productos de vidrio', 'partida_presupuestaria' => '402.07.02.00', 'plan_cuenta' => '6120107',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR'),
                    array('clasificacion_gastos' => 'FG')
                )
            ),
            array('cod_concepto_gasto' => '0039', 'cod_detalle' => '001', 'ind_descripcion' => 'Productos agricolas y pecuarios', 'partida_presupuestaria' => '402.01.03.00', 'plan_cuenta' => '6120101',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR'),
                    array('clasificacion_gastos' => 'FG')
                )
            ),
            array('cod_concepto_gasto' => '0040', 'cod_detalle' => '015', 'ind_descripcion' => 'Aviso', 'partida_presupuestaria' => '403.07.04.00', 'plan_cuenta' => '61305',
                'Det' => array(
                    array('clasificacion_gastos' => 'CR'),
                    array('clasificacion_gastos' => 'FG')
                )
            )
        );
        return $cp_b005_concepto_gasto;
    }

    public function metDataCuentasBancarias()
    {
        $cp_b014_cuenta_bancaria = array(
            array('cod_detalle_banco' => '0001', 'ind_num_cuenta' => '01020613800000091954', 'ind_descripcion' => 'Banco de Venezuela 2014 Nomina', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2011-08-15', 'plan_cuenta' => '111010201001', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => '', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '2', 'num_flag_debito_bancario' => '2',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '01','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0004', 'ind_num_cuenta' => '01710006874000032035', 'ind_descripcion' => 'Banco Activo 2014 de Funcionamiento', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2009-06-12', 'plan_cuenta' => '111010202001', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'Plaza Ayacucho', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '2', 'num_flag_debito_bancario' => '2',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '05','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0003', 'ind_num_cuenta' => '01340043170431005638', 'ind_descripcion' => 'Banco Banesco 2014 Fondos de Terceros', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2005-01-01', 'plan_cuenta' => '111010202002', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => '', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '2', 'num_flag_debito_bancario' => '2',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0002', 'ind_num_cuenta' => '1150076510761034176', 'ind_descripcion' => 'Banco Exterior 2014 Fideicomiso', 'cod_detalle_tipo_cuenta' => 'AH', 'fec_apertura' => '2005-01-01', 'plan_cuenta' => '111010202003', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => '', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '2', 'num_flag_debito_bancario' => '2',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0005', 'ind_num_cuenta' => '01340043170431024187', 'ind_descripcion' => 'BANCO BANESCO CUENTA  DE TERCEROS', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2005-02-01', 'plan_cuenta' => '111010202004', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'MATURIN II', 'ind_distrito' => 'MONAGAS', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '2',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0001', 'ind_num_cuenta' => '01020613840000197955', 'ind_descripcion' => 'Banco de Venezuela 2014 Matriz (01020613840000197955)', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2014-03-24', 'plan_cuenta' => '111010201002', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'La Piramide', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '1',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0001', 'ind_num_cuenta' => '01020613810000160955', 'ind_descripcion' => 'Banco de Venezuela Nomina 2014 (01020613810000160955)', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2014-03-24', 'plan_cuenta' => '111010201003', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'La Piramide', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '1',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '01','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0001', 'ind_num_cuenta' => '01020613890000228853', 'ind_descripcion' => 'Banco de Venezuela Terceros 2014 (01020613890000228853)', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2014-03-24', 'plan_cuenta' => '111010201004', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'La Piramide', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '1',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0004', 'ind_num_cuenta' => '01710006806000925417', 'ind_descripcion' => 'Banco Activo Funcionamiento 2014 (01710006806000925417)', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2014-03-18', 'plan_cuenta' => '111010202005', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'Plaza Ayacucho', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '1',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '05','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0002', 'ind_num_cuenta' => '01150076501003567844', 'ind_descripcion' => 'Banco Exterior 2014 Fideicomiso (01150076501003567844)', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2014-04-08', 'plan_cuenta' => '111010202006', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'LA CATEDRAL', 'ind_distrito' => '', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '1',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '04','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0001', 'ind_num_cuenta' => '01020613860000230922', 'ind_descripcion' => 'Banco de Venezuela Nomina 2014 (01020613860000230922)', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2014-05-28', 'plan_cuenta' => '111010201005', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'LA PIRAMIDE', 'ind_distrito' => 'MATURIN', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '1',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0006', 'ind_num_cuenta' => 'CIERRE CAJA CHICA', 'ind_descripcion' => 'CIERRE CAJA CHICA', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2014-12-30', 'plan_cuenta' => '111010101', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => '', 'ind_distrito' => '1110102', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '2', 'num_flag_conciliacion_cp' => '2', 'num_flag_debito_bancario' => '2',
                'Det' => array(
                    array('cod_detalle' => '03','int_numero_ultimo_cheque' => '0')
                )),
            array('cod_detalle_banco' => '0001', 'ind_num_cuenta' => '01020613820000325578', 'ind_descripcion' => 'Banco de Venezuela Nomina 2016', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2015-12-14', 'plan_cuenta' => '111010201006', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'PIRAMIDE', 'ind_distrito' => 'MATURIN', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '1',
                'Det' => array(
                    array('cod_detalle' => '01','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '05','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '06','int_numero_ultimo_cheque' => '0')
                )
            ),
            array('cod_detalle_banco' => '0001', 'ind_num_cuenta' => '01020613810000325565', 'ind_descripcion' => 'Banco de Venezuela Funcionamiento 2016', 'cod_detalle_tipo_cuenta' => 'CO', 'fec_apertura' => '2015-12-14', 'plan_cuenta' => '111010201007', 'plan_cuenta_pub20' => '1110102', 'ind_agencia' => 'PIRAMIDE', 'ind_distrito' => 'MATURIN', 'ind_atencion' => '', 'ind_cargo' => '', 'num_flag_conciliacion_bancaria' => '1', 'num_flag_conciliacion_cp' => '1', 'num_flag_debito_bancario' => '1',
                'Det' => array(
                    array('cod_detalle' => '02','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '05','int_numero_ultimo_cheque' => '0'),
                    array('cod_detalle' => '06','int_numero_ultimo_cheque' => '0')
                )
            )
        );
        return $cp_b014_cuenta_bancaria;
    }

    public function metDataTipoTransaccion()
    {
        $cp_b006_banco_tipo_transaccion = array(
            array('cod_tipo_transaccion' => 'ABP', 'ind_descripcion' => 'Ajuste Bancario Positivo', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'AGJ', 'ind_descripcion' => 'Aguinaldos Jubilados', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'AGP', 'ind_descripcion' => 'Aguinaldos Pensionados', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'AJB', 'ind_descripcion' => 'Ajuste Bancario Negativo', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'AJU', 'ind_descripcion' => 'Aguinandos Jubilados', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'ANP', 'ind_descripcion' => 'Anticipo Proveedores', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '11209', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'AOB', 'ind_descripcion' => 'Aguinaldos Pensionados', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'BVE', 'ind_descripcion' => 'Bono Vacacional Empleado', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '45', 'plan_cuenta' => '111010201008', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'CB', 'ind_descripcion' => 'Comisiones Bancarias', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'CCG', 'ind_descripcion' => 'Otras Cuentas Por Pagar', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110499', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'CH', 'ind_descripcion' => 'Cheques Proveedores', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110401', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'CHP', 'ind_descripcion' => 'Cheques Proveedores', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110401', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'CPA', 'ind_descripcion' => 'Complemento al Personal de Alto Nivel por Comisión', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => 'PR', 'plan_cuenta' => '111010201008', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'DCP', 'ind_descripcion' => 'Deposito Cheque', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110401', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'DPA', 'ind_descripcion' => 'Depositos de Asignacion', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '517010102', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'DRL', 'ind_descripcion' => 'Deposito de Retenciones Laborales', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'FDE', 'ind_descripcion' => 'Fedeicomiso Empleados', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211010401', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'FDO', 'ind_descripcion' => 'Fedeicomiso Obreros', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211010402', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'FIE', 'ind_descripcion' => 'Fideicomiso Empleados', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211010401', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'FIO', 'ind_descripcion' => 'Fideicomiso Obreros', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211010402', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'GM', 'ind_descripcion' => 'Gastos Medicos Empleados', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IBA', 'ind_descripcion' => 'Impuesto a Debito Bancario', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'ICC', 'ind_descripcion' => 'Impuesto Dep Cta Cte', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IDB', 'ind_descripcion' => 'Impuesto a Debito Bancario', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IDC', 'ind_descripcion' => 'Impuesto Dep Cta Cte', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IDE', 'ind_descripcion' => 'Interes Bancario', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IMT', 'ind_descripcion' => 'ISLR Retenido al Trabajador', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110310', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'ING', 'ind_descripcion' => 'Interes Ganado', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IPI', 'ind_descripcion' => 'Ingresos por Interes', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IST', 'ind_descripcion' => 'ISLR Retenido al Trabajador', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110310', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'ITB', 'ind_descripcion' => 'Interes Bancario', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'ITF', 'ind_descripcion' => 'Pago de Debito Bancario', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'ITG', 'ind_descripcion' => 'Interes Ganado', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IVA', 'ind_descripcion' => 'Agente Retencion IVA', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '214990102', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IVR', 'ind_descripcion' => 'Agente Retencion IVA', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '214990102', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IXI', 'ind_descripcion' => 'Ingresos por Interes', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'LFT', 'ind_descripcion' => 'Ley Paro Forzoso Trabajador', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '', 'plan_cuenta' => '2110304', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'LPF', 'ind_descripcion' => 'Ley Paro Forzoso Patronal', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110204', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'LPH', 'ind_descripcion' => 'Ley de Politica Habitacional Patronal', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110205', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'LPP', 'ind_descripcion' => 'Ley Paro Forzoso Patronal', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110204', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'LPT', 'ind_descripcion' => 'Ley de Politica Habitacional Trabajador', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110305', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'NEM', 'ind_descripcion' => 'Nomina Pago Empleados', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'NJB', 'ind_descripcion' => 'Nomina Jubildos', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'NJU', 'ind_descripcion' => 'Nomina Jubildos', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'NOB', 'ind_descripcion' => 'Nomina Pagar Obreros', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211010102', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'NOP', 'ind_descripcion' => 'Nomina Pensionados', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'NPE', 'ind_descripcion' => 'Nomina Pago Empleados', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'NPO', 'ind_descripcion' => 'Nomina Pagar Obreros', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211010102', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'NPS', 'ind_descripcion' => 'Pago Nomina Pensionados', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'OCP', 'ind_descripcion' => 'Otras Cuentas Por Pagar', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110499', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PAG', 'ind_descripcion' => 'Pagos', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PDB', 'ind_descripcion' => 'Pago de Debito Bancario', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PFT', 'ind_descripcion' => 'Ley Paro Forzoso Trabajador', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110304', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PHP', 'ind_descripcion' => 'Ley de Politica Habitacional Patronal', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110205', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PHT', 'ind_descripcion' => 'Ley de Politica Habitacional Trabajador', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110305', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PJ', 'ind_descripcion' => 'Pension Sobreviviente Jubilados', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => 'PR', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PRV', 'ind_descripcion' => 'Proveedores Nacionales', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110401', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PS', 'ind_descripcion' => 'Pension Sobreviviente Pensionados', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => 'PR', 'plan_cuenta' => '631010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PVE', 'ind_descripcion' => 'Pago de Viaticos Empleado', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61307', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PVN', 'ind_descripcion' => 'Proveedores Nacionales', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110401', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RAC', 'ind_descripcion' => 'REINTEGRO POR ANULACIÓN DE CHEQUE', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '', 'plan_cuenta' => '5210701', 'num_flag_transaccion' => '1', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RCC', 'ind_descripcion' => 'Reintegro de Caja Chica', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '111010101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RPA', 'ind_descripcion' => 'Reverso de Pago', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RPC', 'ind_descripcion' => 'Reintegro Prima a Empleado', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '6110103', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RSE', 'ind_descripcion' => 'Reintegro Sueldo Empleado', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '6110101', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RSO', 'ind_descripcion' => 'Reintegro Sueldo Obrero', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '6110111', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RVI', 'ind_descripcion' => 'Reintegro de Viaticos', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61307', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SAP', 'ind_descripcion' => 'Seguro Social aporte Patronal', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110201', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SAT', 'ind_descripcion' => 'Seguro Social Trabajadores', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110301', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SEV', 'ind_descripcion' => 'Seguro Vehiculos', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SGV', 'ind_descripcion' => 'Seguro Vehiculos', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '61306', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SI', 'ind_descripcion' => 'Saldo Inicial', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2199901', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SIN', 'ind_descripcion' => 'Saldo Inicial', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2199901', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SSP', 'ind_descripcion' => 'Seguro Social aporte Patronal', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110201', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SST', 'ind_descripcion' => 'Seguro Social Trabajadores', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2110301', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'TB', 'ind_descripcion' => 'Transferencia negativo', 'cod_detalle' => 'T', 'FlagVoucher' => 'S', 'num_flag_voucher' => '', 'plan_cuenta' => '', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'TBP', 'ind_descripcion' => 'Transferencia Positivo', 'cod_detalle' => 'T', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'TEC', 'ind_descripcion' => 'Transferencia entre Cuentas', 'cod_detalle' => 'T', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'DSS', 'ind_descripcion' => 'Deuda Instituto Venezolano De Los Seguros Sociales', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '30', 'plan_cuenta' => '211049906', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RSS', 'ind_descripcion' => 'Deuda Instituto Venezolano De Los Seguros Sociales', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '07', 'plan_cuenta' => '211049906', 'num_flag_transaccion' => '1', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'PPA', 'ind_descripcion' => 'Prestaciones Sociales en Fideicomiso Pendientes po', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '15', 'plan_cuenta' => '211049908', 'num_flag_transaccion' => '1', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RRE', 'ind_descripcion' => 'Reintregro de Retencion de Ley no Efectuadas a Emp', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '112039901', 'num_flag_transaccion' => '1', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'DTC', 'ind_descripcion' => 'Deposito Transferencia entre Cuentas', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '15', 'plan_cuenta' => '1199901', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'SCC', 'ind_descripcion' => 'Saldo Inicial para el Ciere de Caja Chica', 'cod_detalle' => 'I', 'FlagVoucher' => 'N', 'num_flag_voucher' => '16', 'plan_cuenta' => '', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RVA', 'ind_descripcion' => 'Reintegros Varios de Nomina Por Reclasificar', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211039902', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'IS', 'ind_descripcion' => 'Deposito indemnización Polizas de Seguros', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '02', 'plan_cuenta' => '2199903', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RVF', 'ind_descripcion' => 'Reintegros VROS Gastos de Funcionamientos P/Reclas', 'cod_detalle' => 'I', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2199999', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RRA', 'ind_descripcion' => 'Reintegros Varios de Nomina Por Reclasificar', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '211039902', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1'),
            array('cod_tipo_transaccion' => 'RRF', 'ind_descripcion' => 'Reintegros VROS Gastos de Funcionamientos P/Reclas', 'cod_detalle' => 'E', 'FlagVoucher' => 'S', 'num_flag_voucher' => '1', 'plan_cuenta' => '2199999', 'num_flag_transaccion' => '2', 'num_flag_transaccion_planilla' => '2', 'num_estatus' => '1')
        );
        return $cp_b006_banco_tipo_transaccion;
    }

}