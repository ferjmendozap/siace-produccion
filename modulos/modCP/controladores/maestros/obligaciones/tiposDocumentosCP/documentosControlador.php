<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class documentosControlador extends Controlador
{
    private $atDocumentosModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atDocumentosModelo = $this->metCargarModelo('documento');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoDocumento()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $ver = $this->metObtenerInt('ver');
        $valido = $this->metObtenerInt('valido');
        $idDocumento = $this->metObtenerInt('idDocumento');

        if ($valido == 1){
            $Excepcion=array('num_estatus','fk_cbb004_num_plan_cuenta','fk_cbb004_num_plan_cuenta_pub20',
                'fk_cbb004_num_plan_cuenta_ade','fk_cbb004_num_plan_cuenta_ade_pub20','fk_cbc003_num_tipo_voucher_ordPago');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excepcion);
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');

            if($alphaNum!=null && $ind==null){
                $registro=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $registro=$ind;
            }else{
                $registro=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$registro)){
                $registro['status']='error';
                echo json_encode($registro);
                exit;
            }

            if (!isset($registro['num_estatus'])) {
                $registro['num_estatus'] = 0;
                $registro['ico_estatus'] = 'md md-not-interested';
            }
            if (!isset($registro['num_flag_auto_nomina'])) {
                $registro['num_flag_auto_nomina'] = 0;
            }
            if (!isset($registro['num_flag_fiscal'])) {
                $registro['num_flag_fiscal'] = 0;
                $registro['ico_flag_fiscal']='md md-not-interested';
            }
            if (!isset($registro['num_flag_provision'])) {
                $registro['num_flag_provision'] = 0;
                $registro['ico_flag_provision']='md md-not-interested';
            }
            if (!isset($registro['num_flag_adelanto'])) {
                $registro['num_flag_adelanto'] = 0;
                $registro['ico_flag_adelanto']='md md-not-interested';
            }
            if ($registro['fk_cbb004_num_plan_cuenta']==0) {
                $registro['fk_cbb004_num_plan_cuenta'] = null;
            }
            if ($registro['fk_cbb004_num_plan_cuenta_pub20']==0) {
                $registro['fk_cbb004_num_plan_cuenta_pub20'] = null;
            }
            if ($registro['fk_cbb004_num_plan_cuenta_ade']==0) {
                $registro['fk_cbb004_num_plan_cuenta_ade'] = null;
            }
            if ($registro['fk_cbb004_num_plan_cuenta_ade_pub20']==0) {
                $registro['fk_cbb004_num_plan_cuenta_ade_pub20'] = null;
            }
            if ($registro['fk_cbc003_num_tipo_voucher_ordPago']==0) {
                $registro['fk_cbc003_num_tipo_voucher_ordPago'] = null;
            }

            if ($idDocumento === 0) {

                $registro['status'] = 'nuevo';
                $id = $this->atDocumentosModelo->metNuevoDocumento($registro['cod_tipo_documento'],
                    $registro['ind_descripcion'],$registro['num_flag_adelanto'],$registro['num_flag_auto_nomina'],$registro['num_flag_fiscal'],
                    $registro['num_flag_provision'],$registro['num_estatus'],$registro['fk_a006_num_miscelaneo_detalle_regimen_fiscal'],
                    $registro['fk_a006_num_miscelaneo_detalle_clasificacion'],$registro['fk_cbc003_num_tipo_voucher'],$registro['fk_cbc003_num_tipo_voucher_ordPago'],
                    $registro['fk_cbb004_num_plan_cuenta'],$registro['fk_cbb004_num_plan_cuenta_ade'],
                    $registro['fk_cbb004_num_plan_cuenta_pub20'],$registro['fk_cbb004_num_plan_cuenta_ade_pub20']);
                $registro['idDocumento'] = $id;

            } else {
                $registro['status'] = 'modificar';
                 $id = $this->atDocumentosModelo->metModificarDocumento($idDocumento,$registro['ind_descripcion'],$registro['num_flag_adelanto'],$registro['num_flag_auto_nomina'],$registro['num_flag_fiscal'],
                     $registro['num_flag_provision'],$registro['num_estatus'],$registro['fk_a006_num_miscelaneo_detalle_regimen_fiscal'],
                     $registro['fk_a006_num_miscelaneo_detalle_clasificacion'],$registro['fk_cbc003_num_tipo_voucher'],$registro['fk_cbc003_num_tipo_voucher_ordPago'],
                     $registro['fk_cbb004_num_plan_cuenta'],$registro['fk_cbb004_num_plan_cuenta_ade'],
                     $registro['fk_cbb004_num_plan_cuenta_pub20'],$registro['fk_cbb004_num_plan_cuenta_ade_pub20']);
                $registro['idDocumento'] = $id;
           }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            echo json_encode($registro);
            exit;
        }
        if ($idDocumento != 0) {
            $db = $this->atDocumentosModelo->metDocumentoConsultaDetalle($idDocumento);
            $dbCuenta = $this->atDocumentosModelo->metDocumentoConsultaDetalleCuenta($idDocumento);
            $this->atVista->assign('dataBD', $db);
            $this->atVista->assign('cuenta', $dbCuenta);
        }

        $this->atVista->assign('listadoRegimen',$this->atDocumentosModelo->atMiscelaneoModelo->metMostrarSelect('REGFISCAL'));
        $this->atVista->assign('listadoVoucher',$this->atDocumentosModelo->atTipoVoucherModelo->metListar());
        $this->atVista->assign('selectCLASIFICAC',$this->atDocumentosModelo->atMiscelaneoModelo->metMostrarSelect('CLASDOC'));

        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idDocumento',$idDocumento);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }

    public function metEliminar()
    {
        $idDocumento = $this->metObtenerInt('idDocumento');
        if($idDocumento!=0){
            $id=$this->atDocumentosModelo->metEliminarDocumento($idDocumento);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Docuemento se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idDocumento'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
            d.*,
            rf.ind_nombre_detalle AS regimen,
            cl.ind_nombre_detalle AS clasificacion,
            cb_c003_tipo_voucher.cod_voucher
            FROM
            cp_b002_tipo_documento AS d
            INNER JOIN a006_miscelaneo_detalle AS rf ON rf.pk_num_miscelaneo_detalle = d.fk_a006_num_miscelaneo_detalle_regimen_fiscal
            INNER JOIN a006_miscelaneo_detalle AS cl ON cl.pk_num_miscelaneo_detalle = d.fk_a006_num_miscelaneo_detalle_clasificacion
            LEFT JOIN cb_c003_tipo_voucher ON cb_c003_tipo_voucher.pk_num_voucher = d.fk_cbc003_num_tipo_voucher
            WHERE d.num_flag_obligacion = 1 
            ";
        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        d.cod_tipo_documento LIKE '%$busqueda[value]%' OR
                        d.ind_descripcion LIKE '%$busqueda[value]%' OR
                        cb_c003_tipo_voucher.cod_voucher LIKE '%$busqueda[value]%' OR
                        cl.ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                        rf.ind_nombre_detalle LIKE '%$busqueda[value]%'  
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_tipo_documento','ind_descripcion','num_flag_adelanto','num_flag_provision','num_flag_fiscal','num_flag_auto_nomina','cod_voucher','clasificacion','regimen','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_tipo_documento';
        #construyo el listado de botones
        $flags = array('num_flag_adelanto','num_flag_provision','num_flag_fiscal','num_flag_auto_nomina');

        if (in_array('CP-01-07-01-03-01-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar Tipo Documento btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idDocumento="'.$clavePrimaria.'" title="Editar"
                        descipcion="El Usuario ha Modificado un Tipo de Documento" titulo="<i class=\'fa fa-edit\'></i> Editar Tipo Documento">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('CP-01-07-01-03-02-V',$rol)) {
            $campos['boton']['Ver'] = '
               <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idDocumento="'.$clavePrimaria.'" title="Consultar"
                        descipcion="El Usuario esta viendo un Tipo de Documento" titulo="<i class=\'md md-remove-red-eye\'></i> Consultar Tipo de Documento">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('CP-01-07-01-03-03-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                 <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDocumento="'.$clavePrimaria.'" title="Eliminar"  boton="si, Eliminar"
                        descipcion="El usuario ha eliminado un Tipo de Documento" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Documento!!">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false, $flags);

    }

}