<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class cuentasdControlador extends Controlador
{
    private $atCuentasdModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atCuentasdModelo = $this->metCargarModelo('cuentasd');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('cuentasdBD',$this->atCuentasdModelo->metCuentasdListar());
        $this->atVista->metRenderizar('listado');
    }
    public function metNuevaCuentad(){
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $ver = $this->metObtenerInt('ver');
        $idCuentad = $this->metObtenerInt('idCuentad');
        $valido = $this->metObtenerInt('valido');

        if ($valido == 1) {
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $registro = array_merge($ind);

            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if ($idCuentad == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atCuentasdModelo->metNuevaCuentad($registro['fk_cpb014_pk_num_cuenta_bancaria'],$registro['fk_a006_num_miscelaneo_detalle_tipo_pago']);
                $registro['idCuentad'] = $id;
                $idNuevo = $this->atCuentasdModelo->metCuentasdListar($registro['idCuentad']);

            } else {
                $registro['status'] = 'modificar';
                $id = $this->atCuentasdModelo->metModificarCuenta($idCuentad,$registro['fk_cpb014_pk_num_cuenta_bancaria'],$registro['fk_a006_num_miscelaneo_detalle_tipo_pago']);
                $registro['idCuentad'] = $id;
                $idNuevo = $this->atCuentasdModelo->metCuentasdListar($idCuentad);
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }

        if ($idCuentad != 0) {
            $db = $this->atCuentasdModelo->metCuentasdConsultaDetalle($idCuentad);
            $this->atVista->assign('cuentasdBD', $db);
        }

        $this->atVista->assign('listadoTipoPago',$this->atCuentasdModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
        $this->atVista->assign('listadoCuentas',$this->atCuentasdModelo->atCuentasModelo->metCuentaListar());

        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idCuentad',$idCuentad);
        $this->atVista->metRenderizar('nuevo', 'modales');

    }

    public function metEliminar()
    {
        $idCuentad = $this->metObtenerInt('idCuentad');
        if($idCuentad!=0){
            $id=$this->atCuentasdModelo->metEliminarCuentad($idCuentad);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Cuenta Bancaria se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idCuentad'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


}