<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class chequeraControlador extends Controlador
{
    private $atChequeraModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atChequeraModelo = $this->metCargarModelo('chequera');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('dataBD',$this->atChequeraModelo->metChequeraListar());
        $this->atVista->metRenderizar('listado');
    }

    public function metAcciones()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $ver = $this->metObtenerInt('ver');
        $valido = $this->metObtenerInt('valido');
        $idChequera = $this->metObtenerInt('idChequera');
        $idCuenta = $this->metObtenerInt('idCuenta');

        if ($valido == 1){
            $Excepcion=array('num_estatus');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excepcion);
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');

            if($alphaNum!=null && $ind==null){
                $registro=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $registro=$ind;
            }else{
                $registro=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$registro)){
                $registro['status']='error';
                echo json_encode($registro);
                exit;
            }
            if(!isset($registro['chequera'])){
                $registro['status'] = 'errorChequera';
                echo json_encode($registro);
                exit;
            }else{
                $secuencia = $registro['chequera']['num_secuencia'];
                foreach($secuencia as $sec){
                    $desde = $registro['chequera'][$sec]['num_chequera_desde'];
                    $hasta = $registro['chequera'][$sec]['num_chequera_hasta'];
                    if($desde == '' or $hasta == '') {
                        $registro['status'] = 'errorChequera';
                        echo json_encode($registro);
                        exit;
                    }else{
                        if($desde > $hasta){
                            $registro['status'] = 'errorCorrelativo';
                            echo json_encode($registro);
                            exit;
                        }
                    }

                    if($registro['chequera'][$sec]['ind_num_cheque'] == '') {
                        $registro['chequera'][$sec]['ind_num_cheque'] = 0;
                    }
                    if(!isset($registro['chequera'][$sec]['num_estatus'])) {
                        $registro['chequera'][$sec]['num_estatus'] = 0;
                    }
                }
            }

            if ($idChequera === 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atChequeraModelo->metNuevo($registro);
                $registro['idChequera'] = $id;
                $idNuevo = $this->atChequeraModelo->metChequeraListar($registro['idChequera']);

            } else {
                $registro['status'] = 'modificar';
                 $id = $this->atChequeraModelo->metModificar($idCuenta,$registro);
                $registro['idChequera'] = $id;
                $idNuevo = $this->atChequeraModelo->metChequeraListar($idChequera);
           }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        if ($idChequera != 0) {
            $db = $this->atChequeraModelo->metConsulta($idCuenta);
            $this->atVista->assign('dataBD', $db);

        }
        $this->atVista->assign('selectBANCOS', $this->atChequeraModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));
        $this->atVista->assign('selectCuentas', $this->atChequeraModelo->atCuentasModelo->metCuentaListar());

        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idCuenta',$idCuenta);
        $this->atVista->assign('idChequera',$idChequera);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }

    public function metEliminar()
    {
        $idChequera = $this->metObtenerInt('idChequera');
        if($idChequera!=0){
            $id=$this->atChequeraModelo->metEliminarDocumento($idChequera);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Docuemento se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idChequera'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


}