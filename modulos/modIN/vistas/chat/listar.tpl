<section class="style-default-light">

<!--TITULO DEL FORMULARIO-->
<div class="row">
	<div class="col-lg-12">
		<div class="card-head"><h2 class="text-primary">&nbsp;Listar Conversaciones</h2></div>
	</div>
</div>&nbsp;
<div class="row">
	
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body ">
				<form id="formAjax2" class="form form-validation" role="form" >
					<div class="col-lg-3">
						<div class="form-group" >
							<div class="input-group">
								<div class="input-group-addon">
									<div class=" radio radio-inline radio-styled">
										<label>
											<input id="radio_1" type="radio" name="radio_filtro" value="1" checked >
											<span></span>
										</label>
									</div>
								</div>
								<div class="input-group-content">
                                    <label for="radio_1">Todas la conversaciones</label>
                                </div>
							</div>
						</div>	
					</div>
					<div class="col-lg-6">
						<div class="form-group" >
							<div class="input-group">
								<div class="input-group-addon">
									<div class="radio radio-inline radio-styled">
										<label>
											<input id="radio_2" type="radio" name="radio_filtro" value="2" class="radio_2">
											<span></span>
										</label>
									</div>
								</div>
								<div class="input-group-content">
									<div id="demo-date-range1" class="input-daterange input-group">
										<div class="input-group-content">
											<input disabled id="start_fec_conversacion" type="text" class="form-control" name="start_fec_conversacion"  value="" required aria-required="true" aria-invalid="true" >
											<label>Fecha de la conversación:</label>
											<p class="help-block">DD-MM-YYYY</p>
										</div>
										<span class="input-group-addon">a</span>
										<div class="input-group-content">
											<input disabled  id="end_fec_conversacion" class="form-control" type="text" name="end_fec_conversacion"  value="" required aria-required="true" aria-invalid="true" >
											<div class="form-control-line"></div>
											<p class="help-block">DD-MM-YYYY</p>
										</div>
									</div>
								</div>
							</div>	
						</div>
					</div>
					<div class="col-lg-3"  >
						<div class="form-group" >
			    			<button type="submit" id="botonBuscarConversacion" class="btn btn-sm btn-primary ink-reaction btn-raised logsUsuarioModal" >Buscar</button>
            			</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<div class="row">
	<div class="col-lg-12" align="right">
		<div class="form-group" >
			<button type="submit" id="botonEliminarConversacion" tipoEliminacion="T" class="btn btn-sm btn-danger ink-reaction btn-raised logsUsuarioModal botonEliminarConversacion" >Eliminar conversaciones</button>
		</div>

		<table id="datatableChat" class="table table-striped table-hover" WIDTH="90%" >
			<thead>
				<tr  align="center">
                    <th ><i class=""></i>N°</th>
					<th align="center" style="width: 200px;" >Funcionario</th>{* md-insert-photo*}
					<th ><i class=""></i>Mensaje</th>
					<th ><i class=""></i>Fecha</th>{*md-today*}
                    <th ><i class=""></i>Hora</th>{*md-query-builder*}
                    <th ><i class=""></i>Eliminar</th>{*md-border-color*}
	            </tr>
			</thead>
			<tbody >
			{$i=1}
			{foreach name=for_fecha_conversacion item=indice2 from=$fechasConversaciones}
					<tr class="fila_agregar separador_conversacion " >
						<td></td>
						<td></td>
						<td ><i class="fa md-forum" ></i> Conversación {$indice2.fec_registro}</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					{foreach name=for_conversacion item=indice from=$datosConversacion}
						{if $indice2.fec_registro ==  $indice.fec_registro}
						<tr class="fila_agregar" >
							<td>{$i++}</td>
							<td><div style="width: 100%; padding-right: 20px; padding-left: 0px;" align="center"  ><img class="img-circle width-0 ampliar_foto imagenFuncionario" src="{$_Parametros.ruta_Img}modRH/fotos/{$indice.ind_foto}" alt="" style="cursor: pointer" title="Ampliar Foto" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" ind_foto="{$indice.ind_foto}" titulo="{$indice.nombre} {$indice.apellido}"><span class="text-medium "><h6>{$indice.nombre} {$indice.apellido}</h6></span></div></td>
							<td class="formato_mensaje">{$indice.ind_mensaje}</td>
							<td>{$indice.fec_registro}</td>
							<td>{$indice.hora}</td>
							<td><button type="button" mensaje="{$indice.ind_mensaje}"  pk_num_chat = "{$indice.pk_num_chat}" class="eliminarMensaje gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" ><i class="md-delete"></i></button></td>
						</tr>
						{/if}
					{/foreach}
			{/foreach}
			</tbody>
		</table>

	</div>
</div>

</section>
<style>
	.separador_conversacion{
		text-align: center;
		background-color:#CAF0EE !important;
		font-weight: bold !important;
	}
	.formato_mensaje{
		overflow-x: auto;
		overflow-y: hidden;
		width: 300px;
		text-align: justify;
	}
</style>
<script>
	$('#datatableChat').DataTable({ //ley
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columnas",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": 'Mostrar _MENU_',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});
	var tabla_listado = $('#datatableChat').DataTable();
//*************************************************************
//*  FORMATEAR LA FECHA 
//*************************************************************
$("#demo-date-range1").datepicker({
	todayHighlight: true,
	format:'dd-mm-yyyy',
	autoclose: true,
	language:'es'
});
//*************************************************************
//*
//*************************************************************
$(document).ready(function() {
//**************************************************************
//*   VER IMAGEN DEL FUNCIONARIO
//**************************************************************
 $('#datatableChat tbody').on( 'click', '.imagenFuncionario', function () {

	var titulo2 = '';
	if($(this).attr('titulo') != 'null')
	{
		titulo2 = $(this).attr('titulo');
	}

	$('#modalAncho').css("width", "30%");
    $('#formModalLabel').html('');
    $('#formModalLabel').html('FUNC. '+ titulo2);
    var srcImagen = $(this).attr('src');
    var imagenNoticia = '<div align="center"><img width="80%" src="'+srcImagen+'"></div>';    
    $('#ContenidoModal').html('');
    $('#ContenidoModal').html(imagenNoticia);
 });
//**************************************************************
//*   ACTIVAR DESACTIVAR FECHA
//**************************************************************
$(document).on('change','#radio_1', function(){

        if (this.checked){

			$('input[id="start_fec_conversacion"]').val('');
            $('input[id="start_fec_conversacion"]').prop('disabled', true);
			$('input[id="end_fec_conversacion"]').val('');
			$('input[id="end_fec_conversacion"]').prop('disabled', true);

		}
});

$(document).on('change','#radio_2', function(){

	if (this.checked){
		$('input[id="start_fec_conversacion"]').val('');
		$('input[id="start_fec_conversacion"]').prop('disabled', false);
		$('input[id="end_fec_conversacion"]').val('');
		$('input[id="end_fec_conversacion"]').prop('disabled', false);



	}

});

//**************************************************************
//*  ELIMINAR MENSAJE
//**************************************************************
$('#datatableChat tbody').on('click','.eliminarMensaje', function() {

	var mensaje = $(this).attr('mensaje');
	var pk_num_chat = $(this).attr('pk_num_chat');
	var colunna = $(this);

	swal({
		title: '¿Eliminar el mensaje de la conversación?',
		html: true,
		text: '<div style="overflow-x: auto; overflow-y: hidden;" ><strong>Mensaje:</strong> '+mensaje+'</div>',
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: 'Sí, Eliminar',
		cancelButtonText: "Cancelar",
		closeOnConfirm: false
	}, function () {

			var urlEliminar = '{$_Parametros.url}modIN/chatCONTROL/EliminarMensajeChatMET';
			$.post(urlEliminar, { pk_num_chat: pk_num_chat }, function (resp) {
				if (resp == 1) {

					swal("Mensaje eliminado", "Mensaje eliminado exitosamente", "success");
					var tabla_listado = $('#datatableChat').DataTable();
					tabla_listado.row(colunna.parents('tr'))
							.remove()
							.draw();

				}
				else {
					swal("Error al eliminar mensaje",resp, "error");
				}
			});
	});
});

//**************************************************************
//*  ELIMINAR CONVERSACIONES
//**************************************************************
$('#botonEliminarConversacion').click(function() {

	var tabla_listado = $('#datatableChat').DataTable();
	var total = tabla_listado.data().length;

	if( total == 0)
	{
		swal("No hay conversaciones...", "No hay registros", "warning");
	}
	else
	{
		var tipoEliminacion = $(this).attr('tipoEliminacion');
		if(tipoEliminacion == 'T')
		{
			swal({
				title: '¿Desea eliminar todas las conversaciones?',
				html: true,
				text: 'Se eliminaran permanentemente',
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: 'Sí, Eliminar',
				cancelButtonText: "Cancelar",
				closeOnConfirm: false
			}, function () {

				var urlEliminar = '{$_Parametros.url}modIN/chatCONTROL/EliminarConversacionesChatMET';
				$.post(urlEliminar, { tipo: 'T' }, function (resp) {
					if (resp == 1) {

						swal("Conversaciones eliminadas", "eliminadas exitosamente", "success");
						var tabla_listado = $('#datatableChat').DataTable();
						tabla_listado.clear().draw();

					}
					else {
						swal("Error al eliminar conversaciones", resp, "error");
					}
				});
			});
		}
		else
		{
			var fechUnicaConversacion = $('#start_fec_conversacion').val();
			var fechUnicaConversacion2 = $('#end_fec_conversacion').val();

			swal({
				title: '¿Desea eliminar las conversaciones en el rango de fechas especificado?',
				html: true,
				text: 'Se eliminaran permanentemente',
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: 'Sí, Eliminar',
				cancelButtonText: "Cancelar",
				closeOnConfirm: false
			}, function () {

				var urlEliminar = '{$_Parametros.url}modIN/chatCONTROL/EliminarConversacionesChatMET';
				$.post(urlEliminar, { fechUnicaConversacion: fechUnicaConversacion, fechUnicaConversacion2: fechUnicaConversacion2, tipo: 'U' }, function (resp) {
					if (resp == 1) {

						swal("Conversaciones eliminadas", "eliminación exitosa", "success");
						var tabla_listado = $('#datatableChat').DataTable();
						tabla_listado.clear().draw();

					}
					else {
						swal("Error al eliminar conversación", resp, "error");
					}
				});
			});


		}
	}

});

//**************************************************************
//*   FILTRO Y ACTUALIZACIÓN DE LA LISTA
//**************************************************************

 $("#formAjax2").validate({
    
    submitHandler: function(form) {

    	var url_listar='{$_Parametros.url}modIN/chatCONTROL/BuscarConversacionesListadoMET';
		$.post(url_listar,$(form).serialize(),function(respuesta_post){

			//****************************************************
			//*   DATA TABLE
			//****************************************************

			if(respuesta_post)
			{

				if( $('input:radio[name=radio_filtro]:checked').val() == '1')
				{
					$("#botonEliminarConversacion").attr('tipoEliminacion', 'T');
				}
				else
				{
					$("#botonEliminarConversacion").attr('tipoEliminacion', 'U');
				}



				tabla_listado.clear().draw();


					var fechasConversaciones = respuesta_post['fechasConversaciones'];
					var conversaciones = respuesta_post['conversaciones'];

					if(conversaciones.length > 0)
					{
						var contador = 1;
						for (var j = 0; j < fechasConversaciones.length; j++) {
							tabla_listado.row.add([
								'', '', '<i class="fa md-forum" ></i> Conversación ' + fechasConversaciones[j].fec_registro, '', '', ''
							]).draw()
									.nodes()
									.to$()
									.addClass('separador_conversacion');
							for (var i = 0; i < conversaciones.length; i++) {
								if (fechasConversaciones[j].fec_registro == conversaciones[i].fec_registro) {
									tabla_listado.row.add([
										contador++,
										'<div style="width: 100%; padding-right: 20px; padding-left: 0px;" align="center"  ><img class="img-circle width-0 ampliar_foto imagenFuncionario" src="{$_Parametros.ruta_Img}modRH/fotos/' + conversaciones[i].ind_foto + '" alt="" style="cursor: pointer" title="Ampliar Foto" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" ind_foto="' + conversaciones[i].ind_foto + '" titulo="' + conversaciones[i].nombre + ' ' + conversaciones[i].apellido + '"><span class="text-medium "><h6>' + conversaciones[i].nombre + ' ' + conversaciones[i].apellido + '</h6></span></div>',
										conversaciones[i].ind_mensaje,
										conversaciones[i].fec_registro,
										conversaciones[i].hora,
										'<button type="button" mensaje="' + conversaciones[i].ind_mensaje + '"  pk_num_chat = "' + conversaciones[i].pk_num_chat + '" class="eliminarMensaje gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" ><i class="md-delete"></i></button>'
									]).draw()
											.nodes()
											.to$()
											.addClass('').find('td').eq(2).addClass('formato_mensaje');
								}
							}
						}
					}

			}
			else
			{
				swal("No hay resultados!", "Introduzca otro criterio de búsqueda...", "warning");
				tabla_listado.clear().draw();
			}

			//****************************************************
			//*
			//****************************************************


		}, 'json');

    }

 });


});


</script>