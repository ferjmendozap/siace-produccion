
<section class="style-default-light">&nbsp;

<!--CUERPO DEL FORMULARIO-->
 <form id="formAjax2" class="form form-validation" role="form" novalidate="novalidate" >
    <div class="row">

    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-group" >
                <input style="text-transform: uppercase" maxlength="150" value="{$nombreMostrarArchivo}"  type="text" name="nombre_nuevo_archivo" id="nombre_nuevo_archivo" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNombreCarpeta(event)" >
                <label for="nombre_nuevo_archivo">Nombre del archivo</label>
                <p class="help-block">Solo se admiten hasta 150 caracteres</p>
            </div>
        </div>
    </div>
   <div class="row">&nbsp;</div>
    <div class="row">
         <div class="col-sm-12" align="right">
             <button id="botonCerrar" class="btn btn-sm btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
             <button id="botonRenombrarArchivo" class="btn btn-sm btn-primary ink-reaction btn-raised logsUsuarioModal" type="submit">Aceptar</button>
         </div>
     </div>
 </form>
<!--FIN DEL TITULO DEL FORMULARIO-->
</section>
<script>

function validarNombreCarpeta(evt){

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
       return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9\A-Z\a-z\-\_\á\é\í\ó\ú\ñ\Ñ\š\ü ]/.test(str)) {
            return false;
    };
}

$(document).ready(function() {


    $("#formAjax2").validate({

        submitHandler: function(form) {

            var urlRenombrarArchivo = '{$_Parametros.url}modIN/documentosCONTROL/CambiarNombreArchivoMET';
            var nombreAntesArchivo = '{$nombreAntesArchivo}';
            var nombreCarpeta = '{$nombreCarpeta}';
            var pkCarpeta = '{$pkCarpeta}';
            var pkArchivo = '{$pkArchivo}';
            var formatoDocumento = '{$formatoDocumento}';
            var nombreNuevoArchivo = $('#nombre_nuevo_archivo').val();

            $.post(urlRenombrarArchivo, {
                nombreAntesArchivo: nombreAntesArchivo,
                nombreCarpeta: nombreCarpeta,
                pkCarpeta: pkCarpeta,
                pkArchivo: pkArchivo,
                formatoDocumento: formatoDocumento,
                nombreNuevoArchivo: nombreNuevoArchivo
                }, function (resp) {

                if(resp != -1 && resp != -2 && resp != 0)
                {

                    swal({
                        title:'Archivo Renombrado',
                        text: 'El Archivo se renombró exitosamente',
                        type: "success",
                        closeOnConfirm: true
                    }, function(){
                        $("#datatable1 tr td[id_nom_archivo=" + pkArchivo + "]").html(resp);
                        var dependencia = $('select[id="ind_dependencia"] option:selected').html();
                        accionNodo(pkCarpeta, dependencia, nombreCarpeta);
                        $(document.getElementById('cerrarModal2')).click();

                    });

                }
                else
                {
                    if(resp == 0)
                    {
                        swal({
                            title:'El nombre ya existe',
                            text: 'Suministre otro nombre para el Archivo',
                            type: "warning",
                            closeOnConfirm: true
                        } );
                    }
                    else
                    {
                        alert('Error: '+resp);
                    }
                }
            });
        }
    });

});

</script>