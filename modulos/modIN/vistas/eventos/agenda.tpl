
<link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Css}Intranet.css" />
<section class="style-default-light">

<!--TITULO DEL FORMULARIO-->
<div class="row">
	<div class="col-lg-12">
		<div class="card-head"><h2 class="text-primary">Gestionar Eventos</h2></div>
	</div>
</div>&nbsp;

<!--CUERPO DEL DOCUMENTO -->

<div class="row">
    
    <div class="col-lg-12">
    	<div class="card">
    		<div class="card-head style-default" style="margin-bottom: 10px !important;">
    			<header>
					<span class="">Agenda</span> &nbsp;<small class="selected-date">&nbsp;</small>
					{if in_array('IN-01-02-01-01-N',$_Parametros.perfil)}
					<span>&nbsp;<button id="nuevoEventoBtn" class="btn ink-reaction btn-raised btn-sm btn-info" type="button" data-target="#formModal" data-toggle="modal" ><i class="md md-create"></i> Nuevo Evento</button></span>
					{/if}
				</header>
    		</div>
    		<div class="card-body no-padding" style="height: 80em; ">
    			<div class="container">	
	    			<div class="custom-calendar-wrap custom-calendar-full">
	    				<div class="custom-header clearfix">
						<div style="font-size: 20px; font-weight: bold; color:#478CC8" >
							<span id="custom-month" style="" class="custom-month"></span>
							<span id="custom-year" class="custom-year"></span>&nbsp;
						</div>
						<h3 class="custom-month-year">

							<nav>
								<span id="custom-prev-anio" class="custom-prev-anio" title="Año Anterior"></span>
								<span id="custom-prev" class="custom-prev" title="Mes Anterior"></span>
								<span id="custom-next" class="custom-next" title="Mes Siguiente"></span>
								<span id="custom-next-anio" class="custom-next-anio" title="Año Siguiente"></span>
								<span id="custom-current" class="custom-current" title="Ir a Dia Actual"></span>
							</nav>
						</h3>
						</div>
	    				<div id='calendar' class="fc-calendar-container"  ></div>
	    			</div>
    			</div>
    		</div>
    	</div>
    </div>

</div>

</section>

<div class="modal " id="formModalEventos"  role="dialog" aria-labelledby="formModalLabel" >
            <div class="modal-dialog modal-lg" id="modalAnchoEventos" style="">
                <div class="modal-content " id="contenidoModalEventos" >
                    <div class="modal-header" id="headerModalEventos">
                        <button type="button" class="close" id="cerrarModalEventos" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 id="formModalLabelEventos">s</h4>
                    </div>
                        <div cla  ="modal-body" id="ContenidoModalEventos">
							<table id="datatable1" class="table table-striped table-hover" WIDTH="100%" >
								<thead>
								<tr  align="center">
									<th ><i class=""></i>N°</th>
									<th ><i class="md-today"></i>Fecha Evento</th>
									<th ><i class="md-event-note"></i>Eventos</th>
									<th ><i class="md-query-builder"></i>Hora inicio</th>
									<th ><i class="md-query-builder"></i>Hora final</th>
									<th ><i class="md-query-builder"></i>Anual</th>
									<th ><i class=""></i>Editar</th>
									<th ><i class=""></i>Eliminar</th>
									<th ><i class=""></i>Visualizar</th>
								</tr>
								</thead>
								<tbody >
								<tr class="fila_agregar" >
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								</tbody>
							</table>
                        </div>
                </div>
           </div>
</div>
<div id="fondoFormModalEventos" class="fondoForModalEventos oculto_ventana_evento"  tabindex="-1"  ></div>


<script>

var codropsEventsAnual = {$cad_array_eventosAnual};
var codropsEvents = {$cad_array_eventos};


$(document).ready(function() {
		
$('#modalAnchoEventos').css( 'width', '80%' );
$('#ContenidoModalEventos').css( 'padding', '20px' );
$('#formModalLabelEventos').html("Administrador de Eventos");

// =========================================================================
// CALENDAR
// =========================================================================

var cal = $( '#calendar' ).calendario( {
		onDayClick : function( $el, $contentEl, dateProperties ) {

			for( var key in dateProperties )
			{
				console.log( key + ' = ' + dateProperties[ key ] );
			}

			d = dateProperties['day'];
			m = dateProperties['month'];
			a = dateProperties['year'];

			var fecha = a+'-'+m+'-'+d;

			var urlEventos = '{$_Parametros.url}modIN/eventosCONTROL/BuscarListadoEventosMET';
			$.post(urlEventos, { fecha: fecha }, function(dato){

				if(dato != 0)//HAY EVENTOS PARA LA FECHA INDICADA
				{


					//$('#ContenidoModalEventos').html('');
					//$('#ContenidoModalEventos').html(dato);
					var tabla_listado = $('#datatable1').DataTable();//se inicializa la tabla cargada previamente en ContenidoModalEventos
					var contador = 0;
					tabla_listado.clear().draw();
					var cad_modificar_evento = '';
					var cad_eliminar_evento = '';
					for(var i=0; i<dato.length; i++)
					{
						{if in_array('IN-01-02-01-02-M',$_Parametros.perfil)}
							cad_modificar_evento = '<button type="button"  pk_num_evento = "'+dato[i].pk_num_evento+'" class="editarEvento gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="fa fa-edit"></i></button>';
						{else}
							cad_modificar_evento = '<i class="md-not-interested"></i>';
						{/if}

						{if in_array('IN-01-02-01-03-E',$_Parametros.perfil)}
						cad_eliminar_evento = '<button titulo="" type="button"  pk_num_evento = "'+dato[i].pk_num_evento+'" class="eliminarEvento gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-target="#formModal" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md-delete"></i></button>';
						{else}
						cad_eliminar_evento = '<i class="md-not-interested"></i>';
						{/if}

						contador = i+1;

                        var cad_anual = '';
						if(dato[i].ind_anual == 'Y')
                        {
                            cad_anual = '<i class="md-done"></i>';
						}
						else
						{
                            cad_anual = '<i class="md-not-interested "></i>';
						}

						tabla_listado.row.add([
							contador,
							dato[i].fec_registro,
							dato[i].ind_descripcion,
							dato[i].hora_inicio+' '+dato[i].horario_inicio,
							dato[i].hora_final+' '+dato[i].horario_final,
                            cad_anual,
							cad_modificar_evento,
							cad_eliminar_evento,
							'<button titulo="" type="button"  pk_num_evento = "'+dato[i].pk_num_evento+'" class="visualizarEvento gsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-target="#formModal" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md md-search"></i></button>'
						]).draw()
								.nodes()
								.to$()
								.addClass( '' );
					}

					//SE MUESTRA LA MODAL
					$('#formModalEventos').fadeIn(function() {
						window.setTimeout(function(){
							$('#formModalEventos').addClass('visible_ventana_evento');
						}, 100);

						$('#fondoFormModalEventos').removeClass('oculto_ventana_evento');
						$('#fondoFormModalEventos').addClass('visible_ventana_evento');

					});
					//================================================================
					// EDITAR EVENTO LISTADO  (SE DEBE CARGAR DESPUES DE CARGAR LOS ELEMENTOS A LOS CUALES HACE REFERENCIA)
					//================================================================
					$('#datatable1 tbody').on('click','.editarEvento', function(){
						
				            var pk_num_evento = $(this).attr('pk_num_evento');

				                var urlEditar = '{$_Parametros.url}modIN/eventosCONTROL/ModificarEventoMET';
				                $('#modalAncho').css("width", "75%");
				                $('#formModalLabel').html("Modificar Evento");
				                $('#ContenidoModal').html("");

				                $.post(urlEditar, { pk_num_evento: pk_num_evento }, function(dato){
				                    $('#ContenidoModal').html(dato);
				            });

					});
					//================================================================
					// ELIMINAR EVENTO
					//================================================================
					$('#datatable1 tbody').on('click','.eliminarEvento', function(){

						var pk_num_evento = $(this).attr('pk_num_evento');

						var urlEliminar = '{$_Parametros.url}modIN/eventosCONTROL/EliminarVisualizarEventoMET';
						$('#modalAncho').css("width", "90%");
						$('#formModalLabel').html("Eliminar Evento");
						$('#ContenidoModal').html("");
						var band = 1;
						$.post(urlEliminar, { pk_num_evento: pk_num_evento, band: band }, function(dato){
							$('#ContenidoModal').html(dato);
						});

					});
					//================================================================
					// VISUALIZAR EVENTO
					//================================================================
					$('#datatable1 tbody').on('click','.visualizarEvento', function(){

						var pk_num_evento = $(this).attr('pk_num_evento');

						var urlEliminar = '{$_Parametros.url}modIN/eventosCONTROL/EliminarVisualizarEventoMET';
						$('#modalAncho').css("width", "90%");
						$('#formModalLabel').html("Visualizar Evento");
						$('#ContenidoModal').html("");
						var band = 2;
						$.post(urlEliminar, { pk_num_evento: pk_num_evento, band: band }, function(dato){
							$('#ContenidoModal').html(dato);
						});

					});
					//================================================================

				}


			},'json');


								
		},
		caldata :codropsEvents,
        caldataAnual: codropsEventsAnual
	} ),
	$month = $( '#custom-month' ).html( cal.getMonthName() ),
	$year = $( '#custom-year' ).html( cal.getYear() );

	$( '#custom-next' ).on( 'click', function() {
	cal.gotoNextMonth( updateMonthYear );
	} );
	$( '#custom-prev' ).on( 'click', function() {
		cal.gotoPreviousMonth( updateMonthYear );
	} );
	$( '#custom-current' ).on( 'click', function() {
		cal.gotoNow( updateMonthYear );
	} );
	$( '#custom-prev-anio' ).on( 'click', function() {
		cal.gotoPreviousYear( updateMonthYear );
	} );				
	$( '#custom-next-anio' ).on( 'click', function() {
		cal.gotoNextYear( updateMonthYear );
	} );				
	$( '#custom-current' ).on( 'click', function() {
		cal.gotoNow( updateMonthYear );
	} );

	function updateMonthYear() {
		$month.html( cal.getMonthName() );
		$year.html( cal.getYear() );
	}



//===========================================================================	
//CERRAR LA MODAL
//===========================================================================
$("#cerrarModalEventos").click(function() {
    $('#formModalEventos').fadeOut().end().find('#formModalEventos').addClass('oculto_ventana_evento');
    $('#fondoFormModalEventos').removeClass('visible_ventana_evento');
    $('#fondoFormModalEventos').addClass('oculto_ventana_evento');
	$.post('{$_Parametros.url}modIN/eventosCONTROL/',{  },function(dato){
	 $('#_contenido').html(dato);
	 });
            
});
//============================================================================
//ABRIR MODAL NUEVO EVENTO
//==========================================================================
$("#nuevoEventoBtn").click(function() {
	$('#modalAncho').css("width", "80%");
	$('#formModalLabel').html("Registrar Nuevo Evento");
	$('#ContenidoModal').html("");

	var urlEventos = '{$_Parametros.url}modIN/eventosCONTROL/RegistrarEventosMET';
	$.post(urlEventos, '', function(dato){
		$('#ContenidoModal').html(dato);
	});

});
//===================================================================================


		
});//READY

</script>

