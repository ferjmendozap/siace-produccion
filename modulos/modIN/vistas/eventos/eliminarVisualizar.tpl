<section class="style-default-light">&nbsp;
<!--CUERPO DEL DOCUMENTO -->
<div class="row">
    
    <div class="col-lg-12">
    	<div class="card">
			<div class="card-body ">
				<table class="table table-striped no-margin">
					<thead>
					<tr>
						<th ><i class="md-today"></i>Fecha</th>
						<th ><i class="md-event-note"></i>Descripción</th>
						<th ><i class="md-query-builder"></i>Hr. Inicio</th>
						<th ><i class="md-query-builder"></i>Hr. Final</th>
						<th ><i class="md-room "></i>Lugar</th>
						<th ><i class=""></i>Anual</th> <!-- md-cached -->
						<th>&nbsp;</th>
						<th ><i class="md-insert-photo"></i>Imagen</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>{$arrayDatosEvento["fec_registro"]}</td>
						<td>{$arrayDatosEvento["ind_descripcion"]}</td>
						<td>{$arrayDatosEvento["hora_inicio"]} {$arrayDatosEvento["horario_inicio"]}</td>
						<td>{$arrayDatosEvento["hora_final"]} {$arrayDatosEvento["horario_final"]}</td>
						<td>{$arrayDatosEvento["ind_lugar"]}</td>
						<td>{$arrayDatosEvento["ind_anual"]}</td>
						<td>&nbsp;</td>
						<td ><img align="center" class="img-circle width-0 " style="width: 90px; height: 90px;" src="{$_Parametros.ruta_Img}modIN/eventos/{$arrayDatosEvento["ind_ruta_img"]}" alt="" ></td>
					</tr>
					</tbody>
				</table>
			</div>
    	</div>
    </div>

</div>
{if $tipoFormulario == 1}
<div class="row" >
	<div class="col-lg-12">
		<div align="center">
			<button id="botonCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
			<button id="botonEliminarEvento" pk_num_evento="{$arrayDatosEvento["pk_num_evento"]}" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" type="button">Eliminar</button>
		</div>
	</div>
</div>
{/if}
</section>

<script>

	$(document).ready(function() {

		//*********************************************
		//*			ELIMINAR EVENTOS
		//*********************************************
		$("#botonEliminarEvento").click(function() {

			var pk_num_evento = $(this).attr('pk_num_evento');

			swal({
				title:'Estas Seguro?',
				text: 'Se Eliminará el Evento!!!',
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "si, Eliminar",
				closeOnConfirm: false
			}, function(){

				var url_eliminar = '{$_Parametros.url}modIN/eventosCONTROL/QuitarEventoMET';

				$.post(url_eliminar, { pk_num_evento :  pk_num_evento },function(dato){

					if(dato == 1)
					{
						swal({
							title:'Eliminado!',
							text: 'Evento eliminado',
							type: "success"
						},  function () {

							var fecha2 = '{$arrayDatosEvento["fec_registro"]}';

							fecha2 = fecha2.split('/');
							fecha2 = fecha2[2]+'-'+fecha2[1]+'-'+fecha2[0];
							$.post('{$_Parametros.url}modIN/eventosCONTROL/BuscarListadoEventosMET', { fecha: fecha2 }, function(dato){

								var tabla_listado = $('#datatable1').DataTable();
								tabla_listado.clear().draw();
								var contador =0;
								for(var i=0; i<dato.length; i++)
								{

									contador = i+1;

									tabla_listado.row.add([
										contador,
										dato[i].fec_registro,
										dato[i].ind_descripcion,
										dato[i].hora_inicio,
										dato[i].hora_final,
										'<button type="button"  pk_num_evento = "'+dato[i].pk_num_evento+'" class="editarEvento gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" ><i class="fa fa-edit"></i></button>',
										'<button titulo="" type="button"  pk_num_evento = "'+dato[i].pk_num_evento+'" class="eliminarEvento gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-target="#formModal" data-toggle="modal" ><i class="md-delete"></i></button>',
										'<button titulo="" type="button"  pk_num_evento = "'+dato[i].pk_num_evento+'" class="visualizarEvento gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" ><i class="md md-remove-red-eye"></i></button>'
									]).draw()
											.nodes()
											.to$()
											.addClass( '' );
								}

								$(document.getElementById('cerrarModal')).click();
								$('#formModalLabel').html("");
								$('#ContenidoModal').html("");

							}, 'json');

						});
					}
					else
					{
						alert('Error: '+dato);
					}

				});

			});

		});
		//*********************************************
		//*
		//*********************************************

	});

</script>
