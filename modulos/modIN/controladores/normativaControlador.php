<?php 
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: INTRANET
 * PROCESO: GESTIONAR NORMATIVA LEGAL
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1  |         Maikol Isava                                  |    maikol.isava@gmail.com      |          0426-1814058          |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        19 -07-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 class normativaControlador extends Controlador
{
    private $atModeloNormativa;
    private $atIdUsuario;
    private $atModeloDocumentos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atIdUsuario = Session::metObtener('idUsuario');
		$this->atModeloNormativa = $this->metCargarModelo('normativa');
        $this->atModeloDocumentos = $this->metCargarModelo('documentos');

    }

    
    public function metIndex()
    {

        $complementosCss = array(

            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array(
            'materialSiace/App',
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'
        );


		
		$this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
		
		$datosOrganismo  = $this->atModeloNormativa->metConsultarOrganismos();
        $datosDependencias  = $this->atModeloNormativa->metConsultarDepencias();
    	
		$this->atVista->assign('datosOrganismo', $datosOrganismo);
        $this->atVista->assign('datosDependencias', $datosDependencias);

		$this->atVista->metRenderizar('registrar');
		
	}

    
    public function  metRegistrar()
    {
        $ind_tipo_normativa_aux = $_POST['ind_tipo_normativa'];

        $ind_tipo_normativa = $ind_tipo_normativa_aux;

        if($ind_tipo_normativa_aux == 'R' || $ind_tipo_normativa_aux == 'M' )
        {
            $ind_tipo_normativa_aux = 'RM';
        }

        switch ($ind_tipo_normativa_aux)
        {
            case "L" :
                $num_gaceta_otro = $this->metObtenerTexto('num_gaceta_ley');
                $fec_gaceta_otro = $_POST['fec_publicacion_gaceta_ley'];
                $ind_descripcion = $this->metObtenerTexto('ind_descripcion_ley');
                $ind_tipo_ley_otro = $this->metObtenerTexto('ind_tipo_ley');
                $ind_dependencia_circular = null;
                $num_resolucion = null;
                $fec_resolucion = null;

                $aux = trim($fec_gaceta_otro);
                $aux2 = explode('-', $aux);
                $fec_gaceta_otro = $aux2[2]."-".$aux2[1]."-".$aux2[0];


            break;
            case "N" :
                $num_gaceta_otro = $this->metObtenerTexto('num_gaceta_normativa');
                $fec_gaceta_otro = $_POST['fec_publicacion_gaceta_normativa'];
                $ind_descripcion = $this->metObtenerTexto('ind_descripcion_normativa');
                $ind_tipo_ley_otro = null;
                $ind_dependencia_circular = null;
                $num_resolucion = $this->metObtenerTexto('num_resolucion_normativa');
                $fec_resolucion = $_POST['fec_publicacion_resolucion_normativa'];

                $aux = trim($fec_gaceta_otro);
                $aux2 = explode('-', $aux);
                $fec_gaceta_otro = $aux2[2]."-".$aux2[1]."-".$aux2[0];

                $aux = trim($fec_resolucion);
                $aux2 = explode('-', $aux);
                $fec_resolucion = $aux2[2]."-".$aux2[1]."-".$aux2[0];

            break;
            case "C" :

                $num_gaceta_otro = $this->metObtenerTexto('num_circular');
                $fec_gaceta_otro = $_POST['fec_publicacion_circular'];
                $ind_descripcion = $this->metObtenerTexto('ind_descripcion_circular');

                $ind_tipo_ley_otro = $this->metObtenerTexto('ind_tipo_circular');
                if(strcmp($ind_tipo_ley_otro,'INT' )==0)
                {
                    $ind_dependencia_circular = $this->metObtenerInt('ind_dependencia_circular');
                }
                else//CGR
                {
                    $ind_dependencia_circular = null;
                }

                $num_resolucion = null;
                $fec_resolucion = null;

                $aux = trim($fec_gaceta_otro);
                $aux2 = explode('-', $aux);
                $fec_gaceta_otro = $aux2[2]."-".$aux2[1]."-".$aux2[0];

            break;
            case "RM" :
                $num_gaceta_otro = $this->metObtenerTexto('num_gaceta_resolucion_manuales');
                $fec_gaceta_otro = $_POST['fec_publicacion_gaceta_resolucion_manuales'];
                $ind_descripcion = $this->metObtenerTexto('ind_descripcion_resolucion_manuales');
                $ind_dependencia_circular = null;
                $ind_tipo_ley_otro = null;
                $num_resolucion = $this->metObtenerTexto('num_resolucion_resolucion_manuales');
                $fec_resolucion = $_POST['fec_publicacion_resolucion_resolucion_manuales'];

                $aux = trim($fec_gaceta_otro);
                $aux2 = explode('-', $aux);
                $fec_gaceta_otro = $aux2[2]."-".$aux2[1]."-".$aux2[0];

                $aux = trim($fec_resolucion);
                $aux2 = explode('-', $aux);
                $fec_resolucion = $aux2[2]."-".$aux2[1]."-".$aux2[0];

            break;
            default:
                echo "Error switch: ".$ind_tipo_normativa;
                exit;

        }

        $cod_img= "normativa_".mt_rand(0,999);
        $destino =  "publico/imagenes/modIN/normativa/";

        if(isset($_FILES["ind_ruta_pdf"]))
        {
            $aux = explode('.',$_FILES["ind_ruta_pdf"]["name"]);
            $ind_formato = $aux[1];
            $aux2 = $this->atModeloDocumentos->sanear_string(mb_strtoupper($aux[0]));

            $pdf_f   = $cod_img."_".$aux2.".".$ind_formato;
            $ruta    = $destino.$pdf_f;
            //===========
            if (!file_exists($ruta))//False si no existe
            {
                $resultado = move_uploaded_file($_FILES["ind_ruta_pdf"]["tmp_name"], $ruta);
                chmod($ruta, 0777);
                if ($resultado)
                {
                   //Registrar la noticia
                   $ind_ruta_pdf = $pdf_f;

                    $fecha_ultima_modificacion = date('Y-m-d');

                   $resp  = $this->atModeloNormativa->metInsertarNormativaLegal($num_gaceta_otro, $ind_tipo_normativa, $fec_gaceta_otro, mb_strtoupper($ind_descripcion), $ind_tipo_ley_otro,$ind_dependencia_circular, mb_strtoupper($num_resolucion), $fec_resolucion, $ind_ruta_pdf, $this->atIdUsuario, $fecha_ultima_modificacion);
                   if($resp)
                   {
                     echo 1;
                   }
                   else
                   {
                     echo $resp;
                   } 
                   
                } 
                else
                {
                    echo -2;//ocurrio un error al mover el archivo
                }
             }
             else
             {
                echo -4;//el nombre ya existe no se registra la normativa
             }
        }
        else
        {
            echo -5;//error al cargar el input file
        }

                  
       
    }

    public function metListarNormativa()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',

        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array(
            'materialSiace/App',
            //'materialSiace/core/demo/DemoTableDynamic',
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
            'modIN/printThis'

        );
        
        
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $datosNormativas  = $this->atModeloNormativa->metListarNormativa('T');

        for($i=0; $i<count($datosNormativas); $i++)
        {
            switch ($datosNormativas[$i]['ind_tipo_normativa'])
            {

                case "L" :
                    $datosNormativas[$i]['ind_tipo_normativa']= "Ley";
                    $datosNormativas[$i]['ind_resolucion']= "---";
                    $datosNormativas[$i]['fec_resolucion']= "---";
                    if($datosNormativas[$i]['ind_tipo_ley_circular'] == 'NAC')
                    {
                        $datosNormativas[$i]['ind_tipo_ley_circular'] = 'Nacional';
                    }
                    else
                    {
                        $datosNormativas[$i]['ind_tipo_ley_circular'] = 'Estadal';
                    }
                    $datosNormativas[$i]['fk_a004_num_dependencia']= "---";
                    break;
                case "N":
                    $datosNormativas[$i]['ind_tipo_normativa']= "Normativa";
                    $datosNormativas[$i]['ind_tipo_ley_circular'] = '---';
                    $datosNormativas[$i]['fk_a004_num_dependencia']= "---";
                    break;
                case "C":
                    $datosNormativas[$i]['ind_tipo_normativa']= "Circular";
                    if( $datosNormativas[$i]['ind_tipo_ley_circular'] == 'INT')
                    {
                        $datosNormativas[$i]['ind_tipo_ley_circular'] = 'Interno';
                    }

                    $datosNormativas[$i]['ind_resolucion']= "---";
                    $datosNormativas[$i]['fec_resolucion']= "---";

                    break;
                case "R":
                    $datosNormativas[$i]['ind_tipo_normativa']= "Resolución";
                    $datosNormativas[$i]['ind_tipo_ley_circular'] = '---';
                    $datosNormativas[$i]['fk_a004_num_dependencia']= "---";
                    break;
                case "M":
                    $datosNormativas[$i]['ind_tipo_normativa']= "Manual";
                    $datosNormativas[$i]['ind_tipo_ley_circular'] = '---';
                    $datosNormativas[$i]['fk_a004_num_dependencia']= "---";
                    break;
                default;
                    $datosNormativas[$i]['ind_tipo_normativa']= 'error';
            }
        }

        $this->atVista->assign('datosNormativas', $datosNormativas);
        $this->atVista->assign('tablas',2 );

        if(isset($_POST['origenFormNorm']))
        {
            $this->atVista->assign('origenFormNorm', $_POST['origenFormNorm']);
        }
        else
        {
            $this->atVista->assign('origenFormNorm', 'L');
        }

        $this->atVista->metRenderizar('listarNormativa');

        
    }


    public function metBuscarNormativaFiltro()
    {

        $ind_tipo_normativa = $_POST['ind_tipo_normativa'];
        if(isset($_POST['radio_filtro']))
        {

            $radio_filtro = $_POST['radio_filtro'];

            if ($ind_tipo_normativa == 'N' || $ind_tipo_normativa == 'R' || $ind_tipo_normativa == 'M') {
                $opc = "NRM";
            } else {
                $opc = $ind_tipo_normativa;
            }

            switch ($opc) {

                case "L" :

                    if ($radio_filtro == '1')//SE MARCARON LAS FECHAS
                    {
                        $start_fecha_gaceta = $_POST['start_fecha_gaceta'];
                        $start_fecha_gaceta = $this->atModeloNormativa->formatFechaAMD($start_fecha_gaceta);
                        $end_fecha_gaceta = $_POST['end_fecha_gaceta'];
                        $end_fecha_gaceta = $this->atModeloNormativa->formatFechaAMD($end_fecha_gaceta);
                        $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(1, $start_fecha_gaceta, $end_fecha_gaceta, '', $ind_tipo_normativa);
                        if ($listadoNormativa)
                        {
                            echo json_encode($listadoNormativa);
                        }
                        else {

                            echo -1;//error
                        }

                    }
                    else
                    {
                        if ($radio_filtro == '3')
                        {
                            $num_tipo_normativa = $this->metObtenerTexto('num_tipo_normativa');
                            $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(3, '', '', $num_tipo_normativa, $ind_tipo_normativa);
                            if ($listadoNormativa)
                            {
                                echo json_encode($listadoNormativa);
                            }
                            else
                            {
                                echo -3;//error
                            }
                        }
                        else
                        {
                            echo -2;
                        }
                    }

                    break;

                case "NRM":
                    if ($radio_filtro == '1')//FECHAS GACETA
                    {
                        $start_fecha_gaceta = $_POST['start_fecha_gaceta'];
                        $start_fecha_gaceta = $this->atModeloNormativa->formatFechaAMD($start_fecha_gaceta);
                        $end_fecha_gaceta = $_POST['end_fecha_gaceta'];
                        $end_fecha_gaceta = $this->atModeloNormativa->formatFechaAMD($end_fecha_gaceta);
                        $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(1, $start_fecha_gaceta, $end_fecha_gaceta, '', $ind_tipo_normativa);
                        if ($listadoNormativa)
                        {
                            echo json_encode($listadoNormativa);
                        }
                        else
                        {
                            echo -1;//error
                        }

                    } else {
                        if ($radio_filtro == '2')
                        {
                            $start_fecha_resolucion = $_POST['start_fecha_resolucion'];
                            $start_fecha_resolucion = $this->atModeloNormativa->formatFechaAMD($start_fecha_resolucion);
                            $end_fecha_resolucion = $_POST['end_fecha_resolucion'];
                            $end_fecha_resolucion = $this->atModeloNormativa->formatFechaAMD($end_fecha_resolucion);
                            $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(2, $start_fecha_resolucion, $end_fecha_resolucion, '', $ind_tipo_normativa);
                            if ($listadoNormativa) {
                                echo json_encode($listadoNormativa);
                            } else {
                                echo -1;//error
                            }
                        }
                        else
                        {
                            if ($radio_filtro == '3')
                            {
                                $num_tipo_normativa = $this->metObtenerTexto('num_tipo_normativa');
                                $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(3, '', '', $num_tipo_normativa, $ind_tipo_normativa);
                                if ($listadoNormativa) {
                                    echo json_encode($listadoNormativa);
                                } else {
                                    echo -3;//error
                                }
                            } else {
                                echo -4;
                            }

                        }
                    }
                    break;

                case "C":

                    if ($radio_filtro == '4')//FECHAS GACETA
                    {
                        $start_fecha_circular = $_POST['start_fecha_circular'];
                        $start_fecha_circular = $this->atModeloNormativa->formatFechaAMD($start_fecha_circular);
                        $end_fecha_circular = $_POST['end_fecha_circular'];
                        $end_fecha_circular = $this->atModeloNormativa->formatFechaAMD($end_fecha_circular);
                        $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(4, $start_fecha_circular, $end_fecha_circular, '', $ind_tipo_normativa);
                        if ($listadoNormativa)
                        {
                            $datosDependencias  = $this->atModeloNormativa->metConsultarDepencias();
                            for($j=0; $j<count($datosDependencias); $j++)
                            {
                                for ($i = 0; $i < count($listadoNormativa); $i++) {
                                    if ( $datosDependencias[$j]['pk_num_dependencia'] == $listadoNormativa[$i]['fk_a004_num_dependencia'])
                                    {
                                        $listadoNormativa[$i]['fk_a004_num_dependencia'] = $datosDependencias[$j]['ind_dependencia'];
                                    }
                                }
                            }
                            echo json_encode($listadoNormativa);

                        } else {
                            echo -1;//error
                        }

                    } else {
                        if ($radio_filtro == '3') {
                            $num_tipo_normativa = $this->metObtenerTexto('num_tipo_normativa');
                            $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(3, '', '', $num_tipo_normativa, $ind_tipo_normativa);
                            if ($listadoNormativa)
                            {
                                $datosDependencias  = $this->atModeloNormativa->metConsultarDepencias();
                                for($j=0; $j<count($datosDependencias); $j++)
                                {
                                    for ($i = 0; $i < count($listadoNormativa); $i++) {
                                        if ( $datosDependencias[$j]['pk_num_dependencia'] == $listadoNormativa[$i]['fk_a004_num_dependencia'])
                                        {
                                            $listadoNormativa[$i]['fk_a004_num_dependencia'] = $datosDependencias[$j]['ind_dependencia'];
                                        }
                                    }
                                }

                                echo json_encode($listadoNormativa);

                            }
                            else
                            {
                                echo -3;//error
                            }
                        } else {
                            echo -4;
                        }
                    }

                    break;
                case "T":
                    if ($radio_filtro == '1')//FECHAS GACETA
                    {
                        $start_fecha_gaceta = $_POST['start_fecha_gaceta'];

                        $start_fecha_gaceta = $this->atModeloNormativa->formatFechaAMD($start_fecha_gaceta);
                        $end_fecha_gaceta = $_POST['end_fecha_gaceta'];
                        $end_fecha_gaceta = $this->atModeloNormativa->formatFechaAMD($end_fecha_gaceta);
                        $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(1, $start_fecha_gaceta, $end_fecha_gaceta, '', $ind_tipo_normativa);
                        if ($listadoNormativa) {
                            echo json_encode($listadoNormativa);
                        } else {
                            echo -1;//error
                        }

                    } else {
                        if ($radio_filtro == '2') {
                            $start_fecha_resolucion = $_POST['start_fecha_resolucion'];
                            $start_fecha_resolucion = $this->atModeloNormativa->formatFechaAMD($start_fecha_resolucion);
                            $end_fecha_resolucion = $_POST['end_fecha_resolucion'];
                            $end_fecha_resolucion = $this->atModeloNormativa->formatFechaAMD($end_fecha_resolucion);
                            $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(2, $start_fecha_resolucion, $end_fecha_resolucion, '', $ind_tipo_normativa);
                            if ($listadoNormativa) {
                                echo json_encode($listadoNormativa);
                            } else {
                                echo -1;//error
                            }
                        } else {
                            if ($radio_filtro == '3')
                            {
                                $num_tipo_normativa = $this->metObtenerTexto('num_tipo_normativa');
                                $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(3, '', '', $num_tipo_normativa, $ind_tipo_normativa);
                                if ($listadoNormativa)
                                {
                                    echo json_encode($listadoNormativa);
                                }
                                else
                                {
                                    echo -1;
                                }
                            }
                            else
                            {
                                if ($radio_filtro == '4')
                                {
                                    $start_fecha_circular = $_POST['start_fecha_circular'];
                                    $start_fecha_circular = $this->atModeloNormativa->formatFechaAMD($start_fecha_circular);
                                    $end_fecha_circular = $_POST['end_fecha_circular'];
                                    $end_fecha_circular = $this->atModeloNormativa->formatFechaAMD($end_fecha_circular);
                                    $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro(4, $start_fecha_circular, $end_fecha_circular, '', $ind_tipo_normativa);
                                    if ($listadoNormativa)
                                    {
                                        echo json_encode($listadoNormativa);
                                    }
                                    else
                                    {
                                        echo -1;//error
                                    }

                                }
                                else
                                {
                                    echo -5;
                                }
                            }

                        }
                    }
                    break;

                default;
                    echo 'error';
            }
        }
        else
        {
            $listadoNormativa = $this->atModeloNormativa->metBuscarNormativaFiltro('', '', '', '', $ind_tipo_normativa);
            if ($listadoNormativa)
            {
                if($ind_tipo_normativa == 'C')
                {
                    $datosDependencias  = $this->atModeloNormativa->metConsultarDepencias();
                    for($j=0; $j<count($datosDependencias); $j++)
                    {
                        for ($i = 0; $i < count($listadoNormativa); $i++)
                        {
                            if ( $datosDependencias[$j]['pk_num_dependencia'] == $listadoNormativa[$i]['fk_a004_num_dependencia'])
                            {
                                $listadoNormativa[$i]['fk_a004_num_dependencia'] = $datosDependencias[$j]['ind_dependencia'];
                            }
                        }
                    }
                }

                echo json_encode($listadoNormativa);
            }
            else
            {
                echo -100;//error
            }
        }




    }

    public function metEditarNormativa()
    {
        $complementosCss = array(

            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array(
            'materialSiace/App',
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'
        );



        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $datosOrganismo  = $this->atModeloNormativa->metConsultarOrganismos();
        $datosDependencias  = $this->atModeloNormativa->metConsultarDepencias();

        $this->atVista->assign('datosOrganismo', $datosOrganismo);
        $this->atVista->assign('datosDependencias', $datosDependencias);

        $pk_num_normativa_legal  = $_POST['pk_num_normativa_legal'];
        $datosNormativas  = $this->atModeloNormativa->metBusquedaUnicaNormativa($pk_num_normativa_legal);


        $this->atVista->assign('datosNormativas', $datosNormativas );
        $this->atVista->metRenderizar('editar', 'modales');


    }

    public function metModificarNormativa()
    {

        $ind_tipo_normativa_aux = $_POST['ind_tipo_normativa_edit'];

        if($ind_tipo_normativa_aux == 'R' || $ind_tipo_normativa_aux == 'M' )
        {
            $ind_tipo_normativa_aux = 'RM';
        }
	
	    $band = 0;

        switch ($ind_tipo_normativa_aux)
        {
            case "L" :
                $num_gaceta_otro = $_POST['num_gaceta_ley_edt'];
                $fec_gaceta_otro = $_POST['fec_publicacion_gaceta_ley_edt'];
                $ind_descripcion = $_POST['ind_descripcion_ley_edt'];
                $ind_tipo_ley_otro = $_POST['ind_tipo_ley_edt'];
                $ind_dependencia_circular = null;
                $num_resolucion = null;
                $fec_resolucion = null;

                $aux = trim($fec_gaceta_otro);
                $aux2 = explode('-', $aux);
                $fec_gaceta_otro = $aux2[2]."-".$aux2[1]."-".$aux2[0];


                break;
            case "N" :
                $num_gaceta_otro = $this->metObtenerTexto('num_gaceta_normativa_edt');
                $fec_gaceta_otro = $_POST['fec_publicacion_gaceta_normativa_edt'];
                $ind_descripcion = $this->metObtenerTexto('ind_descripcion_normativa_edt');
                $ind_tipo_ley_otro = null;
                $ind_dependencia_circular = null;
                $num_resolucion = $this->metObtenerTexto('num_resolucion_normativa_edt');
                $fec_resolucion = $_POST['fec_publicacion_resolucion_normativa_edt'];

                $aux = trim($fec_gaceta_otro);
                $aux2 = explode('-', $aux);
                $fec_gaceta_otro = $aux2[2]."-".$aux2[1]."-".$aux2[0];

                $aux = trim($fec_resolucion);
                $aux2 = explode('-', $aux);
                $fec_resolucion = $aux2[2]."-".$aux2[1]."-".$aux2[0];

                break;
            case "C" :

                $num_gaceta_otro = $this->metObtenerTexto('num_circular_edt');
                $fec_gaceta_otro = $_POST['fec_publicacion_circular_edt'];
                $ind_descripcion = $this->metObtenerTexto('ind_descripcion_circular_edt');

                $ind_tipo_ley_otro = $this->metObtenerTexto('ind_tipo_circular_edt');
                if(strcmp($ind_tipo_ley_otro,'INT' )==0)
                {
                    $ind_dependencia_circular = $this->metObtenerInt('ind_dependencia_circular_edt');
                }
                else//CGR
                {
                    $ind_dependencia_circular = null;
                }

                $num_resolucion = null;
                $fec_resolucion = null;

                $aux = trim($fec_gaceta_otro);
                $aux2 = explode('-', $aux);
                $fec_gaceta_otro = $aux2[2]."-".$aux2[1]."-".$aux2[0];

                break;
            case "RM" :
                $num_gaceta_otro = $this->metObtenerTexto('num_gaceta_resolucion_manuales_edt');
                $fec_gaceta_otro = $_POST['fec_publicacion_gaceta_resolucion_manuales_edt'];
                $ind_descripcion = $this->metObtenerTexto('ind_descripcion_resolucion_manuales_edt');
                $ind_dependencia_circular = null;
                $ind_tipo_ley_otro = null;
                $num_resolucion = $this->metObtenerTexto('num_resolucion_resolucion_manuales_edt');
                $fec_resolucion = $_POST['fec_publicacion_resolucion_resolucion_manuales_edt'];

                $aux = trim($fec_gaceta_otro);
                $aux2 = explode('-', $aux);
                $fec_gaceta_otro = $aux2[2]."-".$aux2[1]."-".$aux2[0];

                $aux = trim($fec_resolucion);
                $aux2 = explode('-', $aux);
                $fec_resolucion = $aux2[2]."-".$aux2[1]."-".$aux2[0];

                break;
            default:
                echo "error switch: ".$ind_tipo_normativa;
		        $band = 1;

        }

        $cod_img= "normativa_".mt_rand(0,999);
        $destino =  "publico/imagenes/modIN/normativa/";
        $ind_ruta_pdf = $_POST['ind_ruta_pdf_f'];//Nombre del archivo
        $pk_num_normativa_legal_edit = $_POST['pk_num_normativa_legal_edit'];

        if(!empty($_FILES["ind_ruta_pdf"]["name"]))//Archivo - se envió un archivo nuevo
        {

            $aux = explode('.',$_FILES["ind_ruta_pdf"]["name"]);
            $ind_formato = $aux[1];
            $aux2 = $this->atModeloDocumentos->sanear_string(mb_strtoupper($aux[0]));

            $pdf_f   = $cod_img."_".$aux2.".".$ind_formato;
            $ruta    = $destino.$pdf_f;
            //===========

            if (!file_exists($ruta))//False si no existe
            {
                $resultado = move_uploaded_file($_FILES["ind_ruta_pdf"]["tmp_name"], $ruta);
                chmod($ruta, 0777);
                if ($resultado)
                {
                    $ind_ruta_pdf = $pdf_f;
                    
                }
                else
                {
			        $band = 1;
			        echo -2;//ocurrio un error al mover el archivo
                }
            }
            else
            {
		        $band = 1;
		        echo -4;//el nombre ya existe no se registra la normativa
            }
        }
        
        if($band == 0)//NO hubo errores
        {
            $fecha_ultima_modificacion = date('Y-m-d');
	
	        $resp  = $this->atModeloNormativa->metModificarNormativaLegal($pk_num_normativa_legal_edit, $num_gaceta_otro, $fec_gaceta_otro, mb_strtoupper($ind_descripcion), $ind_tipo_ley_otro,$ind_dependencia_circular, mb_strtoupper($num_resolucion), $fec_resolucion, $ind_ruta_pdf, $this->atIdUsuario, $fecha_ultima_modificacion);
            if($resp)
            {
                echo 1;
            }
            else
            {
                echo " Error en en update : ".$resp;
            }
        }



    }

    public function metEliminarNormativa()
    {
       $pk_num_normativa_legal  =  $_POST['pk_num_normativa_legal'];
       $rutaPdf  =  $_POST['rutaPdf'];
       $resp  = $this->atModeloNormativa->metBorrarNormativaLegal($pk_num_normativa_legal);
       if($resp)
       {
           $ruta = 'publico/imagenes/modIN/normativa/'.$rutaPdf;
           if(unlink($ruta))
           {
               $arrayInterpretaciones = $this->atModeloNormativa->metBuscarListadoInterpretaciones($pk_num_normativa_legal);

               if($arrayInterpretaciones)
               {
                   $band_inter = 0;
                    for($i=0; $i< count($arrayInterpretaciones); $i++)
                    {
                        $resp = $this->atModeloNormativa->metEliminarInterpretacion($arrayInterpretaciones[$i]["pk_num_interpretacion"]);
                        if($resp)
                        {
                            $ruta = 'publico/imagenes/modIN/interpretacion/'.$arrayInterpretaciones[$i]["ind_ruta_archivo"];
                            if(unlink($ruta))
                            {
                                $band_inter = 1;

                            }
                            else
                            {
                                echo -3;
                                $band_inter = 0;
                                break;
                            }
                        }
                        else
                        {
                            echo -4;
                            $band_inter = 0;
                            break;
                        }
                    }

                   if($band_inter == 1)
                   {
                       echo 1;
                   }
               }
               else//no hay interpretaciones para esta ley
               {
                   echo 1;
               }

           }
           else
           {
               echo -2;
           }


       }
       else
       {
           echo -1;
       }
    }

    public function metInterpretacion()
    {

        $complementosCss = array(

            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array(
            'materialSiace/App',
            'materialSiace/core/cache/63d0445130d69b2868a8d28c93309746',
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'
        );



        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $pk_num_normativa_legal  = $_POST['pk_num_normativa_legal'];
        $datosInterpretacion = $this->atModeloNormativa->metBuscarListadoInterpretaciones($pk_num_normativa_legal);
        if($datosInterpretacion)
        {
            $band = 1;
            $total_inter = count($datosInterpretacion);
        }
        else
        {
            $band = 0;
            $total_inter = 0;
        }

        $this->atVista->assign('datosInterpretacion', $datosInterpretacion );
        $this->atVista->assign('pk_num_normativa_legal', $pk_num_normativa_legal );
        $this->atVista->assign('band', $band );
        $this->atVista->assign('total_inter', $total_inter );
        $this->atVista->metRenderizar('interpretacion', 'modales');

    }

    public function metRegistrarInterpretacion()
    {

        $fk_inc004_num_normativa_legal  =  $_POST['pk_num_normativa_inter'];
        $ind_descripcion  =  $_POST['descripcion_interpretacion'];

        $cod_img= 'interpretacion_'.mt_rand(0,999);
        $destino =  "publico/imagenes/modIN/interpretacion/";

        if(isset($_FILES["ind_ruta_pdf_inter"]))
        {

            $aux = explode('.',$_FILES["ind_ruta_pdf_inter"]["name"]);
            $ind_formato = $aux[1];
            $aux2 = $this->atModeloDocumentos->sanear_string(mb_strtoupper($aux[0]));

            $pdf_f   = $cod_img."_".$aux2.".".$ind_formato;
            $ruta    = $destino.$pdf_f;
            //======

            if(!file_exists($ruta))
            {
                $resultado = move_uploaded_file($_FILES["ind_ruta_pdf_inter"]["tmp_name"], $ruta);

                if($resultado)
                {
                    chmod($ruta, 0777);
                    $fec_ultima_modificacion = date('Y-m-d');
                    $resp = $this->atModeloNormativa->metRegistrarInterpretacion($fk_inc004_num_normativa_legal, mb_strtoupper($ind_descripcion), $pdf_f, $this->atIdUsuario,$fec_ultima_modificacion);

                    if($resp > 0)
                    {
                        $datos_inter = $this->atModeloNormativa->metBuscarListadoInterpretaciones($fk_inc004_num_normativa_legal);
                        if($datos_inter)
                        {
                            echo json_encode($datos_inter);


                        }
                        else
                        {
                            echo -6;
                        }
                    }
                    else
                    {
                        echo -1;//error al insertar
                    }
                }
                else
                {
                    echo -2;//error al mover archivo
                }
            }
            else
            {
                echo -4;//el archivo ya existe
            }
        }
        else
        {
            echo -5;//error al subir el file
        }


    }

    public function metEliminarInterpretacion()
    {
        $pk_num_interpretacion  =  $_POST['pk_num_interpretacion'];
        $rutaPdf = $_POST['rutaPdf'];
        $resp = $this->atModeloNormativa->metEliminarInterpretacion($pk_num_interpretacion);
        if($resp)
        {
            $ruta = 'publico/imagenes/modIN/interpretacion/'.$rutaPdf;
            if(unlink($ruta))
            {
                echo 1;
            }
            else
            {
                echo -2;
            }
        }
        else
        {
            echo -1;
        }


    }


};
 
?>


























