<?php 
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: INTRANET
 * PROCESO: GESTIONAR CHAT
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1  |         Maikol Isava                                  |    maikol.isava@gmail.com      |          0426-1814058          |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-11-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 class chatControlador extends Controlador
{
    private $atModeloChat;
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atModeloChat = $this->metCargarModelo('chat');

    }

    
    public function metIndex()
    {
		
        $complementosCss = array(

            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array(
            'materialSiace/App',
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
            'modIN/printThis'

        );

		$this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $datosConversacion = $this->atModeloChat->metBuscarMensajesChat('', '', 'T');
        $fechasConversaciones = $this->atModeloChat->metBuscarFechasChat();

        $this->atVista->assign('fechasConversaciones', $fechasConversaciones);
        $this->atVista->assign('datosConversacion', $datosConversacion);

        $this->atVista->metRenderizar('listar');

		
	}

    public function  metBuscarConversacionesListado()
    {
        $radio = $_POST['radio_filtro'];
        if($radio == 1)//todas las conversaciones
        {
            $datosConversacion = $this->atModeloChat->metBuscarMensajesChat('', '', 'T');
            $fechasConversaciones = $this->atModeloChat->metBuscarFechasChat();

        }
        else
        {
            $aux1 = $_POST['start_fec_conversacion'];
            $start_fec_conversacion = $this->atModeloChat->formatFechaAMD($aux1);
            $aux2 = $_POST['end_fec_conversacion'];
            $end_fec_conversacion = $this->atModeloChat->formatFechaAMD($aux2);
            $datosConversacion = $this->atModeloChat->metBuscarMensajesChat($start_fec_conversacion, $end_fec_conversacion,'U');
            $fechasConversaciones = $this->atModeloChat->metBuscarFechasChat2($start_fec_conversacion, $end_fec_conversacion);

        }


        $datosConversaciones = array('fechasConversaciones' => $fechasConversaciones, 'conversaciones' => $datosConversacion );
        echo json_encode($datosConversaciones);

    }

    public function metEliminarMensajeChat()
    {
        $pk_num_chat = trim($_POST['pk_num_chat']);
        $resp = $this->atModeloChat->metEliminarMensaje($pk_num_chat);
        if($resp)
        {
            echo 1;
        }
        else
        {
            echo $resp;
        }
    }

    public function metEliminarConversacionesChat()
    {
        $tipo = trim(trim($_POST['tipo']));

        if($tipo == 'T')
        {
            $resp = $this->atModeloChat->metEliminarConversacion('T','','');
            if($resp)
            {
                echo 1;
            }
            else
            {
                echo $resp;
            }
        }
        else
        {
            $fechUnicaConversacion = trim($_POST['fechUnicaConversacion']);
            $fechUnicaConversacion2 = trim($_POST['fechUnicaConversacion2']);

            $resp = $this->atModeloChat->metEliminarConversacion('U',$fechUnicaConversacion ,$fechUnicaConversacion2 );
            if($resp)
            {
                echo 1;
            }
            else
            {
                echo $resp;
            }

        }

    }



	
	
};
 
?>