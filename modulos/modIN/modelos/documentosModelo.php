<?php 
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Intranet
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class documentosModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

    }
	
	 public function metConsultarOrganismos()
    {

        $con = $this->_db->query("
                SELECT * FROM `a001_organismo`
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();//RETORNA FALSE SI NO HAY RESULTADO
    }
	
	public function metUsuario($_seguridad_usuario)
    {
        $usuario = $this->_db->query("select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$_seguridad_usuario' and a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetch();


    }

    public function metConsultarDepencias($opc=0 )
    {

        if($opc == 0)
        {
            $consulta = "SELECT * FROM a004_dependencia ";
        }
        else
        {
            $consulta = "SELECT * FROM `a004_dependencia` WHERE pk_num_dependencia = '$opc'";

        }

         $usuario = $this->_db->query($consulta);
         $usuario->setFetchMode(PDO::FETCH_ASSOC);
         return $usuario->fetchAll();

    }

    public function metBuscarCarpetasDocumentos($pk_num_dependencia)
    {
        $consulta = $this->_db->query("
        SELECT
        pk_num_carpetas_documentos,
        date_format(fec_creacion,'%d-%m-%Y') AS fec_creacion_carpeta,
        ind_descripcion AS ind_descripcion_carpeta,
        fk_a004_dependencia,
        fk_a018_num_seguridad_usuario,
        fec_ultima_modificacion AS fec_ultima_modificacion_carpeta
        FROM
        `in_c006_carpetas_documentos`
        WHERE  fk_a004_dependencia = '$pk_num_dependencia'
        ");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metBuscarDocumentos($pk_num_carpetas_documentos)
    {
        $consulta = $this->_db->query("
        SELECT
        `pk_num_documentos`,
        date_format(fec_creacion,'%d-%m-%Y') AS fec_creacion,
        `ind_peso`,
        `ind_formato`,
        `ind_descripcion`,
        `fk_c006_carpetas_documentos`,
        `fk_a018_num_seguridad_usuario`,
        `fec_ultima_modificacion`
        FROM `in_c007_documentos`
        WHERE  fk_c006_carpetas_documentos = '$pk_num_carpetas_documentos'
        ");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metBuscarTodasCarpetasDocumentos()
    {
        $consulta = $this->_db->query("
        SELECT
        pk_num_carpetas_documentos,
        fec_creacion AS fec_creacion_carpeta,
        ind_descripcion AS ind_descripcion_carpeta,
        fk_a004_dependencia,
        fk_a018_num_seguridad_usuario,
        fec_ultima_modificacion AS fec_ultima_modificacion_carpeta
        FROM
        `in_c006_carpetas_documentos`
        ORDER BY fk_a004_dependencia ASC

        ");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metBorrarDocumento($pk_num_documentos)
    {
        $this->_db->beginTransaction();

        $respuesta = $this->_db->query(
            "DELETE FROM `in_c007_documentos` WHERE pk_num_documentos = '$pk_num_documentos'"
        );

        $this->_db->commit();
        return $respuesta;

    }

    public function metCalcularNumElementosCarpetas($pk_num_carpetas_documentos)
    {
        $consulta = $this->_db->query("
        SELECT COUNT( * ) AS TOTAL
        FROM  `in_c007_documentos`
        WHERE  `fk_c006_carpetas_documentos` =  '$pk_num_carpetas_documentos'");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function renombrarCarpeta($pk_num_carpetas_documentos, $ind_descripcion, $fec_ultima_modificacion)
    {

        $this->_db->beginTransaction();
        $modificacion =  $this->_db->query("
        UPDATE `in_c006_carpetas_documentos`
        SET
        `ind_descripcion`='$ind_descripcion',
        `fec_ultima_modificacion`='$fec_ultima_modificacion'
        WHERE pk_num_carpetas_documentos = '$pk_num_carpetas_documentos' ");

        $this->_db->commit();

        return $modificacion;
    }

    public function metCrearCarpeta($pk_num_dependencia, $ind_descripcion, $fec_ultima_modificacion, $fk_a018_num_seguridad_usuario, $fec_creacion)
    {

        $this->_db->beginTransaction();
        $NuevaSolicitud=$this->_db->prepare(
            "insert into in_c006_carpetas_documentos values (
             null,
             :fec_creacion,
             :ind_descripcion,
             :fk_a004_dependencia,
             :fk_a018_num_seguridad_usuario,
             :fec_ultima_modificacion
             )"
        );

        $NuevaSolicitud->execute(array(
            ':fec_creacion' => $fec_creacion,
            ':ind_descripcion' => $ind_descripcion,
            ':fk_a004_dependencia' => $pk_num_dependencia,
            ':fk_a018_num_seguridad_usuario' => $fk_a018_num_seguridad_usuario,
            ':fec_ultima_modificacion' => $fec_ultima_modificacion
        ));

        $pk_num_carpetas_documentos = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pk_num_carpetas_documentos;
    }

    public function metBorrarCarpeta($pk_num_carpetas_documentos)//Borra carpeta y documentos
    {
        $this->_db->beginTransaction();

        $respuesta = $this->_db->query(
            "DELETE FROM `in_c006_carpetas_documentos` WHERE pk_num_carpetas_documentos = '$pk_num_carpetas_documentos'"
        );

        $this->_db->commit();
        return $respuesta;
    }

    public function metTamanoArchivo($peso , $decimales = 2 )
    {
        $clase = array(" Bytes", " KB", " MB", " GB", " TB");
        return round($peso/pow(1024,($i = floor(log($peso, 1024)))),$decimales ).$clase[$i];
    }

    public function metRegistrarArchivo($fec_creacion, $ind_peso, $ind_formato, $ind_descripcion, $carpeta_subir_archivo, $atIdUsuario, $fec_ultima_modificacion)
    {
        $this->_db->beginTransaction();
        $consulta=$this->_db->prepare(
            "insert into in_c007_documentos values (
             null,
             :fec_creacion,
             :ind_peso,
             :ind_formato,
             :ind_descripcion,
             :fk_c006_carpetas_documentos,
             :fk_a018_num_seguridad_usuario,
             :fec_ultima_modificacion
             )"
        );

        $consulta->execute(array(
            ':fec_creacion' => $fec_creacion,
            ':ind_peso' => $ind_peso,
            ':ind_formato' => $ind_formato,
            ':ind_descripcion' => $ind_descripcion,
            ':fk_c006_carpetas_documentos' => $carpeta_subir_archivo,
            ':fk_a018_num_seguridad_usuario'=> $atIdUsuario,
            ':fec_ultima_modificacion'=> $fec_ultima_modificacion
        ));

        $pk_num_documentos = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pk_num_documentos;
    }

    public function metBuscarCarpetasDocumentosNombre($pk_num_dependencia, $ind_descripcion)
    {
        $consulta = $this->_db->query("
        SELECT COUNT( * ) AS TOTAL
        FROM  `in_c006_carpetas_documentos`
        WHERE  `fk_a004_dependencia` =  '$pk_num_dependencia' AND `ind_descripcion` = '$ind_descripcion' ");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metBorrarDirectorio($directorio)
    {
       $archivos = scandir($directorio); //hace una lista de archivos del directorio

        $num = count($archivos);

        for ($i=2; $i< $num; $i++)
        {
            unlink ($directorio.'/'.$archivos[$i]);
        }

        return rmdir($directorio);
    }

    public function renombrarArchivo($pk_num_documentos, $ind_descripcion, $fec_ultima_modificacion)
    {

        $this->_db->beginTransaction();
        $modificacion =  $this->_db->query("
        UPDATE `in_c007_documentos`
        SET
        `ind_descripcion`= '$ind_descripcion',
        `fec_ultima_modificacion`= '$fec_ultima_modificacion'
        WHERE pk_num_documentos = '$pk_num_documentos' ");

        $this->_db->commit();

        return $modificacion;
    }

    public function sanear_string($string)
    {
        $string = trim($string);

        $string = mb_substr($string, 0, 150);//RECORTE A 150 CARACTERES

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );

        //Esta parte se encarga de eliminar cualquier caracter extraño

        $car_especiales[] = '¨';
        $car_especiales[] = 'º';
        $car_especiales[] = '-';
        $car_especiales[] = '~';
        $car_especiales[] = '#';
        $car_especiales[] = '@';
        $car_especiales[] = '|';
        $car_especiales[] = "!";
        $car_especiales[] = "'";

        $car_especiales[] = '$';
        $car_especiales[] = "";
        $car_especiales[] = '%';
        $car_especiales[] = '&';
        $car_especiales[] = '/';
        $car_especiales[] = "(";
        $car_especiales[] = ")";
        $car_especiales[] = "?";
        $car_especiales[] = "¡";
        $car_especiales[] = "¿";

        $car_especiales[] = "[";
        $car_especiales[] = "^";
        $car_especiales[] = "]";
        $car_especiales[] = "+";
        $car_especiales[] = "}";
        $car_especiales[] = "{";
        $car_especiales[] = "¨";
        $car_especiales[] = ">";
        $car_especiales[] = "< ";
        $car_especiales[] = ";";

        $car_especiales[] = ",";
        $car_especiales[] = "´";
        $car_especiales[] = ":";
        $car_especiales[] = ".";
        $car_especiales[] = " ";

        $string = str_replace( $car_especiales ,'_', $string );

            return $string;
    }

    public function metConsultarVistaDatosEmpleado($pk_num_empleado)
    {
        $consulta = $this->_db->query("
        SELECT * FROM `vl_rh_persona_empleado_datos_laborales`
        WHERE
        `pk_num_empleado` =  '$pk_num_empleado' ");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metBuscarCarpetasDocumentosFiltrar($pk_num_dependencia, $patron)
    {
        $consulta = $this->_db->query("
        SELECT
        pk_num_carpetas_documentos,
        date_format(fec_creacion,'%d-%m-%Y') AS fec_creacion_carpeta,
        ind_descripcion AS ind_descripcion_carpeta,
        fk_a004_dependencia,
        fk_a018_num_seguridad_usuario,
        fec_ultima_modificacion AS fec_ultima_modificacion_carpeta
        FROM
        `in_c006_carpetas_documentos`
        WHERE  UPPER(ind_descripcion) LIKE (UPPER('%".$patron."%')) AND  fk_a004_dependencia = '$pk_num_dependencia'
        ");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public  function metBuscarTodasCarpetasDocumentosFiltrar($patron)
    {
        $consulta = $this->_db->query("
        SELECT
        pk_num_carpetas_documentos,
        fec_creacion AS fec_creacion_carpeta,
        ind_descripcion AS ind_descripcion_carpeta,
        fk_a004_dependencia,
        fk_a018_num_seguridad_usuario,
        fec_ultima_modificacion AS fec_ultima_modificacion_carpeta
        FROM
        `in_c006_carpetas_documentos`
        WHERE UPPER(ind_descripcion) LIKE (UPPER('%".$patron."%'))
        ORDER BY fk_a004_dependencia ASC

        ");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metConsultarSeguridadAlterna($atIdUsuario)
    {
        $consulta = "
        SELECT
        a004_dependencia.pk_num_dependencia,
        a004_dependencia.ind_dependencia
        FROM `a019_seguridad_dependencia`
        INNER JOIN `a004_dependencia`
        ON a019_seguridad_dependencia.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
        WHERE a019_seguridad_dependencia.fk_a018_num_seguridad_usuario =  '$atIdUsuario'
        AND fk_a015_num_seguridad_aplicacion = '20'
        ORDER BY  `fk_a004_num_dependencia` ASC ";

        $usuario = $this->_db->query($consulta);
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }

};


?>

