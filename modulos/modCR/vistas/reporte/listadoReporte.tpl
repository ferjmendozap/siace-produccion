<br/>
<section class="style-default-bright">
    <br/>
    <div class="col-lg-13">	
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Listado de Reportes</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-13 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><i class="md-insert-drive-file"></i>Num Reporte</th>
                            <th><i class="md-account-box "></i>Usuario</th>
                            <th><i class="md-access-time"></i>Fecha del Reporte</th>
                            <th><i class="md-access-time"></i>Fecha Inicial</th>
                            <th><i class="md-access-time"></i>Fecha Final</th>
                            <th>Ver</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                       {foreach item=post from=$_PruebaPost}
                       <tr id="pk_num_reporte{$post.pk_num_reporte}">
                            <td>{$post.pk_num_reporte}</td>
                            <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                            <td>{$post.fec_fecha_reporte}</td>
                            <td>{$post.fec_fecha_inicio}</td>
                            <td>{$post.fec_fecha_fin}</td>
                           <td  align="center"><button class="verReporte logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  pk_num_reporte="{$post.pk_num_reporte}" descipcion="El Usuario ha visualizado el reporte de consumo" title="Reporte de consumo" titulo="Reporte de consumo"> <i class="md md-attach-file"></i></button></td>
                           <td>
                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_reporte="{$post.pk_num_reporte}"  boton="si, Eliminar" descipcion="El usuario a eliminado un reporte" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el reporte?"><i class="md md-delete" style="color: #ffffff;"></i></button>
                            </td>
                       </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var pk_num_reporte=$(this).attr('pk_num_reporte');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCR/reporteCONTROL/EliminarReporteMET';
                $.post($url, { pk_num_reporte: pk_num_reporte},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('pk_num_reporte'+$dato['pk_num_reporte'])).html('');
                        swal("Eliminado!", "Reporte eliminado correctamente.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
    $('#datatable1 tbody').on( 'click', '.verReporte', function () {
        var $pk_num_reporte = $(this).attr('pk_num_reporte');
        var $urlVerReporte =  '{$_Parametros.url}modCR/reporteCONTROL/GenerarReporteMET/?pk_num_reporte='+$pk_num_reporte;
        $('#modalAncho').css( "width", "75%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $('#ContenidoModal').html('<iframe frameborder="0" src="'+$urlVerReporte+'"  width="100%" height="440px"></iframe>');
    });
</script>