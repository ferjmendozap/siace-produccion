<form action="{$_Parametros.url}modCR/equipoCONTROL/EditarEquipoMET" id="formAjax" method="post" class="form" role="form">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
	{if isset($form.pk_num_equipo)}
		<input type="hidden" value="{$form.pk_num_equipo}" name="pk_num_equipo" id="pk_num_equipo" />
	{/if}
	<div class="col-md-6 col-sm-6">
		<div class="form-group floating-label">
			<div class="form-group floating-label">
				<div class="input-group">
					<div class="input-group-content">
						<input type="text" class="form-control dirty" id="remitente" name="remitente" value="{$form.usuario}">
						<input type="hidden" class="form-control" id="pk_num_empleado" name="pk_num_empleado" value="{$form.pk_num_empleado}">
						<label for="groupbutton10">Usuario</label>
					</div>
					<div class="input-group-btn">
						<button class="btn btn-default" type="button" onclick="buscarFuncionario()" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Funcionarios"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
			</div><!--end .form-group -->
		</div>
        <div id="dependencia">
            <div class="form-group floating-label">
                <select id="fk_a004_num_dependencia" name="fk_a004_num_dependencia" id="s2id_single" class="select2-container form-control select2">
                    <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                    <option value="">&nbsp;</option>
                    {foreach item=dep from=$dependencia}
                    	{if $dep.pk_num_dependencia == $form.fk_a004_num_dependencia}
                   			 <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                    	{else}
                    		<option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                    	{/if}
                    {/foreach}
                </select>
                <label for="select2"><i class="md-business"></i> Dependencia</label>
            </div>
        </div>    
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.ind_nombre_equipo}" name="ind_nombre_equipo" id="ind_nombre_equipo">
			<label for="regular2"><i class="md-desktop-windows"></i> Equipo</label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.ind_direccion_fisica}" name="ind_direccion_fisica" id="ind_direccion_fisica">
			<label for="regular2"><i class="md-cast"></i> Dirección Mysar</label>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.ind_ip}" name="ind_ip" id="ind_ip">
			<label for="regular2"><i class="md-wifi-tethering"></i> Ip</label>
		</div>
		{foreach item=ac from=$acceso}
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$ac.fecha}" disabled="disabled">
				<label for="regular2">Última modificación</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$ac.ind_nombre1} {$ac.ind_apellido1}" disabled="disabled">
				<label for="regular2">Último usuario</label>
			</div>
		{/foreach}
			<div align="right">
				<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
				<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").validate({
			rules:{
				fk_a004_num_dependencia:{
					required:true
				},
				ind_nombre_equipo:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modCR/equipoCONTROL/VerificarEquipoMET",
						type:"post",
						 data: {
							pk_num_equipo: function(){
								return $( "#pk_num_equipo" ).val();
							}
						}
					}
				},
				ind_direccion_fisica:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modCR/equipoCONTROL/VerificarDireccionMysarMET",
						type:"post",
						data: {
							pk_num_equipo: function(){
								return $( "#pk_num_equipo" ).val();
							}
						}
					}
				},
				ind_ip:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modCR/equipoCONTROL/VerificarDireccionIpMET",
						type:"post",
						data: {
							pk_num_equipo: function(){
								return $( "#pk_num_equipo" ).val();
							}
						}
					}
				}
			},
			messages:{
				fk_a004_num_dependencia:{
					required: "Este campo es requerido"
				},
				ind_nombre_equipo: {
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				},
				ind_direccion_fisica:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				},
				ind_ip:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				}
			},
			submitHandler: function(form){
				$.post($(form).attr("action"), $(form).serialize(),function(dato){
				$('#pk_num_equipo'+dato['pk_num_equipo']).remove();
				$(document.getElementById('datatable1')).append('<tr id="pk_num_equipo'+dato['pk_num_equipo']+'"><td>'+dato['fk_a003_num_persona']+'</td>' +
				'<td>'+dato['fk_a004_num_dependencia']+'</td><td>'+dato['ind_nombre_equipo']+'</td>' +
				'<td>'+dato['ind_ip']+'</td><td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizar un equipo" title="Visualizar equipo" titulo="Ver equipo" data-toggle="modal" data-target="#formModal" pk_num_equipo="'+dato['pk_num_equipo']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>' + '<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha modificado un equipo" ' +
				'titulo="Modificar Equipo" data-toggle="modal" data-target="#formModal" pk_num_equipo="'+dato['pk_num_equipo']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>' +
						'<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario a eliminado un equipo" titulo="¿Estas Seguro?" mensaje="Estas seguro de eliminar el equipo" boton="si, Eliminar" pk_num_equipo="'+dato['pk_num_equipo']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td></tr>');
				swal("Equipo Modificado", "Equipo modificado exitosamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
            },'json');
        }
    });    
});
	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}
	function repoFormatSelection( repo ) {
		return repo.full_name;
	}

	$( "button[data-select2-open]" ).click( function() {
		$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
	});
	
	 function cargarDependencia(pkNumEmpleado) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modCR/equipoCONTROL/BuscarDependenciaMET",{ pkNumEmpleado:""+pkNumEmpleado }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function buscarFuncionario(){
        var $urlFuncionario = '{$_Parametros.url}modCR/equipoCONTROL/ConsultarEmpleadoMET';
        $('#ContenidoModal2').html("");
        $('#formModalLabel2').html('Listado de Empleados');
        $.post($urlFuncionario,"",function($dato){
            $('#ContenidoModal2').html($dato);
        });
    }
</script>


