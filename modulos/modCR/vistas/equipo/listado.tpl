<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Listado de Equipos</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><i class="md md-person"></i> Usuario</th>
                            <th><i class="md-business"></i> Dependencia</th>
                            <th><i class="md-desktop-windows"></i> Equipo</th>
                            <th><i class="md-wifi-tethering"></i> Ip</th>
                            <th width="50">Ver</th>
                            <th width="50">Editar</th>
                            <th width="70">Eliminar</th>
                        </tr>
                    </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_equipo{$post.pk_num_equipo}">
                            <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                            <td>{$post.ind_dependencia}</td>
                            <td>{$post.ind_nombre_equipo}</td>
                            <td>{$post.ind_ip}</td>
                            <td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_equipo="{$post.pk_num_equipo}" titulo="Visualizar equipo" title="Visualizar equipo" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>
                            <td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_equipo="{$post.pk_num_equipo}" descipcion="El Usuario ha Modificado un equipo" title="Modificar Equipo" titulo="Modificar Equipo" ><i class="fa fa-edit"></i></button></td>
                            <td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_equipo="{$post.pk_num_equipo}"  boton="si, Eliminar" descipcion="El usuario a eliminado un equipo" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el equipo?" title="Eliminar equipo"><i class="md md-delete"></i></button></td>
                            </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7">
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha registrado un equipo" data-toggle="modal" data-target="#formModal" titulo="Registrar nuevo equipo" id="nuevo"><i class="md md-create"></i> Nuevo equipo</button>
                                &nbsp;
                                <button class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteEquipo" descripcion="Generar Reporte de Equipos" data-toggle="modal" data-target="#formModal" titulo="Listado de Equipos">Reporte de Equipos</button>
                            </th>
                        </tr>
                        </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modCR/equipoCONTROL/NuevoEquipoMET';
                var $url_modificar='{$_Parametros.url}modCR/equipoCONTROL/EditarEquipoMET';
                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });

                $('#datatable1 tbody').on( 'click', '.modificar', function () {
					$('#modalAncho').css( "width", "75%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_equipo: $(this).attr('pk_num_equipo')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                    var pk_num_equipo=$(this).attr('pk_num_equipo');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modCR/equipoCONTROL/EliminarEquipoMET';
                        $.post($url, { pk_num_equipo: pk_num_equipo},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_equipo'+$dato['pk_num_equipo'])).html('');
                                swal("Eliminado!", "el post fue eliminado.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });
    $(document).ready(function() {
        $('#reporteEquipo').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modCR/equipoCONTROL/ReporteEquipoMET' border='1' width='100%' height='432'></iframe>");
        });
    });
            var $urlVer='{$_Parametros.url}modCR/equipoCONTROL/VerEquipoMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_equipo: $(this).attr('pk_num_equipo')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
