<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de equipos
class equipoControlador extends Controlador
{
	private $atEquipoModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		#se carga el Modelo Equipo.
		$this->atEquipoModelo = $this->metCargarModelo('equipo');
	}

	//Método Index del controlador Equipo.
	public function metIndex(){ //Permite la carga de los objetos contenidos en la carpeta publico/complementos
		// Creo el array con los archivos css que se van a utilizar
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef'
		);
		// Creo el array con la ruta de los archivos Javascript a utilizar
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		// Cargo la variable $complementosCss y $js en la vista correspondiente
		// El método metCargarCssComplemento le agrega a los archivos la extensión .css
		$this->atVista->metCargarCssComplemento($complementosCss);
		// El método metCargarJs le agrega a los archivos la extensión .js y añade al inicio de la ruta la carpeta Javascript
		$this->atVista->metCargarJs($js);
		// Se llama al método del modelo que se encarga de listar los equipos metListarEquipos()
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$complementosJs[] = 'select2/select2.min';
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJsComplemento($validar);
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 13;
		$this->atVista->assign('_PruebaPost', $this->atEquipoModelo->metListarEquipos($usuario, $idAplicacion));
		// Cargo el archivo listado.tpl sin la extensión .tpl ya que el método metRenderizar concatena la exensión
		$this->atVista->metRenderizar('listado');
	}

	// Método para cargar el formulario de registro de equipos
	public function metNuevoEquipo()
	{
		// La Funcion metObtenerInt valida que los datos enviados por post hacia el controlador sean solo Alpha-Numerico

		if (isset($_POST['valido'])) {
			$valido = $_POST['valido'];
		}
        $usuario = Session::metObtener('idUsuario');
		// Verifica que el formulario esté definido y que sea válido
		if (isset($valido) && $valido == 1) {
			//$this->metValidarToken();
			$pkNumEmpleado = $_POST['pk_num_empleado'];
			$pkNumDependencia = $_POST['pk_num_dependencia'];
			$indNombreEquipo = $_POST['ind_nombre_equipo'];
			$indDireccionFisica = $_POST['ind_direccion_fisica'];
			$indIp = $_POST['ind_ip'];
			$fechaHora = date('Y-m-d H:i:s');
			// Se cargan los métodos del modelo a utilizar y los campos del formulario con datos
			$pkNumEquipo=$this->atEquipoModelo->metGuardarEquipo($pkNumEmpleado, $pkNumDependencia, $indNombreEquipo, $indDireccionFisica, $indIp, $fechaHora,$usuario);
			$nombreUsuario = $this->atEquipoModelo->metUsuarioEquipo($pkNumEmpleado);
			$nombreDependencia = $this->atEquipoModelo->metDependenciaEquipo($pkNumDependencia);
			// Se crea un array con el nombre de los campos que se encuentran en la base de datos y se le asignan las variables recibidas en el formulario
			$arrayPost = array(
				'empleado'=> $nombreUsuario,
				'ind_dependencia' => $nombreDependencia,
				'ind_nombre_equipo' => $indNombreEquipo,
				'ind_direccion_fisica' => $indDireccionFisica,
				'ind_ip' => $indIp,
				'pk_num_equipo' => $pkNumEquipo
			);
			// Se retorna el array en forma de JSON
			echo json_encode($arrayPost);
			// Salimos de la instrucción
			exit;
		}

		// Si el formulario se va a cargar vacio para proceder al llenado; entonces se le asigna el valor null a cada campo
		$form = array(
			'status' => 'nuevo',
			'fk_a003_num_persona'=> null,
			'fk_a004_num_dependencia' => null,
			'ind_nombre_equipo' => null,
			'ind_direccion_fisica' => null,
			'ind_ip' => null,
		);
		// Se llaman a los métodos del modelo a utilizar. En este caso metListarUsuario que consulta el listado de empleados, los cuales serán los futuros usuarios de los equipos.
		// y el metodo ConsultarSeguridadAlterna, el cual lista las dependencias internas de la Contraloría.
		$usuarioListar =$this->atEquipoModelo->metListarUsuario();
		// Se envía a la vista la variable que contiene la lista de elementos a listar en el select de Usuario; en este caso la variable es $usuario que contiene los datos retornados de MetListarUsuario
		$this->atVista->assign('usuario',$usuarioListar);
        $idAplicacion = 13;
		$dependencia= $this->atEquipoModelo->metConsultarSeguridadAlterna($usuario, $idAplicacion);
		$this->atVista->assign('dependencia',$dependencia);
		// Se envía a la vista el array almacenado en la variable $form
		$this->atVista->assign('form', $form);
		// Se carga la ventana modal
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	// Método para gestionar la edición de equipos
	public function metEditarEquipo()
	{
		// La Funcion metObtenerInt valida que los datos enviados por post hacia el controlador sean solo Alpha-Numerico
		$valido = $this->metObtenerInt('valido');
		$pkNumEquipo = $this->metObtenerInt('pk_num_equipo');
		// Verifica que el formulario esté definido y que sea válido
		if (isset($valido) && $valido == 1) {
			// Se validan los campos del formulario
			$this->metValidarToken();
			$pkNumEmpleado = $_POST['pk_num_empleado'];
			$pkNumDependencia = $_POST['fk_a004_num_dependencia'];
			$indNombreEquipo = $_POST['ind_nombre_equipo'];
			$indDireccionFisica = $_POST['ind_direccion_fisica'];
			$indIp = $_POST['ind_ip'];
			$usuario = Session::metObtener('idUsuario');
			$fechaHora = date('Y-m-d H:i:s');
			$consultarExistencia = $this->atEquipoModelo->metUsuario($pkNumEquipo);
			$persona = $consultarExistencia['fk_rhb001_num_empleado'];
			if(($persona>0)&&($pkNumEmpleado>0)) {
				$this->atEquipoModelo->metEditarEquipo($pkNumEmpleado, $pkNumDependencia, $indNombreEquipo, $indDireccionFisica, $indIp, $fechaHora, $usuario, $pkNumEquipo);
			}
			if(($persona==0)&&($pkNumEmpleado>0)){
				$this->atEquipoModelo->metInsertarEditar($pkNumEmpleado, $pkNumDependencia, $indNombreEquipo, $indDireccionFisica, $indIp, $fechaHora, $usuario, $pkNumEquipo);
			}
			if(($persona>0)&&($pkNumEmpleado==0)){
				$this->atEquipoModelo->metEliminarEditar($pkNumEquipo);
			}
			$nombreDependencia = $this->atEquipoModelo->metDependenciaEquipo($pkNumDependencia);
			$nombreUsuario = $this->atEquipoModelo->metUsuarioEquipo($pkNumEmpleado);
			// Se crea un array con el nombre de los campos que se encuentran en la base de datos y se le asignan las variables recibidas en el formulario
			$arrayPost = array(
				'status' => 'modificar',
				'fk_a003_num_persona' => $nombreUsuario,
				'fk_a004_num_dependencia' => $nombreDependencia,
				'ind_nombre_equipo' => $indNombreEquipo,
				'ind_direccion_fisica' => $indDireccionFisica,
				'ind_ip' => $indIp,
				'pk_num_equipo' =>  $pkNumEquipo
			);
			// Se retorna el array en forma de JSON
			echo json_encode($arrayPost);
			// Salimos de la instrucción
			exit;
		}
		// Si el identificador del equipo tiene valor
		if (!empty($pkNumEquipo)) {
			// Se llama al método metVerEquipo y se le pasa el identificador del equipo. Se realiza la consulta correspondiente y se retornan los datos almacenados a ese equipo en particular
			$form = $this->atEquipoModelo->metMostrarEquipo($pkNumEquipo);
			$persona = $this->atEquipoModelo->metVerPersona($pkNumEquipo);
			$pkNumEmpleado = $persona['pk_num_empleado'];
			$form = array(
				'status' => 'modificar',
				'pk_num_empleado' => $pkNumEmpleado,
				'usuario' => $persona['ind_nombre1'].' '.$persona['ind_nombre2'].' '.$persona['ind_apellido1'].' '.$persona['ind_apellido2'],
				'fk_a004_num_dependencia' => $form['fk_a004_num_dependencia'],
				'ind_nombre_equipo' => $form['ind_nombre_equipo'],
				'ind_direccion_fisica' => $form['ind_direccion_fisica'],
				'ind_ip' => $form['ind_ip'],
				'pk_num_equipo' => $pkNumEquipo
			);
			// Se llaman a los métodos del modelo a utilizar. En este caso metListarUsuario que consulta el listado de empleados, los cuales serán los futuros usuarios de los equipos.
			// y el metodo ConsultarSeguridadAlterna, el cual lista las dependencias internas de la Contraloría.
			$usuario=$this->atEquipoModelo->metListarUsuario();
			$this->atVista->assign('usuario',$usuario);
            $usuario = Session::metObtener('idUsuario');
            $idAplicacion = 13;
			$dependencia=$this->atEquipoModelo->metConsultarSeguridadAlterna($usuario, $idAplicacion);
			$this->atVista->assign('dependencia',$dependencia);
			$formulario = $this->atEquipoModelo->metVerAcceso($pkNumEquipo);
			$this->atVista->assign('acceso', $formulario);
			$this->atVista->assign('form', $form);
			// Se carga la ventana modal
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que recibe y envía al modelo el identificador del equipo para proceder a su eliminación
	public function metEliminarEquipo()
	{
		// La Funcion metObtenerInt valida que los datos enviados por post hacia el controlador sean solo Alpha-Numerico
		$pkNumEquipo = $this->metObtenerInt('pk_num_equipo');
		// Se llama al metodo metEliminarEquipo del modelo equipoModelo y se le envía el identificador del equipo a eliminar
		$this->atEquipoModelo->metEliminarEquipo($pkNumEquipo);
		// Se crea un array con el estatus de la operación y el identificador del equipo
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_equipo' => $pkNumEquipo
		);
		// Se retorna el array en forma de JSON al listado de equipos en la vista listado.tpl
		echo json_encode($arrayPost);
	}

	// Método que permite generar el reporte de estaciones de trabajo registradas
	public function metReporteEquipo()
	{
		$this->metObtenerLibreria('cabeceraEquipo','modCR');
		$pdf= new pdfEquipo('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(18, 25 , 30); // margen izquierdo, superior y derecho
		$pdf->SetAutoPageBreak(true,10); // establece el margen inferior
		$pdf->SetFont('Arial','',10);
		$pdf->SetX(18);
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 13;
		$dependencia=$this->atEquipoModelo->metConsultarSeguridadAlterna($usuario, $idAplicacion);
        $usuario = Session::metObtener('idUsuario');
		foreach($dependencia as $dependencia) {
			$pkNumDependencia=$dependencia['pk_num_dependencia'];
			$listado = $this->atEquipoModelo->metListaEquipos($pkNumDependencia, $usuario, $idAplicacion);
			$contadorListado = count($listado);
			if($contadorListado>0){
				$pdf->SetFont('Arial','B',10);
				$pdf->SetWidths(array(185));
				$pdf->Row(array(
					utf8_decode($dependencia['ind_dependencia'])
				), 5);
				$pdf->SetWidths(array(80, 65, 40));
				$pdf->SetFont('Arial','',9);
				foreach ($listado as $listado) {
					$pdf->Row(array(
						utf8_decode($listado['ind_nombre1']) . ' ' . utf8_decode($listado['ind_apellido1']),
						$listado['ind_nombre_equipo'],
						utf8_decode($listado['ind_ip'])
					), 5);
				}
				
			}
		}
		$pdf->Output();
	}

	// Método que permite verificar la existencia de un equipo en el sistema
	public function metVerificarEquipo(){
		$nombreEquipo = $this->metObtenerTexto('ind_nombre_equipo');
		$pkNumEquipo = $this->metObtenerInt("pk_num_equipo");
		$equipoExiste = $this->atEquipoModelo->metBuscarEquipo($nombreEquipo,$pkNumEquipo);
		echo json_encode(!$equipoExiste);
	}

	// Método que permite verificar la existencia de la dirección Mysar en el sistema
	public function metVerificarDireccionMysar(){
		$direccionMysar = $this->metObtenerTexto('ind_direccion_fisica');
		$pkNumEquipo = $this->metObtenerInt("pk_num_equipo");
		$direccionMysarExiste = $this->atEquipoModelo->metBuscarDireccionMysar($direccionMysar, $pkNumEquipo);
		echo json_encode(!$direccionMysarExiste);
	}

	// Método que permite verificar la existencia de la dirección Mysar en el sistema
	public function metVerificarDireccionIp(){
		$direccionIp = $this->metObtenerTexto('ind_ip');
		$pkNumEquipo = $this->metObtenerInt("pk_num_equipo");
		$direccionIpExiste = $this->atEquipoModelo->metBuscarDireccionIp($direccionIp, $pkNumEquipo);
		echo json_encode(!$direccionIpExiste);
	}

	// Método que permite visualizar un equipo
	public function metVerEquipo()
	{
		$pkNumEquipo = $this->metObtenerInt('pk_num_equipo');
		$equipo=$this->atEquipoModelo->metVisualizarEquipo($pkNumEquipo);
		$this->atVista->assign('equipo',$equipo);
		$usuarioEquipo= $this->atEquipoModelo->metUsuarioVer($pkNumEquipo);
		$this->atVista->assign('usuarioEquipo',$usuarioEquipo);
		$verAcceso = $this->atEquipoModelo->metVerAcceso($pkNumEquipo);
		$this->atVista->assign('acceso',$verAcceso);
		$this->atVista->metRenderizar('ver', 'modales');
	}
	
	// Método que permite listar las dependencia a la cual pertenece el usuario
    public function metBuscarDependencia()
    {
        $pkNumEmpleado = $this->metObtenerInt('pkNumEmpleado');
		// Busco la dependencia a la cual pertenece el usuario
		$pkNumDependenciaPrevia = $this->atEquipoModelo->metDependenciaUsuario($pkNumEmpleado);
		$pkNumDependencia = $pkNumDependenciaPrevia['fk_a004_num_dependencia'];
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 13;
        $listarDependencia = $this->atEquipoModelo->metConsultarSeguridadAlterna($usuario, $idAplicacion);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia">';
        foreach ($listarDependencia as $listarDependencia) {
			if($pkNumDependencia == $listarDependencia['pk_num_dependencia']) {
            $a .= '<option value="' . $listarDependencia['pk_num_dependencia'] . '" selected>' . $listarDependencia['ind_dependencia'] . '</option>';
			} else {
				 $a .= '<option value="' . $listarDependencia['pk_num_dependencia'] . '">' . $listarDependencia['ind_dependencia'] . '</option>';	
			}
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
        echo $a;
    }

    // Método que permite buscar un empleado
    public function metConsultarEmpleado()
    {
        $listarEmpleado = $this->atEquipoModelo->metListarEmpleado();
        $this->atVista->assign('listarEmpleado', $listarEmpleado);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');
    }

    //Metodo que permite cargar un empleado a la vista de registro de empleado en la parte del remitente
    public function metCargarEmpleado()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $consultarEmpleado = $this->atEquipoModelo->metConsultarEmpleado($pkNumEmpleado);
        $datoEmpleado = array(
            'remitente' => $consultarEmpleado['ind_nombre1'].' '.$consultarEmpleado['ind_nombre2'].' '.$consultarEmpleado['ind_apellido1'].' '.$consultarEmpleado['ind_apellido2']
        );
        echo json_encode($datoEmpleado);
    }
}
?>
