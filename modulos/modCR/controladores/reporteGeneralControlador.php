<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase donde se genera el reporte general de consumo de todas las estaciones de trabajo registradas en el sistema
class reporteGeneralControlador extends Controlador
{
    private $atConsumoRed;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.s
		$this->atConsumoRed = $this->metCargarModelo('reporteGeneral');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $js = array('materialSiace/core/demo/DemoTableDynamic', 'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$dateRage[]= 'moment/moment.min';
        $datepickerJs[] = 'bootstrap-datepicker/bootstrap-datepicker';
	
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($datepickerJs);
		$this->atVista->metCargarJsComplemento($dateRage);
        $this->atVista->metCargarJs($js);
		$validarjs= array(
		'jquery-validation/dist/jquery.validate.min',
		'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validarjs);
		$this->atVista->metRenderizar('listadoRed');
    }

	// Método que permite realizar la transformación de consumo de red en kb, Mb, Gb y Tb.
	public function metTransformar($valor)
	{
		$datos = array('total_consumo' => 0,'medida' => 0);
		$bytesTotal = $valor;
		if($bytesTotal >= 1024) {
			$kb = $bytesTotal/1024; // regla de tres (La multiplicacion por 1 no se efectua)
			$totalConsumo = $kb;
			$kbPrueba = explode(".",$kb);
			$kbDefinitivo = $kbPrueba[0];
			$medida = 'Kb';
			if($kbDefinitivo >= 1024) {
				$mb = $kb/1024; // regla de tres (La multiplicacion por 1 no se efectua
				$totalConsumo = $mb;
				$mbPrueba = explode(".",$mb);
				$mbDefinitivo = $mbPrueba[0];
				$medida = 'Mb';
				if($mbDefinitivo >= 1024) {
					$gb = $mb/1024; // regla de tres (La multiplicacion por 1 no se efectua
					$totalConsumo = $gb;
					$gbPrueba = explode(".",$gb);
					$gbDefinitivo = $gbPrueba[0];
					$medida ='Gb';
					if($gbDefinitivo >= 1024) {
						$tb=$gb/1024; // regla de tres (La multiplicacion por 1 no se efectua
						$totalConsumo=$tb;
						$tbPrueba= explode(".",$tb);
						$medida='Tb';
					}// cierre condicional gb
				}// cierre condicional mega
			}//cierre condicional kb
		} else {
			$totalConsumo = $bytesTotal;
			$medida = 'Bytes';
		}
		$datos['total_consumo'] = $totalConsumo;
		$datos['medida'] = $medida;
		return $datos;
	}

	// Método que permite calcular el porcentaje de uso de red
	public function metPorcentajeUso($totalConsumo, $redEquipo)
	{
		$porcentaje = ((100*$redEquipo)/$totalConsumo);
		$porcentajeTotal = number_format($porcentaje,2,',','');
		return $porcentajeTotal;
	}

	// Método que permite generar el reporte general de red
	public function metReporteGeneral()
	{
		//Se requiere el uso de ini_set por la gran cantidad de archivos a procesar para generar el reporte
		ini_set("max_execution_time","9000");
		$usuario= Session::metObtener('idUsuario');
		if(isset($_POST['guardar'])!='') {
			$guardar= $_POST['guardar'];
		} else {
			$guardar =0;
		}
		$obtenerUsuario = $this->atConsumoRed->metUsuarioReporte($usuario);
		$this->metObtenerLibreria('cabeceraEquipo','modCR');
		$pdf = new pdfGeneral('L','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true,10); // establece el margen inferior
		if(isset($_POST['fechaInicio'])!='') {
			$fechaI = $_POST['fechaInicio'];
			$fechaF = $_POST['fechaFin'];
		} else {
			echo "<script languaje='javascript' type='text/javascript'>
            alert('Debe cargar el reporte desde formulario de fechas'); window.close();</script>";
		}
		$fechaExplodeInicio = explode("/",$fechaI);
		$fechaInicio = $fechaExplodeInicio[2].'-'.$fechaExplodeInicio[1].'-'.$fechaExplodeInicio[0];
		$fechaExplodeFin = explode("/",$fechaF);
		$fechaFin = $fechaExplodeFin[2].'-'.$fechaExplodeFin[1].'-'.$fechaExplodeFin[0];
		$fechaInicial = $fechaExplodeInicio[0].'/'.$fechaExplodeInicio[1].'/'.$fechaExplodeInicio[2];
		$fechaFinal = $fechaExplodeFin[0].'/'.$fechaExplodeFin[1].'/'.$fechaExplodeFin[2];
		$numSitios =5;
		$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 220); $pdf->SetTextColor(0, 0, 0);	
		$pdf->Cell(115, 5,'Desde: '.$fechaInicial.'   Hasta: '.$fechaFinal, 1, 0, 'L', 1);
		$pdf->Cell(145, 5,utf8_decode('Elaborado por: '.$obtenerUsuario['ind_nombre1'].' '.$obtenerUsuario['ind_apellido1']), 1, 0, 'L', 1);
		$pdf->SetFont('Arial', 'B', 6);
		$pdf->Ln();
		$consumoTotal = $this->atConsumoRed->metConsumoRed($fechaInicio, $fechaFin);
		$totalConsumo = $consumoTotal['total'];
		$red = $this->metTransformar($totalConsumo);
		$redPrevia = $red['total_consumo'];
		$totalRed = number_format($redPrevia, 2, ',', '');
		$piso = $this->atConsumoRed->metPiso();
		$acumuladorDescarga =0;
		$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(229, 229, 229); $pdf->SetTextColor(0, 0, 0);
		//guardado del reporte principal
		if($guardar==1) {
			$fechaReporte= date('Y-m-d');
			$guardarReporte = $this->atConsumoRed->metGuardarReporte($fechaInicio, $fechaFin, $fechaReporte, $usuario);
		}
		foreach($piso as $piso) {
			$numPisoPrevio = $piso['num_piso'];
			$verificarPiso = $this->atConsumoRed->metVerificarPiso($numPisoPrevio);
			$contadorPiso = count($verificarPiso);
			if($contadorPiso>0) {
				$numPiso = $numPisoPrevio-1;
				$pdf->SetFont('Arial','B',8);
				if($numPiso==0){
					$pdf->Cell(260,5,utf8_decode('Piso: PB'),1, 0, 'L', 1);
				} else {
					$pdf->Cell(260,5,utf8_decode('Piso: '.$numPiso),1, 0, 'L', 1);
				}
				$pdf->Ln();
				$dependencia = $this->atConsumoRed->metDependenciaPiso($numPisoPrevio);
				foreach($dependencia as $dependencia) {
					$dependenciaId = $dependencia['pk_num_dependencia'];
					$pdf->SetFont('Arial','B',8);
					$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
					$dependenciaEquipos=$this->atConsumoRed->metDependenciaEquipos($dependenciaId);
					$contadorEquipos = count($dependenciaEquipos);
					if($contadorEquipos>0) {
					$pdf->Cell(260,5,utf8_decode($dependencia['ind_dependencia']),1,0,'L',1);
					$pdf->Ln();
					$pdf->SetFont('Arial','',8);
					$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(229, 229, 229); $pdf->SetTextColor(0, 0, 0);
					$pdf->Cell(45,5,utf8_decode('Equipo'),1,0,'C',1);
					$pdf->Cell(20,5,utf8_decode('Ip'),1,0,'C',1);
					$pdf->Cell(40,5,utf8_decode('Usuario'),1,0,'L',1);
					$pdf->Cell(25,5,utf8_decode('Descarga'),1,0,'C',1);
					$pdf->Cell(15,5,utf8_decode('Porcentaje'),1,0,'C',1);
					$pdf->Cell(115,5,utf8_decode('Sitios Visitados'),1,0,'C',1);
					$pdf->Ln();					
					foreach($dependenciaEquipos as $dependenciaEquipos) {
						$direccionMysar = $dependenciaEquipos['ind_direccion_fisica'];
						$pkNumEquipo = $dependenciaEquipos['pk_num_equipo'];
						$sitiosVisitados = $this->atConsumoRed->metSitiosVisitados($fechaInicio, $fechaFin, $direccionMysar, $numSitios);
						$totalSitios = count($sitiosVisitados);
						if($totalSitios==0){
							$largo =5;
						} else {
							$largo = $totalSitios*5;
						}
						$pdf->Cell(45,$largo,utf8_decode($dependenciaEquipos['ind_nombre_equipo']),1,0,'L',0);
						$pdf->Cell(20,$largo,utf8_decode($dependenciaEquipos['ind_ip']),1,0,'L',0);
						$pdf->Cell(40,$largo,utf8_decode($dependenciaEquipos['ind_nombre1']).' '.utf8_decode($dependenciaEquipos['ind_apellido1']),1,0,'L',0);
						$consumoEquipo= $this->atConsumoRed->metConsumoEquipo($fechaInicio, $fechaFin, $direccionMysar);
						foreach($consumoEquipo as $consumoEquipo) {
							$redEquipo = $consumoEquipo['consumo'];
							if($redEquipo=='') {
								$redEquipo =0;
							}
							// si la bandera de guardar esta activa
							if($guardar==1) {
								$descargaReporte = $this->atConsumoRed->metGuardarDescarga($pkNumEquipo, $guardarReporte, $redEquipo);
							}
							$todo = $this->metTransformar($redEquipo);
							$total_consumo = $todo['total_consumo'];
							$total = number_format($total_consumo,2,',','');
							$pdf->Cell(25,$largo,$total.' '.$todo['medida'],1,0,'R',0);
							if($totalConsumo==0) {
								$porcentajeEquipo =0;
							} else {
								$porcentajeEquipo = $this->metPorcentajeUso($totalConsumo, $redEquipo);
							}
							$pdf->Cell(15,$largo,$porcentajeEquipo.'%',1,0,'R',0);
								if($totalSitios==0) {
									$pdf->Cell(115,5,'',1,0,'R',0);	
								}
								$registros =0;
								 foreach($sitiosVisitados as $sitiosVisitados) {
											if($registros==0) {
												$pdf->Cell(115,5,utf8_decode($sitiosVisitados['site']),1,0,'R',0);
											}
											if($registros>0) {
												if($registros<$totalSitios) {
													$pdf->Ln();
												}
												$pdf->SetX(155);
												$pdf->Cell(115,5,utf8_decode($sitiosVisitados['site']),1,0,'R',0);			
											 }
											if(($guardar==1)&&($totalSitios>0)) {
												$sitio = $sitiosVisitados['site'];
												$consultarSitios = $this->atConsumoRed->metConsultarSitios($sitio);
												$sitioConsulta = $consultarSitios['pk_num_sitio'];
												// si el equipo no existe entonces se guarda en la tabla de sitios de consumo de red
												if($sitioConsulta==0) {
													$pkNumSitio = $this->atConsumoRed->metGuardarSitios($sitio);
												} else {
													$pkNumSitio = $sitioConsulta;
												}
												$this->atConsumoRed->metDescargaSitios($descargaReporte, $pkNumSitio);
											}
										$registros++;
								   }// fin sitios visitados
									$acumuladorDescarga = $acumuladorDescarga + $redEquipo;
					   } // fin consulta de consumo de equipos
					   $pdf->Ln();
					 
					} // fin consulta equipos dependencias
					   $descarga = $this->metTransformar($acumuladorDescarga);
					   $redDescarga = $descarga['total_consumo'];
					   $totalDescarga = number_format($redDescarga, 2, ',', '');
					   if($totalConsumo==0) {
						   $porcentajeDescarga =0;
						   $uso =0;
					   } else {
							$porcentajeDescarga = $this->metPorcentajeUso($totalConsumo, $acumuladorDescarga);
							$uso ='100';
					   }
					   $pdf->SetFont('Arial', 'B', 10);
					   $pdf->Cell(105,5,'Consumo Total',1,0,'R',0);
					   $pdf->Cell(25,5,$totalDescarga.''.$descarga['medida'],1,0,'R',0);
					   $pdf->Cell(15,5,$porcentajeDescarga.'%',1,0,'R',0);
					   $pdf->Cell(115,5,'',1,0,'R',0);
					   $acumuladorDescarga =0;
					   $pdf->Ln();
					}
				}// fin consulta de dependencias piso
			}
		}// piso
		$pdf->Ln();
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetFillColor(229, 229, 229);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(85,5,'Total',1,0,'C',1);
		$pdf->Cell(35,5,'Descarga',1,0,'C',1);
		$pdf->Cell(35,5,'Uso',1,0,'C',1);
		$pdf->Ln();
		$pdf->Cell(85,5,'Consumo General de Red',1,0,'C',0);
		$pdf->Cell(35,5,$totalRed.' '.$red['medida'],1,0,'C',0);
		$pdf->Cell(35,5,$uso.'%',1,0,'C',0);
	    $pdf->Output();
	}

	// función que permite verificar las fechas recibidas
	public function metVerificarFechas()
	{
		$fechaAux = explode("/",$_POST['fechaInicio']);
		$fechaInicio = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		$fechaAux = explode("/",$_POST['fechaFin']);
		$fechaFin = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		if(strtotime($fechaFin) >= strtotime($fechaInicio)) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}
	
}
?>
