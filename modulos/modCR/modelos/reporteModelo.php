<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Clase encargada de generar las consultas del reporte de consumo de red guardado en el sistema
class reporteModelo extends reporteGeneralModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite consultar el listado de reportes guardados
    public function metMostrarReporte()
    {
        $consultarReporte= $this->_db->query(
            "select a.pk_num_reporte, date_format(a.fec_fecha_inicio, '%d/%m/%Y') as fec_fecha_inicio, date_format(a.fec_fecha_fin, '%d/%m/%Y')
             as fec_fecha_fin,date_format(a.fec_fecha_reporte, '%d/%m/%Y') as fec_fecha_reporte,d.ind_nombre1, d.ind_apellido1 from cr_c006_reporte as a,
             a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario
             and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona"
        );
        $consultarReporte->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return  $consultarReporte->fetchAll();
    }

    // Método que permite eliminar el reporte guardado
    public function metEliminarReporte($pkNumReporte)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
         "delete from cr_c006_reporte where pk_num_reporte='$pkNumReporte'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    // Método que permite consultar los datos de un reporte en particular
    public function metConsultarReporte($pkNumReporte)
    {
        $this->_db->beginTransaction();
        $consultarReporte= $this->_db->query("
        select date_format(a.fec_fecha_reporte, '%d/%m/%Y') as fechaReporte,date_format(a.fec_fecha_inicio, '%d/%m/%Y') as fechaInicio,date_format(a.fec_fecha_fin, '%d/%m/%Y') as fechaFin, d.ind_nombre1, d.ind_apellido1
        from cr_c006_reporte as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d
        where a.pk_num_reporte='$pkNumReporte' and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado
        and c.fk_a003_num_persona=d.pk_num_persona
        ");
        $consultarReporte->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarReporte->fetch();
    }

    // Método que consulta y calcula el consumo total de red por parte de todos los equipos asociados al reporte guardado previamente seleccionado
    public function metConsumoTotal($pkNumReporte)
    {
        $consumoTotal=$this->_db->query(
            "select sum(num_descarga_total) as totalConsumo from cr_c003_descarga_equipo where fk_crc006_num_reporte='$pkNumReporte'"
        );
        $consumoTotal->setFetchMode(PDO::FETCH_ASSOC);
        return $consumoTotal->fetch();
    }

    // Método encargado de consultar el piso de las dependencias guardadas en el reporte
    public function metDependenciaReporte($numPisoPrevio, $pkNumReporte)
    {
        $consultarDependencia= $this->_db->query(
        "select c.ind_dependencia, c.pk_num_dependencia from cr_c003_descarga_equipo as a, cr_b001_equipo as b, a004_dependencia as c
        where a.fk_crc006_num_reporte='$pkNumReporte' and a.fk_crb001_num_equipo=b.pk_num_equipo and b.fk_a004_num_dependencia=c.pk_num_dependencia and c.num_piso='$numPisoPrevio'
        group by c.pk_num_dependencia"
        );
        $consultarDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarDependencia->fetchAll();
    }

    // Método que permite obtener una dependencia en particular asociada a un reporte guardado
    public function metEquipoReporte($dependenciaId, $pkNumReporte)
    {
        $equipoReporte=$this->_db->query(
        "select b.pk_num_equipo, b.ind_nombre_equipo, b.ind_ip, f.ind_nombre1, f.ind_apellido1 from cr_c003_descarga_equipo as a, cr_b001_equipo as b, a004_dependencia as c, cr_c002_usuario_equipo as d, rh_b001_empleado as e, a003_persona as f
        where a.fk_crc006_num_reporte='$pkNumReporte' and a.fk_crb001_num_equipo=b.pk_num_equipo and b.fk_a004_num_dependencia=c.pk_num_dependencia and c.pk_num_dependencia='$dependenciaId'
        and b.pk_num_equipo=d.pk_num_equipo and d.fk_rhb001_num_empleado=e.pk_num_empleado and e.fk_a003_num_persona=f.pk_num_persona"
        );
        $equipoReporte->setFetchMode(PDO::FETCH_ASSOC);
        return $equipoReporte->fetchAll();
    }

    // Método que permite calcular el consumo de un equipo asociado a un reporte
    public function metDescargaEquipo($pkNumEquipo ,$pkNumReporte)
    {
        $consumoDescarga= $this->_db->query(
            "select num_descarga_total from cr_c003_descarga_equipo where fk_crb001_num_equipo='$pkNumEquipo' and fk_crc006_num_reporte='$pkNumReporte'"
        );
        $consumoDescarga->setFetchMode(PDO::FETCH_ASSOC);
        return $consumoDescarga->fetch();
    }

    // Método que permite consultar los sitios visitados por un equipo asociado a un reporte
    public function metSitiosDescarga($pkNumEquipo ,$pkNumReporte)
    {
        $sitiosDescarga=$this->_db->query("
        select c.ind_nombre_sitio from cr_c003_descarga_equipo as a, cr_c004_descarga_sitio as b, cr_c005_sitio as c
        where a.fk_crb001_num_equipo='$pkNumEquipo' and a.fk_crc006_num_reporte='$pkNumReporte' and a.pk_num_descarga=b.fk_crc003_num_descarga
        and b.fk_crc005_num_sitio=c.pk_num_sitio
        ");
        $sitiosDescarga->setFetchMode(PDO::FETCH_ASSOC);
        return $sitiosDescarga->fetchAll();
    }
}
?>