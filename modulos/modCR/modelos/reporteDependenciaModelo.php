<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Clase que contiene las consultas correspondientes al reporte de consumo de red por dependencias
class reporteDependenciaModelo extends reporteGeneralModelo
{
    public function __construct() 
    {
        parent::__construct();
    }

    // Método que permite listar las dependencias
    public function metListarDependencia($usuario, $idAplicacion)
    {
        // Consulto las dependencias de la contraloría y se las asigno a la variable $dependencia
        $dependencia = $this->_db->query("select b.pk_num_dependencia, b.ind_dependencia from a019_seguridad_dependencia as a, a004_dependencia as b where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_a018_num_seguridad_usuario=$usuario and a.fk_a015_num_seguridad_aplicacion=$idAplicacion");
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $dependencia->fetchAll();
    }

    // Método que permite obtener una dependencia en específica
   public function metDependenciaListado($pkNumDependencia)
   {
        // Selecciono la dependencia asociada al equipo
        $dependencia = $this->_db->query(
            "SELECT ind_dependencia FROM a004_dependencia where pk_num_dependencia = '$pkNumDependencia'"
        );
        // Asigno el nombre de la dependencia a la variable nombre
        $nombre = $dependencia->fetch();
        //retorno el valor de la posicion 0 de la consulta
        return $nombre[0];
    }
}
?>
