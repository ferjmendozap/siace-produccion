<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de pasillos 
class pasilloControlador extends Controlador
{
	private $atPasilloModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atPasilloModelo = $this->metCargarModelo('almacen');
		$this->atPasilloModelo = $this->metCargarModelo('pasillo');
	}

	//Método Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('_PruebaPost', $this->atPasilloModelo->metListarPasillo());
		$this->atVista->metRenderizar('listado');
	}

	// Método que permite registrar un pasillo
	public function metNuevoPasillo()
	{
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$ind_descripcion_pasillo = $this->metObtenerTexto('ind_descripcion_pasillo');
			$ind_pasillo = $this->metObtenerTexto('ind_pasillo');
			$pk_num_almacen = $this->metObtenerInt('pk_num_almacen');
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$pkNumPasillo=$this->atPasilloModelo->metGuardarPasillo($ind_pasillo, $ind_descripcion_pasillo, $pk_num_almacen, $fecha_hora ,$usuario);
			$ind_descripcion_almacen= $this->atPasilloModelo->metAlmacenDescripcion($pk_num_almacen);
			$ind_ubicacion_almacen= $this->atPasilloModelo->metAlmacenUbicacion($pk_num_almacen);
			$arrayPost = array(
				'ind_pasillo' => $ind_pasillo,
				'ind_descripcion_pasillo' => $ind_descripcion_pasillo,
				'ind_descripcion_almacen' => $ind_descripcion_almacen,
				'ind_ubicacion_almacen' => $ind_ubicacion_almacen,
				'pk_num_pasillo' => $pkNumPasillo
			);
			echo json_encode($arrayPost);
			exit;
		}
			$form = array(
				'status' => 'nuevo',
				'ind_pasillo' => null,
				'ind_descripcion_pasillo' => null,
				'ind_descripcion_almacen' => null,
				'ind_ubicacion_almacen' => null,
			);
			$almacen=$this->atPasilloModelo->metListarAlmacen();
			$this->atVista->assign('almacen',$almacen);
			$this->atVista->assign('form', $form);
			$this->atVista->metRenderizar('nuevo', 'modales');
	}

	// Método que permite editar un pasillo
	public function metEditarPasillo()
	{
		$pkNumPasillo = $this->metObtenerInt('pk_num_pasillo');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$ind_descripcion_pasillo = $this->metObtenerTexto('ind_descripcion_pasillo');
			$ind_pasillo = $this->metObtenerTexto('ind_pasillo');
			$pk_num_almacen= $this->metObtenerInt('pk_num_almacen');
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atPasilloModelo->metEditarPasillo($ind_pasillo, $ind_descripcion_pasillo, $pk_num_almacen, $fecha_hora,$usuario, $pkNumPasillo);
			$ind_ubicacion_almacen= $this->atPasilloModelo->metAlmacenUbicacion($pk_num_almacen);
			$ind_descripcion_almacen= $this->atPasilloModelo->metAlmacenDescripcion($pk_num_almacen);
			$arrayPost = array(
				'status' => 'modificar',
				'ind_descripcion_pasillo' => $ind_descripcion_pasillo,
				'ind_descripcion_almacen' => $ind_descripcion_almacen,
				'ind_ubicacion_almacen' => $ind_ubicacion_almacen,
				'ind_pasillo' => $ind_pasillo,
				'pk_num_almacen' => $pk_num_almacen,
				'pk_num_pasillo' =>  $pkNumPasillo
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumPasillo)) {
			$metodo=1;
			$form = $this->atPasilloModelo->metVerPasillo($pkNumPasillo, $metodo);
			$nombreCompleto= $form['ind_nombre1'].' '.$form['ind_apellido1'];
			$form = array(
				'status' => 'modificar',
				'pk_num_pasillo' => $pkNumPasillo,
				'ind_pasillo' => $form['ind_pasillo'],
				'ind_descripcion_pasillo' => $form['ind_descripcion_pasillo'],
				'fecha' => $form['fecha'],
				'nombreCompleto' => $nombreCompleto,
				'pk_num_almacen' => $form['pk_num_almacen']
			);
			$almacen=$this->atPasilloModelo->metListarAlmacen();
			$this->atVista->assign('almacen',$almacen);
			$this->atVista->assign('form', $form);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	//Método que permite eliminar un pasillo
	public function metEliminarPasillo()
	{
		$pkNumPasillo = $this->metObtenerInt('pk_num_pasillo');
		$this->atPasilloModelo->metEliminarPasillo($pkNumPasillo);
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_pasillo' => $pkNumPasillo
		);
		echo json_encode($arrayPost);
	}

	// Método que permite visualizar un pasillo
	public function metVerPasillo()
	{
		$pkNumPasillo = $this->metObtenerInt('pk_num_pasillo');
		$metodo=2;
		$pasillo=$this->atPasilloModelo->metVerPasillo($pkNumPasillo, $metodo);
		$this->atVista->assign('pasillo',$pasillo);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	// Método que permite ver el reporte de los pasillos
	public function metReportePasillo()
	{
		$usuario = Session::metObtener('idUsuario');
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfPasillo('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetFont('Arial','',10);
		$pdf->SetX(18);
		$almacen= $this->atPasilloModelo->metListarAlmacen();
		$contadorAlmacen = count($almacen);
		if($contadorAlmacen>0){
			foreach($almacen as $almacen) {
				$pkNumAlmacen=$almacen['pk_num_almacen'];
				$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
				$pdf->SetWidths(array(185));
				$pdf->Row(array(
					utf8_decode('Almacén: ').''.utf8_decode($almacen['ind_descripcion_almacen']).'         '.utf8_decode('Ubicación: ').' '.utf8_decode($almacen['ind_ubicacion_almacen'])
				), 5);
				$pasillo= $this->atPasilloModelo->metPasilloAlmacen($pkNumAlmacen);
				foreach($pasillo as $pasillo) {
					$pdf->SetWidths(array(60, 125));
					$pdf->Row(array(
						utf8_decode($pasillo['ind_pasillo']),
						utf8_decode($pasillo['ind_descripcion_pasillo'])
					), 5);
				}
			}
		}
		$pdf->Output();
	}

	// Método que permite verificar la existencia de un pasillo
	public function metVerificarPasillo()
	{
		$ind_pasillo = $this->metObtenerTexto('ind_pasillo');
		$pk_num_almacen = $this->metObtenerInt("pk_num_almacen");
		$pk_num_pasillo = $this->metObtenerInt("pk_num_pasillo");
		$pasilloExiste = $this->atPasilloModelo->metBuscarPasillo($ind_pasillo, $pk_num_almacen, $pk_num_pasillo);
		echo json_encode(!$pasilloExiste);
	}
}
?>
