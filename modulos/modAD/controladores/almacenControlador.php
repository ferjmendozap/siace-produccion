<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital.
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de almacenes 
class almacenControlador extends Controlador
{
	private $atAlmacenModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atAlmacenModelo = $this->metCargarModelo('almacen');
	}

	// Método index del controlador
	public function metIndex(){
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef'
		);
		$complementosJs = array('select2/select2.min');
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('_PruebaPost', $this->atAlmacenModelo->metListarAlmacen());
		$this->atVista->metRenderizar('listado');
	}

	//Método que permite registrar un nuevo almacén
	public function metNuevoAlmacen()
	{
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$ind_descripcion_almacen = $this->metObtenerTexto('ind_descripcion_almacen');
			$ind_ubicacion_almacen = $this->metObtenerTexto('ind_ubicacion_almacen');
			$pkNumEmpleado = $this->metObtenerInt('pk_num_empleado');
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$pkNumAlmacen=$this->atAlmacenModelo->metGuardarAlmacen($ind_descripcion_almacen, $ind_ubicacion_almacen, $pkNumEmpleado, $fecha_hora ,$usuario);
			$nombre_usuario = $this->atAlmacenModelo->metUsuarioAlmacen($pkNumEmpleado);
			$arrayPost = array(
				
				'ind_descripcion_almacen' => $ind_descripcion_almacen,
				'ind_ubicacion_almacen' => $ind_ubicacion_almacen,
				'empleado'=> $nombre_usuario,
				'pk_num_almacen' => $pkNumAlmacen
			);
			echo json_encode($arrayPost);
			exit;
		}
		$form = array(
			'status' => 'nuevo',
			'ind_descripcion_almacen' => null,
			'ind_ubicacion_almacen' => null,
			'fk_a003_num_persona'=> null
		);
		$usuario=$this->atAlmacenModelo->metListarUsuario();
		$this->atVista->assign('usuario',$usuario);
		$this->atVista->assign('form', $form);
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	//Método que permite editar un almacén
	public function metEditarAlmacen()
	{
		$pkNumAlmacen = $this->metObtenerInt('pk_num_almacen');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$ind_descripcion_almacen = $this->metObtenerTexto('ind_descripcion_almacen');
			$ind_ubicacion_almacen = $this->metObtenerTexto('ind_ubicacion_almacen');
			$pkNumEmpleado = $this->metObtenerInt('pk_num_empleado');
			$this->metValidarToken();
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atAlmacenModelo->metEditarAlmacen($ind_descripcion_almacen, $ind_ubicacion_almacen, $pkNumEmpleado, $fecha_hora,$usuario, $pkNumAlmacen);
			$nombre_usuario = $this->atAlmacenModelo->metUsuarioAlmacen($pkNumEmpleado);
			$arrayPost = array(
				'status' => 'modificar',
				'ind_descripcion_almacen' => $ind_descripcion_almacen,
				'ind_ubicacion_almacen' => $ind_ubicacion_almacen,
				'empleado' => $nombre_usuario,
				'pk_num_almacen' =>  $pkNumAlmacen
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumAlmacen)) {
			$form = $this->atAlmacenModelo->metVerAlmacen($pkNumAlmacen, 1);
			$fk_a018_num_seguridad_usuario = $form['fk_a018_num_seguridad_usuario'];
			$ultimoUsuario = $this->atAlmacenModelo->metUsuarioReporte($fk_a018_num_seguridad_usuario);
			$nombreCompleto= $ultimoUsuario['ind_nombre1'].' '.$ultimoUsuario['ind_apellido1'];
			$form = array(
				'status' => 'modificar',
				'ind_descripcion_almacen' => $form['ind_descripcion_almacen'],
				'ind_ubicacion_almacen' => $form['ind_ubicacion_almacen'],
				'pk_num_empleado' => $form['pk_num_empleado'],
				'fecha' => $form['fecha'],
				'nombreCompleto' => $nombreCompleto,
				'pk_num_almacen' => $pkNumAlmacen
			);
			$usuario=$this->atAlmacenModelo->metListarUsuario();
			$this->atVista->assign('usuario',$usuario);
			$this->atVista->assign('form', $form);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	//Método que permite eliminar un almacén
	public function metEliminarAlmacen()
	{
		$pkNumAlmacen = $this->metObtenerInt('pk_num_almacen');
		$this->atAlmacenModelo->metEliminarAlmacen($pkNumAlmacen);
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_almacen' => $pkNumAlmacen
		);
		echo json_encode($arrayPost);
	}

	//Método que permite visualizar un almacén en específico
	public function metVerAlmacen()
	{
		$pkNumAlmacen = $this->metObtenerInt('pk_num_almacen');
		$almacen=$this->atAlmacenModelo->metVerAlmacen($pkNumAlmacen, 2);
		$usuario = $this->atAlmacenModelo->metVerUsuario($pkNumAlmacen);
		$this->atVista->assign('usuario',$usuario);
		$this->atVista->assign('almacen',$almacen);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	//Método que permite ver el reporte de los almacenes registrados
	public function metReporteAlmacen()
	{
		$usuario = Session::metObtener('idUsuario');
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfAlmacen('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetFont('Arial','',10);
		$pdf->SetWidths(array(30, 65, 40, 58));
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetTextColor(0, 0, 0);
		$almacen= $this->atAlmacenModelo->metListarAlmacen();
		foreach($almacen as $almacen) {
			$pdf->Row(array(
				utf8_decode($almacen['pk_num_almacen']),
				utf8_decode($almacen['ind_descripcion_almacen']),
				utf8_decode($almacen['ind_ubicacion_almacen']),
				utf8_decode($almacen['ind_nombre1'].' '.$almacen['ind_apellido1'])
			), 5);
			$pdf->Ln();
		}
		$pdf->Output();
	}

	// Método que permite verificar la existencia de un almacen en el sistema
	public function metVerificarAlmacen(){
		$ind_descripcion_almacen = $this->metObtenerTexto('ind_descripcion_almacen');
		$pk_num_almacen = $this->metObtenerInt("pk_num_almacen");
		$almacenExiste = $this->atAlmacenModelo->metBuscarAlmacen($ind_descripcion_almacen, $pk_num_almacen);
		echo json_encode(!$almacenExiste);
	}
}
?>
