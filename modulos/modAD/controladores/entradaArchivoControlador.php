<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/ 
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de entradas de documentos de retorno al almacén
class entradaArchivoControlador extends Controlador
{
	private $atEntradaModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atEntradaModelo = $this->metCargarModelo('entradaArchivo');
	}

	// Método index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c',
			'wizard/wizardfa6c',
			'lightGallery-master/dist/css/lightgallery.min'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
			'multi-select/jquery.multi-select',
			'wizard/jquery.bootstrap.wizard.min',
			'lightGallery-master/dist/js/lightgallery.min',
			'lightGallery-master/dist/js/lg-thumbnail.min',
			'lightGallery-master/dist/js/lg-fullscreen.min'
		);
		$js = array(
			'Aplicacion/appFunciones',
			'materialSiace/core/demo/DemoTableDynamic',
			'materialSiace/core/demo/DemoFormWizard',
			'materialSiace/App',
			'materialSiace/core/demo/DemoFormComponents'
		);

		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$metodoOrdenar = 1;
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atEntradaModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$listadoOrganismo = $this->atEntradaModelo->metListarOrganismo(0, 1);
		$datosEmpleado = $this->atEntradaModelo->metEmpleado($pkNumEmpleado);
		$pkNumDependencia = $datosEmpleado['pk_num_dependencia'];
		$listadoEmpleados = $this->atEntradaModelo->metDependenciaEmpleado($pkNumDependencia);
		$this->atVista->assign('empleado', $listadoEmpleados);
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
		$listadoDependencia = $this->atEntradaModelo->metListarDependencia($datosEmpleado['pk_num_organismo'], 1, 0, $usuario, $idAplicacion);
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		// Organismo donde trabaja el empleado
		$empleado = array(
			'pk_num_organismo' => $datosEmpleado['pk_num_organismo']
		);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$this->atVista->assign('_PruebaPost', $this->atEntradaModelo->metListarEntrada($metodoOrdenar, 0));
		$this->atVista->metRenderizar('listado');
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
		$listarDependencia=$this->atEntradaModelo->metConsultarSeguridadAlterna($usuario, $idAplicacion, $pkNumOrganismo);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar las salidas de los documentos
	public function metListarSalida()
	{
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
		$this->atVista->assign('salida', $this->atEntradaModelo->metListarSalida($usuario, $idAplicacion));
		$this->atVista->metRenderizar('listadoSalida', 'modales');
	}

	// Método que permite registrar una nueva entrada
	public function metNuevaEntrada()
	{
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$pk_num_salida = $_POST['pk_num_salida'];
			$ind_observacion = $_POST['ind_observacion'];
			$ind_documento = $_POST['ind_documento'];
			$fecha_entrada = $_POST['fecha_entrada'];
			$fechaExplodeEntrada = explode("/", $fecha_entrada);
			$fechaEntrada = $fechaExplodeEntrada[2] . '-' . $fechaExplodeEntrada[1] . '-' . $fechaExplodeEntrada[0];
			$cod_memo = $_POST['cod_memo'];
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$obtenerUsuario = $this->atEntradaModelo->metUsuario($usuario, 1);
			$funcionario = $obtenerUsuario['pk_num_empleado'];
			$pkNumEntrada = $this->atEntradaModelo->metGuardarEntrada($pk_num_salida, $ind_observacion, $cod_memo, $fechaEntrada, $funcionario, $fecha_hora, $usuario);
			$arrayPost = array(
				'ind_estado' => 'PREPARADO',
				'fecha_entrada' => $fecha_entrada,
				'cod_memo' => $cod_memo,
				'ind_documento' => $ind_documento,
				'pk_num_entrada' => $pkNumEntrada
			);
			echo json_encode($arrayPost);
			exit;
		}
		$pkNumSalida = $this->metObtenerInt('pk_num_salida');
		$visualizarSalida = $this->atEntradaModelo->metVerSalida($pkNumSalida, 1);
		$verAcceso = $this->atEntradaModelo->metAcceso($pkNumSalida, 1);
		$this->atVista->assign('acceso', $verAcceso);
		$this->atVista->assign('sal', $visualizarSalida);
		$pkNumRegistroDocumento = $visualizarSalida['fk_adb001_num_registro_documento'];
		$registro=$this->atEntradaModelo->metVisualizarRegistro($pkNumRegistroDocumento, 1);
		$this->atVista->assign('registro',$registro);
		$numFlagArchivo = $registro['num_flag_archivo'];
		if($numFlagArchivo==1){
            $archivo = $this->atEntradaModelo->metBuscarArchivo($pkNumRegistroDocumento);
            $this->atVista->assign('archivo', $archivo);
            $ruta = 'publico/imagenes/modAD/';
            $explodeArchivo = explode(".", $archivo['ind_nombre_archivo']);
            $extension = $explodeArchivo[1];
            $rutaArchivo = array(
                'ruta' => $ruta,
                'extension' => $extension
            );
            $this->atVista->assign('ruta', $rutaArchivo);
		}
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	// Método que permite ver una entrada en particular
	public function metVerEntrada()
	{
		$pkNumEntrada = $this->metObtenerInt('pk_num_entrada');
		$entrada = $this->atEntradaModelo->metVisualizarEntrada($pkNumEntrada, 1);
		$this->atVista->assign('ent', $entrada);
		$verAcceso = $this->atEntradaModelo->metAcceso($pkNumEntrada, 2);
		$this->atVista->assign('acceso', $verAcceso);
		// Consultar Salida
		$pkNumSalida = $entrada['fk_adb002_num_salida'];
		$visualizarSalida = $this->atEntradaModelo->metVerSalida($pkNumSalida, 1);
		$pkNumRegistroDocumento = $visualizarSalida['fk_adb001_num_registro_documento'];
		$registro=$this->atEntradaModelo->metVisualizarRegistro($pkNumRegistroDocumento, 1);
		$this->atVista->assign('registro',$registro);
        $numFlagArchivo = $registro['num_flag_archivo'];
        if($numFlagArchivo==1){
            $archivo = $this->atEntradaModelo->metBuscarArchivo($pkNumRegistroDocumento);
            $this->atVista->assign('archivo', $archivo);
            $ruta = 'publico/imagenes/modAD/';
            $explodeArchivo = explode(".", $archivo['ind_nombre_archivo']);
            $extension = $explodeArchivo[1];
            $rutaArchivo = array(
                'ruta' => $ruta,
                'extension' => $extension
            );
            $this->atVista->assign('ruta', $rutaArchivo);
		}
		$this->atVista->metRenderizar('ver', 'modales');
	}

	// Metodo que permite listar las entradas de documentos al almacén
	public function metListarEntrada()
	{
		$valorEntrada = $_GET['valor'];
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'bootstrap-datepicker/datepicker'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$dateRage[] = 'moment/moment.min';
		$datepickerJs[] = 'bootstrap-datepicker/bootstrap-datepicker';

		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($datepickerJs);
		$this->atVista->metCargarJsComplemento($dateRage);
		$this->atVista->metCargarJs($js);
		$validarjs = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validarjs);
		if($valorEntrada!='Todos') {
			$metodoOrdenar = 2;
		} else {
			$metodoOrdenar = 1;
		}
		$this->atVista->assign('_PruebaPost', $this->atEntradaModelo->metListarEntrada($metodoOrdenar, $valorEntrada));
		$this->atVista->metRenderizar('listado');
	}

	// Método que permite eliminar las entradas
	public function metEliminarEntrada()
	{
		$pkNumEntrada = $this->metObtenerInt('pk_num_entrada');
		$this->atEntradaModelo->metEliminarEntrada($pkNumEntrada);
		$listarEntrada = $this->atEntradaModelo->metListarEntrada(1, 0);
		echo json_encode($listarEntrada);
	}

	// Método que permite editar entradas de documentos
	public function metEditarEntrada()
	{
		$pkNumEntrada = $this->metObtenerInt('pk_num_entrada');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$ind_observacion = $_POST['ind_observacion'];
			$fecha_entrada = $_POST['fecha_entrada'];
			$fechaExplodeEntrada = explode("/", $fecha_entrada);
			$fechaEntrada = $fechaExplodeEntrada[2] . '-' . $fechaExplodeEntrada[1] . '-' . $fechaExplodeEntrada[0];
			$cod_memo = $_POST['cod_memo'];
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atEntradaModelo->metEditarEntrada($ind_observacion, $cod_memo, $fechaEntrada, $fecha_hora, $usuario, $pkNumEntrada);
			$entrada = $this->atEntradaModelo->metVisualizarEntrada($pkNumEntrada, 1);
			$ind_documento = $entrada['ind_documento'];
			$estadoPrevio = $entrada['ind_estado'];
			if($estadoPrevio=='PR'){
				$estado = 'PREPARADO';
			}
			if ($estadoPrevio=='AP') {
				$estado = 'APROBADO';
			}
			if ($estadoPrevio=='AN') {
				$estado = 'ANULADO';
			}
			$arrayPost = array(
				'status' => 'modificar',
				'ind_observacion' => $ind_observacion,
				'ind_documento' => $ind_documento,
				'cod_memo' => $cod_memo,
				'fecha_entrada' => $fechaEntrada,
				'ind_estado' => $estado,
				'pk_num_entrada' => $pkNumEntrada
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumEntrada)) {
			$form = $this->atEntradaModelo->metVisualizarEntrada($pkNumEntrada, 1);
			$verAcceso = $this->atEntradaModelo->metAcceso($pkNumEntrada, 2);
			$this->atVista->assign('acceso', $verAcceso);
			$this->atVista->assign('ent', $form);
			// Consultar Salida
			$pkNumSalida = $form['fk_adb002_num_salida'];
			$visualizarSalida = $this->atEntradaModelo->metVerSalida($pkNumSalida, 1);
			$salida = array(
				'pk_num_salida' =>$pkNumSalida
			);
			$pkNumRegistroDocumento = $visualizarSalida['fk_adb001_num_registro_documento'];
			$registro=$this->atEntradaModelo->metVisualizarRegistro($pkNumRegistroDocumento, 1);
			$this->atVista->assign('registro',$registro);
			$this->atVista->assign('sal',$salida);
            $numFlagArchivo = $registro['num_flag_archivo'];
            if($numFlagArchivo==1){
                $archivo = $this->atEntradaModelo->metBuscarArchivo($pkNumRegistroDocumento);
                $this->atVista->assign('archivo', $archivo);
                $ruta = 'publico/imagenes/modAD/';
                $explodeArchivo = explode(".", $archivo['ind_nombre_archivo']);
                $extension = $explodeArchivo[1];
                $rutaArchivo = array(
                    'ruta' => $ruta,
                    'extension' => $extension
                );
                $this->atVista->assign('ruta', $rutaArchivo);
			}
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que permite verificar que la fecha de entrada no sea menor que la fecha de salida
	public function metVerificarFecha()
	{
		$fecha_entrada = $_POST['fechaEntrada'];
		$fechaExplodeEntrada = explode("/", $fecha_entrada);
		$fechaEntrada = $fechaExplodeEntrada[2] . '-' . $fechaExplodeEntrada[1] . '-' . $fechaExplodeEntrada[0];
		$pkNumSalida = $_POST['pkNumSalida'];
		$fechaValidar = $this->atEntradaModelo->metVerSalida($pkNumSalida, 1);
		$fechaSalida = $fechaValidar['fec_fecha_salida'];
		if($fechaEntrada<$fechaSalida) {
			$fecha = true;
		} else {
			$fecha = false;
		}
		$comprobarFecha = array(
			'fecha' => $fecha
		);
		echo json_encode($comprobarFecha);
	}

	// Método que permite cambiar el estatus de una entrada
	public function metEstatus()
	{
		$pkNumEntrada = $_POST['pk_num_entrada'];
		$valor = $_POST['valor'];
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atEntradaModelo->metUsuario($usuario, 1);
		$idEmpleado = $empleado['pk_num_empleado'];
		$fecha_hora = date('Y-m-d H:i:s');
		$this->atEntradaModelo->metCambiarEstatus($pkNumEntrada, $valor, $idEmpleado, $fecha_hora);
		$listarEntrada = $this->atEntradaModelo->metListarEntrada(1, 0);
		echo json_encode($listarEntrada);
	}

	// Método que permite cargar la pantalla de búsqueda de la entrada
	public function metBuscarEntrada()
	{
		$complementosCss = array(
			'bootstrap-datepicker/datepicker',
		);
		$complementosJs = array(
			'bootstrap-datepicker/bootstrap-datepicker'
		);
		$js = array(
			'materialSiace/App',
			'materialSiace/core/demo/DemoFormComponents'
		);
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$this->atVista->metRenderizar('buscarEntrada', 'modales');
	}

	// Método que permite verificar que fecha es mayor
	public function metVerificarFechas()
	{
		$fechaAux = explode("/",$_POST['fechaInicio']);
		$fechaInicio = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		$fechaAux = explode("/",$_POST['fechaFin']);
		$fechaFin = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		if(strtotime($fechaFin) >= strtotime($fechaInicio)) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	// Método que permite cargar el resultado de la búsqueda
	public function metBuscarFecha()
	{
		$fecha_inicio = $_POST['fechaInicio'];
		$fechaInicioExplode = explode ("/", $fecha_inicio);
		$fechaInicio = $fechaInicioExplode[2].'-'.$fechaInicioExplode[1].'-'.$fechaInicioExplode[0];
		$fecha_fin = $_POST['fechaFin'];
		$fechaFinExplode = explode ("/", $fecha_fin);
		$fechaFin = $fechaFinExplode[2].'-'.$fechaFinExplode[1].'-'.$fechaFinExplode[0];
		$validarFechas = $this->atEntradaModelo->metBuscarEntrada($fechaInicio, $fechaFin);
		$total = count($validarFechas);
		if ($total>0) {
			$this->metReporteEntrada($fechaInicio, $fechaFin);
		} else {
			echo '
	        <link rel="stylesheet" type="text/css" href="'.BASE_URL.'publico'.DS.'complementos'.DS.'bootstrap'.DS.'bootstrap_v3.3.4.css">
            <br/><div class="alert alert-danger" align="center">No se encuentran resultados para las fechas dadas</div>';
		}

	}

	// Método que permite ver en reporte las entradas entre dos fechas dadas
	public function metReporteEntrada($fechaInicio, $fechaFin)
	{
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfEntrada('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetFont('Arial','',8);
		$listarEntrada = $this->atEntradaModelo->metBuscarEntrada($fechaInicio, $fechaFin);
		$pdf->SetWidths(array(12.5, 12.5, 20, 25, 38, 20, 20, 48));
		foreach($listarEntrada as $listarEntrada)
		{
			$estado = $listarEntrada['ind_estado'];
			if($estado=='PR'){
				$valorEstado = 'PREPARADO';
			} 
			if($estado=='AP'){
				$valorEstado = 'APROBADO';
			} 
			if($estado=='AN'){
				$valorEstado = 'ANULADO';
			} 
			$pdf->Row(array(
				$listarEntrada['pk_num_entrada'],
				$listarEntrada['num_registro'],
				$valorEstado,
				$listarEntrada['ind_documento'],
				$listarEntrada['cod_memo'],
				$listarEntrada['fecha_salida'],
				$listarEntrada['fecha_entrada'],
				$listarEntrada['ind_observacion']
			), 5);
		}
		$pdf->Output();
	}

	//Método que permite listar la busqueda de la entrada
	public function metConsultarEntrada() 
	{
		$pkNumOrganismo = $_POST['pk_num_organismo'];
		$pkNumDependencia = $_POST['pk_num_dependencia'];
		$indEstado = $_POST['ind_estado'];
		$fecha_inicio = $_POST['fecha_inicio'];
		$fecha_fin = $_POST['fecha_fin'];
		$datosEntrada = $this->atEntradaModelo->metConsultarEntrada($pkNumOrganismo, $pkNumDependencia, $fecha_inicio, $fecha_fin, $indEstado);
		echo json_encode($datosEntrada);
	}

	// Método que permite ver un expediente en particular
	public function metVerExpediente()
	{
		$num_registro = $_GET['num_registro'];
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$path = ROOT.'publico/imagenes/modAD/'.$num_registro.'/';
		$pdf= new pdfCargarRegistro('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->Ln();
		$dir = scandir($path);
		for($i=2;$i<count($dir);$i++) {
			$pdf->AddPage();
			$pdf->Image($path.$dir[$i],1,10,200);
			$pdf->Ln();
		}
		$pdf->Output();
	}
}
?>
