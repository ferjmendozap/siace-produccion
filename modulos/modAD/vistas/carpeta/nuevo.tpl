<form id="formAjax" action="{$_Parametros.url}modAD/carpetaCONTROL/NuevaCarpetaMET" class="form floating-label" novalidate="novalidate">
<input type="hidden" name="valido" value="1" />
<div class="modal-body">
	<div class="col-md-6 col-sm-6">
		<div class="form-group floating-label">
			<select id="pk_num_almacen" name="pk_num_almacen" class="form-control" onchange="cargarPasillo(this.value)">
				<option value="">&nbsp;</option>
				{foreach item=us from=$almacen}
					<option value="{$us.pk_num_almacen}">{$us.ind_descripcion_almacen}</option>
				{/foreach}
			</select>
			<label for="pk_num_almacen"><i class="glyphicon glyphicon-home"></i> Almacén</label>
		</div>
		<div id="pasillo">
			<div class="form-group floating-label">
				<select class="form-control">
					<option value="">&nbsp;</option>
				</select>
				<label><i class="glyphicon glyphicon-menu-hamburger"></i> Pasillo</label>
			</div>
		</div>
		<div id="estante">
			<div class="form-group floating-label">
				<select class="form-control">
					<option value="">&nbsp;</option>
				</select>
				<label><i class="glyphicon glyphicon-th-large"></i> Estante</label>
			</div>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control"  name="ind_descripcion_carpeta" id="ind_descripcion_carpeta">
			<label for="ind_descripcion_carpeta"><i class="glyphicon glyphicon-pencil"></i> Descripción </label>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="col-md-12 col-sm-12">
			<div class="form-group floating-label">
				<select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control" onchange="cargarCajas(this.value)">
						<option value="">&nbsp;</option>
                        {foreach item=dep from=$dependencia}
							<option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                        {/foreach}
				</select>
				<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
			</div>
		</div>
		<div class="col-md-9 col-sm-9">
			<div class="col-lg-2">
				<label class="checkbox-styled" style="margin-top: 20px;">
					<input type="checkbox" id="checkCaja" class="form-control">
					<span></span>
				</label>
			</div>
			<div class="col-lg-10">
			<div class="form-group floating-label">
				<select id="pk_num_caja" name="pk_num_caja" class="form-control" disabled>
					<option value="">&nbsp;</option>
				</select>
				<label for="pk_num_caja"><i class="glyphicon glyphicon-briefcase"></i> Caja</label>
			</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class="form-group floating-label">
                {$foo = $desde}
				<select id="fec_anio" name="fec_anio" class="form-control">
					<option value="">&nbsp;</option>
					{while $foo>1899}
						<option value="{$foo}">{$foo}</option>
						{$foo--}
					{/while}
				</select>
				<label for="fec_anio"><i class="glyphicon glyphicon-briefcase"></i> Año</label>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<div class="form-group floating-label">
				<select id="fk_a006_miscelaneo_tramo" name="fk_a006_miscelaneo_tramo"  class="form-control">
					<option value="">&nbsp;</option>
                    {foreach item=trm from=$tramo}
						<option value="{$trm.pk_num_miscelaneo_detalle}">{$trm.ind_nombre_detalle}</option>
                    {/foreach}
				</select>
				<label for="fk_a006_miscelaneo_tramo"><i class="md-storage "></i> Tramo</label>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<div class="form-group floating-label">
				<select id="fk_a006_miscelaneo_documento" name="fk_a006_miscelaneo_documento"  class="form-control">
					<option value="">&nbsp;</option>
					{foreach item=doc from=$documento}
						<option value="{$doc.pk_num_miscelaneo_detalle}">{$doc.ind_nombre_detalle}</option>
					{/foreach}
				</select>
				<label for="fk_a006_miscelaneo_documento"><i class="glyphicon glyphicon-file"></i> Tipo de documento</label>
			</div>
		</div>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</form>
<script type="text/javascript">
	function cargarPasillo(idAlmacen) {
		$("#pasillo").html("");
		$.post("{$_Parametros.url}modAD/carpetaCONTROL/BuscarPasilloMET",{ idAlmacen:""+idAlmacen }, function (dato) {
			$("#pasillo").html(dato);
		});
	}
	function cargarEstante(idPasillo) {
		$("#estante").html("");
		$.post("{$_Parametros.url}modAD/carpetaCONTROL/BuscarEstanteMET",{ idPasillo:""+idPasillo }, function (dato) {
			$("#estante").html(dato);
		});
	}
	function cargarCajas(idDependencia) {
		$("#pk_num_caja").html("");
		$.post("{$_Parametros.url}modAD/carpetaCONTROL/BuscarCajasMET",{ idDependencia:""+idDependencia }, function (dato) {
			$("#pk_num_caja").html(dato);
		});
	}

    $('#checkCaja').click(function () {
        if($(this).attr('checked')=="checked"){
            $('#pk_num_caja').attr('disabled',false);
        } else {
            $('#pk_num_caja').attr('disabled','disabled');
        }
    });
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				pk_num_almacen:{
					required:true
				},
				pk_num_pasillo:{
					required:true
				},
				pk_num_estante:{
					required:true
				},
				fec_anio:{
					required:true
				},
				ind_descripcion_carpeta:{
					required:true
				},
				pk_num_documento:{
					required:true
				},
				pk_num_dependencia:{
					required:true
				}
			},
			messages:{
				pk_num_almacen:{
					required: "Seleccione el almacén"
				},
				pk_num_pasillo:{
					required: "Seleccione el pasillo"
				},
				pk_num_estante: {
					required: "Seleccione el estante"
				},
				fec_anio: {
					required: "Indique el año"
				},
				ind_descripcion_carpeta: {
					required: "Indique la descripción de la carpeta"
				},
				pk_num_documento: {
					required: "Seleccione el tipo de documento"
				},
				pk_num_dependencia: {
					required: "Seleccione la dependencia"
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$(document.getElementById('datatable1')).append('<tr id="pk_num_carpeta'+dato['pk_num_carpeta']+'"><td>'+dato['pk_num_carpeta']+'</td><td>'+dato['pk_num_carpeta']+'-'+dato['ind_dependencia']+'-'+dato['fec_anio']+'</td><td>'+dato['ind_nombre_detalle']+'</td><td>'+dato['ind_descripcion_carpeta']+'</td>' +
						'{if in_array('AD-01-02-04-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado la carpeta" titulo="Visualizar carpeta" title="Visualizar carpeta" data-toggle="modal" data-target="#formModal" pk_num_carpeta="'+dato['pk_num_carpeta']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
						'{if in_array('AD-01-02-04-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado una carpeta" titulo="Modificar Carpeta" title="Modificar Carpeta" data-toggle="modal" data-target="#formModal" pk_num_carpeta="'+dato['pk_num_carpeta']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
						'{if in_array('AD-01-02-04-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado una carpeta" titulo="¿Estás Seguro?" title="Eliminar carpeta" mensaje="¿Estás seguro de eliminar la carpeta?" boton="si, Eliminar" pk_num_carpeta="'+dato['pk_num_carpeta']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Registro Guardado", "Salida guardada satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}
	function repoFormatSelection( repo ) {
		return repo.full_name;
	}

	$( "button[data-select2-open]" ).click( function() {
		$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
	});

</script>
