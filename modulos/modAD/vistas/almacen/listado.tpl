<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Almacén</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de Almacén</th>
                            <th><i class="md-create"></i>Descripcion</th>
                            <th><i class="md-location-searching"></i> Ubicación</th>
                            <th><i class="md-person"></i> Encargado</th>
                            {if in_array('AD-01-02-01-03-V',$_Parametros.perfil)}<th width="40">Ver</th>{/if}
                            {if in_array('AD-01-02-01-04-M',$_Parametros.perfil)}<th width="50">Editar</th>{/if}
                            {if in_array('AD-01-02-01-05-E',$_Parametros.perfil)}<th width="70">Eliminar</th>{/if}
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_almacen{$post.pk_num_almacen}">
                            <td>{$post.pk_num_almacen}</td>
                            <td>{$post.ind_descripcion_almacen}</td>
                            <td>{$post.ind_ubicacion_almacen}</td>
                            <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                                {if in_array('AD-01-02-01-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_almacen="{$post.pk_num_almacen}" title="Ver almacén" titulo="Ver almacén" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>{/if}
                                {if in_array('AD-01-02-01-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_almacen="{$post.pk_num_almacen}" descipcion="El Usuario ha Modificado un almacén" title="Modificar almacén"  titulo="Modificar almacén"><i class="fa fa-edit"></i></button></td>{/if}
                                {if in_array('AD-01-02-01-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_almacen="{$post.pk_num_almacen}"  boton="si, Eliminar" descipcion="El usuario ha eliminado un almacén" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el almacén?" title="Eliminar almacén"><i class="md md-delete"></i></button></td>{/if}
                        </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="7">
                            {if in_array('AD-01-02-01-02-N',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado un almacen" data-toggle="modal" data-target="#formModal" titulo="Registrar nuevo almacén" id="nuevo"><i class="md md-create"></i> Nuevo almacén</button>{/if}
                            &nbsp;
                            {if in_array('AD-01-02-01-06-R',$_Parametros.perfil)}<button class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteAlmacen" descripcion="Generar Reporte de Almacén" data-toggle="modal" data-target="#formModal" titulo="Listado de Almacén">Reporte de Almacén</button>{/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modAD/almacenCONTROL/NuevoAlmacenMET';
                var $url_modificar='{$_Parametros.url}modAD/almacenCONTROL/EditarAlmacenMET';
                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });

                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		    $('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_almacen: $(this).attr('pk_num_almacen')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                    var pk_num_almacen=$(this).attr('pk_num_almacen');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modAD/almacenCONTROL/EliminarAlmacenMET';
                        $.post($url, { pk_num_almacen: pk_num_almacen},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_almacen'+$dato['pk_num_almacen'])).html('');
                                swal("Eliminado!", "El almacén fue eliminado.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });
    $(document).ready(function() {
        $('#reporteAlmacen').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modAD/almacenCONTROL/ImprimirSolicitudesMET' border='1' width='100%' height='440px'></iframe>");
        });
    });
            var $urlVer='{$_Parametros.url}modAD/almacenCONTROL/VerAlmacenMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_almacen: $(this).attr('pk_num_almacen')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
