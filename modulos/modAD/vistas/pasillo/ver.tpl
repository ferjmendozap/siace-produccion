<div class="form floating-label">
	<div class="modal-body" >
		{foreach item=pas from=$pasillo}
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$pas.pk_num_pasillo}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> N° de Pasillo</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$pas.ind_descripcion_pasillo}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$pas.ind_descripcion_almacen}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$pas.ind_ubicacion_almacen}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="md-location-searching"></i> Ubicación</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$pas.fecha}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2">Última modificación</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$pas.ind_nombre1} {$pas.ind_apellido1}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2">Último usuario</label>
			</div>
		{/foreach}
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
	</div>
</div>

