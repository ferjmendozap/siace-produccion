<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Tipo de Documento</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr  align="center">
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de Documento</th>
                            <th><i class="md-create"></i> Descripcion</th>
                            {if in_array('AD-01-02-05-03-V',$_Parametros.perfil)}<th width="40">Ver</th>{/if}
                            {if in_array('AD-01-02-05-04-M',$_Parametros.perfil)}<th width="50">Editar</th>{/if}
                            {if in_array('AD-01-02-05-05-E',$_Parametros.perfil)}<th width="70">Eliminar</th>{/if}
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_documento{$post.pk_num_documento}">
                            <td>{$post.pk_num_documento}</td>
                            <td>{$post.ind_descripcion_documento}</td>
                            {if in_array('AD-01-02-05-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_documento="{$post.pk_num_documento}" titulo="Ver tipo de documento" title="Ver tipo de documento" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>{/if}
                            {if in_array('AD-01-02-05-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_documento="{$post.pk_num_documento}" descipcion="El usuario ha modificado un tipo de documento" titulo="Modificar tipo de documento" title="Modificar tipo de documento" ><i class="fa fa-edit"></i></button></td>{/if}
                            {if in_array('AD-01-02-05-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_documento="{$post.pk_num_documento}"  boton="si, Eliminar" descipcion="El usuario ha eliminado un tipo de documento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el tipo de documento?" title="Eliminar tipo de documento"><i class="md md-delete"></i></button>{/if}
                            </td>
                        </tr>
                            {/foreach}
                        </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5">
                            {if in_array('AD-01-02-05-02-N',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado un tipo de documento" data-toggle="modal" data-target="#formModal" titulo="Registrar nuevo tipo de documento" id="nuevo"><i class="md md-create"></i> Nuevo Tipo de Documento</button>{/if}
                            &nbsp;
                            {if in_array('AD-01-02-05-06-R',$_Parametros.perfil)}<button class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteAlmacen" descripcion="Generar Reporte de Almacén" data-toggle="modal" data-target="#formModal" titulo="Listado de Documentos">Reporte de Documento</button>{/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modAD/tipoDocumentoCONTROL/NuevoDocumentoMET';

                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                var $url_modificar='{$_Parametros.url}modAD/tipoDocumentoCONTROL/EditarDocumentoMET';
                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		    $('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_documento: $(this).attr('pk_num_documento')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                    var pk_num_documento=$(this).attr('pk_num_documento');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modAD/tipoDocumentoCONTROL/EliminarDocumentoMET';
                        $.post($url, { pk_num_documento: pk_num_documento},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_documento'+$dato['pk_num_documento'])).html('');
                                swal("Eliminado!", "El documento fue eliminado.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });
    $(document).ready(function() {
        $('#reporteAlmacen').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modAD/tipoDocumentoCONTROL/ReporteDocumentoMET' border='1' width='100%' height='440px'></iframe>");
        });
    });
            var $urlVer='{$_Parametros.url}modAD/tipoDocumentoCONTROL/VerDocumentoMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_documento: $(this).attr('pk_num_documento')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
