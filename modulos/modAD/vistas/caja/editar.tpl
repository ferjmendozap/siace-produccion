<form action="{$_Parametros.url}modAD/cajaCONTROL/EditarCajaMET" id="formAjax" method="post" class="form" role="form">
	<div class="modal-body">
		<input type="hidden" value="1" name="valido" />
		{if isset($form.pk_num_caja)}
			<input type="hidden" value="{$form.pk_num_caja}" name="pk_num_caja" id="pk_num_caja" />
		{/if}
		<div class="col-md-6 col-sm-6">
			<div class="form-group floating-label">
				<select id="pk_num_almacen" name="pk_num_almacen" class="form-control" onchange="cargarPasillo(this.value)">
					<option value="">&nbsp;</option>
					{foreach item=alm from=$almacen}
						{if $alm.pk_num_almacen == $form.pk_num_almacen}
							<option selected value="{$alm.pk_num_almacen}">{$alm.ind_descripcion_almacen}</option>
						{else}
							<option value="{$alm.pk_num_almacen}">{$alm.ind_descripcion_almacen}</option>
						{/if}
					{/foreach}
				</select>
				<label for="select2"><i class="glyphicon glyphicon-home"></i> Almacen</label>
			</div>
			<div class="form-group floating-label" id="pasillo">
				<select id="pk_num_pasillo" name="pk_num_pasillo" class="form-control">
					<option value="">&nbsp;</option>
					{foreach item=pas from=$pasillo}
						{if $pas.pk_num_pasillo == $form.pk_num_pasillo}
							<option selected value="{$pas.pk_num_pasillo}">{$pas.ind_pasillo}</option>
						{else}
							<option value="{$pas.pk_num_pasillo}">{$pas.ind_pasillo}</option>
						{/if}
					{/foreach}
				</select>
				<label for="select2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
			</div>
			<div class="form-group floating-label" id="estante">
				<select id="pk_num_estante" name="pk_num_estante" class="form-control">
					<option value="">&nbsp;</option>
					{foreach item=est from=$estante}
						{if $est.pk_num_estante == $form.pk_num_estante}
							<option selected value="{$est.pk_num_estante}">{$est.ind_descripcion_estante}</option>
						{else}
							<option value="{$est.pk_num_estante}">{$est.ind_descripcion_estante}</option>
						{/if}
					{/foreach}
				</select>
				<label for="select2"><i class="glyphicon glyphicon-th-list"></i> Estante</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$form.fecha}" disabled="disabled">
				<label for="regular2">última fecha de modificación</label>
			</div>

		</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<select id="pk_num_dependencia" name="pk_num_dependencia" id="s2id_single" class="select2-container form-control select2">
						<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
						<option value="">&nbsp;</option>
						{foreach item=dep from=$dependencia}
							{if $dep.pk_num_dependencia == $form.pk_num_dependencia}
								<option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
							{else}
								<option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
							{/if}
						{/foreach}
					</select>
					<label for="select2"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
				</div>
				<!--<div class="form-group floating-label">
					<select id="pk_num_documento" name="pk_num_documento" class="form-control">
						<option value="">&nbsp;</option>
						{foreach item=doc from=$documento}
							{if $doc.pk_num_documento == $form.pk_num_documento}
								<option selected value="{$doc.pk_num_documento}">{$doc.ind_descripcion_documento}</option>
							{else}
								<option value="{$doc.pk_num_documento}">{$doc.ind_descripcion_documento}</option>
							{/if}
						{/foreach}
					</select>
					<label for="select2"><i class="glyphicon glyphicon-file"></i> Documento</label>
				</div>
				-->
                {$foo = $desde}
				<div class="form-group floating-label">
					<select id="fec_anio" name="fec_anio" class="form-control">
                        {while $foo>1899}
                            {if $foo==$form.fec_anio}
								<option value="{$foo}" selected>{$foo}</option>
                            {else}
								<option value="{$foo}">{$foo}</option>
                            {/if}

                            {$foo--}
                        {/while}
					</select>
					<label for="fec_anio"><i class="glyphicon glyphicon-calendar"></i> Año</label>
				</div>

				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.ind_descripcion_caja}" name="ind_descripcion_caja" id="ind_descripcion_caja">
					<label for="regular2"><i class="glyphicon glyphicon-inbox"></i> Descripción de caja</label>
				</div>

				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.ind_nombre1} {$form.ind_apellido1}" disabled="disabled">
					<label for="regular2">último usuario</label>
				</div>
				<div align="right">
					<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
					<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
				</div>
			</div>

</form>
<script type="text/javascript">
	$(document).ready(function() {
		$("#formAjax").validate({
			rules:{
				pk_num_almacen: {
					required: true
				},
				ind_descripcion_caja: {
					required: true
				},
				fec_anio: {
					required: true
				},
				pk_num_pasillo: {
					required: true
				},
				pk_num_documento: {
					required: true
				},
				pk_num_dependencia: {
					required: true
				},
				pk_num_documento: {
					required: true
				}
			},
			messages:{
				pk_num_almacen: {
					required:  "Seleccione el almacén"
				},
				ind_descripcion_caja: {
					required: "Ingrese la descripción de la caja"
				},
				fec_anio: {
					required: "Ingrese el año"
				},
				pk_num_pasillo: {
					required: "Seleccione el pasillo"
				},
				pk_num_documento: {
					required: "Seleccione el tipo de documento"
				},
				pk_num_dependencia: {
					required: "Seleccione la dependencia"
				}
			},
			submitHandler: function(form){
				$.post($(form).attr("action"), $(form).serialize(),function(dato){
					$('#pk_num_caja'+dato['pk_num_caja']).remove();
					$(document.getElementById('datatable1')).append('<tr id="pk_num_caja'+dato['pk_num_caja']+'"><td>'+dato['pk_num_caja']+'</td>' +
							'<td>'+dato['pk_num_caja']+' '+dato['ind_dependencia']+' '+dato['fec_anio']+'</td>' +
							'<td>'+dato['ind_descripcion_documento']+'</td>' +
							'<td>'+dato['ind_descripcion_caja']+'</td>' +
							'{if in_array('AD-01-02-04-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado una caja" ' +
							'titulo="Visualizar Caja" title="Visualizar Caja" data-toggle="modal" data-target="#formModal" pk_num_caja="'+dato['pk_num_caja']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-04-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha modificado una caja" ' +
							'titulo="Modificar Caja" title="Modificar Caja" data-toggle="modal" data-target="#formModal" pk_num_caja="'+dato['pk_num_caja']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-04-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado una caja" title="Eliminar" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar la caja?" boton="si, Eliminar" pk_num_caja="'+dato['pk_num_caja']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Caja modificado", "Caja modificada exitosamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
	function cargarPasillo(idAlmacen) {
		$("#pasillo").html("");
		$.post("{$_Parametros.url}modAD/cajaCONTROL/BuscarPasilloMET",{ idAlmacen:""+idAlmacen }, function (dato) {
			$("#pasillo").html(dato);
			$("#estante").html("");
			$.post("{$_Parametros.url}modAD/cajaCONTROL/BuscarEstanteMET",{ idPasillo:""+idPasillo }, function (dato) {
				$("#estante").html(dato);
			});
		});
	}
	function cargarEstante(idPasillo) {
		$("#estante").html("");
		$.post("{$_Parametros.url}modAD/cajaCONTROL/BuscarEstanteMET",{ idPasillo:""+idPasillo }, function (dato) {
			$("#estante").html(dato);
		});
	}
	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}
	function repoFormatSelection( repo ) {
		return repo.full_name;
	}

	$( "button[data-select2-open]" ).click( function() {
		$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
	});
</script>

