<form id="formAjax" class="form floating-label" action="{$_Parametros.url}modAD/entradaArchivoCONTROL/BuscarFechaMET" role="form" method="post" target="cargarReporte" novalidate>
<input type="hidden" name="valido" value="1" />
    <div class="section-body contain-lg">
			<table align="center" class="table-responsive">
				<tr>
					<td>
						<div class="section-body contain-lg">
							<div class="form-group" style="width:100%">
								<div class="input-daterange input-group" id="demo-date-range">
									<div class="input-group-content">
										<input type="text" class="form-control" name="fechaInicio" id="fechaInicio">
										<label>Rango de fecha</label>
									</div>
									<span class="input-group-addon">a</span>
									<div class="input-group-content">
										<input type="text" class="form-control" name="fechaFin" id="fechaFin">
										<div class="form-control-line"></div>
									</div>
									<button type="submit"  class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:7px">Consultar</button>&nbsp; &nbsp;  
								</div>
							</div>

						</div>
					</td>
				</tr>
			</table>
            <div>
			<iframe frameborder='0' width='100%' height='440px' name="cargarReporte"></iframe>
			</div>
		</div>
</form>
<script type="text/javascript">
	$("#fechaInicio").datepicker({
			format: 'dd/mm/yyyy',
		});
		$("#fechaFin").datepicker({
			format: 'dd/mm/yyyy',
		});
</script>

