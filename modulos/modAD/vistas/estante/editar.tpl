<form action="{$_Parametros.url}modAD/estanteCONTROL/EditarEstanteMET" id="formAjax" method="post" class="form" role="form">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
	{if isset($form.pk_num_estante)}
		<input type="hidden" value="{$form.pk_num_estante}" name="pk_num_estante" id="pk_num_estante" />
	{/if}
	<div class="form-group floating-label">
		<select id="pk_num_almacen" name="pk_num_almacen" class="form-control" onchange="cargarPasillo(this.value)">
			<option value="">&nbsp;</option>
			{foreach item=alm from=$almacen}
				{if $alm.pk_num_almacen == $form.pk_num_almacen}
					<option selected value="{$alm.pk_num_almacen}">{$alm.ind_descripcion_almacen}</option>
				{else}
					<option value="{$alm.pk_num_almacen}">{$alm.ind_descripcion_almacen}</option>
				{/if}
			{/foreach}
		</select>
		<label for="select2"><i class="glyphicon glyphicon-home"></i> Almacen</label>
	</div>
	<div class="form-group floating-label" id="pasillo">
		<select id="pk_num_pasillo" name="pk_num_pasillo" class="form-control">
			<option value="">&nbsp;</option>
			{foreach item=pas from=$pasillo}
				{if $pas.pk_num_pasillo == $form.pk_num_pasillo}
					<option selected value="{$pas.pk_num_pasillo}">{$pas.ind_pasillo}</option>
				{else}
					<option value="{$pas.pk_num_pasillo}">{$pas.ind_pasillo}</option>
				{/if}
			{/foreach}
		</select>
		<label for="select2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$form.ind_descripcion_estante}" name="ind_descripcion_estante" id="ind_descripcion_estanteo">
		<label for="regular2"><i class="md md-create"></i> Estante</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$form.fecha}" disabled="disabled">
		<label for="regular2">última fecha de modificación</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$form.nombreCompleto}" disabled="disabled">
		<label for="regular2">último usuario</label>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado la modificacion del estante" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").validate({
			rules:{
				pk_num_almacen:{
					required:true
				},
				pk_num_pasillo:{
					required:true
				},
				ind_descripcion_estante:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modAD/estanteCONTROL/VerificarEstanteMET",
						type:"post",
						data: {
							pk_num_almacen: function(){
								return $( "#pk_num_almacen" ).val();
							},
							pk_num_pasillo: function(){
								return $( "#pk_num_pasillo" ).val();
							},
							pk_num_estante: function(){
								return $( "#pk_num_estante" ).val();
							}
						}
					}
				}

			},
			messages:{
				pk_num_almacen:{
					required: "Este campo es requerido"
				},
				pk_num_pasillo:{
					required: "Este campo es requerido"
				},
				ind_descripcion_estante:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				}
			},
			submitHandler: function(form){
				$.post($(form).attr("action"), $(form).serialize(),function(dato){
				$('#pk_num_estante'+dato['pk_num_estante']).remove();
				$(document.getElementById('datatable1')).append('<tr id="pk_num_estante'+dato['pk_num_estante']+'">' +
						'<td>'+dato['pk_num_estante']+'</td>' +
						'<td>'+dato['ind_descripcion_estante']+'</td>' + '<td>'+dato['ind_pasillo']+'</td><td>'+dato['ind_descripcion_almacen']+'</td>' +
						'{if in_array('AD-01-02-03-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado un estante" titulo="Visualizar Estante" title="Visualizar Estante" data-toggle="modal" data-target="#formModal" pk_num_estante="'+dato['pk_num_estante']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
						'{if in_array('AD-01-02-03-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado un estante" ' +
				'titulo="Modificar Estante" title="Modificar Estante" data-toggle="modal" data-target="#formModal" pk_num_estante="'+dato['pk_num_estante']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
						'{if in_array('AD-01-02-03-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un estante" titulo="¿Estás Seguro?" title="Eliminar" mensaje="¿Estás seguro de eliminar el estante?" boton="si, Eliminar" pk_num_estante="'+dato['pk_num_estante']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
				swal("Estante Modificado", "Estante modificado exitosamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
            },'json');
        }
    });
});
	function cargarPasillo(idAlmacen) {
		$("#pasillo").html("");
		$.post("{$_Parametros.url}modAD/estanteCONTROL/BuscarPasilloMET",{ idAlmacen:""+idAlmacen }, function (dato) {
			$("#pasillo").html(dato);
		});
	}
</script>


