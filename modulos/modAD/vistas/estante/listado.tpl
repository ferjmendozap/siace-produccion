<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Estante</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr  align="center">
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de Estante</th>
                            <th><i class="glyphicon glyphicon-pencil"></i> Descripción</th>
                            <th><i class="glyphicon glyphicon-th"></i> Pasillo</th>
                            <th><i class="glyphicon glyphicon-home"></i> Almacén</th>
                            {if in_array('AD-01-02-03-03-V',$_Parametros.perfil)}<th width="40">Ver</th>{/if}
                            {if in_array('AD-01-02-03-04-M',$_Parametros.perfil)}<th width="50">Editar</th>{/if}
                            {if in_array('AD-01-02-03-05-E',$_Parametros.perfil)}<th width="70">Eliminar</th>{/if}
                        </tr>
                    </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_estante{$post.pk_num_estante}">
                            <td>{$post.pk_num_estante}</td>
                            <td>{$post.ind_descripcion_estante}</td>
                            <td>{$post.ind_pasillo}</td>
                            <td>{$post.ind_descripcion_almacen}</td>
                                {if in_array('AD-01-02-03-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_estante="{$post.pk_num_estante}" titulo="Ver estante" title="Ver Estante"  id="ver"><i class="glyphicon glyphicon-search"></i></button></td>{/if}
                                {if in_array('AD-01-02-03-04-M',$_Parametros.perfil)}<td align="center"> <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_estante="{$post.pk_num_estante}" descipcion="El Usuario ha Modificado un estante" title="Modificar estante" titulo="Modificar estante" ><i class="fa fa-edit"></i></button></td>{/if}
                                {if in_array('AD-01-02-03-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_estante="{$post.pk_num_estante}"  boton="si, Eliminar" descipcion="El usuario ha eliminado un estante" titulo="¿Estás Seguro?" title="Eliminar estante" mensaje="¿Desea eliminar el estante?" title="Eliminar estante"><i class="md md-delete"></i></button>{/if}
                            </td>
                        </tr>
                            {/foreach}
                        </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="7">
                            {if in_array('AD-01-02-03-02-N',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado un almacen" data-toggle="modal" data-target="#formModal" titulo="Registrar nuevo estante" id="nuevo"><i class="md md-create"></i> Nuevo estante</button>{/if}
                            &nbsp;
                            {if in_array('AD-01-02-03-06-R',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteEstante" descripcion="Generar Reporte de Almacén" data-toggle="modal" data-target="#formModal" titulo="Listado de Estantes">Reporte de Estante</button>{/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modAD/estanteCONTROL/NuevoEstanteMET';
                var $url_modificar='{$_Parametros.url}modAD/estanteCONTROL/EditarEstanteMET';
                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });

                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		    $('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_estante: $(this).attr('pk_num_estante')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                    var pk_num_estante=$(this).attr('pk_num_estante');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modAD/estanteCONTROL/EliminarEstanteMET';
                        $.post($url, { pk_num_estante: pk_num_estante},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_estante'+$dato['pk_num_estante'])).html('');
                                swal("Eliminado!", "El estante fue eliminado.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });
    $(document).ready(function() {
        $('#reporteEstante').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modAD/estanteCONTROL/ReporteEstanteMET' border='1' width='100%' height='440px'></iframe>");
        });
    });
            var $urlVer='{$_Parametros.url}modAD/estanteCONTROL/VerEstanteMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_estante: $(this).attr('pk_num_estante')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
