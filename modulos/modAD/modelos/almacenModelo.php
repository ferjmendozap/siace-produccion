<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de almacenes 
class almacenModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    public function metListarAlmacen()
    {
        $listaAlmacen =  $this->_db->query(
            "SELECT a.pk_num_almacen, a.ind_descripcion_almacen, a.ind_ubicacion_almacen, c.ind_nombre1, c.ind_apellido1, b.fk_a003_num_persona from ad_c001_almacen as a, rh_b001_empleado as b, a003_persona as c where a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $listaAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $listaAlmacen->fetchAll();
    }

    public function metVerAlmacen($pkNumAlmacen, $metodo)
    {
        $verAlmacen =  $this->_db->query(
            "SELECT a.pk_num_almacen, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%i:%S') as fecha, b.fk_a003_num_persona, a.ind_descripcion_almacen, a.ind_ubicacion_almacen, a.fk_a018_num_seguridad_usuario, b.pk_num_empleado, c.ind_nombre1, c.ind_apellido1 from ad_c001_almacen as a, rh_b001_empleado as b, a003_persona as c where a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAlmacen->fetch();
        } else {
            return $verAlmacen->fetchAll();
        }

    }

    public function metVerUsuario($pkNumAlmacen)
    {
        $verUsuario =  $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1 from ad_c001_almacen as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetchAll();

    }

    public function metUsuarioAlmacen($pkNumEmpleado){
        $usuario = $this->_db->query(
            "SELECT  b.ind_nombre1, b.ind_apellido1, a.fk_a003_num_persona from rh_b001_empleado as a, a003_persona as b
             where a.fk_a003_num_persona=b.pk_num_persona and a.pk_num_empleado='$pkNumEmpleado'"
        );
        $nombre_usuario = $usuario->fetch();
        return $nombre_usuario[0].' '.$nombre_usuario[1];
    }

    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
        "SELECT a.fk_a003_num_persona, a.pk_num_empleado, b.ind_nombre1, b.ind_apellido1 from rh_b001_empleado as a, a003_persona as b
         where a.fk_a003_num_persona=b.pk_num_persona and a.num_estatus=1 order by b.ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }

    public function metGuardarAlmacen($ind_descripcion_almacen, $ind_ubicacion_almacen, $fk_a003_num_persona, $fecha_hora, $usuario)
    {
        $this->_db->beginTransaction();
        $NuevoAlmacen=$this->_db->prepare(
            "insert into ad_c001_almacen values (null, :ind_descripcion_almacen, :num_ubicacion_almacen, :fk_rhb001_num_empleado, :fec_ultima_modificacion, :fk_a018_num_seguridad_usuario)"
        );
         $NuevoAlmacen->execute(array(
        ':ind_descripcion_almacen' => $ind_descripcion_almacen,
        ':num_ubicacion_almacen'=> $ind_ubicacion_almacen,
        ':fk_rhb001_num_empleado' => $fk_a003_num_persona,
        ':fec_ultima_modificacion' => $fecha_hora,
        ':fk_a018_num_seguridad_usuario' => $usuario
    ));
        $pkNumAlmacen= $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumAlmacen;
    }

	public function metEditarAlmacen($ind_descripcion_almacen, $ind_ubicacion_almacen,$fk_a003_num_persona, $fecha_hora,$usuario, $pkNumAlmacen)
   	 {
        $this->_db->beginTransaction();
        $this->_db->query(
        "update ad_c001_almacen set ind_descripcion_almacen='$ind_descripcion_almacen', ind_ubicacion_almacen='$ind_ubicacion_almacen', fk_rhb001_num_empleado='$fk_a003_num_persona', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario='$usuario'  where pk_num_almacen='$pkNumAlmacen'"
        );
        $this->_db->commit();
   	 }

    public function metEliminarAlmacen($pkNumAlmacen)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ad_c001_almacen where pk_num_almacen = '$pkNumAlmacen'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
        "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metBuscarAlmacen($ind_descripcion_almacen, $pk_num_almacen)
    {
        if($pk_num_almacen == false) {
            $buscarAlmacen = $this->_db->query(
                "select pk_num_almacen from ad_c001_almacen where ind_descripcion_almacen='$ind_descripcion_almacen'"
            );
            $buscarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarAlmacen->fetch();
            $arreglo = $buscarAlmacen->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $buscarAlmacen = $this->_db->query(
                "select pk_num_almacen from ad_c001_almacen where ind_descripcion_almacen='$ind_descripcion_almacen' and pk_num_almacen<>'$pk_num_almacen'"
            );
            $buscarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarAlmacen->fetch();
            $arreglo = $buscarAlmacen->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
}// fin de la clase
?>
