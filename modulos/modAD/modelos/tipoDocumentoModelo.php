<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de tipos de documento
class tipoDocumentoModelo extends Modelo
{
    // Constructor del modelo
    public function __construct()
    {
        parent::__construct();
    } 

    public function metListarDocumento()
    {
        $listarDocumento =  $this->_db->query(
            "select a.pk_num_documento, a.ind_descripcion_documento, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%i:%S') as fecha, d.ind_nombre1, d.ind_apellido1 from ad_c005_tipo_documento as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona"
        );
        $listarDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $listarDocumento->fetchAll();
    }

    public function metVerDocumento($pkNumDocumento,$metodo)
    {
        $verDocumento =  $this->_db->query(
            "select a.pk_num_documento, a.ind_descripcion_documento, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%i:%S') as fecha, d.ind_nombre1, d.ind_apellido1 from ad_c005_tipo_documento as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.pk_num_documento='$pkNumDocumento' and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona"
        );
        $verDocumento->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
        return $verDocumento->fetch();
        else return $verDocumento->fetchAll();
    }

    public function metGuardarDocumento($ind_descripcion_documento, $fecha_hora ,$usuario)
    {
        $this->_db->beginTransaction();
        $NuevoDocumento=$this->_db->prepare(
            "insert into ad_c005_tipo_documento values (null, :ind_descripcion_documento, :fec_ultima_modificacion, :fk_a018_num_seguridad_usuario)"
        );
        $NuevoDocumento->execute(array(
            ':ind_descripcion_documento'=> $ind_descripcion_documento,
            ':fec_ultima_modificacion' => $fecha_hora,
            ':fk_a018_num_seguridad_usuario' => $usuario
        ));
        $pkNumDocumento= $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumDocumento;
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metEliminarDocumento($pkNumDocumento)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ad_c005_tipo_documento " .
            "where pk_num_documento = '$pkNumDocumento'"
        );
        $this->_db->commit();
    }

    public function metEditarDocumento($ind_descripcion_documento, $fecha_hora,$usuario, $pkNumDocumento)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ad_c005_tipo_documento set ind_descripcion_documento='$ind_descripcion_documento', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario='$usuario' where pk_num_documento='$pkNumDocumento'"
        );
        $this->_db->commit();
    }

    public function metBuscarDocumento($pkNumDocumento)
    {
        if($pkNumDocumento == false) {
            $buscarDocumento = $this->_db->query(
                "select pk_num_documento from ad_c005_tipo_documento where pk_num_documento=$pkNumDocumento"
            );
            $buscarDocumento->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarDocumento->fetch();
            $arreglo = $buscarDocumento->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $buscarDocumento = $this->_db->query(
                "select pk_num_documento from ad_c005_tipo_documento where pk_num_documento <>$pkNumDocumento"
            );
            $buscarDocumento->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarDocumento->fetch();
            $arreglo = $buscarDocumento->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }

    }

    public function metBuscarDescripcionDocumento($ind_descripcion_documento, $pk_num_documento)
    {
        if($pk_num_documento == false) {
            $buscarDescripcion = $this->_db->query(
                "select pk_num_documento from ad_c005_tipo_documento where ind_descripcion_documento='$ind_descripcion_documento'"
            );
            $buscarDescripcion->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarDescripcion->fetch();
            $arreglo = $buscarDescripciono->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $buscarDescripcion = $this->_db->query(
                "select pk_num_documento from ad_c005_tipo_documento where ind_descripcion_documento='$ind_descripcion_documento' and pk_num_documento <>'$pk_num_documento'"
            );
            $buscarDescripcion->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarDescripcion->fetch();
            $arreglo = $buscarDescripcion->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}// fin de la clase
?>
