<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de cajas de archivos
class cajaModelo extends Modelo 
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarCaja()
    {
        $listarCaja =  $this->_db->query(
            "SELECT
                      a.pk_num_caja,
                      a.ind_descripcion_caja,
                      a.fec_anio,
                      b.ind_descripcion_estante,
                      c.ind_dependencia,
                      e.ind_pasillo,
                      f.ind_descripcion_almacen
                    FROM
                      ad_c004_caja AS a,
                      ad_c003_estante AS b,
                      a004_dependencia AS c,
                      ad_c002_pasillo AS e,
                      ad_c001_almacen AS f
                    WHERE
                      a.fk_adc003_num_estante = b.pk_num_estante AND 
                      a.fk_a004_num_dependencia = c.pk_num_dependencia AND 
                      b.fk_adc002_num_pasillo = e.pk_num_pasillo AND 
                      e.fk_adc001_num_almacen = f.pk_num_almacen"
        );
        $listarCaja->setFetchMode(PDO::FETCH_ASSOC);
        return $listarCaja->fetchAll();
    }

    public function metVerCaja($pkNumCaja, $metodo)
    {
        $verCaja =  $this->_db->query(
            "SELECT
                      a.pk_num_caja,
                      a.ind_descripcion_caja,
                      a.fec_anio,
                      b.pk_num_estante,
                      b.ind_descripcion_estante,
                      c.pk_num_dependencia,
                      c.ind_dependencia,
                      e.pk_num_pasillo,
                      e.ind_pasillo,
                      f.ind_descripcion_almacen,
                      f.pk_num_almacen,
                      DATE_FORMAT(
                        a.fec_ultima_modificacion,
                        '%d/%m/%Y %H:%i:%S'
                      ) AS fecha,
                      i.ind_nombre1,
                      i.ind_apellido1
                    FROM
                      ad_c004_caja AS a,
                      ad_c003_estante AS b,
                      a004_dependencia AS c,
                      ad_c002_pasillo AS e,
                      ad_c001_almacen AS f,
                      a018_seguridad_usuario AS g,
                      rh_b001_empleado AS h,
                      a003_persona AS i
                    WHERE
                      a.fk_adc003_num_estante = b.pk_num_estante AND 
                      a.fk_a004_num_dependencia = c.pk_num_dependencia AND 
                      a.pk_num_caja = '$pkNumCaja' AND 
                      b.fk_adc002_num_pasillo = e.pk_num_pasillo AND 
                      e.fk_adc001_num_almacen = f.pk_num_almacen AND 
                      a.fk_a018_num_seguridad_usuario = g.pk_num_seguridad_usuario AND 
                      g.fk_rhb001_num_empleado = h.pk_num_empleado AND 
                      h.fk_a003_num_persona = i.pk_num_persona"
        );
        $verCaja->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $verCaja->fetch();
        else return $verCaja->fetchAll();
    }

    public function metEliminarCaja($pkNumCaja)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ad_c004_caja where pk_num_caja = '$pkNumCaja'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metListarDependencia()
    {
        $listarDependencia =  $this->_db->query(
            "select pk_num_dependencia, ind_dependencia from a004_dependencia"
        );
        $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $listarDependencia->fetchAll();
    }

    public function metVerDependencia($pkNumCaja)
    {
        $verDependencia = $this->_db->query(
            "select b.ind_dependencia from ad_c004_caja as a, a004_dependencia as b where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.pk_num_caja='$pkNumCaja'"
        );
        $verDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $verDependencia->fetch();
    }

    public function metVisualizarDependencia($pk_num_dependencia)
    {
        $visualizarDependencia = $this->_db->query(
            "select b.ind_dependencia from ad_c004_caja as a, a004_dependencia as b where pk_num_dependencia='$pk_num_dependencia'"
        );
        $visualizarDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $visualizarDependencia->fetch();
    }

    public function metListarDocumento()
    {
        $listarDocumento = $this->_db->query(
            "select pk_num_documento, ind_descripcion_documento from ad_c005_tipo_documento"
        );
        $listarDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $listarDocumento->fetchAll();
    }

    public function metVerDocumento($pkNumCaja)
    {
        $verDocumento = $this->_db->query(
            "select b.ind_descripcion_documento from ad_c004_caja as a, ad_c005_tipo_documento as b where a.pk_num_caja='$pkNumCaja' and a.fk_adc005_num_documento=b.pk_num_documento"
        );
        $verDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $verDocumento->fetch();
    }

    public function metVisualizarDocumento($pk_num_documento)
    {
        $visualizarDocumento = $this->_db->query(
            "select ind_descripcion_documento from ad_c005_tipo_documento where pk_num_documento='$pk_num_documento'"
        );
        $visualizarDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $visualizarDocumento->fetch();
    }

    public function metBuscarPasillo($pkNumAlmacen)
    {
        $buscarPasillo =  $this->_db->query(
            "SELECT pk_num_pasillo, ind_pasillo from ad_c002_pasillo where fk_adc001_num_almacen = '$pkNumAlmacen' "
        );
        $buscarPasillo->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarPasillo->fetchAll();
    }

    public function metVerPasillo($pk_num_pasillo)
    {
        $verPasillo =  $this->_db->query(
            "SELECT pk_num_pasillo, ind_pasillo from ad_c002_pasillo where pk_num_pasillo='$pk_num_pasillo' "
        );
        $verPasillo->setFetchMode(PDO::FETCH_ASSOC);
        return $verPasillo->fetch();
    }

    public function metListarAlmacen()
    {
        $listarAlmacen =  $this->_db->query(
            "SELECT pk_num_almacen, ind_descripcion_almacen from ad_c001_almacen"
        );
        $listarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $listarAlmacen->fetchAll();
    }

    public function metBuscarAlmacen($pkNumAlmacen)
    {
        $listarAlmacen =  $this->_db->query(
            "SELECT pk_num_almacen, ind_descripcion_almacen from ad_c001_almacen where pk_num_almacen='$pkNumAlmacen'"
        );
        $listarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $listarAlmacen->fetch();
    }

    public function metBuscarEstante($pkNumPasillo)
    {
        $buscarEstante = $this->_db->query(
            "select pk_num_estante, ind_descripcion_estante from ad_c003_estante where fk_adc002_num_pasillo='$pkNumPasillo'"
        );
        $buscarEstante->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarEstante->fetchAll();
    }

    public function metVerEstante($pk_num_estante)
    {
        $verEstante = $this->_db->query(
            "select pk_num_estante, ind_descripcion_estante from ad_c003_estante where pk_num_estante='$pk_num_estante'"
        );
        $verEstante->setFetchMode(PDO::FETCH_ASSOC);
        return $verEstante->fetch();
    }

    public function metGuardarCaja($pk_num_estante, $fec_anio, $ind_descripcion_caja, $pk_num_dependencia)
    {
        $this->_db->beginTransaction();
        $NuevaCaja=$this->_db->prepare(
            "insert into
                        ad_c004_caja 
                        SET 
                        ind_descripcion_caja=:ind_descripcion_caja, 
                        fec_anio=:fec_anio, 
                        fk_a004_num_dependencia=:fk_a004_num_dependencia, 
                        fk_adc003_num_estante=:fk_adc003_num_estante, 
                        fec_ultima_modificacion=NOW(), 
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
");
        $NuevaCaja->execute(array(
            ':ind_descripcion_caja'=> $ind_descripcion_caja,
            ':fec_anio' => $fec_anio,
            ':fk_a004_num_dependencia' => $pk_num_dependencia,
            ':fk_adc003_num_estante' => $pk_num_estante
        ));


        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $NuevaCaja->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metEditarCaja($pk_num_estante,$fec_anio, $ind_descripcion_caja, $pk_num_dependencia,$pkNumCaja)
    {

                $this->_db->beginTransaction();
                $NuevaCaja=$this->_db->prepare(
                    "UPDATE  
                                ad_c004_caja 
                              SET 
                                ind_descripcion_caja=:ind_descripcion_caja, 
                                fec_anio=:fec_anio, 
                                fk_a004_num_dependencia=:fk_a004_num_dependencia, 
                                fk_adc003_num_estante=:fk_adc003_num_estante, 
                                fec_ultima_modificacion=NOW(), 
                                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                              WHERE
                                pk_num_caja='$pkNumCaja'
        ");
                $NuevaCaja->execute(array(
                    ':ind_descripcion_caja'=> $ind_descripcion_caja,
                    ':fec_anio' => $fec_anio,
                    ':fk_a004_num_dependencia' => $pk_num_dependencia,
                    ':fk_adc003_num_estante' => $pk_num_estante
                ));


                $fallaTansaccion = $NuevaCaja->errorInfo();

                if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
                    $this->_db->rollBack();
                    return $fallaTansaccion;
                }else{
                    $this->_db->commit();
                    return $pkNumCaja;
                }
    }
    
}// fin de la clase
?>
