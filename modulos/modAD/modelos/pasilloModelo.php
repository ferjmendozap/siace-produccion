<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de pasillos
class pasilloModelo extends almacenModelo
{
    // Constructor del modelo
    public function __construct() 
    {
        parent::__construct();
    }

    // Método para obtener todos los almacens registrados en la base de datos
    public function metListarPasillo()
    {
        $listarPasillo =  $this->_db->query(
            "SELECT a.pk_num_pasillo, a.ind_pasillo, a.ind_descripcion_pasillo, b.ind_descripcion_almacen, b.ind_ubicacion_almacen
             from ad_c002_pasillo as a, ad_c001_almacen as b where a.fk_adc001_num_almacen= b.pk_num_almacen"
        );
        $listarPasillo->setFetchMode(PDO::FETCH_ASSOC);
        return $listarPasillo->fetchAll();
    }

    public function metAlmacenDescripcion($pk_num_almacen)
    {
        $almacen = $this->_db->query(
           "SELECT pk_num_almacen, ind_descripcion_almacen from ad_c001_almacen where pk_num_almacen='$pk_num_almacen'"
        );
        $ind_descripcion_almacen = $almacen->fetch();
        return $ind_descripcion_almacen[1];
    }
    public function metAlmacenUbicacion($pk_num_almacen)
    {
        $almacen = $this->_db->query(
            "SELECT pk_num_almacen, ind_ubicacion_almacen from ad_c001_almacen where pk_num_almacen='$pk_num_almacen'"
        );
        $ind_ubicacion_almacen = $almacen->fetch();
        return $ind_ubicacion_almacen[1];
    }

    public function metGuardarPasillo($ind_pasillo,$ind_descripcion_pasillo, $pk_num_almacen, $fecha_hora ,$usuario)
    {
        $this->_db->beginTransaction();
        $guardarPasillo=$this->_db->prepare(
            "insert into ad_c002_pasillo values (null, :ind_pasillo, :ind_descripcion_pasillo, :fec_ultima_modificacion, :fk_adc001_num_almacen, :fk_a018_num_seguridad_usuario)"
        );
        $guardarPasillo->execute(array(
            ':ind_pasillo' => $ind_pasillo,
            ':ind_descripcion_pasillo'=> $ind_descripcion_pasillo,
            ':fec_ultima_modificacion' => $fecha_hora,
            ':fk_adc001_num_almacen' => $pk_num_almacen,
            ':fk_a018_num_seguridad_usuario' => $usuario
        ));
        $pkNumPasillo= $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumPasillo;
    }

    public function metVerPasillo($pkNumPasillo, $metodo)
    {
        $verPasillo =  $this->_db->query(
            "SELECT a.pk_num_pasillo, a.ind_pasillo, a.ind_descripcion_pasillo,date_format(a.fec_ultima_modificacion,'%d/%m/%Y %H:%i:%S') as fecha, b.ind_descripcion_almacen, b.ind_ubicacion_almacen, b.pk_num_almacen, e.ind_nombre1, e.ind_apellido1 from ad_c002_pasillo as a, ad_c001_almacen as b, a018_seguridad_usuario as c, rh_b001_empleado as d, a003_persona as e where a.pk_num_pasillo='$pkNumPasillo' and a.fk_adc001_num_almacen=b.pk_num_almacen and b.fk_a018_num_seguridad_usuario=c.pk_num_seguridad_usuario and c.fk_rhb001_num_empleado=d.pk_num_empleado and d.fk_a003_num_persona=e.pk_num_persona"
        );
        $verPasillo->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
        return $verPasillo->fetch();
        else return $verPasillo->fetchAll();
    }

    public function metPasilloAlmacen($pkNumAlmacen)
    {
        $pasilloAlmacen =  $this->_db->query(
            "SELECT b.pk_num_pasillo, b.ind_pasillo, b.ind_descripcion_pasillo from ad_c001_almacen as a, ad_c002_pasillo as b where a.pk_num_almacen=b.fk_adc001_num_almacen and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $pasilloAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $pasilloAlmacen->fetchAll();
    }

    public function metEliminarPasillo($pkNumPasillo)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ad_c002_pasillo " .
            "where pk_num_pasillo = '$pkNumPasillo'"
        );
        $this->_db->commit();
    }

    public function metEditarPasillo($ind_pasillo, $ind_descripcion_pasillo, $pk_num_almacen, $fecha_hora,$usuario, $pkNumPasillo)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ad_c002_pasillo set ind_pasillo='$ind_pasillo', ind_descripcion_pasillo='$ind_descripcion_pasillo', fk_adc001_num_almacen='$pk_num_almacen', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario='$usuario'  where pk_num_pasillo='$pkNumPasillo'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metBuscarPasillo($ind_pasillo, $pk_num_almacen, $pk_num_pasillo)
    {
        if($pk_num_pasillo == false) {
            $buscarPasillo = $this->_db->query(
                "select a.pk_num_pasillo from ad_c002_pasillo as a, ad_c001_almacen as b where a.fk_adc001_num_almacen=b.pk_num_almacen and b.pk_num_almacen='$pk_num_almacen' and a.ind_pasillo='$ind_pasillo'"
            );
            $buscarPasillo->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarPasillo->fetch();
            $arreglo = $buscarPasillo->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $buscarPasillo = $this->_db->query(
                "select a.pk_num_pasillo from ad_c002_pasillo as a, ad_c001_almacen as b where a.fk_adc001_num_almacen=b.pk_num_almacen and b.pk_num_almacen='$pk_num_almacen' and a.ind_pasillo='$ind_pasillo' and a.pk_num_pasillo <>'$pk_num_pasillo' "
            );
            $buscarPasillo->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarPasillo->fetch();
            $arreglo = $buscarPasillo->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
}// fin de la clase
?>
