<section class="style-default-bright">
    <div class="col-lg-12">
        <div class="section-header"><h2 class="text-primary">Información del Sistema</h2></div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-sm-12">
                <div align="center" class="text-bold text-accent-dark"><h2 class="text-dark">¡Atención!</h2></div>
                <div align="center"><h4>Disculpe, usted no tiene Acceso a esa opción</h4></div>
                <div align="center"><h4>Por favor póngase en contacto con el administrador del sistema</h4></div>
            </div>
        </div>
    </div>
    <div class="clearfix visible-sm"></div>
</section>