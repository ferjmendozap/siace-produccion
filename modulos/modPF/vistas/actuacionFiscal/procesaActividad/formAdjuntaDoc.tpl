<!--
Formulario para el proceso de adjuntar documetos a una actividad en particular
Nota: es montado desde Ejecución de Actividades y Detalles actividades.
-->
<div id="contentAduntos">
    <div id="formAdjuntos">
        <div class="card">
                <div class="card-body" style="margin-bottom: 0;">
                    <form id="formAjaxAdjuntar" class="form form-validation" novalidate="novalidate" role="form">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group" id="txt_nombreDocError">
                                    <input id="txt_nombreDoc" name="form[txt][txt_nombreDoc]" type="text" maxlength="128" class="form-control">
                                    <label for="txt_nombreDoc"><i class="md md-border-color"></i>&nbsp;Nombre documento <span class="text-bold" style="color: red;">*</span></label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group" id="txt_nroDocError">
                                    <input id="txt_nroDoc" name="form[txt][txt_nroDoc]" type="text" maxlength="13" class="form-control">
                                    <label for="txt_nroDoc"><i class="md md-border-color"></i>&nbsp;Nro. documento</label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group" id="txt_fechaDocError">
                                    <input id="txt_fechaDoc" name="form[alphaNum][txt_fechaDoc]" type="text" class="form-control" />
                                    <label for="txt_fechaDoc"><i class="fa fa-calendar"></i>&nbsp;Fecha: <span class="text-bold" style="color: red;">*</span></label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group" id="txt_archivoDocError">
                                    <div id="container">
                                        <table>
                                            <tr>
                                                <td width="89%" class="alinear-left" id="filelist"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <label for="txt_archivoDoc" title="Haga click en el botón 'Seleccionar archivo'" alt="Haga click en el botón 'Seleccionar archivo'"><i class="md md-border-color"></i>&nbsp;Archivo</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-foot"class="text-center">
                    <div class="text-center">
                        <button type="button" onclick="metCancelAdjuntar()" class="btn btn-defoult btn-raised" title="Click para volver" alt="Click para volver">
                            <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Volver
                        </button>
                        {if $adjuntarArchivos==1}
                            <input id="pickfiles" type="button" class="boton btn btn-primary btn-success"
                                   title="Click para buscar y seleccionar el documento" value="Seleccionar archivo" alt="Click para buscar y seleccionar el documento" />
                            <input id="uploadfiles" type="button" class="boton btn btn-primary btn-raised"
                               title="Click para subir el archivo seleccionado" value="Subir archivo" alt="Click para subir el archivo seleccionado" />
                        {else}
                            <input type="button" class="boton btn btn-defoult btn-raised"
                                   title="Opción inactiva" value="Seleccionar archivo" alt="Opción inactiva" />
                            <input type="button" class="boton btn btn-defoult btn-raised"
                                   title="Opción inactiva" value="Subir archivo" alt="Opción inactiva" />
                        {/if}
                    </div>
                </div>
        </div>
        <!--Contenedor para la grilla de documentos adjuntados-->
        <div id="tb_archiAdjuntos"></div>
    </div>
    <div id="contentVisualizador">
        <div id="montaArchivo"></div>
        <div class="text-center">
            <button id="btonListo" type="button" class="btn btn-defoult btn-raised" title="Click para volver a la ventana anterior" alt="Click para volver a la ventana anterior">Volver</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#txt_fechaDoc').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language: 'es'});
        $('#contentVisualizador').hide();
        var uploaderC = new plupload.Uploader({
            runtimes: 'html5',
            browse_button: 'pickfiles',
            container: document.getElementById('container'),
            url: url+'SubeArchivosMET',
            chunk_size: '1mb',
            max_retries: 0,
            unique_names: true,
            multi_selection: false,
            max_file_count: 1,
            multipart : true,
            multipart_params: {
                idPlanificacion: '',
                idActividadPlanif: '',
                txt_nombreDoc: '',
                txt_nroDoc: '',
                txt_fechaDoc: '',
                tipoProceso: ''
            },
            init: {
                PostInit: function () {
                    document.getElementById('filelist').innerHTML = '';
                    document.getElementById('uploadfiles').onclick = function () {
                        uploaderC.settings.multipart_params.idPlanificacion = $('#idPlanificacion').val();
                        uploaderC.settings.multipart_params.idActividadPlanif = $('#idActividadPlanif').val();
                        uploaderC.settings.multipart_params.txt_nombreDoc = $('#txt_nombreDoc').val();
                        uploaderC.settings.multipart_params.txt_nroDoc = $('#txt_nroDoc').val();
                        uploaderC.settings.multipart_params.txt_fechaDoc = $('#txt_fechaDoc').val();
                        uploaderC.settings.multipart_params.tipoProceso = $('#tipoProceso').val();
                        uploaderC.start();
                        return false;
                    };
                },
                FilesAdded: function (up, files) {
                    var max_files = 1;
                    plupload.each(files, function (file) {
                        if (up.files.length > max_files) {
                            swal("Información del proceso", "Sólo puedes agregar"+ max_files + ' archivos a la vez', "success");
                            up.removeFile(file);
                        } else {
                            document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ')<b></b></div>';
                            $("#filelist div:even").addClass("fondo-fila1");
                            $("#filelist div:odd").addClass("fondo-fila2");
                            if(!$('#txt_nombreDoc').val()){
                                swal("¡Atención!", "Ingrese el nombre del documento por favor antes de subír el archivo", "error");return;
                            }else if(!$('#txt_fechaDoc').val()){
                                swal("¡Atención!", "Ingrese la fecha del documento por favor antes de subír el archivo", "error");return;
                            }
                        }
                    });
                },
                UploadProgress: function (up, file) {
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span><font color="green">' + file.percent + "%</font></span>";
                },
                UploadComplete: function (up, files) {
                    //console.log(files);
                    plupload.each(files, function (file) {
                        var $nombre = file.name;
                        var $ext = $nombre.substr(-3);
                        $Gtemp_archiv += file.id + '.' + $ext + '|';
                    });
                    uploaderC.splice();
                },
                FileUploaded: function(uploader,file,response){
                    var $result=$.parseJSON(response.response);
                    if($result.result){
                        swal("Información del proceso", $result.message, "success");
                        metLimpiaFormAdjuntar();
                        metListarAjuntos();
                    }else{
                        swal("Información del proceso", $result.message, "error");
                        $('#filelist').html('');
                    }
                },
                Error: function (up, err) {
                    if(err.code==-600){
                        err.message='Archivo demasiado grande. Debe ser menor a 3 Mega Bit';
                    }else if(err.code==-601){
                        err.message='Extención de archivo no válida';
                    }
                    swal("¡Atención!", err.message, "error");
                }
            },
            filters: {
                max_file_size: '3mb',
                mime_types: [{ title: "Image files", extensions: "jpg,jpeg,png" },{ title: "pdf files", extensions: "pdf" }]
            },
            rename: true,
            resize: { width: 1600, height: 1100, quality: 300 },
        });
        uploaderC.init();
        $('#uploadfiles').click(function () {
            if(!$('#txt_nombreDoc').val()){
                swal("¡Atención!", "Ingrese el nombre del documento por favor", "error");return;
            }else if(!$('#txt_fechaDoc').val()){
                swal("¡Atención!", "Ingrese la fecha del documento por favor", "error");return;
            }else if($('#filelist').html().length==0){
                swal("¡Atención!", "Seleccione un archivo por favor", "error");return;
            }
        });
        /**
         * @brief funcion que permite reiniciar el formulario a sus valores por defecto.
         * @event evento de tipo click asociado al boton limpiar
         */
        metLimpiaFormAdjuntar=function () {
            $('#txt_nombreDoc').val('');$('#txt_nroDoc').val('');
            $('#txt_fechaDoc').val('');$('#filelist').html('');
            uploaderC.splice();
        };
        $('#pickfiles').click(function () {
            $('#filelist').html('');
            uploaderC.splice();
        });
        /**
         * @brief function que permite eliminar un adjunto perteneciente a una actividad.
         * @param idDocumento, id del registro de los metadatos del documento
         * @returns function
         */
        metEliminaAdjunto = function (idDocumento) {
            swal({
                title: 'Confirmación de proceso',
                text: 'Vas a eliminar el documento adjunto "'+$('#tdAjunto'+idDocumento).html()+'". ¿Estás de Acuerdo?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'EliminarArchivoAdjuntoMET', { idDocumento:idDocumento }, function (data) {
                        if(data.reg_afectado){
                            swal("Información del proceso", data.mensaje, "success");
                            metListarAjuntos();
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    },'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
        /**
         * Permite visualizar los archivos adjuntos de una actividad
         * @param $adjunto
         */
        metVerAdjunto = function ($adjunto) {
            $('#contentVisualizador').show();$('#formAdjuntos').hide();
            $('#montaArchivo').html('<iframe frameborder="0" src="'+$adjunto+'" width="100%" height="500px"></iframe>');
        }
        /**
         * Borra el archivo del contenedor y vuelve al form adjuntos
         */
        $('#btonListo').click(function(){
           $('#montaArchivo').html('');
            $('#contentVisualizador').hide();
            $('#formAdjuntos').show();
        });
    });
</script>
