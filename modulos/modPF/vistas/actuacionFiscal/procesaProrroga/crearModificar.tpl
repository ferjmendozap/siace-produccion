<style type="text/css">
    {literal}
        .tab-pane{padding: 0}
        .form-group > label, .form-group .control-label, .form-control {
            font-size: 12px;
            margin-bottom: 0;
            opacity: 1;
        }

        .fondo-fase:hover {
            color: #fdfdfd;
            background-color: #1D8888 !important;
        }
        tr.fondo-Subtotal,
        tr.fondo-Subtotal:hover {
            background-color: #bcd  !important;
        }
        tr.fondo-totales,
        tr.fondo-totales:hover {
            font-weight: bold;
            background-color: #90a4ae  !important;
        }
        .table thead>tr>th.vert-align{
            font-weight: bold; vertical-align: middle;
        }
    {/literal}
</style>
<div class="modal-body" style="padding:0;">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <div class="form-wizard-nav">
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary"></div>
                            </div>
                            <ul class="nav nav-justified">
                                <li><a id="tab_1" class="active" href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                                <li><a id="tab_2" href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">ACTIVIDADES</span></a></li>
                            </ul>
                        </div>

                        <div class="tab-content clearfix">
                            <!--INFORMACIÓN DE LA PLANIFICACIÓN DE ACTUACIÓN FISCAL-->
                            <div class="tab-pane active" id="tab1">
                                <div class="card">
                                    <div class="card-head style-primary-light">
                                        <header>Información General</header>
                                    </div>
                                    <div class="card-body">
                                        <form id="formAjaxTab1" class="form floating-label form-validation" novalidate="novalidate" role="form" method="post">
                                            <input type="hidden" id="valido" value="1" />
                                            <input type="hidden" id="accion" name="form[txt][accion]" value="{$accion}" />
                                            <input type="hidden" id="vmOpcionMenu" name="form[txt][vmOpcionMenu]" value="" />
                                            <input type="hidden" id="idPlanificacion" name="form[int][idPlanificacion]" value="{$idPlanificacion}" />
                                            <input type="hidden" id="estadoPlanificacion" name="form[txt][estadoPlanificacion]" value="{$estadoPlanificacion}" />
                                            <input type="hidden" id="codTipoProceso" name="form[int][codTipoProceso]" value="{$codTipoProceso}" />
                                            <input type="hidden" id="idActividadPlanif" name="form[int][idActividadPlanif]" value="{$formData.id_actividaPlanific}" />
                                            <input type="hidden" id="codPlanificacion" name="form[txt][codPlanificacion]" value="{$codPlanificacion}" />
                                            <input type="hidden" id="idProrroga" name="form[int][idProrroga]" value="{$formData.id_prorroga}" />
                                            <input type="hidden" id="estadoProrroga" name="form[txt][estadoProrroga]" value="{$formData.ind_estado}" />
                                            <input type="hidden" id="cantDiasProrroga" name="form[int][cantDiasProrroga]" value="{$formData.num_dias_prorroga}" / >
                                            <input type="hidden" id="secuenciaActividad" name="form[int][secuenciaActividad]" value="{$formData.secuencia_actividad}" />
                                            <!--input type="hidden" id="actualizarProrroga" name="form[int][actualizarProrroga]" value="" /-->
                                            <div class="row">
                                                <div class="col-sm-2 text-right">
                                                    <label class="control-label"">Prorroga Nro:</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div id="codProrroga">{$formData.cod_prorroga}</div>
                                                </div>
                                                <div class="col-sm-3 text-right">
                                                    <label class="control-label">Estado:</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div id="desc_estadoProrga">{$formData.desc_estadoProrga}</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2 text-right">
                                                    <label class="control-label"">Planificación Nro:</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div>{$codPlanificacion}</div>
                                                </div>
                                                <div class="col-sm-3 text-right">
                                                    <label class="control-label">Fecha Registro:</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div id="fecha_registro">{$formData.fec_registro_prorroga}</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2 text-right">
                                                    <label class="control-label"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span>Motivo:</label>
                                                </div>
                                                <div class="col-sm-7">
                                                    <textarea id="txt_motivoProrroga" name="form[txt][txt_motivoProrroga]" rows="1" cols="60" class="form-control">{$formData.ind_motivo}</textarea>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-sm-2 text-right">
                                                    <label class="control-label"">Preparado por:</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div id="prepradaPor">{$formData.preparada_por}</div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label class="control-label"">Fecha:<span id="fechaPreparada">{$formData.fec_prorroga_preparado}</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2 text-right">
                                                    <label class="control-label"">Revisado por:</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div id="revisadaPor">{$formData.revisada_por}</div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label class="control-label"">Fecha:<span id="fechaRevisada">{$formData.fec_prorroga_revisado}</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2 text-right">
                                                    <label class="control-label"">Aprobado por:</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div id="aprobadaPor">{$formData.aprobada_por}</div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label class="control-label"">Fecha:<span id="fechaAprobada">{$formData.fec_prorroga_aprobado}</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2 text-right">
                                                    <label class="control-label"">Última modificación:</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div id="actualizadaPor">{$formData.actualizada_por}</div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label class="control-label"">Fecha:<span id="fechaModificacion">{$formData.fec_actualiza_prorroga}</span></label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--PANEL DE ACTIVIDADES-->
                            <div class="tab-pane floating-label" id="tab2">
                                <div class="card">
                                    <div class="card-head style-primary-light">
                                        <header>Actividades</header>
                                    </div>
                                    <div class="card-body">
                                        <form id="formAjaxTab2" class="form floating-label form-validation" role="form" method="post">
                                            {*Contenedor para las Actividades*}
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div id="tb_actividades"></div>
                                                 </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div align="center">
                            <button type="button" class="btn btn-default btn-raised" data-dismiss="modal" title="Click para Salir"  alt="Click para Salir">
                                <span class="glyphicon glyphicon-floppy-remove"></span>Cancelar
                            </button>
                            {if $permisoUser AND $botonAnular}
                                <button type="button" id="btn_anularProrroga" class="btn btn-danger btn-raised" title="Click para anular la prórroga" alt="Click para anular la prórroga">
                                    <span class="glyphicon glyphicon-ban-circle"></span>Anular
                                </button>
                            {/if}
                            {if $permisoUser}
                                {$activarBoton=false}
                                {if $botonGuardar}
                                    {$nombreBoton='Guardar'} {$idBoton='btn_guardaProrroga'}
                                    {$tooltic='Click para guardar'} {$activarBoton=true}
                                {elseif $botonRevisar}
                                    {$nombreBoton='Procesar Revisión'} {$idBoton='btn_revisaProrroga'}
                                    {$tooltic='Click para procesar la revisión'} {$activarBoton=true}
                                {elseif $botonAprobar}
                                    {$nombreBoton='Procesar Aprobación'} {$idBoton='btn_apruebaProrroga'}
                                    {$tooltic='Click para procesar la aprobación'} {$activarBoton=true}
                                {/if}
                                {if $activarBoton}
                                    <button type="button" id="{$idBoton}" class="btn btn-primary ink-reaction btn-raised" title="{$tooltic}"  alt="{$tooltic}">
                                        <span class="glyphicon glyphicon-floppy-saved"></span>{$nombreBoton}
                                    </button>
                                {/if}
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<span class="clearfix"></span>
<script type="text/javascript">
    var appf="";
    $(document).ready(function () {
        $('#vmOpcionMenu').val($('#opcionMenu').val());
        appf=new AppfFunciones();
        var app = new AppFunciones();
        app.metWizard();
        // ancho de la Modal
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy",language: 'es'});
        var url = '{$_Parametros.url}modPF/actuacionFiscal/procesaProrrogaCONTROL/';
        $("#tituloForm").html('{$tituloForm}');
        $("#formAjaxTab1").submit(function(){ return false; });
        $("#formAjaxTab1").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        });
        /**
         * Monta actividades en la grilla
         */
        function metMontaActividades(){
            $('#tb_actividades').html('');
            $.post(url+'BuscaActividadesMET',$("#formAjaxTab1").serialize(), function (data) {
                if(data){
                    $('#tb_actividades').html(data);
                    if($('#idProrroga').val()){
                        metRecalcularActividades();
                    }
                }else{
                    swal("Atención!", "Las actividades no se pudieron montar. Intente mas tarde", "error");
                }
            });
        }
        /**
         *Ejecuta la visualización de la grilla con las actividades
         */
        $('#tab_2').click(function(){
            if($('#datatablePf > tbody > tr').length == 0) {
                metMontaActividades();
            }
        });
        /**
         * Calcula los subtotales y totales de días de las actividades
         */
        function metSubtotales(){
            var totalxFase=0, cantLapso= 0, cantDiasAfecta= 0, cantDiasProrga= 0, cantDiasProrgaAcum= 0,
                    totalProrgaAcumxfase= 0, totalProrgaAcum= 0, totalProrgaxFase= 0, totalProrgaFases=0;
            $("#tbody_actividades > tr").each(function (index) {
                if($(this).attr('id')=='tr_actividad'){
                    cantLapso=Number($(this).find("td[id*='td_lapsoActividad']").html());
                    cantDiasProrgaAcum=$(this).find("td[id*='td_prorrAcum']").html();
                    if($(this).find("td[id*='td_cantDiasProrga']").attr('campoProrga')){
                        cantDiasProrga=$(this).find("input[id*='txt_dias_prorroga']").val();
                    }else{
                        cantDiasProrga=Number($(this).find("td[id*='td_cantDiasProrga']").html());
                    }
                    if($(this).find("td[id*='td_lapsoActividad']").attr('actividaAfecta')=='S'){
                        cantDiasAfecta=Number(cantDiasAfecta) + (Number(cantLapso) + Number(cantDiasProrgaAcum) + Number(cantDiasProrga));
                    }
                    totalxFase=Number(totalxFase) + (Number(cantLapso) + Number(cantDiasProrgaAcum) + Number(cantDiasProrga));
                    totalProrgaAcumxfase=Number(totalProrgaAcumxfase)+Number(cantDiasProrgaAcum);
                    totalProrgaxFase=Number(totalProrgaxFase)+Number(cantDiasProrga);
                }
                if($(this).attr('id')=='tr_subtotalFase'){
                    $(this).find("td[id*='td_subtotalFase']").html(totalxFase);
                    $(this).find("td[id*='td_subTotalProrgaAcumFase']").html(totalProrgaAcumxfase);
                    $(this).find("td[id*='td_subTotalProrgaFase']").html(totalProrgaxFase);
                    totalProrgaAcum=Number(totalProrgaAcum) + Number(totalProrgaAcumxfase);
                    totalProrgaFases=Number(totalProrgaFases)+Number(totalProrgaxFase);
                    totalxFase=0; totalProrgaAcumxfase=0; totalProrgaxFase=0; cantLapso=0; cantDiasProrga=0;
                }
                if($(this).attr('id')=='tr_totalPlan'){
                    $(this).find("td[id*='td_totalAfecta']").html(cantDiasAfecta);
                    $(this).find("td[id*='td_totalProrgaAcum']").html(totalProrgaAcum);
                    $(this).find("td[id*='td_totalProrroga']").html(totalProrgaFases);
                }
            });
        }
        /**
         * Recalcula los lapsos de las actividades
         */
        metRecalcularActividades=function(){
            $('#cantDiasProrroga').val($('#txt_dias_prorroga').val());
            if($("#tb_actividades tbody > tr").length>0) {
                $.post(url+'RecalcularActividadesMET', $("#formAjaxTab1").serialize(), function (rcrset) {
                    if (rcrset['filas']) {
                        metSubtotales();
                        $.each(rcrset.filas, function (indice, fila) {
                            if ($('#td_fecha_inicio' + fila.indiceFila).length > 0) {
                                $('#td_fecha_inicio_real' + fila.indiceFila).html(fila.fecha_inicialReal);
                                $('#td_fecha_fin_real' + fila.indiceFila).html(fila.fecha_fin_actividadReal);
                            }
                        });
                    }
                }, 'json');
            }
        }
        /**
         * Valida la entrada de datos
         */
        function metValidaEntrada(){
            var retornar=false;
            appf.metDepuraText2('txt_motivoProrroga');
            if(!$('#txt_motivoProrroga').val()){
                swal("¡Atención!", "Debes ingresar el motivo de la prórroga", "error");
            }else if($('#txt_dias_prorroga').val()==undefined || $('#txt_dias_prorroga').val()==0){ 
                swal("¡Atención!", "Debes ingresar la cantidad de días de prórroga a la actividad", "error");
            }else{
                retornar=true;
            }
            return retornar;
        }
        /**
         * Ejecuta el ingreso de una nueva prórroga ó su actualización
         */
        $('#btn_guardaProrroga').click(function () {
            var msj='', msjTitle='';
            if(!$('#idProrroga').val()){
                if(metValidaEntrada()){
                    msjTitle='Agregar Prórroga';
                    msj='¿Se va a agregar una nueva prórroga estás de acuerdo?';
                }else{
                    return;
                }
            }else{
                if(metValidaEntrada()){
                    msjTitle='Actualizar Prórroga';
                    msj='¿Se va a actualizar la prórroga actual estás de acuerdo?';
                }else{
                    return;
                }
            }
            swal({
                title: msjTitle,
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'GuardarProrrogaMET', $("#formAjaxTab1").serialize(), function (data) {
                        if (data.result=="nuevo" && data.idProrroga) {
                            $('#accion').val('actualizar');
                            $('#preTituloForm').html(' Actualizar ');
                            $('#idProrroga').val(data.idProrroga);
                            $('#codProrroga').html(data.codProrroga);
                            $('#fecha_registro').html(data.fechaProceso);
                            $('#prepradaPor').html(data.nombrePersEjecuta);
                            $('#fechaPreparada').html(data.fechaProceso);
                            $('#actualizadaPor').html(data.nombrePersEjecuta);
                            $('#fechaModificacion').html(data.fechaProceso);
                            swal("Información del proceso", data.mensaje, "success");
                            metBuscar(false);//Refresca el listado principal
                        } else if(data.result=="modificada"){
                            $('#actualizadaPor').html(data.nombrePersEjecuta);
                            $('#fechaModificacion').html(data.fechaProceso);
                            swal("Información del proceso", data.mensaje, "success");
                            metBuscar(false);//Refresca el listado del form principal
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("Proceso cancelado", "El proceso se ha cancelado", "error");
                }
            });
        });
        /**
         * Ejecuta la anulación de una prórroga
         */
        $('#btn_anularProrroga').click(function(){
            swal({
                title: 'Anular Prórroga',
                text: '¿Seguro que deseas anular la prórroga actual?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'AnularProrrogaMET', $("#formAjaxTab1").serialize(), function(data){
                        if (data.reg_afectado) {
                            $('#estadoProrroga').html(data.estado_prorroga);
                            $('#desc_estadoProrga').html(data.desc_estadoProrga);
                            $('#actualizadaPor').html(data.nombrePersEjecuta);
                            $('#fechaModificacion').html(data.fechaProceso);
                            appf.metEstadoObjForm('txt_dias_prorroga',false);
                            $('#btn_anularProrroga').hide();
                            $('#btn_guardaProrroga').hide();
                            swal("Información del proceso", data.mensaje, "success");
                            metBuscar(false);//Refresca el listado del form principal
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("Proceso cancelado", "El proceso se ha cancelado", "error");
                }
            });
        });
        /**
         * Ejecuta la revisión de una prórroga
         */
        $('#btn_revisaProrroga').click(function(){
            swal({
                title: 'Revisar Prórroga',
                text: '¿Seguro que deseas procesar la revisión de la prórroga actual?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'RevisarProrrogaMET', $("#formAjaxTab1").serialize(), function(data){
                        if (data.reg_afectado) {
                            $('#estadoProrroga').html(data.estado_prorroga);
                            $('#desc_estadoProrga').html(data.desc_estadoProrga);
                            $('#revisadaPor').html(data.nombrePersEjecuta);
                            $('#fechaRevisada').html(data.fechaProceso);
                            $('#actualizadaPor').html(data.nombrePersEjecuta);
                            $('#fechaModificacion').html(data.fechaProceso);
                            $('#btn_revisaProrroga').hide();
                            swal("Información del proceso", data.mensaje, "success");
                            metBuscar(false);//Refresca el listado del form principal
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("Proceso cancelado", "El proceso se ha cancelado", "error");
                }
            });
        });
        /**
         * Ejecuta la aprobación de una prórroga
         */
        $('#btn_apruebaProrroga').click(function(){
            swal({
                title: 'Aprobar Prórroga',
                text: '¿Seguro que deseas procesar la aprobación de la prórroga actual?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'AprobarProrrogaMET', $("#formAjaxTab1").serialize(), function(data){
                        if (data.reg_afectado) {
                            $('#estadoProrroga').html(data.estado_prorroga);
                            $('#desc_estadoProrga').html(data.desc_estadoProrga);
                            $('#aprobadaPor').html(data.nombrePersEjecuta);
                            $('#fechaAprobada').html(data.fechaProceso);
                            $('#actualizadaPor').html(data.nombrePersEjecuta);
                            $('#fechaModificacion').html(data.fechaProceso);
                            $('#btn_apruebaProrroga').hide();$('#btn_anularProrroga').hide();
                            swal("Información del proceso", data.mensaje, "success");
                            metBuscar(false);//Refresca el listado del form principal
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("Proceso cancelado", "El proceso se ha cancelado", "error");
                }
            });
        });
    });
</script>