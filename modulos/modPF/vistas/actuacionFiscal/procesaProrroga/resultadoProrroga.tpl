<style type="text/css">
    {literal}
    .table tbody>tr>td.vert-align{
        vertical-align: middle;
    }
    .text-icon{
        font-size: 9px;
    }
    .texto-negritas{
        font-weight: bold;
    }
    {/literal}
</style>
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Prórroga Nro.</th>
                        <th class="text-center">Descripción</th>
                        <th class="text-center">Días</th>
                        <th class="text-center">Fecha Reg.</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Opci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listado}
                        <tr>
                            <td class="vert-align text-center">{$fila.cod_prorroga}</td>
                            <td class="text-justify">
                                <span class="texto-negritas">Actividad:</span><br>{$fila.txt_descripcion_actividad}<br><span class="texto-negritas">Motivo Prórroga:</span><br>{$fila.ind_motivo}
                            </td>
                            <td class="vert-align text-center">{$fila.num_dias_prorroga}</td>
                            <td class="vert-align text-center">{$fila.fec_registro_prorroga}</td>
                            <td class="vert-align text-center">{$fila.desc_estado}</td>
                            <td class="vert-align text-center">
                                <button id="btn_crear" class="btn btn-raised btn-primary btn-xs {if $fila.iCon eq 'ver'}btn-warning{/if}"
                                        onclick="metMontarProrroga({$fila.pk_num_planificacion}{','}{"'"}{$fila.cod_planificacion}{"'"}{','}{"'"}{$fila.ind_estado_planificacion}{"'"}{','}{$fila.pk_num_prorroga_actividad}{','}{"'"}{$fila.ind_estado}{"'"}{','}{$fila.pk_num_prorroga_actividad})"
                                        title="Click para montar Prórroga" alt="Click para montar Prórroga">
                                        {if $fila.iCon eq 'act'}
                                            <i class="fa fa-edit" style="color: #ffffff;">
                                        {else}
                                            <i class="md md-remove-red-eye"></i>
                                        {/if}
                                    </i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>