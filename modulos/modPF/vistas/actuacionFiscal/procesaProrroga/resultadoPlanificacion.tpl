<style type="text/css">
    {literal}
    .table tbody>tr>td.vert-align{
        vertical-align: middle;
    }
    .text-icon{
        font-size: 9px;
    }
    {/literal}
</style>
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Planificación Nro.</th>
                        <th class="text-center">Objetivo</th>
                        <th class="text-center">Ente</th>
                        <th class="text-center">Fecha Inicio</th>
                        <th class="text-center">Fecha fin</th>
                        <th class="text-center">Fec fin Real</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Opci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listado}
                        <tr id="tr_atuacion_{$fila.pk_num_planificacion}">
                            <td class="vert-align">{$fila.cod_planificacion}</td>
                            <td class="vert-justify">{$fila.ind_objetivo}</td>
                            <td>{$fila.nombre_ente}</td>
                            <td class="vert-align">{$fila.fec_inicio}</td>
                            <td class="vert-align">{$fila.fecha_finplan}</td>
                            <td class="vert-align">{$fila.fecha_finrealplan}</td>
                            <td class="vert-align">{$fila.desc_estado}</td>
                            <td class="vert-align">
                                <input type="radio" id="rd_planificacion{$fila.pk_num_planificacion}" name="rd_planificacion"
                                       onclick="metValoresPlanificacion({$fila.pk_num_planificacion}{','}{"'"}{$fila.cod_planificacion}{"'"}{','}{"'"}{$fila.ind_estado_planificacion}{"'"})"
                                       title="Click para seleccionar" alt="Click para seleccionar" {if !$botonNuevaProrga}disabled=true{/if} />
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="8" align="center">
                            {if $botonNuevaProrga}
                                <button id="btn_nuevo" class="button btn ink-reaction btn-raised btn-info"
                                   data-keyboard="false" data-backdrop="static" title="Click para montar la prórroga" alt="Click para montar la prórroga"
                                   onclick="metCrear(true)"
                                ><i class="md md-create"></i>&nbsp;<span id="btnProceso">NUEVA PRÓRROGA<span></button>
                            {else}
                                <button class="button btn ink-reaction btn-raised btn-default"
                                   title="No tiene permiso para este proceso" alt="No tiene permiso para este proceso">
                                    <i class="md md-create"></i>&nbsp;<span id="btnProceso">NUEVA PRÓRROGA<span>
                                </button>
                            {/if}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>