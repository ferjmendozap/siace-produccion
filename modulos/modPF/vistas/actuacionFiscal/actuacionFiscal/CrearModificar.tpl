<style type="text/css">
    {literal}
        .tab-pane{padding: 0}
        .form-group > label, .form-group .control-label, .form-control {
            font-size: 12px;
            margin-bottom: 0;
            opacity: 1;
        }
    {/literal}
</style>
<div class="modal-body" style="padding:0;">
    <div class="container-fluid">
        <div id="rootWizard" class="form-wizard form-wizard-horizontal">
            <div class="form-wizard-nav">
                <div class="progress">
                    <div class="progress-bar progress-bar-primary"></div>
                </div>
                <ul class="nav nav-justified">
                    <li><a id="tab_1" class="active" href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                    <li><a id="tab_2" href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">AUDITORES</span></a></li>
                    <li><a id="tab_3" href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">ACTIVIDADES</span></a></li>
                </ul>
            </div>
            <div class="tab-content clearfix">
                <!--INFORMACIÓN DE LA PLANIFICACIÓN-->
                <div class="tab-pane active" id="tab1">
                    <div class="card" style="padding:0;">
                        <div class="card-head style-primary-light">
                            <header>Información General</header>
                        </div>
                        <div class="card-body">
                            <form id="formAjaxTab1" action="{$_Parametros.url}modPF/actuacionFiscal/actuacionFiscalCONTROL/CrearModificarMET" class="form-horizontal floating-label form-validation" novalidate="novalidate" role="form" method="post">
                                <input type="hidden" name="valido" value="1" />
                                <input type="hidden" id="opcion_modal" name="form[txt][opcion_modal]" value="{$opcion_modal}" />
                                <input type="hidden" id="idPlanificacion" name="form[int][idPlanificacion]" value="{$idPlanificacion}" />
                                <input type="hidden" id="aditores_asig" name="form[int][aditores_asig]" value="{if isset($actuacion.aditores_asig)}{$actuacion.aditores_asig}{else}0{/if}" />
                                <input type="hidden" id="temp_fecha_inicio" name="form[txt][temp_fecha_inicio]" value="{if isset($actuacion.fec_inicio)}{$actuacion.fec_inicio}{else}{/if}" />
                                <input type="hidden" id="temp_tipoAtcuacion" name="form[txt][temp_tipoAtcuacion]" value="{if isset($actuacion.fk_a006_num_miscdet_tipoactuacion)}{$actuacion.fk_a006_num_miscdet_tipoactuacion}{else}{/if}" />
                                <input type="hidden" id="opcion_actividad" name="form[txt][opcion_actividad]" value="{$opcion_actividades}" />
                                <input type="hidden" id="idPersonaEnte" name="form[txt][idPersonaEnte]" value="{if isset($actuacion.fk_a041_num_persona_ente)}{$actuacion.fk_a041_num_persona_ente}{else}{/if}" />
                                <input type="hidden" id="estadoPlanificacion" value="{$actuacion.ind_estado}" />
                                <div class="form-group">
                                    <!--Código de la  Actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="lbl_codplanificacion" class="control-label">Actuación Nro.:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="lbl_codplanificacion" style="margin-top: 8px;">{if isset($actuacion.cod_planificacion)}{$actuacion.cod_planificacion}{/if}</div>
                                    </div>
                                    <!--Estado de la Actuación-->
                                    <div class="col-sm-2 text-right">
                                        <label for="lbl_estado" class="control-label">Estado:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_estado" style="margin-top: 7px;">{if isset($actuacion.desc_estado)}{$actuacion.desc_estado}{/if}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--organismo ejecutante-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_contraloria" class="control-label">Contraloría:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="id_contraloriaError" >
                                            <select id="id_contraloria" name="form[int][id_contraloria]" class="form-control select2" disabled="disabled">
                                                {foreach item=fila from=$listaContraloria}
                                                    <option value="{$fila.id_contraloria}" selected>{$fila.nombre_contraloria}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <!--fecha inicio actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="fec_inicio" class="control-label">Fecha Inicio:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group date" id="fec_inicioError">
                                            <div class="input-group-content">
                                                <input id="fec_inicio" type="text" name="form[txt][fec_inicio]" class="form-control" style="margin-top: 2px;" value="{$actuacion.fec_inicio}" {if $opcion_modal=='Ver'}disabled{/if} placeholder="00-00-0000">
                                            </div>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--dependencia interna de la contraloría-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_dependencia" class="control-label">Dependencia:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="id_dependenciaError">
                                            <select id="id_dependencia" name="form[int][id_dependencia]" class="form-control select2-list select2""
                                                {if isset($actuacion.fk_a004_num_dependencia)}disabled{/if}>
                                                <option value="">Seleccione..</option>
                                                {if $listaDepContraloria|count > 0}
                                                    {foreach item=fila from=$listaDepContraloria}
                                                        {if isset($actuacion.fk_a004_num_dependencia)}
                                                            {if $fila.id_dependencia eq $actuacion.fk_a004_num_dependencia}
                                                                <option value="{$fila.id_dependencia}" selected>{$fila.nombre_dependencia}</option>
                                                            {else}
                                                                <option value="{$fila.id_dependencia}">{$fila.nombre_dependencia}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$fila.id_dependencia}">{$fila.nombre_dependencia}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                    <!--fecha termino actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="fec_termino" class="control-label">Fecha Termino:</label>
                                    </div>
                                    <div class="col-sm-2">
                                            <div id="lbl_fecha_fin_plan" style="margin-top: 7px;">{$actuacion.fecha_fin_plan}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--Dependencias Centro de Costo:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_centro_costo"class="control-label">Centro de Costo:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="pk_num_centro_costoError">
                                            <select id="pk_num_centro_costo" name="form[int][pk_num_centro_costo]" class="form-control select2-list select2"" {if $opcion_modal=='Ver'}disabled{/if}>
                                                {if $listaCentroCosto|count > 0}
                                                    {foreach item=fila from=$listaCentroCosto}
                                                        {if $idPlanificacion > 0 and ($actuacion.fk_a023_num_centro_costo eq $fila.pk_num_centro_costo)}
                                                            <option value="{$fila.pk_num_centro_costo}" selected>{$fila.ind_descripcion_centro_costo}</option>
                                                        {else}
                                                            <option value="{$fila.pk_num_centro_costo}">{$fila.ind_descripcion_centro_costo}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                    <!--Duracion afecta -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracion_afecta" class="control-label">Duración Afecta:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_duracion_afectacion" style="margin-top: 8px;">{$actuacion.cant_dias_afecta}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--Tipo de actuación:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="idtipo_actuacion"class="control-label">Tipo Actuacion:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="idtipo_actuacionError">
                                            <select id="idtipo_actuacion" name="form[int][idtipo_actuacion]" class="form-control select2"" {if $opcion_modal=='Ver'}disabled{/if}>
                                                <option value="">Seleccione..</option>
                                                {if $listaTipoActuacion|count > 0}
                                                    {foreach item=filatipo from=$listaTipoActuacion}
                                                        {if $idPlanificacion > 0 and ($actuacion.fk_a006_num_miscdet_tipoactuacion eq $filatipo.idtipo_actuacion)}
                                                            <option value="{$filatipo.idtipo_actuacion}" selected>{$filatipo.nombretipo_actuacion}</option>
                                                        {else}
                                                            <option value="{$filatipo.idtipo_actuacion}">{$filatipo.nombretipo_actuacion}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                    <!--Duracion no afecta -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracion_no_afecta" class="control-label">Duración no Afecta:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_duracion_no_afectacion" style="margin-top: 7px;">{$actuacion.cant_dias_no_afecta}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--Entes Externos-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_ente" id="idPersonaEnteError" class="control-label">
                                            <a id="lbl_ente" href="#" data-toggle="modal" data-target="#formModal3"
                                               data-keyboard="false" data-backdrop="static"
                                               title="Click para buscar el ente" alt="Click para buscar el ente">
                                                <span class="md md-account-balance"></span>Ente:
                                            </a>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="nombre_ente">{if isset($actuacion.nombre_ente)}{$actuacion.nombre_ente}{else}{/if}</div>
                                    </div>
                                    <!--Prorroga-->
                                    <div class="col-sm-2 text-right">
                                        <label for="prorroga" class="control-label">Prorroga:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label id="lbl_prorroga" style="margin-top: 8px;">{if isset($actuacion.cant_dias_prorroga)}{$actuacion.cant_dias_prorroga}{else}0{/if}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--origen de la actuacion:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="idorigen_actuacion" class="control-label">Origen actuación:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="idorigen_actuacionError">
                                            <select id="idorigen_actuacion" name="form[int][idorigen_actuacion]" class="form-control select2" {if $opcion_modal=='Ver'}disabled{/if}>
                                                <option value="">Seleccione..</option>
                                                {if $listaOrigenActuacion|count > 0}
                                                    {foreach item=fila from=$listaOrigenActuacion}
                                                        {if $idPlanificacion>0 and ($actuacion.fk_a006_num_miscdet_origenactuacion eq $fila.idorigen_actuacion)}
                                                            <option value="{$fila.idorigen_actuacion}" selected>{$fila.nombre_origenactuacion}</option>
                                                        {else}
                                                            <option value="{$fila.idorigen_actuacion}" {if $fila.cod_origen=='POA'}selected{/if}>{$fila.nombre_origenactuacion}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                    <!--Duración total -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracionTotal" class="control-label">Duración total:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label id="lbl_duracionTotal" style="margin-top: 7px;">{$actuacion.totalDias_Plan}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!---objetivo general-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_objetivo" class="control-label">Objetivo General:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div id="ind_objetivoError" >
                                            <textarea id="ind_objetivo" class="form-control" rows="2" name="form[txt][ind_objetivo]" {if $opcion_modal=='Ver'}disabled{/if}>{if isset($actuacion.ind_objetivo)}{$actuacion.ind_objetivo}{/if}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!---alcance-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_alcance" class="control-label">Alcance:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div id="ind_alcanceError" >
                                            <textarea id="ind_alcance" class="form-control" rows="2" name="form[txt][ind_alcance]" {if $opcion_modal=='Ver'}disabled{/if}>{if isset($actuacion.ind_alcance)}{$actuacion.ind_alcance}{/if}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!---observación-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_observacion" class="control-label">Observación:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div id="ind_observacionError">
                                            <textarea id="ind_observacion" class="form-control" rows="2" name="form[txt][ind_observacion]" {if $opcion_modal=='Ver'}disabled{/if}>{if isset($actuacion.ind_observacion)}{$actuacion.ind_observacion}{/if}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="hd_notanulacion" name="form[txt][hd_notanulacion]"/>
                            </form>
                            <!--NOTA DE JUSTIFICACIÓN DE ANULAR Ó CERRAR-->
                            <div id="div_anulaActuacion">
                                <br />
                                <form id="formAnularCerrarPlan" class="form-horizontal" role="form" method="post">
                                    <div class="form-group">
                                        <div class="col-sm-2 text-right">
                                            <label for="ind_objetivo" class="control-label">Nota Justificación:</label>
                                        </div>
                                        <div class="col-sm-10">
                                            <div id="txt_notaError">
                                                <textarea id="txt_nota" class="form-control" rows="2" name="form[txt][txt_nota]" style="width: 90%"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="text-center">
                                        <button type="button" id="btn_cancelAnulaPlan" class="btn btn-defoult btn-raised" title="Click para cancelar" alt="Click para cancelar">
                                            <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
                                        </button>
                                        <button type="button" id="btn_anulaPlan" class="btn btn-primary btn-raised" title="Click para anular la Planificación" alt="Click para anular la Planificación">
                                            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Anular
                                        </button>
                                        <button type="button" id="btn_cerrarPlan" class="btn btn-primary btn-raised" title="Click para cerrar la Planificación" alt="Click para cerrar la Planificación">
                                            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Cerrar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--PANEL AUDITORES-->
                <div class="tab-pane floating-label" id="tab2">
                    <div class="card">
                        <div class="card-head style-primary-light">
                            <header>Auditores</header>
                        </div>
                        <div class="card-body">
                            <form id="formAjaxTab2" action="{$_Parametros.url}modPF/actuacionFiscal/actuacionFiscalCONTROL/AsignaAuditorMET" class="form-horizontal floating-label form-validation" role="form" method="post">
                                {if $habilitaAsignacionResp}
                                    <div class="form-group">
                                        <!--dependencias internas de la contraloría-->
                                        <div class="col-sm-2 text-right">
                                            <label for="id_dep_auditor" class="control-label">Dependencia:</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <div id="id_dep_auditorError">
                                                <select id="id_dep_auditor" name="form[int][id_dep_auditor]" class="form-control input-sm">
                                                    {if $listaDepAuditor|count > 0}
                                                        {if !isset($idDependenciaUser)}
                                                            <option value="">Seleccione</option>
                                                        {/if}
                                                        {foreach item=fila from=$listaDepAuditor}
                                                            <option value="{$fila.id_dep_auditor}" {if $idDependenciaUser==$fila.id_dep_auditor}{$nivelAnterior=$fila.iddep_padre}selected{/if} {if $fila.toltick}title="{$fila.toltick}" alt="{$fila.toltick}"{/if}>{$fila.nombre_dep_auditor}</option>
                                                        {/foreach}
                                                        {if isset($idDependenciaUser)}
                                                            <option value="{if !$nivelAnterior}0{else}{$nivelAnterior}{/if}">-----Nivel Anterior-----</option>
                                                        {/if}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <button type="button" id="btnVolver" class="btn btn-info btn-raised">
                                                <span class="glyphicon glyphicon-share"></span>&nbsp;Listo
                                            </button>
                                        </div>
                                    </div>
                                {/if}
                                <div class="col-lg-12">
                                    {*Grilla Auditores asignados*}
                                    <div class="table-responsive">
                                        <table id="tb_auditores_plan" class="table table-hover table-striped no-margin">
                                            <thead>
                                            <tr>
                                                <th>Coordinador(a)</th>
                                                <th>Nombre Completo</th>
                                                <th>Cargo</th>
                                                <th>Credencial</th>
                                                <th>Fecha</th>
                                                <th>Acción</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {if $auditoresDesignados|count > 0}
                                                {if $habilitaAsignacionResp==false}{$check="disabled"}{/if}
                                                {foreach item=fila from=$auditoresDesignados}
                                                    <tr id="tr_auditor_{$fila.pk_num_empleado}">
                                                        <td>
                                                            {if $fila.flagCordinador==1}
                                                                <input type="radio" id="coordinador_{$fila.pk_num_empleado}" class="radio radio-styled" value="1" checked="true" onclick="metAuditorCoordinador({$fila.pk_num_empleado})" {$check} />
                                                            {else}
                                                                <input type="radio" id="coordinador_{$fila.pk_num_empleado}" class="radio radio-styled" value="0" onclick="metAuditorCoordinador({$fila.pk_num_empleado})" {$check} />
                                                            {/if}
                                                        </td>
                                                        <td id="td_auditor_{$fila.pk_num_empleado}">{$fila.nombre_auditor}</td>
                                                        <td>{$fila.cargo_auditor}</td>
                                                        <td>{$fila.desc_estatus}</td>
                                                        <td>{$fila.fecha_estatus}</td>
                                                        <td align="center">
                                                            {if $fila.opcion_auditor=='Quitar'}
                                                                <a href="#" button class="btn btn-xs btn-danger" title="Click para Quitar" alt="Click para Quitar" onclick="metQuitarAuditor({$fila.pk_num_empleado})">
                                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                                    </button>
                                                                </a>
                                                            {elseif $fila.opcion_auditor=='Activar' }
                                                                <a href="#" button class="btn btn-raised btn-primary btn-xs" title="Click para Activar" alt="Click para Activar" onclick="metActivarAuditor({$fila.pk_num_empleado})">
                                                                    <i class="fa fa-edit"></i>
                                                                    </button>
                                                                </a>
                                                            {else}
                                                                <button class="btn btn-raised btn-primary btn-xs" disabled>
                                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                                </button>
                                                            {/if}
                                                        </td>
                                                    </tr>
                                                {/foreach}
                                            {/if}
                                            </tbody>
                                        </table>
                                    </div>
                                    {*Grilla Auditores disponibles*}
                                    <div class="table-responsive">
                                        <table id="tb_auditores_disp" class="table no-margin">
                                            <thead>
                                            <tr>
                                                <th>Coordinador(a)</th>
                                                <th>Cédula</th>
                                                <th>Nombre Completo</th>
                                                <th>Cargo</th>
                                                <th>Acción</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--PANEL DE ACTIVIDADES-->
                <div class="tab-pane floating-label" id="tab3">
                    <div class="card">
                        <div class="card-head style-primary-light">
                            <header>Actividades</header>
                        </div>
                        <div class="card-body">
                            <form id="formAjaxTab3" action="{$_Parametros.url}modPF/actuacionFiscal/actuacionFiscalCONTROL/GuardaActividadesMET" class="form floating-label form-validation" role="form" method="post">
                                <input type="hidden" id="idPlanificacion_act" name="form[int][idPlanificacion_act]" value="{$idPlanificacion}"/>
                                <input type="hidden" id="opcion_actividades" name="form[txt][opcion_actividades]" value="{$opcion_actividades}"/>
                                <input type="hidden" id="idActividad" name="form[int][idActividad]"/>
                                <input type="hidden" id="fecha_inicio_actuacion" name="form[txt][fecha_inicio_actuacion]"/>
                                {*Contenedor para las Actividades*}
                                <div id="tb_actividades"></div>
                                {*Grilla de actividades disponibles para insertar*}
                                <table id="tb_actividades_disp" class="table no-margin table-condensed table-bordered" style="width: 100%;">
                                    <thead>
                                    <tr>
                                        <th class="text-center" style="width: 66%;">Actividad</th>
                                        <th class="text-center" style="width: 4%;">Días</th>
                                        <th class="text-center" style="width: 10%;">Afecta plan</th>
                                        <th class="text-center" style="width: 10%;">Opción</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <div align="center">
                                    {if $opcion_modal=='ingresar' or $opcion_modal=='Actualizar'}
                                        <button type="button" id="btn_guardaActividades" class="btn btn-primary btn-raised"
                                            {if $opcion_actividades=='ingresar'}
                                                title="Click para agregar las actvidades al plan" alt="Click para agregar las actvidades al plan"
                                            {else}
                                                title="Click para guardar cambios en los días de duración las actividades" alt="Click para guardar cambios en los días de duración las actividades"
                                            {/if}>
                                            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;
                                            {if $opcion_actividades=='ingresar'}Agregar Actividades{else}Guardar Cambios{/if}
                                        </button>
                                        <button type="button" id="btn_actividadDisp" class="btn btn-primary btn-raised" title="Click para listar actividades disponibles" alt="Click para listar actividades disponibles">
                                            <span class="glyphicon glyphicon-search"></span>&nbsp;Actividades Disponibles
                                        </button>
                                        <button type="button" id="btn_ActividadListo" class="btn btn-info btn-raised" title="Click para volver" alt="Click para volver">
                                            <span class="glyphicon glyphicon-share"></span>&nbsp;Listo
                                        </button>
                                    {/if}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_estado">
                <div class="row sinpaddingrow sinpaddingcol">
                    <div class="col-sm-3">
                        <div class="form-group floating-label">
                            <label for="preparador">Preparado por:</label>
                            <div style="font-size: 11px">{if isset($actuacion.preparado_por)}{$actuacion.preparado_por}{else}{''}{/if}</div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group floating-label">
                            <label for="revisador">Revisado:</label>
                            <div style="font-size: 11px">{if isset($actuacion.revisado_por)}{$actuacion.revisado_por}{else}{''}{/if}</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group floating-label">
                            <label for="aprobador">Aprobado por:</label>
                            <div style="font-size: 11px">{if isset($actuacion.aprobado_por)}{$actuacion.aprobado_por}{else}{''}{/if}</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group floating-label">
                            <label for="ultmodificacion"><i class="fa fa-calendar"></i>Fecha registro:</label>
                            <div id="lbl_fehareg" style="font-size: 11px">{if isset($actuacion.fec_ultima_modificacion)}{$actuacion.fec_ultima_modificacion}{else}{$fec_ultima_modificacion}{/if}</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group floating-label">
                            <label for="ultmodificacion"><i class="fa fa-calendar"></i>Última modificación:</label>
                            <div style="font-size: 11px">{if isset($actuacion.fec_ultima_modificacion)}{$actuacion.fec_ultima_modificacion}{else}{$fec_ultima_modificacion}{/if}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="barra_opciones" align="center">
                <button type="button" id="btn_salir" class="btn btn-default btn-raised" data-dismiss="modal" title="Click para Salir"  alt="Click para Salir">
                    <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
                </button>
                {*Botón Anular planificación*}
                {if in_array('PF-01-01-01-01-03-A',$_Parametros.perfil) and $opcion_modal=='Actualizar'}
                    <button type="button" id="btn_formAnularPlan" class="btn btn-warning btn-raised" title="Click para anular la planificación"  alt="Click para anular la planificación">
                        <span class="glyphicon glyphicon-ban-circle"></span>&nbsp;Anular
                    </button>
                {/if}
                {*Botón cerrar planificación*}
                {if in_array('PF-01-01-01-01-04-C',$_Parametros.perfil) and $actuacion.bton_cerrarplan}
                    <button type="button" id="btn_formcerrarPlan" class="btn btn-danger btn-raised" title="Click para cerrar la planificación"  alt="Click para cerrar la planificación">
                        <span class="glyphicon glyphicon-eye-close"></span>&nbsp;Cerrar
                    </button>
                {/if}
                {*Botón modificar planificación*}
                {if in_array('PF-01-01-01-01-01-N',$_Parametros.perfil) and $opcion_modal=='ingresar'}
                    <button type="button" id="btn_guardarPlan" class="btn_aux1 btn btn-primary btn-raised" title="Click para guardar los datos de la ficha"  alt="Click para guardar los datos de la ficha">
                        <span class="glyphicon glyphicon-floppy-disk"></span><span class="btn_aux2">&nbsp;Guardar</span>
                    </button>
                {elseif in_array('PF-01-01-01-01-02-M',$_Parametros.perfil) and $opcion_modal=='Actualizar'}
                    <button type="button" id="btn_guardarPlan" class="btn btn-primary btn-raised" title="Click para guardar los cambios en la ficha"  alt="Click para guardar los cambios en la ficha">
                        <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                    </button>
                {/if}
            </div>
        </div>
    </div>
</div>
<span class="clearfix"></span>
<script type="text/javascript">
    var metAsignarAuditor="",metQuitarAuditor="",metAgregarActividad="",appf="",metActivarAuditor="",metAuditorCoordinador="";
    $(document).ready(function () {
        appf=new AppfFunciones();
        var app = new AppFunciones();
        app.metWizard(); /*Activar el wizard*/
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");/*ancho de la Modal*/
        $('.select2').select2({ allowClear: true });
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy",language: 'es'});
        var url='{$_Parametros.url}modPF/actuacionFiscal/actuacionFiscalCONTROL/';
        $('#formAnular').hide();$('#formAnularCerrarPlan').hide();
        if($('#tb_auditores_plan > tbody > tr').length > 0) {
            $('#tb_auditores_plan').show();
        }else{
            $('#tb_auditores_plan').hide();
        }
        $("#formAjaxTab1").submit(function(){ return false; });
        $("#formAjaxTab1").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        });
        $('#btn_guardaActividades').hide();
        $('#tb_actividades_disp').hide();
        $('#btn_actividadDisp').hide();
        $('#btn_ActividadListo').hide();
        /**
         * Monta las dependencias centros de costo
         */
        function metLlenaSelectBoxCentroCosto(){
            $('#pk_num_centro_costo').html('').select2({ allowClear: true });
            if($('#id_dependencia').val()){
                $.post(url+'DependenciaCentroCostosMET', { idDependencia:$('#id_dependencia').val() }, function (rcrset) {
                    $.each(rcrset.filas, function(indice,fila){
                        if(indice==0){
                            $('#pk_num_centro_costo').append('<option value="" selected>Seleccione...</option>');
                        }
                        $('#pk_num_centro_costo').append('<option value="' + fila.pk_num_centro_costo + '">' + fila.ind_descripcion_centro_costo + '</option>');
                    });
                }, 'json');
            }
        }
        $('#id_dependencia').change(function (){
            metLlenaSelectBoxCentroCosto(); /*Monta las dependencias de centro costo*/
        });
        /**
         *Visualiza el form modal para seleccionar el ente externo involucrado
         */
        $('#lbl_ente').click(function(){
            $.post(url+'CargaGrillaEntesMET',{ lista:true }, function (dato) {
                $('#formModalLabel3').html('<i class="md md-search"></i> Listado Entes');
                $('#ContenidoModal3').html(dato);
            });
        });
        /**
         * Ejecuta la creación ó actualización del registro principal de la planificación.
         */
        $('#btn_guardarPlan').click(function () {
            appf.metDepuraText2('ind_objetivo,ind_alcance,ind_observacion');
            var msj="";
            if($("#idPlanificacion").val()==0){
                msj="Se va a ingresar los datos actuales para una nueva actuación fiscal";
            }else{
                msj="Se van a actualizar los datos actuales";
                if(($("#idtipo_actuacion").val()!=$("#temp_tipoAtcuacion").val()) && $("#opcion_actividad").val()=='actualizar'){
                    msj+=" y se eliminarán las actividades relacionadas al plan";
                }
                msj+=". Estas de Acuerdo";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post($("#formAjaxTab1").attr("action"), $("#formAjaxTab1").serialize(), function (data) {
                    msj="";
                    if (data.Error == 'error') {
                        appf.metActivaError(data);
                        swal("¡Atención!", "Los campos marcados con X en rojo son obligatorios", "error");
                    } else if (data.Error == 'errorSql') {
                        swal("¡Atención!", data.mensaje, "error");
                    } else {
                        if ($("#idPlanificacion").val() == 0 && data.idPlanificacion) {
                            $("#idPlanificacion").val(data.idPlanificacion);
                            $("#estadoPlanificacion").val("PR");
                            $("#temp_fecha_inicio").val($("#fec_inicio").val());
                            $('#temp_tipoAtcuacion').val($('#idtipo_actuacion').val());
                            appf.metEstadoObjForm('id_dependencia',false);
                            $('.btn_aux2').html('&nbsp;Guardar');
                            if(data.proc_actualizar){
                                $('.btn_aux1').attr({ title:'Click para guardar los cambios en la ficha', alt:'Click para los cambios en la ficha' });
                            }else{
                                $('.btn_aux1').addClass('disabled');
                            }
                            $('#btn_Buscar').click();
                        } else if (data.reg_afectado) {
                            if(($("#fec_inicio").val()!=$("#temp_fecha_inicio").val()) && $("#opcion_actividad").val()=='actualizar' && !$("#tb_actividades").html()){
                                metMontaActividades();
                            }
                            $("#temp_fecha_inicio").val($("#fec_inicio").val());
                            if($("#temp_tipoAtcuacion").val()!=$('#idtipo_actuacion').val()){
                                $('#opcion_actividades').val('ingresar');
                                $('#opcion_actividad').val('ingresar');
                                $('#temp_tipoAtcuacion').val($('#idtipo_actuacion').val());
                                $('#lbl_fecha_fin_plan').html('');
                                $('#lbl_duracion_afectacion').html(0);
                                $('#lbl_duracion_no_afectacion').html(0);
                                $('#lbl_prorroga').html(0);
                                $('#lbl_duracionTotal').html(0);
                            }
                            $('#btn_Buscar').click();
                        }
                        appf.metActivaError(data.dataValida);
                        swal("Información del proceso", data.mensaje+msj, "success");
                    }
                }, 'json');
            });
        });
        function metOcultarActividadDisp(){
            $('#btn_actividadDisp').hide();
            $("#tb_actividades_disp tbody").html('');
            $('#tb_actividades_disp').hide();
            $('#btn_ActividadListo').hide();
        }
        $('#tab_1').click(function(){ /*Ficha de Información general*/
            $('#div_estado').show();
            $('#btn_guardaActividades').hide(); $('#tb_actividades_disp').hide();
            $('#barra_opciones').show();
            metOcultarActividadDisp();
        });
        {**********AUDITORES************}
        $('#tab_2').click(function(){ /*Ficha de auditores*/
            metOcultarActividadDisp();
            $("#tab_2").attr({ "data-toggle":"tab" });
            if($("#idPlanificacion").val()==0 || ($("#temp_fecha_inicio").val()!=$("#fec_inicio").val())){
                $("#tab_2").attr({ "data-toggle":"tab disabled" });
                if($("#temp_fecha_inicio").val()!=$("#fec_inicio").val()) {
                    swal("Atención", "Debe guardar el cambio de fecha de inicio de la Planificación antes de seguir", "error");
                }else{
                    swal("Atención", "Debe ingresar y guardar los datos de la ficha Información General antes de seguir", "error");
                }
            }else{
                $('#div_estado').hide();
                $('#barra_opciones').hide();
                $('#btn_guardaActividades').hide();
                if($('#aditores_asig').val()==0){ //Se buscan los auditores
                    if(appf.metBuscaIdValorComboBox('id_dep_auditor',$('#id_dependencia').val())){ /*Si la dependencia de control seleccionada se encuentre entre las dependencias padres del combobox de auditores. Entra*/
                        $('#id_dep_auditor').val($('#id_dependencia').val()).change(); /*Ejecuta la extracción de auditores*/
                    }else{ /*Entra cuando la dependencia de control seleccionada es una hija y por lo tanto no se encuentra entre las dependencias padre del combobox de auditores. Entra*/
                        metListaAuditor(true,$('#id_dependencia').val()); /*Ejecuta la extracción de auditores*/
                    }
                }
            }
        });
        if($('#aditores_asig').val()==1){ /*Si hay auditores asignados. Entra*/
            $('#tb_auditores_disp').hide();$('#btnVolver').hide();
        }
        /**
         * Llena la grilla de auditores disponibles
         */
        function metLlenaGrillaAuditorDisp(){
            $('#tb_auditores_disp tbody').html('');$('#tb_auditores_disp').hide();
            if($("#id_dep_auditor").val()) {
                $.post(url+'LlenaGrillaAuditorMET', { idDepAuditor: $("#id_dep_auditor").val(),idPlanificacion:$("#idPlanificacion").val() }, function (rcrset) {
                    if (rcrset.filas) {
                        $('#tb_auditores_disp').show();
                        $.each(rcrset.filas, function (indice, fila) {
                            var inactivaRrbtn="";
                            if(!fila.asignaCoordinador){
                                inactivaRrbtn="disabled";
                            }
                            param = appf.metPreparaObj(fila);
                            $row = '<tr id="fila_'+fila.pk_num_empleado+'">'
                                    +'<td><input type="radio" id="coordinador_'+fila.pk_num_empleado+'" name="coordinador" class="radio radio-styled" '+inactivaRrbtn+' /></td>'
                                    + '<td>' + fila.cedula_auditor + '</td>'
                                    + '<td>' + fila.nombre_auditor + '</td>'
                                    + '<td>' + fila.cargo_auditor + '</td>'
                                    + '<td><a href="#" button class="btn btn-xs btn-info" title="Click para Agregar" alt="Click para Agregar" onclick="metAsignarAuditor(' + param + ')"><i class="md md-add" style="color: #ffffff;"></i></button></a></tr>';
                            $('#tb_auditores_disp tbody').append($row);
                        });
                    }
                }, 'json');
            }
        }
        /**
         * Llena el selectbox de dependencias de auditores
         */
        function metListaAuditor(listar,idDepAud){
            var nombreOpcion=$("#id_dep_auditor").find("option[value='"+$("#id_dep_auditor").val()+"']").text();
            if(idDepAud){ /*Entra por acá, cuando no hay auditores asignados a la planificación y se clica en la pestaña de auditores */
                var idDepAuditor=idDepAud;
            }else{
                var idDepAuditor=$("#id_dep_auditor").val();
            }
            $('#id_dep_auditor option').remove();
            $.post(url+'ListaDepAuditorMET', { idDepAuditor:idDepAuditor }, function (rcrset) {
                if(rcrset.filas){
                    $.each(rcrset.filas, function(indice,fila){
                        $tultic='title="'+fila.toltick+'" alt="' + fila.toltick +'"';
                        if(indice==0){
                            if(nombreOpcion=="-----Nivel Anterior-----"){
                                $('#id_dep_auditor').append('<option value="">Seleccione</option>');
                                $('#id_dep_auditor').append('<option value="'+fila.id_dep_auditor+'"'+$tultic+'>'+fila.nombre_dep_auditor+'</option>');
                            }else{
                                $('#id_dep_auditor').append('<option value="'+fila.id_dep_auditor+'"'+$tultic+'>'+fila.nombre_dep_auditor+'</option>');
                            }
                            $id_nivel_ant=fila.iddep_padre;
                        }else{
                            $('#id_dep_auditor').append('<option value="'+fila.id_dep_auditor+'"'+$tultic+'>'+fila.nombre_dep_auditor+'</option>');
                        }
                    });
                    if(listar){ metLlenaGrillaAuditorDisp() }
                }
                if(idDepAuditor>0){
                    if(!$id_nivel_ant){ $id_nivel_ant=0; }
                    $('#id_dep_auditor').append('<option value="'+$id_nivel_ant+'">-----Nivel Anterior-----</option>');
                }else{
                    $("#id_dep_auditor").find("option[value='0']").remove();
                }
            }, 'json');
        }
        /*Lista las dependencias de auditores de manera recursiva al seleccionar una*/
        $('#id_dep_auditor').change(function (){
            $('#btnVolver').show(); $('#tb_auditores_plan').hide();
            metListaAuditor(true);
        });
        $('#btnVolver').click(function(){
            if($('#tb_auditores_plan > tbody > tr').length > 0) {
                $('#tb_auditores_plan').fadeIn(1000).show();
            }
            $('#tb_auditores_disp tbody').html('');
            $('#tb_auditores_disp').hide();
            $('#btnVolver').hide();
            $('#id_dep_auditor').html('');
            $('#id_dep_auditor').html('<option value="0" selected>-----Nivel Anterior-----</option>');
            metListaAuditor(false);
        });
        function metLlenaGrillaAuditorPlan(rcrset){
            $('#tb_auditores_plan tbody').html('');
            $.each(rcrset.filas, function (indice, fila) {
                var opcionAuditor='', radioCoordinado='';
                if(fila.flagCordinador==1) {
                    radioCoordinado='<input type = "radio" id = "coordinador_'+fila.pk_num_empleado+'" class = "radio radio-styled" value = "1" checked="true" onclick = "metAuditorCoordinador('+fila.pk_num_empleado+')" / >';
                }else{
                    radioCoordinado='<input type="radio" id="coordinador_'+fila.pk_num_empleado+'" class="radio radio-styled" value="0" onclick="metAuditorCoordinador('+fila.pk_num_empleado+')" />';
                }
                if(fila.opcion_auditor=='Quitar') {
                    opcionAuditor = '<a href="#" button class="btn btn-xs btn-danger" title="Click para Quitar" alt="Click para Quitar" onclick="metQuitarAuditor(' + fila.pk_num_empleado + ')"><i class="md md-delete" style="color: #ffffff;"></i></button></a>';
                }else if(fila.opcion_auditor=='Activar'){
                    opcionAuditor='<a href="#" button class="btn btn-raised btn-primary btn-xs" title="Click para Activar" alt="Click para Activar" onclick="metActivarAuditor('+fila.pk_num_empleado+')"><i class="fa fa-edit"></i></button></a>';
                }
                $row = '<tr id="tr_auditor_'+fila.pk_num_empleado+'">'
                        +'<td>'+radioCoordinado+'</td>'
                        + '<td id="td_auditor_'+fila.pk_num_empleado+'">' + fila.nombre_auditor + '</td>'
                        + '<td>' + fila.cargo_auditor + '</td>'
                        + '<td>' + fila.desc_estatus + '</td>'
                        + '<td>' + fila.fecha_estatus + '</td>'
                        + '<td>'+opcionAuditor+'</tr>';
                $('#tb_auditores_plan tbody').append($row);
            });
        }
        /**
         * Ejecuta la asignación de un auditor.
         */
        metAsignarAuditor=function (params){
            var flagCoord=0;
            if($('#coordinador_'+params.pk_num_empleado).is(':checked')){ flagCoord=1; }
            swal({
                title: "Confirmación de proceso",
                text: "Se va a designar "+params.nombre_auditor+" a la actuación fiscal",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post($("#formAjaxTab2").attr("action"), { pk_num_empleado:params.pk_num_empleado,flagCoord:flagCoord,idPlanificacion:$("#idPlanificacion").val(), estado_planificacion:$('#estadoPlanificacion').val() }, function (data) {
                    if (data.Error == 'errorSql') {
                        swal("¡Atención!", data.mensaje, "error");
                    } else if (data.idDesignado) {
                        $("#aditores_asig").val(1);
                        metLlenaGrillaAuditorDisp();
                        metLlenaGrillaAuditorPlan(data.AudDesignados);
                        swal("Información del proceso", data.mensaje, "success");
                    }
                }, 'json');
            });
        };
        /**
         * Ejecuta el proceso de eliminar la asignación de un auditor de la planificación.
         */
        metQuitarAuditor=function(IdResponsable){
            swal({
                title: 'Quitar Designado',
                text: 'Seguro que desea quitar a "'+$('#td_auditor_'+IdResponsable).html()+'" de la Actuación Fiscal actual',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post(url+'QuitarAuditorMET', { IdResponsable:IdResponsable,idPlanificacion:$("#idPlanificacion").val(),estado_planificacion:$('#estadoPlanificacion').val() },function(data){
                    if(data.reg_afectado){
                        swal("Información del proceso", data.mensaje, "success");
                        if($('#estadoPlanificacion').val()=='AP'){
                            metLlenaGrillaAuditorPlan(data.AudDesignados);
                        }else{
                            $('#tr_auditor_'+IdResponsable).remove();
                        }
                        if($('#tb_auditores_plan > tbody > tr').length == 0) {
                            $('#tb_auditores_plan').fadeIn(1000).hide();
                            $('#aditores_asig').val(0);
                        }
                    }else{
                        swal("Atención!", data.mensaje, "error");
                    }
                },'json');
            });
        };
        /**
         * Activa una persona responsable si está como inactivo.
         */
        metActivarAuditor=function(IdResponsable){
            swal({
                title: 'Activar Responsable',
                text: 'Seguro que desea Activar a "'+$('#td_auditor_'+IdResponsable).html()+'" a la Actuación Fiscal actual',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
               swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
               $.post(url+'ActivarAuditorMET', { IdResponsable:IdResponsable,idPlanificacion:$("#idPlanificacion").val(),estado_planificacion:$('#estadoPlanificacion').val() },function(data){
                    if(data.reg_afectado){
                        swal("Información del proceso", data.mensaje, "success");
                        metLlenaGrillaAuditorPlan(data.AudDesignados);
                    }else{
                        swal("¡Atención!", data.mensaje, "error");
                    }
                },'json');
            });
        };
        /**
         * Activa a una persona responsable como coordinador de la planificación actual
         */
        metAuditorCoordinador=function(IdResponsable){
            if($('#coordinador_'+IdResponsable).val()==1){ return; }
            swal({
                title: 'Seleccionar Coordinador',
                text: 'Seguro que desea seleccionar a "'+$('#td_auditor_'+IdResponsable).html()+'" como Coordinador de la planificación actual',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm){
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'SeleccionarCoordinadoMET', { IdResponsable:IdResponsable,idPlanificacion:$("#idPlanificacion").val(),estado_planificacion:$('#estadoPlanificacion').val() },function(data){
                        if(data.reg_afectado){
                            swal("Información del proceso", data.mensaje, "success");
                            metLlenaGrillaAuditorPlan(data.AudDesignados);
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    },'json');
                }else{
                    $('#coordinador_'+IdResponsable).prop("checked",false);
                    swal("Información del Sistema", "Proceso cancelado", "success");
                }
            });
        };
        /********************ASIGNACIÓN DE ACTIVIDADES AL PLAN*******************************/
        /**
         * Monta actividades en la grilla
         */
        function metMontaActividades(){
            $('#tb_actividades').html('');
            $.post(url+'BuscaActividadesMET',{ idtipo_actuacion:$("#idtipo_actuacion").val(),
                fec_inicio:$("#fec_inicio").val(),opcion_actividades:$('#opcion_actividades').val(),idPlanificacion:$('#idPlanificacion').val(),
                opcion_modal:$('#opcion_modal').val() }, function (data) {
                if(data){
                    $('#btn_guardaActividades').show();
                    $('#tb_actividades').html(data);
                }else{
                    swal("Atención!", "No se encontró actividades disponibles para el Tipo de Actuación", "error");
                }
            });
        }
        $('#tab_3').click(function(){ /*Ficha de actividades*/
            metOcultarActividadDisp();
            $("#tab_3").attr({ "data-toggle":"tab" });
            if($("#aditores_asig").val()==0){
                $("#tab_3").attr({ "data-toggle":"tab disabled" });
                swal("Atención", "Debe asignar los auditores antes de seguir", "error");
            }else if($("#fec_inicio").val()!=$("#temp_fecha_inicio").val()){
                $("#tab_3").attr({ "data-toggle":"tab disabled" });
                swal("Atención", "Debe guardar el cambio de la fecha de Inicio de la Planificación antes de seguir", "error");
            }else if($("#idtipo_actuacion").val()!=$("#temp_tipoAtcuacion").val()){
                $("#tab_3").attr({ "data-toggle":"tab disabled" });
                swal("Atención", "Debe guardar el cambio del Tipo de Actuación de la Planificación antes de seguir", "error");
            }else{
                $('#div_estado').hide();
                $('#barra_opciones').hide();
                metMontaActividades();
            }
        });
        /**
         * Se sacan los totales de acuerdo a las cantidades de días de la actividades.
         */
        function metSubtotales(){
            var totalxFase= 0, valor= 0, itdsubtotal= 0, totalFases=0; totalNoAfecta=0;
            $("#tbody_actividades > tr > td").each(function (index) {
                if($(this).attr('id')=='afecta_plan_S'){
                    if($(this).find("input[id*='txt_duracion_actividad']").val()!=undefined){
                        valor=$(this).find("input[id*='txt_duracion_actividad']").val();
                        totalxFase = Number(totalxFase) + Number(valor);
                    }
                }else{
                    if($(this).find("input[id*='txt_duracion_actividad']").val()!=undefined){
                        valor=$(this).find("input[id*='txt_duracion_actividad']").val();
                        totalNoAfecta = Number(totalNoAfecta) + Number(valor);
                    }
                }
                if($(this).attr('name')=='td_Subtotal'){
                    $('#td_Subtotal'+itdsubtotal).html(totalxFase);
                    totalFases=Number(totalFases)+Number(totalxFase);
                    totalxFase=0;
                    itdsubtotal++;
                }
            });
            $('#td_total').html(totalFases);
            /*Refresca los totales en la ficha datos generales*/
            $('#lbl_duracion_afectacion').html(totalFases);
            $('#lbl_duracion_no_afectacion').html(totalNoAfecta);
            $('#lbl_duracionTotal').html(totalFases+totalNoAfecta);
        }
        /**
         * Ejecuta el recálculo de las actividades
         */
        metRecalcularActividades=function(){
            var fecha_inicial="",set_duracion="",fecha_fin_plan="";
            if($("#tbody_actividades > tr").length>0) {
                $('#formAjaxTab3').find(':input').each(function (indice) {
                    if ($('#txt_duracion_actividad' + indice).length > 0) {
                        if($('#txt_duracion_actividad' + indice).val()==0){ $('#txt_duracion_actividad' + indice).val(0) } /*Si no viene un entero coloca 1 */
                        set_duracion += $('#txt_duracion_actividad' + indice).val() + ',' + indice + 'S';
                    }
                });
                metSubtotales();
                hdd_dataActividad = "";
                $.post(url+'RecalcularActividadesMET',{ fecha_inicial: $('#fec_inicio').val(), set_duracion: set_duracion.slice(0, -1) },function (rcrset) {
                    if (rcrset['filas']) {
                        $.each(rcrset.filas, function (indice, fila) {
                            if ($('#td_fecha_inicio' + fila.indiceFila).length > 0) {
                                $('#td_fecha_inicio' + fila.indiceFila).html(fila.fecha_inicial);
                                $('#td_fecha_fin' + fila.indiceFila).html(fila.fecha_fin_actividad);
                                $('#td_fecha_inicio_real' + fila.indiceFila).html(fila.fecha_inicial);
                                $('#td_fecha_fin_real' + fila.indiceFila).html(fila.fecha_fin_actividad);
                                hdd_dataActividad = $('#hdd_dataActividad' + fila.indiceFila).val();
                                arrdata = hdd_dataActividad.split('N');
                                cadena = arrdata[0] + 'N' + fila.dias_duracion + 'N' + fila.fecha_inicial + 'N' + fila.fecha_fin_actividad + 'N' + fila.fecha_inicial + 'N' + fila.fecha_fin_actividad;
                                $('#hdd_dataActividad' + fila.indiceFila).val(cadena);
                                $('#lbl_fecha_fin_plan').html(fila.fecha_fin_actividad); /*Refresca la fecha fin plan de la ficha de información general*/
                            }
                        });
                    }
                }, 'json');
            }
        };
        /**
         * Recalcula las actividades al cambiar de fecha de inicio
         */
        $('#fec_inicio').change(function(){
            if($('#opcion_actividades').val()=="actualizar"){
                metRecalcularActividades();
            }
        });
        /**
         * Ejecuta la Asignación de las actividades a la planificación ó las actualiza.
         */
        $('#btn_guardaActividades').click(function(){
            $("#idPlanificacion_act").val($("#idPlanificacion").val());
            var msj="";
            if($('#opcion_actividades').val()=="ingresar"){
                msj="Se va a agregar las actividades actuales a la actuación fiscal";
            }else{
                msj="Se va a guardar los cambios en los días de duración de las actividades. Estas de Acuerdo";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post($("#formAjaxTab3").attr("action"), $("#formAjaxTab3").serialize(), function (data) {
                    if (data.result) {
                        $('#opcion_actividades').val(data.opcion_actividades);
                        $('#opcion_actividad').val(data.opcion_actividades);
                        swal("Información del proceso", data.mensaje, "success");
                        $('#btn_guardaActividades').html('Guardar Cambios');
                        $('#btn_guardaActividades').attr({ title:'Click para guardar cambios en los días de duración las actividades', alt:'Click para guardar cambios en los días de duración las actividades' });
                        metMontaActividades();
                    } else if (data.Error == 'errorSql') {
                        swal("¡Atención!", data.mensaje, "error");
                    }
                }, 'json');
            });
        });
        /**
         * Quita una actividad de la planificación.
         */
        metQuitarActividad=function(idAtividad_x){
            $("#idPlanificacion_act").val($("#idPlanificacion").val());
            $('#fecha_inicio_actuacion').val($('#fec_inicio').val());
            $('#idActividad').val(idAtividad_x);
            if($('#opcion_actividades').val()=="actualizar"){
                swal({
                    title: 'Quitar Actividad',
                    text: 'Seguro que desea quitar la Actividad "'+$('#spn_descActividad_'+idAtividad_x).html()+'" de la planificación',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'QuitarActividadMET', $("#formAjaxTab3").serialize(), function (data) {
                        if (data.result) {
                            if(!data.buscar_actividades_disp){
                                metMontaActividades();
                            }else{ /*Si No quedan actividades asignadas entra*/
                                $('#opcion_actividades').val('ingresar');
                                $('#opcion_actividad').val('ingresar');
                                $('#btn_guardaActividades').html('Agregar Actividades');
                                $('#btn_guardaActividades').attr({ title:'Click para agregar las actvidades al plan', alt:'Click para agregar las actvidades al plan' });
                                metMontaActividades(); /*Se montan todas las actividades disponibles apara agregar*/
                            }
                            swal("Actividad Removida", data.mensaje, "success");
                        } else {
                            swal("Atención!", data.mensaje, "error");
                        }
                    }, 'json');
                });
            }else{
                /*Entra cuando no hay actividades asignadas al plan y remueve una actividad de lista*/
                $('#tr_actividad_'+idAtividad_x).remove();
                metRecalcularActividades();
                /*Se calcula cantidad de actividades restantes*/
                var totalActividades=$('#datatablePf_info').html();
                totalActividades=totalActividades.split(':');
                totalActividades=totalActividades[1]-1;
                $('#datatablePf_info').html('Total Actividades:'+totalActividades);
                swal("Información de proceso", "Actividad removida", "success");
            }
        };
        /**
         * Busca actividades disponibles de la planificación.
         */
        $('#btn_actividadDisp').click(function(){
            $('#tb_actividades').html('');
            $('#btn_guardaActividades').hide();
            $('#tb_actividades_disp').show();
            $('#btn_ActividadListo').show();
            $("#tb_actividades_disp tbody").html('');
            $.post(url+'BuscaActividadesDispMET',{ idtipo_actuacion:$("#idtipo_actuacion").val(),idPlanificacion:$("#idPlanificacion").val() }, function (data) {
                if(data.result){
                    $("#tb_actividades_disp tbody").html(data.grilla);
                }else{
                    $('#btn_ActividadListo').click();
                    swal("Atención!", data.mensaje, "error");
                }
            }, 'json');
        });
        /**
         * Agrega una actividad a la planificación.
         * @param id_actividad
         */
        metAgregarActividad=function(id_actividad){
            if(!$("#txt_cantdias"+id_actividad).val() || $("#txt_cantdias"+id_actividad).val()==0){
                swal("Atención!", "Ingrese uno(1) ó más días a la actividad, por favor", "error");return;
            }
            swal({
                title: 'Agregar Actividad',
                text: 'Seguro que desea agregar la actividad: "'+$('#td_desc'+id_actividad).html()+'" a la planificación',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function() {
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post(url+'AgregaActividadMET', { idPlanificacion: $("#idPlanificacion").val(),id_actividad: id_actividad,txt_cantdias: $("#txt_cantdias" + id_actividad).val(),
                    fec_inicio: $("#fec_inicio").val() }, function (data) {
                    if (data.result) {
                        $("#tr_actdisp" + id_actividad).remove();
                        swal("Actividad ingresada", data.mensaje, "success");
                    } else {
                        swal("Atención!", data.mensaje, "error");
                    }
                }, 'json');
            });
        };
        /**
         * Oculta la grilla de actividades disponibles y visualiza la de actividades relacionadas al plan.
         */
        $('#btn_ActividadListo').click(function(){
            metMontaActividades();
            $('#btn_guardaActividades').show();
            metOcultarActividadDisp();
        });
        {*****************ANULAR PLANIFICACIÓN******************}
        $('#btn_formAnularPlan').click(function(){ /*Visualiza el formulario de anulación de planificación*/
            $('#btn_cerrarPlan').hide();$('#btn_anulaPlan').show();
            $('#formModalLabel').html('<i class="fa fa-edit"></i> Anular Planificación de Actuación Fiscal');
            $('#formAjaxTab1').fadeOut(1200,function(){
                $('#formAjaxTab1').hide();$('#barra_opciones').hide();$('#div_estado').hide();
                $('#formAnularCerrarPlan').fadeIn(1200,function(){ $('#formAnularCerrarPlan').show(); });
            });
        });
        /*Ejecuta la anulación de la planificación*/
        $('#btn_anulaPlan').click(function(){
            $('#hd_notanulacion').val($('#txt_nota').val());
            if(!$('#txt_nota').val()){
                swal("Información del proceso", "Ingrese la justificación por favor", "error");return;
            }else {
                swal({
                    title: 'Anular Planificación',
                    text: 'Seguro que desea Anular la planificación actual',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if(isConfirm){
                        swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                        $.post(url+'AnularPlanificacionMET', $("#formAjaxTab1").serialize(), function (data) {
                            if (data.reg_afectado) {
                                $('#hd_notanulacion').val("");
                                $('#txt_nota').val("");
                                swal("Planificación anulada", data.mensaje, "success");
                                $('#btn_Buscar').click();//Refresca la grilla principal
                                $('#ContenidoModal').html('');
                                $('#cerrarModal').click();
                            } else {
                                swal("Información del proceso", data.mensaje, "error");
                            }
                        }, 'json');
                    }else{
                        swal("Proceso cancelado", "El proceso se ha cancelado", "error");
                    }
                });
            }
        });
        /*Cancela y oculta el formulario de anulación y cerrar la planificación*/
        $('#btn_cancelAnulaPlan').click(function(){
            $('#formModalLabel').html('<i class="fa fa-edit"></i> Planificación de Actuación Fiscal');
            $('#formAnularCerrarPlan').fadeOut(1200,function(){
                $('#formAnularCerrarPlan').hide();
                $('#formAjaxTab1').fadeIn(1200,function(){ $('#formAjaxTab1').show();$('#barra_opciones').show();$('#div_estado').show(); });
            });$('#hd_notanulacion').val("");
        });
        {*****************CERRAR PLANIFICACIÓN******************}
        $('#btn_formcerrarPlan').click(function(){ /*Visualiza el formulario de Cerrar planificación*/
            $('#btn_cerrarPlan').show();$('#btn_anulaPlan').hide();
            $('#formModalLabel').html('<i class="fa fa-edit"></i> Cerrar Planificación de Actuación Fiscal');
            $('#formAjaxTab1').fadeOut(1200,function(){
                $('#formAjaxTab1').hide();$('#barra_opciones').hide();$('#div_estado').hide();
                $('#formAnularCerrarPlan').fadeIn(1200,function(){ $('#formAnularCerrarPlan').show(); });
            });
        });
        $('#btn_cerrarPlan').click(function(){ /*Ejecuta el cierre de la planificación*/
            $('#hd_notanulacion').val($('#txt_nota').val());
            if(!$('#txt_nota').val()){
                swal("Información del proceso", "Ingrese la justificación por favor", "error");
            }else{
                swal({
                    title: 'Cerrar Planificación',
                    text: 'Seguro que desea Cerrar la planificación actual',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if (isConfirm) {
                        swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                        $.post(url+'CierraPlanificacionMET', $("#formAjaxTab1").serialize(), function (data) {
                            if (data.reg_afectado) {
                                $('#hd_notanulacion').val("");
                                $('#txt_nota').val("");
                                swal("Planificación cerrada", data.mensaje, "success");
                                $('#btn_Buscar').click();//Refresca la grilla principal
                                $('#ContenidoModal').html('');
                                $('#cerrarModal').click();
                            } else {
                                swal("Información del proceso", data.mensaje, "error");
                            }
                        }, 'json');
                    } else {
                        swal("Proceso cancelado", "El proceso se ha cancelado", "error");
                    }
                });
            }
        });
        $('#btn_cancelAnulaPlan').click(function(){ /*Cancela y oculta el formulario de cierre de planificación*/
            $('#hd_notanulacion').val("");$('#txt_nota').val("");
            $('#formModalLabel').html('<i class="fa fa-edit"></i> Planificación de Actuación Fiscal');
            $('#formAnularCerrarPlan').fadeOut(1200,function(){
                $('#formAnularCerrarPlan').hide();
                $('#formAjaxTab1').fadeIn(1200,function(){ $('#formAjaxTab1').show();$('#barra_opciones').show();$('#div_estado').show(); });
            });
        });
        $('#btn_salir').click(function(){
            $('#ContenidoModal').html(''); $('#cerrarModal').click();
        });
         $('#cerrarModal').click(function(){
            $('#ContenidoModal').html('');
        });
    });
</script>