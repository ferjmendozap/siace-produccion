<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Fases</h2>
    </div>
    <div class="section-body">
        <form id="form1" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-1 text-right">
                    <label for="cbox_proceso" class="control-label">Proceso:</label>
                </div>
                <div class="col-sm-3">
                    <div class="form-group" id="cbox_procesoError">
                        <select id="cbox_proceso" name="form[int][idProceso]" class="form-control texto-size">
                            {if $listaProceso|count > 0}
                                <option value="">Todo</option>
                                {foreach item=fila from=$listaProceso}
                                    <option value="{$fila.pk_num_proceso}">{$fila.txt_descripcion_proceso}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-sm-12">
                <div id="resultadoConsulta"></div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    {literal}
    .texto-size{font-size: 12px;}
    {/literal}
</style>
<script type="text/javascript">
    var metMontaFase="",metEliminaFase="",metCrear="",metBuscar="";
    $(document).ready(function() {
        var url='{$_Parametros.url}modPF/maestros/fasesFiscalCONTROL/';
        metCrear=function(){
            $('#formModalLabel').html("<i class='icm icm-cog3'></i> Crear Fase");$('#ContenidoModal').html("");
            $.post(url+'crearModificarMET',{ idFase:0 },function(datos){
                $('#ContenidoModal').html(datos);
            });
        };
        metBuscar=function(){
            $.post(url+'ListarFasesMET', { idProceso:$('#cbox_proceso').val() }, function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        };
        $('#cbox_proceso').change(function(){
            $('#resultadoConsulta').html('');
            metBuscar();
        });
        /**
         * Lista las fases según criterio de búsqueda
         */

        metBuscar();
        /**
         * Monta los datos para modíficar al clicar un botón de actualizar de la grilla
         */
        metMontaFase=function(idFase){
            $('#formModalLabel').html('');$('#ContenidoModal').html('');
            $.post(url+'crearModificarMET',{ idFase: idFase },function(data){
                if(data){
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Modificar Fase');
                    $('#ContenidoModal').html(data);
                }else{
                    $('#formModalLabel').html('');$('#ContenidoModal').html('');$('#cerrarModal').click();
                    swal("¡Atención!", "No se pudo montar los datos. Intente de nuevo", "error");
                }
            });
        }
        /**
         * Ejecuta el proceso de eliminar un registro al clicar un botón de eliminar de la grilla
         * @param idFase
         */
        metEliminaFase=function(idFase){
            swal({
                title: "Confirmación de proceso",
                text: "Se va a eliminar la fase '"+$('#td_'+idFase).html()+"'. ¿Estas de acuerdo? ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm) {
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'EliminarFaseMET', { idFase:idFase }, function (dato) {
                        if (dato.reg_afectado) {
                            swal("Registro Eliminado", dato.mensaje, "success");
                            $('#tr_'+idFase).remove();
                        } else {
                            swal("¡Atención!", dato.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
        $('#btn_Limpiar').click(function(){
            $('#cbox_proceso').val('');$('#resultadoConsulta').html(''); listadoGrilla=false;
        });
    });
</script>