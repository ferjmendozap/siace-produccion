<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Asignación de Procesos Fiscales a Dependencia de Control</h2>
    </div>
    <div class="section-body">
        <form autocomplete="off" id="formAjax" class="form" role="form" method="post">
            <input type="hidden" id="id_x" name="form[int][id_x]" />
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group" id="cbox_dependenciaError">
                        <select id="cbox_dependencia" name="form[int][id_dependencia]" class="form-control texto-size">
                            <option value="">Seleccione</option>
                            {foreach item=fila from=$listaDependencias}
                                <option value="{$fila.id_dependencia}">{$fila.nombre_depedencia}</option>
                            {/foreach}
                        </select>
                        <label for="cbox_dependencia"><i class="icm icm-cog3"></i> Dependencias de control fiscal</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="resultadoConsulta"></div>
                </div>
            </div>
        </form>
    </div>
    <div class="text‐aviso">¡Aviso importante!</div>
    <div>Los procesos de: Valoración Jurídica y Determinación de Responsabilidad, corresponden a la Dependencia de Determinación
        de Responsabilidad Administrativa, y los demás procesos, corresponden a las 2 dependencias de control restantes.
        Asimismo, cada dependencia de control debe tener asignado sus respectivos procesos para la efectiva ejecución de la aplicación.</div>
</section>
<style type="text/css">
    {literal}
    .texto-size{font-size: 12px;}
    .text‐info{color: limegreen}
    .text‐aviso{color:lightcoral}
    {/literal}
</style>
<script type="text/javascript">
    var metAgregarProceso="",metQuitar="";
    $(document).ready(function(){
        var url="{$_Parametros.url}modPF/maestros/asignaProcesoFiscalCONTROL/";
        $("#formAjax").submit(function(){ return false; });
        /**
         * Lista los procesos fiscales
         */
        function metBuscar(){
            $("#resultadoConsulta").html('');
            $.post(url+'CargarGrillaMET', $( "#formAjax" ).serialize(), function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        };
        $('#cbox_dependencia').change(function(){
            $("#resultadoConsulta").html('');
            if($('#cbox_dependencia').val()){
                metBuscar();
            }
        });
        metAgregarProceso=function (){
            swal({
                title: 'Confirmación de proceso',
                text: 'Se va a agregar el ó los procesos seleccionados a la dependencia actual',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm) {
                    swal({ title: "¡Por favor espere!", text: "Procesando...", showConfirmButton: false});
                    $.post(url+'AgregarProcesoMET', $( "#formAjax" ).serialize(), function(dato){
                        if(dato.result){
                            swal("Exito", dato.mensaje, "success");
                            metBuscar();
                        }else{
                            swal("Atención", dato.mensaje, "error");
                        }
                    },'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        };
        metQuitar=function (idProcesoDependencia) {
            $( "#id_x" ).val(idProcesoDependencia);
            swal({
                title: 'Quitar proceso fiscal',
                text: 'Seguro que desea quitar el proceso fiscal "'+$('#td_'+idProcesoDependencia).html()+'" de la dependencia de control actual',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm) {
                    swal({ title: "¡Por favor espere!", text: "Procesando...", showConfirmButton: false});
                    $.post(url + 'QuitarProcesoMET', $("#formAjax").serialize(), function (dato) {
                        if (dato['reg_afectado']) {
                            swal("¡Exito!", dato['mensaje'], "success");
                            $('#id_x').val('');
                            metBuscar();
                        } else {
                            swal("Error!", dato['mensaje'], "error");
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        };
    });
</script>