<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatablePf" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre Proceso</th>
                            <th>Estatus</th>
                            <th align="center">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$agregarProceso=false}
                        {foreach item=fila from=$listado}
                            {if $fila.pk_num_procesodependencia==0}{$id_x=$fila.pk_num_proceso}{else}{$id_x=$fila.pk_num_procesodependencia}{/if}
                            <tr>
                                <td>{$fila.cod_proceso}</td>
                                <td id="td_{$id_x}">{$fila.txt_descripcion_proceso}</td>
                                <td><i class="{if $fila.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td align="center">
                                    {if $fila.pk_num_procesodependencia==0}
                                        <input type="checkbox" id="chk_proceso" name="form[int][chk_proceso][]" class="puntero_mouse" value="{$fila.pk_num_proceso}" title="Click para seleccionar" alt="Click para seleccionar"/>
                                        {$agregarProceso=true} {*Visualiza el botón de agregar procesos disponibles*}
                                    {else}
                                        {if in_array('PF-01-05-01-05-02-E',$_Parametros.perfil)}
                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger" onclick="metQuitar({$fila.pk_num_procesodependencia})"
                                                title="Click para quitar" alt="Click para quitar"><i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {else}
                                            <button class="btn ink-reaction btn-raised btn-xs btn-defoult"
                                                title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                                <i class="md md-delete"></i>
                                            </button>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="4">
                                {if $agregarProceso}
                                    {if in_array('PF-01-05-01-05-01-N',$_Parametros.perfil)}
                                        <a href="#" id="btn_nuevo" class="btn btn-primary btn-raised" onclick="metAgregarProceso()"
                                           title="Click para agregar" alt="Click para agregar">
                                            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Agregar
                                        </a>
                                    {else}
                                        <a href="#" id="btn_nuevo" class="btn btn-default btn-raised"
                                           title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Agregar
                                        </a>
                                    {/if}
                                {/if}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>