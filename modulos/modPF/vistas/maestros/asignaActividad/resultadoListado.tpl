<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatablePf" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre Actividad</th>
                            <th>Estatus</th>
                            <th>Afecta plan</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$agregarActividad=false}
                        {foreach item=fila from=$listado}
                            {if $fila.pk_num_actividad_tipoactuac==0}{$id_x=$fila.pk_num_actividad}{else}{$id_x=$fila.pk_num_actividad_tipoactuac}{/if}
                            <tr>
                                <td>{$fila.cod_actividad}</td>
                                <td id="td_{$id_x}">{$fila.txt_descripcion_actividad}</td>
                                <td><i class="{if $fila.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td><i class="{if $fila.ind_afecto_plan=='S'}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td align="center">
                                    {if $fila.pk_num_actividad_tipoactuac==0}
                                        <input type="checkbox" id="chk_actividades" name="form[int][chk_actividades][]" class="puntero_mouse" value="{$fila.pk_num_actividad}" title="Click para seleccionar" alt="Click para seleccionar"/>
                                        {$agregarActividad=true} {*Visualiza el botón de agregar actividades disponibles*}
                                    {else}
                                        {if in_array('PF-01-05-01-04-02-E',$_Parametros.perfil)}
                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger"
                                                onclick="metQuitar({$fila.pk_num_actividad_tipoactuac})"
                                                title="Click para quitar" alt="Click para quitar"">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="5">
                                {if $agregarActividad}
                                    {if in_array('PF-01-05-01-04-01-N',$_Parametros.perfil)}
                                        <a href="#" id="btn_nuevo" class="btn btn-primary btn-raised" onclick="metAgregarActividad()"
                                           title="Click para agregar la(s) actividad(es) seleccionada(s)" alt="Click para agregar la(s) actividad(es) seleccionada(s)">
                                            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Agregar
                                        </a>
                                    {else}
                                        <a href="#" id="btn_nuevo" class="btn btn-default btn-raised"
                                           title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Agregar
                                        </a>
                                    {/if}
                                {/if}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>