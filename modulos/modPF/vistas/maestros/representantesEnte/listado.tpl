<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Representantes de Entes</h2>
    </div>
    <div class="section-body">
        <div id="resultadoConsulta"></div>
    </div>
</section>
<script type="text/javascript">
    var metMontaRespresentante="",metCrear="",metBuscar="";
    $(document).ready(function() {
        var url='{$_Parametros.url}modPF/maestros/representantesEnteCONTROL/';
        /**
         * Lista los personal representantes de entes
         */
        metBuscar=function(){
            $.post(url+'ListarRepresentantesMET', '', function (data) {
                $('#resultadoConsulta').html(data);
            });
        };
        metBuscar();
        /**
         * Monta el form modal para el ingreso de un representante ente
         */
        metCrear=function(){
            $('#ContenidoModal').html("");
            $('#formModalLabel').html('<i class="icm icm-cog3"></i> Crear Representante de Entes');
            $.post(url+'crearModPersonaMET',{ idRpstte:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        };
        /**
         * Monta los datos en el form modal para la actualización
         * @param idRpstte
         */
        metMontaRespresentante=function(idRpstte){
            $('#formModalLabel').html('');$('#ContenidoModal').html('');
            $.post(url+'crearModPersonaMET', { idRpstte:idRpstte }, function (data) {
                if(data){
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Actualizar Representante de Entes');
                    $('#ContenidoModal').html(data);
                }else{
                    $('#formModalLabel').html('');$('#ContenidoModal').html('');$('#cerrarModal').click();
                    swal("Información del Sistema", "Disculpe, no se pudo montar los datos. Intente nuevamente", "error");
                }
            });
        }
    });
</script>