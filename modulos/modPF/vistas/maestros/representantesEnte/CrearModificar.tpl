<div class="modal-body" xmlns="http://www.w3.org/1999/html">
    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
        <div class="form-wizard-nav">
            <div class="progress">
                <div class="progress-bar progress-bar-primary"></div>
            </div>
            <ul class="nav nav-justified">
                <li id="tab_1"class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span><span class="title">REPRESENTANTE</span></a></li>
                <li><a id="tab_2" href="#tab2" data-toggle="tab"><span class="step">2</span><span class="title">ORGANIZACIÓN</span></a></li>
            </ul>
        </div>
        <div class="tab-content clearfix">
            <div id="tab1" class="tab-pane active floating-label">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-head style-primary-light">
                                <header>Datos de la Persona</header>
                            </div>
                            <div class="card-body">
                                <form id="formAjaxTab1" action="{$_Parametros.url}modPF/maestros/representantesEnteCONTROL/crearModPersonaMET" class="form form-validation" novalidate="novalidate">
                                    <input type="hidden" value="1" name="valido"/>
                                    <input type="hidden" id="idRpstte" name="form[int][idRpstte]" value="{$formDB.idRpstte}" />
                                    <div id="div_msj" class="text-primary-dark text-center"></div>
                                    <div class="col-lg-12">
                                        <div class="col-sm-2">
                                            <div class="form-group" id="cbox_nacionalidadError">
                                                <select id="cbox_nacionalidad"  name="form[int][cbox_nacionalidad]" class="form-control select2">
                                                    <option value="">&nbsp;</option>
                                                    {if $listaNacionalidad|count > 0}
                                                        {foreach item=fila from=$listaNacionalidad}
                                                            {if $fila.id_nacionalidad==$formDB.cbox_nacionalidad}
                                                                <option selected value="{$fila.id_nacionalidad}">{$fila.desc_nacionalidad}</option>
                                                            {else}
                                                                <option value="{$fila.id_nacionalidad}">{$fila.desc_nacionalidad}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                                <label for="cbox_nacionalidad"><i class="md md-account-circle"></i>&nbsp;Nacionalidad</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group" id="txt_cedulaError">
                                                <input type="text" maxlength="12" name="form[int][txt_cedula]" id="txt_cedula" class="form-control" value="{if isset($formDB.txt_cedula)}{$formDB.txt_cedula}{/if}" onkeypress="return appf.metValidaCampNum(event)" title="Ingrese el número y oprima la tecla enter para búscar" alt="Ingrese el número y oprima la tecla enter para búscar">
                                                <label for="txt_cedula"><i class="md md-search"></i>&nbsp;Cédula</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group" id="txt_documento_fiscalError">
                                                <input type="text" name="form[txt][txt_documento_fiscal]" id="txt_documento_fiscal" maxlength="14" class="form-control" value="{if isset($formDB.txt_documento_fiscal)}{$formDB.txt_documento_fiscal}{/if}" {*onkeypress="return mValidaDocFiscal(event)"*}>
                                                <label for="txt_documento_fiscal"><i class="md md-border-color"></i>&nbsp;R.I.F.</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group" id="txt_nombrepers1Error">
                                                <input type="text" name="form[txt][txt_nombrepers1]" id="txt_nombrepers1" maxlength="15" class="form-control lMayuscula" value="{if isset($formDB.txt_nombrepers1)}{$formDB.txt_nombrepers1}{/if}">
                                                <label for="txt_nombrepers1"><i class="md md-border-color"></i>&nbsp;1er Nombre</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group" id="txt_nombrepers2Error">
                                                <input type="text" name="form[txt][txt_nombrepers2]" id="txt_nombrepers2" maxlength="15" class="form-control lMayuscula" value="{if isset($formDB.txt_nombrepers2)}{$formDB.txt_nombrepers2}{/if}">
                                                <label for="txt_nombrepers2"><i class="md md-border-color"></i>&nbsp;2do Nombre</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-3">
                                            <div class="form-group" id="txt_apellido1Error">
                                                <input type="text" name="form[txt][txt_apellido1]" id="txt_apellido1" maxlength="15" class="form-control lMayuscula" value="{if isset($formDB.txt_apellido1)}{$formDB.txt_apellido1}{/if}">
                                                <label for="txt_apellido1"><i class="md md-border-color"></i>&nbsp;1er Apellido</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group" id="txt_apellido2Error">
                                                <input type="text" name="form[txt][txt_apellido2]" id="txt_apellido2" maxlength="15" class="form-control lMayuscula" value="{if isset($formDB.txt_apellido2)}{$formDB.txt_apellido2}{/if}">
                                                <label for="txt_apellido2"><i class="md md-border-color"></i>&nbsp;2do Apellido</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group" id="txt_emailError">
                                                <input type="text" name="form[txt][txt_email]" id="txt_email" maxlength="40" class="form-control" value="{if isset($formDB.txt_email)}{$formDB.txt_email}{/if}">
                                                <label for="txt_email"><i class="md md-border-color"></i>&nbsp;Email</label>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group" id="cbox_situacionjuridicaError">
                                                <select id="cbox_situacionjuridica"  name="form[txt][cbox_situacionjuridica]" class="form-control select2">
                                                    <option value="J">JURIDICA</option>
                                                    <option value="N"{if isset($formDB.cbox_situacionjuridica) and $formDB.cbox_situacionjuridica == 'N'}selected{/if}>NATURAL</option>
                                                </select>
                                                <label for="cbox_situacionjuridica"><i class="md md-account-circle"></i>&nbsp; Situación jurídica</label>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group" id="cbox_tipopersonaError">
                                                <select id="cbox_tipopersona"  name="form[int][cbox_tipopersona]" class="form-control select2">
                                                    <option value="0" selected>Seleccione...</option>
                                                    {if $tipopersona|count > 0}
                                                        {foreach item=fila from=$tipopersona}
                                                            {if $fila.idtipo_persona==$formDB.cbox_tipopersona}
                                                                <option value="{$fila.idtipo_persona}" selected>{$fila.desctipo_persona}</option>
                                                            {else}
                                                                <option value="{$fila.idtipo_persona}">{$fila.desctipo_persona}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                                <label for="cbox_tipopersona"><i class="md md-account-circle"></i>&nbsp; Tipo de Persona</label>
                                            </div>
                                        </div>

                                        <div class="checkbox checkbox-styled col-sm-3">
                                            <label>
                                                <input type="checkbox" id="chk_estatuspers" name="form[int][chk_estatuspers]"
                                                       {if isset($formDB.chk_estatuspers) and $formDB.chk_estatuspers==1}
                                                           checked
                                                       {elseif $formDB.idRpstte==0 or $formDB.idRpstte==''}
                                                           checked
                                                       {/if}
                                                       value="1" >
                                                <span>Estatus</span>
                                            </label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="barra_opciones" align="center">
                            <button id="btn_LimpiarTab1" class="btn btn-raised btn-default" title="Click para limpiar" alt="Click para limpiar">
                                Limpiar
                            </button>
                            {if (in_array('PF-01-05-02-03-01-N',$_Parametros.perfil) or in_array('PF-01-05-02-03-02-M',$_Parametros.perfil)) and $opcionProceso=='ingresar'}
                                <button type="button" id="btn_guardarpers" class="btn_aux1 btn btn-primary btn-raised" title="Click para guardar" alt="Click para guardar">
                                    <span class="glyphicon glyphicon-floppy-disk"></span><span class="btn_aux2">&nbsp;Guardar</span>
                                </button>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab2" class="tab-pane" floating-label>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-head style-primary-light">
                                <header>Adscripción Organizacional</header>
                            </div>
                            <div class="card-body" style="margin-bottom: 0;">
                                <form id="formAjaxTab2" action="{$_Parametros.url}modPF/maestros/representantesEnteCONTROL/creaModOrganizacionMET" class="form form-validation floating-label" novalidate="novalidate">
                                    <input type="hidden" id="idPersonaEnte" name="form[int][idPersonaEnte]" />
                                    <input type="hidden" id="idPersona" name="form[int][idPersona]" />
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group" id="cbox_enteError">
                                                <select id="cbox_ente"  name="form[int][cbox_ente]" class="form-control">
                                                    <option value="0">Seleccione...</option>
                                                    {if $listaEntes|count > 0}
                                                        {foreach item=fila from=$listaEntes}
                                                            <option value="{$fila.id_ente}">{$fila.nombre_ente}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                                <label for="cbox_ente"><i class="md md-account-circle"></i>&nbsp; Ente</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group" id="cbox_cargoError">
                                                <select id="cbox_cargo"  name="form[int][cbox_cargo]" class="form-control">
                                                    <option value="0" selected>Seleccione...</option>
                                                    {if $listaCargos|count > 0}
                                                        {foreach item=fila from=$listaCargos}
                                                            {if $fila.id_cargo==$formDB.fk_a040_num_cargo_personal_externo}
                                                                <option selected value="{$fila.id_cargo}">{$fila.nombre_cargo}</option>
                                                            {else}
                                                                <option value="{$fila.id_cargo}">{$fila.nombre_cargo}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                                <label for="cbox_cargo"><i class="md md-account-circle"></i>&nbsp; Cargo</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group" id="cbox_situacionError">
                                                <select id="cbox_situacion"  name="form[int][cbox_situacion]" class="form-control">
                                                    <option value="0" selected>Seleccione...</option>
                                                    {if $listaSituacionpers|count > 0}
                                                        {foreach item=fila from=$listaSituacionpers}
                                                            {if $fila.id_situacionpers==$formDB.fk_a006_num_miscdetalle_situacion}
                                                                <option selected value="{$fila.id_situacionpers}">{$fila.desc_situacion}</option>
                                                            {else}
                                                                <option value="{$fila.id_situacionpers}">{$fila.desc_situacion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                                <label for="cbox_situacion"><i class="md md-account-circle"></i>&nbsp; Situación</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group" id="cbox_estatusError">
                                                <select id="cbox_estatus" name="form[int][cbox_estatus]" class="form-control">
                                                    <option value="1">Activo</option>
                                                    <option value="0">Inactivo</option>
                                                </select>
                                                <label for="cbox_estatus"><i class="md md-account-circle"></i>&nbsp; Estatus</label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div id="opciones_organizacion" align="center">
                                    <button id="btn_limpiarTab2" class="button btn btn-default" title="Click para limpiar" alt="Click para limpiar">
                                        Limpiar
                                    </button>
                                    {if (in_array('PF-01-05-02-03-01-N',$_Parametros.perfil) or in_array('PF-01-05-02-03-02-M',$_Parametros.perfil)) and $opcionProceso=='ingresar'}
                                        <button type="button" id="btn_guardarOrgnzcion" class="btn btn-primary btn-raised" title="Click para guardar"  alt="Click para guardar">
                                            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                                        </button>
                                    {/if}
                                </div>
                                <br />
                                <div id="tb_organizacion"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<span class="clearfix"></span>
<script type="text/javascript">
    var metMontaOrganizacion="",appf="",metEliminaAdscripcion="";
    var dataValidadaTab1="",dataValidadaTab2=""; //Permite desmarcar los campos.
    $(document).ready(function () {
        var app = new AppFunciones();
        appf=new AppfFunciones();
        app.metWizard();
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");/*ancho de la Modal*/
        var url='{$_Parametros.url}modPF/maestros/representantesEnteCONTROL/';
        $('.select2').select2({ allowClear: true }); $('#div_msj').hide();
        appf.metValidaDocFiscal($('#txt_documento_fiscal'));//Valida el Rif al tipear.
        appf.metConvertMayuscula($('.lMayuscula'));
        appf.metValidaTextAlfabetico($('.lMayuscula'));
        /**
         *Monta los datos de una persona en la ficha Representante al existír la mísma cuando
         *se da enter en el campo cédula ó al ingresarla como nuevo registro.
         */
        function metMontarDatosPers(data){
            $('#tab1').removeClass('floating-label');
            $('#idRpstte').val(data.idRpstte);
            $('#cbox_nacionalidad').val(data.cbox_nacionalidad).select2();
            $('#txt_documento_fiscal').val(data.txt_documento_fiscal);
            $('#txt_nombrepers1').val(data.txt_nombrepers1);
            $('#txt_nombrepers2').val(data.txt_nombrepers2);
            $('#txt_apellido1').val(data.txt_apellido1);
            $('#txt_apellido2').val(data.txt_apellido2);
            $('#txt_email').val(data.txt_email);
            $('#cbox_situacionjuridica').val(data.cbox_situacionjuridica).select2();
            if($('#cbox_tipopersona').val() != data.cbox_tipopersona){
                if(data.cbox_tipopersona){
                    if(appf.metBuscaIdValorComboBox('cbox_tipopersona',data.cbox_tipopersona)){
                        $('#cbox_tipopersona').val(data.cbox_tipopersona).select2();
                    }else{
                        $('#cbox_tipopersona').append('<option value="' + data.cbox_tipopersona + '" selected>' + data.desc_tipopersona + '</option>').select2();
                    }
                    appf.metEstadoObjForm('cbox_tipopersona',false);
                }
            }
            if(data.chk_estatuspers==0){
                $('#chk_estatuspers').removeAttr('checked');
            }
        }
        $('#txt_documento_fiscal').click(function(){
            if(!$('#txt_documento_fiscal').val()){
                $('#txt_documento_fiscal').attr({ placeholder:"V-9999999-0" });
            }
        });
        $('#txt_documento_fiscal').focusout(function(){
            if(!$('#txt_documento_fiscal').val()){
                $('#txt_documento_fiscal').removeAttr('placeholder');
            }
        });
        $('#txt_cedula').keypress(function(e){
            if (e.which == 13){
                if($('#txt_cedula').val()){
                    $.post(url+'BuscaPersonaMET',{ txt_cedula:$('#txt_cedula').val() }, function (data) {
                        if(data) {
                            metMontarDatosPers(data)
                        }else{
                            swal("¡Atención!", "Esa persona no está en el sistema", "success");
                        }
                    }, 'json');
                }else{
                    swal("¡Atención!", "Ingrese el número primero y luego enter para buscar", "error");
                }
            }
        });
        $('#txt_cedula').focus(function(){
            if(!$('#txt_cedula').val()){
                $('#div_msj').show();
                $('#div_msj').html('::.Ingrese el número de cédula y luego oprima la tecla enter para buscar.::').animate({
                    'margin-right':'600px'
                },1500);
            }
        });
        $('#txt_cedula').focusout(function(){
            $('#div_msj').html('').removeAttr('style');
        });
        $('#btn_guardarpers').click(function(){
            var msj=""; var $cboxBloqueda=false;
            if($("#idRpstte").val()==0){
                msj="Se va a ingresar una nueva persona";
            }else{
                msj="Se van a actualizar los datos actuales de la persona. ¿Estas de Acuerdo?";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    if($('#cbox_tipopersona').is(":disabled")){
                        appf.metEstadoObjForm('cbox_tipopersona',true); var $cboxBloqueda=true;
                    }
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post($("#formAjaxTab1").attr("action"), $("#formAjaxTab1").serialize(), function (data) {
                        if (data.Error == 'error') {
                            appf.metActivaError(data); dataValidadaTab1=data;
                            swal("¡Atención!", "Los campos marcados con X en rojo son obligatorios", "error");
                        } else if (data.Error == 'errorSql') {
                            swal("¡Atención!", data.mensaje, "error");
                        } else if (data.Error == 'errorExiste') {
                            swal("¡Atención!", data.mensaje, "error");
                            metMontarDatosPers(data.dataPersona);
                        } else {
                            if($("#idRpstte").val()==0){
                                if(data.idRpstte) {
                                    swal("Información del proceso", data.mensaje, "success");
                                    $("#idRpstte").val(data.idRpstte);
                                    appf.metActivaError(data.dataValidada);
                                    dataValidadaTab1=data.dataValidada;
                                    metBuscar();
                                }else{
                                    swal("¡Atención!", data.mensaje, "error");
                                }
                            }else{
                                if (data.reg_afectado) {
                                    swal("Información del proceso", data.mensaje, "success");
                                    appf.metActivaError(data.dataValidada);
                                    dataValidadaTab1=data.dataValidada;
                                    metBuscar();
                                }else{
                                    swal("¡Atención!", data.mensaje, "error");
                                }
                            }
                        }
                    }, 'json');
                    if($cboxBloqueda){
                        appf.metEstadoObjForm('cbox_tipopersona',false);
                    }
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        });
        $('#tab_1').click(function(){
            $('#idPersona').val(0);
            $('#btn_limpiarTab2').click();
        });
        /**
         * Limpia los campos de la ficha Representante
         */
        $('#btn_LimpiarTab1').click(function(){
            $("#idRpstte").val(0)
            $('#cbox_nacionalidad').select2("val", "");
            $('#txt_cedula').val('');$('#txt_documento_fiscal').val('');
            $('#txt_nombrepers1').val('');$('#txt_nombrepers2').val('');
            $('#txt_apellido1').val('');$('#txt_apellido2').val('');
            $('#txt_email').val('');$('#cbox_situacionjuridica').val("J").select2();
            $('#cbox_tipopersona').val("0").select2();
            $('#chk_estatuspers').attr({ checked:true });
            if(dataValidadaTab1){
                appf.metActivaError(dataValidadaTab1,true);
                dataValidadaTab1="";
            }
            appf.metEstadoObjForm('cbox_tipopersona',true);
        });
        /***********************FICHA ORGANIZACIÓN*********************************/
        function metListarOrganizacion(){
            $("#tb_organizacion").html('');
            $.post(url+'BuscaOrganizacionPersMET',{ idRpstte:$("#idRpstte").val() }, function (data) {
                if(data) {
                     $("#tb_organizacion").html(data);
                }
            })
        }
        $('#tab_2').click(function(){
            $("#tab_2").attr({ "data-toggle":"tab" });
            if($("#idRpstte").val()==0){
                $("#tab_2").attr({ "data-toggle":"tab disabled" });
                swal("Atención", "Debe ingresar y guardar los datos de la persona antes de seguir", "error");
            }else{
                $("#idPersonaEnte").val(0);
                $('#idPersona').val($("#idRpstte").val());
                metListarOrganizacion();
            }
        });
        /**
         * Llena el combo entes de forma recursiva.
         */
        function metCargCboxRcsvo(idEnte){
            var $tultic='',valor_filtro=idEnte;
            $.post(url+'BuscaEntesMET',{ idEnte:idEnte },function(data){
                $('#cbox_ente').html("");
                if(data.filas){
                    $.each(data.filas, function(i,fila){
                        $tultic='title="'+fila.toltick+'" alt="' +  fila.toltick  +'"';
                        if(i==0){
                            if(valor_filtro==0){
                                $('#cbox_ente').append('<option value="">-Seleccione-</option>');
                                $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                                $('#cbox_ente').val("");
                            }else{
                                $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                                $('#cbox_ente').val(fila.id_ente);
                            }
                            $id_nivel_ant=fila.idente_padre;
                        }else{
                            $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                        }
                    });
                }
                if(valor_filtro!=0){
                    $('#cbox_ente').append('<option value="'+$id_nivel_ant+'">-----Nivel Anterior-----</option>');
                }
            }, "json");
        }
        /**
         * Consulta y monta los entes en el combo de forma recursiva
         */
        $('#cbox_ente').change(function(){
            if($('#cbox_ente').val()){
                metCargCboxRcsvo($('#cbox_ente').val());
            }
        });
        /**
         * Monta los datos de adscripción de la persona al ente para actualizar en la ficha Organización
         */
        metMontaOrganizacion=function(idPersonaEnte,idEnte,idCargoPers,idSituacionPers,estatusRprstnte){
            metCargCboxRcsvo(idEnte);
            $('#idPersonaEnte').val(idPersonaEnte);
            $('#cbox_cargo').val(idCargoPers);
            $('#cbox_situacion').val(idSituacionPers);
            $('#cbox_estatus').val(estatusRprstnte);
            appf.metEstadoObjForm('cbox_ente,cbox_cargo',false);
        }
        /**
         * Ingresa un nuevo ó actualiza un registro de adscripción de persona a una organización
         */
        $('#btn_guardarOrgnzcion').click(function(){
            var $cboxsBloqueda=false;
            var msj="";
            if($("#idPersonaEnte").val()==0){
                msj="Se va a agregar la persona al ente. Verifique que el ente y el cargo sean los correctos, los mísmos no se podrán modificar";
            }else{
                msj="Se van a actualizar los datos actuales. ¿Estas de Acuerdo?";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    if($('#cbox_ente').is(":disabled")){
                        appf.metEstadoObjForm('cbox_ente,cbox_cargo',true); $cboxsBloqueda=true;
                    }
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post($("#formAjaxTab2").attr("action"), $("#formAjaxTab2").serialize(), function (data) {
                        if (data.Error == 'error') {
                            appf.metActivaError(data); dataValidadaTab2=data;
                            swal("¡Atención!", "Los campos marcados con X en rojo son obligatorios", "error");
                        } else if (data.Error == 'errorSql') {
                            swal("¡Atención!", data.mensaje, "error");
                        } else {
                            if($("#idPersonaEnte").val()==0){
                                if(data.idPersonaEnte) {
                                    swal("Información del proceso", data.mensaje, "success");
                                    $("#idPersonaEnte").val(data.idPersonaEnte);
                                    appf.metActivaError(data.dataValidada);
                                    dataValidadaTab2=data.dataValidada
                                    metListarOrganizacion();
                                    metBuscar();
                                }else{
                                    swal("¡Atención!", data.mensaje, "error");
                                }
                            }else{
                                if (data.reg_afectado) {
                                    swal("Información del proceso", data.mensaje, "success");
                                    appf.metActivaError(data.dataValidada);
                                    dataValidadaTab2=data.dataValidada;
                                    metListarOrganizacion();
                                    metBuscar();
                                }else{
                                    swal("¡Atención!", data.mensaje, "error");
                                }
                            }
                        }
                    }, 'json');
                    if($cboxsBloqueda){
                        appf.metEstadoObjForm('cbox_ente,cbox_cargo',false); $cboxsBloqueda=false;
                    }
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        });
        /**
         * Limpia los campos de la ficha organización
         */
        $('#btn_limpiarTab2').click(function(){
            appf.metEstadoObjForm('cbox_ente,cbox_cargo',true);
            if($('#cbox_ente').val()!=0){
                metCargCboxRcsvo(0);
            }
            $("#idPersonaEnte").val(0);
            $('#cbox_cargo').val(0);
            $('#cbox_situacion').val(0);
            $('#cbox_estatus').val(1);
            if(dataValidadaTab2){
                appf.metActivaError(dataValidadaTab2,true);
                dataValidadaTab2="";
            }
        });
        /**
         * Quita la persona representante de una organización.
         */
        metEliminaAdscripcion=function(idPersonaEnte){
            swal({
                title: "Confirmación de proceso",
                text: "Vas a quitar a la persona de la organización. ¿Estas de acuerdo? ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm) {
                    //swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'EliminarAdscripcionMET', { idPersonaEnte:idPersonaEnte }, function (dato) {
                        if (dato.reg_afectado) {
                            swal("Registro Eliminado", dato.mensaje, "success");
                            metListarOrganizacion();
                        } else {
                            swal("¡Atención!", dato.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
    });
</script>
