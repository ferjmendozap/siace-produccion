<form action="{$_Parametros.url}modPF/maestros/categoriaEnteCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" id="idCategoria" name="form[int][idCategoria]" value="{if isset($formDB.pk_num_categoria_ente) }{$formDB.pk_num_categoria_ente}{else}0{/if}" />
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="cbox_tipoenteError" style="margin-top: -10px;">
                    <select id="cbox_tipoente"  name="form[int][cbox_tipoente]" class="form-control" data-placeholder="Seleccione el tipo de ente" {if isset($ver) and $ver==1}disabled{/if}>
                        <option value=""></option>
                        {if $listadoTipoEnte|count > 0}
                            {foreach item=fila from=$listadoTipoEnte}
                                {if isset($formDB.pk_num_categoria_ente) and $formDB.fk_a006_miscdet_tipoente eq $fila.idtipo_ente}
                                    <option value="{$fila.idtipo_ente}" selected>{$fila.nombretipo_ente}</option>
                                {else}
                                    <option value="{$fila.idtipo_ente}">{$fila.nombretipo_ente}</option>
                                {/if}
                            {/foreach}
                        {/if}
                    </select>
                    <label for="cbox_tipoente"><i class="icm icm-cog3"></i> Tipo ente</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="ind_categoria_enteError">
                    <input id="txt_categoria" name="form[alphaNum][txt_categoria]" type="text" class="form-control" value="{if isset($formDB.ind_categoria_ente)}{$formDB.ind_categoria_ente}{/if}">
                    <label for="txt_categoria"><i class="icm icm-cog3"></i> Categoría</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.usuario_mod)}{$formDB.usuario_mod}{/if}" id="usuario_mod">
                    <label for="usuario_mod"><i class="md md-person"></i> Último Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fecha_modificacion)}{$formDB.fecha_modificacion}{/if}" id="fecha_modificacion">
                    <label for="fecha_modificacion"><i class="fa fa-calendar"></i> Última Modificación</label>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="btn_guardar">
            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
        </button>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        var appf=new AppfFunciones();
        $("#formAjax").submit(function(){ return false; });
        $('#modalAncho').css("width","35%");
        /**
         * Ingresa ó actualiza un registro
         */
        $('#btn_guardar').click(function(){
            appf.metDepuraText('txt_categoria');
            var msj="";
            if($("#idCategoria").val()==0){
                msj="Se va a ingresar una nueva categoría";
            }else{
                msj="Se va a actualizar el registro actual";
                msj+=". Estas de Acuerdo";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (data) {
                    if (data.status == 'error') {
                        appf.metActivaError(data);
                        swal("¡Atención!", "Los campos con una x en rojo no debe quedar vacíos", "error");
                    } else if (data.status == 'errorSQL') {
                        swal("¡Atención!", data.mensaje, "error");
                    } else if (data.status == 'nuevo') {
                        swal("Registro Ingresado!", data.mensaje, "success");
                        metBuscar(); /*Refresca la grilla*/
                        $('#ContenidoModal').html(''); $('#cerrarModal').click();
                    } else if (data.status == 'modificar') {
                        metBuscar(); /*Refresca la grilla*/
                        swal("Registro Modificado!", data.mensaje, "success");
                        $('#ContenidoModal').html(''); $('#cerrarModal').click();
                    }
                }, 'json');
            });
        });
    });
</script>