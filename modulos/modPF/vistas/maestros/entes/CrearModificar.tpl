<div class="modal-body">
    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
        <form id="formAjax" action="{$_Parametros.url}modPF/maestros/entesCONTROL/CrearModificarMET" class="form form-validation" novalidate="novalidate">
            <input type="hidden" value="1" name="valido"/>
            <input type="hidden" id="idEntePadre" value="{$idEntePadre}" name="form[int][idEntePadre]">
            <input type="hidden" id="idEnte" name="form[int][idEnte]"value="{$idEnte}" />
            <div class="form-wizard-nav">
                <div class="progress">
                    <div class="progress-bar progress-bar-primary"></div>
                </div>
                <ul class="nav nav-justified">
                    <li><a href="#tab1" data-toggle="tab" class="active"><span class="step">1</span><span class="title">INFORMACIÓN GENERAL</span></a></li>
                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span><span class="title">INFORMACIÓN ADICIONAL</span></a></li>
                </ul>
            </div>
            <div class="tab-content clearfix floating-label" style="margin-bottom: 0;">
                <div class="tab-pane active" id="tab1">
                    <div class="card">
                        <div class="card-head style-primary-light">
                            <header>Datos del Ente</header>
                        </div>
                        <div class="card-body" style="margin-bottom: 0;">
                            <!--Entes Externos-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="entePadre" id="entePadreError" class="control-label" style="margin-top: 10px; margin-bottom: 0;">
                                        <span class="md md-account-balance">Ente Principal:</span>
                                    </label>
                                    <br />
                                    <div id="entePadre">{$formDB.entePadre}</div>
                                    <a href="#" id="btn_Ente" class="bottom btn ink-reaction btn-raised btn-xs btn-primary"
                                        data-toggle="modal" data-target="#formModal3" data-keyboard="false" data-backdrop="static"
                                        title="Click si vas a seleccionar un ente principal" alt="Click si vas a seleccionar un ente principal">Seleccione
                                    </a>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group" id="cbox_tipoEnteError">
                                        <select id="cbox_tipoEnte" name="form[int][cbox_tipoEnte]" class="form-control select2">
                                            <option value="0">Seleccione</option>
                                            {if $listaTipoEnte|count > 0}
                                                {foreach item=fila from=$listaTipoEnte}
                                                    {if $formDB.pk_miscdet_tipoente}
                                                        <option selected value="{$fila.idtipo_ente}">{$fila.nombretipo_ente}</option>
                                                    {else}
                                                        <option value="{$fila.idtipo_ente}">{$fila.nombretipo_ente}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                        <label for="cbox_tipoEnte"><i class="md md-account-balance"></i>&nbsp;Tipo de ente</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group" id="cbox_categoriaEnteError">
                                        <select id="cbox_categoriaEnte" name="form[int][cbox_categoriaEnte]" class="form-control select2">
                                            <option value="">&nbsp;</option>
                                            {if $listaCategoriaEnte|count > 0}
                                                {foreach item=fila from=$listaCategoriaEnte}
                                                    {if $fila.id_categoria==$formDB.fk_a038_num_categoria_ente}
                                                        <option selected value="{$fila.id_categoria}">{$fila.nombre_categoria}</option>
                                                    {else}
                                                        <option value="{$fila.id_categoria}">{$fila.nombre_categoria}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                        <label for="cbox_categoriaEnte"><i class="md md-account-balance"></i>&nbsp;Categoría</label>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group" id="txt_nroRifError">
                                        <input id="txt_nroRif" name="form[txt][txt_nroRif]" type="text" maxlength="13" class="form-control lMayuscula" value="{if isset($formDB.ind_numero_registro)}{$formDB.ind_numero_registro}{/if}">
                                        <label for="txt_nroRif"><i class="md md-border-color"></i>&nbsp;Nº de Registro(R.I.F)</label>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group" id="txt_tomoRegistroError">
                                        <input id="txt_tomoRegistro" name="form[txt][txt_tomoRegistro]" type="text" maxlength="10" class="form-control" value="{if isset($formDB.ind_tomo_registro)}{$formDB.ind_tomo_registro}{/if}" onkeypress="return appf.metValidaCampNum(event)">
                                        <label for="txt_tomoRegistro"><i class="md md-border-color"></i>&nbsp;Nº tomo de Registro</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="form-group" id="txt_nombreEnteError">
                                        <input id="txt_nombreEnte" name="form[txt][txt_nombreEnte]" type="text" class="form-control" value="{if isset($formDB.ind_nombre_ente)}{$formDB.ind_nombre_ente}{/if}">
                                        <label for="txt_nombreEnte"><i class="fa fa-file-text"></i>&nbsp;Nombre Ente</label>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group" id="txt_fechaFundacionError">
                                        <input id="txt_fechaFundacion" name="form[txt][txt_fechaFundacion]" type="text" class="form-control campoFecha"
                                               value="{if isset($formDB.ind_fecha_fundacion)}{$formDB.ind_fecha_fundacion}{elseif isset($formDB.fec_fundacion)}{$formDB.fec_fundacion}{/if}" />
                                        <label for="txt_fechaFundacion"><i class="fa fa-calendar"></i>Fecha Fundación</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group" id="cbox_paisError">
                                        <select id="cbox_pais" class="form-control select2">
                                            <option value="0">Seleccione</option>
                                            {if $listaPais|count > 0}
                                                {foreach item=fila from=$listaPais}
                                                    {if $fila.pk_num_pais==$formDB.id_pais}
                                                        <option value="{$fila.pk_num_pais}"  selected >{$fila.ind_pais}</option>
                                                    {else}
                                                        <option value="{$fila.pk_num_pais}">{$fila.ind_pais}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                        <label for="cbox_pais"><i class="md md-map"></i>&nbsp;País</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group" id="cbox_estadoError">
                                        <select id="cbox_estado" name="form[int][cbox_estado]" class="form-control select2">
                                            <option value="">&nbsp;</option>
                                            {if $listaEntidad|count > 0}
                                                {foreach item=fila from=$listaEntidad}
                                                    {if $fila.pk_num_estado==$formDB.id_entidad}
                                                        <option selected value="{$fila.pk_num_estado}">{$fila.ind_estado}</option>
                                                    {else}
                                                        <option value="{$fila.pk_num_estado}">{$fila.ind_estado}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                        <label for="cbox_estado"><i class="md md-map"></i>&nbsp;Entidad</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group" id="cbox_municipioError">
                                        <select id="cbox_municipio" name="form[int][cbox_municipio]" class="form-control select2">
                                            <option value="">&nbsp;</option>
                                            {if $listaMunicipio|count > 0}
                                                {foreach item=fila from=$listaMunicipio}
                                                    {if $fila.pk_num_municipio==$formDB.id_municipio}
                                                        <option selected value="{$fila.pk_num_municipio}">{$fila.ind_municipio}</option>
                                                    {else}
                                                        <option value="{$fila.pk_num_municipio}">{$fila.ind_municipio}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                        <label for="cbox_municipio"><i class="md md-map"></i>&nbsp;Municipio</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="cbox_parroquiaError">
                                        <select id="cbox_parroquia" name="form[int][cbox_parroquia]" class="form-control select2">
                                            <option value="">&nbsp;</option>
                                            {if $listaParroquia|count > 0}
                                                {foreach item=fila from=$listaParroquia}
                                                    {if $fila.pk_num_parroquia==$formDB.fk_a012_num_parroquia}
                                                        <option value="{$fila.pk_num_parroquia}" selected>{$fila.ind_parroquia}</option>
                                                    {else}
                                                        <option value="{$fila.pk_num_parroquia}">{$fila.ind_parroquia}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                        <label for="cbox_parroquia"><i class="md md-map"></i>&nbsp;Parroquia</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group" id="cbox_ciudadError">
                                        <select id="cbox_ciudad" name="form[int][cbox_ciudad]" class="form-control select2">
                                            <option value="">&nbsp;</option>
                                            {if $listaCiudad|count > 0}
                                                {foreach item=fila from=$listaCiudad}
                                                    {if $fila.pk_num_ciudad==$formDB.fk_a010_num_ciudad}
                                                        <option selected value="{$fila.pk_num_ciudad}">{$fila.ind_ciudad}</option>
                                                    {else}
                                                        <option value="{$fila.pk_num_ciudad}">{$fila.ind_ciudad}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                        <label for="cbox_ciudad"><i class="md md-map"></i>&nbsp;Ciudad</label>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-group" id="txt_direccionError">
                                        <input id="txt_direccion" name="form[txt][txt_direccion]" type="text" class="form-control" value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}">
                                        <label for="txt_direccion"><i class="md md-store"></i>&nbsp;Dirección</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group" id="txt_telefonoError">
                                        <input id="txt_telefono" name="form[txt][txt_telefono]" type="text" class="form-control" value="{if isset($formDB.telefono_1)}{$formDB.telefono_1}{/if}">
                                        <label for="txt_telefono"><i class="md md-border-color"></i>&nbsp;Teléfono 1</label>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group" id="txt_telefono2Error">
                                        <input id="txt_telefono2" name="form[txt][txt_telefono2]"type="text" class="form-control" value="{if isset($formDB.telefono_2)}{$formDB.telefono_2}{/if}">
                                        <label for="txt_telefono2"><i class="md md-border-color"></i>&nbsp;Teléfono 2</label>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group" id="txt_telefono3Error">
                                        <input id="txt_telefono3" name="form[txt][txt_telefono3]" type="text" class="form-control" value="{if isset($formDB.telefono_3)}{$formDB.telefono_3}{/if}">
                                        <label for="txt_telefono3"><i class="md md-border-color"></i>&nbsp;Teléfono 3</label>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group" id="txt_faxError">
                                        <input id="txt_fax" name="form[txt][txt_fax]" type="text" class="form-control" value="{if isset($formDB.fax_1)}{$formDB.fax_1}{/if}">
                                        <label for="txt_fax"><i class="md md-border-color"></i>&nbsp;Fax 1</label>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group" id="txt_fax2Error">
                                        <input id="txt_fax2" name="form[txt][txt_fax2]" type="text" class="form-control" value="{if isset($formDB.fax_2)}{$formDB.fax_2}{/if}">
                                        <label for="txt_fax2"><i class="md md-border-color"></i>&nbsp;Fax 2</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="txt_paginaWebError">
                                        <input id="txt_paginaWeb" name="form[txt][txt_paginaWeb]" type="text" maxlength="50" class="form-control" value="{if isset($formDB.ind_pagina_web)}{$formDB.ind_pagina_web}{/if}">
                                        <label for="txt_paginaWeb"><i class="md md-web"></i>&nbsp;Página Web</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="checkbox checkbox-styled">
                                        <label style="margin-top: 10px; margin-bottom: 0;">
                                            <input name="form[int][chk_estatusEnte]" type="checkbox" {if $formDB.num_estatus==1}checked{/if} value="1">
                                            <span>Estatus Ente</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="checkbox checkbox-styled">
                                        <label style="margin-top: 10px; margin-bottom: 0;">
                                            <input  name="form[int][chk_sujetoControl]" type="checkbox" {if $formDB.num_sujeto_control==1}checked{/if} value="1">
                                            <span>Sujeto a Control</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" style="margin-bottom: 0;">
                                        <label for="txt_usuario"><i class="md md-person"></i>Usuario</label>
                                        <div style="font-size: 11px">{if isset($formDB.usuario)}{$formDB.usuario}{/if}</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" style="margin-bottom: 0;">
                                        <label for="txt_fechaModificacion"><i class="fa fa-calendar"></i>Última Modificacion</label>
                                        <div style="font-size: 11px">{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div class="card">
                        <div class="card-head style-primary-light">
                            <header>Información Adicional</header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group" id="txt_nroGacetaError">
                                        <input id="txt_nroGaceta" name="form[txt][txt_nroGaceta]" type="text" maxlength="10" class="form-control" value="{if isset($formDB.ind_gaceta)}{$formDB.ind_gaceta}{/if}">
                                        <label for="txt_nroGaceta"><i class="fa fa-file-text"></i>&nbsp;Nro Gaceta</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group" id="txt_resolucionError">
                                        <input id="txt_resolucion" name="form[txt][txt_resolucion]" type="text" maxlength="10" class="form-control" value="{if isset($formDB.ind_resolucion)}{$formDB.ind_resolucion}{/if}">
                                        <label for="txt_resolucion"><i class="fa fa-file-text"></i>&nbsp;Resolución</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group" id="txt_misionError">
                                        <textarea id="txt_mision" name="form[txt][txt_mision]" rows="2" class="form-control">{if isset($formDB.ind_mision)}{$formDB.ind_mision}{/if}</textarea>
                                        <label for="txt_mision"><i class="fa fa-file-text"></i>&nbsp;Misión</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group" id="txt_visionError">
                                        <textarea id="txt_vision" name="form[txt][txt_vision]" rows="2" class="form-control">{if isset($formDB.ind_vision)}{$formDB.ind_vision}{/if}</textarea>
                                        <label for="txt_vision"><i class="fa fa-file-text"></i>&nbsp;Visión</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group" id="txt_otrosError">
                                        <textarea id="txt_otros" name="form[txt][txt_otros]" rows="2" class="form-control">{if isset($formDB.ind_otros)}{$formDB.ind_otros}{/if}</textarea>
                                        <label for="txt_otros"><i class="fa fa-file-text"></i>&nbsp;Otros</label>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="button" class="btn btn-default logsUsuarioModal " descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
                    <span class="glyphicon glyphicon-floppy-remove"></span>Cancelar
                </button>
                &nbsp;
                <button id="btn_guardar" type="button" class="btn btn-primary logsUsuarioModal">
                    <span class="glyphicon glyphicon-floppy-saved"></span>Guardar
                </button>
            </div>
        </form>
    </div>
</div>
<span class="clearfix"></span>
<script type="text/javascript">
    var appf="";
    var dataValidadaTab1="",dataValidadaTab2=""; //Permite desmarcar los campos.
    $(document).ready(function () {
        var app = new AppFunciones();
        appf=new AppfFunciones();
        app.metWizard();
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");/*ancho de la Modal*/
        $('.campoFecha').datepicker({ autoclose:true, todayHighlight:true, format:'dd-mm-yyyy', language: 'es'});
        $('#txt_fechaFundacion').focus(function (){
            $('#txt_fechaFundacion').attr({ placeholder:"00-00-0000" });
        });
        $('#txt_fechaFundacion').focusout(function (){
            $('#txt_fechaFundacion').removeAttr('placeholder');
        });
        var url='{$_Parametros.url}modPF/maestros/entesCONTROL/';
        $('.select2').select2({ allowClear: true });
        $("#formAjax").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        });
        appf.metValidaDocFiscal($('#txt_nroRif'));//Valida el Rif al tipear.
        appf.metConvertMayuscula($('.lMayuscula'));
        appf.metValidaUrl('#txt_paginaWeb', 'web');
        /**
         *Visualiza el modal para seleccionar el ente padre.
         */
        $('#btn_Ente').click(function(){
            $('#ContenidoModal3').html('');
            $.post(url+'CargaEntesPadresMET',{ lista:true }, function (dato) {
                if(dato){
                    $('#formModalLabel3').html('<i class="md md-search"></i> Listado Entes');
                    $('#ContenidoModal3').html(dato);
                }
            });
        });
        $("#cbox_tipoEnte").change(function () {
            $("#cbox_categoriaEnte").html('').select2({ allowClear: true });
            if($("#cbox_tipoEnte").val()>0){
                $.post(url+'ListarCategoriaMET', { idTipoEnte: $("#cbox_tipoEnte").val() }, function(data){
                    $("#cbox_categoriaEnte").append(data);
                });
            }
        });
        $("#cbox_pais").change(function () {
            $("#cbox_estado").html('').select2({ allowClear: true }); $("#cbox_municipio").html('').select2({ allowClear: true });
            $("#cbox_ciudad").html('').select2({ allowClear: true }); $("#cbox_parroquia").html('').select2({ allowClear: true });
            if($("#cbox_pais").val()>0){
                $.post(url + 'ListarEstadoMET', { idPais:$("#cbox_pais").val() }, function (data) {
                    $("#cbox_estado").html(data);
                });
            }
        });
        $("#cbox_estado").change(function () {
            $("#cbox_municipio").html('').select2({ allowClear: true });
            $("#cbox_ciudad").html('').select2({ allowClear: true });
            $("#cbox_parroquia").html('').select2({ allowClear: true });
            if($("#cbox_estado").val()>0){
                $.post(url+'ListarMunicipioMET', { idEstado:$("#cbox_estado").val() }, function(data){
                    $("#cbox_municipio").append(data);
                });
                $.post(url+'ListarCiudadMET', { idEstado:$("#cbox_estado").val() }, function(data){
                    $("#cbox_ciudad").append(data);
                });
            }
        });
        $("#cbox_municipio").change(function () {
            $("#cbox_parroquia").html('').select2({ allowClear: true });
            if($("#cbox_municipio").val()>0){
                $.post(url+'ListarParroquiaMET', { idMunicipio:$("#cbox_municipio").val() }, function(data){
                    $("#cbox_parroquia").html(data);
                });
            }
        });
        $("#txt_telefono").mask("(9999) 999-99-99");
        $('#txt_telefono2').mask("(9999) 999-99-99");
        $('#txt_telefono3').mask("(9999) 999-99-99");
        $('#txt_fax').mask("(9999) 999-99-99");
        $('#txt_fax2').mask("(9999) 999-99-99");
        /**
         * Permite guardar ó modificar un registro
         */
        $('#btn_guardar').click(function () {
            if($("#idEnte").val()==0){
                msj="Se va a ingresar un nuevo registro. ¿Estas de Acuerdo?";
            }else{
                msj="Se va a actualizar los datos actuales. ¿Estas de Acuerdo?";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                   // swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (data) {
                        if (data.Error == 'error') {
                            appf.metActivaError(data);
                            swal("¡Atención!", "Los campos marcados con X en rojo son obligatorios", "error");
                        } else if (data.Error == 'errorSql') {
                            swal("¡Atención!", data.mensaje, "error");
                        } else {
                            if($("#idEnte").val()==0){
                                if(data.idEnte) {
                                    swal("Información del proceso", data.mensaje, "success");
                                    metBuscar();
                                    $('#cerrarModal').click();
                                }
                            }else{
                                if (data.reg_afectado) {
                                    swal("Información del proceso", data.mensaje, "success");
                                    metBuscar();
                                    $('#cerrarModal').click();
                                }
                            }
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        });
    });
</script>
