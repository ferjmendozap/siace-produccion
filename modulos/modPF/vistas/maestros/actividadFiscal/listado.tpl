<style type="text/css">
    {literal}
    .texto-size{font-size: 12px;}
    {/literal}
</style>
<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">&nbsp;Actividades</h2>
    </div>
    <div class="section-body">
        <form id="form1" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-1 text-right">
                    <label for="cbox_proceso" class="control-label">Procesos:</label>
                </div>
                <div class="col-sm-3">
                    <div class="form-group" id="cbox_procesoError">
                        <select id="cbox_proceso" name="form[int][idProceso]" class="form-control texto-size">
                            {if $listaProceso|count > 0}
                                <option value="">Todo</option>
                                {foreach item=fila from=$listaProceso}
                                    <option value="{$fila.pk_num_proceso}">{$fila.txt_descripcion_proceso}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="col-sm-1 text-right">
                    <label for="cbox_fase" class="control-label">Fases:</label>
                </div>
                <div class="col-sm-4">
                    <div class="form-group" id="cbox_faseError">
                        <select id="cbox_fase" name="form[int][idActividad]" class="form-control texto-size"></select>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-sm-12">
                <div id="resultadoConsulta"></div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var metMontaActividad="",metCrear="",metBuscar=""; metEliminaActividad="";
    $(document).ready(function() {
        var url='{$_Parametros.url}modPF/maestros/actividadFiscalCONTROL/';
        metCrear=function(){
            $('#formModalLabel').html("<i class='icm icm-cog3'></i> Crear Actividad");$('#ContenidoModal').html("");
            $.post(url+'crearModificarMET',{ idActividad:0 },function(datos){
                $('#ContenidoModal').html(datos);
            });
        };
        /**
         * Lista las fases
         */
        $('#cbox_proceso').change(function(){
            $('#resultadoConsulta').html('');$('#cbox_fase').html('');
            metBuscar();
            if($('#cbox_proceso').val()){
                $.post(url+'ListarFasesMET', { idProceso:$('#cbox_proceso').val() }, function (data) {
                    if(data){
                        $('#cbox_fase').html(data);
                    }else{
                        swal("Información del Sistema", "No hay fases para el proceso seleccionado", "error");
                    }
                });
            }
        });
        $('#cbox_fase').change(function(){
            $('#resultadoConsulta').html('');
            metBuscar();
        });
        /**
         * Lista las fases según criterio de búsqueda
         */
        metBuscar=function(){
            $.post(url+'ListarActividadesMET', { idProceso:$('#cbox_proceso').val(), idFase:$('#cbox_fase').val() }, function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        };
        /**
         * Monta los datos para modíficar al clicar un botón de actualizar de la grilla
         */
        metMontaActividad=function(idActividad){
            $('#formModalLabel').html('');$('#ContenidoModal').html('');
            $.post(url+'crearModificarMET',{ idActividad: idActividad },function(data){
                if(data){
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Modificar Actividad');
                    $('#ContenidoModal').html(data);
                }else{
                    $('#cerrarModal').click();
                    swal("¡Atención!", "No se pudo montar los datos", "error");
                }
            });
        }
        /**
         * Ejecuta el proceso de eliminar un registro al clicar un botón de eliminar de la grilla
         * @param idActividad
         */
        metEliminaActividad=function(idActividad){
            swal({
                title: "Confirmación de proceso",
                text: "Se va a eliminar la actividad '"+$('#td_'+idActividad).html()+"'. ¿Estas de acuerdo? ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm) {
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'eliminarMET', { idActividad:idActividad }, function (dato) {
                        if (dato.reg_afectado) {
                            swal("Registro Eliminado", dato.mensaje, "success");
                            metBuscar();
                        } else {
                            swal("¡Atención!", dato.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
        metBuscar();
    });
</script>