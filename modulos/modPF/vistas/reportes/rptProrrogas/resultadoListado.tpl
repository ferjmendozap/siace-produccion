<style type="text/css">
    {literal}
    .table tbody>tr>td.vert-align{
        vertical-align: middle;
    }
    .text-icon{
        font-size: 9px;
    }
    {/literal}
</style>
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Planificación Nro.</th>
                        <th class="text-center">Objetivo</th>
                        <th class="text-center">Ente</th>
                        <th class="text-center">Fecha Inicio</th>
                        <th class="text-center">Fecha fin</th>
                        <th class="text-center">Fec fin Real</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Opci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listado}
                        <tr id="tr_atuacion_'.{$fila.pk_num_planificacion}'">
                            <td class="vert-align">{$fila.cod_planificacion}</td>
                            <td>{$fila.ind_objetivo}</td>
                            <td>{$fila.nombre_ente}</td>
                            <td class="vert-align">{$fila.fec_inicio}</td>
                            <td class="vert-align">{$fila.fecha_finplan}</td>
                            <td class="vert-align">{$fila.fecha_finrealplan}</td>
                            <td class="vert-align {if $fila.ind_estado eq 'AN'}puntero{/if}" {if $fila.ind_estado eq 'AN'}title="{$fila.ind_nota_justificacion}" alt="{$fila.ind_nota_justificacion}"{/if}>{$fila.desc_estado}</td>
                            <td class="vert-align">
                                <button id="btn_ver" class="btn btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                        onclick="metPrintPdf({$fila.pk_num_planificacion})"
                                        title="Click para visualizar en pdf" alt="Click para visualizar en pdf">
                                    <i class="md md-remove-red-eye"><span class="text-icon">Pdf</span></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>