<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: Asignación de procesos fiscales a dependencias de control
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |___|___________________________________________|____________________________________|________________________________|
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        15-07-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class asignaProcesoFiscalControlador extends Controlador{
    private $atAsignaProceso;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atUserValido;          //Contendrá la validéz del usuario
    public function __construct(){
        parent::__construct();
        $this->atAsignaProceso=$this->metCargarModelo('asignaProcesoFiscal');
        $this->atFuncGnrles = new funcionesGenerales();
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex(){
        if($this->atUserValido) {
            //Se lista las dependencias de control fiscal
            $campos="pk_num_dependencia AS id_dependencia,ind_dependencia AS nombre_depedencia";
            $listaDependencias=$this->atFuncGnrles->metBuscaDependenciaInterna($campos,false,1,1,$this->atIdContraloria);
            $this->atVista->assign('listaDependencias',$listaDependencias);
            /*Se lista los tipo de procesos fiscal*/
            $listaProcesos=$this->atAsignaProceso->metConsultas('por_tipo_proceso');
            $this->atVista->assign('listaProcesos',$listaProcesos);
            $this->atVista->metRenderizar('asignaProceso');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    /**
     * Carga la grilla de procesos asignados y disponibles de acuerdo a la dependencia de control seleccionada.
     */
    public function metCargarGrilla(){
        $js[] = 'modPF/DataTablePf';
        $this->atVista->metCargarJs($js);
        $params=$this->metValidarFormArrayDatos('form','int',array('id_x'));
        unset($params["id_x"]);unset($params["idProceso"]);
        $result=$this->atAsignaProceso->metConsultas('por_tipo_proceso');
        if(COUNT($result)){
            foreach($result as $fila){
                $params["pk_num_proceso"]=$fila["pk_num_proceso"];
                $arrAsignado = $this->atAsignaProceso->metConsultas('por_proceso_asignado',$params);
                if(COUNT($arrAsignado)>0){
                    $fila["pk_num_procesodependencia"]=$arrAsignado[0]["pk_num_procesodependencia"];
                }else{
                    $fila["pk_num_procesodependencia"]=0;
                }
                $data[]=$fila;
            }
            if(is_array($data)){
                $this->atVista->assign('listado',$data);
                $this->atVista->metRenderizar('resultadoListado');
            }else{
                echo false;
            }
        }else{
            echo false;
        }
    }
    /**
     * Ejecuta la asignación de los procesos fiscales seleccionados para la dependencia de control seleccionada.
     */
    public function metAgregarProceso(){
        $Exceccion_int = array('id_x');
        $arrParams=$this->metValidarFormArrayDatos('form','int',$Exceccion_int);

        if (isset($arrParams['chk_proceso'])==null) {
            $retornar = array('result' => false,'mensaje' => "Debe seleccionar al menos un proceso");
        } else {
            $result = $this->atAsignaProceso->metAgregaProcesos($arrParams);
            if ($result) {
                $retornar = array(
                    'result' => $result,
                    'mensaje' => 'El proceso se efectuó exitosamente'
                );
            } else {
                $retornar = array(
                    'result' => $result,
                    'mensaje' => 'El proceso no se pudo efectuar. ¡Intente de nuevo!'
                );
            }
        }
        echo json_encode($retornar);
    }
    /**
     * Ejecuta la eliminación de un registro de asignación de proceso fiscal de la dependencia de control actual.
     */
    public function metQuitarProceso(){
        $arr_ids=$this->metValidarFormArrayDatos('form','int');
        $result=$this->atAsignaProceso->metQuitaProcesos($arr_ids);
        if($result){
            $retornar=array(
                'reg_afectado'=>$result,
                'mensaje'=>'El proceso se efectuó exitosamente'
            );
        }else{
            $retornar=array(
                'reg_afectado'=>$result,
                'mensaje'=>'El proceso fiscal no se pudo quitar. Intente de nuevo por favor'
            );
        }
        echo json_encode($retornar);
    }
}
?>