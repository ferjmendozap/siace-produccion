<?php
/***********************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Reportes de Planficaciones.
 * DESCRIPCIÓN: Permite la creación, modificación, revisión, aprobación y anulación de prórrogas de la actividad en
 *              ejecución de planificaciones aprobadas en los diferentes proceso fiscales: Actuación fiscal,
 *              Valoración preliminar, Potestad investigativa, Valoración jurídica y Procedimientos administrativos.
 * PROGRAMADORES.______________________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |___________________________________________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        10-09-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 ***********************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class procesaProrrogaControlador extends Controlador{
    public $atProrrogaModelo;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    public $atIdDependenciaUser;    //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atIdTipoProceso;       //Para el Id del tipo de proceso fiscal.
    private $atUserValido;          //Contendrá la validéz del usuario
    private $atCodTipoProceso;      //Para el código del tipo de proceso
    private $atParams;              //Para asignar los parámetros
    private $atOpcionMenu;          //Para el identificador de la opción del menú: Listar Prórroga(PR), Revisar Prórroga(RV) y Aprobar Prórroga(AP)
    private $atIdProrroga;          //Para optener el id de una prórroga de forma temporal
    private $atTituloForm;          //Para el título del formulario
    public function __construct(){
        parent::__construct();
        $this->atProrrogaModelo = $this->metCargarModelo('procesaProrroga', 'actuacionFiscal/procesaProrroga');
        $this->atFuncGnrles = new funcionesGenerales();
        if(isset($_GET['tipoProceso'])){//Datos que viene en la url de la opción de menú
            $this->atCodTipoProceso=$_GET['tipoProceso'];
            $this->atOpcionMenu=$_GET['opcion'];
        }else{//Datos que vienen de las vistas.
            $codTipoProceso=@$this->metObtenerTexto('codTipoProceso');//Optiene el valor si viene del forms ppal.
            if($codTipoProceso==''){//Entra cuando viene de la vista por serialización
                $Exceccion_int=array('idProrroga','cantDiasProrroga');
                $arrInt=$this->metValidarFormArrayDatos('form', 'int',$Exceccion_int);
                $Exceccion_txt=array('estadoProrroga','txt_motivoProrroga');
                $arrTxt = $this->metValidarFormArrayDatos('form', 'txt',$Exceccion_txt);
                $arrParams=array_merge($arrInt,$arrTxt);
                $this->atParams=$arrParams;
                $codTipoProceso=$arrParams['codTipoProceso'];
                $this->atOpcionMenu=$arrParams['vmOpcionMenu'];
            }else{//Optiene el valor tambien si viene del forms ppal
                $this->atOpcionMenu=@$this->metObtenerTexto('opcionMenu');
            }
            $this->atCodTipoProceso=$codTipoProceso;
        }
        //Se determina la validéz y el tipo de usuario.
        $arrUser = $this->atFuncGnrles->metValidaUsuario();
        if ($arrUser['userValido']) {//Entra, si es un Usuario Válido.
            $this->atTipoUser = $arrUser['tipoUser'];
            $this->atUserValido = $arrUser['userValido'];
            $this->atIdContraloria = $arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser = $arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser = $arrUser['pk_num_dependencia'];
            /*Se busca y asigna el id del tipo de proceso de acuerdo al código del mísmo*/
            $arr_tipoproc = $this->atFuncGnrles->metTipoProcesoFiscal(null, $this->atCodTipoProceso);
            $this->atIdTipoProceso = $arr_tipoproc[0]['pk_num_proceso'];
            $arr_tipoproc = null;
            //Se determina el título del formulario principal y modal de acuerdo al proceso medular fiscal
            if($this->atCodTipoProceso=='01'){//Actuaciones Fiscales
                $tituloForm='Prórrogas Actuaciones Fiscales';
            }elseif($this->atCodTipoProceso=='02'){//valoraciones preliminares
                $tituloForm='Prórrogas Valoración Preliminar';
            }elseif($this->atCodTipoProceso=='03'){
                $tituloForm='Prórrogas Potestad Investigativa';
            }elseif($this->atCodTipoProceso=='04'){
                $tituloForm='Prórrogas Valoración Jurídica';
            }elseif($this->atCodTipoProceso=='05'){
                $tituloForm='Prórrogas Procedimientos Administrativos';
            }
            $this->atTituloForm=$tituloForm;
        } else {//De lo contrario, usuario no válido
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
        $arrUser = null;
    }
    function __destruct(){}
    /**
     * Busca entes externos
     */
    public function metBuscarEntes($iddep_padre){
        $retornar = array("filas" => false);
        $result = $this->atFuncGnrles->metCargaCboxRecursivoEnte($iddep_padre, '');
        if (COUNT($result) > 0) {
            $retornar = array("filas" => $result);
        }
        return $retornar;
    }
    /**
     * Lista las dependencias de un ente externo al ser seleccionado
     */
    public function metListaEntes(){
        $idEnte = $this->metObtenerInt('idEnte');
        $listaEntes = $this->metBuscarEntes($idEnte);
        echo json_encode($listaEntes);
    }
    public function metConsolidaOpcionesCbox($arrData,$arrEstados){
        $listaOpcionesCbox=array();
        foreach($arrData as $fila){
            if(in_array($fila['estado'],$arrEstados)){
                $listaOpcionesCbox[]=$fila;
            }
        }
        return $listaOpcionesCbox;
    }
    public function metComplementosForm(){
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
    }
    /**
     * Visualiza el from principal
     * @throws Exception
     */
    public function metIndex(){
        $userInvitado=false; $arrDependencia=array(); $listaPersona=array();$selectedDep=false; $quitaOpcion=false;
        $indiceOpcion="";$preTitulo=' Listado ';
        $listaEstadosPlan=array();$verCboxBuscarPor=false;
        $complementosCss = array(
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('tempYear', date('Y'));
        $this->atVista->assign('codTipoProceso', $this->atCodTipoProceso);
        $this->atVista->assign('opcionMenu', $this->atOpcionMenu);
        //Se extrae una Lista de los tipos de estado de la planificación de acuerdo al tipo de proceso.
        if($this->atCodTipoProceso=='01'){//Actuaciones Fiscales
            if($this->atOpcionMenu=='PR'){
                $arrEstadosPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosAF');
                //Se determina los estados a mostrar en el combobox
                $arrEstados=array('AP','TE');
                $listaEstadosPlan=$this->metConsolidaOpcionesCbox($arrEstadosPlan,$arrEstados);
                $verCboxBuscarPor=true;
            }elseif($this->atOpcionMenu=='RV'){
                $quitaOpcion=true;$indiceOpcion=2; $verCboxBuscarPor=false; $preTitulo=' Revisar ';
            }elseif($this->atOpcionMenu=='AP'){
                $quitaOpcion=true;$indiceOpcion=0; $verCboxBuscarPor=false; $preTitulo=' Aprobar ';
            }
        }elseif($this->atCodTipoProceso=='02'){//Valoraciones Preliminares
            if($this->atOpcionMenu=='PR'){
                $arrEstadosPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosVP');
                //Se determina los estados a mostrar en el combobox
                $arrEstados=array('AP');
                $listaEstadosPlan=$this->metConsolidaOpcionesCbox($arrEstadosPlan,$arrEstados);
                $verCboxBuscarPor=true;
            }elseif($this->atOpcionMenu=='RV'){
                $quitaOpcion=true;$indiceOpcion=2; $verCboxBuscarPor=false; $preTitulo=' Revisar ';
            }elseif($this->atOpcionMenu=='AP'){
                $quitaOpcion=true;$indiceOpcion=0; $verCboxBuscarPor=false; $preTitulo=' Aprobar ';
            }
        }elseif($this->atCodTipoProceso=='03'){//Potestad Investigativas
            if($this->atOpcionMenu=='PR') {
                $arrEstadosPlan = $this->atFuncGnrles->metListaEstadosPlanific('EstadosPI');
                $arrEstados = array('AP', 'TE');
                $listaEstadosPlan = $this->metConsolidaOpcionesCbox($arrEstadosPlan, $arrEstados);
                $verCboxBuscarPor=true;
            }elseif($this->atOpcionMenu=='RV'){
                $quitaOpcion=true;$indiceOpcion=2; $verCboxBuscarPor=false; $preTitulo=' Revisar ';
            }elseif($this->atOpcionMenu=='AP'){
                $quitaOpcion=true;$indiceOpcion=0; $verCboxBuscarPor=false; $preTitulo=' Aprobar ';
            }
        }elseif($this->atCodTipoProceso=='04'){//Valoraciones Jurídicas
            if($this->atOpcionMenu=='PR'){
                $arrEstadosPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosVJ');
                //Se determina los estados a mostrar en el combobox
                $arrEstados=array('AP');
                $listaEstadosPlan=$this->metConsolidaOpcionesCbox($arrEstadosPlan,$arrEstados);
                $verCboxBuscarPor=true;
            }elseif($this->atOpcionMenu=='RV'){
                $quitaOpcion=true;$indiceOpcion=2; $verCboxBuscarPor=false; $preTitulo=' Revisar ';
            }elseif($this->atOpcionMenu=='AP'){
                $quitaOpcion=true;$indiceOpcion=0; $verCboxBuscarPor=false; $preTitulo=' Aprobar ';
            }
        }elseif($this->atCodTipoProceso=='05'){//Procedimientos administrativos
            if($this->atOpcionMenu=='PR'){
                $arrEstadosPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosDRA');
                //Se determina los estados a mostrar en el combobox
                $arrEstados=array('AP','TE');
                $listaEstadosPlan=$this->metConsolidaOpcionesCbox($arrEstadosPlan,$arrEstados);
                $verCboxBuscarPor=true;
            }elseif($this->atOpcionMenu=='RV'){
                $quitaOpcion=true;$indiceOpcion=2; $verCboxBuscarPor=false; $preTitulo=' Revisar ';
            }elseif($this->atOpcionMenu=='AP'){
                $quitaOpcion=true;$indiceOpcion=0; $verCboxBuscarPor=false; $preTitulo=' Aprobar ';
            }
        }
        $this->atVista->assign('listaEstadosplan', $listaEstadosPlan);
        /*Lista los estados de Prórrogas*/
        $listaEstadoProrroga=$this->atFuncGnrles->metListaEstadosPlanific('EstadosProg');
        if(COUNT($listaEstadoProrroga)>0){
            if($quitaOpcion){
                unset($listaEstadoProrroga[$indiceOpcion]);
            }
            $this->atVista->assign('listaEstadoProrroga', $listaEstadoProrroga);
        }else{
            $this->atVista->assign('listaEstadoProrroga', array());
        }
        //Si es un usuario invitado, se extraen las dependencias de control al cual está como invitado
        if ($this->atTipoUser == 'userInvitado') {
            $userInvitado=true;
            $arrDependencia = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria, $this->atIdTipoProceso);
            if ($arrDependencia) {
                if(COUNT($arrDependencia)==1){//Si sólo tiene una dependencia como invitado, Entra.
                    $selectedDep=true;//Permite seleccionar por defecto la dependencia al mostrar el formulario.
                }
            }
        }
        $this->atVista->assign('verCboxBuscarPor',$verCboxBuscarPor);
        $this->atVista->assign('listaDependencias', $arrDependencia);
        $this->atVista->assign('listaPersonas', $listaPersona);
        $this->atVista->assign('selectedDep', $selectedDep);
        $this->atVista->assign('userInvitado', $userInvitado);
        //Lista los años a partír de la fecha menor de inicio de las planificaciones.
        $this->atVista->assign('listayears', $this->atFuncGnrles->metComboBoxYears());
        $this->atVista->assign('year_actual', date('Y'));
        /*Lista los tipo de actuación fiscal*/
        $campos = "pk_num_miscelaneo_detalle AS idtipo_actuacion,ind_nombre_detalle AS nombretipo_actuacion";
        if($this->atCodTipoProceso=='04' OR $this->atCodTipoProceso=='05'){//Agrega la opción "Acción Fiscal" a la lista para el combobox Tipo actuación para los casos de Valoración jurídica ó Procedimientos administrativos
            $listaTipoActuacion = $this->atFuncGnrles->metBuscaMiscelanio($campos, 'PFTAF');
        }else{
            $listaTipoActuacion = $this->atFuncGnrles->metBuscaMiscelanio($campos, 'PFTAF','PFAF'); //Menos la opción "Acción Fiscal"
        }
        if(COUNT($listaTipoActuacion)>0){
            $this->atVista->assign('listaTipoActuacion', $listaTipoActuacion);
        }else{
            $this->atVista->assign('listaTipoActuacion', array());
        }
        /*Lista los origen de actuación*/
        $campos="pk_num_miscelaneo_detalle AS idorigen_actuacion,cod_detalle AS cod_origen, ind_nombre_detalle AS nombre_origenactuacion";
        $listaOrigenActuacion=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFODA','');
        if(COUNT($listaOrigenActuacion)>0){
            $this->atVista->assign('listaOrigenActuacion', $listaOrigenActuacion);
        }else{
            $this->atVista->assign('listaOrigenActuacion', array());
        }
        /*Lista los entes externos*/
        $entesExternos = $this->metBuscarEntes(0);
        if(COUNT($entesExternos)>0){
            $this->atVista->assign('listaentes', $entesExternos["filas"]);
        }else{
            $this->atVista->assign('listaentes', array());
        }
        $this->atVista->assign('tituloForm', $preTitulo.$this->atTituloForm);
        $this->atVista->metRenderizar('listadoPlanificacionPrga');
    }
    /**
     * Permite listar las planificaciones ó prórrogas para mostrarlas en la grilla del formulario ppal.
     */
    public function metListarPlanificaciones(){
        //Se capturan los parámetros
        $arrParams['cbox_buscarPor']=$this->metObtenerTexto('cbox_buscarPor');
        $arrParams['cbox_yearplan']=$this->metObtenerTexto('cbox_yearplan');
        $arrParams['txt_codPlanificacion']=$this->metObtenerTexto('txt_codPlanificacion');
        $arrParams['cbox_dependencia']=$this->metObtenerTexto('cbox_dependencia');
        $arrParams['cbox_tipoActuacion']=$this->metObtenerTexto('cbox_tipoActuacion');
        $arrParams['cbox_origenActuacion']=$this->metObtenerTexto('cbox_origenActuacion');
        $arrParams['cbox_ente']=$this->metObtenerTexto('cbox_ente');
        $arrParams['txt_fechainic1']=$this->metObtenerTexto('txt_fechainic1');
        $arrParams['txt_fechainic2']=$this->metObtenerTexto('txt_fechainic2');
        $arrParams['cbox_estadosplan']=$this->metObtenerTexto('cbox_estadosplan'); //OJO si no viene filtrar por los correspondientes estados AP y TE ó AP
        $arrParams['cbox_estadosProrroga']=$this->metObtenerTexto('cbox_estadosProrroga');
        $arrParams['opcionMenu']=$this->atOpcionMenu;
        //Se cargan complementos para grilla
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $arrParams["idTipoProceso"] = $this->atIdTipoProceso;
        if($arrParams['txt_fechainic1'] OR $arrParams['txt_fechainic2']) {
            if (!$arrParams['txt_fechainic1']) {
                $arrParams['txt_fechainic1'] = $arrParams['txt_fechainic2'];
            } elseif (!$arrParams['txt_fechainic2']) {
                $arrParams['txt_fechainic2'] = $arrParams['txt_fechainic1'];
            }
            $arrParams['txt_fechainic1'] = $this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechainic1']);
            $arrParams['txt_fechainic2'] = $this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechainic2']);
        }
        $arrParams["id_contraloria"] = $this->atIdContraloria;
        if ($this->atTipoUser == 'userInvitado') {//Si es un usuario invitado, entra
            $arrParams["idDependencia"] = $arrParams["cbox_dependencia"];
        }else{
            $arrParams["idDependencia"] = $this->atIdDependenciaUser;
        }
        $arrParams["idPlanificacion"]=null; unset($arrParams["cbox_dependencia"]);
        //Se ejecuta la consulta de planificaciones ó prórrogas
        $result = $this->atProrrogaModelo->metConsultaPlanificacion($arrParams);
        $data = array();
        if (COUNT($result) > 0) {//Se prepara los resultados.
            $listaPlanificacion=false;//Permite enviar los resultados a la lista de planificaciones ó a la lista de prórrogas
            foreach ($result as $fila) {
                if($arrParams['cbox_buscarPor']=="PL"){//Entra cuando se consulta por planificaciones
                    $listaPlanificacion=true;
                    $fila['ind_estado'] = $fila['ind_estado_planificacion'];
                    $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($fila['fk_a039_num_ente']);
                    $fila['nombre_ente'] = $arrEnte['cadEntes'];
                    $fila['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($fila['ind_estado']);
                    $fila['fec_inicio'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio']);
                    $arrFechasfin = $this->atFuncGnrles->metBuscaFechaFinPlan($fila['pk_num_planificacion']);
                    $data[] = $fila = array_merge($fila, $arrFechasfin);
                }else{//Entra cuando se consulta por prórroga
                    //Se determina la descripción del estado de la prórroga
                    $fila['desc_estado'] = $this->atFuncGnrles->metEstadoProrroga($fila['ind_estado']);
                    if($fila['fk_rhb001_num_empleado_revispor'] AND $arrParams['cbox_estadosProrroga']=='RV'){
                        if($fila['ind_estado']=='AP'){
                            $fila['desc_estado']='Revisada y '.$fila['desc_estado'];
                        }else if($fila['ind_estado']=='AN'){
                            $fila['desc_estado']='Revisada y '.$fila['desc_estado'];
                        }
                    }
                    $fila['fec_registro_prorroga'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_registro_prorroga']);
                    //Se determina el Icon a mostrar en la grilla de resultados de prórrogas
                    if($this->atOpcionMenu=='PR' OR $this->atOpcionMenu=='RV'){
                        if($fila['ind_estado']=='PR'){
                            $iCon='act';
                        }else{
                            $iCon='ver';
                        }
                    }elseif($this->atOpcionMenu=='AP'){
                        if($fila['ind_estado']=='RV'){
                            $iCon='act';
                        }else{
                            $iCon='ver';
                        }
                    }
                    $fila['iCon'] = $iCon;
                    $data[] = $fila;
                }
            }
            if($listaPlanificacion){//Visualiza el resultado de planificaciones con actividad en ejecución
                //se determina la Permisología de creación de prórrogas del usuario.
                $perfiles=Session::metObtener('perfil'); $permisoUser=false;$botonNuevaProrga = false;
                if(($this->atCodTipoProceso=='01' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-01-03-04-N',$perfiles))
                    OR ($this->atCodTipoProceso=='02' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-02-01-05-04-N',$perfiles))
                    OR ($this->atCodTipoProceso=='03' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-02-02-05-04-N',$perfiles))
                    OR ($this->atCodTipoProceso=='04' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-03-01-06-04-N',$perfiles))
                    OR ($this->atCodTipoProceso=='05' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-03-02-04-04-N',$perfiles))){
                    $botonNuevaProrga = true;
                }
                $this->atVista->assign('botonNuevaProrga', $botonNuevaProrga);
                $this->atVista->assign('listado', $data);
                $this->atVista->metRenderizar('resultadoPlanificacion');
            }else{//Visualiza el resultado de prórrogas
                $this->atVista->assign('listado', $data);
                $this->atVista->metRenderizar('resultadoProrroga');
            }
        } else {
            return false;
        }
    }
    public function metBuscaActividades(){
        $arrParams=$this->atParams;
        $habilitarCampo = false;
        //Se determina la habilitación del campo Prorga de la grilla de actividades
        if($arrParams['accion']=='nueva'){
            $habilitarCampo=true;
        }elseif($this->atOpcionMenu=='PR' AND $arrParams['accion']=='modificar'){
            if($arrParams['estadoProrroga']=='PR') {
                $habilitarCampo = true;
            }
        }
        $arrData=array();
        $result=$this->atProrrogaModelo->metActividadesAsignadas(array("idPlanificacion"=>$arrParams['idPlanificacion'],"completa"=>true));
        if(COUNT($result)>0) {
            $js[] = 'modPF/DataTablePf';
            $this->atVista->metCargarJs($js);
            foreach ($result as $fila) {
                $fila['fec_inicio_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_actividad']);
                $fila['fec_inicio_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_real_actividad']);
                $fila['fec_culmina_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_actividad']);
                $fila['fec_culmina_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_real_actividad']);
                if($arrParams['accion']=='nueva'){//Entra, cuando se va a crear una nueva prórroga
                    $arrProrroga=array('id_prorroga'=>'','num_dias_prorroga'=>'','estado_prorroga'=>'');
                }else{//Entra, cuando existen prórrogas
                    if($arrParams['idProrroga']>0 AND $arrParams['secuenciaActividad']==$fila['num_secuencia_actividad']){
                        //Se extrae la prórroga de la actividad
                        $arrProrroga=$this->atProrrogaModelo->metBuscaProrroga('por_idProrroga',$arrParams['idProrroga']);
                        if($arrProrroga['estado_prorroga']=='AN' OR $arrProrroga['estado_prorroga']=='AP'){
                            $arrProrroga['num_dias_prorroga']=0;
                        }
                        $arrParams['idProrroga']=0; //se asigna cero para que no vuelva a entrar.
                    }else{
                        $arrProrroga=$this->atProrrogaModelo->metBuscaProrroga('por_idActividadPlanif',$fila['pk_num_actividad_planific']);
                        if(!$arrProrroga){
                            $arrProrroga=array('id_prorroga'=>'','num_dias_prorroga'=>'','estado_prorroga'=>'');
                        }else{
                            if($arrProrroga['estado_prorroga']=='AN' OR $arrProrroga['estado_prorroga']=='AP'){
                                $arrProrroga['num_dias_prorroga']=0;
                            }
                        }
                    }
                }
                $arrData[]=array_merge($fila,$arrProrroga);
                $arrProrroga=null;
            }
            $arrData=$this->atFuncGnrles->metAgrupaArray($arrData,'txt_descripcion_fase','cod_fase');
            $this->atVista->assign('habilitarCampo', $habilitarCampo);
            $this->atVista->assign('listaActividades', $arrData);
            $this->atVista->assign('contador', 0);
            $this->atVista->assign('ifilasubtotal', 0);
            $this->atVista->assign('SubTotal_dias', 0);
            $this->atVista->assign('Total_dias', 0);
            $this->atVista->assign('totalDiasNoAfecta', 0);
            $this->atVista->assign('SubTotal_diasProrroga', 0);
            $this->atVista->assign('Totaldias_prorroga', 0);
            $this->atVista->assign('totalGeneralDias', 0);
            $this->atVista->metRenderizar('grillaActividades');
        }else{
            echo "";
        }
    }
    /**
     * Monta el form modal para una prórroga nueva ó existente.
     * @throws Exception
     */
    public function metMontaProrroga(){
        $this->atVista->assign('opcionMenu', $this->atOpcionMenu);
        $this->metComplementosForm();
        //Captura de parámetros
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $idProrroga=$this->metObtenerTexto('idProrroga'); $this->atIdProrroga=$idProrroga;
        $codPlanificacion=$this->metObtenerTexto('codPlanificacion');
        $estadoPlanificacion=$this->metObtenerTexto('estadoPlanificacion');
        $estadoProrroga=$this->metObtenerTexto('estadoProrroga');
        $accion=$this->metObtenerTexto('accion');
        //declaración de variables
        $descEstadoProrga='En Preparación';
        $arrProrga="";
        $preparada_por="";
        $revisada_por="";
        $aprobada_por="";
        $actualizada_por="";
        $botonGuardar=false;
        $botonRevisar=false;
        $botonAnular=false;
        $botonAprobar=false;

        //echo 'Proceso: '.$this->atCodTipoProceso.'  OpcionMenu:'.$this->atOpcionMenu.'  EstadoProga:'.$estadoProrroga;

        //Se extrae la última actividad en ejecución ó terminada
        if ($arrActividad = $this->atProrrogaModelo->metActividadEjTe($idPlanificacion)) {
            if($accion=='nueva'){//Entra, si es para una nueva prórroga
                $arrProrga['descEstadoProrga'] = $descEstadoProrga;
                $arrProrga['ind_estado'] = 'PR';
            }else{
                //Se extrae la prórroga de la actividad en ejecución.
                if ($arrProrga = $this->atProrrogaModelo->metProrrogaActividad($idProrroga)) {
                    $preparada_por = $this->atFuncGnrles->metBuscaUsuario(null, $arrProrga['fk_rhb001_num_empleado_prepapor']);
                    $actualizada_por = $this->atFuncGnrles->metBuscaUsuario($arrProrga['fk_a018_num_seg_user_actpro'], null);
                    $arrProrga['fec_prorroga_preparado'] = $this->atFuncGnrles->metFormateaFecha($arrProrga['fec_prorroga_preparado']);
                    if($arrProrga['fec_prorroga_revisado']){
                        $arrProrga['fec_prorroga_revisado'] = $this->atFuncGnrles->metFormateaFecha($arrProrga['fec_prorroga_revisado']);
                    }
                    if($arrProrga['fec_prorroga_aprobado']){
                        $arrProrga['fec_prorroga_aprobado'] = $this->atFuncGnrles->metFormateaFecha($arrProrga['fec_prorroga_aprobado']);
                    }
                    $arrProrga['id_prorroga'] = $arrProrga['pk_num_prorroga_actividad'];
                    if ($arrProrga['ind_estado'] == 'RV') {
                        $revisada_por = $this->atFuncGnrles->metBuscaUsuario(null, $arrProrga['fk_rhb001_num_empleado_revispor']);
                        $descEstadoProrga = 'Revisada';
                    } elseif ($arrProrga['ind_estado'] == 'AP') {
                        $revisada_por = $this->atFuncGnrles->metBuscaUsuario(null, $arrProrga['fk_rhb001_num_empleado_revispor']);
                        $descEstadoProrga = 'Aprobada';
                        $aprobada_por=$this->atFuncGnrles->metBuscaUsuario(null, $arrProrga['fk_rhb001_num_empleado_aprobpor']);
                    } elseif ($arrProrga['ind_estado'] == 'AN') {
                        $descEstadoProrga = 'Anulada';
                    }
                    $arrProrga['fec_actualiza_prorroga'] = $this->atFuncGnrles->metFormateaFecha($arrProrga['fec_actualiza_prorroga']);
                    $arrProrga['fec_registro_prorroga'] = $this->atFuncGnrles->metFormateaFecha($arrProrga['fec_registro_prorroga']);
                    unset($arrProrga['fk_pfc001_num_actividad_planific']);
                    unset($arrProrga['pk_num_prorroga_actividad']);
                    unset($arrProrga['fk_rhb001_num_empleado_prepapor']);
                    unset($arrProrga['fk_rhb001_num_empleado_revispor']);
                    unset($arrProrga['fk_rhb001_num_empleado_aprobpor']);
                    unset($arrProrga['fk_a018_num_seg_user_regpro']);
                    unset($arrProrga['fk_a018_num_seg_user_actpro']);
                }else{
                    $arrActividad=null; echo ''; exit;
                }
            }
            $arrProrga['desc_estadoProrga'] = $descEstadoProrga;
            $arrProrga['preparada_por'] = $preparada_por;
            $arrProrga['revisada_por'] = $revisada_por;
            $arrProrga['aprobada_por'] = $aprobada_por;
            $arrProrga['actualizada_por'] = $actualizada_por;

            //se determina la Permisología de operación del usuario.
            $perfiles=Session::metObtener('perfil'); $permisoUser=false;
            if(($this->atCodTipoProceso=='01' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-01-03-04-N',$perfiles))
                OR ($this->atCodTipoProceso=='02' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-02-01-05-04-N',$perfiles))
                OR ($this->atCodTipoProceso=='03' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-02-02-05-04-N',$perfiles))
                OR ($this->atCodTipoProceso=='04' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-03-01-06-04-N',$perfiles))
                OR ($this->atCodTipoProceso=='05' AND $this->atOpcionMenu=='PR' AND in_array('PF-01-03-02-04-04-N',$perfiles))){
                $permisoUser = true;
                if ($accion=="nueva") {
                    $botonGuardar = true;
                }else{
                    if ($arrProrga['ind_estado']=='PR'){
                        $botonGuardar = true;
                        if (($this->atCodTipoProceso=='01' AND in_array('PF-01-01-03-07-AN',$perfiles))
                            OR ($this->atCodTipoProceso=='02' AND in_array('PF-01-02-01-05-07-AN',$perfiles))
                            OR ($this->atCodTipoProceso=='03' AND in_array('PF-01-02-02-05-07-AN',$perfiles))
                            OR ($this->atCodTipoProceso=='04' AND in_array('PF-01-03-01-06-07-AN',$perfiles))
                            OR ($this->atCodTipoProceso=='05' AND in_array('PF-01-03-02-04-07-AN',$perfiles))){//Anula sólo si la prórroga está en preparación
                            $botonAnular = true;
                        }
                    }
                }
            }elseif(($this->atCodTipoProceso=='01' AND $this->atOpcionMenu=='RV' AND in_array('PF-01-01-03-05-RV',$perfiles))
                OR ($this->atCodTipoProceso=='02' AND $this->atOpcionMenu=='RV' AND in_array('PF-01-02-01-05-05-RV',$perfiles))
                OR ($this->atCodTipoProceso=='03' AND $this->atOpcionMenu=='RV' AND in_array('PF-01-02-02-05-05-RV',$perfiles))
                OR ($this->atCodTipoProceso=='04' AND $this->atOpcionMenu=='RV' AND in_array('PF-01-03-01-06-05-RV',$perfiles))
                OR ($this->atCodTipoProceso=='05' AND $this->atOpcionMenu=='RV' AND in_array('PF-01-03-02-04-05-RV',$perfiles))){
                $permisoUser = true;
                if ($arrProrga['ind_estado']=='PR') {
                    $botonRevisar=true;
                    if (($this->atCodTipoProceso=='01' AND in_array('PF-01-01-03-07-AN',$perfiles))
                        OR ($this->atCodTipoProceso=='02' AND in_array('PF-01-02-01-05-07-AN',$perfiles))
                        OR ($this->atCodTipoProceso=='03' AND in_array('PF-01-02-02-05-07-AN',$perfiles))
                        OR ($this->atCodTipoProceso=='04' AND in_array('PF-01-03-01-06-07-AN',$perfiles))
                        OR ($this->atCodTipoProceso=='05' AND in_array('PF-01-03-02-04-07-AN',$perfiles))){//Anula sólo si la prórroga está en preparación
                        $botonAnular = true;
                    }
                }elseif ($arrProrga['ind_estado']=='RV'){
                    if(($this->atCodTipoProceso=='01' AND in_array('PF-01-01-03-07-AN',$perfiles))
                    OR ($this->atCodTipoProceso=='02' AND in_array('PF-01-02-01-05-07-AN',$perfiles))
                    OR ($this->atCodTipoProceso=='03' AND in_array('PF-01-02-02-05-07-AN',$perfiles))
                    OR ($this->atCodTipoProceso=='04' AND in_array('PF-01-03-01-06-07-AN',$perfiles))
                    OR ($this->atCodTipoProceso=='05' AND in_array('PF-01-03-02-04-07-AN',$perfiles))) {//Anula sólo si la prórroga está revisada
                        $permisoUser = true; $botonAnular = true;
                    }
                }

            }elseif(($this->atCodTipoProceso=='01' AND $this->atOpcionMenu=='AP' AND in_array('PF-01-01-03-06-AP',$perfiles))
                OR ($this->atCodTipoProceso=='02' AND $this->atOpcionMenu=='AP' AND in_array('PF-01-02-01-05-06-AP',$perfiles))
                OR ($this->atCodTipoProceso=='03' AND $this->atOpcionMenu=='AP' AND in_array('PF-01-02-02-05-06-AP',$perfiles))
                OR ($this->atCodTipoProceso=='04' AND $this->atOpcionMenu=='AP' AND in_array('PF-01-03-01-06-06-AP',$perfiles))
                OR ($this->atCodTipoProceso=='05' AND $this->atOpcionMenu=='AP' AND in_array('PF-01-03-02-04-06-AP',$perfiles))){
                $permisoUser = true;
                if ($arrProrga['ind_estado']=='RV') {
                    $botonAprobar = true;
                    if (($this->atCodTipoProceso=='01' AND in_array('PF-01-01-03-07-AN', $perfiles))
                        OR ($this->atCodTipoProceso=='02' AND in_array('PF-01-02-01-05-07-AN', $perfiles))
                        OR ($this->atCodTipoProceso=='03' AND in_array('PF-01-02-02-05-07-AN', $perfiles))
                        OR ($this->atCodTipoProceso=='04' AND in_array('PF-01-03-01-06-07-AN',$perfiles))
                        OR ($this->atCodTipoProceso=='05' AND in_array('PF-01-03-02-04-07-AN',$perfiles))){
                        $botonAnular = true;
                    }
                }
            }
            $this->atVista->assign('tituloForm', $this->atTituloForm);
            $this->atVista->assign('accion', $accion);
            $this->atVista->assign('idPlanificacion', $idPlanificacion);
            $this->atVista->assign('estadoPlanificacion', $estadoPlanificacion);
            $this->atVista->assign('codPlanificacion', $codPlanificacion);
            $this->atVista->assign('codTipoProceso', $this->atCodTipoProceso);
            $this->atVista->assign('permisoUser', $permisoUser);
            $this->atVista->assign('botonAnular', $botonAnular);
            $this->atVista->assign('botonGuardar', $botonGuardar);
            $this->atVista->assign('botonRevisar', $botonRevisar);
            $this->atVista->assign('botonAprobar', $botonAprobar);
            $formData = array_merge($arrActividad,$arrProrga);
            $this->atVista->assign('formData', $formData);
            $this->atVista->metRenderizar('crearModificar');
        } else {
            echo "";
        }
    }

    /**
     * Permite reprogramar las actividades de acuerdo a la nueva cantidad de días de prórroga
     */
    public function metRecalcularActividades($deAprobar=NULL){
        $arrParams=$this->atParams;//Se asignan los paŕametros
        $result=$this->atProrrogaModelo->metActividadesAsignadas(array('idPlanificacion'=>$arrParams['idPlanificacion'],"completa"=>false));
        $indiceFila=0;
        foreach($result as $fila){
            if($fila['num_secuencia_actividad']>=$arrParams['secuenciaActividad']){
                if($fila['num_secuencia_actividad']==$arrParams['secuenciaActividad']){
                    $fecha_inicial=$fila['fec_inicio_real_actividad'];
                    $duracion_actividad=$arrParams['cantDiasProrroga']+$fila['num_dias_duracion_actividad']+$fila['num_dias_prorroga_actividad'];
                }else{
                    $duracion_actividad=$fila['num_dias_duracion_actividad'];
                }
                $fecha_fin_actividad=$this->atFuncGnrles->metSumarDiasHabiles($fecha_inicial, (int) $duracion_actividad);
                $fila=array('fecha_inicialReal'=>$fecha_inicial,'fecha_fin_actividadReal'=>$fecha_fin_actividad,
                    'indiceFila'=>$indiceFila,'idActividaPlanif'=>$fila['pk_num_actividad_planific'],
                    'cantDiasProrroga'=>$fila['num_dias_prorroga_actividad']);
                $duracion_actividad++;
                $fecha_inicial=$this->atFuncGnrles->metSumarDiasHabiles($fecha_inicial, (int) $duracion_actividad);
                $fila['fecha_inicialReal']=$this->atFuncGnrles->metFormateaFecha($fila['fecha_inicialReal']);
                $fila['fecha_fin_actividadReal']=$this->atFuncGnrles->metFormateaFecha($fila['fecha_fin_actividadReal']);
            }else{
                $fecha_inicialReal=$this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_real_actividad']);
                $fecha_fin_actividadReal=$this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_real_actividad']);
                $fila=array('fecha_inicialReal'=>$fecha_inicialReal,'fecha_fin_actividadReal'=>$fecha_fin_actividadReal,
                    'indiceFila'=>$indiceFila,'idActividaPlanif'=>$fila['pk_num_actividad_planific'],
                    'cantDiasProrroga'=>$fila['num_dias_prorroga_actividad']);
            }
            $arrData[]=$fila;
            $indiceFila++;
        }
        if($deAprobar){
            return $arrData;
        }else{
            echo json_encode(array("filas"=>$arrData));
        }
    }
    /**
     * Permite crear ó actualizar una prórroga
     */
    public function metGuardarProrroga(){
        $arrParams=$this->atParams;
        $arrParams["atIdEmpleadoUser"]=$this->atIdEmpleadoUser;
        $arrParams["fecha_actual"] = date('Y-m-d H:i:s');
        $fechaActual=date('d-m-Y');
        //print_r($arrParams);exit;

        if($arrParams["idProrroga"]==0){
            //Se busca el correlativo actual de prórroga.
            $nroProrroga=$this->atProrrogaModelo->metCorrelativoProrroga($arrParams["idPlanificacion"]);
            if($nroProrroga){
                //Se construye el código de la nueva prórroga
                $nuevoNro=$nroProrroga['nroProrrogaActual']+1;
                $nuevoNro=(string) str_repeat("0", 2-strlen($nuevoNro)).$nuevoNro;
                $codProrroga=$arrParams["codPlanificacion"].'-'.$nuevoNro;
                $arrParams["codProrroga"]=$codProrroga;
                //Se ingresa la prórroga.
                $result=$this->atProrrogaModelo->metIngresaProrroga($arrParams);

                if($result){
                    $nombrePersEjecuta=$this->atFuncGnrles->metBuscaUsuario(null, $this->atIdEmpleadoUser);
                    $retornar = array(
                        "result"=>'nuevo',"idProrroga"=>$result,"codProrroga"=>$codProrroga,
                        "fechaProceso"=>$fechaActual,"nombrePersEjecuta"=>$nombrePersEjecuta,
                        "mensaje"=>"El proceso se efectuó exitosamente"
                    );
                }else{
                    $retornar = array("result"=>"Error");
                    $retornar["mensaje"] = "El proceso no se efectuó. Ocurrió un error de conexión";
                }
            }else{
                $retornar = array("result"=>"Error");
                $retornar["mensaje"] = "El proceso no se efectuó. No se pudo generar el código de la prórroga";
            }
        }else{
            $result=$this->atProrrogaModelo->metActualizaProrroga($arrParams);
            if($result){
                $nombrePersEjecuta=$this->atFuncGnrles->metBuscaUsuario(null, $this->atIdEmpleadoUser);
                $retornar = array(
                    "result"=>'modificada',
                    "fechaProceso"=>$fechaActual,"nombrePersEjecuta"=>$nombrePersEjecuta,
                    "mensaje"=>"El proceso se efectuó exitosamente"
                );
            }else{
                $retornar = array("result"=>"Error");
                $retornar["mensaje"] = "El proceso no se efectuó. Ocurrió un error de conexión";
            }
        }
        echo json_encode($retornar);
    }
    /**
     * Procesa la anulación de una prórroga en preparación ó revisada
     */
    public function metAnularProrroga(){
        $arrParams=$this->atParams;
        $arrParams["fecha_actual"] = date('Y-m-d H:i:s');
        $result=$this->atProrrogaModelo->metAnulaProrroga($arrParams);
        if($result){
            $nombrePersEjecuta=$this->atFuncGnrles->metBuscaUsuario(null, $this->atIdEmpleadoUser);
            $retornar = array(
                "reg_afectado"=>true,
                "fechaProceso"=>date('d-m-Y'),"nombrePersEjecuta"=>$nombrePersEjecuta,
                "estado_prorroga"=>'AN',
                "desc_estadoProrga"=>'Anulada',
                "mensaje"=>"El proceso se efectuó exitosamente"
            );
        }else{
            $retornar = array("reg_afectado"=>"");
            $retornar["mensaje"] = "El proceso no se efectuó. Ocurrió un error de conexión";
        }
        echo json_encode($retornar);
    }
    /**
     * Ejecuta la anulación de una prórroga en preparación ó revisada
     */
    public function metRevisarProrroga(){
        $arrParams=$this->atParams;
        $arrParams["atIdEmpleadoUser"]=$this->atIdEmpleadoUser;
        $arrParams["fecha_actual"] = date('Y-m-d H:i:s');
        $result=$this->atProrrogaModelo->metProcesaRevision($arrParams);
        if($result){
            $nombrePersEjecuta=$this->atFuncGnrles->metBuscaUsuario(null, $this->atIdEmpleadoUser);
            $retornar = array(
                "reg_afectado"=>true,
                "fechaProceso"=>date('d-m-Y'),"nombrePersEjecuta"=>$nombrePersEjecuta,
                "estado_prorroga"=>'RV',
                "desc_estadoProrga"=>'Revisada',
                "mensaje"=>"El proceso se efectuó exitosamente"
            );
        }else{
            $retornar = array("reg_afectado"=>"");
            $retornar["mensaje"] = "El proceso no se efectuó. Ocurrió un error de conexión";
        }
        echo json_encode($retornar);
    }
    /**
     * Ejecuta la aprobación de una prórroga en preparación ó revisada
     */
    public function metAprobarProrroga(){
        $arrParams=$this->atParams;
        $arrParams["atIdEmpleadoUser"]=$this->atIdEmpleadoUser;
        $arrParams["fecha_actual"] = date('Y-m-d H:i:s');
        //Se optienen las actividades reprogramadas.
        $arrActvidades=$this->metRecalcularActividades(true);
        //Se formatéan las fecha a Mysql.
        foreach($arrActvidades as $fila){
            $fila['fecha_inicialReal']=$this->atFuncGnrles->metFormateaFechaMsql($fila['fecha_inicialReal']);
            $fila['fecha_fin_actividadReal']=$this->atFuncGnrles->metFormateaFechaMsql($fila['fecha_fin_actividadReal']);
            if($fila['idActividaPlanif']==$arrParams["idActividadPlanif"]){
                $fila['cantDiasProrroga']=$arrParams["cantDiasProrroga"];//Se asigna la cantidad días prórroga
            }
            $arrDataActividades[]=$fila;
        }
        //Se asigna el array de actividades al array principal de datos.
        $arrParams["setActividades"]=$arrDataActividades;
        //Se procesa la aprobación.
        $result=$this->atProrrogaModelo->metProcesaAprobacion($arrParams);
        if($result){
            //Se extrae el nombre de la persona que procesa.
            $nombrePersEjecuta=$this->atFuncGnrles->metBuscaUsuario(null, $this->atIdEmpleadoUser);
            $retornar = array(
                "reg_afectado"=>true,
                "fechaProceso"=>date('d-m-Y'),"nombrePersEjecuta"=>$nombrePersEjecuta,
                "estado_prorroga"=>'AP',
                "desc_estadoProrga"=>'Aprobada',
                "mensaje"=>"El proceso se efectuó exitosamente"
            );
        }else{
            $retornar = array("reg_afectado"=>"");
            $retornar["mensaje"] = "El proceso no se efectuó. Ocurrió un error de conexión";
        }
        echo json_encode($retornar);
    }
}
?>