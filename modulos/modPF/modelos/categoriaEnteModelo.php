<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación fiscal
 * PROCESO: Ingreso y mantenimiento de categorías de entes.
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        26-01-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class categoriaEnteModelo extends Modelo{
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }
    /**
     * Validación para el ingreso y actualización de una categoria
     * @param string $case
     * @param array $params
     * @return array
     */
    function metConsultaCategorias($case, $params=NULL){
        $sql_criterio="";$criterio=false;$tabla="a038_categoria_ente";$campos="*";$Union="";
        switch($case){
            case "por_listado":
                $Union=" INNER JOIN a006_miscelaneo_detalle ON a038_categoria_ente.fk_a006_miscdet_tipoente = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle";
                $campos="pk_num_categoria_ente,ind_categoria_ente,ind_nombre_detalle AS nombre_tipoente";
                if($params['cboxp_tipoente']){
                    $sql_criterio=($criterio)? " AND ": " WHERE ";$criterio=true;
                    $sql_criterio.="fk_a006_miscdet_tipoente=".$params['cboxp_tipoente'];
                }
                $sql_criterio .= " ORDER BY ind_nombre_detalle,ind_categoria_ente ASC";
                break;
            case "valida_ingreso":
                $sql_criterio = " WHERE fk_a006_miscdet_tipoente=".$params["cbox_tipoente"]." AND ind_categoria_ente='".$params["txt_categoria"]."'";
                break;
            case "valida_modificar":
                $sql_criterio = " WHERE pk_num_categoria_ente!=".$params['idCategoria']." AND fk_a006_miscdet_tipoente=".$params["cbox_tipoente"]." AND ind_categoria_ente='".$params['txt_categoria']."'";
                break;
        }

        $sql_query ="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca la categoría a montar en el form para modificar
     * @param $idCategoria
     * @return mixed
     */
    public function metMostrarCategoria($idCategoria){
        $result = $this->_db->query("
          SELECT a038.*,DATE_FORMAT(a038.fec_ultima_modificacion, '%d-%m-%Y %T') AS fecha_modificacion, a018.ind_usuario AS usuario_mod FROM a038_categoria_ente a038
          INNER JOIN a018_seguridad_usuario a018 ON a038.fk_a018_num_seg_usermod = a018.pk_num_seguridad_usuario
          WHERE pk_num_categoria_ente='$idCategoria'");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Ingreso de una categoría
     */
    public function metIngresaCategoria($params){
        $arrValores=array(
            'fk_a006_miscdet_tipoente'=>$params["cbox_tipoente"],
            'ind_categoria_ente'=>$params["txt_categoria"],
            'fk_a018_num_seg_usermod'=>$this->atIdUsuario,
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s')
        );
        $this->_db->beginTransaction();
        $nuevoRegistro=$this->_db->prepare("
            INSERT INTO a038_categoria_ente SET
            fk_a006_miscdet_tipoente=:fk_a006_miscdet_tipoente,
            ind_categoria_ente=:ind_categoria_ente,
            fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
            fec_ultima_modificacion=:fec_ultima_modificacion
        ");
        $nuevoRegistro->execute($arrValores);
        $idRegistro=$this->_db->lastInsertId();
        $error = $nuevoRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$idRegistro=0;
        }else{
            $this->_db->commit();
        }
        return $idRegistro;
    }
    /**
     * Actualizar una categoría
     */
    public function metModificarCategoria($params){
        $arrValores=array(
            'fk_a006_miscdet_tipoente'=>$params["cbox_tipoente"],
            'ind_categoria_ente'=>$params['txt_categoria'],
            'fk_a018_num_seg_usermod'=>$this->atIdUsuario,
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s'),
            'pk_num_categoria_ente'=>$params['idCategoria']
        );
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("
            UPDATE a038_categoria_ente SET
                fk_a006_miscdet_tipoente=:fk_a006_miscdet_tipoente,
                ind_categoria_ente=:ind_categoria_ente,
                fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
                fec_ultima_modificacion=:fec_ultima_modificacion
            WHERE pk_num_categoria_ente=:pk_num_categoria_ente");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado=0;
        }else{
            $this->_db->commit();$reg_afectado=1;
        }
        return $reg_afectado;
    }
    /**
     * Elimina una categoría siempre y cuando no esté relacionado.
     * @param $idCategoria
     * @return array|bool
     */
    public function metEliminaCategoria($idCategoria){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM a038_categoria_ente WHERE pk_num_categoria_ente=:pk_num_categoria_ente");
        $sql_query->execute(array('pk_num_categoria_ente'=>$idCategoria));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            $reg_afectado=$error;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}
?>