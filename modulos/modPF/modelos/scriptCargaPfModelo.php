<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación Fiscal
 * PROCESO: Carga de data básica
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Keisy Lòpez                      |      lopez.keisy@cmldc.gob.ve      |                                |
 * | 2 |          José Díaz                        |                                    |                                |
 * | 3 |          Alexis Ontiveros                 |    ontiveros.alexis@cmldc.gob.ve   |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-05-2016       |         1.0        |
 * |               #3                      |        05-07-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************************************************************/
class scriptCargaPfModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }
    public function metBusquedaSimple($campos, $tabla, $innerJoin, $where){
        $sqlQuery = $this->_db->query("
            SELECT
              $campos
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $sqlQuery->fetch();
    }
    public function metCargarProcesosFiscales($arraysData){
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO pf_b002_proceso SET
                cod_proceso=:cod_proceso,
                txt_descripcion_proceso=:txt_descripcion_proceso,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            "
        );
        foreach ($arraysData as $array){
            $validaReg = $this->metBusquedaSimple('cod_proceso','pf_b002_proceso', false, "cod_proceso = '" . $array['cod_proceso'] . "'");
            if (!$validaReg) {
                $registro->execute(array(
                    ':cod_proceso'=>$array['cod_proceso'],
                    ':txt_descripcion_proceso'=>mb_strtoupper($array['txt_descripcion_proceso'], 'utf-8'),
                    ':num_estatus'=>$array['num_estatus'],
                    ':fec_ultima_modificacion'=>date('Y-m-d H:i:s'),
                    ':fk_a018_num_seguridad_usuario'=>$array['fk_a018_num_seguridad_usuario']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
        } else {
            $this->_db->commit();
        }
    }
    public function metCargarFasesProceso($arraysData){
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO pf_b003_fase SET
                 fk_pfb002_num_proceso=:fk_pfb002_num_proceso,
                 cod_fase=:cod_fase,
                 txt_descripcion_fase=:txt_descripcion_fase,
                 num_estatus=:num_estatus,
                 fec_ultima_modificacion=:fec_ultima_modificacion,
                 fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            "
        );
        foreach ($arraysData as $array) {
            $validaReg = $this->metBusquedaSimple('cod_fase','pf_b003_fase', false, "cod_fase = '" . $array['cod_fase'] . "'");
            if (!$validaReg) {
                $criterio="cod_proceso = '".$array['cod_proceso']."'";
                $arrData = $this->metBusquedaSimple('pk_num_proceso','pf_b002_proceso', false, $criterio);
                if($arrData) {
                    $registro->execute(array(
                        ':fk_pfb002_num_proceso' => $arrData['pk_num_proceso'],
                        ':cod_fase' => $array['cod_fase'],
                        ':txt_descripcion_fase' => mb_strtoupper($array['txt_descripcion_fase'], 'utf-8'),
                        ':num_estatus' => $array['num_estatus'],
                        ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
                        ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                    ));
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
        } else {
            $this->_db->commit();
        }
    }
    public function metCargarActividadesFase($arraysData){
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO pf_b004_actividad SET
                fk_pfb003_num_fase=:fk_pfb003_num_fase,
                cod_actividad=:cod_actividad,
                txt_descripcion_actividad=:txt_descripcion_actividad,
                txt_comentarios_actividad=:txt_comentarios_actividad,
                num_duracion_actividad=:num_duracion_actividad,
                ind_afecto_plan=:ind_afecto_plan,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
        ");
        foreach ($arraysData as $array) {
            $validaReg = $this->metBusquedaSimple('cod_actividad','pf_b004_actividad', false, "cod_actividad = '" . $array['cod_actividad'] . "'");
            if (!$validaReg) {
                $criterio="cod_fase = '".$array['cod_fase']."'";
                $arrData = $this->metBusquedaSimple('pk_num_fase','pf_b003_fase', false, $criterio);
                if($arrData) {
                    $registro->execute(array(
                        ':fk_pfb003_num_fase' => $arrData['pk_num_fase'],
                        ':cod_actividad' => $array['cod_actividad'],
                        ':txt_descripcion_actividad' => mb_strtoupper($array['txt_descripcion_actividad'], 'utf-8'),
                        ':txt_comentarios_actividad' => $array['txt_comentarios_actividad'],
                        ':num_duracion_actividad' => $array['num_duracion_actividad'],
                        ':ind_afecto_plan' => $array['ind_afecto_plan'],
                        ':num_estatus' => $array['num_estatus'],
                        ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
                        ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                    ));
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
        } else {
            $this->_db->commit();
        }
    }
    public function metCargarTipoDecisiones($arraysData){
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare(
            "INSERT INTO pf_c007_tipo_decision SET
                pk_num_tipodecision=:pk_num_tipodecision,
                ind_tipodecision=:ind_tipodecision,
                flag_num_campo=:flag_num_campo,
                fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
                fec_ultima_modific_decision=:fec_ultima_modific_decision
        ");
        foreach ($arraysData as $array){
            $criterio="pk_num_tipodecision = ".$array['pk_num_tipodecision'];
            $validaReg = $this->metBusquedaSimple('*','pf_c007_tipo_decision', false, $criterio);
            if (!$validaReg) {
                $sql_query->execute(array(
                    ':pk_num_tipodecision' => $array['pk_num_tipodecision'],
                    ':ind_tipodecision' => $array['ind_tipodecision'],
                    ':flag_num_campo' => $array['flag_num_campo'],
                    ':fk_a018_num_seg_usuario_mod' => $array['fk_a018_num_seg_usuario_mod'],
                    ':fec_ultima_modific_decision' => date('Y-m-d H:i:s')
                ));
            }else{
                $sql_query=$this->_db->prepare("
                    UPDATE pf_c007_tipo_decision SET
                        ind_tipodecision=:ind_tipodecision
                    WHERE pk_num_tipodecision=:pk_num_tipodecision
                ");
                $sql_query->execute(array(
                    ':ind_tipodecision' => $array['ind_tipodecision'],
                    ':pk_num_tipodecision'=>$validaReg["pk_num_tipodecision"])
                );
            }
        }
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
        } else {
            $this->_db->commit();
        }
    }
    //Nota: Los tipos de ente deben estar cargados en misceláneos.
    public function metCategoriasDeEntes($arraysData){
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO a038_categoria_ente SET
                fk_a006_miscdet_tipoente=:fk_a006_miscdet_tipoente,
                ind_categoria_ente=:ind_categoria_ente,
                fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
                fec_ultima_modificacion=:fec_ultima_modificacion
            "
        );
        foreach ($arraysData as $array){
            $criterio="ind_categoria_ente = '".$array['ind_categoria_ente']."'";
            $validaReg = $this->metBusquedaSimple('ind_categoria_ente','a038_categoria_ente', false, $criterio);
            if (!$validaReg) { //Se busca el id misceláneos detalle correspondiente al tipo de ente
                $Union="INNER JOIN a006_miscelaneo_detalle a006 ON a005.pk_num_miscelaneo_maestro = a006.fk_a005_num_miscelaneo_maestro ";
                $criterio="cod_maestro = 'PFTIPOENTE' AND ind_nombre_detalle = '".$array['tipo_ente']."'";
                $arrData = $this->metBusquedaSimple('pk_num_miscelaneo_detalle','a005_miscelaneo_maestro a005', $Union, $criterio);
                if($arrData) {
                    $registro->execute(array(
                        ':fk_a006_miscdet_tipoente' => $arrData['pk_num_miscelaneo_detalle'],
                        ':ind_categoria_ente' => $array['ind_categoria_ente'],
                        ':fk_a018_num_seg_usermod' => $array['fk_a018_num_seg_usermod'],
                        ':fec_ultima_modificacion' => date('Y-m-d H:i:s')
                    ));
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
        } else {
            $this->_db->commit();
        }
    }
}