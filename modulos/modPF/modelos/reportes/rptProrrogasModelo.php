<?php
/***********************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Reportes de Prórrogas de actividades.
 * DESCRIPCIÓN: Permite la extración de datos para la consulta y reportes en pdf de las planificaciones cuyas
 *              actividades contienen prórrogas en sus diferentes proceso fiscales: Actuación fiscal,
 *              Valoración preliminar, Potestad investigativa,Valoración jurídica y Procedimientos administrativos.
 * PROGRAMADORES.______________________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |___________________________________________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-08-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 **********************************************************************************************************************/
class rptProrrogasModelo extends Modelo{
    public function metConsultaPlanificacion($params){
        $tabla="pf_b001_planificacion_fiscal tb_planificacion";$sql_criterio="";$criterio=false;
        $campos="tb_planificacion.*,fk_a039_num_ente, tb_tipoactuacion.ind_nombre_detalle AS nombre_tipoactuacion, tb_origenactuacion.ind_nombre_detalle AS nombre_origenactuacion";
        $Union=" INNER JOIN pf_c001_actividad_planific ON tb_planificacion.pk_num_planificacion = pf_c001_actividad_planific.fk_pfb001_num_planificacion";
        $Union.=" INNER JOIN pf_c003_prorroga_actividad ON pf_c001_actividad_planific.pk_num_actividad_planific = pf_c003_prorroga_actividad.fk_pfc001_num_actividad_planific";
        $Union.=" INNER JOIN a041_persona_ente ON tb_planificacion.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente";
        $Union.=" INNER JOIN a006_miscelaneo_detalle tb_tipoactuacion ON tb_planificacion.fk_a006_num_miscdet_tipoactuacion = tb_tipoactuacion.pk_num_miscelaneo_detalle";
        $Union.=" INNER JOIN a006_miscelaneo_detalle tb_origenactuacion ON tb_planificacion.fk_a006_num_miscdet_origenactuacion = tb_origenactuacion.pk_num_miscelaneo_detalle";
        if($params['idTipoProceso']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_b002_num_proceso=".$params['idTipoProceso'];
        }
        if($params['idPlanificacion']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.pk_num_planificacion=".$params['idPlanificacion'];
        }else{
            if($params['cbox_codPlanificacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.cod_planificacion='".$params['cbox_codPlanificacion']."'";
            }
            if($params['id_contraloria']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a001_num_organismo='".$params['id_contraloria']."'";
            }
            if($params['id_dependencia']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a004_num_dependencia=".$params['id_dependencia'];
            }
            if($params['cbox_yearplan']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";
                $sql_criterio.="YEAR(tb_planificacion.fec_inicio)='".$params['cbox_yearplan']."'";
            }
        }
        $sql_criterio.=" GROUP BY tb_planificacion.pk_num_planificacion ";
        $sql_criterio.=" ORDER BY cod_planificacion ASC";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    /**
     * Extrae los códigos de planificaciones cuyas actividades contengan prórrogas
     * @param $params
     * @return array
     */
    public function metBuscaCodigosPlan($params){
        $result=$this->_db->query("
            SELECT DISTINCT(cod_planificacion) AS nro_planificacion
            FROM pf_c003_prorroga_actividad
                INNER JOIN pf_c001_actividad_planific ON pf_c003_prorroga_actividad.fk_pfc001_num_actividad_planific=pf_c001_actividad_planific.pk_num_actividad_planific
                INNER JOIN pf_b001_planificacion_fiscal ON pf_c001_actividad_planific.fk_pfb001_num_planificacion=pf_b001_planificacion_fiscal.pk_num_planificacion
            WHERE fk_b002_num_proceso=".$params['idTipoProceso']." AND fk_a001_num_organismo=".$params['idContraloria']."
                AND fk_a004_num_dependencia=".$params['idDependencia']." AND YEAR(fec_inicio)='".$params['yearPlanificacion']."'"
        );
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    /**
     * Extrea las prórrogas y su actividad correspondiente
     * @param $idPlanificacion
     * @return array
     */
    public function metBuscaProrrogas($idPlanificacion){
        $result=$this->_db->query("
                    SELECT pf_c003_prorroga_actividad.num_dias_prorroga AS cant_diasprorroga,ind_motivo AS motivo_prorroga,ind_estado AS estado_prorroga,
                      txt_descripcion_actividad AS desc_actividad, num_dias_duracion_actividad AS lapso_actividad
                    FROM pf_c003_prorroga_actividad
                        INNER JOIN pf_c001_actividad_planific ON pf_c003_prorroga_actividad.fk_pfc001_num_actividad_planific=pf_c001_actividad_planific.pk_num_actividad_planific
                        INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad
                        INNER JOIN pf_b001_planificacion_fiscal ON pf_c001_actividad_planific.fk_pfb001_num_planificacion=pf_b001_planificacion_fiscal.pk_num_planificacion
                    WHERE pk_num_planificacion=".$idPlanificacion
        );
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    /**
     * Extrae el nombre de un proceso fiscal determinado
     * @param $idProceso
     * @return mixed
     */
    public function metProcesoFiscal($idProceso){
        $result=$this->_db->query("SELECT txt_descripcion_proceso as nombre_proceso FROM pf_b002_proceso WHERE pk_num_proceso=".$idProceso);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
}
?>