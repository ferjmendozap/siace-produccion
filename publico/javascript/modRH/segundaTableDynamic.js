(function(namespace, $) {
	"use strict";

	var DemoTableDynamic2 = function() {
		// Create reference to this instance
		var o2 = this;
		// Initialize app when document is ready
		$(document).ready(function() {
			o2.initialize();
		});

	};
	var p2 = DemoTableDynamic2.prototype;

	// =========================================================================
	// INIT
	// =========================================================================

	p2.initialize = function() {
		this._initDataTables();
	};

	// =========================================================================
	// DATATABLES
	// =========================================================================

	p2._initDataTables = function() {
		if (!$.isFunction($.fn.dataTable)) {
			return;
		}

		// Init the demo DataTables
		this._createDataTable2();
	};

	p2._createDataTable2 = function() {
		$('#datatable2').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columnas",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": 'Mostrar _MENU_',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
	};


	// =========================================================================
	// DETAILS
	// =========================================================================

	p2._formatDetails = function(d) {
		// `d` is the original data object for the row
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
				'<tr>' +
				'<td>Full name:</td>' +
				'<td>' + d.name + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>Extension number:</td>' +
				'<td>' + d.extn + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>Extra info:</td>' +
				'<td>And any further details here (images etc)...</td>' +
				'</tr>' +
				'</table>';
	};

	// =========================================================================
	namespace.DemoTableDynamic2 = new DemoTableDynamic2;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
