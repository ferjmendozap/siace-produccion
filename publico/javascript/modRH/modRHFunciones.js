/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *
 *  * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        15-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/
var ModRhFunciones = function(){
    var f = this;


    /***
     * Descripcion: Json Select Centros de Costo
     * @param url
     * @param idDependencia
     */
    f.metJsonCentroCosto = function(url,idDependencia,idCentroCosto){
        function json(id){
            $('#fk_a023_num_centro_costo').html('');
            $('#fk_a023_num_centro_costo').append('<option value="">Seleccione Centro de Costo</option>');
            if(!id){
                id = $('#fk_a004_num_dependencia').val();
            }else{
                id = idDependencia;
            }
            $.post(url,{ idDependencia: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_centro_costo']==idCentroCosto){
                        $('#fk_a023_num_centro_costo').append('<option selected value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    }else{
                        $('#fk_a023_num_centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    }
                }
            },'json');
        }
        if(idDependencia){
            json(idDependencia);
            $('#fk_a004_num_dependencia').change(function(){
                json(false);

            });
        }else{
            $('#fk_a004_num_dependencia').change(function(){
                json(false);

            });
        }
    }


    /***
     * Descripcion: Json Select
     * @param url
     * @param idPais
     * @param idEstado
     */
    f.metJsonEstadoD = function(url,idPais,idEstado){
        function json(id){
            $('#fk_a009_num_estado2').html('');
            $('#fk_a009_num_estado2').append('<option value="">Seleccione el Estado</option>');
            $('#s2id_fk_a009_num_estado2 .select2-chosen').html('Seleccione el Estado');
            if(!id){
                id = $('#fk_a008_num_pais2').val();
            }else{
                id = idPais;

            }
            $.post(url,{ idPais: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_estado']==idEstado){
                        $('#fk_a009_num_estado2').append('<option selected value="'+dato[i]['pk_num_estado']+'">'+dato[i]['ind_estado']+'</option>');
                        $('#s2id_fk_a009_num_estado2 .select2-chosen').html(dato[i]['ind_estado']);
                    }else{
                        $('#fk_a009_num_estado2').append('<option value="'+dato[i]['pk_num_estado']+'">'+dato[i]['ind_estado']+'</option>');
                    }
                }
            },'json');
        }
        if(idPais){
            json(idPais);
            $('#fk_a008_num_pais2').change(function(){
                json(false);
                $('#fk_a011_num_municipio2').html('');
                $('#fk_a010_num_ciudad2').html('');
            });
        }else{
            $('#fk_a008_num_pais2').change(function(){
                json(false);
                $('#fk_a011_num_municipio2').html('');
                $('#fk_a010_num_ciudad2').html('');
            });
        }
    }

    /***
     * Descripcion: Json de Ciudad
     * @param url
     * @param idEstado
     * @param idCiudad
     */
    f.metJsonCiudadD = function(url,idEstado,idCiudad){
        function json(id){
            $('#fk_a010_num_ciudad2').html('');
            $('#fk_a010_num_ciudad2').append('<option value="">Seleccione la Ciudad</option>');
            $('#s2id_fk_a010_num_ciudad2 .select2-chosen').html('Seleccione la Ciudad');
            if(!id){
                id = $('#fk_a009_num_estado2').val();
            }else{
                id = idEstado;
            }
            $.post(url,{ idEstado: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_ciudad']==idCiudad){
                        $('#fk_a010_num_ciudad2').append('<option selected value="'+dato[i]['pk_num_ciudad']+'">'+dato[i]['ind_ciudad']+'</option>');
                        $('#s2id_fk_a010_num_ciudad2 .select2-chosen').html(dato[i]['ind_ciudad']);
                    }else{
                        $('#fk_a010_num_ciudad2').append('<option value="'+dato[i]['pk_num_ciudad']+'">'+dato[i]['ind_ciudad']+'</option>');
                    }
                }
            },'json');
        }
        if(idEstado){
            json(idEstado);
            $('#fk_a009_num_estado2').change(function(){
                json(false);
            });
        }else{
            $('#fk_a009_num_estado2').change(function(){
                json(false);
            });
        }
    }


    /***
     * Descripcion: Json de Ciudad
     * @param url
     * @param idEstado
     * @param idCiudad
     */
    f.metJsonCiudadN = function(url,idEstado,idCiudad){
        function json(id){
            $('#fk_a010_num_ciudad').html('');
            $('#fk_a010_num_ciudad').append('<option value="">Seleccione la Ciudad</option>');
            $('#s2id_fk_a010_num_ciudad .select2-chosen').html('Seleccione la Ciudad');
            if(!id){
                id = $('#fk_a009_num_estado').val();
            }else{
                id = idEstado;
            }
            $.post(url,{ idEstado: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_ciudad']==idCiudad){
                        $('#fk_a010_num_ciudad').append('<option selected value="'+dato[i]['pk_num_ciudad']+'">'+dato[i]['ind_ciudad']+'</option>');
                        $('#s2id_fk_a010_num_ciudad .select2-chosen').html(dato[i]['ind_ciudad']);
                    }else{
                        $('#fk_a010_num_ciudad').append('<option value="'+dato[i]['pk_num_ciudad']+'">'+dato[i]['ind_ciudad']+'</option>');
                    }
                }
            },'json');
        }
        if(idEstado){
            json(idEstado);
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }else{
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }
    }

    /***
     * Descripcion: Json de Municipio
     * @param url
     * @param idEstado
     * @param idMunicipio
     */
    f.metJsonMunicipioN = function(url,idEstado,idMunicipio){
        function json(id){
            $('#fk_a011_num_municipio').html('');
            $('#fk_a011_num_municipio').append('<option value="">Seleccione el Municipio</option>');
            $('#s2id_fk_a011_num_municipio .select2-chosen').html('Seleccione el Municipio');
            if(!id){
                id = $('#fk_a009_num_estado').val();
            }else{
                id = idEstado;
            }

            $.post(url,{ idEstado: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_municipio']==idMunicipio){
                        $('#fk_a011_num_municipio').append('<option selected value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
                        $('#s2id_fk_a011_num_municipio2 .select2-chosen').html(dato[i]['ind_municipio']);
                    }else{
                        $('#fk_a011_num_municipio').append('<option value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
                    }
                }
            },'json');
        }
        if(idEstado){
            json(idEstado);
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }else{
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }
    }

    /***
     * Descripcion: Json de Municipio
     * @param url
     * @param idEstado
     * @param idMunicipio
     */
    f.metJsonMunicipioD = function(url,idEstado,idMunicipio){
        function json(id){
            $('#fk_a011_num_municipio2').html('');
            $('#fk_a011_num_municipio2').append('<option value="">Seleccione el Municipio</option>');
            $('#s2id_fk_a011_num_municipio2 .select2-chosen').html('Seleccione el Municipio');
            if(!id){
                id = $('#fk_a009_num_estado2').val();
            }else{
                id = idEstado;
            }

            $.post(url,{ idEstado: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_municipio']==idMunicipio){
                        $('#fk_a011_num_municipio2').append('<option selected value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
                        $('#s2id_fk_a011_num_municipio2 .select2-chosen').html(dato[i]['ind_municipio']);
                    }else{
                        $('#fk_a011_num_municipio2').append('<option value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
                    }
                }
            },'json');
        }
        if(idEstado){
            json(idEstado);
            $('#fk_a009_num_estado2').change(function(){
                json(false);
            });
        }else{
            $('#fk_a009_num_estado2').change(function(){
                json(false);
            });
        }
    }

    /***
     * Descripcion: Json de Parroquia
     * @param url
     * @param idMunicipio
     * @param idParroquia
     */
    f.metJsonParroquiaD = function(url,idMunicipio,idParroquia){
        function json(id){
            $('#parroquia').html('');
            $('#parroquia').append('<option value="">Seleccione la Parroquia</option>');
            $('#s2id_parroquia .select2-chosen').html('Seleccione la Parroquia');
            if(!id){
                id = $('#fk_a011_num_municipio2').val();
            }else{
                id = idMunicipio;
            }

            $.post(url,{ idMunicipio: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_parroquia']==idParroquia){
                        $('#parroquia').append('<option selected value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                        $('#s2id_parroquia .select2-chosen').html(dato[i]['ind_parroquia']);
                    }else{
                        $('#parroquia').append('<option value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                    }
                }
            },'json');
        }
        if(idMunicipio){
            json(idMunicipio);
            $('#fk_a011_num_municipio2').change(function(){
                json(false);
            });
        }else{
            $('#fk_a011_num_municipio2').change(function(){
                json(false);
            });
        }
    }

    /***
     * Descripcion: Json de Sector
     * @param url
     * @param idParroquia
     * @param idSector
     */
    f.metJsonSectorD = function(url,idParroquia,idSector){
        function json(id){
            $('#fk_a013_num_sector').html('');
            $('#fk_a013_num_sector').append('<option value="">Seleccione el Sector</option>');
            $('#s2id_fk_a013_num_sector .select2-chosen').html('Seleccione el Sectors');
            if(!id){
                id = $('#parroquia').val();
            }else{
                id = idParroquia;
            }

            $.post(url,{ idParroquia: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_sector']==idSector){
                        $('#fk_a013_num_sector').append('<option selected value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                        $('#s2id_fk_a013_num_sector .select2-chosen').html(dato[i]['ind_sector']);
                    }else{
                        $('#fk_a013_num_sector').append('<option value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                    }
                }
            },'json');
        }
        if(idParroquia){
            json(idParroquia);
            $('#parroquia').change(function(){
                json(false);
            });
        }else{
            $('#parroquia').change(function(){
                json(false);
            });
        }
    }


    /***
     * Descripcion: Json Serie Ocupacional
     * @param url
     * @param idGrupoOcupacional
     * @param idSerieOcupacional
     */
    f.metJsonSerieOcupacional= function(url,idGrupoOcupacional,idSerieOcupacional){
        function json(id){
            $('#fk_rhc010_num_serie').html('');
            $('#fk_rhc010_num_serie').append('<option value="">Seleccione Serie Ocupacional</option>');
            $('#s2id_fk_rhc010_num_serie .select2-chosen').html('Seleccione Serie Ocupacional');
            if(!id){
                id = $('#fk_a006_num_miscelaneo_detalle_grupoocupacional').val();
            }else{
                id = idGrupoOcupacional;
            }

            $.post(url,{ idGrupoOcupacional: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_serie']==idSerieOcupacional){
                        $('#fk_rhc010_num_serie').append('<option selected value="'+dato[i]['pk_num_serie']+'">'+dato[i]['ind_nombre_serie']+'</option>');
                        $('#s2id_fk_a011_num_municipio2 .select2-chosen').html(dato[i]['ind_nombre_serie']);
                    }else{
                        $('#fk_rhc010_num_serie').append('<option value="'+dato[i]['pk_num_serie']+'">'+dato[i]['ind_nombre_serie']+'</option>');
                    }
                }
            },'json');
        }
        if(idGrupoOcupacional){
            json(idGrupoOcupacional);
            $('#fk_a006_num_miscelaneo_detalle_grupoocupacional').change(function(){
                json(false);
            });
        }else{
            $('#fk_a006_num_miscelaneo_detalle_grupoocupacional').change(function(){
                json(false);
            });
        }
    }


    /***
     * Descripcion: Json Cargos
     * @param url
     * @param idGrupoOcupacional
     * @param idSerieOcupacional
     * @param idCargo
     */
    f.metJsonCargos= function(url,idGrupoOcupacional,idSerieOcupacional,idCargo){
        function json(id,id2){
            $('#fk_rhc063_num_puestos_cargo').html('');
            $('#fk_rhc063_num_puestos_cargo').append('<option value="">Seleccione Cargo</option>');
            $('#s2id_fk_rhc063_num_puestos_cargo .select2-chosen').html('Seleccione Cargo');
            if(!id && !id2){
                id = $('#fk_a006_num_miscelaneo_detalle_grupoocupacional').val();
                id2 = $('#fk_rhc010_num_serie').val();
            }else{
                id = idGrupoOcupacional;
                id2 = idSerieOcupacional
            }

            $.post(url,{ idGrupoOcupacional: id, idSerieOcupacional: id2 } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_puestos']==idCargo){
                        $('#fk_rhc063_num_puestos_cargo').append('<option selected value="'+dato[i]['pk_num_puestos']+'">'+dato[i]['ind_descripcion_cargo']+'</option>');
                        $('#s2id_fk_rhc063_num_puestos_cargo .select2-chosen').html(dato[i]['ind_descripcion_cargo']);
                    }else{
                        $('#fk_rhc063_num_puestos_cargo').append('<option value="'+dato[i]['pk_num_puestos']+'">'+dato[i]['ind_descripcion_cargo']+'</option>');
                    }
                }
            },'json');
        }
        if(idGrupoOcupacional && idSerieOcupacional){
            json(idGrupoOcupacional,idSerieOcupacional);
            $('#fk_rhc010_num_serie').change(function(){
                json(false,false);
            });
        }else{
            $('#fk_rhc010_num_serie').change(function(){
                json(false,false);
            });
        }
    }

    /***
     * Descripcion: Burcar Persona
     * @param url
     * @param cedula
     */
    f.metBuscarPersona= function(url,cedula,id_persona){

        /*VERIFICO CUAL ES EL PARAMETRO ENVIADO PARA DETERMINAR QUE TIPO DE BUSQUEDA HACER*/

        if(!cedula){
            //BUSQUEDA POR CODIGO PERSONA
            $.post(url,{ parametro: id_persona, busqueda: 'PorIdPersona' } ,function(dato){

            },'json');

        }else{
            //BUSQUEDA POR CEDULA
            //URL: modRH/gestion/empleadosCONTROL/BuscarPersonaMET
            $.post(url,{ parametro: cedula, busqueda: 'PorCedula' } ,function(dato){

                if(dato==false){

                    $("#persona").prop("disabled",true);
                    $("#empleado").prop("disabled",true);
                    $("#NombCompleto").val('');
                    $("#formAjax")[0].reset();
                    $("#apellido1").focus();
                    $("#cedula").val(cedula);
                    $("#accion").show();
                    $(".form-control").removeAttr("readonly");

                    var DocFiscal = new String();
                    DocFiscal = $("#cedula").val().trim();
                    $("#doc_fiscal").val(DocFiscal+"-");


                }else{

                    if(dato['pk_num_empleado']>0){
                        //existe empleado
                        $(".form-control").attr("readonly","readonly");//desabilito el formulario
                        $("#cedula").removeAttr("readonly");//habilito nada mas que el campo cedula
                        $("#accion").hide();//oculto el boton de guardar
                        $("#siguiente").addClass('disabled');
                        $("#ultimo").addClass('disabled');

                    }else{
                        //existe la persona pero no es empleado
                        $("#accion").show();//muestro el boton de guardar
                        $(".form-control").removeAttr("readonly");//habilito todo el formulario si esta disabled

                    }

                    if(dato['fec_nacimiento']==null){
                        var fn = '00-00-0000';
                    }
                    else{
                        var fn = dato['fec_nacimiento'].split("-");
                    }

                    $("#empleado").val(dato['pk_num_empleado']);
                    $("#persona").val(dato['pk_num_persona']);
                    $("#nombre1").val(dato['ind_nombre1']);
                    $("#nombre2").val(dato['ind_nombre2']);
                    $("#apellido1").val(dato['ind_apellido1']);
                    $("#apellido2").val(dato['ind_apellido2']);
                    $("#NombCompleto").val(dato['ind_nombre1']+" "+dato['ind_nombre2']+" "+dato['ind_apellido1']+" "+dato['ind_apellido2']);
                    $("#fk_a006_num_miscelaneo_detalle_sexo").val(dato['fk_a006_num_miscelaneo_detalle_sexo']);
                    if(dato['ind_estatus_persona']==1){
                        $("#ind_estatus_persona").prop('checked', true);
                    }else{
                        $("#ind_estatus_persona").prop('checked', false);
                    }
                    if(dato['ind_estatus']==1){
                        $("#ind_estatus").prop('checked', true);
                    }else{
                        $("#ind_estatus").prop('checked', false);
                    }
                    $("#fk_a006_num_miscelaneo_detalle_nacionalidad").val(dato['fk_a006_num_miscelaneo_detalle_nacionalidad']);
                    $("#doc_fiscal").val(dato['ind_documento_fiscal']);
                    $("#email").val(dato['ind_email']);
                    $("#fechaNacimiento").val(fn[2]+"-"+fn[1]+"-"+fn[0]);
                    $("#lugarNacimiento").val(dato['ind_lugar_nacimiento']);
                    $("#fk_a010_num_ciudad").val(dato['fk_a010_num_ciudad']);
                    $("#fk_a013_num_sector").val(dato['fk_a013_num_sector']);


                }



            },'json');

        }


    }

}