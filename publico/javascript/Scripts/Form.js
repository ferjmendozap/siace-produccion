
$(document).ready(function() {
    inicializar();
});


/**
 *  Inicializar
 *
 */
function inicializar() {
    if (typeof jQuery.fn.datepicker == 'function') {
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy" });
        $('.periodo').datepicker({
            format: "yyyy-mm",
            minViewMode: 1,
            maxViewMode: 2,
            language: "es"
        });
    }
    if (typeof jQuery.fn.mask == 'function') {
        $('.year').mask('0000');
        $('.periodo').mask('0000-00');
        $('.date').mask('00-00-0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00-00-0000 00:00:00');
        $('.periodo').mask('0000-000');
        $('.phone').mask('(0000)000-00-00');
        $('.money').mask("#.##0,00", {reverse: true});
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
    }
    if (typeof jQuery.fn.selectable == 'function') {
        $('table.seleccionable tbody').selectable({
            stop: function(event, ui) {
                var tbody = $(this);
                $(this).find('tr').each(function() {
                    if ($(this).hasClass('ui-selected')) {
                        $(this).addClass('success');
                        $(this).find('input[type="checkbox"].seleccionable').each(function() {
                            $(this).prop('checked', true);
                        });
                    }
                    else {
                        $(this).removeClass('success');
                        $(this).find('input[type="checkbox"].seleccionable').each(function() {
                            $(this).prop('checked', false);
                        });
                    }
                });
            }
        });
    }
    $('.datatable1').DataTable({
        //"scrollY": 200,
        //"scrollX": true,

        destroy: true,
        "dom": 'lCfrtip',
        "colVis": {
            "buttonText": "Columnas",
            "overlayFade": 0,
            "align": "right"
        },
        "language": {
            "lengthMenu": 'Mostrar _MENU_',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        },
        "order": [[ 0, "asc" ]]
    });
}

/**
 *  Cargar lista seleccionable
 *
 *  @param {object} boton
 *  @param {string} url de la lista
 *  @param {string} nombre de los inputs
 *  @param {string} datos adicionales
 */
function selector(boton, url, campos, data) {
    if (!data) var data = "";
    var j = 0;
    for(var i=0; i<campos.length; i++) {
        j = i + 1;
        data = data + "&" + "campo" + j + "=" + campos[i];
    }
    $('#formModalLabel2').html(boton.data('titulo'));
    $.post(url,data,function($dato){
        $('#ContenidoModal2').html($dato);
    });
}

/**
 *  Seleccionar de una lista
 *
 *  @param {array} campos
 *  @param {array} valores
 */
function seleccionar(campos, valores) {
    for(var i=0; i<campos.length; i++) {
        if ($("#" + campos[i]).length > 0) $("#" + campos[i]).val(valores[i]);
    }
    $('#cerrarModal2').click();
}

/**
 *  Cargar select dependiente
 *
 *  @param {string} url de la pagina a cargar
 *  @param {string} select a cargar
 *  @param {string} variables a enviar
 *  @param {string} nombres de los campos adicionales
 */
function select(url, selectDestino, data, destinos) {
    var option = "<option value=''>&nbsp;</option>";
    selectDestino.empty().append(option);

    if (destinos) {
        for(var i=0; i<destinos.length; i++) {
            var idSelectDestino = "#" + destinos[i];
            if ($(idSelectDestino).length > 0) $(idSelectDestino).empty().append(option);
        }
    }

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        cache: false,
        async: false,
        success: function (resp) {
            selectDestino.empty().append(resp);
        }
    });
}

/**
 *  Abrir modal
 *
 *  @param {string} controlador/metodo
 *  @param {object} boton
 */
function nuevo(url, boton) {
    if (!url) var url = $base + 'formMET';
    if (!boton) var boton = $("#bt_nuevo");

    $('#formModalLabel').html(boton.attr('titulo'));
    $.post(url, '', function($dato) {
        $('#ContenidoModal').html($dato);
    });
}

/**
 *  Abrir modal
 *
 *  @param {string} controlador/metodo
 *  @param {object} boton
 *  @param {boolean} indica si permite multiple seleccion
 *  @param {object} chekbox
 *  @param {object} form
 */
function editar(url, boton, multiple, checkbox, form) {
    if (!url) var url = $base + 'formMET/editar';
    if (!boton) var boton = $("#bt_editar");
    if (!multiple) var multiple = false;
    if (!checkbox) var checkbox = "idRegistro[]";
    if (!form) var form = $("form");
    validado = false;

    boton.attr('data-toggle','');

    if ($('input[name="'+checkbox+'"]:checkbox:checked').length == 0) swal("Error!", "Debe seleccionar un registro", "error");
    else if ($('input[name="'+checkbox+'"]:checkbox:checked').length > 1 && !multiple) swal("Error!", "Debe seleccionar un registro solamente", "error");
    else {
        boton.attr('data-toggle','modal');

        $('#formModalLabel').html(boton.attr('titulo'));
        if (!multiple) {
            var idRegistro = $('input[name="'+checkbox+'"]:checkbox:checked:eq(0)').val();
            $.post(url, { id: idRegistro }, function($dato) {
                $('#ContenidoModal').html($dato);
            });
        } else {
            $.post(url, form.serialize(), function($dato) {
                $('#ContenidoModal').html($dato);
            });
        }
    }
}

/**
 *  Abrir modal
 *
 *  @param {string} controlador/metodo
 *  @param {object} boton
 *  @param {object} chekbox
 */
function insertar(url, boton) {
    if (!url) var url = $base + 'insertarMET';
    if (!boton) var boton = $("#btInsertar");

    var tbody = $('#lista_' + boton.data('lista'));
    var numero = $('#lista_' + boton.data('lista') + ' tr').length + 1;

    $.post(url, "numero="+numero, function(dato) {
        tbody.append(dato);
        inicializar();
    }, '');
}

/**
 *  Abrir modal
 *
 *  @param {string} Titulo de la ventana
 *  @param {string} Texto a mostrar
 *  @param {string} Tipo de modal ()
 *  @param {string} controlador/metodo
 */
function mensaje(funct, title, text, type) {
    if (!title) var title = "Éxito";
    if (!text) var text = "El proceso se ha realizado exitosamente";
    if (!type) var type = "success";

    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: false
    }, function(){
        $('#cerrar').click();
        if (funct) eval(funct);
    });
}

/**
 *  Cargar página
 *
 *  @param {object} Formulario
 *  @param {string} Url de la página
 *  @param {string} Datos a enviar
 */
function cargarPagina(form, url, data) {
    if (!form) var form = $('form');
    if (!url) var url = form.attr('action');
    if (!data) var data = form.serialize();

    $.post(url, data, function(dato) {
        $('#_contenido').html(dato);
        if ($('[data-dismiss="offcanvas"]').length) $('[data-dismiss="offcanvas"]').click();
    });

    return false;
}

/**
 *  Cargar página
 *
 *  @param {string} Url de la página
 */
function cargarUrl(url) {
    $.post(url, {}, function(dato) {
        $('#_contenido').html(dato);
    });
}
