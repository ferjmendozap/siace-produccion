(function(namespace, $) {
	"use strict";
	var DataTablePf = function() {
		// Create reference to this instance
		var oPf = this;
		// Initialize app when document is ready
		$(document).ready(function() {
			oPf.initialize();
		});
	};
	var pPf = DataTablePf.prototype;
	// =========================================================================
	// INIT
	// =========================================================================

	pPf.initialize = function() {
		this._initDataTables();
	};
	// =========================================================================
	// DATATABLES
	// =========================================================================
	pPf._initDataTables = function() {
		if (!$.isFunction($.fn.dataTable)) {
			return;
		}

		// Init the demo DataTables
		this._createDataTablePf();
	};
	pPf._createDataTablePf = function() {
		$('#datatablePf').DataTable({
			"paginate": false,
			"sort": false,
			searching: false,
			"scrollY": "400px",
			"scrollCollapse": true,
			"paging": false
		});
	};
	// =========================================================================
	// DETAILS
	// =========================================================================
	pPf._formatDetails = function(d) {
		// `d` is the original data object for the row
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
				'<tr>' +
				'<td>Full name:</td>' +
				'<td>' + d.name + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>Extension number:</td>' +
				'<td>' + d.extn + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>Extra info:</td>' +
				'<td>And any further details here (images etc)...</td>' +
				'</tr>' +
				'</table>';
	};
	// =========================================================================
	namespace.DataTablePf = new DataTablePf;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
