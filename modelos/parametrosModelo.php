<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'aplicacionModelo.php';
class parametrosModelo extends aplicacionModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarParametros($status=false)
    {
        if($status){
            $status="WHERE a035_parametros.num_estatus='1'";
        }
        $parametros = $this->_db->query("
            SELECT
              a035_parametros.*,
              a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
              a035_parametros
            INNER JOIN a006_miscelaneo_detalle ON a035_parametros.fk_a006_num_miscelaneo_detalle = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            $status
        ");
        $parametros->setFetchMode(PDO::FETCH_ASSOC);
        return $parametros->fetchAll();
    }

    public function metMostrarParametro($idParametros)
    {
        $parametros = $this->_db->query("
            SELECT
              a035_parametros.*,
              a018_seguridad_usuario.ind_usuario
            FROM
              a035_parametros
            INNER JOIN a018_seguridad_usuario ON a035_parametros.fk_a018_num_seguridad_usuario = a018_seguridad_usuario.pk_num_seguridad_usuario
            WHERE
              pk_num_parametros='$idParametros'
        ");
        $parametros->setFetchMode(PDO::FETCH_ASSOC);
        return $parametros->fetch();
    }

    public function metRegistrarParametro($status, $idMiscelaneo,$idAplicacion,$descripcion, $explicacion,$clave, $valor)
    {
        $this->_db->beginTransaction();
        $parametro = $this->_db->prepare("
                  INSERT INTO
                    a035_parametros
                  SET
                    num_estatus=:num_estatus, fec_ultima_modificacion=NOW(), fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle, fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion,
                    ind_descripcion=:ind_descripcion, ind_explicacion=:ind_explicacion, ind_parametro_clave=:ind_parametro_clave, ind_valor_parametro=:ind_valor_parametro
        ");
        $parametro->execute(array(
            'num_estatus' => $status,
            'fk_a006_num_miscelaneo_detalle' => $idMiscelaneo,
            'fk_a015_num_seguridad_aplicacion' => $idAplicacion,
            'ind_descripcion' => $descripcion,
            'ind_explicacion' => $explicacion,
            'ind_parametro_clave' => $clave,
            'ind_valor_parametro' => $valor
        ));

        $idRegistro = $this->_db->lastInsertId();
        $error = $parametro->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarParametro($status, $idMiscelaneo,$idAplicacion,$descripcion, $explicacion,$clave, $valor, $idParametro)
    {
        $this->_db->beginTransaction();
        $parametro = $this->_db->prepare("
                  UPDATE
                    a035_parametros
                  SET
                    num_estatus=:num_estatus, fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle,
                    fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion,
                    ind_descripcion=:ind_descripcion,
                    ind_explicacion=:ind_explicacion,
                    ind_parametro_clave=:ind_parametro_clave,
                    ind_valor_parametro=:ind_valor_parametro
                  WHERE
                    pk_num_parametros='$idParametro'
        ");
        $parametro->execute(array(
            'num_estatus' => $status,
            'fk_a006_num_miscelaneo_detalle' => $idMiscelaneo,
            'fk_a015_num_seguridad_aplicacion' => $idAplicacion,
            'ind_descripcion' => $descripcion,
            'ind_explicacion' => $explicacion,
            'ind_parametro_clave' => $clave,
            'ind_valor_parametro' => $valor
        ));

        $idRegistro = $this->_db->lastInsertId();
        $error = $parametro->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

}
