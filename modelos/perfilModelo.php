<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO.'menuModelo.php';
class perfilModelo extends menuModelo
{
    private $atIdUsuario;
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }


    public function metMostrarPerfil($idPerfil)
    {
        $menu = $this->_db->query(
            "SELECT * FROM a017_seguridad_perfil WHERE pk_num_seguridad_perfil='$idPerfil'"
        );
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetch();
    }

    public function metMostrarPerfilMenu($idPerfil)
    {
        $menu = $this->_db->query(
            "SELECT * FROM a028_seguridad_menupermiso WHERE fk_a017_num_seguridad_perfil='$idPerfil'"
        );
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metListarPerfil()
    {
        $menu = $this->_db->query(
            "SELECT * FROM a017_seguridad_perfil"
        );
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metCrearPerfil(
        $nombre, $descripcion, $status, $menuPermiso = false
    )
    {
        $this->_db->beginTransaction();

        $NuevoPerfil=$this->_db->prepare(
            "INSERT INTO
                a017_seguridad_perfil
              SET
                ind_nombre_perfil=:ind_nombre_perfil, ind_descripcion=:ind_descripcion, fec_ultima_modificacion=NOW(),
                num_estatus=:num_estatus, fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            "
        );
        $NuevoPerfil->execute(array(
            ':ind_nombre_perfil' => mb_strtoupper($nombre, 'utf-8'),
            ':ind_descripcion' => mb_strtoupper($descripcion, 'utf-8'),
            ':num_estatus' => $status,
        ));
        $idPerfil= $this->_db->lastInsertId();
        if($menuPermiso) {
            $NuevoMenuSeguridad = $this->_db->prepare(
                "INSERT INTO
                    a028_seguridad_menupermiso
                  SET
                    fk_a017_num_seguridad_perfil='$idPerfil', fk_a027_num_seguridad_menu=:fk_a027_num_seguridad_menu
                "
            );
            for ($i = 0; $i < count($menuPermiso); $i++) {

                $NuevoMenuSeguridad->execute(array(
                    ':fk_a027_num_seguridad_menu' => $menuPermiso[$i]
                ));
            }
        }
        $error1 = $NuevoPerfil->errorInfo();
        if($menuPermiso){
            $error2 = $NuevoMenuSeguridad->errorInfo();
        }else{
            $error2 =array(1=>null,2=>null);
        }
        if(!empty($error1[1]) && !empty($error1[2]) && !empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            return $error1;
        }else{
            $this->_db->commit();
            return $idPerfil;
        }
    }

    public function metModificarPerfil(
        $idPerfil, $nombre, $descripcion, $status, $menuPermiso = false
    )
    {
        $this->_db->beginTransaction();
        $modificarPerfil=$this->_db->prepare(
            "UPDATE
                a017_seguridad_perfil
              SET
                ind_nombre_perfil=:ind_nombre_perfil, ind_descripcion=:ind_descripcion, fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario', num_estatus=:num_estatus
              WHERE
                pk_num_seguridad_perfil='$idPerfil'
            "
        );
        $modificarPerfil->execute(array(
            ':ind_nombre_perfil' => mb_strtoupper($nombre, 'utf-8'),
            ':ind_descripcion' => mb_strtoupper($descripcion, 'utf-8'),
            ':num_estatus' => $status,
        ));
        $eliminarMenuPerfiles=$this->_db->prepare(
            "DELETE FROM a028_seguridad_menupermiso WHERE  fk_a017_num_seguridad_perfil=:pk_num_seguridad_perfil"
        );
        $eliminarMenuPerfiles->execute(array(':pk_num_seguridad_perfil'=>$idPerfil));
        if($menuPermiso) {
            $NuevoMenuSeguridad = $this->_db->prepare(
                "INSERT INTO
                    a028_seguridad_menupermiso
                  SET
                    fk_a017_num_seguridad_perfil='$idPerfil', fk_a027_num_seguridad_menu=:fk_a027_num_seguridad_menu
                "
            );
            for ($i = 0; $i < count($menuPermiso); $i++) {
                $NuevoMenuSeguridad->execute(array(
                    ':fk_a027_num_seguridad_menu' => $menuPermiso[$i]
                ));
            }
        }
        $error1 = $modificarPerfil->errorInfo();
        $error2 = $eliminarMenuPerfiles->errorInfo();
        if($menuPermiso){
            $error3 = $NuevoMenuSeguridad->errorInfo();
        }else{
            $error3 =array(1=>null,2=>null);
        }
        if(!empty($error1[1]) && !empty($error1[2]) && !empty($error2[1]) && !empty($error2[2]) && !empty($error3[1]) && !empty($error3[2])){
            $this->_db->rollBack();
            return $error1;
        }else{
            $this->_db->commit();
            return $idPerfil;
        }

    }

    public function metEliminarPerfil($idPerfil)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "
                delete from a028_seguridad_menupermiso where fk_a017_num_seguridad_perfil ='$idPerfil';
                delete from a020_seguridad_usuarioperfil where fk_a017_num_seguridad_perfil ='$idPerfil';
                delete from a017_seguridad_perfil where pk_num_seguridad_perfil ='$idPerfil';
            "
        );
        $this->_db->commit();
    }

}
