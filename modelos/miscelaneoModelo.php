<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        29-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO . 'aplicacionModelo.php';

class miscelaneoModelo extends aplicacionModelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarMiscelaneos($aplicacion = false, $estatus = false)
    {
        if ($aplicacion) {
            $filtro = "WHERE
              a005.num_estatus='$estatus' AND a005.fk_a015_num_seguridad_aplicacion='$aplicacion'";
        } elseif ($estatus && ($estatus == 0 || $estatus == 1)) {
            $filtro = "WHERE
              a005.fk_a015_num_seguridad_aplicacion='$aplicacion'";
        } else {
            $filtro = "";
        }

        $miscelaneo = $this->_db->query("
            SELECT
             *
            FROM
              a005_miscelaneo_maestro AS a005
            INNER JOIN
              a015_seguridad_aplicacion AS a015 ON a005.fk_a015_num_seguridad_aplicacion=a015.pk_num_seguridad_aplicacion
              $filtro
        ");
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);

        return $miscelaneo->fetchAll();
    }

    public function metMostrarMiscelaneo($idMiscelaneo)
    {
        $miscelaneo = $this->_db->query("
            SELECT * FROM a005_miscelaneo_maestro WHERE pk_num_miscelaneo_maestro='$idMiscelaneo'
        ");
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);

        return $miscelaneo->fetch();
    }

    public function metMostrarSelect($codMaestro = false, $idMiscelaneo = false)
    {
        if ($codMaestro) {
            $where = "a005.cod_maestro='$codMaestro'";
        } elseif ($idMiscelaneo) {
            $where = "a006.fk_a005_num_miscelaneo_maestro='$idMiscelaneo'";
        }

        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle,
              a006.ind_nombre_detalle,
              a006.cod_detalle,
              a006.num_estatus
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE $where

        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);

        if ($codMaestro) {
            return $miscelaneoDetalle->fetchAll();
        } elseif ($idMiscelaneo) {
            return $miscelaneoDetalle->fetch();
        }
    }

    public function metCrearMiscelaneo($nombre, $descripcion, $CodMaestro, $aplicacion, $estatus, $detalle = false, $statusDetalle = false, $codDetalle = false)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                a005_miscelaneo_maestro
              SET
                fk_a018_num_seguridad_usuario='$this->atIdUsuario', ind_nombre_maestro=:ind_nombre_maestro,
                ind_descripcion=:ind_descripcion, cod_maestro=:cod_maestro, num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(), fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion
            ");
        $NuevoRegistro->execute(array(
            ':ind_nombre_maestro' => $nombre,
            ':ind_descripcion' => $descripcion,
            ':cod_maestro' => $CodMaestro,
            ':fk_a015_num_seguridad_aplicacion' => $aplicacion,
            ':num_estatus' => $estatus
        ));
        $idMiscelaneo = $this->_db->lastInsertId();
        if ($detalle) {
            $nuevoDetalle = $this->_db->prepare(
                "INSERT INTO
                a006_miscelaneo_detalle
              SET
                fk_a005_num_miscelaneo_maestro=$idMiscelaneo, num_estatus=:num_estatus, ind_nombre_detalle=:ind_nombre_detalle, cod_detalle=:cod_detalle
            ");
            for ($i = 0; $i < count($detalle); $i++) {
                if ($statusDetalle) {
                    if (isset($statusDetalle[$i])) {
                        $estatus = $statusDetalle[$i];
                    } else {
                        $estatus = 0;
                    }
                } else {
                    $estatus = 0;
                }
                $nuevoDetalle->execute(array(
                    'num_estatus' => $estatus,
                    'ind_nombre_detalle' => $detalle[$i],
                    'cod_detalle' => $codDetalle[$i]
                ));
            }
        }
        $error = $NuevoRegistro->errorInfo();
        $error1 = $nuevoDetalle->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) && !empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idMiscelaneo;
        }
    }

    public function metActualizarMiscelaneo($idMiscelaneo, $nombre, $descripcion, $CodMaestro, $aplicacion, $estatus, $detalle = false, $statusDetalle = false, $idDetalle = false, $codDetalle = false)
    {
        $this->_db->beginTransaction();
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                a005_miscelaneo_maestro
              SET
                fk_a018_num_seguridad_usuario='$this->atIdUsuario', ind_nombre_maestro=:ind_nombre_maestro,
                ind_descripcion=:ind_descripcion, num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(), fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion
              WHERE
                pk_num_miscelaneo_maestro='$idMiscelaneo'
            ");
        $ActualizarRegistro->execute(array(
            ':ind_nombre_maestro' => $nombre,
            ':ind_descripcion' => $descripcion,
            ':fk_a015_num_seguridad_aplicacion' => $aplicacion,
            ':num_estatus' => $estatus
        ));

        if ($detalle) {
            for ($i = 0; $i < count($detalle); $i++) {
                if (isset($idDetalle[$i])) {
                    $nuevoDetalle = $this->_db->prepare("
                        UPDATE
                          a006_miscelaneo_detalle
                        SET
                          num_estatus=:num_estatus, ind_nombre_detalle=:ind_nombre_detalle, cod_detalle=:cod_detalle
                        WHERE
                          fk_a005_num_miscelaneo_maestro=$idMiscelaneo AND pk_num_miscelaneo_detalle='$idDetalle[$i]'
                    ");
                } else {
                    $nuevoDetalle = $this->_db->prepare("
                        INSERT INTO
                          a006_miscelaneo_detalle
                        SET
                          fk_a005_num_miscelaneo_maestro=$idMiscelaneo, num_estatus=:num_estatus, ind_nombre_detalle=:ind_nombre_detalle, cod_detalle=:cod_detalle
                    ");
                }

                if ($statusDetalle) {
                    if (isset($statusDetalle[$i])) {
                        $estatus = $statusDetalle[$i];
                    } else {
                        $estatus = 0;
                    }
                } else {
                    $estatus = 0;
                }
                $nuevoDetalle->execute(array(
                    'num_estatus' => $estatus,
                    'ind_nombre_detalle' => $detalle[$i],
                    'cod_detalle' => $codDetalle[$i]
                ));
            }
        }
        $error = $ActualizarRegistro->errorInfo();
        if (isset($nuevoDetalle)) {
            $error1 = $nuevoDetalle->errorInfo();
        } else {
            $error1 = array(1 => null, 2 => null);
        }

        if (!empty($error[1]) && !empty($error[2]) && !empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idMiscelaneo;
        }
    }

}
