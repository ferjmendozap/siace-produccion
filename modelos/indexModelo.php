<?php 
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: INTRANET
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class indexModelo extends Modelo
{
	public function __construct()
    {
        parent::__construct();

    }
	
	public function metBuscarNoticias()
	{
			$con = $this->_db->query("SELECT *
			FROM
			`in_c001_noticias`
			WHERE 	`ind_status` = 'm'
			ORDER BY `pk_num_noticia` DESC ");
			
			$con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
	}

	public function metBusquedaUnicaNoticia($pk_num_noticia)
	{       //INNER
		$con = $this->_db->query("
            SELECT *
            FROM in_c001_noticias
            LEFT JOIN a001_organismo ON in_c001_noticias.fk_a001_num_organismo = a001_organismo.pk_num_organismo
            WHERE   `ind_status` = 'm' AND  `pk_num_noticia` = '$pk_num_noticia'    ");

		$con->setFetchMode(PDO::FETCH_ASSOC);
		return $con->fetch();
	}

	public function metBuscarEventos()
	{
		$con = $this->_db->query("SELECT
		`pk_num_evento`,
		`fec_registro`,
		`hora_inicio`,
		`hora_final`,
		`horario_inicio`,
		`horario_final`,
		`ind_descripcion`,
		`ind_anual`,
		`ind_lugar`,
		`ind_estado`,
		`ind_municipio`,
		`ind_parroquia`,
		`ind_ruta_img`,
		`fec_ultima_modificacion`
		 FROM `in_c002_eventos`
		 ORDER BY `pk_num_evento`  DESC ");
			
		$con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();	
	}

	public function metBusquedUnicaMunicipio($bus)
	{
		$con = $this->_db->query("SELECT * FROM
		`a011_municipio` 
		WHERE `pk_num_municipio` = '$bus'");
		$con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
	}

	public function metBusquedaUnicaParroquia($bus)
	{
		$con = $this->_db->query("SELECT * FROM
		`a012_parroquia`
		WHERE `pk_num_parroquia` = '$bus'");
		$con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
	}

	public function metBusquedaUnicaEstado($bus)
	{
		$con = $this->_db->query("SELECT * FROM
		`a009_estado`
		WHERE `pk_num_estado` = '$bus'");
		$con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
	}

	public function metBuscarCumpleanierosHoy($mes, $dia)
	{
		$con = $this->_db->query("
		 SELECT
 rh_b001_empleado.pk_num_empleado,
 a003_persona.ind_nombre1,
 a003_persona.ind_nombre2,
 a003_persona.ind_apellido1,
 a003_persona.ind_apellido2,
 a003_persona.ind_cedula_documento,
 a003_persona.fec_nacimiento,
 a003_persona.ind_foto,
 rh_c005_empleado_laboral.fec_ingreso,
 rh_c006_tipo_cargo.txt_descripcion,
 rh_c009_nivel.num_nivel,
 rh_c007_grado_salarial.ind_grado,
a001_organismo.pk_num_organismo,
a001_organismo.ind_descripcion_empresa,
a004_dependencia.pk_num_dependencia,
a004_dependencia.ind_dependencia,
a004_dependencia.txt_abreviacion,
rh_c063_puestos.ind_descripcion_cargo
FROM a003_persona
INNER JOIN  rh_b001_empleado ON  rh_b001_empleado.fk_a003_num_persona =  a003_persona.pk_num_persona
INNER JOIN  rh_c005_empleado_laboral ON   rh_c005_empleado_laboral.fk_rhb001_num_empleado  = rh_b001_empleado.pk_num_empleado
INNER JOIN  rh_c063_puestos ON  rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
INNER JOIN  rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
INNER JOIN  rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
INNER JOIN  rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = rh_c076_empleado_organizacion.fk_a001_num_organismo
INNER JOIN  a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
WHERE MONTH(  a003_persona.fec_nacimiento ) = '$mes' AND DAY(  a003_persona.fec_nacimiento ) ='$dia'
AND a003_persona.num_estatus = '1' AND rh_b001_empleado.num_estatus = '1'
		 ");
		$con->setFetchMode(PDO::FETCH_ASSOC);
		return $con->fetchAll();


	}

	public function metBuscarCumpleanierosMes($mes)
	{
		$con = $this->_db->query("
		SELECT
             rh_b001_empleado.pk_num_empleado,
             a003_persona.ind_nombre1,
             a003_persona.ind_nombre2,
             a003_persona.ind_apellido1,
             a003_persona.ind_apellido2,
             a003_persona.ind_cedula_documento,
             a003_persona.fec_nacimiento,
             a003_persona.ind_foto,
             rh_c005_empleado_laboral.fec_ingreso,
             rh_c006_tipo_cargo.txt_descripcion,
             rh_c009_nivel.num_nivel,
             rh_c007_grado_salarial.ind_grado,
            a001_organismo.pk_num_organismo,
            a001_organismo.ind_descripcion_empresa,
            a004_dependencia.pk_num_dependencia,
            a004_dependencia.ind_dependencia,
            a004_dependencia.txt_abreviacion,
            rh_c063_puestos.ind_descripcion_cargo
            FROM a003_persona
            INNER JOIN  rh_b001_empleado ON  rh_b001_empleado.fk_a003_num_persona =  a003_persona.pk_num_persona
            INNER JOIN  rh_c005_empleado_laboral ON   rh_c005_empleado_laboral.fk_rhb001_num_empleado  = rh_b001_empleado.pk_num_empleado
            INNER JOIN  rh_c063_puestos ON  rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
            INNER JOIN  rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
            INNER JOIN  rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
            INNER JOIN  rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = rh_c076_empleado_organizacion.fk_a001_num_organismo
            INNER JOIN  a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
            WHERE MONTH(  a003_persona.fec_nacimiento ) ='$mes'
            AND a003_persona.num_estatus = '1' AND rh_b001_empleado.num_estatus = '1' ORDER BY DAY(  a003_persona.fec_nacimiento )
		 ");
		$con->setFetchMode(PDO::FETCH_ASSOC);
		return $con->fetchAll();

	}


	public function metDatosPersonalesUsuario($idEmpleado)
	{
		$rol = $this->_db->query("
          SELECT

			persona.pk_num_persona,
			persona.ind_cedula_documento,
			persona.ind_documento_fiscal,
			persona.ind_nombre1,
			persona.ind_nombre2,
			persona.ind_apellido1,
			persona.ind_apellido2,
			persona.fk_a006_num_miscelaneo_detalle_nacionalidad,
			persona.fec_nacimiento,
			persona.ind_foto,
			persona.ind_tipo_persona,
			persona.num_estatus AS status_persona,
            empleado.pk_num_empleado,
            empleado.ind_estado_aprobacion,
            empleado.num_estatus AS status_empleado,
            empleado.ind_estado_aprobacion

          FROM
            a003_persona AS persona
          INNER JOIN
            rh_b001_empleado AS empleado ON empleado.fk_a003_num_persona = persona.pk_num_persona
          WHERE
            empleado.pk_num_empleado='$idEmpleado'
          GROUP BY
            persona.pk_num_persona
        ");
		$rol->setFetchMode(PDO::FETCH_ASSOC);
		return $rol->fetch();
	}

	public function metListarCertificados($ind_cedula_documento)
	{
		$rol = $this->_db->query("
          SELECT

			
			persona.ind_cedula_documento,
            persona.pk_num_persona,
            evento.pk_num_evento,
			evento.ind_nombre_evento,
			evento.ind_descripcion,
			evento.fec_registro,
			evento.fec_inicio,
			evento.fec_fin,
			evento.fec_horas_total,
			evento.fec_hora_entrada,
            evento.fec_hora_salida,
            evento.ind_certificado,
            evento.num_flag_certificado_participante,
            evento.num_flag_certificado_ponente,
            evento.fk_a006_num_tipo_evento,
            evento.fk_evc003_num_lugar,
            persona_evento.num_flag_culmino_evento,
            persona_evento.fk_a003_num_persona,
            persona_evento.fk_a006_num_persona_capacitacion
			 
        FROM 
         
            ev_b001_evento AS evento INNER JOIN  
            ( 
              ev_c001_persona_evento AS persona_evento
              INNER JOIN
              a003_persona AS persona ON  persona.pk_num_persona = persona_evento.fk_a003_num_persona
            )
            ON persona_evento.fk_evb001_num_evento = evento.pk_num_evento
         
         WHERE persona.ind_cedula_documento = '$ind_cedula_documento' AND  persona_evento.num_flag_culmino_evento = 1
        ");
		$rol->setFetchMode(PDO::FETCH_ASSOC);
		return $rol->fetchAll();

	}

	public function buscarCertificadoPersona($pk_persona)
    {
        $rol = $this->_db->query("SELECT 
            pk_num_persona_certificado,
            num_certificado,
            fk_a003_num_persona,
            fk_evb001_num_evento
            FROM `ev_c006_persona_certificado`
            where fk_a003_num_persona = $pk_persona");
        $rol->setFetchMode(PDO::FETCH_ASSOC);
        return $rol->fetchAll();
    }

	// Método que permite ver el listado de ponentes del evento
	public function metListarPersona($pkNumEvento, $tipoPersona)
	{
		$verPersona = $this->_db->query(
			"select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento from ev_c001_persona_evento as a, a003_persona as b where a.fk_a003_num_persona=b.pk_num_persona and a.fk_evb001_num_evento=$pkNumEvento and a.num_flag_culmino_evento=1 and a.fk_a006_num_persona_capacitacion=$tipoPersona"
		);
		$verPersona->setFetchMode(PDO::FETCH_ASSOC);
		return $verPersona->fetchAll();
	}

	// Método que permite obtener los nombres de los certificados
	public function metObtenerCertificado($pkNumEvento, $pkNumPersona, $metodo)
	{
		$obtenerCertificado = $this->_db->query(
			"select num_certificado from ev_c006_persona_certificado where fk_a003_num_persona=$pkNumPersona and fk_evb001_num_evento=$pkNumEvento"
		);
		$obtenerCertificado->setFetchMode(PDO::FETCH_ASSOC);
		if($metodo==1){
			return $obtenerCertificado->fetchAll();
		} else {
			return $obtenerCertificado->fetch();
		}

	}

	public function metListarPersonaUnica($pkNumEvento, $tipoPersona, $pk_num_persona)
	{
		$verPersona = $this->_db->query(
			"select
				a.fk_a003_num_persona,
				a.num_flag_culmino_evento,
				a.num_flag_recibio_certificado,
				b.ind_nombre1,
				b.ind_nombre2,
				b.ind_apellido1,
				b.ind_apellido2,
				b.ind_cedula_documento
				from
				ev_c001_persona_evento as a,
				a003_persona as b
				where
				a.fk_a003_num_persona=b.pk_num_persona
				and
				a.fk_evb001_num_evento='$pkNumEvento'
				and
				a.num_flag_culmino_evento=1
				and
				a.fk_a006_num_persona_capacitacion='$tipoPersona'
				and
				a.fk_a003_num_persona='$pk_num_persona'
				"
		);
		$verPersona->setFetchMode(PDO::FETCH_ASSOC);
		return $verPersona->fetch();
	}

	public function metConsultarPonentes($pkNumEvento, $tipoPersona)
	{
		$verContralor = $this->_db->query(
			"select a.fk_a003_num_persona, a.num_flag_culmino_evento, a.num_flag_recibio_certificado, a.fk_a006_num_tipo_instruccion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.ind_cedula_documento, c.ind_nombre_detalle, c.pk_num_miscelaneo_detalle, c.cod_detalle from ev_c001_persona_evento as a, a003_persona as b, a006_miscelaneo_detalle as c where a.fk_a003_num_persona=b.pk_num_persona and a.fk_a006_num_persona_capacitacion=$tipoPersona and a.fk_evb001_num_evento=$pkNumEvento and  a.fk_a006_num_tipo_instruccion=c.pk_num_miscelaneo_detalle limit 2"
		);
		$verContralor->setFetchMode(PDO::FETCH_ASSOC);
		return $verContralor->fetchAll();
	}

	public function metObtenerSexo($pkNumPersona)
	{
		$consultarSexo = $this->_db->query(
			"select b.cod_detalle from a003_persona as a, a006_miscelaneo_detalle as b where a.pk_num_persona=$pkNumPersona and a.fk_a006_num_miscelaneo_detalle_sexo=b.pk_num_miscelaneo_detalle"
		);
		$consultarSexo->setFetchMode(PDO::FETCH_ASSOC);
		return $consultarSexo->fetch();
	}

	public function metBuscarMensajesChat($dia, $mes, $anio)
	{
		$verTema = $this->_db->query(
			"SELECT
			 chat.pk_num_chat,
			 chat.ind_mensaje,
			 chat.fec_registro,
			 DATE_FORMAT(chat.fec_registro, '%T' ) as hora,
			 chat.fk_b001_num_empleado,
			 chat.fec_ultima_modificacion,
			 persona.ind_nombre1 as nombre,
			 persona.ind_apellido1 as apellido,
			 persona.ind_foto,
			 empleado.pk_num_empleado
			 FROM
			 `in_c003_chat` AS chat
			 INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = chat.fk_b001_num_empleado
             INNER JOIN a003_persona AS persona ON  persona.pk_num_persona = empleado.fk_a003_num_persona
             WHERE DAY(chat.fec_registro)= '$dia' AND MONTH(  chat.fec_registro ) ='$mes' AND YEAR( chat.fec_registro ) ='$anio'
             ORDER BY chat.pk_num_chat DESC  "
		);
		$verTema->setFetchMode(PDO::FETCH_ASSOC);
		return $verTema->fetchAll();
	}

	public function metInsertarMensajeChat($ind_mensaje, $fec_registro, $fk_b001_num_empleado, $fec_ultima_modificacion)
	{
		$this->_db->beginTransaction();
		$NuevaSolicitud=$this->_db->prepare(
			"insert into in_c003_chat values (null, :ind_mensaje, :fec_registro, :fk_b001_num_empleado, :fec_ultima_modificacion)"
		);
		$NuevaSolicitud->execute(array(

			':ind_mensaje' => $ind_mensaje,
			':fec_registro' => $fec_registro,
			':fk_b001_num_empleado' => $fk_b001_num_empleado,
			':fec_ultima_modificacion' => $fec_ultima_modificacion

		));
		$pk_num_chat = $this->_db->lastInsertId();
		$this->_db->commit();
		return $pk_num_chat;
	}

	public function metContarMensajeHoy($atIdUsuario)
	{
		$dia = date('d');
		$mes = date('m');
		$anio = date('Y');

		$consulta = $this->_db->query("
          SELECT
          COUNT(*) AS total
          FROM
          in_c003_chat
          WHERE DAY(fec_registro)= '$dia' AND MONTH(  fec_registro ) ='$mes' AND YEAR( fec_registro ) ='$anio' AND  fk_b001_num_empleado <> '$atIdUsuario'
        ");
		$consulta->setFetchMode(PDO::FETCH_ASSOC);
		return $consulta->fetch();
	}

	/*consultas para mostrar las notificaciones en el sistema*/
	public function metNotificaciones($sql)
    	{
		$consulta=$this->_db->prepare($sql);
		$consulta->execute();
		return $consulta->rowCount();
    	}



}
?>
