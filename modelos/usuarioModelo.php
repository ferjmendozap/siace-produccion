<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_MODELO.'perfilModelo.php';
class usuarioModelo extends perfilModelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metEmpleado()
    {
        $menu = $this->_db->query(
            "select 
                * 
            from 
                rh_b001_empleado
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona 
                LEFT JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = a003_persona.fk_a006_num_miscelaneo_detalle_sexo
            WHERE 
               rh_b001_empleado.pk_num_empleado NOT IN ( SELECT fk_rhb001_num_empleado FROM a018_seguridad_usuario )
            "
        );
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metMostrarUsuario($idUsuario)
    {
        $menu = $this->_db->query(
            "SELECT * FROM a018_seguridad_usuario WHERE pk_num_seguridad_usuario='$idUsuario'"
        );
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetch();
    }

    public function metMostrarUsuarioPerfil($idUsuario)
    {
        $menu = $this->_db->query(
            "SELECT * FROM a020_seguridad_usuarioperfil WHERE fk_a018_num_seguridad_usuario='$idUsuario'"
        );
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metObtenerRol($idUsuario)
    {
        $rol = $this->_db->query("
          SELECT
            a027.ind_rol
          FROM
            a020_seguridad_usuarioperfil AS a020
          INNER JOIN
            a028_seguridad_menupermiso AS a028 ON a020.fk_a017_num_seguridad_perfil=a028.fk_a017_num_seguridad_perfil
          INNER JOIN
            a027_seguridad_menu AS a027 ON a028.fk_a027_num_seguridad_menu=a027.pk_num_seguridad_menu
          WHERE
            a020.fk_a018_num_seguridad_usuario='$idUsuario'
          GROUP BY
            a027.ind_rol
        ");
        $rol->setFetchMode(PDO::FETCH_ASSOC);
        return $rol->fetchAll();
    }
    /*
    *FERNANDO MENDOZA
    *16Ene18
    *Permite obtener el rol de al app y usuario
    */
    public function metObtenerRolApp($idUsuario,$app)
    {
        $rol = $this->_db->query("
          SELECT
            a027.ind_rol
          FROM
            a020_seguridad_usuarioperfil AS a020
          INNER JOIN
            a028_seguridad_menupermiso AS a028 ON a020.fk_a017_num_seguridad_perfil=a028.fk_a017_num_seguridad_perfil
          INNER JOIN
            a027_seguridad_menu AS a027 ON a028.fk_a027_num_seguridad_menu=a027.pk_num_seguridad_menu
          INNER JOIN
            a015_seguridad_aplicacion AS a015 ON a027.fk_a015_num_seguridad_aplicacion=a015.pk_num_seguridad_aplicacion  
          WHERE
            a020.fk_a018_num_seguridad_usuario='$idUsuario' and a015.cod_aplicacion='$app'
          GROUP BY
            a027.ind_rol
        ");
        $rol->setFetchMode(PDO::FETCH_ASSOC);
        return $rol->fetchAll();
    }     

    public function metListarUsuarios()
    {
        $usuario = $this->_db->query(
            "SELECT 
              * 
            FROM a018_seguridad_usuario AS a018
            INNER JOIN rh_b001_empleado AS rh_b001 ON a018.fk_rhb001_num_empleado = rh_b001.pk_num_empleado
            INNER JOIN a003_persona AS a003 ON rh_b001.fk_a003_num_persona = a003.pk_num_persona
            "
        );
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }

    public function metRegistrarUsuario($usuario, $pass, $template, $status, $perfil=false, $empleado=null)
    {
        $this->_db->beginTransaction();
            $NuevoRegistro=$this->_db->prepare(
                "INSERT INTO
                    a018_seguridad_usuario
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                    ind_usuario=:ind_usuario, ind_password=:ind_password, ind_template=:ind_template, num_estatus=:num_estatus,
                    fec_ultima_modificacion=NOW()
                "
            );
            $NuevoRegistro->execute(array(
                ':ind_usuario' => $usuario,
                ':ind_password' => Hash::metObtenerHash($pass),
                ':ind_template' => $template,
                ':num_estatus' => $status,
                ':fk_rhb001_num_empleado' => $empleado
            ));
            $idUsuario= $this->_db->lastInsertId();
            if($perfil) {
                $NuevoPerfil = $this->_db->prepare(
                    "INSERT INTO
                        a020_seguridad_usuarioperfil
                      SET
                        fk_a018_num_seguridad_usuario='$idUsuario', fk_a017_num_seguridad_perfil=:fk_a017_num_seguridad_perfil
                    "
                );
                for ($i = 0; $i < count($perfil); $i++) {
                    $NuevoPerfil->execute(array(
                        ':fk_a017_num_seguridad_perfil' => $perfil[$i]
                    ));
                }
            }
            $error1 = $NuevoRegistro->errorInfo();
            if($perfil){
                $error2 = $NuevoPerfil->errorInfo();
            }else{
                $error2 =array(1=>null,2=>null);
            }
            if(!empty($error1[1]) && !empty($error1[2]) && !empty($error2[1]) && !empty($error2[2])){
                $this->_db->rollBack();
                return $error1;
            }else{
                $this->_db->commit();
                return $idUsuario;
            }
    }

    public function metactualizarUsuario($idUsuario,$usuario, $pass, $template,$status, $perfil=false)
    {
        if($pass=='noModificar'){
            $password='';
            $arrayParametros=array(
                ':ind_usuario' => $usuario,
                ':ind_template' => $template,
                ':num_estatus' => $status
            );
        }else{
            $password='ind_password=:ind_password,';
            $arrayParametros=array(
                ':ind_usuario' => $usuario,
                ':ind_password' => Hash::metObtenerHash($pass),
                ':ind_template' => $template,
                ':num_estatus' => $status
            );
        }
        $this->_db->beginTransaction();
            $ModificarRegistro=$this->_db->prepare(
                "UPDATE
                    a018_seguridad_usuario
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_usuario=:ind_usuario,
                    $password ind_template=:ind_template, num_estatus=:num_estatus
                  WHERE
                    pk_num_seguridad_usuario='$idUsuario'"
            );
            $ModificarRegistro->execute($arrayParametros);
            $eliminarMenuPerfiles=$this->_db->prepare(
                "DELETE FROM a020_seguridad_usuarioperfil WHERE  fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario"
            );
            $eliminarMenuPerfiles->execute(array(':fk_a018_num_seguridad_usuario'=>$idUsuario));
            if($perfil) {
                $NuevoPerfil = $this->_db->prepare(
                    "INSERT INTO
                        a020_seguridad_usuarioperfil
                      SET
                        fk_a018_num_seguridad_usuario='$idUsuario', fk_a017_num_seguridad_perfil=:fk_a017_num_seguridad_perfil
                    "
                );
                for ($i = 0; $i < count($perfil); $i++) {
                    $NuevoPerfil->execute(array(
                        ':fk_a017_num_seguridad_perfil' => $perfil[$i]
                    ));
                }
            }
            $error1 = $ModificarRegistro->errorInfo();
            $error2 = $eliminarMenuPerfiles->errorInfo();
            if($perfil){
                $error3 = $NuevoPerfil->errorInfo();
            }else{
                $error3 =array(1=>null,2=>null);
            }
        if(!empty($error1[1]) && !empty($error1[2]) && !empty($error2[1]) && !empty($error2[2]) && !empty($error3[1]) && !empty($error3[2])){
            $this->_db->rollBack();
            return $error1;
        }else{
            $this->_db->commit();
            return $idUsuario;
        }
    }

    public function metEliminarUsuario($idUsuario)
    {
        $this->_db->beginTransaction();
        $eliminarRegistro=$this->_db->prepare(
            "delete from a020_seguridad_usuarioperfil where fk_a018_num_seguridad_usuario = :fk_a018_num_seguridad_usuario"
        );
        $eliminarRegistro->execute(array(
            ':fk_a018_num_seguridad_usuario' => $idUsuario
        ));
        $eliminarRegistro2=$this->_db->prepare(
            "delete from a018_seguridad_usuario where pk_num_seguridad_usuario = :pk_num_seguridad_usuario "
        );
        $eliminarRegistro2->execute(array(
            ':pk_num_seguridad_usuario' => $idUsuario
        ));

        $error1 = $eliminarRegistro->errorInfo();
        $error2 = $eliminarRegistro2->errorInfo();
        if(!empty($error1[1]) && !empty($error1[2])){
            $this->_db->rollBack();
            return $error1;
        }elseif(!empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            return $error2;
        }else{
            $this->_db->commit();
            return $idUsuario;
        }
    }

    public function metActualizarPass($idUsuario,$password)
    {
        $this->_db->beginTransaction();
        $ModificarRegistro=$this->_db->prepare(
            "UPDATE
                    a018_seguridad_usuario
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_password=:ind_password
                  WHERE
                    pk_num_seguridad_usuario='$idUsuario'"
        );
        $ModificarRegistro->execute(array(
            ':ind_password' => Hash::metObtenerHash($password)
        ));

        $error1 = $ModificarRegistro->errorInfo();
        if(!empty($error1[1]) && !empty($error1[2])){
            $this->_db->rollBack();
            return $error1;
        }else{
            $this->_db->commit();
            return $idUsuario;
        }
    }

}
