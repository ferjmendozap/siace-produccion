<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

#Clase que se encarga Para encriptar Con MD5 y SHA-1 Con un token Unico de la aplicacion
class Hash
{
    public static function metObtenerHash($data)
    {
        $algoritgmo='sha1';
        $key=LLAVE_APLICACION;
        #hash_init — Inicializa un contexto incremental para cifrar
        #Funcion de Encriptacion
        $hash= hash_init($algoritgmo, HASH_HMAC, $key);
        #hash_update — Pega más datos en un contexto incremental de cifrado activo
        hash_update($hash, md5($data));
        #hash_final — Finaliza un contexto incremental y devuelve el resultado cifrado
        #Devuelce la Encriptacion
        return hash_final($hash);
    }
}
