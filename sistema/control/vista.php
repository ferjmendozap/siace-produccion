<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

#se incluye el motor de plantillas smarty.
require_once ROOT . 'librerias' . DS . 'smarty' . DS . 'libs' . DS . 'Smarty.class.php';

#Vista Principal. se encarga de cargar los css, js, plugins y armar las vistas de cada controlador
class Vista extends Smarty
{
    private static $atItem;
    private $atControlador;
    private $atModulos;
    private $atJs;
    private $atJsComplemento;
    private $atCssComplemento;
    private $atSolicitud;
    private $atRutas;
    private $atPlantillaWeb;
    private $atRutaVista;

    public function __construct(Solicitud $solicitud)
	{
        parent::__construct();
        $this->atSolicitud = $solicitud;
        $this->atJs = array();
        $this->atJsComplemento = array();
        $this->atCssComplemento= array();
        $this->atRutas= array();
        $this->atPlantillaWeb= RUTA_PLANTILLA_WEB_PREDEFINIDA;
        self::$atItem = null;

        $this->atModulos = $this->atSolicitud->metObtenerModulo();
        $this->atControlador = $this->atSolicitud->metObtenerControlador();
        $this->atRutaVista= $this->atSolicitud->metObtenerRutaControlador();

        if($this->atModulos){
            $this->atRutas['vista'] = ROOT.'modulos'.DS.$this->atModulos.DS.'vistas'.DS.$this->atRutaVista.$this->atControlador.DS;
        }else{
            $this->atRutas['vista'] = ROOT.'vistas'.DS.$this->atControlador.DS;
        }
        $this->atRutas['js'] = BASE_URL . 'publico/javascript/';
        $this->atRutas['jsComplemento'] = BASE_URL . 'publico/complementos/';
        $this->atRutas['CssComplemento'] = BASE_URL . 'publico/complementos/';

    }

    #Agregar un Js nuevo desde el controlador
    public function metCargarJs(array $js)
    {
        if(is_array($js) && count($js)){
            for($i=0; $i < count($js); $i++){
                $this->atJs[] = $this->atRutas['js'] . $js[$i] . '.js';
            }
        }else{
            throw new Exception('Error de js');
        }
    }

    #Agregar un JsPlugins nuevo desde el controlador
    public function metCargarJsComplemento(array $js)
    {
        if(is_array($js) && count($js)){
            for($i=0; $i < count($js); $i++){
                $this->atJsComplemento[] = $this->atRutas['jsComplemento'] .  $js[$i] . '.js';
            }
        }else{
            throw new Exception('Error de js plugin');
        }
    }

    #Agregar un JsPlugins nuevo desde el controlador
    public function metCargarCssComplemento(array $js)
    {
        if(is_array($js) && count($js)){
            for($i=0; $i < count($js); $i++){
                $this->atCssComplemento[] = $this->atRutas['jsComplemento'] . $js[$i] . '.css';
            }
        }else{
            throw new Exception('Error de js plugin');
        }
    }

    public function metCargarPlantilla($template)
    {
        $this->atPlantillaWeb = (string) $template;
    }

    #Metodo utilizado para cargar las vistas de cada controlador al template principal
    public function metRenderizar($vista,$vistaMaestraDefauld=false, $item = false)
    {
        if ($item) {
            self::$atItem = $item;
        }

        $this->caching = false;
        $this->force_compile = true;
        #Se define la ruta donde se van a cargar los template
        $this->template_dir = ROOT.'sistema'.DS.'plantillas'.DS.$this->atPlantillaWeb.DS;

        #Se define la ruta donde se van a guardar los archivos de configuracion
        #    $this->config_dir = ROOT.'sistema'.DS.'plantillas'.DS.$this->atPlantillaWeb.DS.'config'.DS;
        #Se define la ruta donde se van a guardar los archivos de chache
        $this->cache_dir = ROOT.'sistema'.DS.'temporales'.DS.'cache'.DS;
        #Se define la ruta donde se van a guardar los templates compilados
        $this->compile_dir = ROOT.'sistema'.DS.'temporales'.DS.'compilados'.DS;

        #Array de parametros de rutas a los js css y imagenes de la aplicacion
        $_Parametros = array(
            'ruta_Css' => BASE_URL . 'publico/estilos/',
            'ruta_Img' => BASE_URL . 'publico/imagenes/',
            'ruta_Js' => BASE_URL . 'publico/javascript/',
            'ruta_Fonts' => BASE_URL . 'publico/fonts/',
            'ruta_Complementos' => BASE_URL . 'publico/complementos/',
            'js' => $this->atJs,
            'js_complemento' => $this->atJsComplemento,
            'css_complemento' => $this->atCssComplemento,
            'url' => BASE_URL,
            'idUsuario' => Session::metObtener('idUsuario'),
            'nombreUsuario' => Session::metObtener('nombreUsuario'),
            'imagenUsuario' => Session::metObtener('imagenUsuario'),
            'cargoEmpleado' => Session::metObtener('cargoEmpleado'),
            'menu' =>Session::metObtener('menu'),
            'perfil' =>Session::metObtener('perfil'),
            'configs' => array(
                'session_tiempo' => SESSION_TIEMPO * 60000,
                'app_nombre' => APP_NOMBRE,
                'app_descripcion' => APP_DESCRIPCION,
                'app_organismo' => APP_ORGANISMO,
            ),
            'colorHeader' => Session::metObtener('CHEADERM'),
            'colorBorde' => Session::metObtener('BMODAL'),
            'colorTitulo' => Session::metObtener('CTITULOM')
        );

        #parametro que indica si el chat esta inactivo o no
        $estadoChat = Session::metObtener('ESTADOCHAT');
        $this->assign('estadoChat', $estadoChat );

        #modo de interfaz
        $interfaz = Session::metObtener('INTERFAZ');
        $this->assign('INTERFAZ', $interfaz );        

        #si no existe ruta maestra se le asigna la que se encuentra por defecto en el archivo de config.php
        if(!$vistaMaestraDefauld){
            $vistaMaestraDefauld=PLANTILLA_WEB_PREDEFINIDA;
        }
        #se valida que la ruta hacia el contenido de la vista sea legible
        if(is_readable($this->atRutas['vista'].$vista.'.tpl')){
            #si es legible se incorpora en el template principal
            $this->assign('_contenido', $this->atRutas['vista'].$vista.'.tpl');
        #si no es legible
        }else{
            #Se genera Un Mensaje de Error.
            header('location:' . BASE_URL . 'error/error/402');
            exit;
        }
        #se asignan los parametros a la vista principal
        $this->assign('_Parametros', $_Parametros);
        #se carga la vista principal.
        $this->display($vistaMaestraDefauld.'.tpl');
    }

    #FERNANDO MENDOZA (0424-8942068) (16Ene18) (dt.jefe.ait@cgesucre.gob.ve)
    #Metodo utilizado para cargar las vistas de cada controlador al template principal del modulo
    public function metRenderizarModulo($vista,$vistaMaestraDefauld=false, $item = false, $app)
    {
        if ($item) {
            self::$atItem = $item;
        }

        $this->caching = false;
        $this->force_compile = true;
        #Se define la ruta donde se van a cargar los template
        $this->template_dir = ROOT.'sistema'.DS.'plantillas'.DS.$this->atPlantillaWeb.DS;

        #Se define la ruta donde se van a guardar los archivos de configuracion
        #    $this->config_dir = ROOT.'sistema'.DS.'plantillas'.DS.$this->atPlantillaWeb.DS.'config'.DS;
        #Se define la ruta donde se van a guardar los archivos de chache
        $this->cache_dir = ROOT.'sistema'.DS.'temporales'.DS.'cache'.DS;
        #Se define la ruta donde se van a guardar los templates compilados
        $this->compile_dir = ROOT.'sistema'.DS.'temporales'.DS.'compilados'.DS;

        #Array de parametros de rutas a los js css y imagenes de la aplicacion
        $_Parametros = array(
            'ruta_Css' => BASE_URL . 'publico/estilos/',
            'ruta_Img' => BASE_URL . 'publico/imagenes/',
            'ruta_Js' => BASE_URL . 'publico/javascript/',
            'ruta_Fonts' => BASE_URL . 'publico/fonts/',
            'ruta_Complementos' => BASE_URL . 'publico/complementos/',
            'js' => $this->atJs,
            'js_complemento' => $this->atJsComplemento,
            'css_complemento' => $this->atCssComplemento,
            'url' => BASE_URL,
            'idUsuario' => Session::metObtener('idUsuario'),
            'nombreUsuario' => Session::metObtener('nombreUsuario'),
            'imagenUsuario' => Session::metObtener('imagenUsuario'),
            'menu' =>Session::metObtener('menu'),
            'perfil' =>Session::metObtener('perfil'),
            'configs' => array(
                'session_tiempo' => SESSION_TIEMPO,
                'app_nombre' => APP_NOMBRE,
                'app_descripcion' => APP_DESCRIPCION,
                'app_organismo' => APP_ORGANISMO,
            )
        );

        #parametro que indica si el chat esta inactivo o no
        $estadoChat = Session::metObtener('ESTADOCHAT');
        $this->assign('estadoChat', $estadoChat );

        #modo de interfaz
        $interfaz = Session::metObtener('INTERFAZ');
        $this->assign('INTERFAZ', $interfaz );

        #si no existe ruta maestra se le asigna la que se encuentra por defecto en el archivo de config.php
        if(!$vistaMaestraDefauld){
            $vistaMaestraDefauld=PLANTILLA_WEB_PREDEFINIDA;
        }

        #se valida que la ruta hacia el contenido de la vista sea legible
        if(is_readable($this->atRutas['vista'].$vista.'.tpl')){
            #si es legible se incorpora en el template principal
            $this->assign('_contenido', $this->atRutas['vista'].$vista.'.tpl');
            #si no es legible
        }else{
            #Se genera Un Mensaje de Error.
            header('location:' . BASE_URL . 'error/error/402');
            exit;
        }
        #se asignan los parametros a la vista principal
        $this->assign('_Parametros', $_Parametros);
        #se carga la vista principal.
        $this->display($vistaMaestraDefauld.'.tpl');
    }    


}
