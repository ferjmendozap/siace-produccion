<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
#Modelo Principal Donde se construye la conexión a base de datos y parámetros a usar
class Modelo
{
	protected $_db;
    protected $_db2;

    #se usa un metodo constructor para asignarle a todos los modelos que extiendan del principal estos
    #parametros
	public function __construct()
	{
        #se instancia el controlador db.
        $this->_db = new db();
        #se le asigna un atributo de metdo PDO para habilitar el rollback en el sistema y evitar que se hagan
        #inserciones a la base de datos sin que se encuentre dentro de una transaccion

        #PDO::ATTR_AUTOCOMMIT Si este valor es FALSE o 0, PDO intenta desactivar la autoconsigna para que la
        # conexión comience una transacción.
        $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);

        if(DB_HOST2!='' && DB_NAME2!='' && DB_USER2!='' && DB_PASS2!='') {
            $this->_db2 = new db2();
            $this->_db2->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
        }
	}

	#Funcion que prmite acceder a la base de datos desde e controlador o cualquier metodo del sistema.
	public function metAccesoControladorDB()
    {
        return $this->_db;
    }
	
}
