<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
#Clase De Conexion a la base de datos que extiende del estandar de pdo usado por php
class db extends PDO
{
	
	public function __construct()
	{
		parent::__construct(
			'mysql:host=' . DB_HOST .
			';dbname=' . DB_NAME,
			DB_USER,
			DB_PASS,
            array(
			    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . DB_CHAR
            )
        );
		
	}
}

class db2 extends PDO
{
	public function __construct()
	{
		if(DB_HOST2!='' && DB_NAME2!='' && DB_USER2!='' && DB_PASS2!=''){
			parent::__construct(
				'mysql:host=' . DB_HOST2 .
				';dbname=' . DB_NAME2,
				DB_USER2,
				DB_PASS2,
				array(
					PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . DB_CHAR2
				)
			);
		}
	}
}
