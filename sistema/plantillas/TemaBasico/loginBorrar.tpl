<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="SIACE">
    <meta name="keywords" content="SIACE">
    <title>SIACE - Login</title>

    <!-- Favicons
    <link rel="icon" href="http://demo.geekslabs.com/materialize/v3.1/images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons
    <link rel="apple-touch-icon-precomposed" href="http://demo.geekslabs.com/materialize/v3.1/images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Fonts}font-awesome_v4.3.0.css" />
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Fonts}material-design-iconic-font.mine.css" />
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Fonts}icomoon.css" />
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Complementos}sweet-alert/sweet-alert.min.css" />

    <link href="{$_Parametros.ruta_Css}login/materialize.min.css" type="text/css" rel="stylesheet"
          media="screen,projection">
    <link href="{$_Parametros.ruta_Css}login/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="{$_Parametros.ruta_Css}login/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{$_Parametros.ruta_Css}login/page-center.css" type="text/css" rel="stylesheet"
          media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{$_Parametros.ruta_Css}login/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{$_Parametros.ruta_Css}login/perfect-scrollbar.css" type="text/css" rel="stylesheet"
          media="screen,projection">
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}jquery/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}spin.js/spin.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}autosize/jquery.autosize.min.js"></script>
    <script type="text/javascript"
            src="{$_Parametros.ruta_Complementos}nanoscroller/jquery.nanoscroller.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}DataTables/datatable.js"></script>
    <script type="text/javascript"
            src="{$_Parametros.ruta_Complementos}nanoscroller/jquery.nanoscroller.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}sweet-alert/sweet-alert.min.js"></script>
    <script type="text/javascript"
            src="{$_Parametros.ruta_Js}materialSiace/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Js}materialSiace/core/demo/Demo.js"></script>
</head>

<body class="cyan loaded">
<!-- End Page Loading -->
<div id="login-page" class="row">
    {include file=$_contenido}
</div>


<!-- ================================================
  Scripts
  ================================================ -->


<div class="hiddendiv common"></div>
</body>
</html>
