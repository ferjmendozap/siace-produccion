<!doctype html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Contraloria del Estado">
    <title>{$modulo}</title>
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Complementos}bootstrap/bootstrap_v3.3.4.css" />
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Css}MaterialSiace.css" />
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Fonts}font-awesome_v4.3.0.css" />
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Fonts}material-design-iconic-font.mine.css" />
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Fonts}icomoon.css" />
    <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Complementos}sweet-alert/sweet-alert.min.css" />

    {if isset($_Parametros.css_complemento) && count($_Parametros.css_complemento)}
        {foreach item=css from=$_Parametros.css_complemento}
            <link rel="stylesheet" href="{$css}" type="text/css">
        {/foreach}
    {/if}

    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}jquery/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}spin.js/spin.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}autosize/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}nanoscroller/jquery.nanoscroller.min.js"></script>

    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}DataTables/datatable.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Complementos}sweet-alert/sweet-alert.min.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Js}materialSiace/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Js}materialSiace/offcanvas.js"></script>
    <script type="text/javascript" src="{$_Parametros.ruta_Js}materialSiace/core/demo/Demo.js"></script>

    {if isset($_Parametros.js_complemento) && count($_Parametros.js_complemento)}
        {foreach item=js from=$_Parametros.js_complemento}
            <script type="text/javascript" src="{$js}"></script>
        {/foreach}
    {/if}
    {if isset($_Parametros.js) && count($_Parametros.js)}
        {foreach item=js from=$_Parametros.js}
            <script type="text/javascript" src="{$js}"></script>
        {/foreach}
    {/if}
    <script type="text/javascript">
        var time;
        function inicio() {
            time = setTimeout(function() {
                $(document).ready(function(e) {
                    $.ajax({
                        url:'{$_Parametros.url}error',
                        type:'POST',
                        data:'verificacion=1',
                        success: function(data){
                            if(data == 1)
                            {
                                document.location.href='{$_Parametros.url}error/error/401';
                            }
                        }

                    });
                });
            },{$_Parametros.configs.session_tiempo});//fin timeout
        }//fin inicio
        function reset() {
            clearTimeout(time);//limpia el timeout para resetear el tiempo desde cero
            time = setTimeout(function() {
                $(document).ready(function(e) {
                    $.ajax({
                        url:'{$_Parametros.url}error',
                        type:'POST',
                        data:'verificacion=1',
                        success: function(data){
                            if(data == 1){
                                document.location.href='{$_Parametros.url}error/error/401';
                            }
                        }
                    });
                });
            },{$_Parametros.configs.session_tiempo});//fin timeout
        }//fin reset
        $(document).ready(function() {
            $('#ModalLinkPrincipal').click(function () {
                var url = $(this).attr('url');
                var valorId = $(this).attr('valorId');
                var nombreId = $(this).attr('nombreId');
                $('#formModalLabel').html($(this).attr('titulo'));
                $.ajax({
                    url: '{$_Parametros.url}' + url,
                    type: 'POST',
                    data: nombreId + '=' + valorId,
                    success: function (data) {
                        $('#ContenidoModal').html(data);
                    }
                });
            });
        });
    </script>
</head>

<body class="menubar-hoverable header-fixed" onload="inicio()" onkeypress="reset()" onclick="reset()" onMouseMove="reset()">
<header id="header" class="header-inverse">
    <div class="headerbar">
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li class="header-nav-brand" >
                    <div class="brand-holder">
                        <a href="{$_Parametros.url}">
                            <span class="text-lg text-bold text-primary">SIACE - {$modulo}</span>
                        </a>
                    </div>
                </li>
                <li>
                    <a class="btn btn-icon-toggle menubar-toggle hidden-lg hidden-md" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="headerbar-right">
            <!--
            <ul class="header-nav header-nav-options">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                        <i class="fa fa-bell"></i><sup class="badge style-danger">1</sup>
                    </a>
                    <ul class="dropdown-menu animation-expand">
                        <li class="dropdown-header">NOTIFICACIONES</li>
                        <li>
                            <a class="alert alert-callout alert-warning" href="javascript:void(0);"data-toggle="modal" data-target="#formModal">
                                <img class="pull-right img-circle dropdown-avatar" src="" alt="" />
                                <strong>Nombre Creador</strong><br/>
                                <small>Obligacion: Nomina de Empleados Por Revisar</small>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            -->
            <ul class="header-nav header-nav-profile">
                <!-- -->
                <li>
                    <a href="javascript:void(0);" id="notificacion_chat" class="btn btn-icon-toggle btn-default abrir_chat_btn" data-keyboard="false" data-backdrop="static" data-target="#formModalChatIntranet" data-toggle="modal" >
                        <span id="notificacion_chat"><i class="md md-chat"></i></span>
                    </a>
                </li>
                <!-- -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                        <img src="{$_Parametros.imagenUsuario}" alt="" />
                        <span class="profile-info">
                            {$_Parametros.nombreUsuario}
                            <small>Administrador del Sistema Siace</small>
                        </span>
                    </a>
                    <ul class="dropdown-menu animation-dock">
                        <li class="dropdown-header">Configuracion</li>
                        <li>
                            <a data-toggle="modal" data-target="#formModal" data-keyboard="false"
                               data-backdrop="static" titulo="<i class='icm icm-key'></i> Cambiar Contraseña" id="ModalLinkPrincipal"
                               href="#formModal" url="usuario/cambiarPass" valorId="{$_Parametros.idUsuario}" nombreId="idUsuario"
                               class="logsUsuario" descipcion="el Usuario a creado cambiado su contraseña"
                            >
                                <i class="icm icm-key"></i> Cambiar Contraseña
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#" onclick="window.close()"><i class="fa fa-fw fa-sign-out text-danger"></i> Cerrar Módulo</a></li>
                    </ul>
                </li>
            </ul>
            <!-- -->
            <ul class="header-nav header-nav-toggle">
                <li>
                    <a class="btn btn-icon-toggle btn-default pull-right abrir_chat_btn " data-keyboard="false" data-backdrop="static" data-target="#formModalChatIntranet" data-toggle="modal" id="abrir_chat_btn"  >
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                </li>
            </ul>
            <!-- -->
        </div>
    </div>
</header>



<div id="base">
    <div id="menubar" class="menubar-inverse ">
        <div class="menubar-fixed-panel">
            <div>
                <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            <div class="expanded">
                <a href="{$_Parametros.url}">
                    <span class="text-lg text-bold text-primary ">SIACE</span>
                </a>
            </div>
        </div>
        <div class="menubar-scroll-panel">
            <!-- BEGIN MAIN MENU -->
            <ul id="main-menu" class="gui-controls">
                <li class="active expanding expanded" modulo="1">
                    <a href="{$_Parametros.url}index/vistaModulo/{$moduloV}">
                        <div class="gui-icon"><i class="md md-home"></i></div>
                        <span class="title">Inicio</span>
                    </a>
                </li>
                {foreach item=menu from=$_Parametros.menu}
                    {if in_array($menu.menu.ind_rol,$_Parametros.perfil)}
                        {if $menu.menu1 != null }
                            <li class="gui-folder" modulo="1">
                                <a>
                                    <div class="gui-icon"><i class="{$menu.menu.ind_icono}"></i></div>
                                    <span class="title">{$menu.menu.ind_nombre}</span>
                                </a>
                                <ul>
                                    {foreach item=menu1 from=$menu.menu1}
                                        {if in_array($menu1.menu1.ind_rol,$_Parametros.perfil)}
                                            {if $menu1.menu2 != null }
                                                <li class="gui-folder">
                                                    <a>
                                                        <span class="title"><i class="{$menu1.menu1.ind_icono}"></i> {$menu1.menu1.ind_nombre}</span>
                                                    </a>
                                                    <ul>
                                                        {foreach item=menu2 from=$menu1.menu2}
                                                            {if in_array($menu2.menu2.ind_rol,$_Parametros.perfil)}
                                                                {if $menu2.menu3 != null }
                                                                    <li class="gui-folder">
                                                                        <a>
                                                                            <span class="title"><i class="{$menu2.menu2.ind_icono}"></i> {$menu2.menu2.ind_nombre}</span>
                                                                        </a>
                                                                        <ul>
                                                                            {foreach item=menu3 from=$menu2.menu3}
                                                                                {if in_array($menu3.menu3.ind_rol,$_Parametros.perfil)}
                                                                                    {if $menu3.menu4 != null }
                                                                                        <li class="gui-folder">
                                                                                            <a>
                                                                                                <span class="title"><i class="{$menu3.menu3.ind_icono}"></i> {$menu3.menu3.ind_nombre}</span>
                                                                                            </a>
                                                                                            <ul>
                                                                                                {foreach item=menu4 from=$menu3.menu4}
                                                                                                    {if in_array($menu4.menu4.ind_rol,$_Parametros.perfil)}
                                                                                                        {if $menu4.menu5 != null }
                                                                                                            <li class="gui-folder">
                                                                                                                <a>
                                                                                                                    <span class="title"><i class="{$menu4.menu4.ind_icono}"></i> {$menu4.menu4.ind_nombre}</span>
                                                                                                                </a>
                                                                                                                <ul>
                                                                                                                    {foreach item=menu5 from=$menu4.menu5}
                                                                                                                        {if in_array($menu5.menu5.ind_rol,$_Parametros.perfil)}
                                                                                                                            <li link="1">
                                                                                                                                <a href="{$_Parametros.url}{$menu5.menu5.ind_ruta}">
                                                                                                                                    <span class="title"><i class="{$menu5.menu5.ind_icono}"></i> {$menu5.menu5.ind_nombre}</span>
                                                                                                                                </a>
                                                                                                                            </li>
                                                                                                                        {/if}
                                                                                                                    {/foreach}
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        {else}
                                                                                                            <li link="1">
                                                                                                                <a href="{$_Parametros.url}{$menu4.menu4.ind_ruta}">
                                                                                                                    <span class="title"><i class="{$menu4.menu4.ind_icono}"></i> {$menu4.menu4.ind_nombre}</span>
                                                                                                                </a>
                                                                                                            </li>
                                                                                                        {/if}
                                                                                                    {/if}
                                                                                                {/foreach}
                                                                                            </ul>
                                                                                        </li>
                                                                                    {else}
                                                                                        <li link="1">
                                                                                            <a href="{$_Parametros.url}{$menu3.menu3.ind_ruta}">
                                                                                                <span class="title"><i class="{$menu3.menu3.ind_icono}"></i> {$menu3.menu3.ind_nombre}</span>
                                                                                            </a>
                                                                                        </li>
                                                                                    {/if}
                                                                                {/if}
                                                                            {/foreach}
                                                                        </ul>
                                                                    </li>
                                                                {else}
                                                                    <li link="1">
                                                                        <a href="{$_Parametros.url}{$menu2.menu2.ind_ruta}">
                                                                            <span class="title"><i class="{$menu2.menu2.ind_icono}"></i> {$menu2.menu2.ind_nombre}</span>
                                                                        </a>
                                                                    </li>
                                                                {/if}
                                                            {/if}
                                                        {/foreach}
                                                    </ul>
                                                </li>
                                            {else}
                                                <li link="1">
                                                    <a href="{$_Parametros.url}{$menu1.menu1.ind_ruta}">
                                                        <span class="title"><i class="{$menu1.menu1.ind_icono}"></i> {$menu1.menu1.ind_nombre}</span>
                                                    </a>
                                                </li>
                                            {/if}
                                        {/if}
                                    {/foreach}
                                </ul>
                            </li>
                        {else}
                            <li link="1">
                                <a href="{$_Parametros.url}{$menu.menu.ind_ruta}">
                                    <div class="gui-icon"><i class="{$menu.menu.ind_icono}"></i></div>
                                    <span class="title"> {$menu.menu.ind_nombre}</span>
                                </a>
                            </li>
                        {/if}
                    {/if}
                {/foreach}
            </ul>
            <!-- END MAIN MENU -->

            <div class="menubar-foot-panel">
                <small class="no-linebreak hidden-folded">
                    <span class="opacity-75">SIACE &copy; 2016</span> <strong>CONTRALORIA DEL ESTADO</strong>
                </small>
            </div>
        </div><!--end .menubar-scroll-panel-->
    </div><!--end #menubar-->
    <div class="offcanvas"></div>
    <div id="content">
        <section>
            <div id="_contenido"></div>
        </section>
    </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" id="modalAncho">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="cerrarModal" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">s</h4>
            </div>
            <div class="modal-body"  id="ContenidoModal">
                asd
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="formModal2" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" id="modalAncho2">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="cerrarModal2" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel2">s</h4>
            </div>
            <div class="modal-body"  id="ContenidoModal2">
                asd
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="formModal3" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" id="modalAncho3">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="cerrarModal3" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel3">s</h4>
            </div>
            <div class="modal-body"  id="ContenidoModal3">
                asd
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- chat modal-->
<div class="modal fade" id="formModalChatIntranet" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" id="modalAnchoChatIntranet">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="cerrarModalChatIntranet" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabelChatIntranet">s</h4>
            </div>
            <div class="modal-body"  id="ContenidoModalChatIntranet">
                contenido
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<input type="hidden" id="conta_oculto" value="0" />
<input type="hidden" id="notificaciones_chat" value="{$estadoChat}" />
<!-- -->
<script type="text/javascript">
    $.post("{$_Parametros.url}"+"index/vistaModulo/{$moduloV}",{  },function(dato){
        $('#_contenido').html(dato);
    });

    $(document).ready(function() {
        $('.logsUsuario').click(function () {
            var url='{$_Parametros.url}logs/registrarLogs';
            $.post(url,{ descripcion: $(this).attr('descipcion'), tipo: 1},function(dato){ },'json');
        });
        $('#main-menu').on( 'click', 'li', function () {
            if($(this).attr('modulo')!== undefined){
                $( "#main-menu li" ).each(function() {
                    if($(this).attr('modulo')!== undefined){
                        $(this).removeClass( "active expanding" );
                    }else{
                        $(this).removeClass( "active expanding" );
                    }
                });
                $(this).addClass('active expanding');
            }

            if($(this).attr('link')!== undefined){
                $( "#main-menu li" ).each(function() {
                    if($(this).attr('link')!== undefined){
                        $(this).removeClass( "active expanding" );
                    }
                });
                $(this).addClass('active expanding');
            }
        });
        $('#main-menu').on( 'click', 'a', function (event) {
            event.preventDefault();
            if($(this).attr('href')!== undefined){
                $.post($(this).attr('href'),{  },function(dato){
                    $('#_contenido').html(dato);
                });
            }
        });


        //***********************************************
        //* ACTUALIZACIÓN DE LAS NOTIFICACIONES DEL CHAT
        //***********************************************

        function actualizarNotificacionChat() {
            var contador = $('#conta_oculto').val();

            var url_chat = '{$_Parametros.url}index/NotificacionChatPublico';
            $.post(url_chat, { contador: contador}, function (dato) {
                if (dato > 0)//hay mensajes nuevos
                {
                    $('#notificacion_chat').html('<span id="notificacion_chat"><i class="md md-chat"></i><sup class="badge style-danger"><i class="md-mail"></i></sup></span>');
                    $('#conta_oculto').val(dato);
                }

            });
        }

        if($("#notificaciones_chat").val()==1){
            tiempo3 = setInterval (actualizarNotificacionChat, 5000);//actualizarNotificacionChat();
        }


        //***********************************************
        //* ACTUALIZAR EL CHAT AL ABRIR EL PANEL
        //***********************************************
        $(".abrir_chat_btn").click(function () {

            if($("#notificaciones_chat").val()==1) {

                $('#lista_chat').html('');
                var url_chat = '{$_Parametros.url}index/ListaChatPublico';
                $.post(url_chat, '', function (dato) {

                    $('#lista_chat').html(dato);
                    $('#notificacion_chat').html('<span id="notificacion_chat"><i class="md md-chat"></i></span>');//reiniciamos la notificacion

                });

                tiempo3 = clearInterval(tiempo3);//paramos la actualización de la notificacion chat
                //---------------------------------------------
                $('#modalAnchoChatIntranet').css("width", "30%");
                $('#modalAnchoChatIntranet').css("height", "100%");
                $('#modalAnchoChatIntranet').css("padding-left", "10px");
                $('#modalAnchoChatIntranet').css("padding-right", "10px");
                $('#formModalLabelChatIntranet').html('Sala de chat SIACE');
                $('#ContenidoModalChatIntranet').html('');
                var urModalChat = '{$_Parametros.url}index/CargarChatModal';
                $.post(urModalChat, '', function (dato) {
                    $('#ContenidoModalChatIntranet').html(dato);
                });

            }else{
                swal("Atención", "Chat No Activo", "warning");
                return false;
            }

        });
        //***********************************************
        //* CERRAR EL CHAT
        //***********************************************
        $(document.getElementById('cerrarModalChatIntranet')).click(function () {

            tiempo = clearInterval(tiempo);//paramos la actualización del chat
            $('#ContenidoModalChatIntranet').html('');
            var url_conta = '{$_Parametros.url}index/TotalMensajesOtros';
            $.post(url_conta, '', function (dato) {
                $('#conta_oculto').val(dato);
                $('#notificacion_chat').html('<span id="notificacion_chat"><i class="md md-chat"></i></span>');
                tiempo3 = setInterval(actualizarNotificacionChat, 5000);//iniciamos la actualizacion de las notificaciones
            });
        });




    });
</script>
</body>
</html>